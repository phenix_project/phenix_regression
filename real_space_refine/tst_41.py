from __future__ import print_function
import time, os
from libtbx import easy_run
import iotbx.pdb
import libtbx.load_env
from phenix_regression.real_space_refine import run_real_space_refine

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  Make sure use-provided NCS selections are kept.
  No quotes in param file
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/real_space_refine/data/tst_21.pdb",
    test=os.path.isfile)
  mtz = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/real_space_refine/data/tst_21.mtz",
    test=os.path.isfile)
  params = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/real_space_refine/data/tst_21_2.eff",
    test=os.path.isfile)
  #
  args = [
    "%s"%pdb,
    "%s"%mtz,
    "%s"%params,
    "macro_cycles=0",
    "ramachandran_plot_restraints.enabled=false",
    "rotamers.restraints.enabled=False",
    "secondary_structure.enabled=false",
    "dry_run=true"]
  r = run_real_space_refine(args = args, prefix = prefix)
  # check for expected output
  log = open(r.log, "r")
  log_lines = log.readlines()
  for i,l in enumerate(log_lines):
    log_lines[i] = l.strip()
  cntr=0
  for needed_string in [
      "Number of NCS constrained groups: 1",
      "pdb_interpretation.ncs_group {",
      "reference = chain 'Aa' or chain 'Ab' or chain 'Ac' or chain 'Ad' or chain 'Ae' or chain 'Af' or chain 'Ag' or chain 'Ah' or chain 'Ai' or chain 'Aj' or chain 'Ak'",
      "selection = chain 'Al' or chain 'Am' or chain 'An' or chain 'Ao' or chain 'Ap' or chain 'Aq' or chain 'Ar' or chain 'As' or chain 'At' or chain 'Au' or chain 'Av'",
      "}"
    ]:
    assert needed_string in log_lines
    cntr+=1
  assert cntr==5

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK time =%8.3f"%(time.time() - t0))
