from __future__ import print_function
from libtbx import easy_run
import time
import iotbx.pdb
from scitbx.array_family import flex
import os
from phenix_regression.real_space_refine import run_real_space_refine

pdb_str_answer = """\
CRYST1   27.475   35.191   27.490  90.00  90.00  90.00 P 21 21 21    0
HELIX    1   1 ALA E    1  ALA E   16  1                                  16
ATOM      1  N   ALA E   1       6.709  31.817   5.664  1.00 20.00           N
ATOM      2  CA  ALA E   1       7.794  30.914   6.029  1.00 20.00           C
ATOM      3  C   ALA E   1       7.267  29.691   6.773  1.00 20.00           C
ATOM      4  O   ALA E   1       7.942  29.146   7.647  1.00 20.00           O
ATOM      5  CB  ALA E   1       8.831  31.643   6.870  1.00 20.00           C
ATOM      0  H1  ALA E   1       7.048  32.594   5.392  1.00 20.00           H   new
ATOM      0  H2  ALA E   1       6.230  31.456   5.006  1.00 20.00           H   new
ATOM      0  H3  ALA E   1       6.186  31.954   6.371  1.00 20.00           H   new
ATOM      0  HA  ALA E   1       8.216  30.607   5.211  1.00 20.00           H   new
ATOM      0  HB1 ALA E   1       9.546  31.031   7.105  1.00 20.00           H   new
ATOM      0  HB2 ALA E   1       9.195  32.385   6.363  1.00 20.00           H   new
ATOM      0  HB3 ALA E   1       8.414  31.978   7.679  1.00 20.00           H   new
ATOM      6  N   HIS E   2       6.057  29.267   6.421  1.00 20.00           N
ATOM      7  CA  HIS E   2       5.434  28.110   7.053  1.00 20.00           C
ATOM      8  C   HIS E   2       6.176  26.824   6.701  1.00 20.00           C
ATOM      9  O   HIS E   2       6.477  26.012   7.576  1.00 20.00           O
ATOM     10  CB  HIS E   2       3.965  28.000   6.640  1.00 20.00           C
ATOM     11  CG  HIS E   2       3.140  29.191   7.016  1.00 20.00           C
ATOM     12  ND1 HIS E   2       2.481  29.289   8.222  1.00 20.00           N
ATOM     13  CD2 HIS E   2       2.869  30.335   6.345  1.00 20.00           C
ATOM     14  CE1 HIS E   2       1.838  30.441   8.278  1.00 20.00           C
ATOM     15  NE2 HIS E   2       2.057  31.096   7.152  1.00 20.00           N
ATOM      0  H   HIS E   2       5.577  29.640   5.813  1.00 20.00           H   new
ATOM      0  HA  HIS E   2       5.482  28.235   8.014  1.00 20.00           H   new
ATOM      0  HB2 HIS E   2       3.917  27.874   5.679  1.00 20.00           H   new
ATOM      0  HB3 HIS E   2       3.581  27.209   7.050  1.00 20.00           H   new
ATOM      0  HD2 HIS E   2       3.175  30.563   5.497  1.00 20.00           H   new
ATOM      0  HE1 HIS E   2       1.319  30.740   8.989  1.00 20.00           H   new
ATOM     16  N   CYS E   3       6.468  26.648   5.417  1.00 20.00           N
ATOM     17  CA  CYS E   3       7.193  25.473   4.949  1.00 20.00           C
ATOM     18  C   CYS E   3       8.635  25.492   5.443  1.00 20.00           C
ATOM     19  O   CYS E   3       9.223  24.444   5.712  1.00 20.00           O
ATOM     20  CB  CYS E   3       7.161  25.396   3.421  1.00 20.00           C
ATOM     21  SG  CYS E   3       5.501  25.285   2.714  1.00 20.00           S
ATOM      0  H   CYS E   3       6.253  27.203   4.796  1.00 20.00           H   new
ATOM      0  HA  CYS E   3       6.755  24.687   5.311  1.00 20.00           H   new
ATOM      0  HB2 CYS E   3       7.603  26.180   3.059  1.00 20.00           H   new
ATOM      0  HB3 CYS E   3       7.674  24.624   3.136  1.00 20.00           H   new
ATOM      0  HG  CYS E   3       5.578  25.232   1.518  1.00 20.00           H   new
ATOM     22  N   ALA E   4       9.197  26.690   5.560  1.00 20.00           N
ATOM     23  CA  ALA E   4      10.560  26.855   6.050  1.00 20.00           C
ATOM     24  C   ALA E   4      10.655  26.475   7.523  1.00 20.00           C
ATOM     25  O   ALA E   4      11.599  25.805   7.942  1.00 20.00           O
ATOM     26  CB  ALA E   4      11.031  28.285   5.837  1.00 20.00           C
ATOM      0  H   ALA E   4       8.801  27.427   5.359  1.00 20.00           H   new
ATOM      0  HA  ALA E   4      11.138  26.261   5.546  1.00 20.00           H   new
ATOM      0  HB1 ALA E   4      11.938  28.380   6.167  1.00 20.00           H   new
ATOM      0  HB2 ALA E   4      11.008  28.496   4.890  1.00 20.00           H   new
ATOM      0  HB3 ALA E   4      10.447  28.893   6.317  1.00 20.00           H   new
ATOM     27  N   ILE E   5       9.669  26.907   8.304  1.00 20.00           N
ATOM     28  CA  ILE E   5       9.621  26.584   9.725  1.00 20.00           C
ATOM     29  C   ILE E   5       9.325  25.103   9.932  1.00 20.00           C
ATOM     30  O   ILE E   5       9.836  24.482  10.865  1.00 20.00           O
ATOM     31  CB  ILE E   5       8.580  27.459  10.467  1.00 20.00           C
ATOM     32  CG1 ILE E   5       9.071  28.905  10.559  1.00 20.00           C
ATOM     33  CG2 ILE E   5       8.307  26.928  11.866  1.00 20.00           C
ATOM     34  CD1 ILE E   5       8.105  29.836  11.259  1.00 20.00           C
ATOM      0  H   ILE E   5       9.015  27.392   8.027  1.00 20.00           H   new
ATOM      0  HA  ILE E   5      10.494  26.778  10.102  1.00 20.00           H   new
ATOM      0  HB  ILE E   5       7.754  27.427   9.959  1.00 20.00           H   new
ATOM      0 HG12 ILE E   5       9.919  28.920  11.029  1.00 20.00           H   new
ATOM      0 HG13 ILE E   5       9.237  29.239   9.663  1.00 20.00           H   new
ATOM      0 HG21 ILE E   5       7.653  27.494  12.305  1.00 20.00           H   new
ATOM      0 HG22 ILE E   5       7.963  26.023  11.807  1.00 20.00           H   new
ATOM      0 HG23 ILE E   5       9.130  26.928  12.379  1.00 20.00           H   new
ATOM      0 HD11 ILE E   5       8.477  30.731  11.282  1.00 20.00           H   new
ATOM      0 HD12 ILE E   5       7.262  29.849  10.779  1.00 20.00           H   new
ATOM      0 HD13 ILE E   5       7.955  29.525  12.166  1.00 20.00           H   new
ATOM     35  N   TYR E   6       8.498  24.543   9.054  1.00 20.00           N
ATOM     36  CA  TYR E   6       8.157  23.127   9.116  1.00 20.00           C
ATOM     37  C   TYR E   6       9.375  22.264   8.811  1.00 20.00           C
ATOM     38  O   TYR E   6       9.598  21.239   9.454  1.00 20.00           O
ATOM     39  CB  TYR E   6       7.017  22.795   8.150  1.00 20.00           C
ATOM     40  CG  TYR E   6       5.662  23.291   8.603  1.00 20.00           C
ATOM     41  CD1 TYR E   6       5.434  23.633   9.930  1.00 20.00           C
ATOM     42  CD2 TYR E   6       4.610  23.413   7.705  1.00 20.00           C
ATOM     43  CE1 TYR E   6       4.197  24.086  10.349  1.00 20.00           C
ATOM     44  CE2 TYR E   6       3.369  23.865   8.114  1.00 20.00           C
ATOM     45  CZ  TYR E   6       3.169  24.200   9.437  1.00 20.00           C
ATOM     46  OH  TYR E   6       1.935  24.650   9.849  1.00 20.00           O
ATOM      0  H   TYR E   6       8.121  24.971   8.410  1.00 20.00           H   new
ATOM      0  HA  TYR E   6       7.859  22.933  10.018  1.00 20.00           H   new
ATOM      0  HB2 TYR E   6       7.218  23.179   7.283  1.00 20.00           H   new
ATOM      0  HB3 TYR E   6       6.976  21.833   8.031  1.00 20.00           H   new
ATOM      0  HD1 TYR E   6       6.125  23.556  10.547  1.00 20.00           H   new
ATOM      0  HD2 TYR E   6       4.742  23.187   6.813  1.00 20.00           H   new
ATOM      0  HE1 TYR E   6       4.059  24.312  11.240  1.00 20.00           H   new
ATOM      0  HE2 TYR E   6       2.674  23.943   7.501  1.00 20.00           H   new
ATOM      0  HH  TYR E   6       1.410  24.670   9.193  1.00 20.00           H   new
ATOM     47  N   THR E   7      10.161  22.688   7.825  1.00 20.00           N
ATOM     48  CA  THR E   7      11.380  21.976   7.459  1.00 20.00           C
ATOM     49  C   THR E   7      12.441  22.134   8.541  1.00 20.00           C
ATOM     50  O   THR E   7      13.199  21.204   8.823  1.00 20.00           O
ATOM     51  CB  THR E   7      11.948  22.456   6.109  1.00 20.00           C
ATOM     52  OG1 THR E   7      11.987  23.888   6.086  1.00 20.00           O
ATOM     53  CG2 THR E   7      11.086  21.957   4.959  1.00 20.00           C
ATOM      0  H   THR E   7      10.004  23.390   7.354  1.00 20.00           H   new
ATOM      0  HA  THR E   7      11.143  21.040   7.371  1.00 20.00           H   new
ATOM      0  HB  THR E   7      12.845  22.100   6.006  1.00 20.00           H   new
ATOM      0  HG1 THR E   7      11.855  24.185   6.861  1.00 20.00           H   new
ATOM      0 HG21 THR E   7      11.457  22.267   4.118  1.00 20.00           H   new
ATOM      0 HG22 THR E   7      11.067  20.987   4.965  1.00 20.00           H   new
ATOM      0 HG23 THR E   7      10.183  22.298   5.059  1.00 20.00           H   new
ATOM     54  N   ILE E   8      12.489  23.318   9.145  1.00 20.00           N
ATOM     55  CA  ILE E   8      13.433  23.596  10.220  1.00 20.00           C
ATOM     56  C   ILE E   8      13.128  22.736  11.441  1.00 20.00           C
ATOM     57  O   ILE E   8      14.038  22.232  12.099  1.00 20.00           O
ATOM     58  CB  ILE E   8      13.437  25.097  10.601  1.00 20.00           C
ATOM     59  CG1 ILE E   8      14.259  25.898   9.591  1.00 20.00           C
ATOM     60  CG2 ILE E   8      14.003  25.309  11.997  1.00 20.00           C
ATOM     61  CD1 ILE E   8      14.305  27.383   9.880  1.00 20.00           C
ATOM      0  H   ILE E   8      11.977  23.979   8.944  1.00 20.00           H   new
ATOM      0  HA  ILE E   8      14.319  23.371   9.896  1.00 20.00           H   new
ATOM      0  HB  ILE E   8      12.518  25.407  10.590  1.00 20.00           H   new
ATOM      0 HG12 ILE E   8      15.165  25.552   9.577  1.00 20.00           H   new
ATOM      0 HG13 ILE E   8      13.889  25.761   8.705  1.00 20.00           H   new
ATOM      0 HG21 ILE E   8      13.994  26.255  12.210  1.00 20.00           H   new
ATOM      0 HG22 ILE E   8      13.462  24.828  12.642  1.00 20.00           H   new
ATOM      0 HG23 ILE E   8      14.915  24.979  12.030  1.00 20.00           H   new
ATOM      0 HD11 ILE E   8      14.840  27.828   9.204  1.00 20.00           H   new
ATOM      0 HD12 ILE E   8      13.404  27.743   9.868  1.00 20.00           H   new
ATOM      0 HD13 ILE E   8      14.700  27.531  10.754  1.00 20.00           H   new
ATOM     62  N   HIS E   9      11.843  22.573  11.737  1.00 20.00           N
ATOM     63  CA  HIS E   9      11.415  21.732  12.848  1.00 20.00           C
ATOM     64  C   HIS E   9      11.639  20.260  12.519  1.00 20.00           C
ATOM     65  O   HIS E   9      11.974  19.460  13.395  1.00 20.00           O
ATOM     66  CB  HIS E   9       9.943  21.982  13.182  1.00 20.00           C
ATOM     67  CG  HIS E   9       9.681  23.326  13.789  1.00 20.00           C
ATOM     68  ND1 HIS E   9      10.679  24.249  14.013  1.00 20.00           N
ATOM     69  CD2 HIS E   9       8.534  23.900  14.221  1.00 20.00           C
ATOM     70  CE1 HIS E   9      10.158  25.335  14.556  1.00 20.00           C
ATOM     71  NE2 HIS E   9       8.858  25.149  14.693  1.00 20.00           N
ATOM      0  H   HIS E   9      11.199  22.944  11.303  1.00 20.00           H   new
ATOM      0  HA  HIS E   9      11.948  21.962  13.625  1.00 20.00           H   new
ATOM      0  HB2 HIS E   9       9.417  21.894  12.372  1.00 20.00           H   new
ATOM      0  HB3 HIS E   9       9.638  21.295  13.795  1.00 20.00           H   new
ATOM      0  HD2 HIS E   9       7.685  23.520  14.202  1.00 20.00           H   new
ATOM      0  HE1 HIS E   9      10.627  26.100  14.800  1.00 20.00           H   new
ATOM     72  N   SER E  10      11.450  19.912  11.250  1.00 20.00           N
ATOM     73  CA  SER E  10      11.664  18.546  10.785  1.00 20.00           C
ATOM     74  C   SER E  10      13.125  18.147  10.944  1.00 20.00           C
ATOM     75  O   SER E  10      13.431  17.029  11.357  1.00 20.00           O
ATOM     76  CB  SER E  10      11.237  18.394   9.324  1.00 20.00           C
ATOM     77  OG  SER E  10      12.163  19.021   8.455  1.00 20.00           O
ATOM      0  H   SER E  10      11.195  20.458  10.637  1.00 20.00           H   new
ATOM      0  HA  SER E  10      11.118  17.957  11.329  1.00 20.00           H   new
ATOM      0  HB2 SER E  10      11.166  17.453   9.101  1.00 20.00           H   new
ATOM      0  HB3 SER E  10      10.357  18.783   9.199  1.00 20.00           H   new
ATOM      0  HG  SER E  10      12.451  19.724   8.813  1.00 20.00           H   new
ATOM     78  N   VAL E  11      14.025  19.068  10.613  1.00 20.00           N
ATOM     79  CA  VAL E  11      15.454  18.835  10.782  1.00 20.00           C
ATOM     80  C   VAL E  11      15.820  18.850  12.262  1.00 20.00           C
ATOM     81  O   VAL E  11      16.689  18.095  12.705  1.00 20.00           O
ATOM     82  CB  VAL E  11      16.292  19.885  10.015  1.00 20.00           C
ATOM     83  CG1 VAL E  11      17.776  19.723  10.312  1.00 20.00           C
ATOM     84  CG2 VAL E  11      16.036  19.776   8.520  1.00 20.00           C
ATOM      0  H   VAL E  11      13.826  19.839  10.287  1.00 20.00           H   new
ATOM      0  HA  VAL E  11      15.658  17.962  10.413  1.00 20.00           H   new
ATOM      0  HB  VAL E  11      16.020  20.766  10.315  1.00 20.00           H   new
ATOM      0 HG11 VAL E  11      18.279  20.391   9.821  1.00 20.00           H   new
ATOM      0 HG12 VAL E  11      17.930  19.837  11.263  1.00 20.00           H   new
ATOM      0 HG13 VAL E  11      18.065  18.837  10.042  1.00 20.00           H   new
ATOM      0 HG21 VAL E  11      16.567  20.439   8.051  1.00 20.00           H   new
ATOM      0 HG22 VAL E  11      16.282  18.889   8.213  1.00 20.00           H   new
ATOM      0 HG23 VAL E  11      15.095  19.930   8.341  1.00 20.00           H   new
ATOM     85  N   ASP E  12      15.147  19.714  13.017  1.00 20.00           N
ATOM     86  CA  ASP E  12      15.358  19.816  14.457  1.00 20.00           C
ATOM     87  C   ASP E  12      15.062  18.486  15.137  1.00 20.00           C
ATOM     88  O   ASP E  12      15.780  18.073  16.046  1.00 20.00           O
ATOM     89  CB  ASP E  12      14.495  20.925  15.064  1.00 20.00           C
ATOM     90  CG  ASP E  12      15.107  22.301  14.890  1.00 20.00           C
ATOM     91  OD1 ASP E  12      16.350  22.397  14.824  1.00 20.00           O
ATOM     92  OD2 ASP E  12      14.344  23.288  14.820  1.00 20.00           O
ATOM      0  H   ASP E  12      14.556  20.257  12.709  1.00 20.00           H   new
ATOM      0  HA  ASP E  12      16.290  20.042  14.606  1.00 20.00           H   new
ATOM      0  HB2 ASP E  12      13.618  20.910  14.651  1.00 20.00           H   new
ATOM      0  HB3 ASP E  12      14.366  20.749  16.009  1.00 20.00           H   new
ATOM     93  N   ALA E  13      14.001  17.822  14.690  1.00 20.00           N
ATOM     94  CA  ALA E  13      13.688  16.481  15.167  1.00 20.00           C
ATOM     95  C   ALA E  13      14.741  15.506  14.655  1.00 20.00           C
ATOM     96  O   ALA E  13      15.423  14.826  15.444  1.00 20.00           O
ATOM     97  CB  ALA E  13      12.303  16.062  14.706  1.00 20.00           C
ATOM      0  H   ALA E  13      13.448  18.132  14.108  1.00 20.00           H   new
ATOM      0  HA  ALA E  13      13.694  16.477  16.137  1.00 20.00           H   new
ATOM      0  HB1 ALA E  13      12.110  15.169  15.031  1.00 20.00           H   new
ATOM      0  HB2 ALA E  13      11.644  16.682  15.055  1.00 20.00           H   new
ATOM      0  HB3 ALA E  13      12.269  16.067  13.737  1.00 20.00           H   new
ATOM     98  N   PHE E  14      14.885  15.479  13.328  1.00 20.00           N
ATOM     99  CA  PHE E  14      15.842  14.623  12.623  1.00 20.00           C
ATOM    100  C   PHE E  14      17.192  14.548  13.323  1.00 20.00           C
ATOM    101  O   PHE E  14      17.881  13.533  13.245  1.00 20.00           O
ATOM    102  CB  PHE E  14      16.026  15.083  11.174  1.00 20.00           C
ATOM    103  CG  PHE E  14      14.911  14.666  10.258  1.00 20.00           C
ATOM    104  CD1 PHE E  14      14.035  13.657  10.624  1.00 20.00           C
ATOM    105  CD2 PHE E  14      14.739  15.281   9.029  1.00 20.00           C
ATOM    106  CE1 PHE E  14      13.008  13.271   9.783  1.00 20.00           C
ATOM    107  CE2 PHE E  14      13.714  14.900   8.183  1.00 20.00           C
ATOM    108  CZ  PHE E  14      12.848  13.893   8.561  1.00 20.00           C
ATOM      0  H   PHE E  14      14.417  15.971  12.800  1.00 20.00           H   new
ATOM      0  HA  PHE E  14      15.464  13.730  12.629  1.00 20.00           H   new
ATOM      0  HB2 PHE E  14      16.102  16.050  11.158  1.00 20.00           H   new
ATOM      0  HB3 PHE E  14      16.862  14.727  10.834  1.00 20.00           H   new
ATOM      0  HD1 PHE E  14      14.139  13.234  11.446  1.00 20.00           H   new
ATOM      0  HD2 PHE E  14      15.320  15.959   8.770  1.00 20.00           H   new
ATOM      0  HE1 PHE E  14      12.426  12.593  10.040  1.00 20.00           H   new
ATOM      0  HE2 PHE E  14      13.608  15.321   7.361  1.00 20.00           H   new
ATOM      0  HZ  PHE E  14      12.158  13.634   7.993  1.00 20.00           H   new
ATOM    109  N   ALA E  15      17.558  15.630  14.003  1.00 20.00           N
ATOM    110  CA  ALA E  15      18.699  15.619  14.904  1.00 20.00           C
ATOM    111  C   ALA E  15      18.268  15.190  16.307  1.00 20.00           C
ATOM    112  O   ALA E  15      19.035  14.538  17.024  1.00 20.00           O
ATOM    113  CB  ALA E  15      19.357  16.988  14.944  1.00 20.00           C
ATOM      0  H   ALA E  15      17.152  16.387  13.954  1.00 20.00           H   new
ATOM      0  HA  ALA E  15      19.347  14.977  14.573  1.00 20.00           H   new
ATOM      0  HB1 ALA E  15      20.115  16.965  15.548  1.00 20.00           H   new
ATOM      0  HB2 ALA E  15      19.660  17.228  14.054  1.00 20.00           H   new
ATOM      0  HB3 ALA E  15      18.716  17.646  15.255  1.00 20.00           H   new
ATOM    114  N   GLU E  16      17.036  15.542  16.687  1.00 20.00           N
ATOM    115  CA  GLU E  16      16.549  15.284  18.047  1.00 20.00           C
ATOM    116  C   GLU E  16      16.685  13.831  18.518  1.00 20.00           C
ATOM    117  O   GLU E  16      17.064  13.664  19.676  1.00 20.00           O
ATOM    118  CB  GLU E  16      15.119  15.792  18.273  1.00 20.00           C
ATOM    119  CG  GLU E  16      15.040  17.145  18.965  1.00 20.00           C
ATOM    120  CD  GLU E  16      13.612  17.612  19.170  1.00 20.00           C
ATOM    121  OE1 GLU E  16      12.682  16.879  18.772  1.00 20.00           O
ATOM    122  OE2 GLU E  16      13.420  18.711  19.730  1.00 20.00           O
ATOM      0  H   GLU E  16      16.467  15.931  16.173  1.00 20.00           H   new
ATOM      0  HA  GLU E  16      17.151  15.803  18.603  1.00 20.00           H   new
ATOM      0  HB2 GLU E  16      14.668  15.851  17.416  1.00 20.00           H   new
ATOM      0  HB3 GLU E  16      14.635  15.140  18.803  1.00 20.00           H   new
ATOM      0  HG2 GLU E  16      15.486  17.091  19.825  1.00 20.00           H   new
ATOM      0  HG3 GLU E  16      15.519  17.803  18.437  1.00 20.00           H   new
TER
"""

pdb_str_poor = """\
CRYST1   27.475   35.191   27.490  90.00  90.00  90.00 P 21 21 21
ATOM      1  N   ALA E   1       9.709  31.817   5.664  1.00 20.00           N
ATOM      2  CA  ALA E   1      10.794  30.914   6.029  1.00 20.00           C
ATOM      3  C   ALA E   1      10.267  29.691   6.773  1.00 20.00           C
ATOM      4  O   ALA E   1      10.942  29.146   7.647  1.00 20.00           O
ATOM      5  CB  ALA E   1      11.831  31.643   6.870  1.00 20.00           C
ATOM      6  H1  ALA E   1      10.048  32.594   5.392  1.00 20.00           H
ATOM      7  H2  ALA E   1       9.230  31.456   5.006  1.00 20.00           H
ATOM      8  H3  ALA E   1       9.186  31.954   6.371  1.00 20.00           H
ATOM      9  HA  ALA E   1      11.216  30.607   5.211  1.00 20.00           H
ATOM     10  HB1 ALA E   1      12.546  31.031   7.105  1.00 20.00           H
ATOM     11  HB2 ALA E   1      12.195  32.385   6.363  1.00 20.00           H
ATOM     12  HB3 ALA E   1      11.414  31.978   7.679  1.00 20.00           H
ATOM     13  N   HIS E   2       9.057  29.267   6.421  1.00 20.00           N
ATOM     14  CA  HIS E   2       8.434  28.110   7.053  1.00 20.00           C
ATOM     15  C   HIS E   2       9.176  26.824   6.701  1.00 20.00           C
ATOM     16  O   HIS E   2       9.477  26.012   7.576  1.00 20.00           O
ATOM     17  CB  HIS E   2       6.965  28.000   6.640  1.00 20.00           C
ATOM     18  CG  HIS E   2       6.140  29.191   7.016  1.00 20.00           C
ATOM     19  ND1 HIS E   2       5.481  29.289   8.222  1.00 20.00           N
ATOM     20  CD2 HIS E   2       5.869  30.335   6.345  1.00 20.00           C
ATOM     21  CE1 HIS E   2       4.838  30.441   8.278  1.00 20.00           C
ATOM     22  NE2 HIS E   2       5.057  31.096   7.152  1.00 20.00           N
ATOM     23  H   HIS E   2       8.577  29.640   5.813  1.00 20.00           H
ATOM     24  HA  HIS E   2       8.482  28.235   8.014  1.00 20.00           H
ATOM     25  HB2 HIS E   2       6.917  27.874   5.679  1.00 20.00           H
ATOM     26  HB3 HIS E   2       6.581  27.209   7.050  1.00 20.00           H
ATOM     27  HD2 HIS E   2       6.175  30.563   5.497  1.00 20.00           H
ATOM     28  HE1 HIS E   2       4.319  30.740   8.989  1.00 20.00           H
ATOM     29  N   CYS E   3       9.468  26.648   5.417  1.00 20.00           N
ATOM     30  CA  CYS E   3      10.193  25.473   4.949  1.00 20.00           C
ATOM     31  C   CYS E   3      11.635  25.492   5.443  1.00 20.00           C
ATOM     32  O   CYS E   3      12.223  24.444   5.712  1.00 20.00           O
ATOM     33  CB  CYS E   3      10.161  25.396   3.421  1.00 20.00           C
ATOM     34  SG  CYS E   3       8.501  25.285   2.714  1.00 20.00           S
ATOM     35  H   CYS E   3       9.253  27.203   4.796  1.00 20.00           H
ATOM     36  HA  CYS E   3       9.755  24.687   5.311  1.00 20.00           H
ATOM     37  HB2 CYS E   3      10.603  26.180   3.059  1.00 20.00           H
ATOM     38  HB3 CYS E   3      10.674  24.624   3.136  1.00 20.00           H
ATOM     39  HG  CYS E   3       8.578  25.232   1.518  1.00 20.00           H
ATOM     40  N   ALA E   4      12.197  26.690   5.560  1.00 20.00           N
ATOM     41  CA  ALA E   4      13.560  26.855   6.050  1.00 20.00           C
ATOM     42  C   ALA E   4      13.655  26.475   7.523  1.00 20.00           C
ATOM     43  O   ALA E   4      14.599  25.805   7.942  1.00 20.00           O
ATOM     44  CB  ALA E   4      14.031  28.285   5.837  1.00 20.00           C
ATOM     45  H   ALA E   4      11.801  27.427   5.359  1.00 20.00           H
ATOM     46  HA  ALA E   4      14.138  26.261   5.546  1.00 20.00           H
ATOM     47  HB1 ALA E   4      14.938  28.380   6.167  1.00 20.00           H
ATOM     48  HB2 ALA E   4      14.008  28.496   4.890  1.00 20.00           H
ATOM     49  HB3 ALA E   4      13.447  28.893   6.317  1.00 20.00           H
ATOM     50  N   ILE E   5      12.669  26.907   8.304  1.00 20.00           N
ATOM     51  CA  ILE E   5      12.621  26.584   9.725  1.00 20.00           C
ATOM     52  C   ILE E   5      12.325  25.103   9.932  1.00 20.00           C
ATOM     53  O   ILE E   5      12.836  24.482  10.865  1.00 20.00           O
ATOM     54  CB  ILE E   5      11.580  27.459  10.467  1.00 20.00           C
ATOM     55  CG1 ILE E   5      12.071  28.905  10.559  1.00 20.00           C
ATOM     56  CG2 ILE E   5      11.307  26.928  11.866  1.00 20.00           C
ATOM     57  CD1 ILE E   5      11.105  29.836  11.259  1.00 20.00           C
ATOM     58  H   ILE E   5      12.015  27.392   8.027  1.00 20.00           H
ATOM     59  HA  ILE E   5      13.494  26.778  10.102  1.00 20.00           H
ATOM     60  HB  ILE E   5      10.754  27.427   9.959  1.00 20.00           H
ATOM     61 HG12 ILE E   5      12.919  28.920  11.029  1.00 20.00           H
ATOM     62 HG13 ILE E   5      12.237  29.239   9.663  1.00 20.00           H
ATOM     63 HG21 ILE E   5      10.653  27.494  12.305  1.00 20.00           H
ATOM     64 HG22 ILE E   5      10.963  26.023  11.807  1.00 20.00           H
ATOM     65 HG23 ILE E   5      12.130  26.928  12.379  1.00 20.00           H
ATOM     66 HD11 ILE E   5      11.477  30.731  11.282  1.00 20.00           H
ATOM     67 HD12 ILE E   5      10.262  29.849  10.779  1.00 20.00           H
ATOM     68 HD13 ILE E   5      10.955  29.525  12.166  1.00 20.00           H
ATOM     69  N   TYR E   6      11.498  24.543   9.054  1.00 20.00           N
ATOM     70  CA  TYR E   6      11.157  23.127   9.116  1.00 20.00           C
ATOM     71  C   TYR E   6      12.375  22.264   8.811  1.00 20.00           C
ATOM     72  O   TYR E   6      12.598  21.239   9.454  1.00 20.00           O
ATOM     73  CB  TYR E   6      10.017  22.795   8.150  1.00 20.00           C
ATOM     74  CG  TYR E   6       8.662  23.291   8.603  1.00 20.00           C
ATOM     75  CD1 TYR E   6       8.434  23.633   9.930  1.00 20.00           C
ATOM     76  CD2 TYR E   6       7.610  23.413   7.705  1.00 20.00           C
ATOM     77  CE1 TYR E   6       7.197  24.086  10.349  1.00 20.00           C
ATOM     78  CE2 TYR E   6       6.369  23.865   8.114  1.00 20.00           C
ATOM     79  CZ  TYR E   6       6.169  24.200   9.437  1.00 20.00           C
ATOM     80  OH  TYR E   6       4.935  24.650   9.849  1.00 20.00           O
ATOM     81  H   TYR E   6      11.121  24.971   8.410  1.00 20.00           H
ATOM     82  HA  TYR E   6      10.859  22.933  10.018  1.00 20.00           H
ATOM     83  HB2 TYR E   6      10.218  23.179   7.283  1.00 20.00           H
ATOM     84  HB3 TYR E   6       9.976  21.833   8.031  1.00 20.00           H
ATOM     85  HD1 TYR E   6       9.125  23.556  10.547  1.00 20.00           H
ATOM     86  HD2 TYR E   6       7.742  23.187   6.813  1.00 20.00           H
ATOM     87  HE1 TYR E   6       7.059  24.312  11.240  1.00 20.00           H
ATOM     88  HE2 TYR E   6       5.674  23.943   7.501  1.00 20.00           H
ATOM     89  HH  TYR E   6       4.410  24.670   9.193  1.00 20.00           H
ATOM     90  N   THR E   7      13.161  22.688   7.825  1.00 20.00           N
ATOM     91  CA  THR E   7      14.380  21.976   7.459  1.00 20.00           C
ATOM     92  C   THR E   7      15.441  22.134   8.541  1.00 20.00           C
ATOM     93  O   THR E   7      16.199  21.204   8.823  1.00 20.00           O
ATOM     94  CB  THR E   7      14.948  22.456   6.109  1.00 20.00           C
ATOM     95  OG1 THR E   7      14.987  23.888   6.086  1.00 20.00           O
ATOM     96  CG2 THR E   7      14.086  21.957   4.959  1.00 20.00           C
ATOM     97  H   THR E   7      13.004  23.390   7.354  1.00 20.00           H
ATOM     98  HA  THR E   7      14.143  21.040   7.371  1.00 20.00           H
ATOM     99  HB  THR E   7      15.845  22.100   6.006  1.00 20.00           H
ATOM    100  HG1 THR E   7      14.855  24.185   6.861  1.00 20.00           H
ATOM    101 HG21 THR E   7      14.457  22.267   4.118  1.00 20.00           H
ATOM    102 HG22 THR E   7      14.067  20.987   4.965  1.00 20.00           H
ATOM    103 HG23 THR E   7      13.183  22.298   5.059  1.00 20.00           H
ATOM    104  N   ILE E   8      15.489  23.318   9.145  1.00 20.00           N
ATOM    105  CA  ILE E   8      16.433  23.596  10.220  1.00 20.00           C
ATOM    106  C   ILE E   8      16.128  22.736  11.441  1.00 20.00           C
ATOM    107  O   ILE E   8      17.038  22.232  12.099  1.00 20.00           O
ATOM    108  CB  ILE E   8      16.437  25.097  10.601  1.00 20.00           C
ATOM    109  CG1 ILE E   8      17.259  25.898   9.591  1.00 20.00           C
ATOM    110  CG2 ILE E   8      17.003  25.309  11.997  1.00 20.00           C
ATOM    111  CD1 ILE E   8      17.305  27.383   9.880  1.00 20.00           C
ATOM    112  H   ILE E   8      14.977  23.979   8.944  1.00 20.00           H
ATOM    113  HA  ILE E   8      17.319  23.371   9.896  1.00 20.00           H
ATOM    114  HB  ILE E   8      15.518  25.407  10.590  1.00 20.00           H
ATOM    115 HG12 ILE E   8      18.165  25.552   9.577  1.00 20.00           H
ATOM    116 HG13 ILE E   8      16.889  25.761   8.705  1.00 20.00           H
ATOM    117 HG21 ILE E   8      16.994  26.255  12.210  1.00 20.00           H
ATOM    118 HG22 ILE E   8      16.462  24.828  12.642  1.00 20.00           H
ATOM    119 HG23 ILE E   8      17.915  24.979  12.030  1.00 20.00           H
ATOM    120 HD11 ILE E   8      17.840  27.828   9.204  1.00 20.00           H
ATOM    121 HD12 ILE E   8      16.404  27.743   9.868  1.00 20.00           H
ATOM    122 HD13 ILE E   8      17.700  27.531  10.754  1.00 20.00           H
ATOM    123  N   HIS E   9      14.843  22.573  11.737  1.00 20.00           N
ATOM    124  CA  HIS E   9      14.415  21.732  12.848  1.00 20.00           C
ATOM    125  C   HIS E   9      14.639  20.260  12.519  1.00 20.00           C
ATOM    126  O   HIS E   9      14.974  19.460  13.395  1.00 20.00           O
ATOM    127  CB  HIS E   9      12.943  21.982  13.182  1.00 20.00           C
ATOM    128  CG  HIS E   9      12.681  23.326  13.789  1.00 20.00           C
ATOM    129  ND1 HIS E   9      13.679  24.249  14.013  1.00 20.00           N
ATOM    130  CD2 HIS E   9      11.534  23.900  14.221  1.00 20.00           C
ATOM    131  CE1 HIS E   9      13.158  25.335  14.556  1.00 20.00           C
ATOM    132  NE2 HIS E   9      11.858  25.149  14.693  1.00 20.00           N
ATOM    133  H   HIS E   9      14.199  22.944  11.303  1.00 20.00           H
ATOM    134  HA  HIS E   9      14.948  21.962  13.625  1.00 20.00           H
ATOM    135  HB2 HIS E   9      12.417  21.894  12.372  1.00 20.00           H
ATOM    136  HB3 HIS E   9      12.638  21.295  13.795  1.00 20.00           H
ATOM    137  HD2 HIS E   9      10.685  23.520  14.202  1.00 20.00           H
ATOM    138  HE1 HIS E   9      13.627  26.100  14.800  1.00 20.00           H
ATOM    139  N   SER E  10      14.450  19.912  11.250  1.00 20.00           N
ATOM    140  CA  SER E  10      14.664  18.546  10.785  1.00 20.00           C
ATOM    141  C   SER E  10      16.125  18.147  10.944  1.00 20.00           C
ATOM    142  O   SER E  10      16.431  17.029  11.357  1.00 20.00           O
ATOM    143  CB  SER E  10      14.237  18.394   9.324  1.00 20.00           C
ATOM    144  OG  SER E  10      15.163  19.021   8.455  1.00 20.00           O
ATOM    145  H   SER E  10      14.195  20.458  10.637  1.00 20.00           H
ATOM    146  HA  SER E  10      14.118  17.957  11.329  1.00 20.00           H
ATOM    147  HB2 SER E  10      14.166  17.453   9.101  1.00 20.00           H
ATOM    148  HB3 SER E  10      13.357  18.783   9.199  1.00 20.00           H
ATOM    149  HG  SER E  10      15.451  19.724   8.813  1.00 20.00           H
ATOM    150  N   VAL E  11      17.025  19.068  10.613  1.00 20.00           N
ATOM    151  CA  VAL E  11      18.454  18.835  10.782  1.00 20.00           C
ATOM    152  C   VAL E  11      18.820  18.850  12.262  1.00 20.00           C
ATOM    153  O   VAL E  11      19.689  18.095  12.705  1.00 20.00           O
ATOM    154  CB  VAL E  11      19.292  19.885  10.015  1.00 20.00           C
ATOM    155  CG1 VAL E  11      20.776  19.723  10.312  1.00 20.00           C
ATOM    156  CG2 VAL E  11      19.036  19.776   8.520  1.00 20.00           C
ATOM    157  H   VAL E  11      16.826  19.839  10.287  1.00 20.00           H
ATOM    158  HA  VAL E  11      18.658  17.962  10.413  1.00 20.00           H
ATOM    159  HB  VAL E  11      19.020  20.766  10.315  1.00 20.00           H
ATOM    160 HG11 VAL E  11      21.279  20.391   9.821  1.00 20.00           H
ATOM    161 HG12 VAL E  11      20.930  19.837  11.263  1.00 20.00           H
ATOM    162 HG13 VAL E  11      21.065  18.837  10.042  1.00 20.00           H
ATOM    163 HG21 VAL E  11      19.567  20.439   8.051  1.00 20.00           H
ATOM    164 HG22 VAL E  11      19.282  18.889   8.213  1.00 20.00           H
ATOM    165 HG23 VAL E  11      18.095  19.930   8.341  1.00 20.00           H
ATOM    166  N   ASP E  12      18.147  19.714  13.017  1.00 20.00           N
ATOM    167  CA  ASP E  12      18.358  19.816  14.457  1.00 20.00           C
ATOM    168  C   ASP E  12      18.062  18.486  15.137  1.00 20.00           C
ATOM    169  O   ASP E  12      18.780  18.073  16.046  1.00 20.00           O
ATOM    170  CB  ASP E  12      17.495  20.925  15.064  1.00 20.00           C
ATOM    171  CG  ASP E  12      18.107  22.301  14.890  1.00 20.00           C
ATOM    172  OD1 ASP E  12      19.350  22.397  14.824  1.00 20.00           O
ATOM    173  OD2 ASP E  12      17.344  23.288  14.820  1.00 20.00           O
ATOM    174  H   ASP E  12      17.556  20.257  12.709  1.00 20.00           H
ATOM    175  HA  ASP E  12      19.290  20.042  14.606  1.00 20.00           H
ATOM    176  HB2 ASP E  12      16.618  20.910  14.651  1.00 20.00           H
ATOM    177  HB3 ASP E  12      17.366  20.749  16.009  1.00 20.00           H
ATOM    178  N   ALA E  13      17.001  17.822  14.690  1.00 20.00           N
ATOM    179  CA  ALA E  13      16.688  16.481  15.167  1.00 20.00           C
ATOM    180  C   ALA E  13      17.741  15.506  14.655  1.00 20.00           C
ATOM    181  O   ALA E  13      18.423  14.826  15.444  1.00 20.00           O
ATOM    182  CB  ALA E  13      15.303  16.062  14.706  1.00 20.00           C
ATOM    183  H   ALA E  13      16.448  18.132  14.108  1.00 20.00           H
ATOM    184  HA  ALA E  13      16.694  16.477  16.137  1.00 20.00           H
ATOM    185  HB1 ALA E  13      15.110  15.169  15.031  1.00 20.00           H
ATOM    186  HB2 ALA E  13      14.644  16.682  15.055  1.00 20.00           H
ATOM    187  HB3 ALA E  13      15.269  16.067  13.737  1.00 20.00           H
ATOM    188  N   PHE E  14      17.885  15.479  13.328  1.00 20.00           N
ATOM    189  CA  PHE E  14      18.842  14.623  12.623  1.00 20.00           C
ATOM    190  C   PHE E  14      20.192  14.548  13.323  1.00 20.00           C
ATOM    191  O   PHE E  14      20.881  13.533  13.245  1.00 20.00           O
ATOM    192  CB  PHE E  14      19.026  15.083  11.174  1.00 20.00           C
ATOM    193  CG  PHE E  14      17.911  14.666  10.258  1.00 20.00           C
ATOM    194  CD1 PHE E  14      17.035  13.657  10.624  1.00 20.00           C
ATOM    195  CD2 PHE E  14      17.739  15.281   9.029  1.00 20.00           C
ATOM    196  CE1 PHE E  14      16.008  13.271   9.783  1.00 20.00           C
ATOM    197  CE2 PHE E  14      16.714  14.900   8.183  1.00 20.00           C
ATOM    198  CZ  PHE E  14      15.848  13.893   8.561  1.00 20.00           C
ATOM    199  H   PHE E  14      17.417  15.971  12.800  1.00 20.00           H
ATOM    200  HA  PHE E  14      18.464  13.730  12.629  1.00 20.00           H
ATOM    201  HB2 PHE E  14      19.102  16.050  11.158  1.00 20.00           H
ATOM    202  HB3 PHE E  14      19.862  14.727  10.834  1.00 20.00           H
ATOM    203  HD1 PHE E  14      17.139  13.234  11.446  1.00 20.00           H
ATOM    204  HD2 PHE E  14      18.320  15.959   8.770  1.00 20.00           H
ATOM    205  HE1 PHE E  14      15.426  12.593  10.040  1.00 20.00           H
ATOM    206  HE2 PHE E  14      16.608  15.321   7.361  1.00 20.00           H
ATOM    207  HZ  PHE E  14      15.158  13.634   7.993  1.00 20.00           H
ATOM    208  N   ALA E  15      20.558  15.630  14.003  1.00 20.00           N
ATOM    209  CA  ALA E  15      21.699  15.619  14.904  1.00 20.00           C
ATOM    210  C   ALA E  15      21.268  15.190  16.307  1.00 20.00           C
ATOM    211  O   ALA E  15      22.035  14.538  17.024  1.00 20.00           O
ATOM    212  CB  ALA E  15      22.357  16.988  14.944  1.00 20.00           C
ATOM    213  H   ALA E  15      20.152  16.387  13.954  1.00 20.00           H
ATOM    214  HA  ALA E  15      22.347  14.977  14.573  1.00 20.00           H
ATOM    215  HB1 ALA E  15      23.115  16.965  15.548  1.00 20.00           H
ATOM    216  HB2 ALA E  15      22.660  17.228  14.054  1.00 20.00           H
ATOM    217  HB3 ALA E  15      21.716  17.646  15.255  1.00 20.00           H
ATOM    218  N   GLU E  16      20.036  15.542  16.687  1.00 20.00           N
ATOM    219  CA  GLU E  16      19.549  15.284  18.047  1.00 20.00           C
ATOM    220  C   GLU E  16      19.685  13.831  18.518  1.00 20.00           C
ATOM    221  O   GLU E  16      20.064  13.664  19.676  1.00 20.00           O
ATOM    222  CB  GLU E  16      18.119  15.792  18.273  1.00 20.00           C
ATOM    223  CG  GLU E  16      18.040  17.145  18.965  1.00 20.00           C
ATOM    224  CD  GLU E  16      16.612  17.612  19.170  1.00 20.00           C
ATOM    225  OE1 GLU E  16      15.682  16.879  18.772  1.00 20.00           O
ATOM    226  OE2 GLU E  16      16.420  18.711  19.730  1.00 20.00           O
ATOM    227  H   GLU E  16      19.467  15.931  16.173  1.00 20.00           H
ATOM    228  HA  GLU E  16      20.151  15.803  18.603  1.00 20.00           H
ATOM    229  HB2 GLU E  16      17.668  15.851  17.416  1.00 20.00           H
ATOM    230  HB3 GLU E  16      17.635  15.140  18.803  1.00 20.00           H
ATOM    231  HG2 GLU E  16      18.486  17.091  19.825  1.00 20.00           H
ATOM    232  HG3 GLU E  16      18.519  17.803  18.437  1.00 20.00           H
TER
END
"""

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  Exercise phenix.real_space_refine with morphing only, H present.
  """
  pdb_file_name_answer = "%s_answer.pdb"%prefix
  of=open(pdb_file_name_answer, "w")
  print(pdb_str_answer, file=of)
  of.close()
  #
  pdb_file_name_poor = "%s_poor.pdb"%prefix
  of=open(pdb_file_name_poor, "w")
  print(pdb_str_poor, file=of)
  of.close()
  #
  cmd = " ".join([
    "phenix.fmodel",
    "%s"%pdb_file_name_answer,
    "file_name=%s.mtz"%prefix,
    "high_res=1",
    ">%s.zlog"%prefix])
  assert easy_run.call(cmd)==0
  #
  args = [
    "%s"%pdb_file_name_poor,
    "%s.mtz"%prefix,
    "run=morphing",
    "rotamers.restraints.enabled=False"]
  r = run_real_space_refine(args = args, prefix = prefix)
  #
  xrs_answer = iotbx.pdb.input(file_name=
    pdb_file_name_answer).xray_structure_simple()
  xrs_poor = iotbx.pdb.input(file_name=
    pdb_file_name_poor).xray_structure_simple()
  xrs_refined = r.xrs
  #
  dap = xrs_answer.distances(xrs_poor)
  dar = xrs_answer.distances(xrs_refined)
  assert flex.mean(dap)> 2.99
  assert flex.max(dap) > 2.99
  assert flex.mean(dar) < 0.1, flex.mean(dar)
  assert flex.max(dar)  < 0.15, flex.max(dar)

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
