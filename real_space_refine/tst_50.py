from __future__ import print_function
import time
from libtbx import easy_run
import iotbx.pdb
from scitbx.array_family import flex
from scitbx.math import superpose
import os
import libtbx.load_env
from phenix_regression.real_space_refine import run_real_space_refine
from libtbx.test_utils import assert_lines_in_file

pdb_str="""
CRYST1   40.410   24.706   31.507  90.00  90.00  90.00 P 1
SCALE1      0.024746  0.000000  0.000000        0.00000
SCALE2      0.000000  0.040476  0.000000        0.00000
SCALE3      0.000000  0.000000  0.031739        0.00000
ATOM      1  N   GLY A   1       5.043   9.706  12.193  1.00 16.77           N
ATOM      2  CA  GLY A   1       5.000   9.301  10.742  1.00 16.57           C
ATOM      3  C   GLY A   1       6.037   8.234  10.510  1.00 16.16           C
ATOM      4  O   GLY A   1       6.529   7.615  11.472  1.00 16.78           O
ATOM      5  N   ASN A   2       6.396   8.017   9.246  1.00 15.02           N
ATOM      6  CA  ASN A   2       7.530   7.132   8.922  1.00 14.10           C
ATOM      7  C   ASN A   2       8.811   7.631   9.518  1.00 13.13           C
ATOM      8  O   ASN A   2       9.074   8.836   9.517  1.00 11.91           O
ATOM      9  CB  ASN A   2       7.706   6.975   7.432  1.00 15.38           C
ATOM     10  CG  ASN A   2       6.468   6.436   6.783  1.00 14.08           C
ATOM     11  OD1 ASN A   2       6.027   5.321   7.107  1.00 17.46           O
ATOM     12  ND2 ASN A   2       5.848   7.249   5.922  1.00 11.72           N
ATOM     13  N   ASN A   3       9.614   6.684   9.996  1.00 12.26           N
ATOM     14  CA  ASN A   3      10.859   6.998  10.680  1.00 11.74           C
ATOM     15  C   ASN A   3      12.097   6.426   9.986  1.00 11.10           C
ATOM     16  O   ASN A   3      12.180   5.213   9.739  1.00 10.42           O
ATOM     17  CB  ASN A   3      10.793   6.472  12.133  1.00 12.15           C
ATOM     18  CG  ASN A   3      12.046   6.833  12.952  1.00 12.82           C
ATOM     19  OD1 ASN A   3      12.350   8.019  13.163  1.00 15.05           O
ATOM     20  ND2 ASN A   3      12.781   5.809  13.397  1.00 13.48           N
ATOM     21  N   GLN A   4      13.047   7.322   9.689  1.00 10.29           N
ATOM     22  CA  GLN A   4      14.436   6.982   9.290  1.00 10.53           C
ATOM     23  C   GLN A   4      15.487   7.700  10.179  1.00 10.24           C
ATOM     24  O   GLN A   4      15.599   8.937  10.206  1.00  8.86           O
ATOM     25  CB  GLN A   4      14.708   7.242   7.802  1.00  9.80           C
ATOM     26  CG  GLN A   4      15.996   6.552   7.304  1.00 10.25           C
ATOM     27  CD  GLN A   4      16.556   7.138   6.002  1.00 12.43           C
ATOM     28  OE1 GLN A   4      16.796   8.362   5.901  1.00 14.62           O
ATOM     29  NE2 GLN A   4      16.802   6.255   5.000  1.00  9.05           N
ATOM     30  N   GLN A   5      16.206   6.915  10.962  1.00 10.38           N
ATOM     31  CA  GLN A   5      17.322   7.455  11.731  1.00 11.39           C
ATOM     32  C   GLN A   5      18.646   6.862  11.263  1.00 11.52           C
ATOM     33  O   GLN A   5      18.820   5.640  11.145  1.00 12.05           O
ATOM     34  CB  GLN A   5      17.108   7.277  13.238  1.00 11.96           C
ATOM     35  CG  GLN A   5      15.881   8.044  13.738  1.00 10.81           C
ATOM     36  CD  GLN A   5      15.396   7.508  15.045  1.00 13.10           C
ATOM     37  OE1 GLN A   5      14.826   6.419  15.093  1.00 10.65           O
ATOM     38  NE2 GLN A   5      15.601   8.281  16.130  1.00 12.30           N
ATOM     39  N   ASN A   6      19.566   7.758  10.947  1.00 11.99           N
ATOM     40  CA  ASN A   6      20.883   7.404  10.409  1.00 12.30           C
ATOM     41  C   ASN A   6      21.906   7.855  11.415  1.00 13.40           C
ATOM     42  O   ASN A   6      22.271   9.037  11.465  1.00 13.92           O
ATOM     43  CB  ASN A   6      21.117   8.110   9.084  1.00 12.13           C
ATOM     44  CG  ASN A   6      20.013   7.829   8.094  1.00 12.77           C
ATOM     45  OD1 ASN A   6      19.850   6.698   7.642  1.00 14.27           O
ATOM     46  ND2 ASN A   6      19.247   8.841   7.770  1.00 10.07           N
ATOM     47  N   TYR A   7      22.344   6.911  12.238  1.00 14.70           N
ATOM     48  CA  TYR A   7      23.211   7.238  13.390  1.00 15.18           C
ATOM     49  C   TYR A   7      24.655   7.425  12.976  1.00 15.91           C
ATOM     50  O   TYR A   7      25.093   6.905  11.946  1.00 15.76           O
ATOM     51  CB  TYR A   7      23.113   6.159  14.460  1.00 15.35           C
ATOM     52  CG  TYR A   7      21.717   6.023  14.993  1.00 14.45           C
ATOM     53  CD1 TYR A   7      21.262   6.850  16.011  1.00 14.80           C
ATOM     54  CD2 TYR A   7      20.823   5.115  14.418  1.00 15.68           C
ATOM     55  CE1 TYR A   7      19.956   6.743  16.507  1.00 14.33           C
ATOM     56  CE2 TYR A   7      19.532   5.000  14.887  1.00 13.46           C
ATOM     57  CZ  TYR A   7      19.099   5.823  15.922  1.00 15.09           C
ATOM     58  OH  TYR A   7      17.818   5.683  16.382  1.00 14.39           O
ATOM     59  OXT TYR A   7      25.410   8.093  13.703  1.00 17.49           O
TER
ATOM     60  N   GLY B   1      15.043  19.706  22.193  1.00 16.77           N
ATOM     61  CA  GLY B   1      15.000  19.301  20.742  1.00 16.57           C
ATOM     62  C   GLY B   1      16.037  18.234  20.510  1.00 16.16           C
ATOM     63  O   GLY B   1      16.529  17.615  21.472  1.00 16.78           O
ATOM     64  N   ASN B   2      16.396  18.017  19.246  1.00 15.02           N
ATOM     65  CA  ASN B   2      17.530  17.132  18.922  1.00 14.10           C
ATOM     66  C   ASN B   2      18.811  17.631  19.518  1.00 13.13           C
ATOM     67  O   ASN B   2      19.074  18.836  19.517  1.00 11.91           O
ATOM     68  CB  ASN B   2      17.706  16.975  17.432  1.00 15.38           C
ATOM     69  CG  ASN B   2      16.468  16.436  16.783  1.00 14.08           C
ATOM     70  OD1 ASN B   2      16.027  15.321  17.107  1.00 17.46           O
ATOM     71  ND2 ASN B   2      15.848  17.249  15.922  1.00 11.72           N
ATOM     72  N   ASN B   3      19.614  16.684  19.996  1.00 12.26           N
ATOM     73  CA  ASN B   3      20.859  16.998  20.680  1.00 11.74           C
ATOM     74  C   ASN B   3      22.097  16.426  19.986  1.00 11.10           C
ATOM     75  O   ASN B   3      22.180  15.213  19.739  1.00 10.42           O
ATOM     76  CB  ASN B   3      20.793  16.472  22.133  1.00 12.15           C
ATOM     77  CG  ASN B   3      22.046  16.833  22.952  1.00 12.82           C
ATOM     78  OD1 ASN B   3      22.350  18.019  23.163  1.00 15.05           O
ATOM     79  ND2 ASN B   3      22.781  15.809  23.397  1.00 13.48           N
ATOM     80  N   GLN B   4      23.047  17.322  19.689  1.00 10.29           N
ATOM     81  CA  GLN B   4      24.436  16.982  19.290  1.00 10.53           C
ATOM     82  C   GLN B   4      25.487  17.700  20.179  1.00 10.24           C
ATOM     83  O   GLN B   4      25.599  18.937  20.206  1.00  8.86           O
ATOM     84  CB  GLN B   4      24.708  17.242  17.802  1.00  9.80           C
ATOM     85  CG  GLN B   4      25.996  16.552  17.304  1.00 10.25           C
ATOM     86  CD  GLN B   4      26.556  17.138  16.002  1.00 12.43           C
ATOM     87  OE1 GLN B   4      26.796  18.362  15.901  1.00 14.62           O
ATOM     88  NE2 GLN B   4      26.802  16.255  15.000  1.00  9.05           N
ATOM     89  N   GLN B   5      26.206  16.915  20.962  1.00 10.38           N
ATOM     90  CA  GLN B   5      27.322  17.455  21.731  1.00 11.39           C
ATOM     91  C   GLN B   5      28.646  16.862  21.263  1.00 11.52           C
ATOM     92  O   GLN B   5      28.820  15.640  21.145  1.00 12.05           O
ATOM     93  CB  GLN B   5      27.108  17.277  23.238  1.00 11.96           C
ATOM     94  CG  GLN B   5      25.881  18.044  23.738  1.00 10.81           C
ATOM     95  CD  GLN B   5      25.396  17.508  25.045  1.00 13.10           C
ATOM     96  OE1 GLN B   5      24.826  16.419  25.093  1.00 10.65           O
ATOM     97  NE2 GLN B   5      25.601  18.281  26.130  1.00 12.30           N
ATOM     98  N   ASN B   6      29.566  17.758  20.947  1.00 11.99           N
ATOM     99  CA  ASN B   6      30.883  17.404  20.409  1.00 12.30           C
ATOM    100  C   ASN B   6      31.906  17.855  21.415  1.00 13.40           C
ATOM    101  O   ASN B   6      32.271  19.037  21.465  1.00 13.92           O
ATOM    102  CB  ASN B   6      31.117  18.110  19.084  1.00 12.13           C
ATOM    103  CG  ASN B   6      30.013  17.829  18.094  1.00 12.77           C
ATOM    104  OD1 ASN B   6      29.850  16.698  17.642  1.00 14.27           O
ATOM    105  ND2 ASN B   6      29.247  18.841  17.770  1.00 10.07           N
ATOM    106  N   TYR B   7      32.344  16.911  22.238  1.00 14.70           N
ATOM    107  CA  TYR B   7      33.211  17.238  23.390  1.00 15.18           C
ATOM    108  C   TYR B   7      34.655  17.425  22.976  1.00 15.91           C
ATOM    109  O   TYR B   7      35.093  16.905  21.946  1.00 15.76           O
ATOM    110  CB  TYR B   7      33.113  16.159  24.460  1.00 15.35           C
ATOM    111  CG  TYR B   7      31.717  16.023  24.993  1.00 14.45           C
ATOM    112  CD1 TYR B   7      31.262  16.850  26.011  1.00 14.80           C
ATOM    113  CD2 TYR B   7      30.823  15.115  24.418  1.00 15.68           C
ATOM    114  CE1 TYR B   7      29.956  16.743  26.507  1.00 14.33           C
ATOM    115  CE2 TYR B   7      29.532  15.000  24.887  1.00 13.46           C
ATOM    116  CZ  TYR B   7      29.099  15.823  25.922  1.00 15.09           C
ATOM    117  OH  TYR B   7      27.818  15.683  26.382  1.00 14.39           O
ATOM    118  OXT TYR B   7      35.410  18.093  23.703  1.00 17.49           O
TER
END
"""


def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  Do not move model if max_iterations=0.
  """
  pdb_file = "%s.pdb"%prefix
  mtz_file = "%s.mtz"%prefix
  with open(pdb_file,"w") as fo:
    fo.write(pdb_str)
  #
  cmd = " ".join([
    "phenix.fmodel",
    pdb_file,
    "file_name=%s"%mtz_file,
    "high_res=6",
    ">%s.zlog"%prefix])
  assert easy_run.call(cmd)==0
  #
  for ncs_c in ["True","False"]:
    args = [pdb_file, mtz_file, "run=minimization_global","max_iterations=0",
      "macro_cycles=10", "ncs_constraints=%s"%ncs_c]
    r = run_real_space_refine(args = args, prefix = prefix)
    #
    s1 = iotbx.pdb.input(file_name=pdb_file).atoms().extract_xyz()
    s2 = iotbx.pdb.input(file_name=r.cif).atoms().extract_xyz()
    d = flex.max(flex.sqrt((s1 - s2).dot()))
    assert d<1.e-4, d

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK time =%8.3f"%(time.time() - t0))
