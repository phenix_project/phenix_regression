from __future__ import print_function
from libtbx import easy_run
import time
import iotbx.pdb
from scitbx.array_family import flex
from libtbx.test_utils import show_diff
import os
from phenix_regression.real_space_refine import run_real_space_refine

pdb_str_answer = """\
CRYST1   15.531   16.590   13.936  90.00  90.00  90.00 P 1
SCALE1      0.064387  0.000000  0.000000        0.00000
SCALE2      0.000000  0.060277  0.000000        0.00000
SCALE3      0.000000  0.000000  0.071757        0.00000
ATOM      1  N   ARG A   1      10.508   5.145   5.931  1.00 10.00           N
ATOM      2  CA  ARG A   1       9.711   5.975   6.827  1.00 10.00           C
ATOM      3  C   ARG A   1       8.841   5.116   7.739  1.00 10.00           C
ATOM      4  O   ARG A   1       8.873   5.262   8.961  1.00 10.00           O
ATOM      5  CB  ARG A   1       8.839   6.945   6.027  1.00 10.00           C
ATOM      6  CG  ARG A   1       8.041   7.913   6.885  1.00 10.00           C
ATOM      7  CD  ARG A   1       7.266   8.900   6.027  1.00 10.00           C
ATOM      8  NE  ARG A   1       6.465   9.815   6.835  1.00 10.00           N
ATOM      9  CZ  ARG A   1       5.706  10.785   6.334  1.00 10.00           C
ATOM     10  NH1 ARG A   1       5.642  10.969   5.023  1.00 10.00           N
ATOM     11  NH2 ARG A   1       5.011  11.571   7.145  1.00 10.00           N
TER
END
"""

pdb_str_poor = """\
CRYST1   15.531   16.590   13.936  90.00  90.00  90.00 P 1
SCALE1      0.064387  0.000000  0.000000        0.00000
SCALE2      0.000000  0.060277  0.000000        0.00000
SCALE3      0.000000  0.000000  0.071757        0.00000
ATOM      1  N   ARG A   1      10.508   5.145   5.931  0.00 10.00           N
ATOM      2  CA  ARG A   1       9.711   5.975   6.827  0.00 10.00           C
ATOM      3  C   ARG A   1       8.841   5.116   7.739  0.00 10.00           C
ATOM      4  O   ARG A   1       8.873   5.262   8.961  0.00 10.00           O
ATOM      5  CB  ARG A   1       8.839   6.945   6.027  0.00 10.00           C
ATOM      6  CG  ARG A   1       8.041   7.913   6.885  0.00 10.00           C
ATOM      7  CD  ARG A   1       7.266   8.900   6.027  0.00 10.00           C
ATOM      8  NE  ARG A   1       6.465   9.815   6.835  0.00 10.00           N
ATOM      9  CZ  ARG A   1       5.706  10.785   6.334  0.00 10.00           C
ATOM     10  NH1 ARG A   1       5.642  10.969   5.023  0.00 10.00           N
ATOM     11  NH2 ARG A   1       5.011  11.571   7.145  0.00 10.00           N
TER
END
"""

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  Deal with inputs having occupancy all zero.
  """
  pdb_file_name_answer = "%s_answer.pdb"%prefix
  of=open(pdb_file_name_answer, "w")
  print(pdb_str_answer, file=of)
  of.close()
  #
  pdb_file_name_poor = "%s_poor.pdb"%prefix
  of=open(pdb_file_name_poor, "w")
  print(pdb_str_poor, file=of)
  of.close()
  #
  cmd = " ".join([
    "phenix.fmodel",
    "%s"%pdb_file_name_answer,
    "file_name=%s.mtz"%prefix,
    "high_res=1.5",
    ">%s.zlog"%prefix])
  assert easy_run.call(cmd)==0
  #
  args = [
    "macro_cycles=1",
    "%s"%pdb_file_name_poor,
    "%s.mtz"%prefix]
  r = run_real_space_refine(args = args, prefix = prefix)
  #
  xrs_answer = iotbx.pdb.input(file_name=
    pdb_file_name_answer).xray_structure_simple()
  xrs_poor = iotbx.pdb.input(file_name=
    pdb_file_name_poor).xray_structure_simple()
  xrs_refined = r.xrs
  #
  dap = xrs_answer.distances(xrs_poor)
  dar = xrs_answer.distances(xrs_refined)
  assert flex.mean(dap) < 0.01, flex.mean(dap)
  assert flex.mean(dar) < 0.055, flex.mean(dar)
  assert flex.mean(xrs_poor.scatterers().extract_occupancies()) < 0.01
  assert flex.mean(xrs_refined.scatterers().extract_occupancies()) > 0.9999

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
