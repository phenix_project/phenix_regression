from __future__ import print_function
import time
from libtbx import easy_run
import iotbx.pdb
from scitbx.array_family import flex
from scitbx.math import superpose
import os
import libtbx.load_env
from phenix_regression.real_space_refine import run_real_space_refine
from libtbx.test_utils import assert_lines_in_file

pdb_str = """
CRYST1                                                 P 1                      
ORIGX1      1.000000  0.000000  0.000000        0.00000                         
ORIGX2      0.000000  1.000000  0.000000        0.00000                         
ORIGX3      0.000000  0.000000  1.000000        0.00000                         
SCALE1      1.000000  0.000000  0.000000        0.00000                         
SCALE2      0.000000  1.000000  0.000000        0.00000                         
SCALE3      0.000000  0.000000  1.000000        0.00000                         
ATOM      1  N   GLY A   1       5.043   9.706  12.649  1.00  9.00           N
ATOM      2  CA  GLY A   1       5.000   9.301  11.198  1.00 11.00           C
ATOM      3  C   GLY A   1       6.037   8.234  10.966  1.00 10.00           C
ATOM      4  O   GLY A   1       6.529   7.615  11.928  1.00  9.00           O
ATOM      5  N   ASN A   2       6.396   8.017   9.702  1.00  9.00           N
ATOM      6  CA  ASN A   2       7.530   7.132   9.378  1.00  9.00           C
ATOM      7  C   ASN A   2       8.811   7.631   9.974  1.00  9.00           C
ATOM      8  O   ASN A   2       9.074   8.836   9.973  1.00  9.00           O
ATOM      9  CB  ASN A   2       7.706   6.975   7.888  1.00 11.00           C
ATOM     10  CG  ASN A   2       6.468   6.436   7.239  1.00 10.00           C
ATOM     11  OD1 ASN A   2       6.027   5.321   7.563  1.00  9.00           O
ATOM     12  ND2 ASN A   2       5.848   7.249   6.378  1.00  9.00           N
ATOM     13  N   ASN A   3       9.614   6.684  10.452  1.00 11.00           N
ATOM     14  CA  ASN A   3      10.859   6.998  11.136  1.00 11.00           C
ATOM     15  C   ASN A   3      12.097   6.426  10.442  1.00 10.00           C
ATOM     16  O   ASN A   3      12.180   5.213  10.195  1.00 11.00           O
ATOM     17  CB  ASN A   3      10.793   6.472  12.589  1.00 10.00           C
ATOM     18  CG  ASN A   3      12.046   6.833  13.408  1.00  9.00           C
ATOM     19  OD1 ASN A   3      12.350   8.019  13.619  1.00 11.00           O
ATOM     20  ND2 ASN A   3      12.781   5.809  13.853  1.00  9.00           N
ATOM     21  N  AGLN A   4      13.047   7.322  10.145  0.10 11.00           N
ATOM     22  CA AGLN A   4      14.436   6.982   9.746  0.10 10.00           C
ATOM     23  C  AGLN A   4      15.487   7.700  10.635  0.10 11.00           C
ATOM     24  O  AGLN A   4      15.599   8.937  10.662  0.10 11.00           O
ATOM     25  CB AGLN A   4      14.708   7.242   8.258  0.10 11.00           C
ATOM     26  CG AGLN A   4      15.996   6.552   7.760  0.10  9.00           C
ATOM     27  CD AGLN A   4      16.556   7.138   6.458  0.10 10.00           C
ATOM     28  OE1AGLN A   4      16.796   8.362   6.357  0.10  9.00           O
ATOM     29  NE2AGLN A   4      16.802   6.255   5.456  0.10 10.00           N
ATOM     30  N  BGLN A   4      13.147   7.322  10.145  0.10 11.00           N
ATOM     31  CA BGLN A   4      14.536   6.982   9.746  0.10 10.00           C
ATOM     32  C  BGLN A   4      15.587   7.700  10.635  0.10 11.00           C
ATOM     33  O  BGLN A   4      15.699   8.937  10.662  0.10 11.00           O
ATOM     34  CB BGLN A   4      14.808   7.242   8.258  0.10 11.00           C
ATOM     35  CG BGLN A   4      14.009   6.298   7.334  0.10  9.00           C
ATOM     36  CD BGLN A   4      14.201   6.577   5.838  0.10 10.00           C
ATOM     37  OE1BGLN A   4      14.536   7.714   5.436  0.10  9.00           O
ATOM     38  NE2BGLN A   4      14.021   5.524   5.000  0.10 10.00           N
ATOM     39  N   GLN A   5      16.206   6.915  11.418  1.00  9.00           N
ATOM     40  CA  GLN A   5      17.322   7.455  12.187  1.00  9.00           C
ATOM     41  C   GLN A   5      18.646   6.862  11.719  1.00  9.00           C
ATOM     42  O   GLN A   5      18.820   5.640  11.601  1.00 11.00           O
ATOM     43  CB  GLN A   5      17.108   7.277  13.694  1.00  9.00           C
ATOM     44  CG  GLN A   5      15.881   8.044  14.194  1.00 11.00           C
ATOM     45  CD  GLN A   5      15.396   7.508  15.501  1.00 11.00           C
ATOM     46  OE1 GLN A   5      14.826   6.419  15.549  1.00 10.00           O
ATOM     47  NE2 GLN A   5      15.601   8.281  16.586  1.00  9.00           N
ATOM     48  N   ASN A   6      19.566   7.758  11.403  1.00 11.00           N
END
"""

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  Cope with bogus CRYST1.
  """
  pdb_file = "%s.pdb"%prefix
  with open(pdb_file,"w") as fo:
    fo.write(pdb_str)
  mtz = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/real_space_refine/data/%s.mtz"%prefix,
    test=os.path.isfile)
  #
  args = [pdb_file, mtz, "macro_cycles=1", "run=none"]
  r = run_real_space_refine(args = args, prefix = prefix)

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK time =%8.3f"%(time.time() - t0))
