from __future__ import print_function
import time, os
from libtbx import easy_run
import iotbx.pdb
import libtbx.load_env
from libtbx.test_utils import approx_equal
from phenix_regression.real_space_refine import run_real_space_refine

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  Check correct mmcif output: ensure particular records appear, such as
  citation, secondary structure, SS bond, crystal info.
  also make sure coordinates are shifted for any combination of pdb/mmcif output
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/real_space_refine/data/3rdy_m.pdb",
    test=os.path.isfile)
  #
  cmd = " ".join([
      "phenix.fmodel",
      "%s"%pdb,
      "high_resolution=5",
      "output.file_name=%s.mtz" % prefix])
  print(cmd)
  assert easy_run.call(cmd)==0
  args = [
    "%s"%pdb,
    "%s.mtz" % prefix,
    "macro_cycles=0"]
  r = run_real_space_refine(args = args, prefix = prefix)
  # mmCIF
  fname = r.cif
  assert os.path.isfile(fname)
  cif_f = open(fname, 'r')
  cif_l = cif_f.readlines()
  cif_f.close()
  for l in [
      "_cell.angle_alpha                 90.000\n",
      "_exptl.method                     'ELECTRON MICROSCOPY'\n",
      "  _software.name\n",
      "  _citation.title\n",
      "  _citation_author.name\n",
      "  _refine_ls_restr.number\n",
      "  _struct_conf.pdbx_PDB_helix_id\n",
      "  _struct_sheet.id\n",
      "  _struct_sheet_range.id\n",
      "  _atom_site.label_atom_id\n",
      ]:
    assert l in cif_l, "%s not in cif file!" % l
  h = iotbx.pdb.input(fname).construct_hierarchy()
  approx_equal(h.atoms()[0].xyz, (18.185, 1.476, 8.239), eps=1e-2)
  # PDB
  fname = r.pdb
  assert os.path.isfile(fname)
  pdb_f = open(fname, 'r')
  pdb_l = pdb_f.readlines()
  pdb_f.close()
  for l in ["CRYST1   62.654   62.654   45.906  90.00  90.00  90.00 P 43 21 2\n",
      "HELIX    1   1 TRP A   10  VAL A   14  5                                   5\n",
      "SHEET    1   A 2 ARG A  33  PRO A  38  0\n",]:
    assert l in pdb_l, "%s not in pdb file!" % l
  h = iotbx.pdb.input(fname).construct_hierarchy()
  approx_equal(h.atoms()[0].xyz, (18.185, 1.476, 8.239), eps=1e-2)

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK time =%8.3f"%(time.time() - t0))
