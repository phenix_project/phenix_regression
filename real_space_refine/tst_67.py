from __future__ import absolute_import, division, print_function
from libtbx import easy_run
import time
import os
from phenix_regression.real_space_refine import run_real_space_refine
from libtbx.test_utils import assert_lines_in_file

cif_str = """\
data_test_1YWF
#
_cell.entry_id           1YWF
_cell.length_a           113.068
_cell.length_b           113.068
_cell.length_c           53.292
_cell.angle_alpha        90.00
_cell.angle_beta         90.00
_cell.angle_gamma        90.00
_cell.Z_PDB              8
_cell.pdbx_unique_axis   ?
_cell.length_a_esd       ?
_cell.length_b_esd       ?
_cell.length_c_esd       ?
_cell.angle_alpha_esd    ?
_cell.angle_beta_esd     ?
_cell.angle_gamma_esd    ?
#
_symmetry.entry_id                         1YWF
_symmetry.space_group_name_H-M             'I 41'
_symmetry.pdbx_full_space_group_name_H-M   ?
_symmetry.cell_setting                     ?
_symmetry.Int_Tables_number                80
_symmetry.space_group_name_Hall            ?
#
_exptl.entry_id          1YWF
_exptl.method            'X-RAY DIFFRACTION'
_exptl.crystals_number   1
#
loop_
_struct_conf.conf_type_id
_struct_conf.id
_struct_conf.pdbx_PDB_helix_id
_struct_conf.beg_label_comp_id
_struct_conf.beg_label_asym_id
_struct_conf.beg_label_seq_id
_struct_conf.pdbx_beg_PDB_ins_code
_struct_conf.end_label_comp_id
_struct_conf.end_label_asym_id
_struct_conf.end_label_seq_id
_struct_conf.pdbx_end_PDB_ins_code
_struct_conf.beg_auth_comp_id
_struct_conf.beg_auth_asym_id
_struct_conf.beg_auth_seq_id
_struct_conf.end_auth_comp_id
_struct_conf.end_auth_asym_id
_struct_conf.end_auth_seq_id
_struct_conf.pdbx_PDB_helix_class
_struct_conf.details
_struct_conf.pdbx_PDB_helix_length
HELX_P HELX_P2  2  ASP longA 57  ? GLY longA 68  ? ASP alongA 37  GLY alongA 48  1 ? 12
#
_struct_conf_type.id          HELX_P
_struct_conf_type.criteria    ?
_struct_conf_type.reference   ?
#
_struct_sheet.id               A
_struct_sheet.type             ?
_struct_sheet.number_strands   5
_struct_sheet.details          ?
#
loop_
_struct_sheet_order.sheet_id
_struct_sheet_order.range_id_1
_struct_sheet_order.range_id_2
_struct_sheet_order.offset
_struct_sheet_order.sense
A 1 2 ? anti-parallel
#
loop_
_struct_sheet_range.sheet_id
_struct_sheet_range.id
_struct_sheet_range.beg_label_comp_id
_struct_sheet_range.beg_label_asym_id
_struct_sheet_range.beg_label_seq_id
_struct_sheet_range.pdbx_beg_PDB_ins_code
_struct_sheet_range.end_label_comp_id
_struct_sheet_range.end_label_asym_id
_struct_sheet_range.end_label_seq_id
_struct_sheet_range.pdbx_end_PDB_ins_code
_struct_sheet_range.beg_auth_comp_id
_struct_sheet_range.beg_auth_asym_id
_struct_sheet_range.beg_auth_seq_id
_struct_sheet_range.end_auth_comp_id
_struct_sheet_range.end_auth_asym_id
_struct_sheet_range.end_auth_seq_id
A 1 ARG longA 33  ? ASP longA 34  ? ARG alongA 13  ASP alongA 14
A 2 LEU longA 47  ? SER longA 50  ? LEU alongA 27  SER alongA 30
#
loop_
_pdbx_struct_sheet_hbond.sheet_id
_pdbx_struct_sheet_hbond.range_id_1
_pdbx_struct_sheet_hbond.range_id_2
_pdbx_struct_sheet_hbond.range_1_label_atom_id
_pdbx_struct_sheet_hbond.range_1_label_comp_id
_pdbx_struct_sheet_hbond.range_1_label_asym_id
_pdbx_struct_sheet_hbond.range_1_label_seq_id
_pdbx_struct_sheet_hbond.range_1_PDB_ins_code
_pdbx_struct_sheet_hbond.range_1_auth_atom_id
_pdbx_struct_sheet_hbond.range_1_auth_comp_id
_pdbx_struct_sheet_hbond.range_1_auth_asym_id
_pdbx_struct_sheet_hbond.range_1_auth_seq_id
_pdbx_struct_sheet_hbond.range_2_label_atom_id
_pdbx_struct_sheet_hbond.range_2_label_comp_id
_pdbx_struct_sheet_hbond.range_2_label_asym_id
_pdbx_struct_sheet_hbond.range_2_label_seq_id
_pdbx_struct_sheet_hbond.range_2_PDB_ins_code
_pdbx_struct_sheet_hbond.range_2_auth_atom_id
_pdbx_struct_sheet_hbond.range_2_auth_comp_id
_pdbx_struct_sheet_hbond.range_2_auth_asym_id
_pdbx_struct_sheet_hbond.range_2_auth_seq_id
A 1 2 N ARG longA 33  ? N ARG alongA 13  O ARG longA 49  ? O ARG alongA 29
#
_atom_sites.entry_id                    1YWF
_atom_sites.fract_transf_matrix[1][1]   0.008844
_atom_sites.fract_transf_matrix[1][2]   0.000000
_atom_sites.fract_transf_matrix[1][3]   0.000000
_atom_sites.fract_transf_matrix[2][1]   0.000000
_atom_sites.fract_transf_matrix[2][2]   0.008844
_atom_sites.fract_transf_matrix[2][3]   0.000000
_atom_sites.fract_transf_matrix[3][1]   0.000000
_atom_sites.fract_transf_matrix[3][2]   0.000000
_atom_sites.fract_transf_matrix[3][3]   0.018765
_atom_sites.fract_transf_vector[1]      0.00000
_atom_sites.fract_transf_vector[2]      0.00000
_atom_sites.fract_transf_vector[3]      0.00000
#
loop_
_atom_type.symbol
C
N
O
P
S
#
loop_
_atom_site.group_PDB
_atom_site.id
_atom_site.type_symbol
_atom_site.label_atom_id
_atom_site.label_alt_id
_atom_site.label_comp_id
_atom_site.label_asym_id
_atom_site.label_entity_id
_atom_site.label_seq_id
_atom_site.pdbx_PDB_ins_code
_atom_site.Cartn_x
_atom_site.Cartn_y
_atom_site.Cartn_z
_atom_site.occupancy
_atom_site.B_iso_or_equiv
_atom_site.pdbx_formal_charge
_atom_site.auth_seq_id
_atom_site.auth_comp_id
_atom_site.auth_asym_id
_atom_site.auth_atom_id
_atom_site.pdbx_PDB_model_num
ATOM   1    N N   . ARG longA 1 24  ? 7.511  46.981 14.258  1.00 46.91 ? 4   ARG alongA N   1
ATOM   2    C CA  . ARG longA 1 24  ? 7.057  47.032 12.837  1.00 47.14 ? 4   ARG alongA CA  1
ATOM   3    C C   . ARG longA 1 24  ? 7.149  48.454 12.261  1.00 44.77 ? 4   ARG alongA C   1
ATOM   4    O O   . ARG longA 1 24  ? 7.798  48.643 11.247  1.00 45.20 ? 4   ARG alongA O   1
ATOM   5    C CB  . ARG longA 1 24  ? 5.613  46.509 12.682  1.00 47.77 ? 4   ARG alongA CB  1
ATOM   6    C CG  . ARG longA 1 24  ? 5.282  45.235 13.472  1.00 52.79 ? 4   ARG alongA CG  1
ATOM   7    C CD  . ARG longA 1 24  ? 4.921  43.958 12.663  1.00 58.16 ? 4   ARG alongA CD  1
ATOM   8    N NE  . ARG longA 1 24  ? 4.877  44.193 11.216  1.00 63.64 ? 4   ARG alongA NE  1
ATOM   9    C CZ  . ARG longA 1 24  ? 5.947  44.408 10.439  1.00 67.41 ? 4   ARG alongA CZ  1
ATOM   10   N NH1 . ARG longA 1 24  ? 7.176  44.420 10.954  1.00 68.79 ? 4   ARG alongA NH1 1
ATOM   11   N NH2 . ARG longA 1 24  ? 5.785  44.627 9.133   1.00 68.95 ? 4   ARG alongA NH2 1
ATOM   12   N N   . GLU longA 1 25  ? 6.489  49.425 12.914  1.00 42.64 ? 5   GLU alongA N   1
ATOM   13   C CA  . GLU longA 1 25  ? 6.285  50.786 12.396  1.00 40.70 ? 5   GLU alongA CA  1
ATOM   14   C C   . GLU longA 1 25  ? 7.010  51.885 13.188  1.00 37.71 ? 5   GLU alongA C   1
ATOM   15   O O   . GLU longA 1 25  ? 7.249  51.776 14.392  1.00 37.47 ? 5   GLU alongA O   1
ATOM   16   C CB  . GLU longA 1 25  ? 4.775  51.134 12.383  1.00 41.69 ? 5   GLU alongA CB  1
ATOM   17   C CG  . GLU longA 1 25  ? 3.932  50.296 11.422  1.00 44.73 ? 5   GLU alongA CG  1
ATOM   18   C CD  . GLU longA 1 25  ? 4.140  50.666 9.954   1.00 49.40 ? 5   GLU alongA CD  1
ATOM   19   O OE1 . GLU longA 1 25  ? 4.507  51.827 9.648   1.00 50.99 ? 5   GLU alongA OE1 1
ATOM   20   O OE2 . GLU longA 1 25  ? 3.938  49.789 9.079   1.00 54.37 ? 5   GLU alongA OE2 1
ATOM   21   N N   . LEU longA 1 26  ? 7.297  52.974 12.490  1.00 34.71 ? 6   LEU alongA N   1
ATOM   22   C CA  . LEU longA 1 26  ? 7.928  54.158 13.057  1.00 32.83 ? 6   LEU alongA CA  1
ATOM   23   C C   . LEU longA 1 26  ? 7.124  55.354 12.548  1.00 32.42 ? 6   LEU alongA C   1
ATOM   24   O O   . LEU longA 1 26  ? 7.421  55.925 11.503  1.00 31.01 ? 6   LEU alongA O   1
ATOM   25   C CB  . LEU longA 1 26  ? 9.391  54.265 12.608  1.00 32.60 ? 6   LEU alongA CB  1
ATOM   26   C CG  . LEU longA 1 26  ? 10.179 55.463 13.111  1.00 30.47 ? 6   LEU alongA CG  1
ATOM   27   C CD1 . LEU longA 1 26  ? 10.149 55.453 14.651  1.00 33.03 ? 6   LEU alongA CD1 1
ATOM   28   C CD2 . LEU longA 1 26  ? 11.634 55.474 12.576  1.00 30.34 ? 6   LEU alongA CD2 1
ATOM   29   N N   . PRO longA 1 27  ? 6.021  55.686 13.228  1.00 31.97 ? 7   PRO alongA N   1
ATOM   30   C CA  . PRO longA 1 27  ? 5.071  56.656 12.662  1.00 31.54 ? 7   PRO alongA CA  1
ATOM   31   C C   . PRO longA 1 27  ? 5.654  58.019 12.286  1.00 29.40 ? 7   PRO alongA C   1
ATOM   32   O O   . PRO longA 1 27  ? 6.359  58.672 13.062  1.00 31.00 ? 7   PRO alongA O   1
ATOM   33   C CB  . PRO longA 1 27  ? 3.963  56.730 13.741  1.00 31.52 ? 7   PRO alongA CB  1
ATOM   34   C CG  . PRO longA 1 27  ? 4.012  55.430 14.379  1.00 32.79 ? 7   PRO alongA CG  1
ATOM   35   C CD  . PRO longA 1 27  ? 5.514  55.095 14.483  1.00 32.60 ? 7   PRO alongA CD  1
ATOM   36   N N   . GLY longA 1 28  ? 5.386  58.417 11.036  1.00 28.81 ? 8   GLY alongA N   1
ATOM   37   C CA  . GLY longA 1 28  ? 5.916  59.617 10.412  1.00 28.14 ? 8   GLY alongA CA  1
ATOM   38   C C   . GLY longA 1 28  ? 7.143  59.366 9.540   1.00 26.94 ? 8   GLY alongA C   1
ATOM   39   O O   . GLY longA 1 28  ? 7.557  60.229 8.751   1.00 26.16 ? 8   GLY alongA O   1
ATOM   40   N N   . ALA longA 1 29  ? 7.683  58.159 9.650   1.00 26.13 ? 9   ALA alongA N   1
ATOM   41   C CA  . ALA longA 1 29  ? 8.750  57.696 8.757   1.00 25.16 ? 9   ALA alongA CA  1
ATOM   42   C C   . ALA longA 1 29  ? 8.313  56.419 8.034   1.00 25.14 ? 9   ALA alongA C   1
ATOM   43   O O   . ALA longA 1 29  ? 7.221  55.876 8.276   1.00 25.57 ? 9   ALA alongA O   1
ATOM   44   C CB  . ALA longA 1 29  ? 10.038 57.470 9.546   1.00 25.41 ? 9   ALA alongA CB  1
ATOM   45   N N   . TRP longA 1 30  ? 9.132  55.968 7.091   1.00 24.79 ? 10  TRP alongA N   1
ATOM   46   C CA  . TRP longA 1 30  ? 8.801  54.772 6.341   1.00 23.84 ? 10  TRP alongA CA  1
ATOM   47   C C   . TRP longA 1 30  ? 10.106 53.977 6.084   1.00 23.68 ? 10  TRP alongA C   1
ATOM   48   O O   . TRP longA 1 30  ? 11.196 54.413 6.461   1.00 21.87 ? 10  TRP alongA O   1
ATOM   49   C CB  . TRP longA 1 30  ? 8.093  55.130 5.023   1.00 23.84 ? 10  TRP alongA CB  1
ATOM   50   C CG  . TRP longA 1 30  ? 8.914  55.811 3.994   1.00 21.18 ? 10  TRP alongA CG  1
ATOM   51   C CD1 . TRP longA 1 30  ? 9.502  55.245 2.912   1.00 23.12 ? 10  TRP alongA CD1 1
ATOM   52   C CD2 . TRP longA 1 30  ? 9.276  57.199 3.966   1.00 23.18 ? 10  TRP alongA CD2 1
ATOM   53   N NE1 . TRP longA 1 30  ? 10.195 56.191 2.193   1.00 23.94 ? 10  TRP alongA NE1 1
ATOM   54   C CE2 . TRP longA 1 30  ? 10.049 57.406 2.829   1.00 24.73 ? 10  TRP alongA CE2 1
ATOM   55   C CE3 . TRP longA 1 30  ? 8.976  58.314 4.790   1.00 25.02 ? 10  TRP alongA CE3 1
ATOM   56   C CZ2 . TRP longA 1 30  ? 10.585 58.649 2.506   1.00 24.98 ? 10  TRP alongA CZ2 1
ATOM   57   C CZ3 . TRP longA 1 30  ? 9.511  59.554 4.437   1.00 24.23 ? 10  TRP alongA CZ3 1
ATOM   58   C CH2 . TRP longA 1 30  ? 10.275 59.704 3.315   1.00 23.25 ? 10  TRP alongA CH2 1
ATOM   59   N N   . ASN longA 1 31  ? 9.963  52.795 5.522   1.00 22.49 ? 11  ASN alongA N   1
ATOM   60   C CA  . ASN longA 1 31  ? 11.136 51.943 5.206   1.00 21.75 ? 11  ASN alongA CA  1
ATOM   61   C C   . ASN longA 1 31  ? 11.981 51.670 6.455   1.00 20.98 ? 11  ASN alongA C   1
ATOM   62   O O   . ASN longA 1 31  ? 13.206 51.602 6.401   1.00 21.93 ? 11  ASN alongA O   1
ATOM   63   C CB  . ASN longA 1 31  ? 11.972 52.553 4.061   1.00 21.22 ? 11  ASN alongA CB  1
ATOM   64   C CG  . ASN longA 1 31  ? 12.899 51.561 3.394   1.00 20.64 ? 11  ASN alongA CG  1
ATOM   65   O OD1 . ASN longA 1 31  ? 12.644 50.355 3.401   1.00 19.60 ? 11  ASN alongA OD1 1
ATOM   66   N ND2 . ASN longA 1 31  ? 13.963 52.067 2.780   1.00 21.26 ? 11  ASN alongA ND2 1
ATOM   67   N N   . PHE longA 1 32  ? 11.309 51.467 7.570   1.00 21.68 ? 12  PHE alongA N   1
ATOM   68   C CA  . PHE longA 1 32  ? 11.948 51.243 8.862   1.00 21.29 ? 12  PHE alongA CA  1
ATOM   69   C C   . PHE longA 1 32  ? 12.048 49.773 9.242   1.00 20.70 ? 12  PHE alongA C   1
ATOM   70   O O   . PHE longA 1 32  ? 11.089 49.016 9.167   1.00 22.11 ? 12  PHE alongA O   1
ATOM   71   C CB  . PHE longA 1 32  ? 11.184 52.004 9.960   1.00 22.35 ? 12  PHE alongA CB  1
ATOM   72   C CG  . PHE longA 1 32  ? 11.502 51.541 11.339  1.00 22.18 ? 12  PHE alongA CG  1
ATOM   73   C CD1 . PHE longA 1 32  ? 12.682 51.926 11.981  1.00 23.77 ? 12  PHE alongA CD1 1
ATOM   74   C CD2 . PHE longA 1 32  ? 10.635 50.684 11.993  1.00 26.59 ? 12  PHE alongA CD2 1
ATOM   75   C CE1 . PHE longA 1 32  ? 12.979 51.476 13.266  1.00 27.16 ? 12  PHE alongA CE1 1
ATOM   76   C CE2 . PHE longA 1 32  ? 10.943 50.211 13.279  1.00 28.73 ? 12  PHE alongA CE2 1
ATOM   77   C CZ  . PHE longA 1 32  ? 12.117 50.615 13.900  1.00 27.68 ? 12  PHE alongA CZ  1
ATOM   78   N N   . ARG longA 1 33  ? 13.212 49.360 9.695   1.00 20.61 ? 13  ARG alongA N   1
ATOM   79   C CA  . ARG longA 1 33  ? 13.382 48.025 10.237  1.00 21.15 ? 13  ARG alongA CA  1
ATOM   80   C C   . ARG longA 1 33  ? 14.617 47.938 11.086  1.00 21.28 ? 13  ARG alongA C   1
ATOM   81   O O   . ARG longA 1 33  ? 15.544 48.703 10.936  1.00 21.58 ? 13  ARG alongA O   1
ATOM   82   C CB  . ARG longA 1 33  ? 13.493 46.957 9.140   1.00 21.46 ? 13  ARG alongA CB  1
ATOM   83   C CG  . ARG longA 1 33  ? 14.538 47.243 8.071   1.00 21.12 ? 13  ARG alongA CG  1
ATOM   84   C CD  . ARG longA 1 33  ? 14.365 46.347 6.849   1.00 20.85 ? 13  ARG alongA CD  1
ATOM   85   N NE  . ARG longA 1 33  ? 15.257 46.627 5.739   1.00 20.56 ? 13  ARG alongA NE  1
ATOM   86   C CZ  . ARG longA 1 33  ? 15.050 47.554 4.796   1.00 18.84 ? 13  ARG alongA CZ  1
ATOM   87   N NH1 . ARG longA 1 33  ? 15.935 47.721 3.856   1.00 20.00 ? 13  ARG alongA NH1 1
ATOM   88   N NH2 . ARG longA 1 33  ? 14.005 48.326 4.841   1.00 21.30 ? 13  ARG alongA NH2 1
ATOM   89   N N   . ASP longA 1 34  ? 14.584 46.992 12.022  1.00 22.41 ? 14  ASP alongA N   1
ATOM   90   C CA  . ASP longA 1 34  ? 15.779 46.502 12.667  1.00 22.53 ? 14  ASP alongA CA  1
ATOM   91   C C   . ASP longA 1 34  ? 16.467 45.565 11.655  1.00 22.55 ? 14  ASP alongA C   1
ATOM   92   O O   . ASP longA 1 34  ? 15.847 44.680 11.057  1.00 22.99 ? 14  ASP alongA O   1
ATOM   93   C CB  . ASP longA 1 34  ? 15.347 45.751 13.910  1.00 24.13 ? 14  ASP alongA CB  1
ATOM   94   C CG  . ASP longA 1 34  ? 16.488 45.299 14.788  1.00 28.19 ? 14  ASP alongA CG  1
ATOM   95   O OD1 . ASP longA 1 34  ? 17.648 45.153 14.351  1.00 23.57 ? 14  ASP alongA OD1 1
ATOM   96   O OD2 . ASP longA 1 34  ? 16.231 45.009 15.975  1.00 31.43 ? 14  ASP alongA OD2 1
ATOM   97   N N   . VAL longA 1 35  ? 17.761 45.694 11.515  1.00 22.86 ? 15  VAL alongA N   1
ATOM   98   C CA  . VAL longA 1 35  ? 18.479 44.862 10.563  1.00 24.05 ? 15  VAL alongA CA  1
ATOM   99   C C   . VAL longA 1 35  ? 18.433 43.426 11.022  1.00 24.50 ? 15  VAL alongA C   1
ATOM   100  O O   . VAL longA 1 35  ? 18.482 42.511 10.224  1.00 22.99 ? 15  VAL alongA O   1
ATOM   101  C CB  . VAL longA 1 35  ? 19.905 45.408 10.297  1.00 24.79 ? 15  VAL alongA CB  1
ATOM   102  C CG1 . VAL longA 1 35  ? 20.638 44.504 9.380   1.00 27.38 ? 15  VAL alongA CG1 1
ATOM   103  C CG2 . VAL longA 1 35  ? 19.790 46.809 9.678   1.00 24.62 ? 15  VAL alongA CG2 1
ATOM   104  N N   . ALA longA 1 36  ? 18.311 43.243 12.339  1.00 24.05 ? 16  ALA alongA N   1
ATOM   105  C CA  . ALA longA 1 36  ? 18.235 41.899 12.944  1.00 25.81 ? 16  ALA alongA CA  1
ATOM   106  C C   . ALA longA 1 36  ? 16.917 41.185 12.596  1.00 27.13 ? 16  ALA alongA C   1
ATOM   107  O O   . ALA longA 1 36  ? 16.831 39.985 12.735  1.00 27.81 ? 16  ALA alongA O   1
ATOM   108  C CB  . ALA longA 1 36  ? 18.382 42.023 14.447  1.00 26.22 ? 16  ALA alongA CB  1
ATOM   109  N N   . ASP longA 1 37  ? 15.907 41.918 12.128  1.00 27.98 ? 17  ASP alongA N   1
ATOM   110  C CA  . ASP longA 1 37  ? 14.682 41.273 11.658  1.00 28.73 ? 17  ASP alongA CA  1
ATOM   111  C C   . ASP longA 1 37  ? 15.004 40.350 10.493  1.00 29.07 ? 17  ASP alongA C   1
ATOM   112  O O   . ASP longA 1 37  ? 14.352 39.344 10.313  1.00 30.53 ? 17  ASP alongA O   1
ATOM   113  C CB  . ASP longA 1 37  ? 13.612 42.270 11.216  1.00 29.29 ? 17  ASP alongA CB  1
ATOM   114  C CG  . ASP longA 1 37  ? 13.134 43.191 12.318  1.00 33.79 ? 17  ASP alongA CG  1
ATOM   115  O OD1 . ASP longA 1 37  ? 13.364 42.935 13.534  1.00 36.75 ? 17  ASP alongA OD1 1
ATOM   116  O OD2 . ASP longA 1 37  ? 12.543 44.276 12.027  1.00 39.54 ? 17  ASP alongA OD2 1
ATOM   117  N N   . THR longA 1 38  ? 16.026 40.674 9.718   1.00 28.45 ? 18  THR alongA N   1
ATOM   118  C CA  . THR longA 1 38  ? 16.424 39.884 8.579   1.00 27.82 ? 18  THR alongA CA  1
ATOM   119  C C   . THR longA 1 38  ? 17.641 39.035 8.906   1.00 28.55 ? 18  THR alongA C   1
ATOM   120  O O   . THR longA 1 38  ? 17.622 37.801 8.712   1.00 28.77 ? 18  THR alongA O   1
ATOM   121  C CB  . THR longA 1 38  ? 16.662 40.804 7.377   1.00 27.83 ? 18  THR alongA CB  1
ATOM   122  O OG1 . THR longA 1 38  ? 15.427 41.485 7.071   1.00 26.22 ? 18  THR alongA OG1 1
ATOM   123  C CG2 . THR longA 1 38  ? 16.991 40.012 6.126   1.00 28.79 ? 18  THR alongA CG2 1
ATOM   124  N N   . ALA longA 1 39  ? 18.691 39.684 9.412   1.00 27.94 ? 19  ALA alongA N   1
ATOM   125  C CA  . ALA longA 1 39  ? 19.966 39.033 9.715   1.00 28.40 ? 19  ALA alongA CA  1
ATOM   126  C C   . ALA longA 1 39  ? 19.838 38.519 11.116  1.00 29.58 ? 19  ALA alongA C   1
ATOM   127  O O   . ALA longA 1 39  ? 20.212 39.211 12.059  1.00 29.94 ? 19  ALA alongA O   1
ATOM   128  C CB  . ALA longA 1 39  ? 21.085 40.016 9.634   1.00 28.29 ? 19  ALA alongA CB  1
ATOM   129  N N   . THR longA 1 40  ? 19.337 37.305 11.238  1.00 30.82 ? 20  THR alongA N   1
ATOM   130  C CA  . THR longA 1 40  ? 18.941 36.786 12.519  1.00 33.20 ? 20  THR alongA CA  1
ATOM   131  C C   . THR longA 1 40  ? 20.125 36.388 13.417  1.00 33.18 ? 20  THR alongA C   1
ATOM   132  O O   . THR longA 1 40  ? 19.927 36.094 14.588  1.00 34.30 ? 20  THR alongA O   1
ATOM   133  C CB  . THR longA 1 40  ? 17.950 35.631 12.357  1.00 33.49 ? 20  THR alongA CB  1
ATOM   134  O OG1 . THR longA 1 40  ? 18.379 34.756 11.312  1.00 38.10 ? 20  THR alongA OG1 1
ATOM   135  C CG2 . THR longA 1 40  ? 16.602 36.169 11.911  1.00 35.29 ? 20  THR alongA CG2 1
ATOM   136  N N   . ALA longA 1 41  ? 21.338 36.419 12.894  1.00 33.33 ? 21  ALA alongA N   1
ATOM   137  C CA  . ALA longA 1 41  ? 22.533 36.242 13.735  1.00 33.67 ? 21  ALA alongA CA  1
ATOM   138  C C   . ALA longA 1 41  ? 22.880 37.499 14.531  1.00 34.19 ? 21  ALA alongA C   1
ATOM   139  O O   . ALA longA 1 41  ? 23.684 37.437 15.478  1.00 34.52 ? 21  ALA alongA O   1
ATOM   140  C CB  . ALA longA 1 41  ? 23.713 35.807 12.896  1.00 33.55 ? 21  ALA alongA CB  1
ATOM   141  N N   . LEU longA 1 42  ? 22.312 38.647 14.151  1.00 32.65 ? 22  LEU alongA N   1
ATOM   142  C CA  . LEU longA 1 42  ? 22.477 39.866 14.928  1.00 32.39 ? 22  LEU alongA CA  1
ATOM   143  C C   . LEU longA 1 42  ? 21.610 39.869 16.173  1.00 31.74 ? 22  LEU alongA C   1
ATOM   144  O O   . LEU longA 1 42  ? 20.489 39.369 16.165  1.00 31.53 ? 22  LEU alongA O   1
ATOM   145  C CB  . LEU longA 1 42  ? 22.101 41.111 14.119  1.00 31.77 ? 22  LEU alongA CB  1
ATOM   146  C CG  . LEU longA 1 42  ? 22.845 41.462 12.825  1.00 34.56 ? 22  LEU alongA CG  1
ATOM   147  C CD1 . LEU longA 1 42  ? 22.334 42.821 12.335  1.00 34.55 ? 22  LEU alongA CD1 1
ATOM   148  C CD2 . LEU longA 1 42  ? 24.312 41.481 13.028  1.00 35.86 ? 22  LEU alongA CD2 1
ATOM   149  N N   . ARG longA 1 43  ? 22.124 40.462 17.231  1.00 31.58 ? 23  ARG alongA N   1
ATOM   150  C CA  . ARG longA 1 43  ? 21.329 40.846 18.373  1.00 32.81 ? 23  ARG alongA CA  1
ATOM   151  C C   . ARG longA 1 43  ? 20.327 41.908 17.930  1.00 32.72 ? 23  ARG alongA C   1
ATOM   152  O O   . ARG longA 1 43  ? 20.713 42.871 17.284  1.00 31.45 ? 23  ARG alongA O   1
ATOM   153  C CB  . ARG longA 1 43  ? 22.189 41.476 19.452  1.00 33.29 ? 23  ARG alongA CB  1
ATOM   154  C CG  . ARG longA 1 43  ? 23.022 40.485 20.263  1.00 38.29 ? 23  ARG alongA CG  1
ATOM   155  C CD  . ARG longA 1 43  ? 23.997 41.198 21.224  1.00 43.01 ? 23  ARG alongA CD  1
ATOM   156  N NE  . ARG longA 1 43  ? 23.270 42.014 22.197  1.00 48.08 ? 23  ARG alongA NE  1
ATOM   157  C CZ  . ARG longA 1 43  ? 23.703 43.150 22.759  1.00 51.77 ? 23  ARG alongA CZ  1
ATOM   158  N NH1 . ARG longA 1 43  ? 24.897 43.684 22.460  1.00 51.92 ? 23  ARG alongA NH1 1
ATOM   159  N NH2 . ARG longA 1 43  ? 22.923 43.771 23.640  1.00 53.88 ? 23  ARG alongA NH2 1
ATOM   160  N N   . PRO longA 1 44  ? 19.065 41.741 18.302  1.00 33.30 ? 24  PRO alongA N   1
ATOM   161  C CA  . PRO longA 1 44  ? 18.061 42.776 18.079  1.00 33.43 ? 24  PRO alongA CA  1
ATOM   162  C C   . PRO longA 1 44  ? 18.325 44.038 18.886  1.00 33.05 ? 24  PRO alongA C   1
ATOM   163  O O   . PRO longA 1 44  ? 18.981 44.006 19.927  1.00 33.47 ? 24  PRO alongA O   1
ATOM   164  C CB  . PRO longA 1 44  ? 16.753 42.107 18.507  1.00 33.79 ? 24  PRO alongA CB  1
ATOM   165  C CG  . PRO longA 1 44  ? 17.043 40.692 18.647  1.00 35.21 ? 24  PRO alongA CG  1
ATOM   166  C CD  . PRO longA 1 44  ? 18.499 40.540 18.945  1.00 33.65 ? 24  PRO alongA CD  1
ATOM   167  N N   . GLY longA 1 45  ? 17.848 45.165 18.370  1.00 32.33 ? 25  GLY alongA N   1
ATOM   168  C CA  . GLY longA 1 45  ? 17.919 46.446 19.045  1.00 31.89 ? 25  GLY alongA CA  1
ATOM   169  C C   . GLY longA 1 45  ? 19.214 47.228 18.951  1.00 32.08 ? 25  GLY alongA C   1
ATOM   170  O O   . GLY longA 1 45  ? 19.419 48.201 19.699  1.00 32.75 ? 25  GLY alongA O   1
ATOM   171  N N   . ARG longA 1 46  ? 20.116 46.808 18.070  1.00 30.09 ? 26  ARG alongA N   1
ATOM   172  C CA  . ARG longA 1 46  ? 21.431 47.446 17.922  1.00 30.48 ? 26  ARG alongA CA  1
ATOM   173  C C   . ARG longA 1 46  ? 21.540 48.359 16.680  1.00 28.67 ? 26  ARG alongA C   1
ATOM   174  O O   . ARG longA 1 46  ? 22.088 49.468 16.758  1.00 29.16 ? 26  ARG alongA O   1
ATOM   175  C CB  . ARG longA 1 46  ? 22.547 46.403 17.864  1.00 30.59 ? 26  ARG alongA CB  1
ATOM   176  C CG  . ARG longA 1 46  ? 22.543 45.313 18.945  1.00 35.33 ? 26  ARG alongA CG  1
ATOM   177  C CD  . ARG longA 1 46  ? 22.180 45.784 20.308  1.00 40.30 ? 26  ARG alongA CD  1
ATOM   178  N NE  . ARG longA 1 46  ? 23.179 46.725 20.737  1.00 44.93 ? 26  ARG alongA NE  1
ATOM   179  C CZ  . ARG longA 1 46  ? 23.130 47.423 21.855  1.00 47.75 ? 26  ARG alongA CZ  1
ATOM   180  N NH1 . ARG longA 1 46  ? 22.100 47.310 22.694  1.00 45.88 ? 26  ARG alongA NH1 1
ATOM   181  N NH2 . ARG longA 1 46  ? 24.125 48.256 22.122  1.00 48.65 ? 26  ARG alongA NH2 1
ATOM   182  N N   . LEU longA 1 47  ? 21.047 47.872 15.539  1.00 26.64 ? 27  LEU alongA N   1
ATOM   183  C CA  . LEU longA 1 47  ? 21.173 48.606 14.299  1.00 24.67 ? 27  LEU alongA CA  1
ATOM   184  C C   . LEU longA 1 47  ? 19.851 48.646 13.555  1.00 22.50 ? 27  LEU alongA C   1
ATOM   185  O O   . LEU longA 1 47  ? 19.363 47.637 13.098  1.00 20.72 ? 27  LEU alongA O   1
ATOM   186  C CB  . LEU longA 1 47  ? 22.205 47.944 13.390  1.00 24.59 ? 27  LEU alongA CB  1
ATOM   187  C CG  . LEU longA 1 47  ? 22.447 48.705 12.070  1.00 26.18 ? 27  LEU alongA CG  1
ATOM   188  C CD1 . LEU longA 1 47  ? 22.821 50.177 12.205  1.00 28.23 ? 27  LEU alongA CD1 1
ATOM   189  C CD2 . LEU longA 1 47  ? 23.476 47.957 11.324  1.00 28.36 ? 27  LEU alongA CD2 1
ATOM   190  N N   . PHE longA 1 48  ? 19.321 49.837 13.459  1.00 21.65 ? 28  PHE alongA N   1
ATOM   191  C CA  . PHE longA 1 48  ? 18.080 50.106 12.807  1.00 21.14 ? 28  PHE alongA CA  1
ATOM   192  C C   . PHE longA 1 48  ? 18.390 50.998 11.603  1.00 19.54 ? 28  PHE alongA C   1
ATOM   193  O O   . PHE longA 1 48  ? 19.449 51.628 11.522  1.00 21.04 ? 28  PHE alongA O   1
ATOM   194  C CB  . PHE longA 1 48  ? 17.115 50.849 13.715  1.00 22.33 ? 28  PHE alongA CB  1
ATOM   195  C CG  . PHE longA 1 48  ? 16.729 50.090 14.988  1.00 24.51 ? 28  PHE alongA CG  1
ATOM   196  C CD1 . PHE longA 1 48  ? 15.580 49.350 15.037  1.00 25.75 ? 28  PHE alongA CD1 1
ATOM   197  C CD2 . PHE longA 1 48  ? 17.516 50.181 16.131  1.00 27.07 ? 28  PHE alongA CD2 1
ATOM   198  C CE1 . PHE longA 1 48  ? 15.203 48.693 16.204  1.00 27.03 ? 28  PHE alongA CE1 1
ATOM   199  C CE2 . PHE longA 1 48  ? 17.118 49.532 17.321  1.00 28.58 ? 28  PHE alongA CE2 1
ATOM   200  C CZ  . PHE longA 1 48  ? 15.980 48.781 17.324  1.00 26.62 ? 28  PHE alongA CZ  1
ATOM   201  N N   . ARG longA 1 49  ? 17.401 51.061 10.730  1.00 20.38 ? 29  ARG alongA N   1
ATOM   202  C CA  . ARG longA 1 49  ? 17.458 51.839 9.509   1.00 18.92 ? 29  ARG alongA CA  1
ATOM   203  C C   . ARG longA 1 49  ? 16.072 52.311 9.112   1.00 19.50 ? 29  ARG alongA C   1
ATOM   204  O O   . ARG longA 1 49  ? 15.081 51.630 9.381   1.00 18.98 ? 29  ARG alongA O   1
ATOM   205  C CB  . ARG longA 1 49  ? 18.053 50.976 8.382   1.00 18.31 ? 29  ARG alongA CB  1
ATOM   206  C CG  . ARG longA 1 49  ? 17.360 49.609 8.130   1.00 18.67 ? 29  ARG alongA CG  1
ATOM   207  C CD  . ARG longA 1 49  ? 17.993 48.792 7.009   1.00 18.80 ? 29  ARG alongA CD  1
ATOM   208  N NE  . ARG longA 1 49  ? 17.919 49.545 5.751   1.00 19.69 ? 29  ARG alongA NE  1
ATOM   209  C CZ  . ARG longA 1 49  ? 18.593 49.238 4.659   1.00 18.68 ? 29  ARG alongA CZ  1
ATOM   210  N NH1 . ARG longA 1 49  ? 19.319 48.157 4.587   1.00 18.27 ? 29  ARG alongA NH1 1
ATOM   211  N NH2 . ARG longA 1 49  ? 18.509 50.025 3.602   1.00 19.30 ? 29  ARG alongA NH2 1
ATOM   212  N N   . SER longA 1 50  ? 16.040 53.447 8.419   1.00 18.99 ? 30  SER alongA N   1
ATOM   213  C CA  . SER longA 1 50  ? 14.786 54.030 7.992   1.00 18.63 ? 30  SER alongA CA  1
ATOM   214  C C   . SER longA 1 50  ? 14.986 55.127 6.973   1.00 18.35 ? 30  SER alongA C   1
ATOM   215  O O   . SER longA 1 50  ? 16.097 55.505 6.601   1.00 18.52 ? 30  SER alongA O   1
ATOM   216  C CB  . SER longA 1 50  ? 14.005 54.576 9.188   1.00 18.02 ? 30  SER alongA CB  1
ATOM   217  O OG  . SER longA 1 50  ? 14.595 55.788 9.620   1.00 21.28 ? 30  SER alongA OG  1
ATOM   218  N N   . SER longA 1 51  ? 13.850 55.649 6.527   1.00 20.12 ? 31  SER alongA N   1
ATOM   219  C CA  . SER longA 1 51  ? 13.777 56.923 5.846   1.00 20.18 ? 31  SER alongA CA  1
ATOM   220  C C   . SER longA 1 51  ? 14.173 58.059 6.785   1.00 20.70 ? 31  SER alongA C   1
ATOM   221  O O   . SER longA 1 51  ? 14.397 57.854 7.962   1.00 21.17 ? 31  SER alongA O   1
ATOM   222  C CB  . SER longA 1 51  ? 12.359 57.191 5.404   1.00 19.82 ? 31  SER alongA CB  1
ATOM   223  O OG  . SER longA 1 51  ? 11.567 57.454 6.525   1.00 23.02 ? 31  SER alongA OG  1
ATOM   224  N N   . GLU longA 1 52  ? 14.189 59.256 6.248   1.00 22.11 ? 32  GLU alongA N   1
ATOM   225  C CA  . GLU longA 1 52  ? 14.327 60.411 7.092   1.00 23.25 ? 32  GLU alongA CA  1
ATOM   226  C C   . GLU longA 1 52  ? 13.234 60.477 8.172   1.00 23.89 ? 32  GLU alongA C   1
ATOM   227  O O   . GLU longA 1 52  ? 12.142 59.891 8.077   1.00 23.70 ? 32  GLU alongA O   1
ATOM   228  C CB  . GLU longA 1 52  ? 14.393 61.687 6.242   1.00 22.80 ? 32  GLU alongA CB  1
ATOM   229  C CG  . GLU longA 1 52  ? 13.055 62.081 5.646   1.00 23.93 ? 32  GLU alongA CG  1
ATOM   230  C CD  . GLU longA 1 52  ? 13.040 63.456 4.986   1.00 26.07 ? 32  GLU alongA CD  1
ATOM   231  O OE1 . GLU longA 1 52  ? 14.090 64.034 4.724   1.00 25.53 ? 32  GLU alongA OE1 1
ATOM   232  O OE2 . GLU longA 1 52  ? 11.917 63.948 4.713   1.00 26.61 ? 32  GLU alongA OE2 1
ATOM   233  N N   . LEU longA 1 53  ? 13.577 61.255 9.187   1.00 24.71 ? 33  LEU alongA N   1
ATOM   234  C CA  . LEU longA 1 53  ? 12.824 61.371 10.405  1.00 25.72 ? 33  LEU alongA CA  1
ATOM   235  C C   . LEU longA 1 53  ? 12.233 62.784 10.547  1.00 27.12 ? 33  LEU alongA C   1
ATOM   236  O O   . LEU longA 1 53  ? 11.744 63.118 11.610  1.00 28.65 ? 33  LEU alongA O   1
ATOM   237  C CB  . LEU longA 1 53  ? 13.743 61.092 11.574  1.00 25.40 ? 33  LEU alongA CB  1
ATOM   238  C CG  . LEU longA 1 53  ? 14.424 59.708 11.631  1.00 26.74 ? 33  LEU alongA CG  1
ATOM   239  C CD1 . LEU longA 1 53  ? 15.233 59.603 12.933  1.00 26.02 ? 33  LEU alongA CD1 1
ATOM   240  C CD2 . LEU longA 1 53  ? 13.437 58.617 11.584  1.00 24.98 ? 33  LEU alongA CD2 1
ATOM   241  N N   . SER longA 1 54  ? 12.238 63.550 9.461   1.00 28.04 ? 34  SER alongA N   1
ATOM   242  C CA  . SER longA 1 54  ? 11.816 64.963 9.494   1.00 28.62 ? 34  SER alongA CA  1
ATOM   243  C C   . SER longA 1 54  ? 10.312 65.161 9.737   1.00 29.69 ? 34  SER alongA C   1
ATOM   244  O O   . SER longA 1 54  ? 9.883  66.307 9.960   1.00 30.38 ? 34  SER alongA O   1
ATOM   245  C CB  . SER longA 1 54  ? 12.220 65.667 8.209   1.00 27.82 ? 34  SER alongA CB  1
ATOM   246  O OG  . SER longA 1 54  ? 13.644 65.671 8.138   1.00 30.07 ? 34  SER alongA OG  1
ATOM   247  N N   . ARG longA 1 55  ? 9.530  64.077 9.711   1.00 29.16 ? 35  ARG alongA N   1
ATOM   248  C CA  . ARG longA 1 55  ? 8.089  64.157 9.959   1.00 29.52 ? 35  ARG alongA CA  1
ATOM   249  C C   . ARG longA 1 55  ? 7.657  63.114 10.970  1.00 30.36 ? 35  ARG alongA C   1
ATOM   250  O O   . ARG longA 1 55  ? 6.482  62.735 11.053  1.00 29.10 ? 35  ARG alongA O   1
ATOM   251  C CB  . ARG longA 1 55  ? 7.314  64.072 8.658   1.00 29.70 ? 35  ARG alongA CB  1
ATOM   252  C CG  . ARG longA 1 55  ? 7.575  65.241 7.715   1.00 28.95 ? 35  ARG alongA CG  1
ATOM   253  C CD  . ARG longA 1 55  ? 7.045  66.636 8.178   1.00 32.14 ? 35  ARG alongA CD  1
ATOM   254  N NE  . ARG longA 1 55  ? 7.247  67.693 7.177   1.00 32.59 ? 35  ARG alongA NE  1
ATOM   255  C CZ  . ARG longA 1 55  ? 8.401  68.327 6.980   1.00 31.73 ? 35  ARG alongA CZ  1
ATOM   256  N NH1 . ARG longA 1 55  ? 9.466  68.046 7.736   1.00 32.13 ? 35  ARG alongA NH1 1
ATOM   257  N NH2 . ARG longA 1 55  ? 8.499  69.290 6.075   1.00 33.77 ? 35  ARG alongA NH2 1
ATOM   258  N N   . LEU longA 1 56  ? 8.623  62.655 11.750  1.00 30.18 ? 36  LEU alongA N   1
ATOM   259  C CA  . LEU longA 1 56  ? 8.314  61.788 12.854  1.00 32.34 ? 36  LEU alongA CA  1
ATOM   260  C C   . LEU longA 1 56  ? 7.243  62.506 13.664  1.00 33.27 ? 36  LEU alongA C   1
ATOM   261  O O   . LEU longA 1 56  ? 7.333  63.729 13.859  1.00 32.39 ? 36  LEU alongA O   1
ATOM   262  C CB  . LEU longA 1 56  ? 9.554  61.594 13.717  1.00 32.54 ? 36  LEU alongA CB  1
ATOM   263  C CG  . LEU longA 1 56  ? 9.679  60.254 14.430  1.00 35.29 ? 36  LEU alongA CG  1
ATOM   264  C CD1 . LEU longA 1 56  ? 9.649  59.058 13.444  1.00 33.08 ? 36  LEU alongA CD1 1
ATOM   265  C CD2 . LEU longA 1 56  ? 10.910 60.175 15.369  1.00 35.73 ? 36  LEU alongA CD2 1
ATOM   266  N N   . ASP longA 1 57  ? 6.265  61.752 14.145  1.00 35.17 ? 37  ASP alongA N   1
ATOM   267  C CA  . ASP longA 1 57  ? 5.251  62.335 15.056  1.00 37.08 ? 37  ASP alongA CA  1
ATOM   268  C C   . ASP longA 1 57  ? 5.433  61.900 16.511  1.00 37.79 ? 37  ASP alongA C   1
ATOM   269  O O   . ASP longA 1 57  ? 6.443  61.316 16.858  1.00 37.54 ? 37  ASP alongA O   1
ATOM   270  C CB  . ASP longA 1 57  ? 3.827  62.120 14.521  1.00 37.53 ? 37  ASP alongA CB  1
ATOM   271  C CG  . ASP longA 1 57  ? 3.427  60.683 14.400  1.00 38.76 ? 37  ASP alongA CG  1
ATOM   272  O OD1 . ASP longA 1 57  ? 4.001  59.819 15.070  1.00 38.84 ? 37  ASP alongA OD1 1
ATOM   273  O OD2 . ASP longA 1 57  ? 2.506  60.327 13.624  1.00 41.78 ? 37  ASP alongA OD2 1
ATOM   274  N N   . ASP longA 1 58  ? 4.467  62.205 17.382  1.00 38.31 ? 38  ASP alongA N   1
ATOM   275  C CA  . ASP longA 1 58  ? 4.609  61.829 18.786  1.00 38.69 ? 38  ASP alongA CA  1
ATOM   276  C C   . ASP longA 1 58  ? 4.781  60.335 18.955  1.00 37.78 ? 38  ASP alongA C   1
ATOM   277  O O   . ASP longA 1 58  ? 5.598  59.886 19.760  1.00 38.31 ? 38  ASP alongA O   1
ATOM   278  C CB  . ASP longA 1 58  ? 3.376  62.258 19.608  1.00 39.51 ? 38  ASP alongA CB  1
ATOM   279  C CG  . ASP longA 1 58  ? 3.378  63.724 19.972  1.00 42.76 ? 38  ASP alongA CG  1
ATOM   280  O OD1 . ASP longA 1 58  ? 4.462  64.343 20.161  1.00 48.07 ? 38  ASP alongA OD1 1
ATOM   281  O OD2 . ASP longA 1 58  ? 2.295  64.337 20.144  1.00 47.65 ? 38  ASP alongA OD2 1
ATOM   282  N N   . ALA longA 1 59  ? 4.003  59.561 18.209  1.00 36.68 ? 39  ALA alongA N   1
ATOM   283  C CA  . ALA longA 1 59  ? 4.065  58.107 18.287  1.00 36.58 ? 39  ALA alongA CA  1
ATOM   284  C C   . ALA longA 1 59  ? 5.433  57.607 17.773  1.00 35.91 ? 39  ALA alongA C   1
ATOM   285  O O   . ALA longA 1 59  ? 6.014  56.661 18.319  1.00 35.28 ? 39  ALA alongA O   1
ATOM   286  C CB  . ALA longA 1 59  ? 2.947  57.491 17.483  1.00 36.33 ? 39  ALA alongA CB  1
ATOM   287  N N   . GLY longA 1 60  ? 5.948  58.257 16.745  1.00 35.33 ? 40  GLY alongA N   1
ATOM   288  C CA  . GLY longA 1 60  ? 7.296  57.938 16.267  1.00 35.20 ? 40  GLY alongA CA  1
ATOM   289  C C   . GLY longA 1 60  ? 8.386  58.218 17.295  1.00 34.81 ? 40  GLY alongA C   1
ATOM   290  O O   . GLY longA 1 60  ? 9.320  57.432 17.456  1.00 34.92 ? 40  GLY alongA O   1
ATOM   291  N N   . ARG longA 1 61  ? 8.297  59.351 17.981  1.00 35.65 ? 41  ARG alongA N   1
ATOM   292  C CA  . ARG longA 1 61  ? 9.300  59.698 18.970  1.00 35.75 ? 41  ARG alongA CA  1
ATOM   293  C C   . ARG longA 1 61  ? 9.257  58.681 20.093  1.00 37.10 ? 41  ARG alongA C   1
ATOM   294  O O   . ARG longA 1 61  ? 10.295 58.291 20.642  1.00 37.65 ? 41  ARG alongA O   1
ATOM   295  C CB  . ARG longA 1 61  ? 9.090  61.118 19.494  1.00 36.15 ? 41  ARG alongA CB  1
ATOM   296  C CG  . ARG longA 1 61  ? 9.575  62.196 18.563  1.00 35.51 ? 41  ARG alongA CG  1
ATOM   297  C CD  . ARG longA 1 61  ? 9.383  63.592 19.134  1.00 38.98 ? 41  ARG alongA CD  1
ATOM   298  N NE  . ARG longA 1 61  ? 7.999  64.012 18.913  1.00 40.46 ? 41  ARG alongA NE  1
ATOM   299  C CZ  . ARG longA 1 61  ? 7.537  64.446 17.753  1.00 41.44 ? 41  ARG alongA CZ  1
ATOM   300  N NH1 . ARG longA 1 61  ? 8.326  64.534 16.682  1.00 42.62 ? 41  ARG alongA NH1 1
ATOM   301  N NH2 . ARG longA 1 61  ? 6.261  64.776 17.649  1.00 43.05 ? 41  ARG alongA NH2 1
ATOM   302  N N   . ALA longA 1 62  ? 8.053  58.238 20.441  1.00 38.18 ? 42  ALA alongA N   1
ATOM   303  C CA  . ALA longA 1 62  ? 7.878  57.270 21.524  1.00 38.42 ? 42  ALA alongA CA  1
ATOM   304  C C   . ALA longA 1 62  ? 8.398  55.909 21.116  1.00 38.32 ? 42  ALA alongA C   1
ATOM   305  O O   . ALA longA 1 62  ? 8.952  55.181 21.927  1.00 37.15 ? 42  ALA alongA O   1
ATOM   306  C CB  . ALA longA 1 62  ? 6.387  57.158 21.948  1.00 38.91 ? 42  ALA alongA CB  1
ATOM   307  N N   . THR longA 1 63  ? 8.209  55.567 19.842  1.00 37.57 ? 43  THR alongA N   1
ATOM   308  C CA  . THR longA 1 63  ? 8.756  54.324 19.328  1.00 37.21 ? 43  THR alongA CA  1
ATOM   309  C C   . THR longA 1 63  ? 10.284 54.321 19.472  1.00 36.33 ? 43  THR alongA C   1
ATOM   310  O O   . THR longA 1 63  ? 10.842 53.315 19.824  1.00 36.44 ? 43  THR alongA O   1
ATOM   311  C CB  . THR longA 1 63  ? 8.316  54.130 17.873  1.00 37.54 ? 43  THR alongA CB  1
ATOM   312  O OG1 . THR longA 1 63  ? 6.890  53.948 17.829  1.00 38.41 ? 43  THR alongA OG1 1
ATOM   313  C CG2 . THR longA 1 63  ? 8.897  52.837 17.280  1.00 36.05 ? 43  THR alongA CG2 1
ATOM   314  N N   . LEU longA 1 64  ? 10.948 55.436 19.192  1.00 36.74 ? 44  LEU alongA N   1
ATOM   315  C CA  . LEU longA 1 64  ? 12.410 55.504 19.283  1.00 36.66 ? 44  LEU alongA CA  1
ATOM   316  C C   . LEU longA 1 64  ? 12.877 55.316 20.729  1.00 37.24 ? 44  LEU alongA C   1
ATOM   317  O O   . LEU longA 1 64  ? 13.840 54.613 20.978  1.00 36.26 ? 44  LEU alongA O   1
ATOM   318  C CB  . LEU longA 1 64  ? 12.957 56.819 18.725  1.00 36.22 ? 44  LEU alongA CB  1
ATOM   319  C CG  . LEU longA 1 64  ? 12.786 57.061 17.209  1.00 34.97 ? 44  LEU alongA CG  1
ATOM   320  C CD1 . LEU longA 1 64  ? 13.386 58.400 16.795  1.00 33.89 ? 44  LEU alongA CD1 1
ATOM   321  C CD2 . LEU longA 1 64  ? 13.399 55.928 16.404  1.00 33.15 ? 44  LEU alongA CD2 1
ATOM   322  N N   . ARG longA 1 65  ? 12.147 55.914 21.675  1.00 38.31 ? 45  ARG alongA N   1
ATOM   323  C CA  . ARG longA 1 65  ? 12.485 55.801 23.095  1.00 39.49 ? 45  ARG alongA CA  1
ATOM   324  C C   . ARG longA 1 65  ? 12.296 54.381 23.589  1.00 39.97 ? 45  ARG alongA C   1
ATOM   325  O O   . ARG longA 1 65  ? 13.113 53.864 24.338  1.00 40.63 ? 45  ARG alongA O   1
ATOM   326  C CB  . ARG longA 1 65  ? 11.614 56.757 23.935  1.00 39.94 ? 45  ARG alongA CB  1
ATOM   327  N N   . ARG longA 1 66  ? 11.186 53.775 23.179  1.00 41.00 ? 46  ARG alongA N   1
ATOM   328  C CA  . ARG longA 1 66  ? 10.849 52.397 23.503  1.00 41.33 ? 46  ARG alongA CA  1
ATOM   329  C C   . ARG longA 1 66  ? 11.912 51.412 23.025  1.00 40.34 ? 46  ARG alongA C   1
ATOM   330  O O   . ARG longA 1 66  ? 12.278 50.485 23.731  1.00 39.81 ? 46  ARG alongA O   1
ATOM   331  C CB  . ARG longA 1 66  ? 9.524  52.063 22.835  1.00 41.72 ? 46  ARG alongA CB  1
ATOM   332  C CG  . ARG longA 1 66  ? 8.773  50.911 23.395  1.00 46.36 ? 46  ARG alongA CG  1
ATOM   333  C CD  . ARG longA 1 66  ? 7.352  50.836 22.851  1.00 51.59 ? 46  ARG alongA CD  1
ATOM   334  N NE  . ARG longA 1 66  ? 7.345  50.162 21.548  1.00 57.79 ? 46  ARG alongA NE  1
ATOM   335  C CZ  . ARG longA 1 66  ? 6.851  50.659 20.399  1.00 61.01 ? 46  ARG alongA CZ  1
ATOM   336  N NH1 . ARG longA 1 66  ? 6.282  51.872 20.344  1.00 62.67 ? 46  ARG alongA NH1 1
ATOM   337  N NH2 . ARG longA 1 66  ? 6.918  49.916 19.290  1.00 61.73 ? 46  ARG alongA NH2 1
ATOM   338  N N   . LEU longA 1 67  ? 12.402 51.620 21.809  1.00 39.47 ? 47  LEU alongA N   1
ATOM   339  C CA  . LEU longA 1 67  ? 13.439 50.765 21.223  1.00 38.25 ? 47  LEU alongA CA  1
ATOM   340  C C   . LEU longA 1 67  ? 14.826 51.006 21.800  1.00 37.39 ? 47  LEU alongA C   1
ATOM   341  O O   . LEU longA 1 67  ? 15.742 50.247 21.530  1.00 38.19 ? 47  LEU alongA O   1
ATOM   342  C CB  . LEU longA 1 67  ? 13.502 51.010 19.712  1.00 38.57 ? 47  LEU alongA CB  1
ATOM   343  C CG  . LEU longA 1 67  ? 12.264 50.556 18.951  1.00 38.58 ? 47  LEU alongA CG  1
ATOM   344  C CD1 . LEU longA 1 67  ? 12.346 51.046 17.517  1.00 38.92 ? 47  LEU alongA CD1 1
ATOM   345  C CD2 . LEU longA 1 67  ? 12.101 49.038 19.050  1.00 38.51 ? 47  LEU alongA CD2 1
ATOM   346  N N   . GLY longA 1 68  ? 14.997 52.083 22.557  1.00 36.96 ? 48  GLY alongA N   1
ATOM   347  C CA  . GLY longA 1 68  ? 16.262 52.383 23.191  1.00 35.72 ? 48  GLY alongA CA  1
ATOM   348  C C   . GLY longA 1 68  ? 17.323 52.969 22.286  1.00 34.43 ? 48  GLY alongA C   1
ATOM   349  O O   . GLY longA 1 68  ? 18.512 52.912 22.607  1.00 34.93 ? 48  GLY alongA O   1
ATOM   350  N N   . ILE longA 1 69  ? 16.910 53.540 21.149  1.00 33.30 ? 49  ILE alongA N   1
ATOM   351  C CA  . ILE longA 1 69  ? 17.849 54.176 20.252  1.00 31.75 ? 49  ILE alongA CA  1
ATOM   352  C C   . ILE longA 1 69  ? 18.393 55.461 20.882  1.00 31.44 ? 49  ILE alongA C   1
ATOM   353  O O   . ILE longA 1 69  ? 17.614 56.332 21.269  1.00 31.61 ? 49  ILE alongA O   1
ATOM   354  C CB  . ILE longA 1 69  ? 17.158 54.429 18.875  1.00 30.66 ? 49  ILE alongA CB  1
ATOM   355  C CG1 . ILE longA 1 69  ? 16.852 53.073 18.242  1.00 28.65 ? 49  ILE alongA CG1 1
ATOM   356  C CG2 . ILE longA 1 69  ? 17.997 55.327 18.021  1.00 31.50 ? 49  ILE alongA CG2 1
ATOM   357  C CD1 . ILE longA 1 69  ? 15.809 53.159 17.127  1.00 31.05 ? 49  ILE alongA CD1 1
ATOM   358  N N   . THR longA 1 70  ? 19.713 55.583 20.950  1.00 31.26 ? 50  THR alongA N   1
ATOM   359  C CA  . THR longA 1 70  ? 20.380 56.742 21.535  1.00 31.21 ? 50  THR alongA CA  1
ATOM   360  C C   . THR longA 1 70  ? 21.064 57.625 20.530  1.00 30.07 ? 50  THR alongA C   1
ATOM   361  O O   . THR longA 1 70  ? 21.440 58.769 20.840  1.00 30.33 ? 50  THR alongA O   1
ATOM   362  C CB  . THR longA 1 70  ? 21.414 56.290 22.600  1.00 31.56 ? 50  THR alongA CB  1
ATOM   363  O OG1 . THR longA 1 70  ? 22.370 55.375 22.056  1.00 33.21 ? 50  THR alongA OG1 1
ATOM   364  C CG2 . THR longA 1 70  ? 20.716 55.533 23.751  1.00 34.53 ? 50  THR alongA CG2 1
#
"""

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  Exercise long chain ids with secondary structure annotations and restraints
  enabled.
  """
  #
  model_fname = "%s.cif"%prefix
  with open(model_fname,"w") as fo:
    fo.write(cif_str)
  #
  mtz_file = "%s.mtz"%prefix
  cmd = " ".join([
    "phenix.fmodel",
    model_fname,
    "file_name=%s"%mtz_file,
    "high_res=3.0",
    ">%s.zlog"%prefix])
  assert easy_run.call(cmd)==0
  #
  args = [
    "run=minimization_global",
    "resolution=3",
    "macro_cycles=1",
    "secondary_structure.enabled=True",
    "%s.cif"%prefix,
    mtz_file]
  # Sorry is not expected here, but looks like infrastructure cannot
  # handle cases where pdb file is not produced because the model
  # does not fit into it.
  r = run_real_space_refine(
      args = args,
      prefix = prefix,
      sorry_expected = True)
  for f in [r.log, r.eff, r.std_out, r.geo_ini, r.cif]:
    assert os.path.isfile(f)

  assert_lines_in_file(r.log, """
  Finding SS restraints...
    Secondary structure from input PDB file:
      1 helices and 1 sheets defined
      25.5% alpha, 8.5% beta
      0 base pairs and 0 stacking pairs defined.
""")

  assert_lines_in_file(r.cif, """
  _struct_conf.pdbx_PDB_helix_length
   HELX_P 1 2 ASP alongA 37 ? GLY alongA 48 ? 1 ? 12""")

  assert_lines_in_file(r.cif, """
  _pdbx_struct_sheet_hbond.range_2_PDB_ins_code
   A 1 2 N ARG alongA 13 ? O ARG alongA 29 ?""")

  assert_lines_in_file(r.cif, """  _atom_site.pdbx_PDB_model_num
   ATOM 1 N . ARG alongA 4 ?""")

  assert_lines_in_file(r.cif, """
  _pdbx_struct_sheet_hbond.range_2_PDB_ins_code
   A 1 2 N ARG alongA 13 ? O ARG alongA 29 ?""")

  assert_lines_in_file(r.geo_ini, """Bond | Bond-like | restraints: 10
Sorted by residual:
bond pdb=" O   LEUalongA  44 "
     pdb=" N   GLYalongA  48 "
""")

  assert_lines_in_file(r.geo_ini, """Bond angle | Secondary Structure restraints around h-bond | restraints: 27
Sorted by residual:
angle pdb=" C   THRalongA  43 "
      pdb=" N   LEUalongA  44 "
      pdb=" O   GLYalongA  40 "
""")


if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
