from __future__ import print_function
import time
from libtbx import easy_run
import iotbx.pdb
from scitbx.array_family import flex
from scitbx.math import superpose
import os
import libtbx.load_env
from phenix_regression.real_space_refine import run_real_space_refine
from libtbx.test_utils import assert_lines_in_file
from libtbx.test_utils import approx_equal, not_approx_equal

pdb_str = """
CRYST1   17.373   15.976   15.622  90.00  90.00  90.00 P 1
ATOM      1  N   ASN B   1       5.467   5.569   7.949  1.00340.49           N
ATOM      2  CA  ASN B   1       6.887   5.856   7.601  1.00340.49           C
ATOM      3  C   ASN B   1       7.559   6.640   8.722  1.00340.49           C
ATOM      4  O   ASN B   1       8.348   6.094   9.491  1.00340.49           O
ATOM      5  CB  ASN B   1       6.971   6.631   6.282  1.00340.49           C
ATOM      6  CG  ASN B   1       8.400   6.929   5.868  1.00340.49           C
ATOM      7  OD1 ASN B   1       9.303   6.124   6.091  1.00340.49           O
ATOM      8  ND2 ASN B   1       8.609   8.087   5.256  1.00340.49           N
ATOM      9  HA  ASN B   1       7.364   5.018   7.492  1.00340.49           H
ATOM     10  HB1 ASN B   1       6.562   6.100   5.581  1.00340.49           H
ATOM     11  HB2 ASN B   1       6.501   7.474   6.375  1.00340.49           H
ATOM     12 HD21 ASN B   1       9.402   8.300   5.000  1.00340.49           H
ATOM     13 HD22 ASN B   1       7.953   8.624   5.115  1.00340.49           H
ATOM     14  H 1 ASN B   1       5.124   5.000   7.356  1.00340.49           H
ATOM     15  H 2 ASN B   1       5.428   5.209   8.762  1.00340.49           H
ATOM     16  H 3 ASN B   1       5.000   6.326   7.938  1.00340.49           H
ATOM     17  N   PHE B   2       7.240   7.926   8.805  1.00318.09           N
ATOM     18  CA  PHE B   2       7.782   8.813   9.820  1.00318.09           C
ATOM     19  C   PHE B   2       6.638   9.420  10.622  1.00318.09           C
ATOM     20  O   PHE B   2       5.471   9.326  10.234  1.00318.09           O
ATOM     21  CB  PHE B   2       8.617   9.916   9.177  1.00318.09           C
ATOM     22  CG  PHE B   2       9.703   9.407   8.271  1.00318.09           C
ATOM     23  CD1 PHE B   2      10.302   8.185   8.499  1.00318.09           C
ATOM     24  CD2 PHE B   2      10.118  10.152   7.184  1.00318.09           C
ATOM     25  CE1 PHE B   2      11.297   7.719   7.668  1.00318.09           C
ATOM     26  CE2 PHE B   2      11.113   9.692   6.349  1.00318.09           C
ATOM     27  CZ  PHE B   2      11.702   8.474   6.590  1.00318.09           C
ATOM     28  H   PHE B   2       6.692   8.317   8.270  1.00318.09           H
ATOM     29  HA  PHE B   2       8.346   8.311  10.429  1.00318.09           H
ATOM     30  HB1 PHE B   2       8.024  10.468   8.645  1.00318.09           H
ATOM     31  HB2 PHE B   2       9.031  10.449   9.875  1.00318.09           H
ATOM     32  HD1 PHE B   2      10.035   7.670   9.226  1.00318.09           H
ATOM     33  HD2 PHE B   2       9.723  10.976   7.016  1.00318.09           H
ATOM     34  HE1 PHE B   2      11.693   6.895   7.835  1.00318.09           H
ATOM     35  HE2 PHE B   2      11.383  10.204   5.621  1.00318.09           H
ATOM     36  HZ  PHE B   2      12.373   8.160   6.028  1.00318.09           H
TER
END
"""

pdb_str_poor = """
ATOM      1  N   ASN B   1       7.236   8.824   7.384  1.00188.20           N
ATOM      2  CA  ASN B   1       8.334   7.896   7.756  1.00191.45           C
ATOM      3  C   ASN B   1       9.169   8.509   8.782  1.00202.35           C
ATOM      4  O   ASN B   1      10.016   7.886   9.388  1.00204.00           O
ATOM      5  CB  ASN B   1       9.133   7.518   6.557  1.00185.23           C
ATOM      6  CG  ASN B   1       8.343   6.636   5.720  1.00181.69           C
ATOM      7  OD1 ASN B   1       7.429   5.986   6.226  1.00181.51           O
ATOM      8  ND2 ASN B   1       8.602   6.627   4.443  1.00178.37           N
ATOM      9  HA  ASN B   1       7.924   7.104   8.120  1.00185.61           H
ATOM     10  HB1 ASN B   1       9.356   8.307   6.034  1.00181.96           H
ATOM     11  HB2 ASN B   1       9.962   7.075   6.815  1.00182.34           H
ATOM     12 HD21 ASN B   1       8.156   6.124   3.907  1.00177.17           H
ATOM     13 HD22 ASN B   1       9.236   7.121   4.152  1.00177.84           H
ATOM     14  H 1 ASN B   1       6.805   8.563   6.648  1.00183.31           H
ATOM     15  H 2 ASN B   1       6.665   8.873   8.066  1.00183.80           H
ATOM     16  H 3 ASN B   1       7.579   9.627   7.233  1.00184.06           H
ATOM     17  N   PHE B   2       8.919   9.792   8.922  1.00216.16           N
ATOM     18  CA  PHE B   2       8.846  10.347  10.235  1.00225.38           C
ATOM     19  C   PHE B   2       7.394  10.752  10.181  1.00227.34           C
ATOM     20  O   PHE B   2       6.582   9.955   9.685  1.00223.62           O
ATOM     21  CB  PHE B   2       9.859  11.475  10.485  1.00231.70           C
ATOM     22  CG  PHE B   2       9.747  12.113  11.852  1.00236.64           C
ATOM     23  CD1 PHE B   2      10.289  11.439  12.931  1.00239.19           C
ATOM     24  CD2 PHE B   2       9.330  13.428  12.045  1.00239.51           C
ATOM     25  CE1 PHE B   2      10.259  11.958  14.204  1.00240.94           C
ATOM     26  CE2 PHE B   2       9.330  13.974  13.342  1.00240.97           C
ATOM     27  CZ  PHE B   2       9.803  13.218  14.419  1.00241.57           C
ATOM     28  H   PHE B   2       8.742  10.334   8.275  1.00214.04           H
ATOM     29  HA  PHE B   2       8.943   9.663  10.908  1.00225.13           H
ATOM     30  HB1 PHE B   2       9.673  12.184   9.841  1.00232.40           H
ATOM     31  HB2 PHE B   2      10.754  11.118  10.389  1.00232.82           H
ATOM     32  HD1 PHE B   2      10.570  10.567  12.802  1.00238.11           H
ATOM     33  HD2 PHE B   2       9.001  13.945  11.340  1.00240.43           H
ATOM     34  HE1 PHE B   2      10.575  11.457  14.910  1.00241.81           H
ATOM     35  HE2 PHE B   2       9.007  14.841  13.503  1.00241.77           H
ATOM     36  HZ  PHE B   2       9.794  13.587  15.280  1.00241.92           H
"""

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  Make sure presence of H 1, H 2 and H 3 does not crash the program.
  """
  # 
  pdb_file = "%s.pdb"%prefix
  with open(pdb_file,"w") as fo:
    fo.write(pdb_str)
  #
  mtz_file = "%s.mtz"%prefix
  cmd = " ".join([
    "phenix.fmodel",
    "%s"%pdb_file,
    "file_name=%s"%mtz_file,
    "high_res=2.0",
    ">%s.zlog"%prefix])
  assert easy_run.call(cmd)==0
  # poor
  pdb_file_poor = "%s_poor.pdb"%prefix
  with open(pdb_file_poor,"w") as fo:
    fo.write(pdb_str_poor)
  #
  args = [pdb_file_poor, mtz_file]
  r = run_real_space_refine(args = args, prefix = prefix)
  #

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK time =%8.3f"%(time.time() - t0))
