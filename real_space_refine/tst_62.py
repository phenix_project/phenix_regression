from __future__ import print_function
import time
from libtbx import easy_run
import iotbx.pdb
from scitbx.array_family import flex
from scitbx.math import superpose
import os
import libtbx.load_env
from phenix_regression.real_space_refine import run_real_space_refine
from libtbx.test_utils import assert_lines_in_file
from libtbx.test_utils import approx_equal, not_approx_equal

pdb_str_good = """
CRYST1  129.259  176.262  199.763  90.00  90.00  90.00 P1            1
ATOM      1  N   PRO A  34     193.434 123.309 166.953  1.00  0.00           N
ATOM      2  HN1 PRO A  34     192.978 123.273 166.052  1.00  0.00           H
ATOM      3  HN2 PRO A  34     192.690 123.401 167.630  1.00  0.00           H
ATOM      4  CD  PRO A  34     194.209 122.095 167.310  1.00  0.00           C
ATOM      5  HD1 PRO A  34     194.167 121.907 168.383  1.00  0.00           H
ATOM      6  HD2 PRO A  34     193.790 121.262 166.747  1.00  0.00           H
ATOM      7  CG  PRO A  34     195.681 122.235 167.042  1.00  0.00           C
ATOM      8  HG1 PRO A  34     196.115 121.956 168.002  1.00  0.00           H
ATOM      9  HG2 PRO A  34     195.950 121.623 166.182  1.00  0.00           H
ATOM     10  CB  PRO A  34     195.844 123.701 166.645  1.00  0.00           C
ATOM     11  HB1 PRO A  34     196.620 124.140 167.272  1.00  0.00           H
ATOM     12  HB2 PRO A  34     196.106 123.739 165.588  1.00  0.00           H
ATOM     13  CA  PRO A  34     194.474 124.377 167.025  1.00  0.00           C
ATOM     14  HA  PRO A  34     194.438 124.831 168.016  1.00  0.00           H
ATOM     15  C   PRO A  34     194.182 125.628 166.169  1.00  0.00           C
ATOM     16  O   PRO A  34     194.413 125.469 164.956  1.00  0.00           O
END
"""

pdb_str_poor = """
CRYST1  129.259  176.262  199.763  90.00  90.00  90.00               1
ATOM      1  N   PRO A  34     193.434 123.309 166.953  1.00  0.00           N
ATOM      2  HN1 PRO A  34     192.978 123.273 166.052  1.00  0.00           H
ATOM      3  HN2 PRO A  34     192.690 123.401 167.630  1.00  0.00           H
ATOM      4  CD  PRO A  34     194.209 122.095 167.310  1.00  0.00           C
ATOM      5  HD1 PRO A  34     194.167 121.907 168.383  1.00  0.00           H
ATOM      6  HD2 PRO A  34     193.790 121.262 166.747  1.00  0.00           H
ATOM      7  CG  PRO A  34     195.681 122.235 167.042  1.00  0.00           C
ATOM      8  HG1 PRO A  34     196.115 121.956 168.002  1.00  0.00           H
ATOM      9  HG2 PRO A  34     195.950 121.623 166.182  1.00  0.00           H
ATOM     10  CB  PRO A  34     195.844 123.701 166.645  1.00  0.00           C
ATOM     11  HB1 PRO A  34     196.620 124.140 167.272  1.00  0.00           H
ATOM     12  HB2 PRO A  34     196.106 123.739 165.588  1.00  0.00           H
ATOM     13  CA  PRO A  34     194.474 124.377 167.025  1.00  0.00           C
ATOM     14  HA  PRO A  34     194.438 124.831 168.016  1.00  0.00           H
ATOM     15  C   PRO A  34     194.182 125.628 166.169  1.00  0.00           C
ATOM     16  O   PRO A  34     194.413 125.469 164.956  1.00  0.00           O
END
"""

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  Handle this kind of incomplete CRYST1
  """
  #
  pdb_file_good = "%s_good.pdb"%prefix
  with open(pdb_file_good,"w") as fo:
    fo.write(pdb_str_good)
  #
  pdb_file_poor = "%s_poor.pdb"%prefix
  with open(pdb_file_poor,"w") as fo:
    fo.write(pdb_str_poor)
  #
  mtz_file = "%s.mtz"%prefix
  cmd = " ".join([
    "phenix.fmodel",
    "%s"%pdb_file_good,
    "file_name=%s"%mtz_file,
    "high_res=5.0",
    ">%s.zlog"%prefix])
  assert easy_run.call(cmd)==0
  #
  args = [pdb_file_poor, mtz_file, "macro_cycles=1",
          "skip_map_model_overlap_check=True"]
  r = run_real_space_refine(args = args, prefix = prefix)
  #

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK time =%8.3f"%(time.time() - t0))
