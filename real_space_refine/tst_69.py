from __future__ import division
from __future__ import print_function
import iotbx.pdb
from libtbx import easy_run
from cctbx import miller
from scitbx.array_family import flex
from cctbx import adptbx
from libtbx.test_utils import approx_equal
import os
from phenix_regression.real_space_refine import run_real_space_refine

pdb_answer_str = """\n
CRYST1   40.408   25.281   31.173  90.00  90.00  90.00 P 1
ATOM      1  N   GLY A   1       5.043   9.706  12.193  1.00 10.00           N
ATOM      2  CA  GLY A   1       5.000   9.301  10.742  1.00 10.00           C
ATOM      3  C   GLY A   1       6.037   8.234  10.510  1.00 10.00           C
ATOM      4  O   GLY A   1       6.529   7.615  11.472  1.00 10.00           O
ATOM      5  N   ASN A   2       6.396   8.017   9.246  1.00 10.00           N
ATOM      6  CA  ASN A   2       7.530   7.132   8.922  1.00 10.00           C
ATOM      7  C   ASN A   2       8.811   7.631   9.518  1.00 10.00           C
ATOM      8  O   ASN A   2       9.074   8.836   9.517  1.00 10.00           O
ATOM      9  CB  ASN A   2       7.706   6.975   7.432  1.00 10.00           C
ATOM     10  CG  ASN A   2       6.468   6.436   6.783  1.00 10.00           C
ATOM     11  OD1 ASN A   2       6.027   5.321   7.107  1.00 10.00           O
ATOM     12  ND2 ASN A   2       5.848   7.249   5.922  1.00 10.00           N
ATOM     13  N   ASN A   3       9.614   6.684   9.996  1.00 10.00           N
ATOM     14  CA  ASN A   3      10.859   6.998  10.680  1.00 10.00           C
ATOM     15  C   ASN A   3      12.097   6.426   9.986  1.00 10.00           C
ATOM     16  O   ASN A   3      12.180   5.213   9.739  1.00 10.00           O
ATOM     17  CB  ASN A   3      10.793   6.472  12.133  1.00 10.00           C
ATOM     18  CG  ASN A   3      12.046   6.833  12.952  1.00 10.00           C
ATOM     19  OD1 ASN A   3      12.350   8.019  13.163  1.00 10.00           O
ATOM     20  ND2 ASN A   3      12.781   5.809  13.397  1.00 10.00           N
ATOM     21  N   GLN A   4      13.047   7.322   9.689  1.00 10.00           N
ATOM     22  CA  GLN A   4      14.436   6.982   9.290  1.00 10.00           C
ATOM     23  C   GLN A   4      15.487   7.700  10.179  1.00 10.00           C
ATOM     24  O   GLN A   4      15.599   8.937  10.206  1.00 10.00           O
ATOM     25  CB  GLN A   4      14.708   7.242   7.802  1.00 10.00           C
ATOM     26  CG  GLN A   4      15.996   6.552   7.304  1.00 10.00           C
ATOM     27  CD  GLN A   4      16.556   7.138   6.002  1.00 10.00           C
ATOM     28  OE1 GLN A   4      16.796   8.362   5.901  1.00 10.00           O
ATOM     29  NE2 GLN A   4      16.802   6.255   5.000  1.00 10.00           N
ATOM     30  N   GLN A   5      16.206   6.915  10.962  1.00 10.00           N
ATOM     31  CA  GLN A   5      17.322   7.455  11.731  1.00 10.00           C
ATOM     32  C   GLN A   5      18.646   6.862  11.263  1.00 10.00           C
ATOM     33  O   GLN A   5      18.820   5.640  11.145  1.00 10.00           O
ATOM     34  CB  GLN A   5      17.108   7.277  13.238  1.00 10.00           C
ATOM     35  CG  GLN A   5      15.881   8.044  13.738  1.00 10.00           C
ATOM     36  CD  GLN A   5      15.396   7.508  15.045  1.00 10.00           C
ATOM     37  OE1 GLN A   5      14.826   6.419  15.093  1.00 10.00           O
ATOM     38  NE2 GLN A   5      15.601   8.281  16.130  1.00 10.00           N
ATOM     39  N   ASN A   6      19.566   7.758  10.947  1.00 10.00           N
ATOM     40  CA  ASN A   6      20.883   7.404  10.409  1.00 10.00           C
ATOM     41  C   ASN A   6      21.906   7.855  11.415  1.00 10.00           C
ATOM     42  O   ASN A   6      22.271   9.037  11.465  1.00 10.00           O
ATOM     43  CB  ASN A   6      21.117   8.110   9.084  1.00 10.00           C
ATOM     44  CG  ASN A   6      20.013   7.829   8.094  1.00 10.00           C
ATOM     45  OD1 ASN A   6      19.850   6.698   7.642  1.00 10.00           O
ATOM     46  ND2 ASN A   6      19.247   8.841   7.770  1.00 10.00           N
ATOM     47  N   TYR A   7      22.344   6.911  12.238  1.00 10.00           N
ATOM     48  CA  TYR A   7      23.211   7.238  13.390  1.00 10.00           C
ATOM     49  C   TYR A   7      24.655   7.425  12.976  1.00 10.00           C
ATOM     50  O   TYR A   7      25.093   6.905  11.946  1.00 10.00           O
ATOM     51  CB  TYR A   7      23.113   6.159  14.460  1.00 10.00           C
ATOM     52  CG  TYR A   7      21.717   6.023  14.993  1.00 10.00           C
ATOM     53  CD1 TYR A   7      20.823   5.115  14.418  1.00 10.00           C
ATOM     54  CD2 TYR A   7      21.262   6.850  16.011  1.00 10.00           C
ATOM     55  CE1 TYR A   7      19.532   5.000  14.887  1.00 10.00           C
ATOM     56  CE2 TYR A   7      19.956   6.743  16.507  1.00 10.00           C
ATOM     57  CZ  TYR A   7      19.099   5.823  15.922  1.00 10.00           C
ATOM     58  OH  TYR A   7      17.818   5.683  16.382  1.00 10.00           O
ATOM     59  OXT TYR A   7      25.410   8.093  13.703  1.00 10.00           O
TER
ATOM     60  N   GLY B   1      15.117  17.344  24.100  1.00 20.00           N
ATOM     61  CA  GLY B   1      14.893  17.193  22.618  1.00 20.00           C
ATOM     62  C   GLY B   1      16.041  16.412  22.035  1.00 20.00           C
ATOM     63  O   GLY B   1      16.791  15.749  22.776  1.00 20.00           O
ATOM     64  N   ASN B   2      16.207  16.491  20.716  1.00 20.00           N
ATOM     65  CA  ASN B   2      17.402  15.920  20.067  1.00 20.00           C
ATOM     66  C   ASN B   2      18.662  16.557  20.568  1.00 20.00           C
ATOM     67  O   ASN B   2      18.711  17.772  20.773  1.00 20.00           O
ATOM     68  CB  ASN B   2      17.340  16.059  18.566  1.00 20.00           C
ATOM     69  CG  ASN B   2      16.119  15.401  18.000  1.00 20.00           C
ATOM     70  OD1 ASN B   2      15.938  14.182  18.151  1.00 20.00           O
ATOM     71  ND2 ASN B   2      15.229  16.208  17.414  1.00 20.00           N
ATOM     72  N   ASN B   3      19.686  15.722  20.731  1.00 20.00           N
ATOM     73  CA  ASN B   3      20.958  16.158  21.285  1.00 20.00           C
ATOM     74  C   ASN B   3      22.136  15.974  20.326  1.00 20.00           C
ATOM     75  O   ASN B   3      22.381  14.862  19.832  1.00 20.00           O
ATOM     76  CB  ASN B   3      21.237  15.389  22.598  1.00 20.00           C
ATOM     77  CG  ASN B   3      22.532  15.849  23.291  1.00 20.00           C
ATOM     78  OD1 ASN B   3      22.661  17.018  23.692  1.00 20.00           O
ATOM     79  ND2 ASN B   3      23.498  14.932  23.416  1.00 20.00           N
ATOM     80  N   GLN B   4      22.853  17.079  20.087  1.00 20.00           N
ATOM     81  CA  GLN B   4      24.189  17.098  19.439  1.00 20.00           C
ATOM     82  C   GLN B   4      25.240  17.850  20.300  1.00 20.00           C
ATOM     83  O   GLN B   4      25.142  19.061  20.559  1.00 20.00           O
ATOM     84  CB  GLN B   4      24.150  17.658  18.011  1.00 20.00           C
ATOM     85  CG  GLN B   4      25.431  17.336  17.211  1.00 20.00           C
ATOM     86  CD  GLN B   4      25.647  18.237  15.989  1.00 20.00           C
ATOM     87  OE1 GLN B   4      25.653  19.483  16.103  1.00 20.00           O
ATOM     88  NE2 GLN B   4      25.863  17.606  14.806  1.00 20.00           N
ATOM     89  N   GLN B   5      26.207  17.103  20.803  1.00 20.00           N
ATOM     90  CA  GLN B   5      27.331  17.717  21.503  1.00 20.00           C
ATOM     91  C   GLN B   5      28.635  17.490  20.747  1.00 20.00           C
ATOM     92  O   GLN B   5      28.992  16.367  20.363  1.00 20.00           O
ATOM     93  CB  GLN B   5      27.415  17.244  22.958  1.00 20.00           C
ATOM     94  CG  GLN B   5      26.181  17.652  23.767  1.00 20.00           C
ATOM     95  CD  GLN B   5      26.029  16.814  24.994  1.00 20.00           C
ATOM     96  OE1 GLN B   5      25.671  15.641  24.900  1.00 20.00           O
ATOM     97  NE2 GLN B   5      26.284  17.416  26.173  1.00 20.00           N
ATOM     98  N   ASN B   6      29.319  18.593  20.493  1.00 20.00           N
ATOM     99  CA  ASN B   6      30.564  18.608  19.718  1.00 20.00           C
ATOM    100  C   ASN B   6      31.653  19.076  20.643  1.00 20.00           C
ATOM    101  O   ASN B   6      31.814  20.281  20.878  1.00 20.00           O
ATOM    102  CB  ASN B   6      30.440  19.563  18.542  1.00 20.00           C
ATOM    103  CG  ASN B   6      29.245  19.239  17.678  1.00 20.00           C
ATOM    104  OD1 ASN B   6      29.202  18.193  17.036  1.00 20.00           O
ATOM    105  ND2 ASN B   6      28.273  20.117  17.673  1.00 20.00           N
ATOM    106  N   TYR B   7      32.383  18.113  21.191  1.00 20.00           N
ATOM    107  CA  TYR B   7      33.367  18.405  22.254  1.00 20.00           C
ATOM    108  C   TYR B   7      34.664  18.946  21.690  1.00 20.00           C
ATOM    109  O   TYR B   7      34.999  18.709  20.526  1.00 20.00           O
ATOM    110  CB  TYR B   7      33.643  17.162  23.089  1.00 20.00           C
ATOM    111  CG  TYR B   7      32.405  16.659  23.771  1.00 20.00           C
ATOM    112  CD1 TYR B   7      31.593  15.703  23.155  1.00 20.00           C
ATOM    113  CD2 TYR B   7      31.999  17.192  24.987  1.00 20.00           C
ATOM    114  CE1 TYR B   7      30.442  15.252  23.765  1.00 20.00           C
ATOM    115  CE2 TYR B   7      30.837  16.742  25.628  1.00 20.00           C
ATOM    116  CZ  TYR B   7      30.061  15.782  24.994  1.00 20.00           C
ATOM    117  OH  TYR B   7      28.923  15.311  25.589  1.00 20.00           O
ATOM    118  OXT TYR B   7      35.408  19.618  22.425  1.00 20.00           O
TER
END
"""

pdb_poor_adp_str = """\n
CRYST1   40.408   25.281   31.173  90.00  90.00  90.00 P 1
ATOM      1  N   GLY A   1       5.043   9.706  12.193  1.00  1.00           N
ATOM      2  CA  GLY A   1       5.000   9.301  10.742  1.00  1.00           C
ATOM      3  C   GLY A   1       6.037   8.234  10.510  1.00  1.00           C
ATOM      4  O   GLY A   1       6.529   7.615  11.472  1.00  1.00           O
ATOM      5  N   ASN A   2       6.396   8.017   9.246  1.00  1.00           N
ATOM      6  CA  ASN A   2       7.530   7.132   8.922  1.00  1.00           C
ATOM      7  C   ASN A   2       8.811   7.631   9.518  1.00  1.00           C
ATOM      8  O   ASN A   2       9.074   8.836   9.517  1.00  1.00           O
ATOM      9  CB  ASN A   2       7.706   6.975   7.432  1.00  1.00           C
ATOM     10  CG  ASN A   2       6.468   6.436   6.783  1.00  1.00           C
ATOM     11  OD1 ASN A   2       6.027   5.321   7.107  1.00  1.00           O
ATOM     12  ND2 ASN A   2       5.848   7.249   5.922  1.00  1.00           N
ATOM     13  N   ASN A   3       9.614   6.684   9.996  1.00  1.00           N
ATOM     14  CA  ASN A   3      10.859   6.998  10.680  1.00  1.00           C
ATOM     15  C   ASN A   3      12.097   6.426   9.986  1.00  1.00           C
ATOM     16  O   ASN A   3      12.180   5.213   9.739  1.00  1.00           O
ATOM     17  CB  ASN A   3      10.793   6.472  12.133  1.00  1.00           C
ATOM     18  CG  ASN A   3      12.046   6.833  12.952  1.00  1.00           C
ATOM     19  OD1 ASN A   3      12.350   8.019  13.163  1.00  1.00           O
ATOM     20  ND2 ASN A   3      12.781   5.809  13.397  1.00  1.00           N
ATOM     21  N   GLN A   4      13.047   7.322   9.689  1.00  1.00           N
ATOM     22  CA  GLN A   4      14.436   6.982   9.290  1.00  1.00           C
ATOM     23  C   GLN A   4      15.487   7.700  10.179  1.00  1.00           C
ATOM     24  O   GLN A   4      15.599   8.937  10.206  1.00  1.00           O
ATOM     25  CB  GLN A   4      14.708   7.242   7.802  1.00  1.00           C
ATOM     26  CG  GLN A   4      15.996   6.552   7.304  1.00  1.00           C
ATOM     27  CD  GLN A   4      16.556   7.138   6.002  1.00  1.00           C
ATOM     28  OE1 GLN A   4      16.796   8.362   5.901  1.00  1.00           O
ATOM     29  NE2 GLN A   4      16.802   6.255   5.000  1.00  1.00           N
ATOM     30  N   GLN A   5      16.206   6.915  10.962  1.00  1.00           N
ATOM     31  CA  GLN A   5      17.322   7.455  11.731  1.00  1.00           C
ATOM     32  C   GLN A   5      18.646   6.862  11.263  1.00  1.00           C
ATOM     33  O   GLN A   5      18.820   5.640  11.145  1.00  1.00           O
ATOM     34  CB  GLN A   5      17.108   7.277  13.238  1.00  1.00           C
ATOM     35  CG  GLN A   5      15.881   8.044  13.738  1.00  1.00           C
ATOM     36  CD  GLN A   5      15.396   7.508  15.045  1.00  1.00           C
ATOM     37  OE1 GLN A   5      14.826   6.419  15.093  1.00  1.00           O
ATOM     38  NE2 GLN A   5      15.601   8.281  16.130  1.00  1.00           N
ATOM     39  N   ASN A   6      19.566   7.758  10.947  1.00  1.00           N
ATOM     40  CA  ASN A   6      20.883   7.404  10.409  1.00  1.00           C
ATOM     41  C   ASN A   6      21.906   7.855  11.415  1.00  1.00           C
ATOM     42  O   ASN A   6      22.271   9.037  11.465  1.00  1.00           O
ATOM     43  CB  ASN A   6      21.117   8.110   9.084  1.00  1.00           C
ATOM     44  CG  ASN A   6      20.013   7.829   8.094  1.00  1.00           C
ATOM     45  OD1 ASN A   6      19.850   6.698   7.642  1.00  1.00           O
ATOM     46  ND2 ASN A   6      19.247   8.841   7.770  1.00  1.00           N
ATOM     47  N   TYR A   7      22.344   6.911  12.238  1.00  1.00           N
ATOM     48  CA  TYR A   7      23.211   7.238  13.390  1.00  1.00           C
ATOM     49  C   TYR A   7      24.655   7.425  12.976  1.00  1.00           C
ATOM     50  O   TYR A   7      25.093   6.905  11.946  1.00  1.00           O
ATOM     51  CB  TYR A   7      23.113   6.159  14.460  1.00  1.00           C
ATOM     52  CG  TYR A   7      21.717   6.023  14.993  1.00  1.00           C
ATOM     53  CD1 TYR A   7      20.823   5.115  14.418  1.00  1.00           C
ATOM     54  CD2 TYR A   7      21.262   6.850  16.011  1.00  1.00           C
ATOM     55  CE1 TYR A   7      19.532   5.000  14.887  1.00  1.00           C
ATOM     56  CE2 TYR A   7      19.956   6.743  16.507  1.00  1.00           C
ATOM     57  CZ  TYR A   7      19.099   5.823  15.922  1.00  1.00           C
ATOM     58  OH  TYR A   7      17.818   5.683  16.382  1.00  1.00           O
ATOM     59  OXT TYR A   7      25.410   8.093  13.703  1.00  1.00           O
TER
ATOM     60  N   GLY B   1      15.117  17.344  24.100  1.00  2.00           N
ATOM     61  CA  GLY B   1      14.893  17.193  22.618  1.00  2.00           C
ATOM     62  C   GLY B   1      16.041  16.412  22.035  1.00  2.00           C
ATOM     63  O   GLY B   1      16.791  15.749  22.776  1.00  2.00           O
ATOM     64  N   ASN B   2      16.207  16.491  20.716  1.00  2.00           N
ATOM     65  CA  ASN B   2      17.402  15.920  20.067  1.00  2.00           C
ATOM     66  C   ASN B   2      18.662  16.557  20.568  1.00  2.00           C
ATOM     67  O   ASN B   2      18.711  17.772  20.773  1.00  2.00           O
ATOM     68  CB  ASN B   2      17.340  16.059  18.566  1.00  2.00           C
ATOM     69  CG  ASN B   2      16.119  15.401  18.000  1.00  2.00           C
ATOM     70  OD1 ASN B   2      15.938  14.182  18.151  1.00  2.00           O
ATOM     71  ND2 ASN B   2      15.229  16.208  17.414  1.00  2.00           N
ATOM     72  N   ASN B   3      19.686  15.722  20.731  1.00  2.00           N
ATOM     73  CA  ASN B   3      20.958  16.158  21.285  1.00  2.00           C
ATOM     74  C   ASN B   3      22.136  15.974  20.326  1.00  2.00           C
ATOM     75  O   ASN B   3      22.381  14.862  19.832  1.00  2.00           O
ATOM     76  CB  ASN B   3      21.237  15.389  22.598  1.00  2.00           C
ATOM     77  CG  ASN B   3      22.532  15.849  23.291  1.00  2.00           C
ATOM     78  OD1 ASN B   3      22.661  17.018  23.692  1.00  2.00           O
ATOM     79  ND2 ASN B   3      23.498  14.932  23.416  1.00  2.00           N
ATOM     80  N   GLN B   4      22.853  17.079  20.087  1.00  2.00           N
ATOM     81  CA  GLN B   4      24.189  17.098  19.439  1.00  2.00           C
ATOM     82  C   GLN B   4      25.240  17.850  20.300  1.00  2.00           C
ATOM     83  O   GLN B   4      25.142  19.061  20.559  1.00  2.00           O
ATOM     84  CB  GLN B   4      24.150  17.658  18.011  1.00  2.00           C
ATOM     85  CG  GLN B   4      25.431  17.336  17.211  1.00  2.00           C
ATOM     86  CD  GLN B   4      25.647  18.237  15.989  1.00  2.00           C
ATOM     87  OE1 GLN B   4      25.653  19.483  16.103  1.00  2.00           O
ATOM     88  NE2 GLN B   4      25.863  17.606  14.806  1.00  2.00           N
ATOM     89  N   GLN B   5      26.207  17.103  20.803  1.00  2.00           N
ATOM     90  CA  GLN B   5      27.331  17.717  21.503  1.00  2.00           C
ATOM     91  C   GLN B   5      28.635  17.490  20.747  1.00  2.00           C
ATOM     92  O   GLN B   5      28.992  16.367  20.363  1.00  2.00           O
ATOM     93  CB  GLN B   5      27.415  17.244  22.958  1.00  2.00           C
ATOM     94  CG  GLN B   5      26.181  17.652  23.767  1.00  2.00           C
ATOM     95  CD  GLN B   5      26.029  16.814  24.994  1.00  2.00           C
ATOM     96  OE1 GLN B   5      25.671  15.641  24.900  1.00  2.00           O
ATOM     97  NE2 GLN B   5      26.284  17.416  26.173  1.00  2.00           N
ATOM     98  N   ASN B   6      29.319  18.593  20.493  1.00  2.00           N
ATOM     99  CA  ASN B   6      30.564  18.608  19.718  1.00  2.00           C
ATOM    100  C   ASN B   6      31.653  19.076  20.643  1.00  2.00           C
ATOM    101  O   ASN B   6      31.814  20.281  20.878  1.00  2.00           O
ATOM    102  CB  ASN B   6      30.440  19.563  18.542  1.00  2.00           C
ATOM    103  CG  ASN B   6      29.245  19.239  17.678  1.00  2.00           C
ATOM    104  OD1 ASN B   6      29.202  18.193  17.036  1.00  2.00           O
ATOM    105  ND2 ASN B   6      28.273  20.117  17.673  1.00  2.00           N
ATOM    106  N   TYR B   7      32.383  18.113  21.191  1.00  2.00           N
ATOM    107  CA  TYR B   7      33.367  18.405  22.254  1.00  2.00           C
ATOM    108  C   TYR B   7      34.664  18.946  21.690  1.00  2.00           C
ATOM    109  O   TYR B   7      34.999  18.709  20.526  1.00  2.00           O
ATOM    110  CB  TYR B   7      33.643  17.162  23.089  1.00  2.00           C
ATOM    111  CG  TYR B   7      32.405  16.659  23.771  1.00  2.00           C
ATOM    112  CD1 TYR B   7      31.593  15.703  23.155  1.00  2.00           C
ATOM    113  CD2 TYR B   7      31.999  17.192  24.987  1.00  2.00           C
ATOM    114  CE1 TYR B   7      30.442  15.252  23.765  1.00  2.00           C
ATOM    115  CE2 TYR B   7      30.837  16.742  25.628  1.00  2.00           C
ATOM    116  CZ  TYR B   7      30.061  15.782  24.994  1.00  2.00           C
ATOM    117  OH  TYR B   7      28.923  15.311  25.589  1.00  2.00           O
ATOM    118  OXT TYR B   7      35.408  19.618  22.425  1.00  2.00           O
TER
END
"""

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  Group ADP refinement: one B per chain.
  """
  pdb_inp_poor = iotbx.pdb.input(source_info=None, lines=pdb_poor_adp_str)
  pdb_inp_poor.write_pdb_file(file_name="%s_poor.pdb"%prefix)
  ph = pdb_inp_poor.construct_hierarchy()
  ph.atoms().reset_i_seq()
  xrs_poor = pdb_inp_poor.xray_structure_simple()
  xrs_answer = iotbx.pdb.input(source_info=None,
    lines=pdb_answer_str).xray_structure_simple()
  ####
  f_obs = xrs_answer.structure_factors(d_min=1.5,algorithm="fft").f_calc()
  mtz_dataset = f_obs.as_mtz_dataset(column_root_label="F-calc")
  mtz_dataset.add_miller_array(
    miller_array=abs(f_obs),
    column_root_label="F-obs")
  mtz_object = mtz_dataset.mtz_object()
  mtz_object.write(file_name = "%s.mtz"%prefix)
  ###
  args = [
    "run=adp",
    "adp.individual.isotropic=False adp.group.mode=one_per_chain",
    "%s_poor.pdb"%prefix,
    "%s.mtz"%prefix]
  r = run_real_space_refine(args = args, prefix = prefix)
  # check results
  xrs_refined = r.xrs
  b_isos = list(set(xrs_refined.extract_u_iso_or_u_equiv()*adptbx.u_as_b(1)))
  print(b_isos)
  assert len(b_isos) == 2
  assert abs(10-b_isos[0])<3.
  assert abs(20-b_isos[1])<3.

if (__name__ == "__main__"):
  run()
