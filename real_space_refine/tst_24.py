from __future__ import division
from __future__ import print_function
from libtbx import easy_run
import time, math, sys, os
import iotbx.pdb
from scitbx.array_family import flex
from libtbx.test_utils import approx_equal
from cctbx import maptbx
from cctbx import miller
from libtbx import group_args
from phenix.refinement import macro_cycle_real_space
from phenix.command_line import real_space_refine
import mmtbx.model
from phenix_regression.real_space_refine import run_real_space_refine
from phenix.refinement.rsr import parameters
from iotbx import map_manager, map_model_manager

pdb_str="""
HELIX    1   1 GLY      1  GLY      9  1                                   9
CRYST1   14.819   24.824   14.332  90.00  90.00  90.00 P 1
ATOM     83  N   GLY     1       6.356   5.527   9.092  1.00  9.50           N
ATOM     84  CA  GLY     1       7.618   6.239   9.167  1.00 11.77           C
ATOM     85  C   GLY     1       7.881   7.120   7.961  1.00 11.01           C
ATOM     86  O   GLY     1       8.410   8.224   8.091  1.00 12.88           O
ATOM     90  N   GLY     2       7.508   6.627   6.779  1.00 12.40           N
ATOM     93  C   GLY     2       6.768   8.576   5.448  1.00 11.60           C
ATOM     94  O   GLY     2       7.167   9.672   5.045  1.00 11.27           O
ATOM     91  CA  GLY     2       7.721   7.402   5.568  1.00 12.69           C
ATOM    103  N   GLY     3       5.497   8.366   5.793  1.00 11.26           N
ATOM    104  CA  GLY     3       4.532   9.449   5.710  1.00 10.37           C
ATOM    105  C   GLY     3       4.720  10.486   6.800  1.00  9.88           C
ATOM    106  O   GLY     3       4.628  11.691   6.546  1.00 10.25           O
ATOM    110  N   GLY     4       4.985  10.037   8.028  1.00  8.62           N
ATOM    111  CA  GLY     4       5.193  10.977   9.116  1.00  8.63           C
ATOM    112  C   GLY     4       6.535  11.680   9.034  1.00  8.81           C
ATOM    113  O   GLY     4       6.636  12.879   9.309  1.00  8.24           O
ATOM    103  N   GLY     5       7.583  10.946   8.656  1.00 11.26           N
ATOM    104  CA  GLY     5       8.896  11.560   8.540  1.00 10.37           C
ATOM    105  C   GLY     5       9.021  12.446   7.315  1.00  9.88           C
ATOM    106  O   GLY     5       9.568  13.550   7.391  1.00 10.25           O
ATOM    103  N   GLY     6       8.518  11.979   6.172  1.00 11.26           N
ATOM    104  CA  GLY     6       8.593  12.782   4.963  1.00 10.37           C
ATOM    105  C   GLY     6       7.617  13.943   4.969  1.00  9.88           C
ATOM    106  O   GLY     6       7.934  15.035   4.489  1.00 10.25           O
ATOM    110  N   GLY     7       6.417  13.727   5.513  1.00  8.62           N
ATOM    111  CA  GLY     7       5.433  14.793   5.568  1.00  8.63           C
ATOM    112  C   GLY     7       5.662  15.777   6.693  1.00  8.81           C
ATOM    113  O   GLY     7       5.306  16.953   6.569  1.00  8.24           O
ATOM    118  N   GLY     8       6.250  15.324   7.797  1.00  9.28           N
ATOM    119  CA  GLY     8       6.515  16.195   8.924  1.00 10.02           C
ATOM    120  C   GLY     8       7.886  16.836   8.872  1.00  9.51           C
ATOM    121  O   GLY     8       8.132  17.847   9.536  1.00 10.70           O
ATOM    127  N   GLY     9       8.787  16.256   8.086  1.00  9.78           N
ATOM    128  CA  GLY     9      10.134  16.779   7.955  1.00 11.03           C
ATOM    129  C   GLY     9      10.287  17.762   6.812  1.00  9.43           C
ATOM    130  O   GLY     9       9.710  18.850   6.834  1.00 10.28           O
TER
END
"""

def check_hbonds(file_name):
  pairs = [("1","5"),("2","6"),("3","7"),("4","8"),("5","9")]
  ph = iotbx.pdb.input(file_name=file_name).construct_hierarchy()
  asc = ph.atom_selection_cache()
  sites_cart = ph.atoms().extract_xyz()
  result = flex.double()
  for p in pairs:
    o = asc.selection("resseq %s and name O"%p[0])
    n = asc.selection("resseq %s and name N"%p[1])
    ro = sites_cart.select(o)[0]
    rn = sites_cart.select(n)[0]
    d = math.sqrt((ro[0]-rn[0])**2+(ro[1]-rn[1])**2+(ro[2]-rn[2])**2)
    result.append(d)
  return result

def get_target_map_object(map_data, xray_structure, d_min):
  cg = maptbx.crystal_gridding(
    unit_cell             = xray_structure.unit_cell(),
    pre_determined_n_real = map_data.all(),
      space_group_info    = xray_structure.space_group_info())
  complete_set = miller.build_set(
    crystal_symmetry = xray_structure.crystal_symmetry(),
    anomalous_flag   = False,
    d_min            = d_min)
  miller_array = complete_set.structure_factors_from_map(
    map            = map_data,
    use_scale      = True,
    anomalous_flag = False,
    use_sg         = True)
  return group_args(
    map_data             = map_data,
    f_map_diff       = None,
    miller_array     = miller_array,
    crystal_gridding = cg,
    d_min            = d_min)

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  Exercise phenix.real_space_refine running from Python script.
  """
  d_min=10.
  file_name="%s.pdb"%prefix
  of=open(file_name, "w")
  print(pdb_str, file=of)
  of.close()
  #
  cmd = " ".join([
    "phenix.fmodel",
    "%s"%file_name,
    "random_seed=0",
    "file_name=%s.mtz"%prefix,
    "high_res=%s"%str(d_min),
    ">%s.zlog"%prefix])
  assert easy_run.call(cmd)==0
  #
  args = [
    "%s"%file_name,
    "%s.mtz"%prefix,
    "ncs_constraints=False",
    "ramachandran_plot_restraints.enabled=false",
    "secondary_structure.enabled=True"]
  r = run_real_space_refine(args = args, prefix = prefix,
    assert_is_similar_hierarchy = False)
  # check H-bonds
  d1 = check_hbonds(file_name=r.pdb)
  # Target is really 2.9
  assert approx_equal(d1.min_max_mean().as_tuple(), [2.9,2.9,2.9], 0.2)
  #
  # run same refinement from python script
  #
  params = parameters.get_master_params().extract() #real_space_refine.master_params().extract()
  params.ncs_constraints = False # do not use NCS constraints
  log = sys.stdout
  #
  params.pdb_interpretation.ncs_search.enabled=params.ncs_constraints # do not use NCS constraints
  params.pdb_interpretation.secondary_structure.enabled=True
  params.pdb_interpretation.peptide_link.ramachandran_restraints=False
  params.resolution=d_min

  pdb_inp = iotbx.pdb.input(file_name)
  model = mmtbx.model.manager(model_input = pdb_inp)
  model.process(pdb_interpretation_params = params,
    make_restraints=True)
  fc = model.get_xray_structure().structure_factors(d_min=d_min).f_calc()
  fft_map = fc.fft_map(resolution_factor=0.25)
  fft_map.apply_sigma_scaling()
  map_data = fft_map.real_map_unpadded()
  mm = map_manager.map_manager(
    map_data                   = map_data,
    unit_cell_grid             = map_data.all(),
    unit_cell_crystal_symmetry = fc.crystal_symmetry(),
    wrapping                   = False)
  mmm = map_model_manager.map_model_manager(
    model = model, map_manager = mm)
  # run refinement
  refined = macro_cycle_real_space.run(
    params                = params,
    map_model_manager     = mmm,
    rotamer_manager       = None,
    sin_cos_table         = None,
    rigid_body_selections = None,
    log                   = log)
  refined.structure_monitor.model.get_hierarchy().write_pdb_file(
    file_name        = "%s_from_script.pdb"%prefix,
    crystal_symmetry = model.crystal_symmetry())
  d1 = check_hbonds(file_name="%s_from_script.pdb"%prefix)
  assert approx_equal(d1.min_max_mean().as_tuple(), [2.9,2.9,2.9], 0.2)

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
