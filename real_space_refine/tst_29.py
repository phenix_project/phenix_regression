from __future__ import print_function
import time, math
from libtbx import easy_run
import iotbx.pdb
from scitbx.array_family import flex
from libtbx.test_utils import approx_equal
import os
from phenix_regression.real_space_refine import run_real_space_refine

pdb_str_1="""
CRYST1   35.663   38.756   36.341  90.00  90.00  90.00 P 1
ATOM      1  N   ALA A   1       8.124  22.830   6.966  1.00 20.00           N
ATOM      2  CA  ALA A   1       8.341  22.692   8.401  1.00 20.00           C
ATOM      3  C   ALA A   1       7.947  21.300   8.882  1.00 20.00           C
ATOM      4  O   ALA A   1       8.602  20.725   9.753  1.00 20.00           O
ATOM      5  CB  ALA A   1       7.563  23.756   9.160  1.00 20.00           C
ATOM      6  N   ALA A   2       6.867  20.764   8.306  1.00 20.00           N
ATOM      7  CA  ALA A   2       6.402  19.435   8.690  1.00 20.00           C
ATOM      8  C   ALA A   2       7.365  18.353   8.218  1.00 20.00           C
ATOM      9  O   ALA A   2       7.609  17.375   8.936  1.00 20.00           O
ATOM     10  CB  ALA A   2       5.000  19.188   8.133  1.00 20.00           C
ATOM     11  N   ALA A   3       7.922  18.510   7.014  1.00 20.00           N
ATOM     12  CA  ALA A   3       8.860  17.519   6.496  1.00 20.00           C
ATOM     13  C   ALA A   3      10.184  17.571   7.248  1.00 20.00           C
ATOM     14  O   ALA A   3      10.790  16.527   7.522  1.00 20.00           O
ATOM     15  CB  ALA A   3       9.081  17.735   5.000  1.00 20.00           C
ATOM     16  N   ALA A   4      10.646  18.776   7.592  1.00 20.00           N
ATOM     17  CA  ALA A   4      11.900  18.908   8.326  1.00 20.00           C
ATOM     18  C   ALA A   4      11.746  18.425   9.762  1.00 20.00           C
ATOM     19  O   ALA A   4      12.657  17.795  10.313  1.00 20.00           O
ATOM     20  CB  ALA A   4      12.380  20.358   8.293  1.00 20.00           C
ATOM     21  N   ALA A   5      10.596  18.708  10.382  1.00 20.00           N
ATOM     22  CA  ALA A   5      10.365  18.266  11.753  1.00 20.00           C
ATOM     23  C   ALA A   5      10.170  16.757  11.815  1.00 20.00           C
ATOM     24  O   ALA A   5      10.644  16.101  12.751  1.00 20.00           O
ATOM     25  CB  ALA A   5       9.157  18.992  12.344  1.00 20.00           C
ATOM     26  N   ALA A   6       9.478  16.189  10.822  1.00 20.00           N
ATOM     27  CA  ALA A   6       9.262  14.746  10.801  1.00 20.00           C
ATOM     28  C   ALA A   6      10.556  14.007  10.488  1.00 20.00           C
ATOM     29  O   ALA A   6      10.829  12.948  11.067  1.00 20.00           O
ATOM     30  CB  ALA A   6       8.177  14.390   9.786  1.00 20.00           C
ATOM     31  N   ALA A   7      11.367  14.553   9.576  1.00 20.00           N
ATOM     32  CA  ALA A   7      12.635  13.917   9.235  1.00 20.00           C
ATOM     33  C   ALA A   7      13.625  14.021  10.388  1.00 20.00           C
ATOM     34  O   ALA A   7      14.370  13.072  10.663  1.00 20.00           O
ATOM     35  CB  ALA A   7      13.215  14.543   7.967  1.00 20.00           C
ATOM     36  N   ALA A   8      13.641  15.167  11.076  1.00 20.00           N
ATOM     37  CA  ALA A   8      14.545  15.341  12.209  1.00 20.00           C
ATOM     38  C   ALA A   8      14.118  14.471  13.384  1.00 20.00           C
ATOM     39  O   ALA A   8      14.965  13.902  14.083  1.00 20.00           O
ATOM     40  CB  ALA A   8      14.601  16.813  12.617  1.00 20.00           C
ATOM     41  N   ALA A   9      12.806  14.355  13.611  1.00 20.00           N
ATOM     42  CA  ALA A   9      12.312  13.523  14.704  1.00 20.00           C
ATOM     43  C   ALA A   9      12.542  12.047  14.410  1.00 20.00           C
ATOM     44  O   ALA A   9      12.853  11.267  15.318  1.00 20.00           O
ATOM     45  CB  ALA A   9      10.830  13.802  14.951  1.00 20.00           C
ATOM     46  N   ALA A  10      12.399  11.648  13.142  1.00 20.00           N
ATOM     47  CA  ALA A  10      12.621  10.253  12.775  1.00 20.00           C
ATOM     48  C   ALA A  10      14.102   9.904  12.841  1.00 20.00           C
ATOM     49  O   ALA A  10      14.468   8.802  13.270  1.00 20.00           O
ATOM     50  CB  ALA A  10      12.063   9.980  11.379  1.00 20.00           C
ATOM     51  N   ALA A  11      14.968  10.832  12.423  1.00 20.00           N
ATOM     52  CA  ALA A  11      16.405  10.583  12.472  1.00 20.00           C
ATOM     53  C   ALA A  11      16.906  10.561  13.911  1.00 20.00           C
ATOM     54  O   ALA A  11      17.769   9.747  14.262  1.00 20.00           O
ATOM     55  CB  ALA A  11      17.151  11.639  11.656  1.00 20.00           C
ATOM     56  N   ALA A  12      16.371  11.446  14.757  1.00 20.00           N
ATOM     57  CA  ALA A  12      16.785  11.478  16.156  1.00 20.00           C
ATOM     58  C   ALA A  12      16.263  10.260  16.906  1.00 20.00           C
ATOM     59  O   ALA A  12      16.965   9.701  17.758  1.00 20.00           O
ATOM     60  CB  ALA A  12      16.304  12.769  16.819  1.00 20.00           C
ATOM     61  N   ALA A  13      15.034   9.835  16.601  1.00 20.00           N
ATOM     62  CA  ALA A  13      14.467   8.663  17.260  1.00 20.00           C
ATOM     63  C   ALA A  13      15.173   7.390  16.812  1.00 20.00           C
ATOM     64  O   ALA A  13      15.402   6.482  17.620  1.00 20.00           O
ATOM     65  CB  ALA A  13      12.967   8.576  16.982  1.00 20.00           C
ATOM     66  N   ALA A  14      15.527   7.308  15.527  1.00 20.00           N
ATOM     67  CA  ALA A  14      16.222   6.129  15.022  1.00 20.00           C
ATOM     68  C   ALA A  14      17.658   6.081  15.527  1.00 20.00           C
ATOM     69  O   ALA A  14      18.189   5.000  15.808  1.00 20.00           O
ATOM     70  CB  ALA A  14      16.185   6.109  13.495  1.00 20.00           C
ATOM     71  N   ALA A  15      18.303   7.245  15.649  1.00 20.00           N
ATOM     72  CA  ALA A  15      19.679   7.281  16.133  1.00 20.00           C
ATOM     73  C   ALA A  15      19.745   7.007  17.631  1.00 20.00           C
ATOM     74  O   ALA A  15      20.663   6.326  18.103  1.00 20.00           O
ATOM     75  CB  ALA A  15      20.318   8.629  15.803  1.00 20.00           C
ATOM     76  N   ALA A  16      18.786   7.527  18.390  1.00 20.00           N
ATOM     77  CA  ALA A  16      18.757   7.322  19.834  1.00 20.00           C
ATOM     78  C   ALA A  16      18.263   5.920  20.175  1.00 20.00           C
ATOM     79  O   ALA A  16      18.233   5.527  21.341  1.00 20.00           O
ATOM     80  CB  ALA A  16      17.881   8.371  20.501  1.00 20.00           C
TER
ATOM     81  N   ALA B   1      18.124  32.830  16.966  1.00500.00           N
ATOM     82  CA  ALA B   1      18.341  32.692  18.401  1.00500.00           C
ATOM     83  C   ALA B   1      17.947  31.300  18.882  1.00500.00           C
ATOM     84  O   ALA B   1      18.602  30.725  19.753  1.00500.00           O
ATOM     85  CB  ALA B   1      17.563  33.756  19.160  1.00500.00           C
ATOM     86  N   ALA B   2      16.867  30.764  18.306  1.00500.00           N
ATOM     87  CA  ALA B   2      16.402  29.435  18.690  1.00500.00           C
ATOM     88  C   ALA B   2      17.365  28.353  18.218  1.00500.00           C
ATOM     89  O   ALA B   2      17.609  27.375  18.936  1.00500.00           O
ATOM     90  CB  ALA B   2      15.000  29.188  18.133  1.00500.00           C
ATOM     91  N   ALA B   3      17.922  28.510  17.014  1.00500.00           N
ATOM     92  CA  ALA B   3      18.860  27.519  16.496  1.00500.00           C
ATOM     93  C   ALA B   3      20.184  27.571  17.248  1.00500.00           C
ATOM     94  O   ALA B   3      20.790  26.527  17.522  1.00500.00           O
ATOM     95  CB  ALA B   3      19.081  27.735  15.000  1.00500.00           C
ATOM     96  N   ALA B   4      20.646  28.776  17.592  1.00500.00           N
ATOM     97  CA  ALA B   4      21.900  28.908  18.326  1.00500.00           C
ATOM     98  C   ALA B   4      21.746  28.425  19.762  1.00500.00           C
ATOM     99  O   ALA B   4      22.657  27.795  20.313  1.00500.00           O
ATOM    100  CB  ALA B   4      22.380  30.358  18.293  1.00500.00           C
ATOM    101  N   ALA B   5      20.596  28.708  20.382  1.00500.00           N
ATOM    102  CA  ALA B   5      20.365  28.266  21.753  1.00500.00           C
ATOM    103  C   ALA B   5      20.170  26.757  21.815  1.00500.00           C
ATOM    104  O   ALA B   5      20.644  26.101  22.751  1.00500.00           O
ATOM    105  CB  ALA B   5      19.157  28.992  22.344  1.00500.00           C
ATOM    106  N   ALA B   6      19.478  26.189  20.822  1.00500.00           N
ATOM    107  CA  ALA B   6      19.262  24.746  20.801  1.00500.00           C
ATOM    108  C   ALA B   6      20.556  24.007  20.488  1.00500.00           C
ATOM    109  O   ALA B   6      20.829  22.948  21.067  1.00500.00           O
ATOM    110  CB  ALA B   6      18.177  24.390  19.786  1.00500.00           C
ATOM    111  N   ALA B   7      21.367  24.553  19.576  1.00500.00           N
ATOM    112  CA  ALA B   7      22.635  23.917  19.235  1.00500.00           C
ATOM    113  C   ALA B   7      23.625  24.021  20.388  1.00500.00           C
ATOM    114  O   ALA B   7      24.370  23.072  20.663  1.00500.00           O
ATOM    115  CB  ALA B   7      23.215  24.543  17.967  1.00500.00           C
ATOM    116  N   ALA B   8      23.641  25.167  21.076  1.00500.00           N
ATOM    117  CA  ALA B   8      24.545  25.341  22.209  1.00500.00           C
ATOM    118  C   ALA B   8      24.118  24.471  23.384  1.00500.00           C
ATOM    119  O   ALA B   8      24.965  23.902  24.083  1.00500.00           O
ATOM    120  CB  ALA B   8      24.601  26.813  22.617  1.00500.00           C
ATOM    121  N   ALA B   9      22.806  24.355  23.611  1.00500.00           N
ATOM    122  CA  ALA B   9      22.312  23.523  24.704  1.00500.00           C
ATOM    123  C   ALA B   9      22.542  22.047  24.410  1.00500.00           C
ATOM    124  O   ALA B   9      22.853  21.267  25.318  1.00500.00           O
ATOM    125  CB  ALA B   9      20.830  23.802  24.951  1.00500.00           C
ATOM    126  N   ALA B  10      22.399  21.648  23.142  1.00500.00           N
ATOM    127  CA  ALA B  10      22.621  20.253  22.775  1.00500.00           C
ATOM    128  C   ALA B  10      24.102  19.904  22.841  1.00500.00           C
ATOM    129  O   ALA B  10      24.468  18.802  23.270  1.00500.00           O
ATOM    130  CB  ALA B  10      22.063  19.980  21.379  1.00500.00           C
ATOM    131  N   ALA B  11      24.968  20.832  22.423  1.00500.00           N
ATOM    132  CA  ALA B  11      26.405  20.583  22.472  1.00500.00           C
ATOM    133  C   ALA B  11      26.906  20.561  23.911  1.00500.00           C
ATOM    134  O   ALA B  11      27.769  19.747  24.262  1.00500.00           O
ATOM    135  CB  ALA B  11      27.151  21.639  21.656  1.00500.00           C
ATOM    136  N   ALA B  12      26.371  21.446  24.757  1.00500.00           N
ATOM    137  CA  ALA B  12      26.785  21.478  26.156  1.00500.00           C
ATOM    138  C   ALA B  12      26.263  20.260  26.906  1.00500.00           C
ATOM    139  O   ALA B  12      26.965  19.701  27.758  1.00500.00           O
ATOM    140  CB  ALA B  12      26.304  22.769  26.819  1.00500.00           C
ATOM    141  N   ALA B  13      25.034  19.835  26.601  1.00500.00           N
ATOM    142  CA  ALA B  13      24.467  18.663  27.260  1.00500.00           C
ATOM    143  C   ALA B  13      25.173  17.390  26.812  1.00500.00           C
ATOM    144  O   ALA B  13      25.402  16.482  27.620  1.00500.00           O
ATOM    145  CB  ALA B  13      22.967  18.576  26.982  1.00500.00           C
ATOM    146  N   ALA B  14      25.527  17.308  25.527  1.00500.00           N
ATOM    147  CA  ALA B  14      26.222  16.129  25.022  1.00500.00           C
ATOM    148  C   ALA B  14      27.658  16.081  25.527  1.00500.00           C
ATOM    149  O   ALA B  14      28.189  15.000  25.808  1.00500.00           O
ATOM    150  CB  ALA B  14      26.185  16.109  23.495  1.00500.00           C
ATOM    151  N   ALA B  15      28.303  17.245  25.649  1.00500.00           N
ATOM    152  CA  ALA B  15      29.679  17.281  26.133  1.00500.00           C
ATOM    153  C   ALA B  15      29.745  17.007  27.631  1.00500.00           C
ATOM    154  O   ALA B  15      30.663  16.326  28.103  1.00500.00           O
ATOM    155  CB  ALA B  15      30.318  18.629  25.803  1.00500.00           C
ATOM    156  N   ALA B  16      28.786  17.527  28.390  1.00500.00           N
ATOM    157  CA  ALA B  16      28.757  17.322  29.834  1.00500.00           C
ATOM    158  C   ALA B  16      28.263  15.920  30.175  1.00500.00           C
ATOM    159  O   ALA B  16      28.233  15.527  31.341  1.00500.00           O
ATOM    160  CB  ALA B  16      27.881  18.371  30.501  1.00500.00           C
TER
END
"""

pdb_str_2="""
HELIX    1   1 ALA A    1  ALA A   16  1                                  16
HELIX    1   1 ALA B    1  ALA B   16  1                                  16
%s
"""%pdb_str_1

eff_str = """
pdb_interpretation {
  secondary_structure {
    enabled = True
    protein {
      helix {
        serial_number = 1
        helix_identifier = "1"
        selection = chain 'A' and resid 1 through 16
        sigma = 0.005
      }
    }
  }
}
"""

def dist(site1, site2):
  return math.sqrt(
    (site1[0]-site2[0])**2 +
    (site1[1]-site2[1])**2 +
    (site1[2]-site2[2])**2)

def h_bond_i_seqs(pdb_hierarchy):
  h_bonds_i_seqs_A = []
  h_bonds_i_seqs_B = []
  atoms = pdb_hierarchy.atoms()
  distances = flex.double()
  for i, a_i in enumerate(list(atoms)):
    for j, a_j in enumerate(list(atoms)):
      if(i<j):
        if(a_i.name.strip() in ["N","O"] and a_j.name.strip() in ["O","N"] and
           a_i.name.strip() != a_j.name.strip()):
          d = dist(a_i.xyz, a_j.xyz)
          ci = a_i.parent().parent().parent().id.strip()
          cj = a_j.parent().parent().parent().id.strip()
          if(d<3.0 and d>2.8):
            if(ci==cj=="A"): h_bonds_i_seqs_A.append([a_i.i_seq, a_j.i_seq])
            if(ci==cj=="B"): h_bonds_i_seqs_B.append([a_i.i_seq, a_j.i_seq])
  assert len(h_bonds_i_seqs_A) == len(h_bonds_i_seqs_B)
  return h_bonds_i_seqs_A, h_bonds_i_seqs_B

def count_helices_in_pdb(file_name):
  inp_f = open(file_name, 'r')
  n = 0
  for l in inp_f.readlines():
    if l.startswith("HELIX"):
      n += 1
  inp_f.close()
  return n

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  eff file contains helix for chain A
  HELIX/SHEET - pdb contains helix for chains A and B
  Presumably, manually crafted annotations (eff) are of higher priority that
  those from pdb. They should be used and outputted to the final pdb file.
  Exercise SS restraints with scenarious:
    1) HELIX/SHEET + eff - only chain A idealized
    2) -----------   eff - exactly the same as 1)
    3) HELIX/SHEET   --- - chain A and B idealized.
  Annotation used in refinement should be outputted to resulting pdb file

  Input model is perfect.
  """
  # Write files
  # 1
  of = open("%s_1.pdb"%prefix, "w")
  print(pdb_str_1, file=of)
  of.close()
  # 2
  of = open("%s_2.pdb"%prefix, "w")
  print(pdb_str_2, file=of)
  of.close()
  # eff
  of = open("%s.eff"%prefix, "w")
  print(eff_str, file=of)
  of.close()
  # H-bonds answer
  ph = iotbx.pdb.input(source_info=None, lines=pdb_str_1).construct_hierarchy()
  hb_pairs_A, hb_pairs_B = h_bond_i_seqs(pdb_hierarchy = ph)
  assert len(hb_pairs_A) == 12
  # Compute map
  cmd = " ".join([
    "phenix.fmodel",
    "%s_1.pdb"%prefix,
    "file_name=%s.mtz"%prefix,
    "high_res=6",
    "low_res=7",
    ">%s.zlog"%prefix])
  assert easy_run.call(cmd)==0
  ###
  common = [
    "run=minimization_global",
    "target_bonds_rmsd=0.005",
    "target_angles_rmsd=0.5",
    "%s.mtz"%prefix,
    "macro_cycles=3",
    "ncs_constraints=false",
    "ramachandran_plot_restraints.enabled=false",
    "secondary_structure.enabled=true"]
  ###
  # Refinement #1
  args = common + [
    "%s_2.pdb"%prefix,
    "%s.eff"%prefix]
  r = run_real_space_refine(args = args, prefix = prefix)
  xyz = iotbx.pdb.input(file_name=r.pdb
    ).construct_hierarchy().atoms().extract_xyz()
  for p in hb_pairs_A:
    d = dist(xyz[p[0]], xyz[p[1]])
    assert approx_equal(d, 2.9, 0.15)
  ds = flex.double()
  for p in hb_pairs_B:
    d = dist(xyz[p[0]], xyz[p[1]])
    ds.append(d)
  assert flex.mean(ds) > 3.5, flex.mean(ds)
  assert count_helices_in_pdb(file_name=r.pdb) == 1
  # Refinement #2
  args = common + [
    "%s_1.pdb"%prefix,
    "%s.eff"%prefix]
  r = run_real_space_refine(args = args, prefix = prefix)
  xyz = iotbx.pdb.input(file_name=r.pdb
    ).construct_hierarchy().atoms().extract_xyz()
  for p in hb_pairs_A:
    d = dist(xyz[p[0]], xyz[p[1]])
    assert approx_equal(d, 2.9, 0.15)
  ds = flex.double()
  for p in hb_pairs_B:
    d = dist(xyz[p[0]], xyz[p[1]])
    ds.append(d)
  assert flex.mean(ds) > 3.5, flex.mean(ds)
  assert count_helices_in_pdb(file_name=r.pdb) == 1
  # Refinement #3
  args = common + [
    "%s_2.pdb"%prefix]
  r = run_real_space_refine(args = args, prefix = prefix)
  xyz = iotbx.pdb.input(file_name=r.pdb
    ).construct_hierarchy().atoms().extract_xyz()
  for p in hb_pairs_A+hb_pairs_B:
    d = dist(xyz[p[0]], xyz[p[1]])
    assert approx_equal(d, 2.9, 0.15)
  assert count_helices_in_pdb(file_name=r.pdb) == 2

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK time =%8.3f"%(time.time() - t0))
