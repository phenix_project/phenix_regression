from __future__ import print_function
import time, os
from libtbx import easy_run
import iotbx.pdb
import libtbx.load_env
from phenix_regression.real_space_refine import run_real_space_refine

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  Make sure program stops with Sorry if NCS group selections are empty (at least
  one).
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/real_space_refine/data/tst_17.pdb",
    test=os.path.isfile)
  map_data = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/real_space_refine/data/tst_17.ccp4",
    test=os.path.isfile)
  params = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/real_space_refine/data/tst_17.eff",
    test=os.path.isfile)
  #
  args = [
    "%s"%pdb,
    "%s"%map_data,
    "%s"%params,
    "resolution=10",
    "ramachandran_plot_restraints.enabled=false"]
  r = run_real_space_refine(args = args, prefix = prefix, sorry_expected=True)
  line = r.r.stderr_lines[-1].strip()
  assert line == "Sorry: Empty selection in NCS group definition: chain H"

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK time =%8.3f"%(time.time() - t0))
