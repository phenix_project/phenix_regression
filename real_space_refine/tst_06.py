from __future__ import print_function
from libtbx import easy_run
import time
import iotbx.pdb
from scitbx.array_family import flex
import os
from phenix_regression.real_space_refine import run_real_space_refine

pdb_str_answer = """\
CRYST1   19.599   17.955   21.862  90.00  90.00  90.00 P 1
HETATM 6830  C01 LIG A   1      12.234   8.131  16.194  1.00 20.00           C
HETATM 6831  C02 LIG A   1      12.906   7.437  15.030  1.00 20.00           C
HETATM 6832  C03 LIG A   1      12.301   6.051  14.852  1.00 20.00           C
HETATM 6833  C04 LIG A   1      11.040   6.039  13.943  1.00 20.00           C
HETATM 6834  C05 LIG A   1       9.757   5.590  14.691  1.00 20.00           C
HETATM 6835  C06 LIG A   1       8.996   6.783  15.324  1.00 20.00           C
HETATM 6836  C07 LIG A   1       7.927   7.381  14.541  1.00 20.00           C
HETATM 6837  C35 LIG A   1       8.807   6.840  16.862  1.00 20.00           C
HETATM 6838  C09 LIG A   1       7.619   6.724  13.167  1.00 20.00           C
HETATM 6839  C10 LIG A   1       8.090   7.555  11.975  1.00 20.00           C
HETATM 6840  C12 LIG A   1       8.943   9.734  11.541  1.00 20.00           C
HETATM 6841  C14 LIG A   1       9.366  11.116  12.083  1.00 20.00           C
HETATM 6842  C15 LIG A   1       9.871  11.054  13.521  1.00 20.00           C
HETATM 6843  C16 LIG A   1      11.290  11.644  13.621  1.00 20.00           C
HETATM 6844  C17 LIG A   1      12.334  10.622  13.056  1.00 20.00           C
HETATM 6845  C19 LIG A   1      13.114   9.710  14.008  1.00 20.00           C
HETATM 6846  O08 LIG A   1       9.208   8.065  14.666  1.00 20.00           O
HETATM 6847  O11 LIG A   1       8.014   8.923  12.275  1.00 20.00           O
HETATM 6848  O13 LIG A   1       9.403   9.334  10.484  1.00 20.00           O
HETATM 6849  C20 LIG A   1      12.733   8.265  13.752  1.00 20.00           C
HETATM 6850  C22 LIG A   1      14.599   9.893  13.779  1.00 20.00           C
HETATM 6851  O18 LIG A   1      12.531  10.552  11.852  1.00 20.00           O
HETATM 6852  C23 LIG A   1      11.372  12.955  12.820  1.00 20.00           C
HETATM 6853  O21 LIG A   1      13.551   7.740  12.741  1.00 20.00           O
HETATM 6854  C24 LIG A   1      11.610  11.939  15.101  1.00 20.00           C
HETATM 6855  O25 LIG A   1       9.012  11.774  14.343  1.00 20.00           O
HETATM 6856  C26 LIG A   1       7.203   7.247  10.774  1.00 20.00           C
HETATM 6857  C27 LIG A   1       7.610   6.332   9.836  1.00 20.00           C
HETATM 6858  C28 LIG A   1       6.718   6.022   8.629  1.00 20.00           C
HETATM 6859  C29 LIG A   1       5.712   5.006   8.590  1.00 20.00           C
HETATM 6860  N32 LIG A   1       6.771   6.736   7.322  1.00 20.00           N
HETATM 6861  C31 LIG A   1       5.818   6.206   6.429  1.00 20.00           C
HETATM 6862  S30 LIG A   1       5.000   5.000   7.148  1.00 20.00           S
HETATM 6863  C33 LIG A   1       5.589   6.691   5.000  1.00 20.00           C
HETATM 6864  C34 LIG A   1       5.855   7.963  10.615  1.00 20.00           C
TER
END
"""

cif_str = """\
#   SMILES string: C[C@H]1CCC[C@@]2([C@@H](O2)C[C@H](OC(=O)C[C@@H](C(C(=O)[C@@H]([C@H]1O)C)(C)C)O)/C(=C/C3=CSC(=N3)C)/C)C
#
data_comp_list
loop_
_chem_comp.id
_chem_comp.three_letter_code
_chem_comp.name
_chem_comp.group
_chem_comp.number_atoms_all
_chem_comp.number_atoms_nh
_chem_comp.desc_level
LIG        LIG 'epothilone B             ' ligand 76 35 .
#
data_comp_LIG
#
loop_
_chem_comp_atom.comp_id
_chem_comp_atom.atom_id
_chem_comp_atom.type_symbol
_chem_comp_atom.type_energy
_chem_comp_atom.partial_charge
_chem_comp_atom.x
_chem_comp_atom.y
_chem_comp_atom.z
LIG         C01    C   CH3   .          5.0574   -0.3562   -2.8996
LIG         C02    C   CH1   .          3.5684   -0.1392   -2.7584
LIG         C03    C   CH2   .          3.2041   -0.0653   -1.2843
LIG         C04    C   CH2   .          1.9706   -0.9426   -0.9377
LIG         C05    C   CH2   .          2.2719   -1.9624    0.1900
LIG         C06    C   CT    .          1.8674   -1.4303    1.5875
LIG         C07    C   CH1   .          0.4673   -1.5836    1.9407
LIG         O08    O   O2    .          0.9882   -0.2688    1.5908
LIG         C09    C   CH2   .         -0.4288   -2.2590    0.8688
LIG         C10    C   CH1   .         -1.8083   -1.6123    0.7757
LIG         O11    O   O2    .         -1.6796   -0.2199    0.8706
LIG         C12    C   C     .         -2.0268    0.4788   -0.3311
LIG         O13    O   O     .         -2.8973    0.0369   -1.0627
LIG         C14    C   CH2   .         -1.3711    1.8391   -0.6481
LIG         C15    C   CH1   .         -1.2824    2.1031   -2.1469
LIG         C16    C   CT    .         -0.1332    3.0846   -2.4290
LIG         C17    C   C     .          0.9998    2.3487   -3.2182
LIG         O18    O   O     .          0.8540    2.1004   -4.4052
LIG         C19    C   CH1   .          2.3153    1.9954   -2.5190
LIG         C20    C   CH1   .          3.1755    1.1672   -3.4499
LIG         O21    O   OH1   .          4.3320    1.8866   -3.7815
LIG         C22    C   CH3   .          3.0494    3.2672   -2.1508
LIG         C23    C   CH3   .          0.4279    3.6134   -1.0988
LIG         C24    C   CH3   .         -0.6608    4.2648   -3.2691
LIG         O25    O   OH1   .         -1.0459    0.9074   -2.8153
LIG         C26    C   C     .         -2.6867   -2.1211    1.9124
LIG         C27    C   C1    .         -3.0096   -1.2893    2.9542
LIG         C28    C   CR5   .         -4.2228   -1.6022    3.8366
LIG         C29    C   CR15  .         -5.5614   -1.1673    3.5770
LIG         S30    S   S2    .         -6.5392   -1.6916    4.7413
LIG         C31    C   CR5   .         -5.4677   -2.4869    5.6679
LIG         N32    N   N     .         -4.1816   -2.3948    5.0973
LIG         C33    C   CH3   .         -5.8026   -3.2214    6.9626
LIG         C34    C   CH3   .         -3.3063   -3.5229    1.8299
LIG         C35    C   CH3   .          2.8710   -1.5162    2.7651
LIG        H021    H   HCH1  .          3.0876   -0.8883   -3.1771
LIG        H071    H   HCH1  .          0.2812   -1.7280    2.8955
LIG        H101    H   HCH1  .         -2.2211   -1.8448   -0.0858
LIG        H151    H   HCH1  .         -2.1305    2.4923   -2.4618
LIG        H191    H   HCH1  .          2.1262    1.4841   -1.7107
LIG        H201    H   HCH1  .          2.6698    0.9646   -4.2695
LIG        H011    H   HCH3  .          5.5390    0.3296   -2.3848
LIG        H012    H   HCH3  .          5.2928   -1.2484   -2.5578
LIG        H013    H   HCH3  .          5.3098   -0.2915   -3.8486
LIG        H031    H   HCH2  .          3.9728   -0.3708   -0.7502
LIG        H032    H   HCH2  .          3.0069    0.8633   -1.0532
LIG        H041    H   HCH2  .          1.6914   -1.4329   -1.7445
LIG        H042    H   HCH2  .          1.2384   -0.3605   -0.6510
LIG        H051    H   HCH2  .          3.2355   -2.1604    0.1913
LIG        H052    H   HCH2  .          1.7762   -2.7923    0.0096
LIG        H091    H   HCH2  .          0.0128   -2.1906   -0.0026
LIG        H092    H   HCH2  .         -0.5375   -3.2103    1.0976
LIG        H141    H   HCH2  .         -0.4686    1.8516   -0.2700
LIG        H142    H   HCH2  .         -1.9030    2.5550   -0.2290
LIG        H211    H   HOH1  .          4.4052    1.9400   -4.6717
LIG        H221    H   HCH3  .          2.8552    3.4985   -1.2157
LIG        H222    H   HCH3  .          2.7527    3.9991   -2.7380
LIG        H223    H   HCH3  .          4.0174    3.1308   -2.2605
LIG        H231    H   HCH3  .         -0.3144    3.9095   -0.5240
LIG        H232    H   HCH3  .          1.0291    4.3726   -1.2749
LIG        H233    H   HCH3  .          0.9257    2.9006   -0.6478
LIG        H241    H   HCH3  .         -1.3081    4.7822   -2.7379
LIG        H242    H   HCH3  .         -1.1017    3.9202   -4.0782
LIG        H243    H   HCH3  .          0.0901    4.8453   -3.5269
LIG        H251    H   HOH1  .         -1.6654    0.7931   -3.4507
LIG        H271    H   H     .         -2.4431   -0.5155    3.1570
LIG        H291    H   HCR5  .         -5.8400   -0.6311    2.8065
LIG        H331    H   HCH3  .         -5.2034   -2.9142    7.6778
LIG        H332    H   HCH3  .         -6.7343   -3.0352    7.2134
LIG        H333    H   HCH3  .         -5.6861   -4.1878    6.8304
LIG        H341    H   HCH3  .         -3.0954   -4.0234    2.6497
LIG        H342    H   HCH3  .         -2.9389   -3.9982    1.0524
LIG        H343    H   HCH3  .         -4.2818   -3.4451    1.7354
LIG        H351    H   HCH3  .          2.9476   -2.4508    3.0634
LIG        H352    H   HCH3  .          2.5508   -0.9596    3.5100
LIG        H353    H   HCH3  .          3.7512   -1.1934    2.4698
#
loop_
_chem_comp_bond.comp_id
_chem_comp_bond.atom_id_1
_chem_comp_bond.atom_id_2
_chem_comp_bond.type
_chem_comp_bond.value_dist
_chem_comp_bond.value_dist_esd
LIG   C02     C01   single        1.511 0.020
LIG   C03     C02   single        1.520 0.020
LIG   C04     C03   single        1.553 0.020
LIG   C05     C04   single        1.550 0.020
LIG   C06     C05   single        1.549 0.020
LIG   O08     C07   single        1.457 0.020
LIG   C09     C07   single        1.552 0.020
LIG   C10     C09   single        1.526 0.020
LIG   C12     O11   single        1.433 0.020
LIG   O13     C12   double        1.220 0.020
LIG   C14     C12   single        1.543 0.020
LIG   C15     C14   single        1.524 0.020
LIG   O18     C17   double        1.221 0.020
LIG   C19     C17   single        1.531 0.020
LIG   O21     C20   single        1.402 0.020
LIG   C20     C19   single        1.514 0.020
LIG   C22     C19   single        1.514 0.020
LIG   C17     C16   single        1.565 0.020
LIG   C23     C16   single        1.538 0.020
LIG   C24     C16   single        1.542 0.020
LIG   C16     C15   single        1.537 0.020
LIG   O25     C15   single        1.390 0.020
LIG   O11     C10   single        1.402 0.020
LIG   C26     C10   single        1.524 0.020
LIG   C28     C27   single        1.532 0.020
LIG   C29     C28   aromatic      1.431 0.020
LIG   S30     C29   aromatic      1.608 0.020
LIG   C31     S30   aromatic      1.625 0.020
LIG   N32     C31   aromatic      1.410 0.020
LIG   C33     C31   single        1.526 0.020
LIG   C27     C26   double        1.372 0.020
LIG   C34     C26   single        1.535 0.020
LIG   C07     C06   single        1.452 0.020
LIG   C35     C06   single        1.550 0.020
LIG   C02     C20   single        1.529 0.020
LIG   C06     O08   single        1.457 0.020
LIG   C28     N32   aromatic      1.490 0.020
LIG  H021     C02   single        0.984 0.020
LIG  H071     C07   single        0.983 0.020
LIG  H101     C10   single        0.983 0.020
LIG  H151     C15   single        0.985 0.020
LIG  H191     C19   single        0.975 0.020
LIG  H201     C20   single        0.984 0.020
LIG  H011     C01   single        0.984 0.020
LIG  H012     C01   single        0.984 0.020
LIG  H013     C01   single        0.984 0.020
LIG  H031     C03   single        0.985 0.020
LIG  H032     C03   single        0.977 0.020
LIG  H041     C04   single        0.985 0.020
LIG  H042     C04   single        0.978 0.020
LIG  H051     C05   single        0.984 0.020
LIG  H052     C05   single        0.983 0.020
LIG  H091     C09   single        0.979 0.020
LIG  H092     C09   single        0.984 0.020
LIG  H141     C14   single        0.979 0.020
LIG  H142     C14   single        0.985 0.020
LIG  H211     O21   single        0.895 0.020
LIG  H221     C22   single        0.983 0.020
LIG  H222     C22   single        0.984 0.020
LIG  H223     C22   single        0.984 0.020
LIG  H231     C23   single        0.984 0.020
LIG  H232     C23   single        0.984 0.020
LIG  H233     C23   single        0.979 0.020
LIG  H241     C24   single        0.984 0.020
LIG  H242     C24   single        0.984 0.020
LIG  H243     C24   single        0.984 0.020
LIG  H251     O25   single        0.895 0.020
LIG  H271     C27   single        0.980 0.020
LIG  H291     C29   single        0.979 0.020
LIG  H331     C33   single        0.982 0.020
LIG  H332     C33   single        0.983 0.020
LIG  H333     C33   single        0.982 0.020
LIG  H341     C34   single        0.983 0.020
LIG  H342     C34   single        0.983 0.020
LIG  H343     C34   single        0.983 0.020
LIG  H351     C35   single        0.984 0.020
LIG  H352     C35   single        0.983 0.020
LIG  H353     C35   single        0.983 0.020
#
loop_
_chem_comp_angle.comp_id
_chem_comp_angle.atom_id_1
_chem_comp_angle.atom_id_2
_chem_comp_angle.atom_id_3
_chem_comp_angle.value_angle
_chem_comp_angle.value_angle_esd
LIG  H013     C01    H012         109.46 3.000
LIG  H013     C01    H011         109.47 3.000
LIG  H012     C01    H011         109.47 3.000
LIG  H013     C01     C02         109.47 3.000
LIG  H012     C01     C02         109.48 3.000
LIG  H011     C01     C02         109.48 3.000
LIG  H021     C02     C20         109.42 3.000
LIG  H021     C02     C03         109.42 3.000
LIG   C20     C02     C03         109.59 3.000
LIG  H021     C02     C01         109.42 3.000
LIG   C20     C02     C01         109.48 3.000
LIG   C03     C02     C01         109.49 3.000
LIG  H032     C03    H031         108.91 3.000
LIG  H032     C03     C04         108.90 3.000
LIG  H031     C03     C04         108.89 3.000
LIG  H032     C03     C02         108.90 3.000
LIG  H031     C03     C02         108.89 3.000
LIG   C04     C03     C02         112.30 3.000
LIG  H042     C04    H041         108.91 3.000
LIG  H042     C04     C05         108.89 3.000
LIG  H041     C04     C05         108.89 3.000
LIG  H042     C04     C03         108.89 3.000
LIG  H041     C04     C03         108.89 3.000
LIG   C05     C04     C03         112.32 3.000
LIG  H052     C05    H051         108.91 3.000
LIG  H052     C05     C06         108.89 3.000
LIG  H051     C05     C06         108.89 3.000
LIG  H052     C05     C04         108.89 3.000
LIG  H051     C05     C04         108.89 3.000
LIG   C06     C05     C04         112.31 3.000
LIG   C35     C06     O08         115.68 3.000
LIG   O08     C06     C07          60.11 3.000
LIG   C35     C06     C07         115.71 3.000
LIG   O08     C06     C05         115.69 3.000
LIG   C35     C06     C05         119.83 3.000
LIG   C07     C06     C05         115.78 3.000
LIG  H071     C07     C09         119.83 3.000
LIG  H071     C07     O08         115.68 3.000
LIG   C09     C07     O08         115.68 3.000
LIG  H071     C07     C06         115.72 3.000
LIG   C09     C07     C06         115.77 3.000
LIG   O08     C07     C06          60.10 3.000
LIG   C07     O08     C06          59.79 3.000
LIG  H092     C09    H091         108.91 3.000
LIG  H092     C09     C10         108.89 3.000
LIG  H091     C09     C10         108.89 3.000
LIG  H092     C09     C07         108.89 3.000
LIG  H091     C09     C07         108.89 3.000
LIG   C10     C09     C07         112.31 3.000
LIG  H101     C10     C26         109.43 3.000
LIG  H101     C10     O11         109.44 3.000
LIG   C26     C10     O11         109.51 3.000
LIG  H101     C10     C09         109.43 3.000
LIG   C26     C10     C09         109.51 3.000
LIG   O11     C10     C09         109.50 3.000
LIG   C12     O11     C10         113.91 3.000
LIG   C14     C12     O13         119.96 3.000
LIG   C14     C12     O11         119.95 3.000
LIG   O13     C12     O11         119.96 3.000
LIG  H142     C14    H141         108.92 3.000
LIG  H142     C14     C15         108.89 3.000
LIG  H141     C14     C15         108.90 3.000
LIG  H142     C14     C12         108.89 3.000
LIG  H141     C14     C12         108.89 3.000
LIG   C15     C14     C12         112.30 3.000
LIG  H151     C15     O25         109.43 3.000
LIG  H151     C15     C16         109.43 3.000
LIG   O25     C15     C16         109.50 3.000
LIG  H151     C15     C14         109.43 3.000
LIG   O25     C15     C14         109.49 3.000
LIG   C16     C15     C14         109.54 3.000
LIG   C24     C16     C23         109.45 3.000
LIG   C24     C16     C17         109.45 3.000
LIG   C23     C16     C17         109.51 3.000
LIG   C24     C16     C15         109.44 3.000
LIG   C23     C16     C15         109.49 3.000
LIG   C17     C16     C15         109.48 3.000
LIG   C19     C17     O18         119.96 3.000
LIG   C19     C17     C16         120.02 3.000
LIG   O18     C17     C16         119.96 3.000
LIG  H191     C19     C22         109.45 3.000
LIG  H191     C19     C20         109.45 3.000
LIG   C22     C19     C20         109.49 3.000
LIG  H191     C19     C17         109.44 3.000
LIG   C22     C19     C17         109.50 3.000
LIG   C20     C19     C17         109.49 3.000
LIG  H201     C20     O21         109.43 3.000
LIG  H201     C20     C19         109.43 3.000
LIG   O21     C20     C19         109.48 3.000
LIG  H201     C20     C02         109.43 3.000
LIG   C19     C20     C02         109.59 3.000
LIG   O21     C20     C02         109.47 3.000
LIG  H211     O21     C20         109.48 3.000
LIG  H223     C22    H222         109.47 3.000
LIG  H223     C22    H221         109.46 3.000
LIG  H222     C22    H221         109.47 3.000
LIG  H223     C22     C19         109.48 3.000
LIG  H222     C22     C19         109.47 3.000
LIG  H221     C22     C19         109.48 3.000
LIG  H233     C23    H232         109.47 3.000
LIG  H233     C23    H231         109.47 3.000
LIG  H232     C23    H231         109.46 3.000
LIG  H233     C23     C16         109.48 3.000
LIG  H232     C23     C16         109.47 3.000
LIG  H231     C23     C16         109.47 3.000
LIG  H243     C24    H242         109.47 3.000
LIG  H243     C24    H241         109.47 3.000
LIG  H242     C24    H241         109.47 3.000
LIG  H243     C24     C16         109.47 3.000
LIG  H242     C24     C16         109.48 3.000
LIG  H241     C24     C16         109.48 3.000
LIG  H251     O25     C15         109.48 3.000
LIG   C34     C26     C27         119.97 3.000
LIG   C34     C26     C10         119.83 3.000
LIG   C27     C26     C10         119.98 3.000
LIG  H271     C27     C28         119.97 3.000
LIG  H271     C27     C26         119.99 3.000
LIG   C28     C27     C26         119.99 3.000
LIG   N32     C28     C29         109.94 3.000
LIG   N32     C28     C27         125.03 3.000
LIG   C29     C28     C27         125.03 3.000
LIG  H291     C29     S30         125.11 3.000
LIG  H291     C29     C28         125.11 3.000
LIG   S30     C29     C28         109.77 3.000
LIG   C31     S30     C29          99.88 3.000
LIG   C33     C31     N32         125.10 3.000
LIG   C33     C31     S30         125.10 3.000
LIG   N32     C31     S30         109.80 3.000
LIG   C31     N32     C28         110.61 3.000
LIG  H333     C33    H332         109.47 3.000
LIG  H333     C33    H331         109.47 3.000
LIG  H332     C33    H331         109.47 3.000
LIG  H333     C33     C31         109.47 3.000
LIG  H332     C33     C31         109.47 3.000
LIG  H331     C33     C31         109.47 3.000
LIG  H343     C34    H342         109.47 3.000
LIG  H343     C34    H341         109.47 3.000
LIG  H342     C34    H341         109.47 3.000
LIG  H343     C34     C26         109.48 3.000
LIG  H342     C34     C26         109.47 3.000
LIG  H341     C34     C26         109.48 3.000
LIG  H353     C35    H352         109.47 3.000
LIG  H353     C35    H351         109.47 3.000
LIG  H352     C35    H351         109.47 3.000
LIG  H353     C35     C06         109.48 3.000
LIG  H352     C35     C06         109.48 3.000
LIG  H351     C35     C06         109.47 3.000
#
loop_
_chem_comp_tor.comp_id
_chem_comp_tor.id
_chem_comp_tor.atom_id_1
_chem_comp_tor.atom_id_2
_chem_comp_tor.atom_id_3
_chem_comp_tor.atom_id_4
_chem_comp_tor.value_angle
_chem_comp_tor.value_angle_esd
_chem_comp_tor.period
LIG CONST_01       C31     S30     C29     C28          -0.00   0.0 0
LIG CONST_02       N32     C31     S30     C29           0.00   0.0 0
LIG CONST_03       C28     N32     C31     S30           0.00   0.0 0
LIG CONST_04       C29     C28     N32     C31          -0.00   0.0 0
LIG CONST_05       S30     C29     C28     N32           0.00   0.0 0
LIG CONST_06       S30     C29     C28     C27        -179.54   0.0 0
LIG CONST_07       C31     N32     C28     C27         179.54   0.0 0
LIG CONST_08       C33     C31     N32     C28        -179.91   0.0 0
LIG CONST_09       C33     C31     S30     C29         179.91   0.0 0
LIG CONST_10      H291     C29     S30     C31        -180.00   0.0 0
LIG CONST_11       C28     C27     C26     C10         159.82   0.0 0
LIG CONST_12       C34     C26     C27     C28         -14.94   0.0 0
LIG CONST_13      H271     C27     C26     C10         -22.70   0.0 0
LIG Var_01         C05     C04     C03     C02         124.34  30.0 3
LIG Var_02         C06     C05     C04     C03          94.73  30.0 3
LIG Var_03         C14     C12     O11     C10        -151.88  30.0 3
LIG Var_04         C15     C14     C12     O11         151.66  30.0 3
LIG Var_05         C19     C17     C16     C15        -107.42  30.0 3
LIG Var_06         C29     C28     C27     C26         -88.36  30.0 2
LIG Var_07        H231     C23     C16     C15         -48.40  30.0 3
LIG Var_08        H241     C24     C16     C15          65.69  30.0 3
LIG Var_09        H331     C33     C31     S30        -127.80  30.0 2
LIG Var_10        H341     C34     C26     C10         127.88  30.0 3
#
loop_
_chem_comp_chir.comp_id
_chem_comp_chir.id
_chem_comp_chir.atom_id_centre
_chem_comp_chir.atom_id_1
_chem_comp_chir.atom_id_2
_chem_comp_chir.atom_id_3
_chem_comp_chir.volume_sign
LIG chir_01   C02     C01     C20     C03   positiv
LIG chir_02   C06     O08     C07     C35   positiv
LIG chir_03   C07     C06     O08     C09   negativ
LIG chir_04   C10     C09     O11     C26   positiv
LIG chir_05   C15     C14     C16     O25   negativ
LIG chir_06   C19     C17     C20     C22   negativ
LIG chir_07   C20     C19     C02     O21   positiv
#
loop_
_chem_comp_plane_atom.comp_id
_chem_comp_plane_atom.plane_id
_chem_comp_plane_atom.atom_id
_chem_comp_plane_atom.dist_esd
LIG plan-1    C27 0.020
LIG plan-1    C28 0.020
LIG plan-1    C29 0.020
LIG plan-1    S30 0.020
LIG plan-1    C31 0.020
LIG plan-1    N32 0.020
LIG plan-1    C33 0.020
LIG plan-1   H291 0.020
LIG plan-2    O11 0.020
LIG plan-2    C12 0.020
LIG plan-2    O13 0.020
LIG plan-2    C14 0.020
LIG plan-3    C16 0.020
LIG plan-3    C17 0.020
LIG plan-3    O18 0.020
LIG plan-3    C19 0.020
LIG plan-4    C10 0.020
LIG plan-4    C26 0.020
LIG plan-4    C27 0.020
LIG plan-4    C28 0.020
LIG plan-4    C34 0.020
LIG plan-4   H271 0.020
"""

pdb_str_poor = """\
CRYST1   19.599   17.955   21.862  90.00  90.00  90.00 P 1
HETATM    1  C01 LIG A   1      13.339   8.500  16.829  1.00 20.00           C
HETATM    2  C02 LIG A   1      13.882   7.955  15.948  1.00 20.00           C
HETATM    3  C03 LIG A   1      13.196   6.969  15.468  1.00 20.00           C
HETATM    4  C04 LIG A   1      12.143   6.705  14.458  1.00 20.00           C
HETATM    5  C05 LIG A   1      10.920   6.134  15.377  1.00 20.00           C
HETATM    6  C06 LIG A   1       9.966   7.152  16.175  1.00 20.00           C
HETATM    7  C07 LIG A   1       9.009   7.595  16.045  1.00 20.00           C
HETATM    8  C35 LIG A   1      10.452   6.478  17.700  1.00 20.00           C
HETATM    9  C09 LIG A   1       8.291   7.551  14.222  1.00 20.00           C
HETATM   10  C10 LIG A   1       8.856   8.227  13.327  1.00 20.00           C
HETATM   11  C12 LIG A   1       9.217  10.510  13.528  1.00 20.00           C
HETATM   12  C14 LIG A   1       9.342  11.762  14.505  1.00 20.00           C
HETATM   13  C15 LIG A   1      10.131  11.730  15.475  1.00 20.00           C
HETATM   14  C16 LIG A   1      11.649  12.479  15.814  1.00 20.00           C
HETATM   15  C17 LIG A   1      12.765  11.606  14.629  1.00 20.00           C
HETATM   16  C19 LIG A   1      13.588  10.772  15.295  1.00 20.00           C
HETATM   17  O08 LIG A   1      10.307   8.121  16.308  1.00 20.00           O
HETATM   18  O11 LIG A   1       8.379   9.897  14.269  1.00 20.00           O
HETATM   19  O13 LIG A   1       9.765  10.912  12.104  1.00 20.00           O
HETATM   20  C20 LIG A   1      13.172   9.359  14.423  1.00 20.00           C
HETATM   21  C22 LIG A   1      15.384  11.015  14.709  1.00 20.00           C
HETATM   22  O18 LIG A   1      12.961  11.814  13.495  1.00 20.00           O
HETATM   23  C23 LIG A   1      11.603  13.968  15.347  1.00 20.00           C
HETATM   24  O21 LIG A   1      13.829   9.226  13.389  1.00 20.00           O
HETATM   25  C24 LIG A   1      12.215  12.430  16.937  1.00 20.00           C
HETATM   26  O25 LIG A   1       9.627  12.253  16.580  1.00 20.00           O
HETATM   27  C26 LIG A   1       7.766   8.622  12.612  1.00 20.00           C
HETATM   28  C27 LIG A   1       8.076   8.034  11.482  1.00 20.00           C
HETATM   29  C28 LIG A   1       6.819   7.473  10.160  1.00 20.00           C
HETATM   30  C29 LIG A   1       5.969   6.438  10.008  1.00 20.00           C
HETATM   31  N32 LIG A   1       6.355   8.692   9.150  1.00 20.00           N
HETATM   32  C31 LIG A   1       5.500   8.271   8.355  1.00 20.00           C
HETATM   33  S30 LIG A   1       5.139   6.599   8.942  1.00 20.00           S
HETATM   34  C33 LIG A   1       4.916   8.836   7.131  1.00 20.00           C
HETATM   35  C34 LIG A   1       6.090   9.144  12.711  1.00 20.00           C
TER
END
"""

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  Handling unknown ligand and input CIF provided for it.
  """
  pdb_file_name_answer = "%s_answer.pdb"%prefix
  of=open(pdb_file_name_answer, "w")
  print(pdb_str_answer, file=of)
  of.close()
  #
  cif_file = "%s_ligand.cif"%prefix
  of=open(cif_file, "w")
  print(cif_str, file=of)
  of.close()
  #
  pdb_file_name_poor = "%s_poor.pdb"%prefix
  of=open(pdb_file_name_poor, "w")
  print(pdb_str_poor, file=of)
  of.close()
  #
  cmd = " ".join([
    "phenix.fmodel",
    "%s"%pdb_file_name_answer,
    "file_name=%s.mtz"%prefix,
    "high_res=3.0",
    ">%s.zlog"%prefix])
  assert easy_run.call(cmd)==0
  #
  args = [
    "%s"%pdb_file_name_poor,
    "%s"%cif_file,
    "%s.mtz"%prefix]
  r = run_real_space_refine(args = args, prefix = prefix)
  #
  f = open(r.log,"r")
  cc_final = None
  for l in f.readlines():
    l = l.strip()
    if(l.count("CC_mask:")):
      cc_final = float(l.split()[len(l.split())-1])
  assert cc_final >= 0.97, cc_final
  f.close()
  #
  x1 = iotbx.pdb.input(source_info=None,
    lines=pdb_str_answer).construct_hierarchy().extract_xray_structure()
  x2 = r.xrs
  d = x1.distances(x2)
  assert flex.max(d)<0.31, flex.max(d)
  assert flex.mean(d)<0.17, flex.mean(d)

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
