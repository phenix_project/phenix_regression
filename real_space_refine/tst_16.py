from __future__ import print_function
from libtbx import easy_run
import time
import iotbx.pdb
import os
from phenix_regression.real_space_refine import run_real_space_refine

pdb_str_1 = """\
CRYST1   34.891   15.321   22.435  90.00  90.00  90.00 P 1
ATOM      1  N   GLY A   1       7.277   9.706  12.570  1.00 16.77           N
ATOM      2  CA  GLY A   1       7.234   9.301  11.119  1.00 16.57           C
ATOM      3  C   GLY A   1       8.271   8.234  10.887  1.00 16.16           C
ATOM      4  O   GLY A   1       8.763   7.615  11.849  1.00 16.78           O
TER
END
"""

pdb_str_2 = """\
CRYST1   34.892   15.321   22.435  90.00  90.01  90.00 P 1
ATOM      1  N   GLY A   1       7.277   9.706  12.570  1.00 16.77           N
ATOM      2  CA  GLY A   1       7.234   9.301  11.119  1.00 16.57           C
ATOM      3  C   GLY A   1       8.271   8.234  10.887  1.00 16.16           C
ATOM      4  O   GLY A   1       8.763   7.615  11.849  1.00 16.78           O
TER
END
"""

pdb_str_3 = """\
ATOM      1  N   GLY A   1       7.277   9.706  12.570  1.00 16.77           N
ATOM      2  CA  GLY A   1       7.234   9.301  11.119  1.00 16.57           C
ATOM      3  C   GLY A   1       8.271   8.234  10.887  1.00 16.16           C
ATOM      4  O   GLY A   1       8.763   7.615  11.849  1.00 16.78           O
TER
END
"""

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  Make sure the program stops if crystal symmetry is inconsistent.
  Make sure input PDB with no CRYST1 will have one in output.
  """
  for i, pdb_str in enumerate([pdb_str_1, pdb_str_2]):
    pdb_file_name = "%s_%s.pdb"%(str(i), prefix)
    of=open(pdb_file_name, "w")
    print(pdb_str, file=of)
    of.close()
    #
    cmd = " ".join([
      "phenix.fmodel",
      "%s_%s.pdb"%(str(i), prefix),
      "high_res=2",
      "output.file_name=%s_%s.mtz"%(str(i), prefix),
      ">%s.zlog"%prefix])
    assert easy_run.call(cmd)==0
    #
    cmd = " ".join([
      "phenix.mtz2map",
      "%s_%s.mtz"%(str(i), prefix),
      "include_fmodel=true",
      "output.prefix=%s_%s"%(str(i), prefix),
      ">%s.zlog"%prefix])
    assert easy_run.call(cmd)==0
  #
  pdb_file_name = "%s_%s.pdb"%(str(2), prefix)
  of=open(pdb_file_name, "w")
  print(pdb_str_3, file=of)
  of.close()
  #
  opt1 = "0_%s.pdb 1_%s.mtz"%(prefix,prefix)
  opt2 = "0_%s.pdb 1_%s_fmodel.ccp4 resolution=2"%(prefix,prefix)
  opt3 = "2_%s.pdb 1_%s.mtz"%(prefix,prefix)
  opt4 = "2_%s.pdb 1_%s_fmodel.ccp4 resolution=2"%(prefix,prefix)
  cmds = ["phenix.real_space_refine %s "%(opt1),
          "phenix.real_space_refine %s "%(opt2)]
  print(cmds)
  STOP()
  for cmd in cmds:
    print(cmd)
    r = easy_run.fully_buffered(cmd)
    assert ("Sorry: Model is not similar to 'None': " in r.stdout_lines ) or \
    ("Sorry: Model is not similar to '1_tst_16_real_space_refine_fmodel.ccp4': "
      in r.stdout_lines)
  #
  cmds = ["phenix.real_space_refine macro_cycles=1 %s >zlog.%s"%(opt3,prefix),
          "phenix.real_space_refine macro_cycles=1 %s >zlog.%s"%(opt4,prefix)]
  for cmd in cmds:
    assert easy_run.call(cmd)==0
    r1 = easy_run.go(command="grep 'CRYST1' "+
      "2_%s_real_space_refined.pdb"%prefix).stdout_lines[0]
    r2 = easy_run.go(command="grep 'CRYST1' "+
      "1_%s.pdb"%prefix).stdout_lines[0]
    assert r1==r2

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
