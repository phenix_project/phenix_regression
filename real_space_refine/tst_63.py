from __future__ import print_function
import time
from libtbx import easy_run
import iotbx.pdb
from scitbx.array_family import flex
from scitbx.math import superpose
import os
import libtbx.load_env
from phenix_regression.real_space_refine import run_real_space_refine
from libtbx.test_utils import assert_lines_in_file
from libtbx.test_utils import approx_equal, not_approx_equal

pdb_str = """
CRYST1   34.891   15.321   22.435  90.00  90.00  90.00 P 1
SCALE1      0.028661  0.000000  0.000000        0.00000
SCALE2      0.000000  0.065270  0.000000        0.00000
SCALE3      0.000000  0.000000  0.044573        0.00000
ATOM      1  N   GLY A   1       7.277   9.706  12.570  1.00 16.77           N
ATOM      2  CA  GLY A   1       7.234   9.301  11.119  1.00 16.57           C
ATOM      3  C   GLY A   1       8.271   8.234  10.887  1.00 16.16           C
ATOM      4  O   GLY A   1       8.763   7.615  11.849  1.00 16.78           O
ATOM      5  N   ASN A   2       8.630   8.017   9.623  1.00 15.02           N
ATOM      6  CA  ASN A   2       9.764   7.132   9.299  1.00 14.10           C
ATOM      7  C   ASN A   2      11.045   7.631   9.895  1.00 13.13           C
ATOM      8  O   ASN A   2      11.308   8.836   9.894  1.00 11.91           O
ATOM      9  CB  ASN A   2       9.940   6.975   7.809  1.00 15.38           C
ATOM     10  CG  ASN A   2       8.702   6.436   7.160  1.00 14.08           C
ATOM     11  OD1 ASN A   2       8.261   5.321   7.484  1.00 17.46           O
ATOM     12  ND2 ASN A   2       8.082   7.249   6.299  1.00 11.72           N
END
"""

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  Handle rotamers.restraints.sigma=0.5 correctly
  """
  #
  pdb_file = "%s.pdb"%prefix
  with open(pdb_file,"w") as fo:
    fo.write(pdb_str)
  #
  mtz_file = "%s.mtz"%prefix
  cmd = " ".join([
    "phenix.fmodel",
    "%s"%pdb_file,
    "file_name=%s"%mtz_file,
    "high_res=3.0",
    ">%s.zlog"%prefix])
  assert easy_run.call(cmd)==0
  #
  args = [pdb_file, mtz_file, "macro_cycles=3", "rotamers.restraints.sigma=0.5"]
  r = run_real_space_refine(args = args, prefix = prefix)
  #
  cntr = 0
  with open(r.log,"r") as fo:
    for l in fo.readlines():
      if(l.count("Rotamers are restrained with sigma=0.50")):
        cntr+=1
  assert cntr==3, cntr

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK time =%8.3f"%(time.time() - t0))
