from __future__ import print_function
import time, os
from libtbx import easy_run
import iotbx.pdb
import libtbx.load_env
from phenix_regression.real_space_refine import run_real_space_refine

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  Cope with user-selection that isn't great but not entirely wrong.
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/real_space_refine/data/tst_20.pdb",
    test=os.path.isfile)
  mtz = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/real_space_refine/data/tst_20.mtz",
    test=os.path.isfile)
  params = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/real_space_refine/data/tst_20.eff",
    test=os.path.isfile)
  #
  args = [
    "%s"%pdb,
    "%s"%mtz,
    "%s"%params,
    "macro_cycles=0",
    "ramachandran_plot_restraints.enabled=false",
    "rotamers.restraints.enabled=False",
    "secondary_structure.enabled=false"]
  r = run_real_space_refine(args = args, prefix = prefix)

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK time =%8.3f"%(time.time() - t0))
