from __future__ import print_function
import time, os
from libtbx import easy_run
import iotbx.pdb
import libtbx.load_env
from scitbx.array_family import flex
from phenix_regression.real_space_refine import run_real_space_refine

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  Rigid-body refinement with rigid groups found automatically.
  """
  pdb_in = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/real_space_refine/data/tst_39.pdb",
    test=os.path.isfile)
  map_in = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/real_space_refine/data/tst_39.ccp4",
    test=os.path.isfile)
  #
  args = [
    "%s"%pdb_in,
    "%s"%map_in,
    "run=rigid_body",
    "macro_cycles=1",
    # SS annotations are corrupt in tst_39.pdb and have nothing to do with
    # this particular test. Therefore turning SS restraints off.
    "secondary_structure.enabled=False",
    "resolution=4.35"]
  r = run_real_space_refine(args = args, prefix = prefix)
  # check for expected output
  fo = open(r.log,"r")
  shifts = flex.double()
  for l in fo.readlines():
    l = l.strip()
    if(l.startswith("moved from start")):
      l = l.split()
      shifts.append(float(l[-1]))
  assert flex.max(shifts) < 0.1



if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK time =%8.3f"%(time.time() - t0))
