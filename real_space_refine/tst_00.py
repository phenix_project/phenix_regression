from __future__ import print_function
from libtbx import easy_run
import time
import iotbx.pdb
from scitbx.array_family import flex
from libtbx.test_utils import show_diff
import os
from phenix_regression.real_space_refine import run_real_space_refine

pdb_str_answer = """\
CRYST1   27.475   35.191   27.490  90.00  90.00  90.00 P 21 21 21
HELIX    1   1 ALA E    1  ALA E   16  1                                  16
ATOM      1  N   ALA E   1       6.709  31.817   5.664  1.00 20.00           N
ATOM      2  CA  ALA E   1       7.794  30.914   6.029  1.00 20.00           C
ATOM      3  C   ALA E   1       7.267  29.691   6.773  1.00 20.00           C
ATOM      4  O   ALA E   1       7.942  29.146   7.647  1.00 20.00           O
ATOM      5  CB  ALA E   1       8.831  31.643   6.870  1.00 20.00           C
ATOM      6  N   HIS E   2       6.057  29.267   6.421  1.00 20.00           N
ATOM      7  CA  HIS E   2       5.434  28.110   7.053  1.00 20.00           C
ATOM      8  C   HIS E   2       6.176  26.824   6.701  1.00 20.00           C
ATOM      9  O   HIS E   2       6.477  26.012   7.576  1.00 20.00           O
ATOM     10  CB  HIS E   2       3.965  28.000   6.640  1.00 20.00           C
ATOM     11  CG  HIS E   2       3.140  29.191   7.016  1.00 20.00           C
ATOM     12  ND1 HIS E   2       2.481  29.289   8.222  1.00 20.00           N
ATOM     13  CD2 HIS E   2       2.869  30.335   6.345  1.00 20.00           C
ATOM     14  CE1 HIS E   2       1.838  30.441   8.278  1.00 20.00           C
ATOM     15  NE2 HIS E   2       2.057  31.096   7.152  1.00 20.00           N
ATOM     16  N   CYS E   3       6.468  26.648   5.417  1.00 20.00           N
ATOM     17  CA  CYS E   3       7.193  25.473   4.949  1.00 20.00           C
ATOM     18  C   CYS E   3       8.635  25.492   5.443  1.00 20.00           C
ATOM     19  O   CYS E   3       9.223  24.444   5.712  1.00 20.00           O
ATOM     20  CB  CYS E   3       7.161  25.396   3.421  1.00 20.00           C
ATOM     21  SG  CYS E   3       5.501  25.285   2.714  1.00 20.00           S
ATOM     22  N   ALA E   4       9.197  26.690   5.560  1.00 20.00           N
ATOM     23  CA  ALA E   4      10.560  26.855   6.050  1.00 20.00           C
ATOM     24  C   ALA E   4      10.655  26.475   7.523  1.00 20.00           C
ATOM     25  O   ALA E   4      11.599  25.805   7.942  1.00 20.00           O
ATOM     26  CB  ALA E   4      11.031  28.285   5.837  1.00 20.00           C
ATOM     27  N   ILE E   5       9.669  26.907   8.304  1.00 20.00           N
ATOM     28  CA  ILE E   5       9.621  26.584   9.725  1.00 20.00           C
ATOM     29  C   ILE E   5       9.325  25.103   9.932  1.00 20.00           C
ATOM     30  O   ILE E   5       9.836  24.482  10.865  1.00 20.00           O
ATOM     31  CB  ILE E   5       8.580  27.459  10.467  1.00 20.00           C
ATOM     32  CG1 ILE E   5       9.071  28.905  10.559  1.00 20.00           C
ATOM     33  CG2 ILE E   5       8.307  26.928  11.866  1.00 20.00           C
ATOM     34  CD1 ILE E   5       8.105  29.836  11.259  1.00 20.00           C
ATOM     35  N   TYR E   6       8.498  24.543   9.054  1.00 20.00           N
ATOM     36  CA  TYR E   6       8.157  23.127   9.116  1.00 20.00           C
ATOM     37  C   TYR E   6       9.375  22.264   8.811  1.00 20.00           C
ATOM     38  O   TYR E   6       9.598  21.239   9.454  1.00 20.00           O
ATOM     39  CB  TYR E   6       7.017  22.795   8.150  1.00 20.00           C
ATOM     40  CG  TYR E   6       5.662  23.291   8.603  1.00 20.00           C
ATOM     41  CD1 TYR E   6       5.434  23.633   9.930  1.00 20.00           C
ATOM     42  CD2 TYR E   6       4.610  23.413   7.705  1.00 20.00           C
ATOM     43  CE1 TYR E   6       4.197  24.086  10.349  1.00 20.00           C
ATOM     44  CE2 TYR E   6       3.369  23.865   8.114  1.00 20.00           C
ATOM     45  CZ  TYR E   6       3.169  24.200   9.437  1.00 20.00           C
ATOM     46  OH  TYR E   6       1.935  24.650   9.849  1.00 20.00           O
ATOM     47  N   THR E   7      10.161  22.688   7.825  1.00 20.00           N
ATOM     48  CA  THR E   7      11.380  21.976   7.459  1.00 20.00           C
ATOM     49  C   THR E   7      12.441  22.134   8.541  1.00 20.00           C
ATOM     50  O   THR E   7      13.199  21.204   8.823  1.00 20.00           O
ATOM     51  CB  THR E   7      11.948  22.456   6.109  1.00 20.00           C
ATOM     52  OG1 THR E   7      11.987  23.888   6.086  1.00 20.00           O
ATOM     53  CG2 THR E   7      11.086  21.957   4.959  1.00 20.00           C
ATOM     54  N   ILE E   8      12.489  23.318   9.145  1.00 20.00           N
ATOM     55  CA  ILE E   8      13.433  23.596  10.220  1.00 20.00           C
ATOM     56  C   ILE E   8      13.128  22.736  11.441  1.00 20.00           C
ATOM     57  O   ILE E   8      14.038  22.232  12.099  1.00 20.00           O
ATOM     58  CB  ILE E   8      13.437  25.097  10.601  1.00 20.00           C
ATOM     59  CG1 ILE E   8      14.259  25.898   9.591  1.00 20.00           C
ATOM     60  CG2 ILE E   8      14.003  25.309  11.997  1.00 20.00           C
ATOM     61  CD1 ILE E   8      14.305  27.383   9.880  1.00 20.00           C
ATOM     62  N   HIS E   9      11.843  22.573  11.737  1.00 20.00           N
ATOM     63  CA  HIS E   9      11.415  21.732  12.848  1.00 20.00           C
ATOM     64  C   HIS E   9      11.639  20.260  12.519  1.00 20.00           C
ATOM     65  O   HIS E   9      11.974  19.460  13.395  1.00 20.00           O
ATOM     66  CB  HIS E   9       9.943  21.982  13.182  1.00 20.00           C
ATOM     67  CG  HIS E   9       9.681  23.326  13.789  1.00 20.00           C
ATOM     68  ND1 HIS E   9      10.679  24.249  14.013  1.00 20.00           N
ATOM     69  CD2 HIS E   9       8.534  23.900  14.221  1.00 20.00           C
ATOM     70  CE1 HIS E   9      10.158  25.335  14.556  1.00 20.00           C
ATOM     71  NE2 HIS E   9       8.858  25.149  14.693  1.00 20.00           N
ATOM     72  N   SER E  10      11.450  19.912  11.250  1.00 20.00           N
ATOM     73  CA  SER E  10      11.664  18.546  10.785  1.00 20.00           C
ATOM     74  C   SER E  10      13.125  18.147  10.944  1.00 20.00           C
ATOM     75  O   SER E  10      13.431  17.029  11.357  1.00 20.00           O
ATOM     76  CB  SER E  10      11.237  18.394   9.324  1.00 20.00           C
ATOM     77  OG  SER E  10      12.163  19.021   8.455  1.00 20.00           O
ATOM     78  N   VAL E  11      14.025  19.068  10.613  1.00 20.00           N
ATOM     79  CA  VAL E  11      15.454  18.835  10.782  1.00 20.00           C
ATOM     80  C   VAL E  11      15.820  18.850  12.262  1.00 20.00           C
ATOM     81  O   VAL E  11      16.689  18.095  12.705  1.00 20.00           O
ATOM     82  CB  VAL E  11      16.292  19.885  10.015  1.00 20.00           C
ATOM     83  CG1 VAL E  11      17.776  19.723  10.312  1.00 20.00           C
ATOM     84  CG2 VAL E  11      16.036  19.776   8.520  1.00 20.00           C
ATOM     85  N   ASP E  12      15.147  19.714  13.017  1.00 20.00           N
ATOM     86  CA  ASP E  12      15.358  19.816  14.457  1.00 20.00           C
ATOM     87  C   ASP E  12      15.062  18.486  15.137  1.00 20.00           C
ATOM     88  O   ASP E  12      15.780  18.073  16.046  1.00 20.00           O
ATOM     89  CB  ASP E  12      14.495  20.925  15.064  1.00 20.00           C
ATOM     90  CG  ASP E  12      15.107  22.301  14.890  1.00 20.00           C
ATOM     91  OD1 ASP E  12      16.350  22.397  14.824  1.00 20.00           O
ATOM     92  OD2 ASP E  12      14.344  23.288  14.820  1.00 20.00           O
ATOM     93  N   ALA E  13      14.001  17.822  14.690  1.00 20.00           N
ATOM     94  CA  ALA E  13      13.688  16.481  15.167  1.00 20.00           C
ATOM     95  C   ALA E  13      14.741  15.506  14.655  1.00 20.00           C
ATOM     96  O   ALA E  13      15.423  14.826  15.444  1.00 20.00           O
ATOM     97  CB  ALA E  13      12.303  16.062  14.706  1.00 20.00           C
ATOM     98  N   PHE E  14      14.885  15.479  13.328  1.00 20.00           N
ATOM     99  CA  PHE E  14      15.842  14.623  12.623  1.00 20.00           C
ATOM    100  C   PHE E  14      17.192  14.548  13.323  1.00 20.00           C
ATOM    101  O   PHE E  14      17.881  13.533  13.245  1.00 20.00           O
ATOM    102  CB  PHE E  14      16.026  15.083  11.174  1.00 20.00           C
ATOM    103  CG  PHE E  14      14.911  14.666  10.258  1.00 20.00           C
ATOM    104  CD1 PHE E  14      14.035  13.657  10.624  1.00 20.00           C
ATOM    105  CD2 PHE E  14      14.739  15.281   9.029  1.00 20.00           C
ATOM    106  CE1 PHE E  14      13.008  13.271   9.783  1.00 20.00           C
ATOM    107  CE2 PHE E  14      13.714  14.900   8.183  1.00 20.00           C
ATOM    108  CZ  PHE E  14      12.848  13.893   8.561  1.00 20.00           C
ATOM    109  N   ALA E  15      17.558  15.630  14.003  1.00 20.00           N
ATOM    110  CA  ALA E  15      18.699  15.619  14.904  1.00 20.00           C
ATOM    111  C   ALA E  15      18.268  15.190  16.307  1.00 20.00           C
ATOM    112  O   ALA E  15      19.035  14.538  17.024  1.00 20.00           O
ATOM    113  CB  ALA E  15      19.357  16.988  14.944  1.00 20.00           C
ATOM    114  N   GLU E  16      17.036  15.542  16.687  1.00 20.00           N
ATOM    115  CA  GLU E  16      16.549  15.284  18.047  1.00 20.00           C
ATOM    116  C   GLU E  16      16.685  13.831  18.518  1.00 20.00           C
ATOM    117  O   GLU E  16      17.064  13.664  19.676  1.00 20.00           O
ATOM    118  CB  GLU E  16      15.119  15.792  18.273  1.00 20.00           C
ATOM    119  CG  GLU E  16      15.040  17.145  18.965  1.00 20.00           C
ATOM    120  CD  GLU E  16      13.612  17.612  19.170  1.00 20.00           C
ATOM    121  OE1 GLU E  16      12.682  16.879  18.772  1.00 20.00           O
ATOM    122  OE2 GLU E  16      13.420  18.711  19.730  1.00 20.00           O
TER
"""

pdb_str_poor = """\
CRYST1   27.475   35.191   27.490  90.00  90.00  90.00 P 21 21 21
HELIX    1   1 ALA E    1  ALA E   16  1                                  16
ATOM      1  N   ALA E   1       9.565  32.109   5.832  1.00 20.00           N
ATOM      2  CA  ALA E   1      10.697  31.218   6.046  1.00 20.00           C
ATOM      3  C   ALA E   1      10.233  29.893   6.638  1.00 20.00           C
ATOM      4  O   ALA E   1      11.003  29.198   7.300  1.00 20.00           O
ATOM      5  CB  ALA E   1      11.728  31.876   6.952  1.00 20.00           C
ATOM      6  N   HIS E   2       8.970  29.550   6.392  1.00 20.00           N
ATOM      7  CA  HIS E   2       8.390  28.309   6.899  1.00 20.00           C
ATOM      8  C   HIS E   2       9.121  27.118   6.306  1.00 20.00           C
ATOM      9  O   HIS E   2       9.209  26.051   6.913  1.00 20.00           O
ATOM     10  CB  HIS E   2       6.900  28.225   6.564  1.00 20.00           C
ATOM     11  CG  HIS E   2       6.624  27.962   5.117  1.00 20.00           C
ATOM     12  ND1 HIS E   2       6.634  26.695   4.574  1.00 20.00           N
ATOM     13  CD2 HIS E   2       6.347  28.806   4.095  1.00 20.00           C
ATOM     14  CE1 HIS E   2       6.367  26.769   3.283  1.00 20.00           C
ATOM     15  NE2 HIS E   2       6.188  28.039   2.967  1.00 20.00           N
ATOM     16  N   CYS E   3       9.629  27.318   5.099  1.00 20.00           N
ATOM     17  CA  CYS E   3      10.416  26.320   4.406  1.00 20.00           C
ATOM     18  C   CYS E   3      11.760  26.164   5.118  1.00 20.00           C
ATOM     19  O   CYS E   3      12.281  25.058   5.252  1.00 20.00           O
ATOM     20  CB  CYS E   3      10.603  26.734   2.942  1.00 20.00           C
ATOM     21  SG  CYS E   3      10.808  25.412   1.725  1.00 20.00           S
ATOM     22  N   ALA E   4      12.302  27.281   5.595  1.00 20.00           N
ATOM     23  CA  ALA E   4      13.629  27.294   6.206  1.00 20.00           C
ATOM     24  C   ALA E   4      13.595  26.798   7.640  1.00 20.00           C
ATOM     25  O   ALA E   4      14.564  26.230   8.141  1.00 20.00           O
ATOM     26  CB  ALA E   4      14.218  28.693   6.157  1.00 20.00           C
ATOM     27  N   ILE E   5      12.474  27.025   8.306  1.00 20.00           N
ATOM     28  CA  ILE E   5      12.366  26.648   9.697  1.00 20.00           C
ATOM     29  C   ILE E   5      11.977  25.184   9.831  1.00 20.00           C
ATOM     30  O   ILE E   5      12.180  24.593  10.889  1.00 20.00           O
ATOM     31  CB  ILE E   5      11.359  27.535  10.442  1.00 20.00           C
ATOM     32  CG1 ILE E   5       9.948  27.326   9.891  1.00 20.00           C
ATOM     33  CG2 ILE E   5      11.782  28.995  10.340  1.00 20.00           C
ATOM     34  CD1 ILE E   5       9.000  26.651  10.863  1.00 20.00           C
ATOM     35  N   TYR E   6      11.439  24.577   8.775  1.00 20.00           N
ATOM     36  CA  TYR E   6      11.087  23.170   8.919  1.00 20.00           C
ATOM     37  C   TYR E   6      12.354  22.336   8.767  1.00 20.00           C
ATOM     38  O   TYR E   6      12.571  21.401   9.540  1.00 20.00           O
ATOM     39  CB  TYR E   6       9.929  22.742   7.971  1.00 20.00           C
ATOM     40  CG  TYR E   6      10.123  22.416   6.484  1.00 20.00           C
ATOM     41  CD1 TYR E   6      10.884  21.328   6.060  1.00 20.00           C
ATOM     42  CD2 TYR E   6       9.408  23.118   5.513  1.00 20.00           C
ATOM     43  CE1 TYR E   6      10.997  21.010   4.707  1.00 20.00           C
ATOM     44  CE2 TYR E   6       9.514  22.806   4.162  1.00 20.00           C
ATOM     45  CZ  TYR E   6      10.310  21.750   3.766  1.00 20.00           C
ATOM     46  OH  TYR E   6      10.431  21.420   2.433  1.00 20.00           O
ATOM     47  N   THR E   7      13.221  22.741   7.841  1.00 20.00           N
ATOM     48  CA  THR E   7      14.491  22.064   7.595  1.00 20.00           C
ATOM     49  C   THR E   7      15.486  22.251   8.747  1.00 20.00           C
ATOM     50  O   THR E   7      16.271  21.348   9.034  1.00 20.00           O
ATOM     51  CB  THR E   7      15.136  22.550   6.258  1.00 20.00           C
ATOM     52  OG1 THR E   7      16.089  21.587   5.789  1.00 20.00           O
ATOM     53  CG2 THR E   7      15.810  23.918   6.409  1.00 20.00           C
ATOM     54  N   ILE E   8      15.456  23.405   9.417  1.00 20.00           N
ATOM     55  CA  ILE E   8      16.392  23.629  10.518  1.00 20.00           C
ATOM     56  C   ILE E   8      15.851  22.984  11.761  1.00 20.00           C
ATOM     57  O   ILE E   8      16.576  22.787  12.732  1.00 20.00           O
ATOM     58  CB  ILE E   8      16.664  25.118  10.824  1.00 20.00           C
ATOM     59  CG1 ILE E   8      15.411  25.811  11.375  1.00 20.00           C
ATOM     60  CG2 ILE E   8      17.279  25.816   9.622  1.00 20.00           C
ATOM     61  CD1 ILE E   8      15.527  26.222  12.846  1.00 20.00           C
ATOM     62  N   HIS E   9      14.566  22.661  11.748  1.00 20.00           N
ATOM     63  CA  HIS E   9      14.083  21.804  12.803  1.00 20.00           C
ATOM     64  C   HIS E   9      14.211  20.361  12.281  1.00 20.00           C
ATOM     65  O   HIS E   9      13.914  19.410  12.999  1.00 20.00           O
ATOM     66  CB  HIS E   9      12.665  22.235  13.291  1.00 20.00           C
ATOM     67  CG  HIS E   9      11.492  21.612  12.584  1.00 20.00           C
ATOM     68  ND1 HIS E   9      11.454  20.295  12.181  1.00 20.00           N
ATOM     69  CD2 HIS E   9      10.268  22.122  12.297  1.00 20.00           C
ATOM     70  CE1 HIS E   9      10.282  20.034  11.630  1.00 20.00           C
ATOM     71  NE2 HIS E   9       9.542  21.125  11.692  1.00 20.00           N
ATOM     72  N   SER E  10      14.714  20.213  11.049  1.00 20.00           N
ATOM     73  CA  SER E  10      14.934  18.894  10.427  1.00 20.00           C
ATOM     74  C   SER E  10      16.417  18.545  10.265  1.00 20.00           C
ATOM     75  O   SER E  10      16.755  17.405   9.934  1.00 20.00           O
ATOM     76  CB  SER E  10      14.270  18.810   9.045  1.00 20.00           C
ATOM     77  OG  SER E  10      12.863  18.643   9.112  1.00 20.00           O
ATOM     78  N   VAL E  11      17.286  19.539  10.439  1.00 20.00           N
ATOM     79  CA  VAL E  11      18.721  19.304  10.564  1.00 20.00           C
ATOM     80  C   VAL E  11      19.096  19.367  12.040  1.00 20.00           C
ATOM     81  O   VAL E  11      20.246  19.121  12.404  1.00 20.00           O
ATOM     82  CB  VAL E  11      19.573  20.337   9.800  1.00 20.00           C
ATOM     83  CG1 VAL E  11      19.110  20.478   8.359  1.00 20.00           C
ATOM     84  CG2 VAL E  11      19.529  21.672  10.516  1.00 20.00           C
ATOM     85  N   ASP E  12      18.132  19.741  12.884  1.00 20.00           N
ATOM     86  CA  ASP E  12      18.325  19.664  14.334  1.00 20.00           C
ATOM     87  C   ASP E  12      17.677  18.368  14.765  1.00 20.00           C
ATOM     88  O   ASP E  12      17.914  17.854  15.862  1.00 20.00           O
ATOM     89  CB  ASP E  12      17.771  20.913  15.075  1.00 20.00           C
ATOM     90  CG  ASP E  12      16.296  20.796  15.540  1.00 20.00           C
ATOM     91  OD1 ASP E  12      15.820  19.738  16.013  1.00 20.00           O
ATOM     92  OD2 ASP E  12      15.612  21.841  15.491  1.00 20.00           O
ATOM     93  N   ALA E  13      16.840  17.857  13.874  1.00 20.00           N
ATOM     94  CA  ALA E  13      16.309  16.519  14.004  1.00 20.00           C
ATOM     95  C   ALA E  13      17.404  15.463  13.852  1.00 20.00           C
ATOM     96  O   ALA E  13      17.460  14.534  14.648  1.00 20.00           O
ATOM     97  CB  ALA E  13      15.214  16.288  12.987  1.00 20.00           C
ATOM     98  N   PHE E  14      18.280  15.586  12.852  1.00 20.00           N
ATOM     99  CA  PHE E  14      19.182  14.465  12.564  1.00 20.00           C
ATOM    100  C   PHE E  14      20.413  14.415  13.483  1.00 20.00           C
ATOM    101  O   PHE E  14      21.311  13.587  13.303  1.00 20.00           O
ATOM    102  CB  PHE E  14      19.589  14.449  11.059  1.00 20.00           C
ATOM    103  CG  PHE E  14      20.514  15.581  10.589  1.00 20.00           C
ATOM    104  CD1 PHE E  14      21.429  16.215  11.422  1.00 20.00           C
ATOM    105  CD2 PHE E  14      20.462  15.983   9.261  1.00 20.00           C
ATOM    106  CE1 PHE E  14      22.257  17.217  10.942  1.00 20.00           C
ATOM    107  CE2 PHE E  14      21.281  16.988   8.780  1.00 20.00           C
ATOM    108  CZ  PHE E  14      22.179  17.605   9.621  1.00 20.00           C
ATOM    109  N   ALA E  15      20.440  15.285  14.483  1.00 20.00           N
ATOM    110  CA  ALA E  15      21.666  15.502  15.238  1.00 20.00           C
ATOM    111  C   ALA E  15      21.598  14.971  16.663  1.00 20.00           C
ATOM    112  O   ALA E  15      22.595  14.481  17.196  1.00 20.00           O
ATOM    113  CB  ALA E  15      22.001  16.980  15.253  1.00 20.00           C
ATOM    114  N   GLU E  16      20.411  15.073  17.258  1.00 20.00           N
ATOM    115  CA  GLU E  16      20.183  14.786  18.672  1.00 20.00           C
ATOM    116  C   GLU E  16      20.082  13.292  18.971  1.00 20.00           C
ATOM    117  O   GLU E  16      20.192  12.873  20.126  1.00 20.00           O
ATOM    118  CB  GLU E  16      18.906  15.493  19.146  1.00 20.00           C
ATOM    119  CG  GLU E  16      17.661  14.603  19.156  1.00 20.00           C
ATOM    120  CD  GLU E  16      16.559  15.073  18.210  1.00 20.00           C
ATOM    121  OE1 GLU E  16      16.223  16.280  18.203  1.00 20.00           O
ATOM    122  OE2 GLU E  16      16.018  14.220  17.474  1.00 20.00           O
TER
"""

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  Exercise phenix.real_space_refine with all defaults, no H.
  """
  pdb_file_name_answer = "%s_answer.pdb"%prefix
  of=open(pdb_file_name_answer, "w")
  print(pdb_str_answer, file=of)
  of.close()
  #
  pdb_file_name_poor = "%s_poor.pdb"%prefix
  of=open(pdb_file_name_poor, "w")
  print(pdb_str_poor, file=of)
  of.close()
  #
  cmd = " ".join([
    "phenix.fmodel",
    "%s"%pdb_file_name_answer,
    "file_name=%s.mtz"%prefix,
    "high_res=2",
    ">%s.zlog"%prefix])
  assert easy_run.call(cmd)==0
  #
  args = [
    "run=minimization_global+local_grid_search+morphing",
    "target_bonds_rmsd=0.015",
    "target_angles_rmsd=1.5",
    "ramachandran_plot_restraints.enabled=False",
    "%s"%pdb_file_name_poor,
    "%s.mtz"%prefix]
  r = run_real_space_refine(args = args, prefix = prefix)
  #
  xrs_answer = iotbx.pdb.input(file_name=
    pdb_file_name_answer).xray_structure_simple()
  xrs_poor = iotbx.pdb.input(file_name=
    pdb_file_name_poor).xray_structure_simple()
  xrs_refined = r.xrs
  #
  dap = xrs_answer.distances(xrs_poor)
  dar = xrs_answer.distances(xrs_refined)
  #
  assert flex.mean(dap) > 3.8
  assert flex.mean(dar) < 0.2, flex.mean(dar)

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
