from __future__ import print_function
import time, math
from libtbx import easy_run
import iotbx.pdb
from scitbx.array_family import flex
from libtbx.test_utils import approx_equal
import os
from phenix_regression.real_space_refine import run_real_space_refine

pdb_str_0="""
CRYST1   14.819   24.824   14.332  90.00  90.00  90.00 P 1
ATOM     83  N   GLY A   1       6.309   4.741   7.903  1.00  9.50           N
ATOM     84  CA  GLY A   1       6.940   5.587   8.898  1.00 11.77           C
ATOM     85  C   GLY A   1       7.731   6.735   8.306  1.00 11.01           C
ATOM     86  O   GLY A   1       7.506   7.904   8.642  1.00 12.88           O
ATOM     90  N   GLY A   2       8.673   6.398   7.423  1.00 12.40           N
ATOM     93  C   GLY A   2       8.477   8.400   6.008  1.00 11.60           C
ATOM     94  O   GLY A   2       8.781   9.591   5.884  1.00 11.27           O
ATOM     91  CA  GLY A   2       9.404   7.432   6.712  1.00 12.69           C
ATOM    103  N   GLY A   3       7.329   7.904   5.547  1.00 11.26           N
ATOM    104  CA  GLY A   3       6.309   8.796   5.033  1.00 10.37           C
ATOM    105  C   GLY A   3       5.890   9.827   6.059  1.00  9.88           C
ATOM    106  O   GLY A   3       5.809  11.020   5.757  1.00 10.25           O
ATOM    110  N   GLY A   4       5.639   9.386   7.291  1.00  8.62           N
ATOM    111  CA  GLY A   4       5.283  10.327   8.335  1.00  8.63           C
ATOM    112  C   GLY A   4       6.383  11.330   8.596  1.00  8.81           C
ATOM    113  O   GLY A   4       6.140  12.542   8.614  1.00  8.24           O
ATOM    103  N   GLY A   5       7.614  10.845   8.777  1.00 11.26           N
ATOM    104  CA  GLY A   5       8.718  11.745   9.055  1.00 10.37           C
ATOM    105  C   GLY A   5       8.883  12.804   7.983  1.00  9.88           C
ATOM    106  O   GLY A   5       8.894  14.004   8.278  1.00 10.25           O
ATOM    103  N   GLY A   6       8.995  12.372   6.727  1.00 11.26           N
ATOM    104  CA  GLY A   6       9.105  13.329   5.641  1.00 10.37           C
ATOM    105  C   GLY A   6       7.933  14.283   5.600  1.00  9.88           C
ATOM    106  O   GLY A   6       8.114  15.496   5.458  1.00 10.25           O
ATOM    110  N   GLY A   7       6.715  13.751   5.736  1.00  8.62           N
ATOM    111  CA  GLY A   7       5.543  14.600   5.780  1.00  8.63           C
ATOM    112  C   GLY A   7       5.668  15.723   6.780  1.00  8.81           C
ATOM    113  O   GLY A   7       5.505  16.892   6.418  1.00  8.24           O
ATOM    118  N   GLY A   8       5.974  15.395   8.033  1.00  9.28           N
ATOM    119  CA  GLY A   8       6.230  16.414   9.025  1.00 10.02           C
ATOM    120  C   GLY A   8       7.260  17.404   8.528  1.00  9.51           C
ATOM    121  O   GLY A   8       7.074  18.619   8.645  1.00 10.70           O
ATOM    127  N   GLY A   9       8.339  16.896   7.944  1.00  9.78           N
ATOM    128  CA  GLY A   9       9.311  17.768   7.317  1.00 11.03           C
ATOM    129  C   GLY A   9       8.767  18.492   6.098  1.00  9.43           C
ATOM    130  O   GLY A   9       8.132  19.541   6.211  1.00 10.28           O
TER
END
"""

pdb_str_1="""
HELIX    1   1 GLY A    1  GLY A    9  1                                   9
%s
"""%pdb_str_0

def check_hbonds(file_name):
  pairs = [("1","5"),("2","6"),("3","7"),("4","8"),("5","9")]
  ph = iotbx.pdb.input(file_name=file_name).construct_hierarchy()
  asc = ph.atom_selection_cache()
  sites_cart = ph.atoms().extract_xyz()
  result = flex.double()
  for p in pairs:
    o = asc.selection("resseq %s and name O"%p[0])
    n = asc.selection("resseq %s and name N"%p[1])
    ro = sites_cart.select(o)[0]
    rn = sites_cart.select(n)[0]
    d = math.sqrt((ro[0]-rn[0])**2+(ro[1]-rn[1])**2+(ro[2]-rn[2])**2)
    result.append(d)
  return result

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  Make sure SS restraints defined for full model via HELIX records and via
  automatic search using from_ca work correctly and produce identical results.
  Compare with if no SS used at all, in which case SS is distorted.
  """
  # output pdb files
  for i, pdb_str in enumerate([pdb_str_0, pdb_str_1]):
    pdb_fn = "%s_%s.pdb"%(prefix, str(i))
    fo = open(pdb_fn,"w")
    print(pdb_str, file=fo)
    fo.close()
  # use one of the two to make up fobs
  cmd = " ".join([
    "phenix.fmodel",
    "%s"%pdb_fn,
    "file_name=%s.mtz"%prefix,
    "high_res=10",
    "low_res=10.5",
    ">%s.zlog"%prefix])
  assert easy_run.call(cmd)==0
  ## Common params for refinements
  args_base = [
    "%s.mtz"%prefix,
    "macro_cycles=1",
    "ramachandran_plot_restraints.enabled=false",
    "target_bonds_rmsd=0.05",
    "target_angles_rmsd=0.5"]
  ##
  # Rely on secondary_structure.protein.search_method=from_ca
  # no HELIX in header
  args = args_base+[
    "%s_0.pdb"%prefix,
    "secondary_structure.protein.search_method=from_ca",
    "secondary_structure.enabled=True"]
  r = run_real_space_refine(args = args, prefix = prefix)
  d1 = check_hbonds(file_name=r.pdb)
  assert approx_equal(flex.max(d1), 2.9, 0.1)
  # Rely on HELIX in header
  args = args_base+[
    "%s_1.pdb"%prefix,
    "secondary_structure.enabled=True"]
  r = run_real_space_refine(args = args, prefix = prefix)
  d2 = check_hbonds(file_name=r.pdb)
  assert approx_equal(flex.max(d2), 2.9, 0.1)
  # do not use SS restraints
  args = args_base+[
    "%s_1.pdb"%prefix,
    "secondary_structure.enabled=False"]
  r = run_real_space_refine(args = args, prefix = prefix)
  d3 = check_hbonds(file_name=r.pdb)
  assert flex.mean(d3) > 3.8, flex.mean(d3)

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK time =%8.3f"%(time.time() - t0))
