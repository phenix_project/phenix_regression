from __future__ import print_function
import time
from libtbx import easy_run
import iotbx.pdb
from scitbx.array_family import flex
from scitbx.math import superpose
import os
import libtbx.load_env
from phenix_regression.real_space_refine import run_real_space_refine
from libtbx.test_utils import assert_lines_in_file
import mmtbx.model
from libtbx.utils import null_out
from libtbx.test_utils import approx_equal

eff_str="""
pdb_interpretation {
  ncs_group {
    reference = chain 'C' or chain 'L' or chain 'K'
    selection = chain 'F' or chain 'I' or chain 'J'
  }
}
"""

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  Protein and NA model, C2 dimer. Map is symmetriezed (real data altered).
  Make sure refinements with and without NCS produce the same results.
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/real_space_refine/data/%s.pdb"%prefix,
    test=os.path.isfile)
  mtz = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/real_space_refine/data/%s.mtz"%prefix,
    test=os.path.isfile)
  eff = "%s.eff"%prefix
  with open(eff,"w") as fo:
    fo.write(eff_str)
  #
  base = [pdb, mtz, eff, "refine_ncs_operators=false", "macro_cycles=1",
          "run=minimization_global+local_grid_search"]
  args1 = base+["ncs_constraints=false",]
  args2 = base+["ncs_constraints=true",]
  r1 = run_real_space_refine(args = args1, prefix = prefix+"_NCS_False")
  r2 = run_real_space_refine(args = args2, prefix = prefix+"_NCS_True")
  #
  def get_value(f):
    m = mmtbx.model.manager(model_input = iotbx.pdb.input(file_name=f),
      log = null_out())
    m.process(make_restraints=True)
    return m.geometry_statistics()
  #
  g1 = get_value(r1.pdb)
  g2 = get_value(r2.pdb)
  #
  assert approx_equal(g1.rotamer().outliers, g2.rotamer().outliers, 0.1)

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK time =%8.3f"%(time.time() - t0))
