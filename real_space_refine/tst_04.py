from __future__ import print_function
from libtbx import easy_run
import time
import iotbx.pdb
from scitbx.array_family import flex
import os
from phenix_regression.real_space_refine import run_real_space_refine

pdb_str_answer = """\
CRYST1   18.415   14.419   12.493  90.00  90.00  90.00 P 1
ATOM      1  N   THR A   1      11.782  12.419   4.645  1.00 10.00           N
ATOM      2  CA  THR A   1      11.671  11.061   4.125  1.00 10.00           C
ATOM      3  C   THR A   1      11.746  10.033   5.249  1.00 10.00           C
ATOM      4  O   THR A   1      12.561  10.157   6.163  1.00 10.00           O
ATOM      5  CB  THR A   1      12.772  10.760   3.092  1.00 10.00           C
ATOM      6  OG1 THR A   1      12.672  11.682   2.000  1.00 10.00           O
ATOM      7  CG2 THR A   1      12.635   9.340   2.565  1.00 10.00           C
TER
ATOM      1  N   THR B   1       6.768   9.093   9.237  1.00 10.00           N
ATOM      2  CA  THR B   1       7.284   8.654   7.945  1.00 10.00           C
ATOM      3  C   THR B   1       8.638   7.968   8.097  1.00 10.00           C
ATOM      4  O   THR B   1       9.495   8.426   8.852  1.00 10.00           O
ATOM      5  CB  THR B   1       7.423   9.832   6.963  1.00 10.00           C
ATOM      6  OG1 THR B   1       6.144  10.446   6.765  1.00 10.00           O
ATOM      7  CG2 THR B   1       7.962   9.350   5.625  1.00 10.00           C
TER
ATOM      1  N   THR C   1       9.093   2.000  10.493  1.00 10.00           N
ATOM      2  CA  THR C   1       8.879   2.702   9.233  1.00 10.00           C
ATOM      3  C   THR C   1      10.081   3.570   8.875  1.00 10.00           C
ATOM      4  O   THR C   1      10.652   4.241   9.734  1.00 10.00           O
ATOM      5  CB  THR C   1       7.618   3.584   9.284  1.00 10.00           C
ATOM      6  OG1 THR C   1       6.472   2.770   9.559  1.00 10.00           O
ATOM      7  CG2 THR C   1       7.417   4.305   7.960  1.00 10.00           C
END

"""

pdb_str_poor = """\
CRYST1   18.415   14.419   12.493  90.00  90.00  90.00 P 1
ATOM      1  N   THR A   1      11.120  12.388   4.399  1.00 10.00           N
ATOM      2  CA  THR A   1      11.623  11.024   4.280  1.00 10.00           C
ATOM      3  C   THR A   1      12.335  10.587   5.556  1.00 10.00           C
ATOM      4  O   THR A   1      13.094  11.353   6.149  1.00 10.00           O
ATOM      5  CB  THR A   1      12.588  10.881   3.089  1.00 10.00           C
ATOM      6  OG1 THR A   1      11.911  11.230   1.876  1.00 10.00           O
ATOM      7  CG2 THR A   1      13.099   9.452   2.986  1.00 10.00           C
TER
ATOM      8  N   THR B   1       7.221   8.778   9.110  1.00 10.00           N
ATOM      9  CA  THR B   1       7.661   8.575   7.734  1.00 10.00           C
ATOM     10  C   THR B   1       9.058   7.965   7.687  1.00 10.00           C
ATOM     11  O   THR B   1       9.944   8.359   8.445  1.00 10.00           O
ATOM     12  CB  THR B   1       7.660   9.895   6.941  1.00 10.00           C
ATOM     13  OG1 THR B   1       6.338  10.446   6.927  1.00 10.00           O
ATOM     14  CG2 THR B   1       8.122   9.659   5.511  1.00 10.00           C
TER
ATOM     15  N   THR C   1       8.639   1.605   9.684  1.00 10.00           N
ATOM     16  CA  THR C   1       8.751   2.735   8.769  1.00 10.00           C
ATOM     17  C   THR C   1      10.144   3.354   8.827  1.00 10.00           C
ATOM     18  O   THR C   1      10.716   3.520   9.904  1.00 10.00           O
ATOM     19  CB  THR C   1       7.704   3.820   9.080  1.00 10.00           C
ATOM     20  OG1 THR C   1       6.388   3.265   8.969  1.00 10.00           O
ATOM     21  CG2 THR C   1       7.842   4.986   8.114  1.00 10.00           C

"""

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  NCS constraints: full ASU in input.
  """
  pdb_file_name_answer = "%s_answer.pdb"%prefix
  of=open(pdb_file_name_answer, "w")
  print(pdb_str_answer, file=of)
  of.close()
  #
  pdb_file_name_poor = "%s_poor.pdb"%prefix
  of=open(pdb_file_name_poor, "w")
  print(pdb_str_poor, file=of)
  of.close()
  #
  cmd = " ".join([
    "phenix.fmodel",
    "%s"%pdb_file_name_answer,
    "file_name=%s.mtz"%prefix,
    "high_res=1.0",
    ">%s.zlog"%prefix])
  assert easy_run.call(cmd)==0
  #
  args = [
    "run=minimization_global+local_grid_search+morphing",
    "%s"%pdb_file_name_poor,
    "%s.mtz"%prefix]
  r = run_real_space_refine(args = args, prefix = prefix)
  ###
  xrs_answer = iotbx.pdb.input(file_name=
    pdb_file_name_answer).xray_structure_simple()
  xrs_poor = iotbx.pdb.input(file_name=
    pdb_file_name_poor).xray_structure_simple()
  xrs_refined = r.xrs
  #
  dap = xrs_answer.distances(xrs_poor)
  dar = xrs_answer.distances(xrs_refined)
  assert flex.mean(dap) > 0.5
  assert flex.mean(dar) < 0.04, flex.mean(dar)

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
