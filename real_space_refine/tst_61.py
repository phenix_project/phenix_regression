from __future__ import print_function
import time
from libtbx import easy_run
import iotbx.pdb
from scitbx.array_family import flex
from scitbx.math import superpose
import os
import libtbx.load_env
from phenix_regression.real_space_refine import run_real_space_refine
from libtbx.test_utils import assert_lines_in_file
from libtbx.test_utils import approx_equal, not_approx_equal

pdb_str = """
CRYST1   15.510   16.920   16.920  90.00  90.00  90.00 P 1
SCALE1      0.064475  0.000000  0.000000        0.00000
SCALE2      0.000000  0.059102  0.000000        0.00000
SCALE3      0.000000  0.000000  0.059102        0.00000
ATOM      1  O5'   GS2 330       6.305   4.338   5.565  1.00206.26           O
ATOM      2  C5'   GS2 330       5.747   5.597   5.203  1.00205.32           C
ATOM      3  C4'   GS2 330       6.799   6.677   5.140  1.00205.31           C
ATOM      4  O4'   GS2 330       7.169   6.898   3.751  1.00205.57           O
ATOM      5  C3'   GS2 330       6.369   8.037   5.687  1.00205.01           C
ATOM      6  O3'   GS2 330       7.492   8.683   6.285  1.00205.10           O
ATOM      7  C2'   GS2 330       5.959   8.791   4.427  1.00204.77           C
ATOM      8  O2'   GS2 330       6.021  10.197   4.537  1.00204.10           O
ATOM      9  C1'   GS2 330       6.965   8.257   3.412  1.00205.33           C
ATOM     10  P     US2 331       7.727   8.597   7.873  1.00199.13           P
ATOM     11  OP1   US2 331       6.399   8.627   8.536  1.00198.34           O
ATOM     12  OP2   US2 331       8.752   9.607   8.240  1.00198.62           O
ATOM     13  O5'   US2 331       8.358   7.151   8.093  1.00198.68           O
ATOM     14  C5'   US2 331       9.511   6.971   8.910  1.00198.27           C
ATOM     15  C4'   US2 331      10.398   5.871   8.382  1.00198.16           C
ATOM     16  O4'   US2 331      10.283   5.828   6.932  1.00198.80           O
TER
HETATM   17  O   HOHS22005       3.571  11.446  11.378  1.00 67.05           O
TER
END
"""

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  ADP refinement when the water becomes one single group
  """
  #
  pdb_file = "%s.pdb"%prefix
  with open(pdb_file,"w") as fo:
    fo.write(pdb_str)
  #
  mtz_file = "%s.mtz"%prefix
  cmd = " ".join([
    "phenix.fmodel",
    "%s"%pdb_file,
    "file_name=%s"%mtz_file,
    "high_res=2.0",
    ">%s.zlog"%prefix])
  assert easy_run.call(cmd)==0
  #
  args = [pdb_file, mtz_file, "resolution=2.8", "run=adp", "nproc=2"]
  r = run_real_space_refine(args = args, prefix = prefix)
  #
  assert_lines_in_file(r.log, "Skip one atom model, chains: (S2)")

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK time =%8.3f"%(time.time() - t0))
