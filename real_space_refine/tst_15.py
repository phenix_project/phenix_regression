from __future__ import print_function
import time, os
from libtbx import easy_run
import iotbx.pdb
import libtbx.load_env
from phenix_regression.real_space_refine import run_real_space_refine

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  Update NCS operators after large rigid-body shifts.
  """
  pdb_answer = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/real_space_refine/data/tst_15.pdb",
    test=os.path.isfile)
  pdb_poor = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/real_space_refine/data/tst_15_poor.pdb",
    test=os.path.isfile)
  params = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/real_space_refine/data/tst_15.eff",
    test=os.path.isfile)
  #
  cmd = " ".join([
    "phenix.fmodel",
    "%s"%pdb_answer,
    "high_res=3.5",
    "output.file_name=%s.mtz"%prefix,
    "> zlog1"])
  assert easy_run.call(cmd)==0
  #
  args = [
    "%s"%pdb_poor,
    "%s"%params,
    "%s.mtz"%prefix,
    "run=rigid_body+minimization_global",
    "ramachandran_plot_restraints.enabled=false",
    "macro_cycles=1"]
  r = run_real_space_refine(args = args, prefix = prefix)
  # check for expected output
  result_pdb = r.pdb
  cmd = "phenix.mtz2map %s.mtz include_fmodel=True"%prefix
  assert easy_run.call(cmd)==0
  r = easy_run.fully_buffered(
    "phenix.model_map_cc force %s %s_fmodel.ccp4 resolution=3.5"%(pdb_poor,prefix))
  cc_start=None
  for l in r.stdout_lines:
    if(l.count("CC_mask  :")>0): cc_start = float(l.split()[2])
  cmd = "phenix.model_map_cc force %s %s_fmodel.ccp4 resolution=3.5"%(result_pdb,prefix)
  print(cmd)
  r = easy_run.fully_buffered(cmd)
  cc_final=None
  for l in r.stdout_lines:
    if(l.count("CC_mask  :")): cc_final = float(l.split()[2])
  #
  assert cc_start < 0.1  , cc_start
  assert cc_final > 0.93 , cc_final

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK time =%8.3f"%(time.time() - t0))
