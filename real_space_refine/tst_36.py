from __future__ import print_function
from libtbx import easy_run
import time
import iotbx.pdb
from scitbx.array_family import flex
from libtbx.test_utils import approx_equal
import random
import os
from phenix_regression.real_space_refine import run_real_space_refine

pdb_str = """\
CRYST1   18.361   15.679   19.962  90.00  90.00  90.00 P 1
ATOM      1  N   GLN A3048       5.650   5.153  10.470  1.00 20.00           N
ATOM      2  CA  GLN A3048       6.793   5.477   9.629  1.00 20.00           C
ATOM      3  C   GLN A3048       7.677   6.465  10.370  1.00 20.00           C
ATOM      4  O   GLN A3048       7.248   7.582  10.672  1.00 20.00           O
ATOM      5  CB  GLN A3048       6.358   6.085   8.295  1.00 20.00           C
ATOM      6  CG  GLN A3048       5.737   5.129   7.306  1.00 20.00           C
ATOM      7  CD  GLN A3048       5.423   5.796   5.973  1.00 20.00           C
ATOM      8  OE1 GLN A3048       5.577   7.007   5.817  1.00 20.00           O
ATOM      9  NE2 GLN A3048       5.000   5.000   5.000  1.00 20.00           N
ATOM     10  N   ARG A3049       8.911   6.064  10.661  1.00 20.00           N
ATOM     11  CA  ARG A3049       9.829   6.912  11.413  1.00 20.00           C
ATOM     12  C   ARG A3049      11.071   7.190  10.577  1.00 20.00           C
ATOM     13  O   ARG A3049      11.990   6.346  10.533  1.00 20.00           O
ATOM     14  CB  ARG A3049      10.186   6.273  12.758  1.00 20.00           C
ATOM     15  CG  ARG A3049      11.151   7.087  13.606  1.00 20.00           C
ATOM     16  CD  ARG A3049      10.628   8.492  13.808  1.00 20.00           C
ATOM     17  NE  ARG A3049       9.296   8.492  14.403  1.00 20.00           N
ATOM     18  CZ  ARG A3049       8.483   9.541  14.407  1.00 20.00           C
ATOM     19  NH1 ARG A3049       8.866  10.679  13.848  1.00 20.00           N
ATOM     20  NH2 ARG A3049       7.284   9.454  14.962  1.00 20.00           N
ATOM     21  N   PRO A3050      11.146   8.342   9.892  1.00 20.00           N
ATOM     22  CA  PRO A3050      12.364   8.788   9.211  1.00 20.00           C
ATOM     23  C   PRO A3050      13.361   9.374  10.196  1.00 20.00           C
ATOM     24  O   PRO A3050      12.964   9.623  11.332  1.00 20.00           O
ATOM     25  CB  PRO A3050      11.854   9.859   8.247  1.00 20.00           C
ATOM     26  CG  PRO A3050      10.643  10.400   8.913  1.00 20.00           C
ATOM     27  CD  PRO A3050      10.009   9.241   9.623  1.00 20.00           C
TER
END
"""

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  Adjust user-supplied resolution to match map sampling limit.
  """
  #
  pdb_inp = iotbx.pdb.input(source_info=None, lines=pdb_str)
  ph = pdb_inp.construct_hierarchy()
  ph.write_pdb_file(file_name="%s.pdb"%prefix,
    crystal_symmetry=pdb_inp.crystal_symmetry())
  #
  cmd = " ".join([
    "phenix.model_map",
    "%s.pdb"%prefix,
    "output_file_name_prefix=%s"%prefix,
    "grid_step=2.0",
    ">%s.zlog"%prefix])
  assert easy_run.call(cmd)==0
  #
  args = [
    "run=minimization_global",
    "resolution=1",
    "macro_cycles=1",
    "%s.pdb"%prefix,
    "%s.ccp4"%prefix]
  r = run_real_space_refine(args = args, prefix = prefix)
  #

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
