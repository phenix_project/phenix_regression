from __future__ import print_function
from libtbx import easy_run
import time
import iotbx.pdb
import os
from phenix_regression.real_space_refine import run_real_space_refine

pdb_str_1 = """\
CRYST1   27.475   35.191   27.490  90.00  90.00  90.00 P 21 21 21
MODEL        1
ATOM      1  N   ALA E   1       6.709  31.817   5.664  1.00 20.00           N
ATOM      2  CA  ALA E   1       7.794  30.914   6.029  1.00 20.00           C
ATOM      3  C   ALA E   1       7.267  29.691   6.773  1.00 20.00           C
ATOM      4  O   ALA E   1       7.942  29.146   7.647  1.00 20.00           O
ATOM      5  CB  ALA E   1       8.831  31.643   6.870  1.00 20.00           C
ENDMDL
"""
pdb_str_2 = """\
MODEL        2
ATOM      1  N   ALA E   1       6.000  31.000   5.000  1.00 20.00           N
ATOM      2  CA  ALA E   1       7.000  30.000   6.000  1.00 20.00           C
ATOM      3  C   ALA E   1       7.000  29.000   6.000  1.00 20.00           C
ATOM      4  O   ALA E   1       7.000  29.000   7.000  1.00 20.00           O
ATOM      5  CB  ALA E   1       8.000  31.000   6.000  1.00 20.00           C
ENDMDL
"""

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  Exercise multi-model is not supported.
  """
  pdb_file_name = "%s_answer.pdb"%prefix
  of=open(pdb_file_name, "w")
  print(pdb_str_1 + pdb_str_2, file=of)
  of.close()
  #
  cmd = " ".join([
    "phenix.fmodel",
    "%s"%pdb_file_name,
    "file_name=%s.mtz"%prefix,
    "high_res=2",
    ">%s.zlog"%prefix])
  assert easy_run.call(cmd)==0
  #
  args = [
    "%s"%pdb_file_name,
    "%s.mtz"%prefix]
  r = run_real_space_refine(args = args, prefix = prefix, sorry_expected=True)
  # check stderr
  assert r.r.stderr_lines[-1] == \
    'Sorry: Multi-model PDB (with MODEL-ENDMDL) is not supported.'
  # Now the same with just one model, but with MODEL ENDMDL keywords
  of=open(pdb_file_name, "w")
  print(pdb_str_1, file=of)
  of.close()
  cmd = " ".join([
    "phenix.real_space_refine",
    "%s"%pdb_file_name,
    "%s.mtz"%prefix,
    ">%s.zlog"%prefix])
  r = run_real_space_refine(args = args, prefix = prefix)
  # check refinement finished OK
  #  ... done as part of run_real_space_refine call

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
