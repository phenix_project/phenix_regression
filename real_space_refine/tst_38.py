from __future__ import print_function
from libtbx import easy_run
import time
import iotbx.pdb
from scitbx.array_family import flex
from libtbx.test_utils import approx_equal
import random
import os
from phenix_regression.real_space_refine import run_real_space_refine

pdb_str = """\
CRYST1   15.167   14.392   12.685  90.00  90.00  90.00 P 1
SCALE1      0.065933  0.000000  0.000000        0.00000
SCALE2      0.000000  0.069483  0.000000        0.00000
SCALE3      0.000000  0.000000  0.078833        0.00000
ATOM      1  N   GLU A 131       7.063   9.392   5.000  1.00204.16           N
ATOM      2  CA  GLU A 131       6.656   8.366   5.953  1.00204.16           C
ATOM      3  C   GLU A 131       5.993   8.979   7.171  1.00204.16           C
ATOM      4  O   GLU A 131       5.000   8.455   7.685  1.00204.16           O
ATOM      5  CB  GLU A 131       7.862   7.548   6.404  1.00204.16           C
ATOM      6  CG  GLU A 131       8.456   6.644   5.374  1.00204.16           C
ATOM      7  CD  GLU A 131       9.643   5.898   5.930  1.00204.16           C
ATOM      8  OE1 GLU A 131      10.051   6.210   7.069  1.00204.16           O
ATOM      9  OE2 GLU A 131      10.167   5.000   5.239  1.00204.16           O1-
TER
END
"""

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  Deal with O1-.
  """
  #
  pdb_inp = iotbx.pdb.input(source_info=None, lines=pdb_str)
  ph = pdb_inp.construct_hierarchy()
  ph.write_pdb_file(file_name="%s.pdb"%prefix,
    crystal_symmetry=pdb_inp.crystal_symmetry())
  #
  cmd = " ".join([
    "phenix.fmodel",
    "%s.pdb"%prefix,
    "output.file_name=%s.mtz"%prefix,
    "high_res=2.0",
    ">%s.zlog"%prefix])
  assert easy_run.call(cmd)==0
  #
  args = [
    "macro_cycles=1",
    "%s.pdb"%prefix,
    "model_format=pdb+mmcif",
    "%s.mtz"%prefix]
  r = run_real_space_refine(args = args, prefix = prefix)
  #

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
