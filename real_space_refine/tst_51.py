from __future__ import print_function
import time
from libtbx import easy_run
import iotbx.pdb
import os
from phenix_regression.real_space_refine import run_real_space_refine
from libtbx.test_utils import assert_lines_in_file

pdb_str="""
CRYST1   39.363   61.623   37.813  90.00  90.00  90.00 P 1
SCALE1      0.025405  0.000000  0.000000        0.00000
SCALE2      0.000000  0.016228  0.000000        0.00000
SCALE3      0.000000  0.000000  0.026446        0.00000
ATOM      1  N   LYS A   2      25.116  32.082   9.162  1.00 97.45           N
ATOM      2  CA  LYS A   2      24.617  33.374   9.614  1.00 97.45           C
ATOM      3  C   LYS A   2      25.650  34.078  10.483  1.00 97.45           C
ATOM      4  O   LYS A   2      26.289  33.457  11.329  1.00 97.45           O
ATOM      5  CB  LYS A   2      23.306  33.205  10.385  1.00 97.45           C
ATOM      6  CG  LYS A   2      22.069  33.135   9.505  1.00 97.45           C
ATOM      7  CD  LYS A   2      21.227  34.395   9.621  1.00 97.45           C
ATOM      8  CE  LYS A   2      19.853  34.197   8.998  1.00 97.45           C
ATOM      9  NZ  LYS A   2      19.028  35.436   9.038  1.00 97.45           N
ATOM     10  N   VAL A   3      25.809  35.382  10.267  1.00 88.50           N
ATOM     11  CA  VAL A   3      26.783  36.200  10.980  1.00 88.50           C
ATOM     12  C   VAL A   3      26.361  37.657  10.826  1.00 88.50           C
ATOM     13  O   VAL A   3      25.690  38.029   9.863  1.00 88.50           O
ATOM     14  CB  VAL A   3      28.217  35.965  10.447  1.00 88.50           C
ATOM     15  CG1 VAL A   3      28.269  36.219   8.962  1.00 88.50           C
ATOM     16  CG2 VAL A   3      29.241  36.810  11.181  1.00 88.50           C
ATOM     17  N   ASN A   4      26.741  38.487  11.794  1.00 80.66           N
ATOM     18  CA  ASN A   4      26.380  39.894  11.770  1.00 80.66           C
ATOM     19  C   ASN A   4      27.068  40.609  10.612  1.00 80.66           C
ATOM     20  O   ASN A   4      28.168  40.253  10.188  1.00 80.66           O
ATOM     21  CB  ASN A   4      26.756  40.574  13.085  1.00 80.66           C
ATOM     22  CG  ASN A   4      25.772  40.282  14.192  1.00 80.66           C
ATOM     23  OD1 ASN A   4      24.634  40.745  14.165  1.00 80.66           O
ATOM     24  ND2 ASN A   4      26.208  39.512  15.178  1.00 80.66           N
ATOM     25  N   ASN A   5      26.402  41.643  10.111  1.00 80.56           N
ATOM     26  CA  ASN A   5      26.893  42.438   8.997  1.00 80.56           C
ATOM     27  C   ASN A   5      27.318  43.812   9.494  1.00 80.56           C
ATOM     28  O   ASN A   5      26.622  44.430  10.304  1.00 80.56           O
ATOM     29  CB  ASN A   5      25.820  42.582   7.918  1.00 80.56           C
ATOM     30  CG  ASN A   5      25.510  41.273   7.230  1.00 80.56           C
ATOM     31  OD1 ASN A   5      26.084  40.954   6.194  1.00 80.56           O
ATOM     32  ND2 ASN A   5      24.603  40.501   7.810  1.00 80.56           N
ATOM     33  N   THR A   6      28.462  44.283   9.005  1.00 72.28           N
ATOM     34  CA  THR A   6      29.029  45.559   9.417  1.00 72.28           C
ATOM     35  C   THR A   6      29.257  46.432   8.192  1.00 72.28           C
ATOM     36  O   THR A   6      29.799  45.966   7.187  1.00 72.28           O
ATOM     37  CB  THR A   6      30.343  45.358  10.175  1.00 72.28           C
ATOM     38  OG1 THR A   6      30.096  44.589  11.357  1.00 72.28           O
ATOM     39  CG2 THR A   6      30.945  46.693  10.565  1.00 72.28           C
ATOM     40  N   ILE A   7      28.844  47.693   8.283  1.00 70.36           N
ATOM     41  CA  ILE A   7      28.966  48.656   7.195  1.00 70.36           C
ATOM     42  C   ILE A   7      29.912  49.764   7.631  1.00 70.36           C
ATOM     43  O   ILE A   7      29.770  50.317   8.727  1.00 70.36           O
ATOM     44  CB  ILE A   7      27.592  49.228   6.800  1.00 70.36           C
ATOM     45  CG1 ILE A   7      26.590  48.095   6.593  1.00 70.36           C
ATOM     46  CG2 ILE A   7      27.706  50.075   5.551  1.00 70.36           C
ATOM     47  CD1 ILE A   7      25.199  48.568   6.282  1.00 70.36           C
ATOM     48  N   VAL A   8      30.873  50.091   6.772  1.00 69.44           N
ATOM     49  CA  VAL A   8      31.916  51.062   7.086  1.00 69.44           C
ATOM     50  C   VAL A   8      31.663  52.299   6.235  1.00 69.44           C
ATOM     51  O   VAL A   8      31.683  52.234   5.000  1.00 69.44           O
ATOM     52  CB  VAL A   8      33.320  50.504   6.845  1.00 69.44           C
ATOM     53  CG1 VAL A   8      34.363  51.554   7.174  1.00 69.44           C
ATOM     54  CG2 VAL A   8      33.540  49.255   7.671  1.00 69.44           C
ATOM     55  N   VAL A   9      31.441  53.431   6.889  1.00 68.77           N
ATOM     56  CA  VAL A   9      31.251  54.702   6.204  1.00 68.77           C
ATOM     57  C   VAL A   9      32.437  55.598   6.530  1.00 68.77           C
ATOM     58  O   VAL A   9      32.698  55.898   7.698  1.00 68.77           O
ATOM     59  CB  VAL A   9      29.929  55.364   6.615  1.00 68.77           C
ATOM     60  CG1 VAL A   9      29.704  56.623   5.808  1.00 68.77           C
ATOM     61  CG2 VAL A   9      28.779  54.393   6.434  1.00 68.77           C
TER
ATOM     62  N   ARG B   2       5.091   9.971  31.047  1.00110.07           N
ATOM     63  CA  ARG B   2       6.274  10.065  31.896  1.00110.07           C
ATOM     64  C   ARG B   2       7.521  10.413  31.090  1.00110.07           C
ATOM     65  O   ARG B   2       8.603  10.586  31.647  1.00110.07           O
ATOM     66  CB  ARG B   2       6.490   8.760  32.669  1.00110.07           C
ATOM     67  CG  ARG B   2       7.170   7.642  31.890  1.00110.07           C
ATOM     68  CD  ARG B   2       6.276   7.086  30.793  1.00110.07           C
ATOM     69  NE  ARG B   2       6.898   5.969  30.096  1.00110.07           N
ATOM     70  CZ  ARG B   2       7.751   6.093  29.089  1.00110.07           C
ATOM     71  NH1 ARG B   2       8.091   7.280  28.614  1.00110.07           N
ATOM     72  NH2 ARG B   2       8.273   5.000  28.544  1.00110.07           N
ATOM     73  N   GLU B   3       7.361  10.519  29.776  1.00110.02           N
ATOM     74  CA  GLU B   3       8.440  10.927  28.888  1.00110.02           C
ATOM     75  C   GLU B   3       8.731  12.415  29.041  1.00110.02           C
ATOM     76  O   GLU B   3       7.824  13.218  29.286  1.00110.02           O
ATOM     77  CB  GLU B   3       8.081  10.579  27.444  1.00110.02           C
ATOM     78  CG  GLU B   3       6.699  11.036  27.038  1.00110.02           C
ATOM     79  CD  GLU B   3       6.189  10.313  25.813  1.00110.02           C
ATOM     80  OE1 GLU B   3       6.962   9.542  25.212  1.00110.02           O
ATOM     81  OE2 GLU B   3       5.000  10.476  25.479  1.00110.02           O
ATOM     82  N   ILE B   4      10.007  12.772  28.899  1.00101.18           N
ATOM     83  CA  ILE B   4      10.516  14.097  29.242  1.00101.18           C
ATOM     84  C   ILE B   4      11.203  14.691  28.021  1.00101.18           C
ATOM     85  O   ILE B   4      12.019  14.025  27.374  1.00101.18           O
ATOM     86  CB  ILE B   4      11.490  14.041  30.430  1.00101.18           C
ATOM     87  CG1 ILE B   4      10.783  13.524  31.681  1.00101.18           C
ATOM     88  CG2 ILE B   4      12.092  15.409  30.693  1.00101.18           C
ATOM     89  CD1 ILE B   4      11.725  13.200  32.813  1.00101.18           C
ATOM     90  N   LEU B   5      10.880  15.945  27.717  1.00 96.11           N
ATOM     91  CA  LEU B   5      11.526  16.673  26.634  1.00 96.11           C
ATOM     92  C   LEU B   5      12.644  17.548  27.188  1.00 96.11           C
ATOM     93  O   LEU B   5      12.478  18.198  28.223  1.00 96.11           O
ATOM     94  CB  LEU B   5      10.501  17.533  25.897  1.00 96.11           C
ATOM     95  CG  LEU B   5      10.980  18.503  24.815  1.00 96.11           C
ATOM     96  CD1 LEU B   5      11.916  17.829  23.833  1.00 96.11           C
ATOM     97  CD2 LEU B   5       9.797  19.110  24.092  1.00 96.11           C
ATOM     98  N   SER B   6      13.779  17.567  26.498  1.00 90.33           N
ATOM     99  CA  SER B   6      14.914  18.396  26.875  1.00 90.33           C
ATOM    100  C   SER B   6      14.978  19.621  25.973  1.00 90.33           C
ATOM    101  O   SER B   6      14.650  19.546  24.787  1.00 90.33           O
ATOM    102  CB  SER B   6      16.222  17.611  26.783  1.00 90.33           C
ATOM    103  OG  SER B   6      16.226  16.527  27.693  1.00 90.33           O
ATOM    104  N   ILE B   7      15.386  20.751  26.540  1.00 87.07           N
ATOM    105  CA  ILE B   7      15.467  22.015  25.817  1.00 87.07           C
ATOM    106  C   ILE B   7      16.828  22.627  26.108  1.00 87.07           C
ATOM    107  O   ILE B   7      17.167  22.862  27.273  1.00 87.07           O
ATOM    108  CB  ILE B   7      14.339  22.971  26.228  1.00 87.07           C
ATOM    109  CG1 ILE B   7      12.984  22.370  25.866  1.00 87.07           C
ATOM    110  CG2 ILE B   7      14.517  24.318  25.555  1.00 87.07           C
ATOM    111  CD1 ILE B   7      11.821  23.105  26.456  1.00 87.07           C
ATOM    112  N   HIS B   8      17.607  22.892  25.062  1.00 86.24           N
ATOM    113  CA  HIS B   8      18.965  23.394  25.232  1.00 86.24           C
ATOM    114  C   HIS B   8      19.107  24.736  24.531  1.00 86.24           C
ATOM    115  O   HIS B   8      18.977  24.813  23.305  1.00 86.24           O
ATOM    116  CB  HIS B   8      19.978  22.391  24.691  1.00 86.24           C
ATOM    117  CG  HIS B   8      19.720  20.985  25.130  1.00 86.24           C
ATOM    118  ND1 HIS B   8      20.092  20.514  26.370  1.00 86.24           N
ATOM    119  CD2 HIS B   8      19.140  19.943  24.491  1.00 86.24           C
ATOM    120  CE1 HIS B   8      19.747  19.244  26.478  1.00 86.24           C
ATOM    121  NE2 HIS B   8      19.168  18.873  25.351  1.00 86.24           N
ATOM    122  N   VAL B   9      19.419  25.776  25.302  1.00 85.55           N
ATOM    123  CA  VAL B   9      19.462  27.148  24.806  1.00 85.55           C
ATOM    124  C   VAL B   9      20.852  27.722  25.045  1.00 85.55           C
ATOM    125  O   VAL B   9      21.454  27.492  26.100  1.00 85.55           O
ATOM    126  CB  VAL B   9      18.384  28.031  25.460  1.00 85.55           C
ATOM    127  CG1 VAL B   9      18.625  28.186  26.932  1.00 85.55           C
ATOM    128  CG2 VAL B   9      18.336  29.394  24.792  1.00 85.55           C
TER
END
"""

pdb_str_ref="""
ATOM      1  N   LYS D   2      46.352 -49.081 -19.373  1.00 97.45           N
ATOM      2  CA  LYS D   2      45.853 -47.789 -18.921  1.00 97.45           C
ATOM      3  C   LYS D   2      46.886 -47.085 -18.052  1.00 97.45           C
ATOM      4  O   LYS D   2      47.525 -47.706 -17.206  1.00 97.45           O
ATOM      5  CB  LYS D   2      44.542 -47.958 -18.150  1.00 97.45           C
ATOM      6  CG  LYS D   2      43.305 -48.028 -19.030  1.00 97.45           C
ATOM      7  CD  LYS D   2      42.463 -46.768 -18.914  1.00 97.45           C
ATOM      8  CE  LYS D   2      41.089 -46.966 -19.537  1.00 97.45           C
ATOM      9  NZ  LYS D   2      40.264 -45.727 -19.497  1.00 97.45           N
ATOM     10  N   VAL D   3      47.045 -45.781 -18.268  1.00 88.50           N
ATOM     11  CA  VAL D   3      48.019 -44.963 -17.555  1.00 88.50           C
ATOM     12  C   VAL D   3      47.597 -43.506 -17.709  1.00 88.50           C
ATOM     13  O   VAL D   3      46.926 -43.134 -18.672  1.00 88.50           O
ATOM     14  CB  VAL D   3      49.453 -45.198 -18.088  1.00 88.50           C
ATOM     15  CG1 VAL D   3      49.505 -44.944 -19.573  1.00 88.50           C
ATOM     16  CG2 VAL D   3      50.477 -44.353 -17.354  1.00 88.50           C
ATOM     17  N   ASN D   4      47.977 -42.676 -16.741  1.00 80.66           N
ATOM     18  CA  ASN D   4      47.616 -41.269 -16.765  1.00 80.66           C
ATOM     19  C   ASN D   4      48.304 -40.554 -17.923  1.00 80.66           C
ATOM     20  O   ASN D   4      49.404 -40.910 -18.347  1.00 80.66           O
ATOM     21  CB  ASN D   4      47.992 -40.589 -15.450  1.00 80.66           C
ATOM     22  CG  ASN D   4      47.008 -40.881 -14.343  1.00 80.66           C
ATOM     23  OD1 ASN D   4      45.870 -40.418 -14.370  1.00 80.66           O
ATOM     24  ND2 ASN D   4      47.444 -41.651 -13.357  1.00 80.66           N
ATOM     25  N   ASN D   5      47.638 -39.520 -18.424  1.00 80.56           N
ATOM     26  CA  ASN D   5      48.129 -38.725 -19.538  1.00 80.56           C
ATOM     27  C   ASN D   5      48.554 -37.351 -19.041  1.00 80.56           C
ATOM     28  O   ASN D   5      47.858 -36.733 -18.231  1.00 80.56           O
ATOM     29  CB  ASN D   5      47.056 -38.581 -20.617  1.00 80.56           C
ATOM     30  CG  ASN D   5      46.746 -39.890 -21.305  1.00 80.56           C
ATOM     31  OD1 ASN D   5      47.320 -40.209 -22.341  1.00 80.56           O
ATOM     32  ND2 ASN D   5      45.839 -40.662 -20.725  1.00 80.56           N
ATOM     33  N   THR D   6      49.698 -36.880 -19.530  1.00 72.28           N
ATOM     34  CA  THR D   6      50.265 -35.604 -19.118  1.00 72.28           C
ATOM     35  C   THR D   6      50.493 -34.731 -20.343  1.00 72.28           C
ATOM     36  O   THR D   6      51.035 -35.197 -21.348  1.00 72.28           O
ATOM     37  CB  THR D   6      51.579 -35.805 -18.360  1.00 72.28           C
ATOM     38  OG1 THR D   6      51.332 -36.574 -17.178  1.00 72.28           O
ATOM     39  CG2 THR D   6      52.181 -34.470 -17.970  1.00 72.28           C
ATOM     40  N   ILE D   7      50.080 -33.470 -20.252  1.00 70.36           N
ATOM     41  CA  ILE D   7      50.202 -32.507 -21.340  1.00 70.36           C
ATOM     42  C   ILE D   7      51.148 -31.399 -20.904  1.00 70.36           C
ATOM     43  O   ILE D   7      51.006 -30.846 -19.808  1.00 70.36           O
ATOM     44  CB  ILE D   7      48.828 -31.935 -21.735  1.00 70.36           C
ATOM     45  CG1 ILE D   7      47.826 -33.068 -21.942  1.00 70.36           C
ATOM     46  CG2 ILE D   7      48.942 -31.088 -22.984  1.00 70.36           C
ATOM     47  CD1 ILE D   7      46.435 -32.595 -22.253  1.00 70.36           C
ATOM     48  N   VAL D   8      52.109 -31.072 -21.763  1.00 69.44           N
ATOM     49  CA  VAL D   8      53.152 -30.101 -21.449  1.00 69.44           C
ATOM     50  C   VAL D   8      52.899 -28.864 -22.300  1.00 69.44           C
ATOM     51  O   VAL D   8      52.919 -28.929 -23.535  1.00 69.44           O
ATOM     52  CB  VAL D   8      54.556 -30.659 -21.690  1.00 69.44           C
ATOM     53  CG1 VAL D   8      55.599 -29.609 -21.361  1.00 69.44           C
ATOM     54  CG2 VAL D   8      54.776 -31.908 -20.864  1.00 69.44           C
ATOM     55  N   VAL D   9      52.677 -27.732 -21.646  1.00 68.77           N
ATOM     56  CA  VAL D   9      52.487 -26.461 -22.331  1.00 68.77           C
ATOM     57  C   VAL D   9      53.673 -25.565 -22.005  1.00 68.77           C
ATOM     58  O   VAL D   9      53.934 -25.265 -20.837  1.00 68.77           O
ATOM     59  CB  VAL D   9      51.165 -25.799 -21.920  1.00 68.77           C
ATOM     60  CG1 VAL D   9      50.940 -24.540 -22.727  1.00 68.77           C
ATOM     61  CG2 VAL D   9      50.015 -26.770 -22.101  1.00 68.77           C
TER
ATOM   3324  N   ARG E   2      26.327 -71.192   2.512  1.00110.07           N
ATOM   3325  CA  ARG E   2      27.510 -71.098   3.361  1.00110.07           C
ATOM   3326  C   ARG E   2      28.757 -70.750   2.555  1.00110.07           C
ATOM   3327  O   ARG E   2      29.839 -70.577   3.112  1.00110.07           O
ATOM   3328  CB  ARG E   2      27.726 -72.403   4.134  1.00110.07           C
ATOM   3329  CG  ARG E   2      28.406 -73.521   3.355  1.00110.07           C
ATOM   3330  CD  ARG E   2      27.512 -74.077   2.258  1.00110.07           C
ATOM   3331  NE  ARG E   2      28.134 -75.194   1.561  1.00110.07           N
ATOM   3332  CZ  ARG E   2      28.987 -75.070   0.554  1.00110.07           C
ATOM   3333  NH1 ARG E   2      29.327 -73.883   0.079  1.00110.07           N
ATOM   3334  NH2 ARG E   2      29.509 -76.163   0.009  1.00110.07           N
ATOM   3335  N   GLU E   3      28.597 -70.644   1.241  1.00110.02           N
ATOM   3336  CA  GLU E   3      29.676 -70.236   0.353  1.00110.02           C
ATOM   3337  C   GLU E   3      29.967 -68.748   0.506  1.00110.02           C
ATOM   3338  O   GLU E   3      29.060 -67.945   0.751  1.00110.02           O
ATOM   3339  CB  GLU E   3      29.317 -70.584  -1.091  1.00110.02           C
ATOM   3340  CG  GLU E   3      27.935 -70.127  -1.497  1.00110.02           C
ATOM   3341  CD  GLU E   3      27.425 -70.850  -2.722  1.00110.02           C
ATOM   3342  OE1 GLU E   3      28.198 -71.621  -3.323  1.00110.02           O
ATOM   3343  OE2 GLU E   3      26.236 -70.687  -3.056  1.00110.02           O
ATOM   3344  N   ILE E   4      31.243 -68.391   0.364  1.00101.18           N
ATOM   3345  CA  ILE E   4      31.752 -67.066   0.707  1.00101.18           C
ATOM   3346  C   ILE E   4      32.439 -66.472  -0.514  1.00101.18           C
ATOM   3347  O   ILE E   4      33.255 -67.138  -1.161  1.00101.18           O
ATOM   3348  CB  ILE E   4      32.726 -67.122   1.895  1.00101.18           C
ATOM   3349  CG1 ILE E   4      32.019 -67.639   3.146  1.00101.18           C
ATOM   3350  CG2 ILE E   4      33.328 -65.754   2.158  1.00101.18           C
ATOM   3351  CD1 ILE E   4      32.961 -67.963   4.278  1.00101.18           C
ATOM   3352  N   LEU E   5      32.116 -65.218  -0.818  1.00 96.11           N
ATOM   3353  CA  LEU E   5      32.762 -64.490  -1.901  1.00 96.11           C
ATOM   3354  C   LEU E   5      33.880 -63.615  -1.347  1.00 96.11           C
ATOM   3355  O   LEU E   5      33.714 -62.965  -0.312  1.00 96.11           O
ATOM   3356  CB  LEU E   5      31.737 -63.630  -2.638  1.00 96.11           C
ATOM   3357  CG  LEU E   5      32.216 -62.660  -3.720  1.00 96.11           C
ATOM   3358  CD1 LEU E   5      33.152 -63.334  -4.702  1.00 96.11           C
ATOM   3359  CD2 LEU E   5      31.033 -62.053  -4.443  1.00 96.11           C
ATOM   3360  N   SER E   6      35.015 -63.596  -2.037  1.00 90.33           N
ATOM   3361  CA  SER E   6      36.150 -62.767  -1.660  1.00 90.33           C
ATOM   3362  C   SER E   6      36.214 -61.542  -2.562  1.00 90.33           C
ATOM   3363  O   SER E   6      35.886 -61.617  -3.748  1.00 90.33           O
ATOM   3364  CB  SER E   6      37.458 -63.552  -1.752  1.00 90.33           C
ATOM   3365  OG  SER E   6      37.462 -64.636  -0.842  1.00 90.33           O
ATOM   3366  N   ILE E   7      36.622 -60.412  -1.995  1.00 87.07           N
ATOM   3367  CA  ILE E   7      36.703 -59.148  -2.718  1.00 87.07           C
ATOM   3368  C   ILE E   7      38.064 -58.536  -2.427  1.00 87.07           C
ATOM   3369  O   ILE E   7      38.403 -58.301  -1.262  1.00 87.07           O
ATOM   3370  CB  ILE E   7      35.575 -58.192  -2.307  1.00 87.07           C
ATOM   3371  CG1 ILE E   7      34.220 -58.793  -2.669  1.00 87.07           C
ATOM   3372  CG2 ILE E   7      35.753 -56.845  -2.980  1.00 87.07           C
ATOM   3373  CD1 ILE E   7      33.057 -58.058  -2.079  1.00 87.07           C
ATOM   3374  N   HIS E   8      38.843 -58.271  -3.473  1.00 86.24           N
ATOM   3375  CA  HIS E   8      40.201 -57.769  -3.303  1.00 86.24           C
ATOM   3376  C   HIS E   8      40.343 -56.427  -4.004  1.00 86.24           C
ATOM   3377  O   HIS E   8      40.213 -56.350  -5.230  1.00 86.24           O
ATOM   3378  CB  HIS E   8      41.214 -58.772  -3.844  1.00 86.24           C
ATOM   3379  CG  HIS E   8      40.956 -60.178  -3.405  1.00 86.24           C
ATOM   3380  ND1 HIS E   8      41.328 -60.649  -2.165  1.00 86.24           N
ATOM   3381  CD2 HIS E   8      40.376 -61.220  -4.044  1.00 86.24           C
ATOM   3382  CE1 HIS E   8      40.983 -61.919  -2.057  1.00 86.24           C
ATOM   3383  NE2 HIS E   8      40.404 -62.290  -3.184  1.00 86.24           N
ATOM   3384  N   VAL E   9      40.655 -55.387  -3.233  1.00 85.55           N
ATOM   3385  CA  VAL E   9      40.698 -54.015  -3.729  1.00 85.55           C
ATOM   3386  C   VAL E   9      42.088 -53.441  -3.490  1.00 85.55           C
ATOM   3387  O   VAL E   9      42.690 -53.671  -2.435  1.00 85.55           O
ATOM   3388  CB  VAL E   9      39.620 -53.132  -3.075  1.00 85.55           C
ATOM   3389  CG1 VAL E   9      39.861 -52.977  -1.603  1.00 85.55           C
ATOM   3390  CG2 VAL E   9      39.572 -51.769  -3.743  1.00 85.55           C
TER
END
"""

eff_str="""
reference_model {
  enabled = True
  reference_group {
    reference = chain D
    selection = chain A
    file_name = '%s'
  }
  reference_group {
    reference = chain E
    selection = chain B
    file_name = '%s'
  }
}
"""


def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  Users tend to misunderstand that reference_model.file is a mandatory field
  where all reference models need to be listed and reference_group.file_name
  is to be used to clarify what file should be used for the particular reference
  group. We decided to forgive this misinterpretation. This test is to ensure
  that even if users don't have reference_model.file specified,
  we extract the paths to reference models from reference_group.file_name.
  """
  pdb_file = "%s.pdb"%prefix
  mtz_file = "%s.mtz"%prefix
  ref_file = "%s_reference.pdb"%prefix
  eff_file = "%s.eff"%prefix
  #
  with open(pdb_file,"w") as fo:
    fo.write(pdb_str)
  with open(ref_file,"w") as fo:
    fo.write(pdb_str_ref)
  with open(eff_file,"w") as fo:
    fo.write(eff_str%(ref_file,ref_file))
  #
  cmd = " ".join([
    "phenix.fmodel",
    pdb_file,
    "file_name=%s"%mtz_file,
    "high_res=4",
    ">%s.zlog"%prefix])
  assert easy_run.call(cmd)==0
  #
  args = [pdb_file, mtz_file, eff_file, "macro_cycles=0"]
  r = run_real_space_refine(args = args, prefix = prefix)
  assert_lines_in_file(r.log, """\
      Model:              Reference:
      LYS A   2  <=====>  LYS D   2
      VAL A   3  <=====>  VAL D   3
      ASN A   4  <=====>  ASN D   4
      ASN A   5  <=====>  ASN D   5
      THR A   6  <=====>  THR D   6
      ILE A   7  <=====>  ILE D   7
      VAL A   8  <=====>  VAL D   8
      VAL A   9  <=====>  VAL D   9
      ARG B   2  <=====>  ARG E   2
      GLU B   3  <=====>  GLU E   3
      ILE B   4  <=====>  ILE E   4
      LEU B   5  <=====>  LEU E   5
      SER B   6  <=====>  SER E   6
      ILE B   7  <=====>  ILE E   7
      HIS B   8  <=====>  HIS E   8
      VAL B   9  <=====>  VAL E   9
      """)

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK time =%8.3f"%(time.time() - t0))
