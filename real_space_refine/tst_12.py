from __future__ import print_function
import time
from libtbx import easy_run
import iotbx.pdb
from scitbx.array_family import flex
from scitbx.math import superpose
import os
from phenix_regression.real_space_refine import run_real_space_refine

pdb_str = """\n
CRYST1   34.917   22.246   44.017  90.00  90.00  90.00 P 1
SCALE1      0.028639  0.000000  0.000000        0.00000
SCALE2      0.000000  0.044952  0.000000        0.00000
SCALE3      0.000000  0.000000  0.022718        0.00000
ATOM      1  N   ALA A   1      27.136  16.218  30.659  1.00 10.00           N
ATOM      2  CA  ALA A   1      26.381  15.519  31.495  1.00 10.00           C
ATOM      3  C   ALA A   1      26.329  14.304  30.876  1.00 10.00           C
ATOM      4  O   ALA A   1      26.474  14.091  29.559  1.00 10.00           O
ATOM      5  CB  ALA A   1      24.801  15.569  31.338  1.00 10.00           C
ATOM      6  N   ALA A   2      26.787  12.999  31.571  1.00 10.00           N
ATOM      7  CA  ALA A   2      26.967  11.603  30.978  1.00 10.00           C
ATOM      8  C   ALA A   2      25.992  10.506  30.720  1.00 10.00           C
ATOM      9  O   ALA A   2      25.567   9.619  29.645  1.00 10.00           O
ATOM     10  CB  ALA A   2      28.444  10.893  31.700  1.00 10.00           C
ATOM     11  N   ALA A   3      24.750  11.194  31.923  1.00 10.00           N
ATOM     12  CA  ALA A   3      23.644  10.334  31.475  1.00 10.00           C
ATOM     13  C   ALA A   3      22.824  11.236  30.470  1.00 10.00           C
ATOM     14  O   ALA A   3      22.680  10.126  29.472  1.00 10.00           O
ATOM     15  CB  ALA A   3      22.683  10.883  32.806  1.00 10.00           C
ATOM     16  N   ALA A   4      22.576  12.256  30.057  1.00 10.00           N
ATOM     17  CA  ALA A   4      22.567  13.323  29.126  1.00 10.00           C
ATOM     18  C   ALA A   4      23.320  12.766  27.575  1.00 10.00           C
ATOM     19  O   ALA A   4      22.647  13.035  26.737  1.00 10.00           O
ATOM     20  CB  ALA A   4      22.665  14.791  29.108  1.00 10.00           C
ATOM     21  N   ALA A   5      24.695  12.857  28.071  1.00 10.00           N
ATOM     22  CA  ALA A   5      25.154  12.428  26.407  1.00 10.00           C
ATOM     23  C   ALA A   5      25.264  11.109  25.924  1.00 10.00           C
ATOM     24  O   ALA A   5      25.344  10.461  24.859  1.00 10.00           O
ATOM     25  CB  ALA A   5      26.887  12.830  26.730  1.00 10.00           C
ATOM     26  N   ALA A   6      25.102   9.914  26.933  1.00 10.00           N
ATOM     27  CA  ALA A   6      24.761   8.690  26.894  1.00 10.00           C
ATOM     28  C   ALA A   6      23.294   8.178  26.089  1.00 10.00           C
ATOM     29  O   ALA A   6      23.592   7.790  25.342  1.00 10.00           O
ATOM     30  CB  ALA A   6      24.787   8.117  28.066  1.00 10.00           C
ATOM     31  N   ALA A   7      22.655   9.271  26.383  1.00 10.00           N
ATOM     32  CA  ALA A   7      21.296   9.040  25.932  1.00 10.00           C
ATOM     33  C   ALA A   7      21.344   9.609  24.823  1.00 10.00           C
ATOM     34  O   ALA A   7      20.874   9.326  23.758  1.00 10.00           O
ATOM     35  CB  ALA A   7      19.996   9.531  27.146  1.00 10.00           C
ATOM     36  N   ALA A   8      22.449  10.882  24.514  1.00 10.00           N
ATOM     37  CA  ALA A   8      22.443  11.082  23.294  1.00 10.00           C
ATOM     38  C   ALA A   8      23.415  10.340  22.696  1.00 10.00           C
ATOM     39  O   ALA A   8      23.585  10.967  21.012  1.00 10.00           O
ATOM     40  CB  ALA A   8      23.176  12.809  23.828  1.00 10.00           C
ATOM     41  N   ALA A   9      23.823   9.562  22.891  1.00 10.00           N
ATOM     42  CA  ALA A   9      24.585   8.654  21.975  1.00 10.00           C
ATOM     43  C   ALA A   9      24.297   7.686  21.187  1.00 10.00           C
ATOM     44  O   ALA A   9      24.269   7.384  20.493  1.00 10.00           O
ATOM     45  CB  ALA A   9      25.660   8.159  22.973  1.00 10.00           C
ATOM     46  N   ALA A  10      23.762   6.797  22.123  1.00 10.00           N
ATOM     47  CA  ALA A  10      22.843   5.574  21.574  1.00 10.00           C
ATOM     48  C   ALA A  10      21.382   6.490  21.214  1.00 10.00           C
ATOM     49  O   ALA A  10      20.870   5.856  20.018  1.00 10.00           O
ATOM     50  CB  ALA A  10      22.349   4.965  22.563  1.00 10.00           C
TER
ATOM     51  N   ALA B   1      16.369  17.597  35.791  1.00 50.00           N
ATOM     52  CA  ALA B   1      16.888  15.966  36.619  1.00 50.00           C
ATOM     53  C   ALA B   1      17.048  14.967  35.075  1.00 50.00           C
ATOM     54  O   ALA B   1      16.283  15.612  34.280  1.00 50.00           O
ATOM     55  CB  ALA B   1      15.588  15.676  37.262  1.00 50.00           C
ATOM     56  N   ALA B   2      17.418  13.962  35.059  1.00 50.00           N
ATOM     57  CA  ALA B   2      17.719  13.080  34.288  1.00 50.00           C
ATOM     58  C   ALA B   2      16.513  12.342  33.735  1.00 50.00           C
ATOM     59  O   ALA B   2      16.509  12.007  32.254  1.00 50.00           O
ATOM     60  CB  ALA B   2      19.383  12.734  34.531  1.00 50.00           C
ATOM     61  N   ALA B   3      15.980  11.387  34.558  1.00 50.00           N
ATOM     62  CA  ALA B   3      14.532  10.959  34.115  1.00 50.00           C
ATOM     63  C   ALA B   3      13.669  11.544  33.539  1.00 50.00           C
ATOM     64  O   ALA B   3      13.544  11.449  32.275  1.00 50.00           O
ATOM     65  CB  ALA B   3      14.078  10.056  35.335  1.00 50.00           C
ATOM     66  N   ALA B   4      13.361  12.529  34.481  1.00 50.00           N
ATOM     67  CA  ALA B   4      12.247  13.527  33.948  1.00 50.00           C
ATOM     68  C   ALA B   4      12.466  14.474  32.468  1.00 50.00           C
ATOM     69  O   ALA B   4      11.840  14.533  31.619  1.00 50.00           O
ATOM     70  CB  ALA B   4      11.726  14.702  34.835  1.00 50.00           C
ATOM     71  N   ALA B   5      14.129  14.610  32.733  1.00 50.00           N
ATOM     72  CA  ALA B   5      14.183  15.310  31.161  1.00 50.00           C
ATOM     73  C   ALA B   5      14.759  14.633  30.166  1.00 50.00           C
ATOM     74  O   ALA B   5      14.656  15.046  29.197  1.00 50.00           O
ATOM     75  CB  ALA B   5      15.756  16.283  31.659  1.00 50.00           C
ATOM     76  N   ALA B   6      15.715  14.017  30.246  1.00 50.00           N
ATOM     77  CA  ALA B   6      16.810  12.752  29.514  1.00 50.00           C
ATOM     78  C   ALA B   6      15.514  12.105  29.036  1.00 50.00           C
ATOM     79  O   ALA B   6      15.567  11.940  27.491  1.00 50.00           O
ATOM     80  CB  ALA B   6      17.839  12.433  30.132  1.00 50.00           C
ATOM     81  N   ALA B   7      14.552  11.712  29.724  1.00 50.00           N
ATOM     82  CA  ALA B   7      13.249  10.547  29.551  1.00 50.00           C
ATOM     83  C   ALA B   7      12.304  11.061  29.067  1.00 50.00           C
ATOM     84  O   ALA B   7      11.751  10.698  27.910  1.00 50.00           O
ATOM     85  CB  ALA B   7      13.112   9.844  30.575  1.00 50.00           C
ATOM     86  N   ALA B   8      11.716  12.664  29.587  1.00 50.00           N
ATOM     87  CA  ALA B   8      10.318  13.303  28.896  1.00 50.00           C
ATOM     88  C   ALA B   8      10.938  14.033  27.835  1.00 50.00           C
ATOM     89  O   ALA B   8      10.033  14.136  26.971  1.00 50.00           O
ATOM     90  CB  ALA B   8      10.027  14.143  29.737  1.00 50.00           C
ATOM     91  N   ALA B   9      12.148  14.426  27.612  1.00 50.00           N
ATOM     92  CA  ALA B   9      12.564  15.045  26.226  1.00 50.00           C
ATOM     93  C   ALA B   9      12.834  13.857  25.576  1.00 50.00           C
ATOM     94  O   ALA B   9      12.039  13.993  24.565  1.00 50.00           O
ATOM     95  CB  ALA B   9      14.302  15.579  26.878  1.00 50.00           C
ATOM     96  N   ALA B  10      13.479  12.612  25.298  1.00 50.00           N
ATOM     97  CA  ALA B  10      14.112  11.740  24.590  1.00 50.00           C
ATOM     98  C   ALA B  10      12.796  10.775  24.118  1.00 50.00           C
ATOM     99  O   ALA B  10      12.716  10.229  23.588  1.00 50.00           O
ATOM    100  CB  ALA B  10      14.962  10.463  24.914  1.00 50.00           C
TER
END
"""

def get_dist(pdb_file, sel1, sel2):
  pdb_inp = iotbx.pdb.input(file_name=pdb_file)
  ph = pdb_inp.construct_hierarchy()
  xyz = ph.atoms().extract_xyz()
  asc = ph.atom_selection_cache()
  s1 = ph.atom_selection_cache().selection(string=sel1)
  s2 = ph.atom_selection_cache().selection(string=sel2)
  xyz1 = xyz.select(s1)
  xyz2 = xyz.select(s2)
  fit = superpose.least_squares_fit(
    reference_sites = xyz1,
    other_sites     = xyz2)
  return flex.mean(flex.sqrt((xyz1-fit.other_sites_best_fit()).dot()))

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  phenix.real_space_refine as geometry minimization (w=0), with and w/o NCS
  constraints.
  """
  pdb_in = "%s.pdb"%prefix
  fo = open(pdb_in, "w")
  print(pdb_str, file=fo)
  fo.close()
  # Check input PDB is what we want
  d = get_dist(pdb_file=pdb_in, sel1="chain A", sel2="chain B")
  assert d > 1.0, d
  #
  cmd = " ".join([
    "phenix.fmodel",
    "%s"%pdb_in,
    "high_res=3",
    "low_res=3.5",
    "output.file_name=%s.mtz"%prefix,
    "> zlog1"])
  assert easy_run.call(cmd)==0
  #
  for i, ncs_constraints in enumerate([True, False]):
    print("test:", i)
    args = [
      "cdl=False",
      "%s.{pdb,mtz}"%prefix,
      "c_beta_restraints=False",
      "ramachandran_plot_restraints.enabled=False",
      "rotamers.restraints.enabled=False",
      "secondary_structure.enabled=False",
      "ncs_constraints=%s"%str(ncs_constraints),
      "refinement.macro_cycles=2",
      "refinement.max_iterations=50",
      "weight=0"]
    r = run_real_space_refine(args = args, prefix = "%s_%s"%(prefix,str(i)))
    # check for expected output
    lines = r.get_lines(fn = r.std_out)
    result_pdb = r.pdb
    d = get_dist(
      pdb_file = result_pdb,
      sel1     = "chain A",
      sel2     = "chain B")
    if(ncs_constraints):
      assert d < 0.005, d
    else:
      assert d > 1.0, d
    # check bond/angle rmsds
    #r = easy_run.fully_buffered("phenix.pdbtools cdl=False model_stat=true %s"%result_pdb)
    a_rmsd, b_rmsd = None, None
    for l in lines:
      if(l.strip().startswith("Bond      :")): b_rmsd = float(l.strip().split()[2])
      if(l.strip().startswith("Angle     :")): a_rmsd = float(l.strip().split()[2])
    assert a_rmsd is not None
    assert b_rmsd is not None
    assert a_rmsd < 0.4, a_rmsd
    assert b_rmsd < 0.005, b_rmsd

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK time =%8.3f"%(time.time() - t0))
