from __future__ import print_function
import time, os
from libtbx import easy_run
import iotbx.pdb
import libtbx.load_env
from scitbx.array_family import flex
from phenix_regression.real_space_refine import run_real_space_refine

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refined")):
  """
  Make sure well placed in density side chains do not move out. One of them is a
  rotamer outlier but justified by the map.
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/real_space_refine/data/tst_42.pdb",
    test=os.path.isfile)
  dat = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/real_space_refine/data/tst_42.ccp4",
    test=os.path.isfile)
  #
  args = [
    "%s"%pdb,
    "%s"%dat,
    "macro_cycles=1",
    "resolution=2.3",
    "run=minimization_global+local_grid_search"]
  r = run_real_space_refine(args = args, prefix = prefix)
  # check results
  from iotbx import mrcfile
  map_data = mrcfile.map_reader(file_name=dat).map_data()
  pdb_inp = iotbx.pdb.input(file_name = r.pdb)
  xrs = pdb_inp.xray_structure_simple()
  sites_frac = xrs.sites_frac()
  ph = pdb_inp.construct_hierarchy()
  asc = ph.atom_selection_cache()
  #
  ss="chain C and resseq 379 and not (element H or name CA or name O or name C or name N)"
  sel=asc.selection(ss)
  mv = flex.double()
  for sf in sites_frac.select(sel):
    mv.append( map_data.eight_point_interpolation(sf) )
  mi,ma,me = mv.min_max_mean().as_tuple()
  print("1: mi,ma,me:", mi,ma,me)
  assert mi<0.04 , mi # XXX DO NOT CHANGE WITHOUT LOOKING AT THE MAP!
  assert ma<0.051, ma # XXX DO NOT CHANGE WITHOUT LOOKING AT THE MAP!
  #
  ss="chain F and resseq 33 and not (element H or name CA or name O or name C or name N)"
  sel=asc.selection(ss)
  mv = flex.double()
  for sf in sites_frac.select(sel):
    mv.append( map_data.eight_point_interpolation(sf) )
  mi,ma,me = mv.min_max_mean().as_tuple()
  print("2: mi,ma,me:", mi,ma,me)
  assert mi<0.033, mi # XXX DO NOT CHANGE WITHOUT LOOKING AT THE MAP!
  assert ma<0.07, ma # XXX DO NOT CHANGE WITHOUT LOOKING AT THE MAP!
  assert me<0.05, me # XXX DO NOT CHANGE WITHOUT LOOKING AT THE MAP!

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK time =%8.3f"%(time.time() - t0))
