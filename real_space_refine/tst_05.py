from __future__ import print_function
from libtbx import easy_run
import time
import iotbx.pdb
from scitbx.array_family import flex
import os
from phenix_regression.real_space_refine import run_real_space_refine

pdb_str_answer = """\
CRYST1   26.628   30.419   28.493  90.00  90.00  90.00 P 1
ATOM      1  N   THR A   1      15.638  20.419  12.645  1.00 10.00           N
ATOM      2  CA  THR A   1      15.527  19.061  12.125  1.00 10.00           C
ATOM      3  C   THR A   1      15.602  18.033  13.249  1.00 10.00           C
ATOM      4  O   THR A   1      16.417  18.157  14.163  1.00 10.00           O
ATOM      5  CB  THR A   1      16.628  18.760  11.092  1.00 10.00           C
ATOM      6  OG1 THR A   1      16.528  19.682  10.000  1.00 10.00           O
ATOM      7  CG2 THR A   1      16.491  17.340  10.565  1.00 10.00           C
TER
ATOM      1  N   THR B   1      10.624  17.093  17.237  1.00 10.00           N
ATOM      2  CA  THR B   1      11.140  16.654  15.945  1.00 10.00           C
ATOM      3  C   THR B   1      12.494  15.968  16.097  1.00 10.00           C
ATOM      4  O   THR B   1      13.351  16.426  16.852  1.00 10.00           O
ATOM      5  CB  THR B   1      11.279  17.832  14.963  1.00 10.00           C
ATOM      6  OG1 THR B   1      10.000  18.446  14.765  1.00 10.00           O
ATOM      7  CG2 THR B   1      11.818  17.350  13.625  1.00 10.00           C
TER
ATOM      1  N   THR C   1      12.949  10.000  18.493  1.00 10.00           N
ATOM      2  CA  THR C   1      12.735  10.702  17.233  1.00 10.00           C
ATOM      3  C   THR C   1      13.937  11.570  16.875  1.00 10.00           C
ATOM      4  O   THR C   1      14.508  12.241  17.734  1.00 10.00           O
ATOM      5  CB  THR C   1      11.474  11.584  17.284  1.00 10.00           C
ATOM      6  OG1 THR C   1      10.328  10.770  17.559  1.00 10.00           O
ATOM      7  CG2 THR C   1      11.273  12.305  15.960  1.00 10.00           C
TER
END
"""

pdb_str_poor = """\
CRYST1   26.628   30.419   28.493  90.00  90.00  90.00 P 1
ATOM      1  N   THR A   1      15.886  19.796  13.070  1.00 10.00           N
ATOM      2  CA  THR A   1      15.489  18.833  12.050  1.00 10.00           C
ATOM      3  C   THR A   1      15.086  17.502  12.676  1.00 10.00           C
ATOM      4  O   THR A   1      15.739  17.017  13.600  1.00 10.00           O
ATOM      5  CB  THR A   1      16.619  18.590  11.033  1.00 10.00           C
ATOM      6  OG1 THR A   1      16.963  19.824  10.392  1.00 10.00           O
ATOM      7  CG2 THR A   1      16.182  17.583   9.980  1.00 10.00           C
TER       8      THR A   1
ATOM      1  N   THR B   1      10.028  17.193  16.617  1.00 10.00           N
ATOM      2  CA  THR B   1      11.046  16.727  15.681  1.00 10.00           C
ATOM      3  C   THR B   1      12.336  16.360  16.407  1.00 10.00           C
ATOM      4  O   THR B   1      12.772  17.068  17.313  1.00 10.00           O
remark ATOM      5  CB  THR B   1      11.356  17.789  14.609  1.00 10.00           C
remark ATOM      6  OG1 THR B   1      10.163  18.098  13.879  1.00 10.00           O
remark ATOM      7  CG2 THR B   1      12.418  17.281  13.646  1.00 10.00           C
TER      16      THR B   1
ATOM      1  N   THR C   1      12.121   9.329  18.086  1.00 10.00           N
ATOM      2  CA  THR C   1      12.245  10.284  16.991  1.00 10.00           C
ATOM      3  C   THR C   1      13.707  10.622  16.718  1.00 10.00           C
ATOM      4  O   THR C   1      14.493  10.814  17.645  1.00 10.00           O
ATOM      5  CB  THR C   1      11.474  11.584  17.284  1.00 10.00           C
ATOM      6  OG1 THR C   1      10.087  11.287  17.482  1.00 10.00           O
ATOM      7  CG2 THR C   1      11.619  12.563  16.129  1.00 10.00           C
TER      24      THR C   1
END
"""

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  NCS constraints: full ASU in input, NCS copies contain different amount of
  atoms.
  """
  pdb_file_name_answer = "%s_answer.pdb"%prefix
  of=open(pdb_file_name_answer, "w")
  print(pdb_str_answer, file=of)
  of.close()
  #
  pdb_file_name_poor = "%s_poor.pdb"%prefix
  of=open(pdb_file_name_poor, "w")
  print(pdb_str_poor, file=of)
  of.close()
  #
  cmd = " ".join([
    "phenix.fmodel",
    "%s"%pdb_file_name_answer,
    "file_name=%s.mtz"%prefix,
    "high_res=1",
    ">%s.zlog"%prefix])
  assert easy_run.call(cmd)==0
  #
  args = [
    "run=minimization_global",
    "%s"%pdb_file_name_poor,
    "%s.mtz"%prefix]
  r = run_real_space_refine(args = args, prefix = prefix)
  #
  f = open(r.log,"r")
  cc_final = None
  for l in f.readlines():
    l = l.strip()
    if(l.count("CC_mask:")):
      cc_final = float(l.split()[len(l.split())-1])
  assert cc_final > 0.85
  f.close()

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
