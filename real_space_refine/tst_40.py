from __future__ import print_function
from libtbx import easy_run
import time
import iotbx.pdb
from scitbx.array_family import flex
from libtbx.test_utils import approx_equal
import random
import os
from phenix_regression.real_space_refine import run_real_space_refine

pdb_str = """\
CRYST1   36.247   27.495   53.665  90.00  90.00  90.00 P 1
ATOM     29  C1  CHL A 901      17.682   7.119  31.050  1.00 70.75           C
ATOM     30  C10 CHL A 901      20.205   6.314  33.961  1.00 70.75           C
ATOM     31  C11 CHL A 901      20.456   7.185  35.201  1.00 70.75           C
ATOM     32  C12 CHL A 901      19.042   7.612  35.561  1.00 70.75           C
ATOM     33  C13 CHL A 901      18.445   8.029  34.201  1.00 70.75           C
ATOM     34  C14 CHL A 901      16.941   8.208  34.440  1.00 70.75           C
ATOM     35  C15 CHL A 901      16.648   9.228  35.547  1.00 70.75           C
ATOM     36  C16 CHL A 901      17.338   8.891  36.879  1.00 70.75           C
ATOM     37  C17 CHL A 901      18.842   8.620  36.684  1.00 70.75           C
ATOM     38  C18 CHL A 901      19.445   8.100  37.983  1.00 70.75           C
ATOM     39  C19 CHL A 901      19.004   8.893  39.170  1.00 70.75           C
ATOM     40  C2  CHL A 901      19.016   6.990  31.780  1.00 70.75           C
ATOM     41  C20 CHL A 901      17.989   9.742  39.175  1.00 70.75           C
ATOM     42  C21 CHL A 901      17.071   9.962  37.976  1.00 70.75           C
ATOM     43  C22 CHL A 901      15.609   9.830  38.462  1.00 70.75           C
ATOM     44  C23 CHL A 901      15.292  10.649  39.715  1.00 70.75           C
ATOM     45  C24 CHL A 901      16.220  10.310  40.854  1.00 70.75           C
ATOM     46  C25 CHL A 901      17.655  10.532  40.420  1.00 70.75           C
ATOM     47  C26 CHL A 901      17.316  11.388  37.448  1.00 70.75           C
ATOM     48  C27 CHL A 901      19.077   9.326  33.668  1.00 70.75           C
ATOM     49  C3  CHL A 901      19.847   5.852  31.167  1.00 70.75           C
ATOM     50  C4  CHL A 901      20.425   6.141  29.806  1.00 70.75           C
ATOM     51  C5  CHL A 901      21.246   5.000  29.262  1.00 70.75           C
ATOM     52  C6  CHL A 901      22.048   5.307  27.997  1.00 70.75           C
ATOM     53  C7  CHL A 901      21.150   5.788  26.870  1.00 70.75           C
ATOM     54  C8  CHL A 901      23.148   6.318  28.271  1.00 70.75           C
ATOM     55  C9  CHL A 901      18.886   6.822  33.309  1.00 70.75           C
ATOM     56  O1  CHL A 901      15.925  11.135  41.989  1.00 70.75           O
TER
ATOM     57  N   ARG B 180      24.876  16.918  44.547  1.00 83.97           N
ATOM     58  CA  ARG B 180      26.220  16.446  44.238  1.00 83.97           C
ATOM     59  C   ARG B 180      26.149  15.304  43.234  1.00 83.97           C
ATOM     60  O   ARG B 180      26.584  15.433  42.095  1.00 83.97           O
ATOM     61  CB  ARG B 180      26.945  15.992  45.510  1.00 83.97           C
ATOM     62  CG  ARG B 180      28.463  16.016  45.407  1.00 83.97           C
ATOM     63  CD  ARG B 180      28.998  17.443  45.381  1.00 83.97           C
ATOM     64  NE  ARG B 180      28.991  18.065  46.703  1.00 83.97           N
ATOM     65  CZ  ARG B 180      30.065  18.195  47.478  1.00 83.97           C
ATOM     38  NH1 ARG B 180      31.247  17.755  47.067  1.00 83.97           N
ATOM     39  NH2 ARG B 180      29.960  18.773  48.665  1.00 83.97           N
ATOM     68  N   THR B 181      25.587  14.182  43.672  1.00 85.81           N
ATOM     69  CA  THR B 181      25.416  13.019  42.816  1.00 85.81           C
ATOM     70  C   THR B 181      24.214  13.232  41.900  1.00 85.81           C
ATOM     71  O   THR B 181      24.073  12.560  40.874  1.00 85.81           O
ATOM     72  CB  THR B 181      25.216  11.775  43.686  1.00 85.81           C
ATOM     73  OG1 THR B 181      26.059  11.870  44.841  1.00 85.81           O
ATOM     74  CG2 THR B 181      25.581  10.492  42.920  1.00 85.81           C
TER
END
"""

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  Do not require ligand cif if run=rigid_body.
  """
  #
  pdb_inp = iotbx.pdb.input(source_info=None, lines=pdb_str)
  sites_cart_1 = pdb_inp.atoms().extract_xyz()
  ph = pdb_inp.construct_hierarchy()
  ph.write_pdb_file(file_name="%s.pdb"%prefix,
    crystal_symmetry=pdb_inp.crystal_symmetry())
  #
  cmd = " ".join([
    "phenix.fmodel",
    "%s.pdb"%prefix,
    "output.file_name=%s.mtz"%prefix,
    "high_res=2.0",
    ">%s.zlog"%prefix])
  assert easy_run.call(cmd)==0
  #
  args = [
    "macro_cycles=1",
    "run=rigid_body",
    "%s.pdb"%prefix,
    "model_format=pdb+mmcif",
    "%s.mtz"%prefix]
  r = run_real_space_refine(args = args, prefix = prefix)
  sites_cart_2 = iotbx.pdb.input(
    file_name=r.pdb).atoms().extract_xyz()
  assert flex.max(flex.sqrt((sites_cart_1 - sites_cart_2).dot())) < 1.e-3

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
