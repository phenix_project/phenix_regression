from __future__ import print_function
from libtbx import easy_run
import time
import iotbx.pdb
from scitbx.array_family import flex
from libtbx.test_utils import approx_equal
import random
import os
from phenix_regression.real_space_refine import run_real_space_refine

pdb_str = """\
CRYST1   13.565   12.977   12.928  90.00  90.00  90.00 P 1
SCALE1      0.073719  0.000000  0.000000        0.00000
SCALE2      0.000000  0.077059  0.000000        0.00000
SCALE3      0.000000  0.000000  0.077351        0.00000
ATOM      1  N   SER A   3       5.000   7.158   6.954  1.00 30.00           N
ATOM      2  CA  SER A   3       6.293   6.837   6.397  1.00 30.00           C
ATOM      3  C   SER A   3       6.375   5.372   6.086  1.00 30.00           C
ATOM      4  O   SER A   3       6.755   5.000   5.000  1.00 30.00           O
ATOM      5  CB  SER A   3       7.392   7.220   7.351  1.00 30.00           C
ATOM      6  OG  SER A   3       8.565   7.578   6.646  1.00 30.00           O
ATOM      7  HA  SER A   3       6.419   7.332   5.572  1.00 30.00           H
ATOM      8  HB2 SER A   3       7.099   7.977   7.883  1.00 30.00           H
ATOM      9  HB3 SER A   3       7.588   6.465   7.928  1.00 30.00           H
TER
END
"""

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  Make sure only one real or Fourier map is provided.
  """
  #
  with open("%s.pdb"%prefix,"w") as fo:
    fo.write(pdb_str)
  #
  for p in [1,2]:
    cmd = " ".join([
      "phenix.model_map",
      "%s.pdb"%prefix,
      "output_file_name_prefix=%s_%s"%(prefix, str(p)),
      "grid_step=2.0",
      ">%s.zlog"%prefix])
    assert easy_run.call(cmd)==0
    cmd = " ".join([
      "phenix.fmodel",
      "%s.pdb"%prefix,
      "output.file_name=%s_%s.mtz"%(prefix, str(p)),
      "high_res=3",
      "output.label=MC%s"%str(p),
      ">%s.zlog"%prefix])
    assert easy_run.call(cmd)==0

  #
  args = [
    "run=None",
    "resolution=2",
    "macro_cycles=0",
    "%s.pdb"%prefix,
    "%s_1.ccp4"%prefix,
    "%s_2.ccp4"%prefix]
  r = run_real_space_refine(args = args, prefix = prefix, sorry_expected = True)
  assert r.r.stderr_lines[0]=='Sorry: Exactly one map file is required.'
  #
  args = [
    "run=None",
    "resolution=2",
    "macro_cycles=0",
    "%s.pdb"%prefix,
    "%s_1.mtz"%prefix,
    "%s_2.mtz"%prefix,]
  r = run_real_space_refine(args = args, prefix = prefix, sorry_expected = True)
  assert r.r.stderr_lines[0]=='Sorry: One file with Fourier map coefficeints expected, several found: tst_68_real_space_refine_1.mtz tst_68_real_space_refine_2.mtz.'
  #
  args = [
    "run=None",
    "resolution=2",
    "macro_cycles=0",
    "%s.pdb"%prefix,
    "%s_1.ccp4"%prefix,
    "%s_2.mtz"%prefix]
  r = run_real_space_refine(args = args, prefix = prefix, sorry_expected = True)
  assert r.r.stderr_lines[0]=='Sorry: One real or Fourier map is required.'

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
