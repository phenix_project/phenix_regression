from __future__ import print_function
from libtbx import easy_run
import time
import iotbx.pdb
from scitbx.array_family import flex
from libtbx.test_utils import approx_equal
import random
import os
from phenix_regression.real_space_refine import run_real_space_refine

pdb_str = """\
CRYST1   91.629   92.652  119.291  90.00 106.94  90.00 C 1 2 1
SCALE1      0.010914  0.000000  0.003324        0.00000
SCALE2      0.000000  0.010793  0.000000        0.00000
SCALE3      0.000000  0.000000  0.008763        0.00000
ATOM      1  N   SER A   3      85.613  74.823  70.293  1.00 30.00           N
ATOM      2  CA  SER A   3      86.906  74.502  69.736  1.00 30.00           C
ATOM      3  C   SER A   3      86.988  73.037  69.425  1.00 30.00           C
ATOM      4  O   SER A   3      87.368  72.665  68.339  1.00 30.00           O
ATOM      5  CB  SER A   3      88.005  74.885  70.690  1.00 30.00           C
ATOM      6  OG  SER A   3      89.178  75.243  69.985  1.00 30.00           O
ATOM      7  HA  SER A   3      87.032  74.997  68.911  1.00 30.00           H
ATOM      8  HB2 SER A   3      87.712  75.642  71.222  1.00 30.00           H
ATOM      9  HB3 SER A   3      88.201  74.130  71.267  1.00 30.00           H
END
"""

pdb_str_for_map = """\
CRYST1   13.565   12.977   12.928  90.00  90.00  90.00 P 1
SCALE1      0.073719  0.000000  0.000000        0.00000
SCALE2      0.000000  0.077059  0.000000        0.00000
SCALE3      0.000000  0.000000  0.077351        0.00000
ATOM      1  N   SER A   3       5.000   7.158   6.954  1.00 30.00           N
ATOM      2  CA  SER A   3       6.293   6.837   6.397  1.00 30.00           C
ATOM      3  C   SER A   3       6.375   5.372   6.086  1.00 30.00           C
ATOM      4  O   SER A   3       6.755   5.000   5.000  1.00 30.00           O
ATOM      5  CB  SER A   3       7.392   7.220   7.351  1.00 30.00           C
ATOM      6  OG  SER A   3       8.565   7.578   6.646  1.00 30.00           O
ATOM      7  HA  SER A   3       6.419   7.332   5.572  1.00 30.00           H
ATOM      8  HB2 SER A   3       7.099   7.977   7.883  1.00 30.00           H
ATOM      9  HB3 SER A   3       7.588   6.465   7.928  1.00 30.00           H
TER
END
"""

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  Make sure crystal symmetry matches across all inputs.
  """
  #
  with open("%s.pdb"%prefix,"w") as fo:
    fo.write(pdb_str)
  with open("%s_for_map.pdb"%prefix,"w") as fo:
    fo.write(pdb_str_for_map)
  #
  cmd = " ".join([
    "phenix.model_map",
    "%s_for_map.pdb"%prefix,
    "output_file_name_prefix=%s"%prefix,
    "grid_step=2.0",
    ">%s.zlog"%prefix])
  assert easy_run.call(cmd)==0
  #
  args = [
    "run=minimization_global",
    "resolution=1",
    "macro_cycles=1",
    "%s.pdb"%prefix,
    "%s.ccp4"%prefix]
  r = run_real_space_refine(args = args, prefix = prefix, sorry_expected = True)
  #
  line = r.r.stderr_lines[-2].strip()
  assert line == \
    "Sorry: Symmetry and/or box (unit cell) dimensions mismatch between input model and map.", line

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
