from __future__ import print_function
from libtbx import easy_run
import time
import iotbx.pdb
from scitbx.array_family import flex
from libtbx.test_utils import approx_equal
import random
import os
from phenix_regression.real_space_refine import run_real_space_refine

pdb_str = """\
CRYST1   17.909   16.821   15.115  90.00  90.00  90.00 P 1
SCALE1      0.055838  0.000000  0.000000        0.00000
SCALE2      0.000000  0.059449  0.000000        0.00000
SCALE3      0.000000  0.000000  0.066159        0.00000
ATOM      1  N   ALA m 102      10.857   5.708   5.720  1.00 80.00           N
ATOM      2  CA  ALA m 102      10.723   5.000   6.987  1.00 80.00           C
ATOM      3  C   ALA m 102      11.229   5.852   8.146  1.00 80.00           C
ATOM      4  N   ALA m 103      10.299   6.485   8.860  1.00 80.00           N
ATOM      5  CA  ALA m 103      10.653   7.331   9.993  1.00 80.00           C
ATOM      6  C   ALA m 103      10.070   8.729   9.828  1.00 80.00           C
ATOM      7  N   ALA m 104       8.778   8.879  10.115  1.00 80.00           N
ATOM      8  CA  ALA m 104       8.115  10.172   9.993  1.00 80.00           C
ATOM      9  C   ALA m 104       6.684  10.001   9.499  1.00 80.00           C
ATOM     10  N   ALA m 105       6.486   9.113   8.530  1.00 80.00           N
ATOM     11  CA  ALA m 105       5.174   8.919   7.925  1.00 80.00           C
ATOM     12  C   ALA m 105       5.000   9.839   6.719  1.00 80.00           C
TER
ATOM     13  N   UNK m 102      11.608   6.663   5.131  1.00 80.00           N
ATOM     14  CA  UNK m 102      12.561   7.759   5.000  1.00 80.00           C
ATOM     15  C   UNK m 102      12.909   8.337   6.368  1.00 80.00           C
ATOM     16  N   UNK m 103      11.914   8.917   7.030  1.00 80.00           N
ATOM     17  CA  UNK m 103      12.113   9.510   8.347  1.00 80.00           C
ATOM     18  C   UNK m 103      10.774   9.773   9.029  1.00 80.00           C
ATOM     19  N   UNK m 104      10.112  10.852   8.625  1.00 80.00           N
ATOM     20  CA  UNK m 104       8.820  11.217   9.195  1.00 80.00           C
ATOM     21  C   UNK m 104       7.910  11.821   8.130  1.00 80.00           C
ATOM     22  N   UNK m 105       7.912  11.217   6.946  1.00 80.00           N
ATOM     23  CA  UNK m 105       7.033  11.647   5.866  1.00 80.00           C
ATOM     24  C   UNK m 105       5.712  10.884   5.928  1.00 80.00           C
TER
END
"""

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  Make sure this runs all the way through. This should exercise correct handling
  of UNK.
  """
  #
  pdb_inp = iotbx.pdb.input(source_info=None, lines=pdb_str)
  ph = pdb_inp.construct_hierarchy()
  ph.write_pdb_file(file_name="%s.pdb"%prefix,
    crystal_symmetry=pdb_inp.crystal_symmetry())
  #
  cmd = " ".join([
    "phenix.fmodel",
    "%s.pdb"%prefix,
    "output.file_name=%s.mtz"%prefix,
    "high_res=2.0",
    ">%s.zlog"%prefix])
  assert easy_run.call(cmd)==0
  #
  args = [
    "macro_cycles=1",
    "%s.pdb"%prefix,
    "model_format=pdb+mmcif",
    "%s.mtz"%prefix]
  r = run_real_space_refine(args = args, prefix = prefix)
  #

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
