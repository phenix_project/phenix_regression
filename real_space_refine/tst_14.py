from __future__ import print_function
import time
from libtbx import easy_run
import iotbx.pdb
from scitbx.array_family import flex
from scitbx.math import superpose
import os
from phenix_regression.real_space_refine import run_real_space_refine

pdb_str_1 = """
CRYST1   51.602   56.979   42.881  90.00  90.00  90.00 P 1
ATOM   2261  N   GLN A 112      20.155  10.674  24.967  1.00 34.87           N
ATOM   2262  CA  GLN A 112      19.864  11.677  25.984  1.00 30.79           C
ATOM   2263  CB  GLN A 112      19.642  13.047  25.339  1.00 34.60           C
ATOM   2264  CG  GLN A 112      20.839  13.571  24.560  1.00 32.47           C
ATOM   2265  CD  GLN A 112      22.040  13.846  25.445  1.00 32.75           C
ATOM   2266  OE1 GLN A 112      21.899  14.128  26.635  1.00 28.88           O
ATOM   2267  NE2 GLN A 112      23.232  13.765  24.865  1.00 35.78           N
ATOM   2268  C   GLN A 112      18.644  11.282  26.810  1.00 31.06           C
ATOM   2269  O   GLN A 112      18.771  10.654  27.861  1.00 30.14           O
ATOM   2270  N   ALA A 113      17.463  11.654  26.327  1.00 29.46           N
ATOM   2271  CA  ALA A 113      16.219  11.340  27.019  1.00 33.01           C
ATOM   2272  C   ALA A 113      15.285  10.535  26.122  1.00 28.44           C
ATOM   2273  O   ALA A 113      15.691  10.040  25.071  1.00 38.85           O
ATOM   2274  CB  ALA A 113      15.536  12.615  27.488  1.00 20.00           C
TER
ATOM   5311  N   GLN B 112      13.385  41.168  26.082  1.00 34.87           N
ATOM   5312  CA  GLN B 112      13.092  40.040  25.205  1.00 30.79           C
ATOM   5313  CB  GLN B 112      13.760  38.767  25.728  1.00 34.60           C
ATOM   5314  CG  GLN B 112      15.277  38.850  25.818  1.00 32.47           C
ATOM   5315  CD  GLN B 112      15.938  38.999  24.461  1.00 32.75           C
ATOM   5316  OE1 GLN B 112      15.395  38.573  23.441  1.00 28.88           O
ATOM   5317  NE2 GLN B 112      17.118  39.607  24.442  1.00 35.78           N
ATOM   5318  C   GLN B 112      11.588  39.827  25.072  1.00 31.06           C
ATOM   5319  O   GLN B 112      10.944  40.409  24.198  1.00 30.14           O
ATOM   5320  O   ALA B 113       9.560  39.669  28.119  1.00 30.00           O
ATOM   5321  N   ALA B 113      11.034  38.990  25.943  1.00 30.00           N
ATOM   5322  CA  ALA B 113       9.606  38.698  25.924  1.00 30.00           C
ATOM   5323  C   ALA B 113       8.972  38.986  27.281  1.00 30.00           C
ATOM   5324  CB  ALA B 113       9.366  37.251  25.521  1.00 30.00           C
TER
END
"""

pdb_str_2 = """
CRYST1   28.036   75.997   42.821  90.00  90.00  90.00 P 1
ATOM  18075  P    DT A  15       7.858  14.581  34.900  1.00 40.77           P
ATOM  18076  OP1  DT A  15       7.459  15.962  35.246  1.00 42.63           O
ATOM  18077  OP2  DT A  15       9.155  14.036  35.354  1.00 40.13           O
ATOM  18078  O5'  DT A  15       6.715  13.579  35.379  1.00 39.49           O
ATOM  18079  C5'  DT A  15       5.898  12.944  34.407  1.00 38.32           C
ATOM  18080  C4'  DT A  15       6.177  11.458  34.365  1.00 38.36           C
ATOM  18081  O4'  DT A  15       6.468  11.045  33.002  1.00 36.04           O
ATOM  18082  C3'  DT A  15       7.384  11.013  35.190  1.00 37.68           C
ATOM  18083  O3'  DT A  15       7.173   9.687  35.713  1.00 38.20           O
ATOM  18084  C2'  DT A  15       8.477  10.995  34.126  1.00 35.49           C
ATOM  18085  C1'  DT A  15       7.687  10.346  33.010  1.00 36.09           C
ATOM  18086  N1   DT A  15       8.297  10.416  31.665  1.00 34.05           N
ATOM  18087  C2   DT A  15       7.814   9.587  30.687  1.00 32.66           C
ATOM  18088  O2   DT A  15       6.909   8.800  30.873  1.00 31.60           O
ATOM  18089  N3   DT A  15       8.424   9.708  29.477  1.00 34.64           N
ATOM  18090  C4   DT A  15       9.449  10.560  29.153  1.00 35.70           C
ATOM  18091  O4   DT A  15       9.935  10.597  28.036  1.00 38.57           O
ATOM  18092  C5   DT A  15       9.915  11.403  30.222  1.00 36.35           C
ATOM  18093  C7   DT A  15      11.036  12.364  29.978  1.00 37.41           C
ATOM  18094  C6   DT A  15       9.321  11.295  31.418  1.00 35.24           C
ATOM  18095  P    DC A  16       5.911   9.303  36.642  1.00 20.00           P
ATOM  18096  OP1  DC A  16       5.000  10.444  36.870  1.00 20.00           O
ATOM  18097  OP2  DC A  16       6.464   8.610  37.821  1.00 20.00           O
ATOM  18098  O5'  DC A  16       5.170   8.168  35.793  1.00 20.00           O
ATOM  18099  O3'  DC A  16       7.323   5.000  35.657  1.00 20.00           O
ATOM  18100  C1'  DC A  16       8.515   6.462  33.412  1.00 20.00           C
ATOM  18101  C2'  DC A  16       8.643   6.870  34.868  1.00 20.00           C
ATOM  18102  C3'  DC A  16       7.304   6.390  35.408  1.00 20.00           C
ATOM  18103  C4'  DC A  16       6.354   6.694  34.252  1.00 20.00           C
ATOM  18104  C5'  DC A  16       5.526   7.957  34.420  1.00 20.00           C
ATOM  18105  O4'  DC A  16       7.194   6.803  33.065  1.00 20.00           O
ATOM  18106  N1   DC A  16       9.465   7.176  32.515  1.00 20.00           N
ATOM  18107  C2   DC A  16       9.534   6.858  31.147  1.00 20.00           C
ATOM  18108  N3   DC A  16      10.414   7.529  30.364  1.00 20.00           N
ATOM  18109  C4   DC A  16      11.199   8.464  30.899  1.00 20.00           C
ATOM  18110  C5   DC A  16      11.148   8.792  32.284  1.00 20.00           C
ATOM  18111  C6   DC A  16      10.281   8.129  33.045  1.00 20.00           C
ATOM  18112  O2   DC A  16       8.793   5.982  30.685  1.00 20.00           O
ATOM  18113  N4   DC A  16      12.049   9.099  30.096  1.00 20.00           N
TER
ATOM  18442  P    DT B  15      19.602  60.528   6.964  1.00 40.77           P
ATOM  18443  OP1  DT B  15      20.917  60.400   7.624  1.00 42.63           O
ATOM  18444  OP2  DT B  15      19.266  59.670   5.807  1.00 40.13           O
ATOM  18445  O5'  DT B  15      19.375  62.044   6.531  1.00 39.49           O
ATOM  18446  C5'  DT B  15      18.373  62.805   7.185  1.00 38.32           C
ATOM  18447  C4'  DT B  15      18.842  64.226   7.405  1.00 38.36           C
ATOM  18448  O4'  DT B  15      18.487  64.658   8.746  1.00 36.04           O
ATOM  18449  C3'  DT B  15      20.344  64.419   7.300  1.00 37.68           C
ATOM  18450  O3'  DT B  15      20.609  65.708   6.829  1.00 38.20           O
ATOM  18451  C2'  DT B  15      20.798  64.257   8.743  1.00 35.49           C
ATOM  18452  C1'  DT B  15      19.660  64.933   9.492  1.00 36.09           C
ATOM  18453  N1   DT B  15      19.465  64.394  10.854  1.00 34.05           N
ATOM  18454  C2   DT B  15      18.556  64.990  11.695  1.00 32.66           C
ATOM  18455  O2   DT B  15      17.908  65.963  11.385  1.00 31.60           O
ATOM  18456  N3   DT B  15      18.447  64.407  12.925  1.00 34.64           N
ATOM  18457  C4   DT B  15      19.143  63.301  13.376  1.00 35.70           C
ATOM  18458  O4   DT B  15      18.987  62.833  14.493  1.00 38.57           O
ATOM  18459  C5   DT B  15      20.074  62.731  12.442  1.00 36.35           C
ATOM  18460  C7   DT B  15      20.888  61.538  12.832  1.00 37.41           C
ATOM  18461  C6   DT B  15      20.188  63.296  11.241  1.00 35.24           C
ATOM  18462  P    DC B  16      22.044  66.074   6.216  1.00 40.09           P
ATOM  18463  OP1  DC B  16      22.245  65.259   5.000  1.00 41.12           O
ATOM  18464  OP2  DC B  16      23.036  66.043   7.314  1.00 39.28           O
ATOM  18465  O5'  DC B  16      21.843  67.584   5.777  1.00 38.66           O
ATOM  18466  C5'  DC B  16      20.654  68.241   6.155  1.00 38.77           C
ATOM  18467  C4'  DC B  16      20.827  68.944   7.490  1.00 40.44           C
ATOM  18468  O4'  DC B  16      20.655  68.026   8.601  1.00 39.74           O
ATOM  18469  C3'  DC B  16      22.200  69.613   7.712  1.00 40.34           C
ATOM  18470  O3'  DC B  16      22.024  70.997   7.952  1.00 43.70           O
ATOM  18471  C2'  DC B  16      22.725  68.914   8.974  1.00 38.91           C
ATOM  18472  C1'  DC B  16      21.412  68.568   9.640  1.00 36.79           C
ATOM  18473  N1   DC B  16      21.506  67.630  10.800  1.00 33.92           N
ATOM  18474  C2   DC B  16      20.599  67.765  11.859  1.00 32.74           C
ATOM  18475  O2   DC B  16      19.723  68.631  11.794  1.00 31.89           O
ATOM  18476  N3   DC B  16      20.697  66.938  12.919  1.00 31.15           N
ATOM  18477  C4   DC B  16      21.650  66.017  12.955  1.00 32.44           C
ATOM  18478  N4   DC B  16      21.703  65.228  14.028  1.00 31.33           N
ATOM  18479  C5   DC B  16      22.598  65.869  11.898  1.00 33.30           C
ATOM  18480  C6   DC B  16      22.494  66.696  10.850  1.00 32.95           C
TER
END
"""


def get_dist(pdb_file, sel1, sel2):
  print("get_dist:", pdb_file, sel1, sel2)
  pdb_inp = iotbx.pdb.input(file_name=pdb_file)
  ph = pdb_inp.construct_hierarchy()
  xyz = ph.atoms().extract_xyz()
  asc = ph.atom_selection_cache()
  s1 = ph.atom_selection_cache().selection(string=sel1)
  s2 = ph.atom_selection_cache().selection(string=sel2)
  xyz1 = xyz.select(s1)
  xyz2 = xyz.select(s2)
  fit = superpose.least_squares_fit(
    reference_sites = xyz1,
    other_sites     = xyz2)
  return flex.mean(flex.sqrt((xyz1-fit.other_sites_best_fit()).dot()))

def run(i, pdb_str, prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  NCS constraints and different atom order in NCS related chains.
  Not relevant anymore, because hierarchies are sorted.
  """
  pdb_in = "%s_%s.pdb"%(prefix, str(i))
  fo = open(pdb_in, "w")
  print(pdb_str, file=fo)
  fo.close()
  # Check input PDB is what we want
  # XXX We are now sorting all hierarchies by default, so this check will fail
  # d = get_dist(pdb_file=pdb_in, sel1="chain A", sel2="chain B")
  # assert d > 0.5, d
  #
  cmd = " ".join([
    "phenix.fmodel",
    "%s"%pdb_in,
    "high_res=3",
    "low_res=3.5",
    "output.file_name=%s_%s.mtz"%(prefix,str(i)),
    "> zlog1"])
  assert easy_run.call(cmd)==0
  #
  args = [
    "%s"%pdb_in,
    "%s_%s.mtz"%(prefix,str(i)),
    "ncs_constraints=False",
    "refine_ncs_operators=False",
    "c_beta_restraints=False",
    "ramachandran_plot_restraints.enabled=False",
    "rotamers.restraints.enabled=False",
    "refine_ncs_operators=False",
    "refinement.macro_cycles=2",
    "weight=0"]
  r = run_real_space_refine(args = args, prefix = "%s_%s"%(prefix,str(i)))
  lines = r.get_lines(fn = r.std_out)
  # check for expected output
  #  Skip pdbtools which does not have the keyword model_stat...
  # result_pdb = r.pdb
  # r = easy_run.fully_buffered("phenix.pdbtools model_stat=true %s"%result_pdb)
  a_rmsd, b_rmsd = None, None
  for l in lines:
    if(l.strip().startswith("Bond      :")): b_rmsd = float(l.strip().split()[2])
    if(l.strip().startswith("Angle     :")): a_rmsd = float(l.strip().split()[2])

  assert a_rmsd < 0.6, a_rmsd
  assert b_rmsd < 0.005, b_rmsd

if (__name__ == "__main__"):
  t0 = time.time()
  for i, pdb_str in enumerate([pdb_str_1, pdb_str_2]):
    run(i=i, pdb_str = pdb_str)
    print()
  print("OK time =%8.3f"%(time.time() - t0))
