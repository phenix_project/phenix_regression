from __future__ import print_function
import time, os
from libtbx import easy_run
import iotbx.pdb
import libtbx.load_env
from scitbx.array_family import flex
from libtbx.utils import null_out
import mmtbx.model
from phenix_regression.real_space_refine import run_real_space_refine

pdb_str_noflip = """
CRYST1   16.494   21.200   16.574  90.00  90.00  90.00 P 1
ATOM   3039  N   LEU D 178       5.000  13.354  11.338  1.00 26.36      D    N
ATOM   3040  CA  LEU D 178       6.175  13.491  10.492  1.00 29.18      D    C
ATOM   3041  CB  LEU D 178       6.996  14.713  10.914  1.00 24.25      D    C
ATOM   3042  CG  LEU D 178       8.265  14.994  10.106  1.00 27.58      D    C
ATOM   3043  CD1 LEU D 178       7.918  15.244   8.645  1.00 28.79      D    C
ATOM   3044  CD2 LEU D 178       8.972  16.200  10.688  1.00 33.58      D    C
ATOM   3045  C   LEU D 178       7.054  12.262  10.530  1.00 31.64      D    C
ATOM   3046  O   LEU D 178       7.242  11.644  11.574  1.00 35.45      D    O
ATOM   3047  N   GLN D 179       7.584  11.893   9.373  1.00 36.63      D    N
ATOM   3048  CA  GLN D 179       8.470  10.743   9.294  1.00 39.47      D    C
ATOM   3049  CB  GLN D 179       7.682   9.437   9.362  1.00 38.89      D    C
ATOM   3050  CG  GLN D 179       8.463   8.315  10.040  1.00 45.67      D    C
ATOM   3051  CD  GLN D 179       8.044   6.936   9.564  1.00 50.45      D    C
ATOM   3052  OE1 GLN D 179       8.387   5.921  10.178  1.00 54.72      D    O
ATOM   3053  NE2 GLN D 179       7.312   6.891   8.454  1.00 50.00      D    N
ATOM   3054  C   GLN D 179       9.274  10.785   8.008  1.00 38.45      D    C
ATOM   3055  O   GLN D 179       8.719  10.687   6.911  1.00 37.33      D    O
ATOM   3056  N   SER D 180      10.585  10.946   8.158  1.00 38.76      D    N
ATOM   3057  CA  SER D 180      11.494  10.987   7.023  1.00 38.93      D    C
ATOM   3058  CB  SER D 180      11.245   9.780   6.113  1.00 41.07      D    C
ATOM   3059  OG  SER D 180      11.320   8.572   6.856  1.00 45.70      D    O
ATOM   3060  C   SER D 180      11.321  12.274   6.234  1.00 36.77      D    C
ATOM   3061  O   SER D 180      11.409  12.275   5.000  1.00 36.97      D    O
TER
ATOM   3477  O   HOH W  98       5.866   5.000   8.911  1.00 32.37      W    O
TER
END
"""

pdb_str_flip = """
CRYST1   16.494   21.200   16.574  90.00  90.00  90.00 P 1
ATOM      1  N   LEU D 178       5.001  13.356  11.334  1.00 26.36      D    N
ATOM      2  CA  LEU D 178       6.177  13.495  10.489  1.00 29.18      D    C
ATOM      3  CB  LEU D 178       6.993  14.714  10.913  1.00 24.25      D    C
ATOM      4  CG  LEU D 178       8.260  14.996  10.105  1.00 27.58      D    C
ATOM      5  CD1 LEU D 178       7.911  15.250   8.646  1.00 28.79      D    C
ATOM      6  CD2 LEU D 178       8.971  16.202  10.689  1.00 33.58      D    C
ATOM      7  C   LEU D 178       7.052  12.258  10.527  1.00 31.64      D    C
ATOM      8  O   LEU D 178       7.239  11.650  11.573  1.00 35.45      D    O
ATOM      9  N   GLN D 179       7.587  11.889   9.373  1.00 36.63      D    N
ATOM     10  CA  GLN D 179       8.471  10.742   9.292  1.00 39.47      D    C
ATOM     11  CB  GLN D 179       7.690   9.432   9.375  1.00 38.89      D    C
ATOM     12  CG  GLN D 179       8.479   8.294  10.070  1.00 45.67      D    C
ATOM     13  CD  GLN D 179       8.060   6.920   9.585  1.00 50.45      D    C
ATOM     14  OE1 GLN D 179       7.365   6.852   8.550  1.00 54.72      D    O
ATOM     15  NE2 GLN D 179       8.432   5.867  10.258  1.00 50.00      D    N
ATOM     16  C   GLN D 179       9.272  10.791   8.007  1.00 38.45      D    C
ATOM     17  O   GLN D 179       8.713  10.688   6.913  1.00 37.33      D    O
ATOM     18  N   SER D 180      10.581  10.949   8.153  1.00 38.76      D    N
ATOM     19  CA  SER D 180      11.488  10.987   7.020  1.00 38.93      D    C
ATOM     20  CB  SER D 180      11.260   9.780   6.111  1.00 41.07      D    C
ATOM     21  OG  SER D 180      11.312   8.577   6.852  1.00 45.70      D    O
ATOM     22  C   SER D 180      11.321  12.273   6.235  1.00 36.77      D    C
ATOM     23  O   SER D 180      11.406  12.278   5.004  1.00 36.97      D    O
TER
ATOM     24  O   HOH W  98       5.859   4.994   8.915  1.00 32.37      W    O
TER
END
"""

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refined")):
  """
  Make NQH flips: make sure they happen by default, and do not happen if
  disabled.
  """
  #
  for it in [("noflip",pdb_str_noflip), ("flip",pdb_str_flip)]:
    st, pdb_str = it
    pdb_inp = iotbx.pdb.input(source_info=None, lines=pdb_str)
    model = mmtbx.model.manager(model_input=pdb_inp, log=null_out())
    pdb_file_name = "%s%s.pdb"%(st, prefix)
    with open(pdb_file_name, "w") as fo:
      print(model.model_as_pdb(), file=fo)
    #
    cmd = " ".join([
      "phenix.model_map",
      pdb_file_name,
      "output_file_name_prefix=%s"%prefix,
      "grid_step=0.5",
      ">%s.zlog"%prefix])
    print(cmd)
    assert easy_run.call(cmd)==0
    #
    args = [
      "run=nqh_flips",
      pdb_file_name,
      "%s.ccp4"%prefix,
      "macro_cycles=1",
      "resolution=2.0"]
    r = run_real_space_refine(args = args, prefix = "%s%s"%(st, prefix))
    # check results
    xrs_flip_answer = iotbx.pdb.input(source_info=None,
      lines=pdb_str_flip).construct_hierarchy().extract_xray_structure()
    xrs1 = iotbx.pdb.input(file_name = r.pdb
      ).construct_hierarchy().extract_xray_structure()
    assert flex.max(xrs_flip_answer.distances(xrs1)) < 0.1
  #
  # Flip is needed but not done in refinement
  #
  xrs1 = iotbx.pdb.input(file_name = "fliptst_43_real_space_refined.pdb",
      ).construct_hierarchy().extract_xray_structure()
  xrs2 = iotbx.pdb.input(file_name = "nofliptst_43_real_space_refined.pdb",
      ).construct_hierarchy().extract_xray_structure()
  assert flex.max(xrs1.distances(xrs2)) > 2.35
  for par in [True,False]:
    nqh_flips="none"
    if par: nqh_flips="nqh_flips"
    args = [
      "run=%s"%nqh_flips,
      "nofliptst_43_real_space_refined.pdb",
      "%s.ccp4"%prefix,
      "macro_cycles=1",
      "resolution=2.0"]
    r = run_real_space_refine(args = args, prefix = prefix)
    xrs2 = iotbx.pdb.input(file_name = "nofliptst_43_real_space_refined.pdb",
        ).construct_hierarchy().extract_xray_structure()
    xrs3 = iotbx.pdb.input(file_name = r.pdb,
        ).construct_hierarchy().extract_xray_structure()
    d = flex.max(xrs2.distances(xrs3))
    if(par): assert d>2.35
    else:    assert d<0.001

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK time =%8.3f"%(time.time() - t0))
