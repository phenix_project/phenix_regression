from __future__ import print_function
from libtbx import easy_run
import time
import iotbx.pdb
from scitbx.array_family import flex
from libtbx.test_utils import approx_equal
import random
import os
from phenix_regression.real_space_refine import run_real_space_refine

pdb_str = """\
CRYST1   32.260  147.405   73.888  90.00  90.00  90.00 P 1
SCALE1      0.030998  0.000000  0.000000        0.00000
SCALE2      0.000000  0.006784  0.000000        0.00000
SCALE3      0.000000  0.000000  0.013534        0.00000
ATOM      1  N   GLN A3048      13.015 136.879  64.396  1.00 20.00           N
ATOM      2  CA  GLN A3048      14.158 137.203  63.555  1.00 20.00           C
ATOM      3  C   GLN A3048      15.042 138.191  64.296  1.00 20.00           C
ATOM      4  O   GLN A3048      14.613 139.308  64.598  1.00 20.00           O
ATOM      5  CB  GLN A3048      13.723 137.811  62.221  1.00 20.00           C
ATOM      6  CG  GLN A3048      13.102 136.855  61.232  1.00 20.00           C
ATOM      7  CD  GLN A3048      12.788 137.522  59.899  1.00 20.00           C
ATOM      8  OE1 GLN A3048      12.942 138.733  59.743  1.00 20.00           O
ATOM      9  NE2 GLN A3048      12.365 136.726  58.926  1.00 20.00           N
ATOM     10  N   ARG A3049      16.276 137.790  64.587  1.00 20.00           N
ATOM     11  CA  ARG A3049      17.194 138.638  65.339  1.00 20.00           C
ATOM     12  C   ARG A3049      18.436 138.916  64.503  1.00 20.00           C
ATOM     13  O   ARG A3049      19.355 138.072  64.459  1.00 20.00           O
ATOM     14  CB  ARG A3049      17.551 137.999  66.684  1.00 20.00           C
ATOM     15  CG  ARG A3049      18.516 138.813  67.532  1.00 20.00           C
ATOM     16  CD  ARG A3049      17.993 140.218  67.734  1.00 20.00           C
ATOM     17  NE  ARG A3049      16.661 140.218  68.329  1.00 20.00           N
ATOM     18  CZ  ARG A3049      15.848 141.267  68.333  1.00 20.00           C
ATOM     19  NH1 ARG A3049      16.231 142.405  67.774  1.00 20.00           N
ATOM     20  NH2 ARG A3049      14.649 141.180  68.888  1.00 20.00           N
ATOM     21  N   PRO A3050      18.511 140.068  63.818  1.00 20.00           N
ATOM     22  CA  PRO A3050      19.729 140.514  63.137  1.00 20.00           C
ATOM     23  C   PRO A3050      20.726 141.100  64.122  1.00 20.00           C
ATOM     24  O   PRO A3050      20.329 141.349  65.258  1.00 20.00           O
ATOM     25  CB  PRO A3050      19.219 141.585  62.173  1.00 20.00           C
ATOM     26  CG  PRO A3050      18.008 142.126  62.839  1.00 20.00           C
ATOM     27  CD  PRO A3050      17.374 140.967  63.549  1.00 20.00           C
TER
HETATM   28  C1' FMN A3994       9.364 131.548   9.623  1.00 20.00           C
HETATM   29  C10 FMN A3994       8.338 129.804  11.058  1.00 20.00           C
HETATM   30  C2  FMN A3994       9.105 127.557  10.810  1.00 20.00           C
HETATM   31  C2' FMN A3994       8.775 131.363   8.227  1.00 20.00           C
HETATM   32  C3' FMN A3994       9.706 131.959   7.194  1.00 20.00           C
HETATM   33  C4  FMN A3994       7.411 128.056  12.385  1.00 20.00           C
HETATM   34  C4' FMN A3994       9.863 133.450   7.422  1.00 20.00           C
HETATM   35  C4A FMN A3994       7.456 129.392  12.031  1.00 20.00           C
HETATM   36  C5' FMN A3994      10.580 134.144   6.279  1.00 20.00           C
HETATM   37  C5A FMN A3994       6.682 131.623  12.278  1.00 20.00           C
HETATM   38  C6  FMN A3994       5.847 132.537  12.894  1.00 20.00           C
HETATM   39  C7  FMN A3994       5.904 133.871  12.528  1.00 20.00           C
HETATM   40  C7M FMN A3994       5.000 134.858  13.191  1.00 20.00           C
HETATM   41  C8  FMN A3994       6.793 134.289  11.546  1.00 20.00           C
HETATM   42  C8M FMN A3994       6.885 135.724  11.126  1.00 20.00           C
HETATM   43  C9  FMN A3994       7.624 133.372  10.933  1.00 20.00           C
HETATM   44  C9A FMN A3994       7.569 132.038  11.296  1.00 20.00           C
HETATM   45  N1  FMN A3994       9.158 128.883  10.452  1.00 20.00           N
HETATM   46  N10 FMN A3994       8.394 131.123  10.683  1.00 20.00           N
HETATM   47  N3  FMN A3994       8.230 127.139  11.780  1.00 20.00           N
HETATM   48  N5  FMN A3994       6.628 130.300  12.645  1.00 20.00           N
HETATM   49  O1P FMN A3994       8.722 136.264   6.020  1.00 20.00           O
HETATM   50  O2  FMN A3994       9.842 126.748  10.265  1.00 20.00           O
HETATM   51  O2' FMN A3994       8.643 129.990   7.961  1.00 20.00           O
HETATM   52  O2P FMN A3994      10.175 137.768   7.329  1.00 20.00           O
HETATM   53  O3' FMN A3994      10.962 131.347   7.348  1.00 20.00           O
HETATM   54  O3P FMN A3994      10.781 137.199   5.001  1.00 20.00           O
HETATM   55  O4  FMN A3994       6.636 127.676  13.241  1.00 20.00           O
HETATM   56  O4' FMN A3994       8.609 134.059   7.581  1.00 20.00           O
HETATM   57  O5' FMN A3994      10.929 135.409   6.779  1.00 20.00           O
HETATM   58  P   FMN A3994      10.142 136.699   6.267  1.00 20.00           P
TER
ATOM     59  N   GLN B3048      16.107  10.933  64.395  1.00 20.00           N
ATOM     60  CA  GLN B3048      15.255  11.761  63.554  1.00 20.00           C
ATOM     61  C   GLN B3048      13.957  12.033  64.295  1.00 20.00           C
ATOM     62  O   GLN B3048      13.204  11.103  64.597  1.00 20.00           O
ATOM     63  CB  GLN B3048      14.945  11.080  62.220  1.00 20.00           C
ATOM     64  CG  GLN B3048      16.083  11.020  61.231  1.00 20.00           C
ATOM     65  CD  GLN B3048      15.663  10.415  59.898  1.00 20.00           C
ATOM     66  OE1 GLN B3048      14.537   9.943  59.742  1.00 20.00           O
ATOM     67  NE2 GLN B3048      16.563  10.446  58.925  1.00 20.00           N
ATOM     68  N   ARG B3049      13.688  13.302  64.586  1.00 20.00           N
ATOM     69  CA  ARG B3049      12.494  13.673  65.338  1.00 20.00           C
ATOM     70  C   ARG B3049      11.633  14.610  64.502  1.00 20.00           C
ATOM     71  O   ARG B3049      11.904  15.828  64.458  1.00 20.00           O
ATOM     72  CB  ARG B3049      12.870  14.302  66.683  1.00 20.00           C
ATOM     73  CG  ARG B3049      11.682  14.731  67.531  1.00 20.00           C
ATOM     74  CD  ARG B3049      10.727  13.576  67.733  1.00 20.00           C
ATOM     75  NE  ARG B3049      11.393  12.422  68.328  1.00 20.00           N
ATOM     76  CZ  ARG B3049      10.890  11.194  68.332  1.00 20.00           C
ATOM     77  NH1 ARG B3049       9.713  10.956  67.773  1.00 20.00           N
ATOM     78  NH2 ARG B3049      11.565  10.199  68.887  1.00 20.00           N
ATOM     79  N   PRO B3050      10.597  14.099  63.817  1.00 20.00           N
ATOM     80  CA  PRO B3050       9.602  14.931  63.136  1.00 20.00           C
ATOM     81  C   PRO B3050       8.596  15.502  64.122  1.00 20.00           C
ATOM     82  O   PRO B3050       8.579  15.034  65.258  1.00 20.00           O
ATOM     83  CB  PRO B3050       8.929  13.954  62.173  1.00 20.00           C
ATOM     84  CG  PRO B3050       9.066  12.635  62.839  1.00 20.00           C
ATOM     85  CD  PRO B3050      10.387  12.665  63.548  1.00 20.00           C
TER
HETATM   86  C1' FMN B3994      22.542  10.434   9.621  1.00 20.00           C
HETATM   87  C10 FMN B3994      24.566  10.417  11.056  1.00 20.00           C
HETATM   88  C2  FMN B3994      26.128  12.205  10.807  1.00 20.00           C
HETATM   89  C2' FMN B3994      22.997  10.017   8.225  1.00 20.00           C
HETATM   90  C3' FMN B3994      22.015  10.525   7.192  1.00 20.00           C
HETATM   91  C4  FMN B3994      26.543  10.488  12.382  1.00 20.00           C
HETATM   92  C4' FMN B3994      20.645   9.916   7.420  1.00 20.00           C
HETATM   93  C4A FMN B3994      25.363   9.859  12.029  1.00 20.00           C
HETATM   94  C5' FMN B3994      19.685  10.190   6.277  1.00 20.00           C
HETATM   95  C5A FMN B3994      23.818   8.074  12.276  1.00 20.00           C
HETATM   96  C6  FMN B3994      23.444   6.894  12.892  1.00 20.00           C
HETATM   97  C7  FMN B3994      22.260   6.277  12.526  1.00 20.00           C
HETATM   98  C7M FMN B3994      21.857   5.000  13.189  1.00 20.00           C
HETATM   99  C8  FMN B3994      21.453   6.838  11.544  1.00 20.00           C
HETATM  100  C8M FMN B3994      20.164   6.200  11.124  1.00 20.00           C
HETATM  101  C9  FMN B3994      21.832   8.016  10.931  1.00 20.00           C
HETATM  102  C9A FMN B3994      23.015   8.635  11.294  1.00 20.00           C
HETATM  103  N1  FMN B3994      24.953  11.588  10.450  1.00 20.00           N
HETATM  104  N10 FMN B3994      23.395   9.807  10.681  1.00 20.00           N
HETATM  105  N3  FMN B3994      26.928  11.656  11.777  1.00 20.00           N
HETATM  106  N5  FMN B3994      24.991   8.688  12.643  1.00 20.00           N
HETATM  107  O1P FMN B3994      18.778   7.521   6.018  1.00 20.00           O
HETATM  108  O2  FMN B3994      26.461  13.247  10.262  1.00 20.00           O
HETATM  109  O2' FMN B3994      24.252  10.589   7.959  1.00 20.00           O
HETATM  110  O2P FMN B3994      16.749   8.028   7.328  1.00 20.00           O
HETATM  111  O3' FMN B3994      21.917  11.919   7.346  1.00 20.00           O
HETATM  112  O3P FMN B3994      16.939   8.837   5.000  1.00 20.00           O
HETATM  113  O4  FMN B3994      27.260  10.007  13.238  1.00 20.00           O
HETATM  114  O4' FMN B3994      20.744   8.525   7.579  1.00 20.00           O
HETATM  115  O5' FMN B3994      18.415   9.860   6.777  1.00 20.00           O
HETATM  116  P   FMN B3994      17.691   8.534   6.266  1.00 20.00           P
TER
END
"""

def check(file_name=None, lines=None):
  assert [file_name, lines].count(None)==1
  if(lines is not None):
    pdb_inp = iotbx.pdb.input(source_info=None, lines=lines)
  else:
    pdb_inp = iotbx.pdb.input(file_name=file_name)
  ph = pdb_inp.construct_hierarchy()
  asc = ph.atom_selection_cache()
  sel_a = asc.selection("chain A")
  sel_b = asc.selection("chain B")
  xyz_a = ph.atoms().extract_xyz().select(sel_a)
  xyz_b = ph.atoms().extract_xyz().select(sel_a)
  assert approx_equal(flex.min(flex.sqrt((xyz_a - xyz_b).dot())), 0.0)
  return ph, pdb_inp

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  Make sure NCS related copies with ligands are retained and used.
  """
  #
  ph, pdb_inp = check(lines=pdb_str)
  #
  ph.write_pdb_file(file_name="%s.pdb"%prefix,
    crystal_symmetry=pdb_inp.crystal_symmetry())
  # perturb and output
  for a in ph.atoms():
    x,y,z = a.xyz
    a.xyz = [x+random.random(),y+random.random(),z+random.random()]
  ph.write_pdb_file(file_name="%s_perturbed.pdb"%prefix,
    crystal_symmetry=pdb_inp.crystal_symmetry())
  #
  cmd = " ".join([
    "phenix.fmodel",
    "%s.pdb"%prefix,
    "file_name=%s.mtz"%prefix,
    "high_res=3.5",
    "low_res=4.0",
    ">%s.zlog"%prefix])
  assert easy_run.call(cmd)==0
  #
  args = [
    "run=minimization_global",
    "macro_cycles=3",
    "%s_perturbed.pdb"%prefix,
    "%s.mtz"%prefix]
  r = run_real_space_refine(args = args, prefix = prefix)
  #
  check(file_name=r.pdb)

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
