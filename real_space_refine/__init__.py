from __future__ import absolute_import, division, print_function
from libtbx.utils import search_for
import os
from libtbx.test_utils import approx_equal
from scitbx.array_family import flex
from libtbx import easy_run
from libtbx.test_utils import show_diff
import iotbx.pdb
import mmtbx.utils

class run_real_space_refine(object):
  def __init__(self, args, prefix, sorry_expected=False, geo_expected=True,
      assert_is_similar_hierarchy=True):
    self.sorry_expected = sorry_expected
    assert isinstance([], list)
    assert isinstance(prefix, str)
    self.log     = "%s_real_space_refined_000.log"%prefix
    self.pdb     = "%s_real_space_refined_000.pdb"%prefix
    self.pdb_all = "%s_real_space_refined_000_all_states.pdb"%prefix
    self.cif     = "%s_real_space_refined_000.cif"%prefix
    self.eff     = "%s_real_space_refined_000.eff"%prefix
    self.pkl     = "%s_real_space_refined_000.pkl"%prefix
    self.geo_ini = "%s_real_space_refined_000_initial.geo"%prefix
    self.geo_fin = "%s_real_space_refined_000_final.geo"%prefix
    self.std_out = "%s.log"%prefix
    self.xrs     = None
    self.ph      = None
    #
    args += ["output.prefix=%s"%prefix]
    self.cmd = " ".join(
      ["phenix.real_space_refine overwrite=True"] + args + [">%s"%self.std_out])
    if(1): print(self.cmd)
    self.r = easy_run.fully_buffered(self.cmd)
    #
    log_lines = self.get_lines(fn = self.log)
    buf_lines = self.get_lines(fn = self.std_out)
    if([log_lines,buf_lines].count(None)==0 and
       len(log_lines) != len(buf_lines)):
      if 0:
        for line1, line2 in zip(log_lines, buf_lines):
          if line1!=line2:
            print(line1)
            print(line2)
            assert 0
      raise RuntimeError("Log and stdout are different!")
    #
    if(not sorry_expected):
      assert log_lines[-3] == "Job complete"
      h1, x1 = self.get_xh(fn = self.cif)
      h2, x2 = self.get_xh(fn = self.pdb)
      if assert_is_similar_hierarchy:
        assert h1.is_similar_hierarchy(h2)
      mmtbx.utils.assert_xray_structures_equal(x1 = x1, x2 = x2, eps = 1.e-3,
        eps_occ=2.e-2)
      self.xrs = x1
      self.ph  = h1
    #

  def get_lines(self, fn):
    isf = os.path.isfile(fn)
    if(self.sorry_expected and not isf): return
    assert isf, fn
    result = []
    with open(fn,"r") as fo:
      for l in fo.readlines():
        l = l.strip("\n")
        result.append(l)
    return result

  def get_xh(self, fn):
    assert os.path.isfile(fn), fn
    pdb_inp = iotbx.pdb.input(file_name=fn)
    ph = pdb_inp.construct_hierarchy()
    ph.flip_symmetric_amino_acids()
    xrs = ph.extract_xray_structure(
      crystal_symmetry = pdb_inp.crystal_symmetry())
    return ph, xrs
