from __future__ import print_function
import time
from libtbx import easy_run
import iotbx.pdb
from scitbx.array_family import flex
from scitbx.math import superpose
import os
import libtbx.load_env
from phenix_regression.real_space_refine import run_real_space_refine
from libtbx.test_utils import assert_lines_in_file


def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  Stop with useful message if multuple choice of input map coefficients is
  available.
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/real_space_refine/data/tst_47.pdb",
    test=os.path.isfile)
  mtz = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/real_space_refine/data/tst_47.mtz",
    test=os.path.isfile)
  args = [pdb,mtz]
  r = run_real_space_refine(args = args, prefix = prefix, sorry_expected=True)
  #
  cntr = 0
  l = ''
  with open(r.std_out,"r") as fo:
    for l in fo.readlines():
      l=l.strip()
      if(l.count("Multiple choice of map coefficients available:")): cntr+=1
      if(l.count("['2FOFCWT,PH2FOFCWT', 'FOFCWT,PHFOFCWT', 'F-model,PHIF-model', '2FOFCWT_no_fill,PH2FOFCWT_no_fill']")): cntr+=1
  assert cntr==2
  assert r.r.stderr_lines[-1]=="Sorry: Choose one using map_coefficients_label and re-run.",\
    r.r.stderr_lines[-1]

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK time =%8.3f"%(time.time() - t0))
