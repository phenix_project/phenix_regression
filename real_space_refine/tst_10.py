from __future__ import division
from __future__ import print_function
from libtbx import easy_run
import time
import iotbx.pdb
from libtbx.test_utils import show_diff
import os
from phenix_regression.real_space_refine import run_real_space_refine

original_ss_records = """HELIX    1   1 TYR X    4  MET X   15  1                                  12
SHEET    1   A 2 SER X  19  ILE X  23  0
SHEET    2   A 2 LYS X  26  TYR X  30 -1  O  LYS X  26   N  ILE X  23
"""

from_ksdssp_ss_records = """HELIX    1   1 SER X    5  ALA X   14  1                                  10
SHEET    1   A 2 SER X  19  ILE X  23  0
SHEET    2   A 2 LYS X  26  TYR X  30 -1  N  TYR X  30   O  SER X  19
"""

pdb_str_3e8y = """\
CRYST1   56.633   56.633   31.759  90.00  90.00  90.00 I 41/A       16
ORIGX1      1.000000  0.000000  0.000000        0.00000
ORIGX2      0.000000  1.000000  0.000000        0.00000
ORIGX3      0.000000  0.000000  1.000000        0.00000
SCALE1      0.017658  0.000000  0.000000        0.00000
SCALE2      0.000000  0.017658  0.000000        0.00000
SCALE3      0.000000  0.000000  0.031487        0.00000
ATOM      1  N   ALA X   2      50.332  -1.088  14.526  1.00 24.39           N
ATOM      2  CA  ALA X   2      48.936  -0.717  14.154  1.00 20.65           C
ATOM      3  C   ALA X   2      48.968  -0.016  12.785  1.00 18.28           C
ATOM      4  O   ALA X   2      50.047   0.278  12.275  1.00 22.19           O
ATOM      5  CB  ALA X   2      48.435   0.245  15.173  1.00 23.62           C
ATOM      6  N   CYS X   3      47.812   0.268  12.213  1.00 13.53           N
ATOM      7  CA  CYS X   3      47.733   1.082  11.020  1.00 11.75           C
ATOM      8  C   CYS X   3      47.363   2.490  11.408  1.00 11.38           C
ATOM      9  O   CYS X   3      46.478   2.702  12.242  1.00 11.65           O
ATOM     10  CB  CYS X   3      46.685   0.466  10.088  1.00 12.11           C
ATOM     11  SG  CYS X   3      46.526   1.259   8.480  1.00 12.23           S
ATOM     12  N   TYR X   4      48.046   3.460  10.794  1.00 11.77           N
ATOM     13  CA  TYR X   4      47.763   4.881  10.987  1.00 12.26           C
ATOM     14  C   TYR X   4      47.473   5.469   9.636  1.00 12.16           C
ATOM     15  O   TYR X   4      48.247   5.301   8.679  1.00 12.34           O
ATOM     16  CB  TYR X   4      48.945   5.606  11.657  1.00 14.73           C
ATOM     17  CG  TYR X   4      49.138   5.100  13.051  1.00 17.60           C
ATOM     18  CD1 TYR X   4      49.952   4.008  13.297  1.00 21.42           C
ATOM     19  CD2 TYR X   4      48.404   5.648  14.107  1.00 19.13           C
ATOM     20  CE1 TYR X   4      50.093   3.491  14.581  1.00 24.50           C
ATOM     21  CE2 TYR X   4      48.532   5.129  15.411  1.00 22.62           C
ATOM     22  CZ  TYR X   4      49.361   4.056  15.612  1.00 23.06           C
ATOM     23  OH  TYR X   4      49.493   3.555  16.897  1.00 28.03           O
ATOM     24  N   SER X   5      46.395   6.233   9.560  1.00 14.12           N
ATOM     25  CA  SER X   5      45.878   6.735   8.304  1.00 15.31           C
ATOM     26  C   SER X   5      46.847   7.493   7.447  1.00 12.45           C
ATOM     27  O   SER X   5      46.965   7.231   6.237  1.00 12.44           O
ATOM     28  CB  SER X   5      44.647   7.622   8.574  1.00 17.34           C
ATOM     29  OG  SER X   5      45.018   8.874   9.145  1.00 27.72           O
ATOM     30  N   SER X   6      47.533   8.461   8.021  1.00 12.99           N
ATOM     31  CA ASER X   6      48.396   9.307   7.252  0.50 12.91           C
ATOM     32  CA BSER X   6      48.382   9.298   7.159  0.50 12.46           C
ATOM     33  C   SER X   6      49.532   8.483   6.642  1.00 11.18           C
ATOM     34  O   SER X   6      49.861   8.599   5.442  1.00 11.70           O
ATOM     35  CB ASER X   6      48.916  10.407   8.168  0.50 15.24           C
ATOM     36  CB BSER X   6      48.930  10.595   7.783  0.50 13.73           C
ATOM     37  OG ASER X   6      49.360   9.852   9.391  0.50 17.84           O
ATOM     38  OG BSER X   6      47.929  11.470   8.237  0.50 16.32           O
ATOM     39  N   ASP X   7      50.167   7.660   7.479  1.00 10.16           N
ATOM     40  CA  ASP X   7      51.262   6.851   6.991  1.00  9.44           C
ATOM     41  C   ASP X   7      50.799   5.867   5.940  1.00  8.82           C
ATOM     42  O   ASP X   7      51.520   5.616   4.957  1.00  9.14           O
ATOM     43  CB  ASP X   7      51.926   6.104   8.111  1.00 10.52           C
ATOM     44  CG  ASP X   7      52.689   6.997   9.028  1.00 11.91           C
ATOM     45  OD1 ASP X   7      53.281   8.030   8.624  1.00 11.00           O
ATOM     46  OD2 ASP X   7      52.749   6.606  10.203  1.00 20.13           O
ATOM     47  N   CYS X   8      49.616   5.278   6.115  1.00  9.36           N
ATOM     48  CA  CYS X   8      49.108   4.328   5.147  1.00  9.13           C
ATOM     49  C   CYS X   8      48.897   4.992   3.783  1.00  8.93           C
ATOM     50  O   CYS X   8      49.336   4.458   2.758  1.00  9.24           O
ATOM     51  CB  CYS X   8      47.816   3.727   5.699  1.00 10.04           C
ATOM     52  SG  CYS X   8      46.991   2.619   4.496  1.00 10.08           S
ATOM     53  N   ARG X   9      48.234   6.152   3.762  1.00  9.22           N
ATOM     54  CA AARG X   9      48.055   6.837   2.508  0.50  9.06           C
ATOM     55  CA BARG X   9      48.042   6.854   2.493  0.50 10.13           C
ATOM     56  C   ARG X   9      49.384   7.106   1.815  1.00  8.62           C
ATOM     57  O   ARG X   9      49.533   6.921   0.598  1.00  8.95           O
ATOM     58  CB AARG X   9      47.346   8.142   2.756  0.50 10.63           C
ATOM     59  CB BARG X   9      47.308   8.186   2.688  0.50 11.05           C
ATOM     60  CG AARG X   9      47.135   8.891   1.510  0.50 12.68           C
ATOM     61  CG BARG X   9      45.853   8.085   3.178  0.50 13.00           C
ATOM     62  CD AARG X   9      46.412  10.175   1.795  0.50 16.25           C
ATOM     63  CD BARG X   9      45.164   9.437   3.098  0.50 15.05           C
ATOM     64  N   VAL X  10      50.353   7.606   2.581  1.00  8.32           N
ATOM     65  CA  VAL X  10      51.645   7.994   2.027  1.00  8.29           C
ATOM     66  C   VAL X  10      52.416   6.779   1.516  1.00  7.91           C
ATOM     67  O   VAL X  10      53.049   6.861   0.435  1.00  8.14           O
ATOM     68  CB  VAL X  10      52.411   8.821   3.077  1.00  8.81           C
ATOM     69  CG1 VAL X  10      53.861   9.072   2.640  1.00  9.74           C
ATOM     70  CG2 VAL X  10      51.687  10.160   3.290  1.00 10.00           C
ATOM     71  N   LYS X  11      52.383   5.654   2.230  1.00  7.89           N
ATOM     72  CA ALYS X  11      52.975   4.402   1.752  0.50  7.89           C
ATOM     73  CA BLYS X  11      53.048   4.464   1.703  0.50  7.99           C
ATOM     74  C   LYS X  11      52.377   4.006   0.414  1.00  7.60           C
ATOM     75  O   LYS X  11      53.068   3.648  -0.541  1.00  8.14           O
ATOM     76  CB ALYS X  11      52.717   3.273   2.777  0.50  8.65           C
ATOM     77  CB BLYS X  11      53.031   3.301   2.706  0.50  8.06           C
ATOM     78  CG ALYS X  11      53.809   3.296   3.861  0.50  8.62           C
ATOM     79  CG BLYS X  11      53.742   3.607   4.036  0.50  8.89           C
ATOM     80  CD ALYS X  11      53.668   2.069   4.754  0.50  9.84           C
ATOM     81  CD BLYS X  11      53.721   2.394   4.980  0.50  9.21           C
ATOM     82  CE ALYS X  11      52.445   2.194   5.629  0.50  9.81           C
ATOM     83  CE BLYS X  11      54.486   2.709   6.234  0.50 12.47           C
ATOM     84  NZ ALYS X  11      52.393   1.028   6.528  0.50 10.91           N
ATOM     85  NZ BLYS X  11      54.510   1.619   7.207  0.50 15.92           N
ATOM     86  N   CYS X  12      51.036   4.020   0.362  1.00  7.41           N
ATOM     87  CA  CYS X  12      50.356   3.600  -0.844  1.00  7.50           C
ATOM     88  C   CYS X  12      50.722   4.490  -2.038  1.00  7.54           C
ATOM     89  O   CYS X  12      51.020   3.983  -3.123  1.00  7.93           O
ATOM     90  CB  CYS X  12      48.838   3.613  -0.601  1.00  7.68           C
ATOM     91  SG  CYS X  12      48.294   2.300   0.526  1.00  8.00           S
ATOM     92  N   VAL X  13      50.692   5.816  -1.842  1.00  7.83           N
ATOM     93  CA  VAL X  13      51.041   6.725  -2.939  1.00  7.75           C
ATOM     94  C   VAL X  13      52.488   6.492  -3.360  1.00  7.81           C
ATOM     95  O   VAL X  13      52.808   6.498  -4.560  1.00  8.78           O
ATOM     96  CB  VAL X  13      50.772   8.189  -2.535  1.00  8.30           C
ATOM     97  CG1 VAL X  13      51.293   9.156  -3.591  1.00  8.89           C
ATOM     98  CG2 VAL X  13      49.258   8.393  -2.345  1.00  9.52           C
ATOM     99  N   ALA X  14      53.395   6.290  -2.398  1.00  7.81           N
ATOM    100  CA  ALA X  14      54.805   6.082  -2.730  1.00  8.54           C
ATOM    101  C   ALA X  14      55.001   4.907  -3.665  1.00  8.56           C
ATOM    102  O   ALA X  14      55.914   4.934  -4.498  1.00  9.63           O
ATOM    103  CB  ALA X  14      55.614   5.891  -1.480  1.00 10.01           C
ATOM    104  N   MET X  15      54.172   3.862  -3.517  1.00  8.55           N
ATOM    105  CA  MET X  15      54.271   2.679  -4.363  1.00 10.58           C
ATOM    106  C   MET X  15      53.510   2.798  -5.651  1.00 10.12           C
ATOM    107  O   MET X  15      53.552   1.850  -6.439  1.00 13.02           O
ATOM    108  CB  MET X  15      53.861   1.444  -3.564  1.00 12.94           C
ATOM    109  CG  MET X  15      54.967   1.099  -2.596  1.00 15.97           C
ATOM    110  SD AMET X  15      54.548  -0.293  -1.497  0.50 17.59           S
ATOM    111  SD BMET X  15      55.047  -0.430  -1.844  0.50 13.34           S
ATOM    112  CE AMET X  15      52.828   0.083  -1.377  0.50  7.20           C
ATOM    113  CE BMET X  15      53.572  -0.028  -0.840  0.50 10.83           C
ATOM    114  N   GLY X  16      52.869   3.931  -5.919  1.00  9.17           N
ATOM    115  CA  GLY X  16      52.186   4.173  -7.181  1.00 10.30           C
ATOM    116  C   GLY X  16      50.703   3.940  -7.121  1.00 10.04           C
ATOM    117  O   GLY X  16      50.043   4.046  -8.162  1.00 14.04           O
ATOM    118  N   PHE X  17      50.139   3.657  -5.954  1.00  8.50           N
ATOM    119  CA  PHE X  17      48.703   3.510  -5.794  1.00  8.55           C
ATOM    120  C   PHE X  17      48.077   4.884  -5.570  1.00  9.27           C
ATOM    121  O   PHE X  17      48.774   5.888  -5.341  1.00 10.41           O
ATOM    122  CB  PHE X  17      48.397   2.570  -4.648  1.00  8.46           C
ATOM    123  CG  PHE X  17      48.916   1.176  -4.868  1.00  7.64           C
ATOM    124  CD1 PHE X  17      49.840   0.636  -4.001  1.00  9.08           C
ATOM    125  CD2 PHE X  17      48.474   0.384  -5.935  1.00  9.11           C
ATOM    126  CE1 PHE X  17      50.296  -0.679  -4.185  1.00 10.56           C
ATOM    127  CE2 PHE X  17      48.937  -0.902  -6.115  1.00  9.58           C
ATOM    128  CZ  PHE X  17      49.852  -1.429  -5.256  1.00  9.96           C
ATOM    129  N   SER X  18      46.750   4.946  -5.616  1.00  9.94           N
ATOM    130  CA  SER X  18      46.064   6.245  -5.536  1.00 11.28           C
ATOM    131  C   SER X  18      46.042   6.839  -4.147  1.00 10.68           C
ATOM    132  O   SER X  18      46.152   8.052  -3.958  1.00 11.55           O
ATOM    133  CB  SER X  18      44.582   6.055  -6.072  1.00 13.19           C
ATOM    134  OG  SER X  18      44.616   5.735  -7.451  1.00 13.51           O
ATOM    135  N   SER X  19      45.807   5.985  -3.161  1.00 10.32           N
ATOM    136  CA  SER X  19      45.680   6.348  -1.740  1.00  9.99           C
ATOM    137  C   SER X  19      45.545   5.051  -0.968  1.00  8.21           C
ATOM    138  O   SER X  19      45.754   3.967  -1.541  1.00  8.55           O
ATOM    139  CB  SER X  19      44.430   7.248  -1.520  1.00 12.91           C
ATOM    140  OG  SER X  19      44.482   7.714  -0.186  1.00 16.16           O
ATOM    141  N   GLY X  20      45.158   5.128   0.298  1.00  8.84           N
ATOM    142  CA  GLY X  20      44.861   3.940   1.080  1.00  9.70           C
ATOM    143  C   GLY X  20      44.165   4.282   2.354  1.00 10.04           C
ATOM    144  O   GLY X  20      44.111   5.448   2.741  1.00 13.61           O
ATOM    145  N   LYS X  21      43.639   3.260   3.011  1.00  9.75           N
ATOM    146  CA ALYS X  21      42.951   3.441   4.287  0.50 12.23           C
ATOM    147  CA BLYS X  21      42.812   3.367   4.198  0.50 10.71           C
ATOM    148  C   LYS X  21      43.198   2.263   5.177  1.00 10.15           C
ATOM    149  O   LYS X  21      43.403   1.126   4.730  1.00 10.93           O
ATOM    150  CB ALYS X  21      41.412   3.553   4.151  0.50 13.42           C
ATOM    151  CB BLYS X  21      41.324   3.189   3.761  0.50 12.88           C
ATOM    152  CG ALYS X  21      40.881   4.732   3.357  0.50 17.37           C
ATOM    153  CG BLYS X  21      40.233   3.441   4.786  0.50 15.92           C
ATOM    154  CD ALYS X  21      39.519   5.275   3.875  0.50 18.27           C
ATOM    155  CD BLYS X  21      38.986   4.088   4.162  0.50 17.99           C
ATOM    156  N   CYS X  22      43.148   2.534   6.474  1.00 10.42           N
ATOM    157  CA  CYS X  22      43.277   1.502   7.508  1.00 10.59           C
ATOM    158  C   CYS X  22      41.951   0.777   7.696  1.00 10.63           C
ATOM    159  O   CYS X  22      40.926   1.412   8.015  1.00 13.42           O
ATOM    160  CB  CYS X  22      43.690   2.111   8.825  1.00 11.29           C
ATOM    161  SG  CYS X  22      45.315   2.856   8.819  1.00 12.08           S
ATOM    162  N   ILE X  23      41.976  -0.538   7.570  1.00  9.49           N
ATOM    163  CA  ILE X  23      40.800  -1.384   7.747  1.00 10.33           C
ATOM    164  C   ILE X  23      41.246  -2.575   8.557  1.00  9.60           C
ATOM    165  O   ILE X  23      42.134  -3.333   8.133  1.00 10.11           O
ATOM    166  CB  ILE X  23      40.275  -1.886   6.385  1.00 12.57           C
ATOM    167  CG1 ILE X  23      39.868  -0.716   5.492  1.00 14.85           C
ATOM    168  CG2 ILE X  23      39.149  -2.915   6.595  1.00 14.66           C
ATOM    169  CD1 ILE X  23      38.691   0.068   5.993  1.00 16.53           C
ATOM    170  N   ASN X  24      40.675  -2.771   9.749  1.00 10.00           N
ATOM    171  CA  ASN X  24      40.964  -3.956  10.540  1.00 11.10           C
ATOM    172  C   ASN X  24      42.472  -4.157  10.747  1.00 10.35           C
ATOM    173  O   ASN X  24      42.984  -5.259  10.613  1.00 11.17           O
ATOM    174  CB  ASN X  24      40.327  -5.190   9.907  1.00 12.80           C
ATOM    175  CG  ASN X  24      40.277  -6.363  10.852  1.00 15.01           C
ATOM    176  OD1 ASN X  24      40.395  -6.209  12.100  1.00 15.39           O
ATOM    177  ND2 ASN X  24      40.134  -7.559  10.281  1.00 19.80           N
ATOM    178  N   SER X  25      43.161  -3.051  11.084  1.00 10.36           N
ATOM    179  CA ASER X  25      44.607  -3.067  11.337  0.33 11.29           C
ATOM    180  CA BSER X  25      44.588  -3.038  11.346  0.33 11.76           C
ATOM    181  CA CSER X  25      44.604  -2.979  11.339  0.33 12.05           C
ATOM    182  C   SER X  25      45.474  -3.213  10.115  1.00 11.40           C
ATOM    183  O   SER X  25      46.676  -3.353  10.265  1.00 15.08           O
ATOM    184  CB ASER X  25      45.013  -4.118  12.371  0.33 13.10           C
ATOM    185  CB BSER X  25      44.972  -4.022  12.453  0.33 13.83           C
ATOM    186  CB CSER X  25      45.065  -3.851  12.517  0.33 13.79           C
ATOM    187  OG ASER X  25      44.541  -3.691  13.615  0.33 12.49           O
ATOM    188  OG BSER X  25      46.097  -3.517  13.113  0.33 14.66           O
ATOM    189  OG CSER X  25      45.096  -5.204  12.112  0.33 15.57           O
ATOM    190  N   LYS X  26      44.893  -3.179   8.920  1.00 10.18           N
ATOM    191  CA  LYS X  26      45.642  -3.384   7.674  1.00 10.25           C
ATOM    192  C   LYS X  26      45.613  -2.085   6.871  1.00  8.99           C
ATOM    193  O   LYS X  26      44.556  -1.458   6.720  1.00  9.17           O
ATOM    194  CB  LYS X  26      45.026  -4.509   6.860  1.00 11.05           C
ATOM    195  CG  LYS X  26      45.068  -5.824   7.616  1.00 15.39           C
ATOM    196  CD  LYS X  26      44.351  -6.918   6.867  1.00 20.96           C
ATOM    197  CE  LYS X  26      44.150  -8.136   7.739  1.00 25.42           C
ATOM    198  N   CYS X  27      46.748  -1.716   6.288  1.00  8.96           N
ATOM    199  CA  CYS X  27      46.799  -0.602   5.353  1.00  8.38           C
ATOM    200  C   CYS X  27      46.461  -1.141   3.973  1.00  7.46           C
ATOM    201  O   CYS X  27      47.262  -1.866   3.363  1.00  7.90           O
ATOM    202  CB  CYS X  27      48.203  -0.002   5.378  1.00  8.99           C
ATOM    203  SG  CYS X  27      48.432   1.223   4.077  1.00  9.23           S
ATOM    204  N   LYS X  28      45.263  -0.799   3.506  1.00  7.47           N
ATOM    205  CA  LYS X  28      44.718  -1.289   2.246  1.00  7.67           C
ATOM    206  C   LYS X  28      44.735  -0.176   1.222  1.00  7.60           C
ATOM    207  O   LYS X  28      44.012   0.835   1.355  1.00  7.96           O
ATOM    208  CB  LYS X  28      43.293  -1.844   2.458  1.00  9.06           C
ATOM    209  CG  LYS X  28      43.242  -2.978   3.470  1.00 10.36           C
ATOM    210  CD  LYS X  28      41.928  -3.739   3.333  1.00 13.20           C
ATOM    211  CE  LYS X  28      41.784  -4.863   4.334  1.00 15.85           C
ATOM    212  NZ  LYS X  28      40.572  -5.709   3.945  1.00 19.30           N
ATOM    213  N   CYS X  29      45.544  -0.338   0.169  1.00  7.19           N
ATOM    214  CA  CYS X  29      45.619   0.709  -0.838  1.00  7.14           C
ATOM    215  C   CYS X  29      44.463   0.652  -1.805  1.00  6.98           C
ATOM    216  O   CYS X  29      43.954  -0.438  -2.151  1.00  7.89           O
ATOM    217  CB  CYS X  29      46.908   0.648  -1.653  1.00  7.56           C
ATOM    218  SG  CYS X  29      48.428   0.642  -0.656  1.00  7.92           S
ATOM    219  N   TYR X  30      44.065   1.816  -2.303  1.00  7.38           N
ATOM    220  CA  TYR X  30      43.226   1.949  -3.489  1.00  8.00           C
ATOM    221  C   TYR X  30      44.129   1.920  -4.699  1.00  8.00           C
ATOM    222  O   TYR X  30      45.168   2.637  -4.711  1.00  9.31           O
ATOM    223  CB  TYR X  30      42.488   3.286  -3.476  1.00  8.51           C
ATOM    224  CG  TYR X  30      41.520   3.468  -2.342  1.00  8.52           C
ATOM    225  CD1 TYR X  30      41.813   4.292  -1.273  1.00 10.71           C
ATOM    226  CD2 TYR X  30      40.260   2.875  -2.356  1.00  9.22           C
ATOM    227  CE1 TYR X  30      40.926   4.544  -0.243  1.00 12.47           C
ATOM    228  CE2 TYR X  30      39.325   3.149  -1.326  1.00 10.06           C
ATOM    229  CZ  TYR X  30      39.685   3.989  -0.287  1.00 11.00           C
ATOM    230  OH  TYR X  30      38.810   4.288   0.742  1.00 13.91           O
ATOM    231  N   LYS X  31      43.806   1.153  -5.727  1.00  8.25           N
ATOM    232  CA  LYS X  31      44.613   1.095  -6.919  1.00  9.01           C
ATOM    233  C   LYS X  31      44.849   2.460  -7.517  1.00  9.60           C
ATOM    234  O   LYS X  31      43.887   3.260  -7.571  1.00  9.75           O
ATOM    235  CB  LYS X  31      43.972   0.145  -7.936  1.00  9.58           C
ATOM    236  CG  LYS X  31      44.730   0.010  -9.259  1.00 10.31           C
ATOM    237  CD  LYS X  31      46.173  -0.524  -9.113  1.00 11.19           C
ATOM    238  CE  LYS X  31      46.802  -0.733 -10.462  1.00 12.51           C
ATOM    239  NZ  LYS X  31      48.254  -1.115 -10.316  1.00 13.00           N
ATOM    240  OXT LYS X  31      46.004   2.732  -7.926  1.00 11.39           O
TER"""

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  Exercise phenix.real_space_refine: check that SS records are outputted in the
  resulting file
  """

  # preparation

  fname_input = "%s_3e8y_m.pdb" % prefix
  fname_output = "%s_3e8y_m_real_space_refined_000.pdb" % prefix
  out = open(fname_input, "w")
  print(pdb_str_3e8y, file=out)
  out.close()

  cmd = " ".join([
    "phenix.fmodel",
    "%s"%fname_input,
    "file_name=%s.mtz"%prefix,
    "high_res=10",
    ">%s.zlog"%prefix])
  assert easy_run.call(cmd)==0

  common_params = " ".join([
    "phenix.real_space_refine",
    "run=minimization_global",
    "write_initial_geo_file=False",
    "ramachandran_plot_restraints.favored=oldfield",
    "ramachandran_plot_restraints.allowed=oldfield",
    "ramachandran_plot_restraints.outlier=oldfield",
    "write_all_states=False",
    "macro_cycles=1",
    "max_iterations=0",
    "ncs_search.enabled=False",
    "ncs_constraints=False",
    "overwrite=True",
    "%s"%fname_input,
    "%s.mtz"%prefix])

  # No SS in input file
  cmd = " ".join([common_params,
    "secondary_structure.enabled=False",
    ">%s.zlog"%prefix])
  print(cmd)
  assert easy_run.call(cmd)==0
  pdb_inp = iotbx.pdb.input(file_name=fname_output)
  assert len(pdb_inp.secondary_structure_section()) == 0

  cmd = " ".join([common_params,
    "secondary_structure.enabled=True",
    ">%s.zlog"%prefix])
  print(cmd)
  print()
  assert easy_run.call(cmd)==0
  pdb_inp = iotbx.pdb.input(file_name=fname_output)
  assert not show_diff(
      pdb_inp.secondary_structure_section(), from_ksdssp_ss_records)

  # SS is present in input file
  out = open(fname_input, "w")
  print(original_ss_records, pdb_str_3e8y, file=out)
  out.close()
  cmd = " ".join([common_params,
    ">%s.zlog"%prefix])
  print(cmd)
  assert easy_run.call(cmd)==0
  pdb_inp = iotbx.pdb.input(file_name=fname_output)
  assert not show_diff(
      pdb_inp.secondary_structure_section(), original_ss_records)

  cmd = " ".join([common_params,
    "secondary_structure.enabled=True",
    ">%s.zlog"%prefix])
  print(cmd)
  print()
  assert easy_run.call(cmd)==0
  pdb_inp = iotbx.pdb.input(file_name=fname_output)
  assert not show_diff(
      pdb_inp.secondary_structure_section(), original_ss_records)

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
