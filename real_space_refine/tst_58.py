from __future__ import print_function
import time
from libtbx import easy_run
import iotbx.pdb
from scitbx.array_family import flex
from scitbx.math import superpose
import os
import libtbx.load_env
from phenix_regression.real_space_refine import run_real_space_refine
from libtbx.test_utils import assert_lines_in_file

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  Exercise for 
  /cctbx_project/cctbx/crystal/pair_tables.h(663): CCTBX_ASSERT(j_sym >= 0) failure.
  failure fix.
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/real_space_refine/data/%s.pdb"%prefix,
    test=os.path.isfile)
  mtz = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/real_space_refine/data/%s.mtz"%prefix,
    test=os.path.isfile)
  #
  args = [pdb, mtz, "macro_cycles=1", "run=none"]
  r1 = run_real_space_refine(args = args, prefix = prefix)

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK time =%8.3f"%(time.time() - t0))
