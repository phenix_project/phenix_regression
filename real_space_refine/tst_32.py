from __future__ import print_function
import time, os
from libtbx import easy_run, easy_pickle
import iotbx.pdb
import libtbx.load_env
from phenix_regression.real_space_refine import run_real_space_refine

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  Copy-pasted tst_22.
  Make sure output.write_pkl works.
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/real_space_refine/data/%s.pdb"%'tst_22',
    test=os.path.isfile)
  mtz = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/real_space_refine/data/%s.mtz"%'tst_22',
    test=os.path.isfile)
  #
  args = [
    "%s"%pdb,
    "%s"%mtz,
    "macro_cycles=1",
    "rotamers.fit=none",
    "rotamers.restraints.enabled=false",
    "write_pkl_stats=True"]
  r = run_real_space_refine(args = args, prefix = prefix)
  # check
  r = easy_pickle.load(r.pkl)
  assert hasattr(r, 'starting_stats')
  assert hasattr(r, 'final_stats')
  assert hasattr(r.starting_stats, 'cc')
  assert hasattr(r.starting_stats, 'geometry')

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK time =%8.3f"%(time.time() - t0))
