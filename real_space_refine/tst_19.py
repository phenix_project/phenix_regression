from __future__ import print_function
from libtbx import easy_run
import time
import iotbx.pdb
from scitbx.array_family import flex
import os
from phenix_regression.real_space_refine import run_real_space_refine

pdb_str_answer = """\
CRYST1   19.939   17.914   19.542  90.00  90.00  90.00 P 1
ATOM      1  N   ALA E   1       9.643  12.914   7.898  1.00 20.00           N
ATOM      2  CA  ALA E   1      10.721  11.968   8.160  1.00 20.00           C
ATOM      3  C   ALA E   1      10.216  10.775   8.965  1.00 20.00           C
ATOM      4  O   ALA E   1      10.829  10.379   9.956  1.00 20.00           O
ATOM      5  CB  ALA E   1      11.864  12.656   8.890  1.00 20.00           C
ATOM      6  N   HIS E   2       9.091  10.205   8.530  1.00 20.00           N
ATOM      7  CA  HIS E   2       8.521   9.058   9.227  1.00 20.00           C
ATOM      8  C   HIS E   2       9.237   7.760   8.876  1.00 20.00           C
ATOM      9  O   HIS E   2       9.306   6.851   9.711  1.00 20.00           O
ATOM     10  CB  HIS E   2       7.030   8.936   8.907  1.00 20.00           C
ATOM     11  CG  HIS E   2       6.226  10.133   9.310  1.00 20.00           C
ATOM     12  ND1 HIS E   2       5.642  10.254  10.552  1.00 20.00           N
ATOM     13  CD2 HIS E   2       5.909  11.263   8.634  1.00 20.00           C
ATOM     14  CE1 HIS E   2       5.000  11.406  10.625  1.00 20.00           C
ATOM     15  NE2 HIS E   2       5.146  12.037   9.474  1.00 20.00           N
ATOM     16  N   CYS E   3       9.772   7.654   7.658  1.00 20.00           N
ATOM     17  CA  CYS E   3      10.473   6.444   7.247  1.00 20.00           C
ATOM     18  C   CYS E   3      11.911   6.399   7.745  1.00 20.00           C
ATOM     19  O   CYS E   3      12.505   5.315   7.782  1.00 20.00           O
ATOM     20  CB  CYS E   3      10.455   6.318   5.722  1.00 20.00           C
ATOM     21  SG  CYS E   3       8.800   6.226   5.000  1.00 20.00           S
ATOM     22  N   ALA E   4      12.482   7.542   8.128  1.00 20.00           N
ATOM     23  CA  ALA E   4      13.856   7.561   8.614  1.00 20.00           C
ATOM     24  C   ALA E   4      13.946   7.223  10.096  1.00 20.00           C
ATOM     25  O   ALA E   4      14.939   6.631  10.533  1.00 20.00           O
ATOM     26  CB  ALA E   4      14.491   8.927   8.349  1.00 20.00           C
ATOM     27  N   ILE E   5      12.933   7.586  10.877  1.00 20.00           N
ATOM     28  CA  ILE E   5      12.931   7.305  12.307  1.00 20.00           C
ATOM     29  C   ILE E   5      12.500   5.866  12.561  1.00 20.00           C
ATOM     30  O   ILE E   5      13.328   5.000  12.842  1.00 20.00           O
ATOM     31  CB  ILE E   5      12.023   8.289  13.066  1.00 20.00           C
ATOM     32  CG1 ILE E   5      12.520   9.724  12.881  1.00 20.00           C
ATOM     33  CG2 ILE E   5      11.962   7.926  14.542  1.00 20.00           C
ATOM     34  CD1 ILE E   5      11.676  10.759  13.591  1.00 20.00           C
TER
END
"""

pdb_str_poor = """\
CRYST1   19.939   17.914   19.542  90.00  90.00  90.00 P 1
SCALE1      0.050153  0.000000  0.000000        0.00000
SCALE2      0.000000  0.055822  0.000000        0.00000
SCALE3      0.000000  0.000000  0.051172        0.00000
ATOM      1  N   ALA E   1      11.143  12.914   7.898  1.00 20.00           N
ATOM      2  CA  ALA E   1      12.221  11.968   8.160  1.00 20.00           C
ATOM      3  C   ALA E   1      11.716  10.775   8.965  1.00 20.00           C
ATOM      4  O   ALA E   1      12.329  10.379   9.956  1.00 20.00           O
ATOM      5  CB  ALA E   1      13.364  12.656   8.890  1.00 20.00           C
ATOM      6  N   HIS E   2      10.591  10.205   8.530  1.00 20.00           N
ATOM      7  CA  HIS E   2      10.021   9.058   9.227  1.00 20.00           C
ATOM      8  C   HIS E   2      10.737   7.760   8.876  1.00 20.00           C
ATOM      9  O   HIS E   2      10.806   6.851   9.711  1.00 20.00           O
ATOM     10  CB  HIS E   2       8.530   8.936   8.907  1.00 20.00           C
ATOM     11  CG  HIS E   2       7.726  10.133   9.310  1.00 20.00           C
ATOM     12  ND1 HIS E   2       7.142  10.254  10.552  1.00 20.00           N
ATOM     13  CD2 HIS E   2       7.409  11.263   8.634  1.00 20.00           C
ATOM     14  CE1 HIS E   2       6.500  11.406  10.625  1.00 20.00           C
ATOM     15  NE2 HIS E   2       6.646  12.037   9.474  1.00 20.00           N
ATOM     16  N   CYS E   3      11.272   7.654   7.658  1.00 20.00           N
ATOM     17  CA  CYS E   3      11.973   6.444   7.247  1.00 20.00           C
ATOM     18  C   CYS E   3      13.411   6.399   7.745  1.00 20.00           C
ATOM     19  O   CYS E   3      14.005   5.315   7.782  1.00 20.00           O
ATOM     20  CB  CYS E   3      11.955   6.318   5.722  1.00 20.00           C
ATOM     21  SG  CYS E   3      10.300   6.226   5.000  1.00 20.00           S
ATOM     22  N   ALA E   4      13.982   7.542   8.128  1.00 20.00           N
ATOM     23  CA  ALA E   4      15.356   7.561   8.614  1.00 20.00           C
ATOM     24  C   ALA E   4      15.446   7.223  10.096  1.00 20.00           C
ATOM     25  O   ALA E   4      16.439   6.631  10.533  1.00 20.00           O
ATOM     26  CB  ALA E   4      15.991   8.927   8.349  1.00 20.00           C
ATOM     27  N   ILE E   5      14.433   7.586  10.877  1.00 20.00           N
ATOM     28  CA  ILE E   5      14.431   7.305  12.307  1.00 20.00           C
ATOM     29  C   ILE E   5      14.000   5.866  12.561  1.00 20.00           C
ATOM     30  O   ILE E   5      14.828   5.000  12.842  1.00 20.00           O
ATOM     31  CB  ILE E   5      13.523   8.289  13.066  1.00 20.00           C
ATOM     32  CG1 ILE E   5      14.020   9.724  12.881  1.00 20.00           C
ATOM     33  CG2 ILE E   5      13.462   7.926  14.542  1.00 20.00           C
ATOM     34  CD1 ILE E   5      13.176  10.759  13.591  1.00 20.00           C
TER
END
"""

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  Exercise reference_model.use_starting_model_as_reference.
  """
  pdb_file_name_answer = "%s_answer.pdb"%prefix
  of=open(pdb_file_name_answer, "w")
  print(pdb_str_answer, file=of)
  of.close()
  #
  pdb_file_name_poor = "%s_poor.pdb"%prefix
  of=open(pdb_file_name_poor, "w")
  print(pdb_str_poor, file=of)
  of.close()
  #
  cmd = " ".join([
    "phenix.fmodel",
    "%s"%pdb_file_name_answer,
    "file_name=%s.mtz"%prefix,
    "high_res=1.5",
    ">%s.zlog"%prefix])
  assert easy_run.call(cmd)==0
  #
  options = ["",
  "reference_model.enable=true reference_model.use_starting_model_as_ref=true reference_model.sigma=0.01"]
  for i, opt in enumerate(options):
    args = [
      "run=minimization_global",
      "target_bonds_rmsd=0.01",
      "target_angles_rmsd=0.5",
      "ramachandran_plot_restraints.enabled=False",
      "rotamers.restraints.enabled=False",
      "%s"%pdb_file_name_poor,
      "%s.mtz"%prefix]+[opt]
    r = run_real_space_refine(args = args, prefix = "%s_%s"%(prefix,str(i)))
    xrs_p=iotbx.pdb.input(file_name=pdb_file_name_poor).xray_structure_simple()
    xrs_r=iotbx.pdb.input(file_name=r.pdb).xray_structure_simple()
    xrs_a=iotbx.pdb.input(file_name=pdb_file_name_answer).xray_structure_simple()
    mi,ma,me = xrs_a.distances(xrs_r).min_max_mean().as_tuple()
    # Not a very strong test but can't find anything better!
    if i == 0:
      assert me < 0.035
    else:
      assert me > 0.5

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
