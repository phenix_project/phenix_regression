from __future__ import print_function
import time
from libtbx import easy_run
import iotbx.pdb
from scitbx.array_family import flex
from scitbx.math import superpose
import os
import libtbx.load_env
from phenix_regression.real_space_refine import run_real_space_refine
from libtbx.test_utils import assert_lines_in_file


def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  Stop if map and model are not aligned.
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/real_space_refine/data/tst_49.pdb",
    test=os.path.isfile)
  mrc = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/real_space_refine/data/tst_49.ccp4",
    test=os.path.isfile)
  args = [pdb,mrc,"resolution=2.6","ignore_symmetry_conflicts=true"]
  r = run_real_space_refine(args = args, prefix = prefix, sorry_expected=True)
  #
  assert r.r.stderr_lines[-1]=="Sorry: Map and model are not aligned! Use skip_map_model_overlap_check=True to continue."

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK time =%8.3f"%(time.time() - t0))
