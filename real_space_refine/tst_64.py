from __future__ import print_function
import time
from libtbx import easy_run
import iotbx.pdb
from scitbx.array_family import flex
from scitbx.math import superpose
import os
import libtbx.load_env
from phenix_regression.real_space_refine import run_real_space_refine
from libtbx.test_utils import assert_lines_in_file
from libtbx.test_utils import approx_equal, not_approx_equal

pdb_str_answer = """
CRYST1   41.735   24.706   34.251  90.00  90.00  90.00 P 1
SCALE1      0.023961  0.000000  0.000000        0.00000
SCALE2      0.000000  0.040476  0.000000        0.00000
SCALE3      0.000000  0.000000  0.029196        0.00000
ATOM      1  N   GLY A   1       5.043   9.706  12.649  0.30  9.00           N
ATOM      2  CA  GLY A   1       5.000   9.301  11.198  0.50 11.00           C
ATOM      3  C   GLY A   1       6.037   8.234  10.966  0.60 10.00           C
ATOM      4  O   GLY A   1       6.529   7.615  11.928  0.90  9.00           O
ATOM      5  N   ASN A   2       6.396   8.017   9.702  1.00  9.00           N
ATOM      6  CA  ASN A   2       7.530   7.132   9.378  1.00  9.00           C
ATOM      7  C   ASN A   2       8.811   7.631   9.974  1.00  9.00           C
ATOM      8  O   ASN A   2       9.074   8.836   9.973  1.00  9.00           O
ATOM      9  CB  ASN A   2       7.706   6.975   7.888  1.00 11.00           C
ATOM     10  CG  ASN A   2       6.468   6.436   7.239  1.00 10.00           C
ATOM     11  OD1 ASN A   2       6.027   5.321   7.563  1.00  9.00           O
ATOM     12  ND2 ASN A   2       5.848   7.249   6.378  1.00  9.00           N
ATOM     13  N   ASN A   3       9.614   6.684  10.452  1.00 11.00           N
ATOM     14  CA  ASN A   3      10.859   6.998  11.136  1.00 11.00           C
ATOM     15  C   ASN A   3      12.097   6.426  10.442  1.00 10.00           C
ATOM     16  O   ASN A   3      12.180   5.213  10.195  1.00 11.00           O
ATOM     17  CB  ASN A   3      10.793   6.472  12.589  1.00 10.00           C
ATOM     18  CG  ASN A   3      12.046   6.833  13.408  1.00  9.00           C
ATOM     19  OD1 ASN A   3      12.350   8.019  13.619  1.00 11.00           O
ATOM     20  ND2 ASN A   3      12.781   5.809  13.853  1.00  9.00           N
ATOM     21  N  AGLN A   4      13.047   7.322  10.145  0.50 11.00           N
ATOM     22  CA AGLN A   4      14.436   6.982   9.746  0.50 10.00           C
ATOM     23  C  AGLN A   4      15.487   7.700  10.635  0.50 11.00           C
ATOM     24  O  AGLN A   4      15.599   8.937  10.662  0.50 11.00           O
ATOM     25  CB AGLN A   4      14.708   7.242   8.258  0.50 11.00           C
ATOM     26  CG AGLN A   4      15.996   6.552   7.760  0.50  9.00           C
ATOM     27  CD AGLN A   4      16.556   7.138   6.458  0.50 10.00           C
ATOM     28  OE1AGLN A   4      16.796   8.362   6.357  0.50  9.00           O
ATOM     29  NE2AGLN A   4      16.802   6.255   5.456  0.50 10.00           N
ATOM     30  N  BGLN A   4      13.147   7.322  10.145  0.70 11.00           N
ATOM     31  CA BGLN A   4      14.536   6.982   9.746  0.70 10.00           C
ATOM     32  C  BGLN A   4      15.587   7.700  10.635  0.70 11.00           C
ATOM     33  O  BGLN A   4      15.699   8.937  10.662  0.70 11.00           O
ATOM     34  CB BGLN A   4      14.808   7.242   8.258  0.70 11.00           C
ATOM     35  CG BGLN A   4      14.009   6.298   7.334  0.70  9.00           C
ATOM     36  CD BGLN A   4      14.201   6.577   5.838  0.70 10.00           C
ATOM     37  OE1BGLN A   4      14.536   7.714   5.436  0.70  9.00           O
ATOM     38  NE2BGLN A   4      14.021   5.524   5.000  0.70 10.00           N
ATOM     39  N   GLN A   5      16.206   6.915  11.418  1.00  9.00           N
ATOM     40  CA  GLN A   5      17.322   7.455  12.187  1.00  9.00           C
ATOM     41  C   GLN A   5      18.646   6.862  11.719  1.00  9.00           C
ATOM     42  O   GLN A   5      18.820   5.640  11.601  1.00 11.00           O
ATOM     43  CB  GLN A   5      17.108   7.277  13.694  1.00  9.00           C
ATOM     44  CG  GLN A   5      15.881   8.044  14.194  1.00 11.00           C
ATOM     45  CD  GLN A   5      15.396   7.508  15.501  1.00 11.00           C
ATOM     46  OE1 GLN A   5      14.826   6.419  15.549  1.00 10.00           O
ATOM     47  NE2 GLN A   5      15.601   8.281  16.586  1.00  9.00           N
ATOM     48  N   ASN A   6      19.566   7.758  11.403  1.00 11.00           N
ATOM     49  CA  ASN A   6      20.883   7.404  10.865  1.00 10.00           C
ATOM     50  C   ASN A   6      21.906   7.855  11.871  1.00 11.00           C
ATOM     51  O   ASN A   6      22.271   9.037  11.921  1.00 10.00           O
ATOM     52  CB  ASN A   6      21.117   8.110   9.540  1.00 10.00           C
ATOM     53  CG  ASN A   6      20.013   7.829   8.550  1.00 11.00           C
ATOM     54  OD1 ASN A   6      19.850   6.698   8.098  1.00 10.00           O
ATOM     55  ND2 ASN A   6      19.247   8.841   8.226  1.00 10.00           N
ATOM     56  N  ATYR A   7      22.344   6.911  12.694  0.20 10.00           N
ATOM     57  CA ATYR A   7      23.211   7.238  13.846  0.20  9.00           C
ATOM     58  C  ATYR A   7      24.655   7.425  13.432  0.20 11.00           C
ATOM     59  O  ATYR A   7      25.093   6.905  12.402  0.20  9.00           O
ATOM     60  CB ATYR A   7      23.113   6.159  14.916  0.20 10.00           C
ATOM     61  CG ATYR A   7      21.717   6.023  15.449  0.20  9.00           C
ATOM     62  CD1ATYR A   7      21.262   6.850  16.467  0.20  9.00           C
ATOM     63  CD2ATYR A   7      20.823   5.115  14.874  0.20  9.00           C
ATOM     64  CE1ATYR A   7      19.956   6.743  16.963  0.20 10.00           C
ATOM     65  CE2ATYR A   7      19.532   5.000  15.343  0.20 11.00           C
ATOM     66  CZ ATYR A   7      19.099   5.823  16.378  0.20 11.00           C
ATOM     67  OH ATYR A   7      17.818   5.683  16.838  0.20  9.00           O
ATOM     68  OXTATYR A   7      25.410   8.093  14.159  0.20 10.00           O
ATOM     69  N  BTYR A   7      22.444   6.911  12.694  0.80 10.00           N
ATOM     70  CA BTYR A   7      23.311   7.238  13.846  0.80  9.00           C
ATOM     71  C  BTYR A   7      24.755   7.425  13.432  0.80 11.00           C
ATOM     72  O  BTYR A   7      25.193   6.905  12.402  0.80  9.00           O
ATOM     73  CB BTYR A   7      23.213   6.159  14.916  0.80 10.00           C
ATOM     74  CG BTYR A   7      24.154   6.413  16.057  0.80  9.00           C
ATOM     75  CD1BTYR A   7      23.862   7.370  17.018  0.80  9.00           C
ATOM     76  CD2BTYR A   7      25.304   5.633  16.215  0.80  9.00           C
ATOM     77  CE1BTYR A   7      24.725   7.607  18.096  0.80 10.00           C
ATOM     78  CE2BTYR A   7      26.162   5.846  17.273  0.80 11.00           C
ATOM     79  CZ BTYR A   7      25.863   6.824  18.217  0.80 11.00           C
ATOM     80  OH BTYR A   7      26.735   7.027  19.251  0.80  9.00           O
ATOM     81  OXTBTYR A   7      25.510   8.093  14.159  0.80 10.00           O
TER
ATOM     82  N   GLY B   1      15.043  19.706  22.649  1.00  9.00           N
ATOM     83  CA  GLY B   1      15.000  19.301  21.198  1.00 11.00           C
ATOM     84  C   GLY B   1      16.037  18.234  20.966  1.00 10.00           C
ATOM     85  O   GLY B   1      16.529  17.615  21.928  1.00  9.00           O
ATOM     86  N   ASN B   2      16.396  18.017  19.702  0.90  9.00           N
ATOM     87  CA  ASN B   2      17.530  17.132  19.378  0.90  9.00           C
ATOM     88  C   ASN B   2      18.811  17.631  19.974  0.90  9.00           C
ATOM     89  O   ASN B   2      19.074  18.836  19.973  0.90  9.00           O
ATOM     90  CB  ASN B   2      17.706  16.975  17.888  0.90 11.00           C
ATOM     91  CG  ASN B   2      16.468  16.436  17.239  0.90 10.00           C
ATOM     92  OD1 ASN B   2      16.027  15.321  17.563  0.90  9.00           O
ATOM     93  ND2 ASN B   2      15.848  17.249  16.378  0.90  9.00           N
ATOM     94  N   ASN B   3      19.614  16.684  20.452  1.00 11.00           N
ATOM     95  CA  ASN B   3      20.859  16.998  21.136  1.00 11.00           C
ATOM     96  C   ASN B   3      22.097  16.426  20.442  1.00 10.00           C
ATOM     97  O   ASN B   3      22.180  15.213  20.195  1.00 11.00           O
ATOM     98  CB  ASN B   3      20.793  16.472  22.589  1.00 10.00           C
ATOM     99  CG  ASN B   3      22.046  16.833  23.408  1.00  9.00           C
ATOM    100  OD1 ASN B   3      22.350  18.019  23.619  1.00 11.00           O
ATOM    101  ND2 ASN B   3      22.781  15.809  23.853  1.00  9.00           N
ATOM    102  N  AGLN B   4      23.047  17.322  20.145  0.30 11.00           N
ATOM    103  CA AGLN B   4      24.436  16.982  19.746  0.30 10.00           C
ATOM    104  C  AGLN B   4      25.487  17.700  20.635  0.30 11.00           C
ATOM    105  O  AGLN B   4      25.599  18.937  20.662  0.30 11.00           O
ATOM    106  CB AGLN B   4      24.708  17.242  18.258  0.30 11.00           C
ATOM    107  CG AGLN B   4      25.996  16.552  17.760  0.30  9.00           C
ATOM    108  CD AGLN B   4      26.556  17.138  16.458  0.30 10.00           C
ATOM    109  OE1AGLN B   4      26.796  18.362  16.357  0.30  9.00           O
ATOM    110  NE2AGLN B   4      26.802  16.255  15.456  0.30 10.00           N
ATOM    111  N  BGLN B   4      23.147  17.322  20.145  0.50 11.00           N
ATOM    112  CA BGLN B   4      24.536  16.982  19.746  0.50 10.00           C
ATOM    113  C  BGLN B   4      25.587  17.700  20.635  0.50 11.00           C
ATOM    114  O  BGLN B   4      25.699  18.937  20.662  0.50 11.00           O
ATOM    115  CB BGLN B   4      24.808  17.242  18.258  0.50 11.00           C
ATOM    116  CG BGLN B   4      24.009  16.298  17.334  0.50  9.00           C
ATOM    117  CD BGLN B   4      24.201  16.577  15.838  0.50 10.00           C
ATOM    118  OE1BGLN B   4      24.536  17.714  15.436  0.50  9.00           O
ATOM    119  NE2BGLN B   4      24.021  15.524  15.000  0.50 10.00           N
ATOM    120  N   GLN B   5      26.206  16.915  21.418  1.00  9.00           N
ATOM    121  CA  GLN B   5      27.322  17.455  22.187  1.00  9.00           C
ATOM    122  C   GLN B   5      28.646  16.862  21.719  1.00  9.00           C
ATOM    123  O   GLN B   5      28.820  15.640  21.601  1.00 11.00           O
ATOM    124  CB  GLN B   5      27.108  17.277  23.694  1.00  9.00           C
ATOM    125  CG  GLN B   5      25.881  18.044  24.194  1.00 11.00           C
ATOM    126  CD  GLN B   5      25.396  17.508  25.501  1.00 11.00           C
ATOM    127  OE1 GLN B   5      24.826  16.419  25.549  1.00 10.00           O
ATOM    128  NE2 GLN B   5      25.601  18.281  26.586  1.00  9.00           N
ATOM    129  N   ASN B   6      29.566  17.758  21.403  1.00 11.00           N
ATOM    130  CA  ASN B   6      30.883  17.404  20.865  1.00 10.00           C
ATOM    131  C   ASN B   6      31.906  17.855  21.871  1.00 11.00           C
ATOM    132  O   ASN B   6      32.271  19.037  21.921  1.00 10.00           O
ATOM    133  CB  ASN B   6      31.117  18.110  19.540  1.00 10.00           C
ATOM    134  CG  ASN B   6      30.013  17.829  18.550  1.00 11.00           C
ATOM    135  OD1 ASN B   6      29.850  16.698  18.098  1.00 10.00           O
ATOM    136  ND2 ASN B   6      29.247  18.841  18.226  1.00 10.00           N
ATOM    137  N  ATYR B   7      32.344  16.911  22.694  0.80 10.00           N
ATOM    138  CA ATYR B   7      33.211  17.238  23.846  0.80  9.00           C
ATOM    139  C  ATYR B   7      34.655  17.425  23.432  0.80 11.00           C
ATOM    140  O  ATYR B   7      35.093  16.905  22.402  0.80  9.00           O
ATOM    141  CB ATYR B   7      33.113  16.159  24.916  0.80 10.00           C
ATOM    142  CG ATYR B   7      31.717  16.023  25.449  0.80  9.00           C
ATOM    143  CD1ATYR B   7      31.262  16.850  26.467  0.80  9.00           C
ATOM    144  CD2ATYR B   7      30.823  15.115  24.874  0.80  9.00           C
ATOM    145  CE1ATYR B   7      29.956  16.743  26.963  0.80 10.00           C
ATOM    146  CE2ATYR B   7      29.532  15.000  25.343  0.80 11.00           C
ATOM    147  CZ ATYR B   7      29.099  15.823  26.378  0.80 11.00           C
ATOM    148  OH ATYR B   7      27.818  15.683  26.838  0.80  9.00           O
ATOM    149  OXTATYR B   7      35.410  18.093  24.159  0.80 10.00           O
ATOM    150  N  BTYR B   7      32.444  16.911  22.694  0.20 10.00           N
ATOM    151  CA BTYR B   7      33.311  17.238  23.846  0.20  9.00           C
ATOM    152  C  BTYR B   7      34.755  17.425  23.432  0.20 11.00           C
ATOM    153  O  BTYR B   7      35.193  16.905  22.402  0.20  9.00           O
ATOM    154  CB BTYR B   7      33.213  16.159  24.916  0.20 10.00           C
ATOM    155  CG BTYR B   7      34.154  16.413  26.057  0.20  9.00           C
ATOM    156  CD1BTYR B   7      33.862  17.370  27.018  0.20  9.00           C
ATOM    157  CD2BTYR B   7      35.304  15.633  26.215  0.20  9.00           C
ATOM    158  CE1BTYR B   7      34.725  17.607  28.096  0.20 10.00           C
ATOM    159  CE2BTYR B   7      36.162  15.846  27.273  0.20 11.00           C
ATOM    160  CZ BTYR B   7      35.863  16.824  28.217  0.20 11.00           C
ATOM    161  OH BTYR B   7      36.735  17.027  29.251  0.20  9.00           O
ATOM    162  OXTBTYR B   7      35.510  18.093  24.159  0.20 10.00           O
TER
END
"""

pdb_str_poor = """
CRYST1   41.735   24.706   34.251  90.00  90.00  90.00 P 1
SCALE1      0.023961  0.000000  0.000000        0.00000
SCALE2      0.000000  0.040476  0.000000        0.00000
SCALE3      0.000000  0.000000  0.029196        0.00000
ATOM      1  N   GLY A   1       5.043   9.706  12.649  1.00  9.00           N
ATOM      2  CA  GLY A   1       5.000   9.301  11.198  1.00 11.00           C
ATOM      3  C   GLY A   1       6.037   8.234  10.966  1.00 10.00           C
ATOM      4  O   GLY A   1       6.529   7.615  11.928  1.00  9.00           O
ATOM      5  N   ASN A   2       6.396   8.017   9.702  1.00  9.00           N
ATOM      6  CA  ASN A   2       7.530   7.132   9.378  1.00  9.00           C
ATOM      7  C   ASN A   2       8.811   7.631   9.974  1.00  9.00           C
ATOM      8  O   ASN A   2       9.074   8.836   9.973  1.00  9.00           O
ATOM      9  CB  ASN A   2       7.706   6.975   7.888  1.00 11.00           C
ATOM     10  CG  ASN A   2       6.468   6.436   7.239  1.00 10.00           C
ATOM     11  OD1 ASN A   2       6.027   5.321   7.563  1.00  9.00           O
ATOM     12  ND2 ASN A   2       5.848   7.249   6.378  1.00  9.00           N
ATOM     13  N   ASN A   3       9.614   6.684  10.452  1.00 11.00           N
ATOM     14  CA  ASN A   3      10.859   6.998  11.136  1.00 11.00           C
ATOM     15  C   ASN A   3      12.097   6.426  10.442  1.00 10.00           C
ATOM     16  O   ASN A   3      12.180   5.213  10.195  1.00 11.00           O
ATOM     17  CB  ASN A   3      10.793   6.472  12.589  1.00 10.00           C
ATOM     18  CG  ASN A   3      12.046   6.833  13.408  1.00  9.00           C
ATOM     19  OD1 ASN A   3      12.350   8.019  13.619  1.00 11.00           O
ATOM     20  ND2 ASN A   3      12.781   5.809  13.853  1.00  9.00           N
ATOM     21  N  AGLN A   4      13.047   7.322  10.145  0.10 11.00           N
ATOM     22  CA AGLN A   4      14.436   6.982   9.746  0.10 10.00           C
ATOM     23  C  AGLN A   4      15.487   7.700  10.635  0.10 11.00           C
ATOM     24  O  AGLN A   4      15.599   8.937  10.662  0.10 11.00           O
ATOM     25  CB AGLN A   4      14.708   7.242   8.258  0.10 11.00           C
ATOM     26  CG AGLN A   4      15.996   6.552   7.760  0.10  9.00           C
ATOM     27  CD AGLN A   4      16.556   7.138   6.458  0.10 10.00           C
ATOM     28  OE1AGLN A   4      16.796   8.362   6.357  0.10  9.00           O
ATOM     29  NE2AGLN A   4      16.802   6.255   5.456  0.10 10.00           N
ATOM     30  N  BGLN A   4      13.147   7.322  10.145  0.10 11.00           N
ATOM     31  CA BGLN A   4      14.536   6.982   9.746  0.10 10.00           C
ATOM     32  C  BGLN A   4      15.587   7.700  10.635  0.10 11.00           C
ATOM     33  O  BGLN A   4      15.699   8.937  10.662  0.10 11.00           O
ATOM     34  CB BGLN A   4      14.808   7.242   8.258  0.10 11.00           C
ATOM     35  CG BGLN A   4      14.009   6.298   7.334  0.10  9.00           C
ATOM     36  CD BGLN A   4      14.201   6.577   5.838  0.10 10.00           C
ATOM     37  OE1BGLN A   4      14.536   7.714   5.436  0.10  9.00           O
ATOM     38  NE2BGLN A   4      14.021   5.524   5.000  0.10 10.00           N
ATOM     39  N   GLN A   5      16.206   6.915  11.418  1.00  9.00           N
ATOM     40  CA  GLN A   5      17.322   7.455  12.187  1.00  9.00           C
ATOM     41  C   GLN A   5      18.646   6.862  11.719  1.00  9.00           C
ATOM     42  O   GLN A   5      18.820   5.640  11.601  1.00 11.00           O
ATOM     43  CB  GLN A   5      17.108   7.277  13.694  1.00  9.00           C
ATOM     44  CG  GLN A   5      15.881   8.044  14.194  1.00 11.00           C
ATOM     45  CD  GLN A   5      15.396   7.508  15.501  1.00 11.00           C
ATOM     46  OE1 GLN A   5      14.826   6.419  15.549  1.00 10.00           O
ATOM     47  NE2 GLN A   5      15.601   8.281  16.586  1.00  9.00           N
ATOM     48  N   ASN A   6      19.566   7.758  11.403  1.00 11.00           N
ATOM     49  CA  ASN A   6      20.883   7.404  10.865  1.00 10.00           C
ATOM     50  C   ASN A   6      21.906   7.855  11.871  1.00 11.00           C
ATOM     51  O   ASN A   6      22.271   9.037  11.921  1.00 10.00           O
ATOM     52  CB  ASN A   6      21.117   8.110   9.540  1.00 10.00           C
ATOM     53  CG  ASN A   6      20.013   7.829   8.550  1.00 11.00           C
ATOM     54  OD1 ASN A   6      19.850   6.698   8.098  1.00 10.00           O
ATOM     55  ND2 ASN A   6      19.247   8.841   8.226  1.00 10.00           N
ATOM     56  N  ATYR A   7      22.344   6.911  12.694  0.20 10.00           N
ATOM     57  CA ATYR A   7      23.211   7.238  13.846  0.20  9.00           C
ATOM     58  C  ATYR A   7      24.655   7.425  13.432  0.20 11.00           C
ATOM     59  O  ATYR A   7      25.093   6.905  12.402  0.20  9.00           O
ATOM     60  CB ATYR A   7      23.113   6.159  14.916  0.20 10.00           C
ATOM     61  CG ATYR A   7      21.717   6.023  15.449  0.20  9.00           C
ATOM     62  CD1ATYR A   7      21.262   6.850  16.467  0.20  9.00           C
ATOM     63  CD2ATYR A   7      20.823   5.115  14.874  0.20  9.00           C
ATOM     64  CE1ATYR A   7      19.956   6.743  16.963  0.20 10.00           C
ATOM     65  CE2ATYR A   7      19.532   5.000  15.343  0.20 11.00           C
ATOM     66  CZ ATYR A   7      19.099   5.823  16.378  0.20 11.00           C
ATOM     67  OH ATYR A   7      17.818   5.683  16.838  0.20  9.00           O
ATOM     68  OXTATYR A   7      25.410   8.093  14.159  0.20 10.00           O
ATOM     69  N  BTYR A   7      22.444   6.911  12.694  0.20 10.00           N
ATOM     70  CA BTYR A   7      23.311   7.238  13.846  0.20  9.00           C
ATOM     71  C  BTYR A   7      24.755   7.425  13.432  0.20 11.00           C
ATOM     72  O  BTYR A   7      25.193   6.905  12.402  0.20  9.00           O
ATOM     73  CB BTYR A   7      23.213   6.159  14.916  0.20 10.00           C
ATOM     74  CG BTYR A   7      24.154   6.413  16.057  0.20  9.00           C
ATOM     75  CD1BTYR A   7      23.862   7.370  17.018  0.20  9.00           C
ATOM     76  CD2BTYR A   7      25.304   5.633  16.215  0.20  9.00           C
ATOM     77  CE1BTYR A   7      24.725   7.607  18.096  0.20 10.00           C
ATOM     78  CE2BTYR A   7      26.162   5.846  17.273  0.20 11.00           C
ATOM     79  CZ BTYR A   7      25.863   6.824  18.217  0.20 11.00           C
ATOM     80  OH BTYR A   7      26.735   7.027  19.251  0.20  9.00           O
ATOM     81  OXTBTYR A   7      25.510   8.093  14.159  0.20 10.00           O
TER
ATOM     82  N   GLY B   1      15.043  19.706  22.649  1.00  9.00           N
ATOM     83  CA  GLY B   1      15.000  19.301  21.198  1.00 11.00           C
ATOM     84  C   GLY B   1      16.037  18.234  20.966  1.00 10.00           C
ATOM     85  O   GLY B   1      16.529  17.615  21.928  1.00  9.00           O
ATOM     86  N   ASN B   2      16.396  18.017  19.702  1.00  9.00           N
ATOM     87  CA  ASN B   2      17.530  17.132  19.378  1.00  9.00           C
ATOM     88  C   ASN B   2      18.811  17.631  19.974  1.00  9.00           C
ATOM     89  O   ASN B   2      19.074  18.836  19.973  1.00  9.00           O
ATOM     90  CB  ASN B   2      17.706  16.975  17.888  1.00 11.00           C
ATOM     91  CG  ASN B   2      16.468  16.436  17.239  1.00 10.00           C
ATOM     92  OD1 ASN B   2      16.027  15.321  17.563  1.00  9.00           O
ATOM     93  ND2 ASN B   2      15.848  17.249  16.378  1.00  9.00           N
ATOM     94  N   ASN B   3      19.614  16.684  20.452  1.00 11.00           N
ATOM     95  CA  ASN B   3      20.859  16.998  21.136  1.00 11.00           C
ATOM     96  C   ASN B   3      22.097  16.426  20.442  1.00 10.00           C
ATOM     97  O   ASN B   3      22.180  15.213  20.195  1.00 11.00           O
ATOM     98  CB  ASN B   3      20.793  16.472  22.589  1.00 10.00           C
ATOM     99  CG  ASN B   3      22.046  16.833  23.408  1.00  9.00           C
ATOM    100  OD1 ASN B   3      22.350  18.019  23.619  1.00 11.00           O
ATOM    101  ND2 ASN B   3      22.781  15.809  23.853  1.00  9.00           N
ATOM    102  N  AGLN B   4      23.047  17.322  20.145  0.30 11.00           N
ATOM    103  CA AGLN B   4      24.436  16.982  19.746  0.30 10.00           C
ATOM    104  C  AGLN B   4      25.487  17.700  20.635  0.30 11.00           C
ATOM    105  O  AGLN B   4      25.599  18.937  20.662  0.30 11.00           O
ATOM    106  CB AGLN B   4      24.708  17.242  18.258  0.30 11.00           C
ATOM    107  CG AGLN B   4      25.996  16.552  17.760  0.30  9.00           C
ATOM    108  CD AGLN B   4      26.556  17.138  16.458  0.30 10.00           C
ATOM    109  OE1AGLN B   4      26.796  18.362  16.357  0.30  9.00           O
ATOM    110  NE2AGLN B   4      26.802  16.255  15.456  0.30 10.00           N
ATOM    111  N  BGLN B   4      23.147  17.322  20.145  0.30 11.00           N
ATOM    112  CA BGLN B   4      24.536  16.982  19.746  0.30 10.00           C
ATOM    113  C  BGLN B   4      25.587  17.700  20.635  0.30 11.00           C
ATOM    114  O  BGLN B   4      25.699  18.937  20.662  0.30 11.00           O
ATOM    115  CB BGLN B   4      24.808  17.242  18.258  0.30 11.00           C
ATOM    116  CG BGLN B   4      24.009  16.298  17.334  0.30  9.00           C
ATOM    117  CD BGLN B   4      24.201  16.577  15.838  0.30 10.00           C
ATOM    118  OE1BGLN B   4      24.536  17.714  15.436  0.30  9.00           O
ATOM    119  NE2BGLN B   4      24.021  15.524  15.000  0.30 10.00           N
ATOM    120  N   GLN B   5      26.206  16.915  21.418  1.00  9.00           N
ATOM    121  CA  GLN B   5      27.322  17.455  22.187  1.00  9.00           C
ATOM    122  C   GLN B   5      28.646  16.862  21.719  1.00  9.00           C
ATOM    123  O   GLN B   5      28.820  15.640  21.601  1.00 11.00           O
ATOM    124  CB  GLN B   5      27.108  17.277  23.694  1.00  9.00           C
ATOM    125  CG  GLN B   5      25.881  18.044  24.194  1.00 11.00           C
ATOM    126  CD  GLN B   5      25.396  17.508  25.501  1.00 11.00           C
ATOM    127  OE1 GLN B   5      24.826  16.419  25.549  1.00 10.00           O
ATOM    128  NE2 GLN B   5      25.601  18.281  26.586  1.00  9.00           N
ATOM    129  N   ASN B   6      29.566  17.758  21.403  1.00 11.00           N
ATOM    130  CA  ASN B   6      30.883  17.404  20.865  1.00 10.00           C
ATOM    131  C   ASN B   6      31.906  17.855  21.871  1.00 11.00           C
ATOM    132  O   ASN B   6      32.271  19.037  21.921  1.00 10.00           O
ATOM    133  CB  ASN B   6      31.117  18.110  19.540  1.00 10.00           C
ATOM    134  CG  ASN B   6      30.013  17.829  18.550  1.00 11.00           C
ATOM    135  OD1 ASN B   6      29.850  16.698  18.098  1.00 10.00           O
ATOM    136  ND2 ASN B   6      29.247  18.841  18.226  1.00 10.00           N
ATOM    137  N  ATYR B   7      32.344  16.911  22.694  0.40 10.00           N
ATOM    138  CA ATYR B   7      33.211  17.238  23.846  0.40  9.00           C
ATOM    139  C  ATYR B   7      34.655  17.425  23.432  0.40 11.00           C
ATOM    140  O  ATYR B   7      35.093  16.905  22.402  0.40  9.00           O
ATOM    141  CB ATYR B   7      33.113  16.159  24.916  0.40 10.00           C
ATOM    142  CG ATYR B   7      31.717  16.023  25.449  0.40  9.00           C
ATOM    143  CD1ATYR B   7      31.262  16.850  26.467  0.40  9.00           C
ATOM    144  CD2ATYR B   7      30.823  15.115  24.874  0.40  9.00           C
ATOM    145  CE1ATYR B   7      29.956  16.743  26.963  0.40 10.00           C
ATOM    146  CE2ATYR B   7      29.532  15.000  25.343  0.40 11.00           C
ATOM    147  CZ ATYR B   7      29.099  15.823  26.378  0.40 11.00           C
ATOM    148  OH ATYR B   7      27.818  15.683  26.838  0.40  9.00           O
ATOM    149  OXTATYR B   7      35.410  18.093  24.159  0.40 10.00           O
ATOM    150  N  BTYR B   7      32.444  16.911  22.694  0.40 10.00           N
ATOM    151  CA BTYR B   7      33.311  17.238  23.846  0.40  9.00           C
ATOM    152  C  BTYR B   7      34.755  17.425  23.432  0.40 11.00           C
ATOM    153  O  BTYR B   7      35.193  16.905  22.402  0.40  9.00           O
ATOM    154  CB BTYR B   7      33.213  16.159  24.916  0.40 10.00           C
ATOM    155  CG BTYR B   7      34.154  16.413  26.057  0.40  9.00           C
ATOM    156  CD1BTYR B   7      33.862  17.370  27.018  0.40  9.00           C
ATOM    157  CD2BTYR B   7      35.304  15.633  26.215  0.40  9.00           C
ATOM    158  CE1BTYR B   7      34.725  17.607  28.096  0.40 10.00           C
ATOM    159  CE2BTYR B   7      36.162  15.846  27.273  0.40 11.00           C
ATOM    160  CZ BTYR B   7      35.863  16.824  28.217  0.40 11.00           C
ATOM    161  OH BTYR B   7      36.735  17.027  29.251  0.40  9.00           O
ATOM    162  OXTBTYR B   7      35.510  18.093  24.159  0.40 10.00           O
TER
END
"""

# This to make sure NCS will be used as defined below
eff_str = """
pdb_interpretation {
  ncs_search {
    validate_user_supplied_groups = False
  }
  ncs_group {
    reference = chain A
    selection = chain B
  }
}
refinement {
  occupancies {
    individual = chain A and resseq 1
    constrained_group {
      selection = chain B and resseq 2
    }
    constrained_group {
      selection = chain A and resseq 4 and altloc A
    }
    constrained_group {
      selection = chain A and resseq 4 and altloc B
    }
  }
}
"""

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  Occupancy refinement (altlocs) using user-supplied selections.
  """
  # user-supplsied NCS groups
  eff = "%s.eff"%prefix
  with open(eff,"w") as fo:
    fo.write(eff_str)
  # answer model
  pdb_answer = "%s_answer.pdb"%prefix
  with open(pdb_answer,"w") as fo:
    fo.write(pdb_str_answer)
  ph_answer = iotbx.pdb.input(file_name=pdb_answer).construct_hierarchy()
  asc = ph_answer.atom_selection_cache()
  o_answer = ph_answer.atoms().extract_occ()
  # answer map
  mtz_file = "%s.mtz"%prefix
  cmd = " ".join([
    "phenix.fmodel",
    "%s"%pdb_answer,
    "file_name=%s"%mtz_file,
    "high_res=1.0",
    ">%s.zlog"%prefix])
  assert easy_run.call(cmd)==0
  # poor model
  pdb_poor = "%s_poor.pdb"%prefix
  with open(pdb_poor,"w") as fo:
    fo.write(pdb_str_poor)
  # Run 1: ncs_constraints=True
  args = [pdb_poor, mtz_file, eff, "run=occupancy", "ncs_constraints=True"]
  r = run_real_space_refine(args = args, prefix = prefix+"_True", sorry_expected = True)
  line = r.r.stderr_lines[-1].strip()
  assert line == "Sorry: NCS constraints and custom occupancy selections are not supported."
  #
  # Run 2: ncs_constraints=False
  args = [pdb_poor, mtz_file, eff, "run=occupancy", "ncs_constraints=False","resolution_factor=0.25"]
  r = run_real_space_refine(args = args, prefix = prefix+"_False")
  
  #
  #o_T_1=iotbx.pdb.input("%s_True_0_real_space_refined_000.pdb"%prefix).atoms().extract_occ()
  #o_T_2=iotbx.pdb.input("%s_True_1_real_space_refined_000.pdb"%prefix).atoms().extract_occ()
  #o_F_1=iotbx.pdb.input("%s_False_0_real_space_refined_000.pdb"%prefix).atoms().extract_occ()
  #o_F_2=iotbx.pdb.input("%s_False_1_real_space_refined_000.pdb"%prefix).atoms().extract_occ()
  ##
  #assert approx_equal(o_F_1, o_answer_1, 0.1)
  #assert approx_equal(o_F_2, o_answer_2, 0.1)
  ##
  #assert approx_equal(    o_T_1, o_answer_1, 0.021)
  #assert not_approx_equal(o_T_2, o_answer_2, 0.1)
  ##
  #selA = asc.selection("chain A")
  #selB = asc.selection("chain B")
  #assert approx_equal(o_T_1.select(selA), o_T_1.select(selB))
  #assert approx_equal(o_T_2.select(selA), o_T_2.select(selB))

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK time =%8.3f"%(time.time() - t0))
