from __future__ import print_function
import time, os
from libtbx import easy_run
import iotbx.pdb
import libtbx.load_env
from phenix_regression.real_space_refine import run_real_space_refine

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  Make sure non-P1 and reference_coordinate_restraints.enabled=true works.
  This exercises making sure that box'ed restraints do not contain reference
  coordinate restraints. Otherwise refinement takes forever and results in
  poor model.
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/real_space_refine/data/tst_22.pdb",
    test=os.path.isfile)
  mtz = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/real_space_refine/data/tst_22.mtz",
    test=os.path.isfile)
  #
  args = [
    "%s"%pdb,
    "%s"%mtz,
    "rotamers.fit=none",
    "rotamers.restraints.enabled=false",
    "macro_cycles=1",
    "reference_coordinate_restraints.enabled=true"]
  r = run_real_space_refine(args = args, prefix = prefix)
  # check
  b = []
  a = []
  with open(r.log, "r") as fo:
    for l in fo.readlines():
      if(" Bond      :" in l): b.append(l)
      if(" Angle     :" in l): a.append(l)
  b = b[-1]
  a = a[-1]
  assert float(b.split()[2]) < 0.02
  assert float(a.split()[2]) < 1.0

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK time =%8.3f"%(time.time() - t0))
