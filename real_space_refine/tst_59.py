from __future__ import print_function
import time
import iotbx.pdb
from scitbx.array_family import flex
import os
import libtbx.load_env
from phenix_regression.real_space_refine import run_real_space_refine

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  Residues 59 and 69 (tests 1 and 2) should not move or move only a little.
  """
  files = {59:1, 69:2}
  for k, v in zip(files.keys(), files.values()):
    c="phenix_regression/real_space_refine/data/%s_%d"%(prefix, v)
    pdb = libtbx.env.find_in_repositories(
      relative_path="%s.pdb"%c,
      test=os.path.isfile)
    mrc = libtbx.env.find_in_repositories(
      relative_path="%s.ccp4"%c,
      test=os.path.isfile)
    #
    args = [pdb, mrc, "resolution=2.6", "macro_cycles=1", 
            "ignore_symmetry_conflicts=True",
            "run=local_grid_search+minimization_global"]
    r  = run_real_space_refine(args = args, prefix = prefix+"_%d"%v)
    #
    ph1 = iotbx.pdb.input(file_name=pdb).construct_hierarchy()
    asc = ph1.atom_selection_cache()
    sel = asc.selection("resseq %d"%k)
    xyz1 = ph1.select(sel).atoms().extract_xyz()
    #
    ph2 = iotbx.pdb.input(file_name=r.pdb).construct_hierarchy()
    xyz2 = ph2.select(sel).atoms().extract_xyz()
    #
    d = flex.max(flex.sqrt((xyz1 - xyz2).dot()))
    assert d < 0.3, d

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK time =%8.3f"%(time.time() - t0))
