from __future__ import print_function
import time, os
from libtbx import easy_run
import iotbx.pdb
import libtbx.load_env
from phenix_regression.real_space_refine import run_real_space_refine

def run(prefix=os.path.basename(__file__).replace(".py","_real_space_refine")):
  """
  Make sure input model with blank chain ID does not create a problem.
  """
  # PDB file is NOT inlined into the test script intentionally!
  # DO NOT MODIFY by inlining PDB file content into this script!
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/real_space_refine/data/tst_real_space_refine_25.pdb",
    test=os.path.isfile)
  cmd = " ".join([
    "phenix.fmodel",
    "%s"%pdb,
    "file_name=%s.mtz"%prefix,
    "high_res=2",
    ">%s.zlog"%prefix])
  assert easy_run.call(cmd)==0
  args = [
    "%s"%pdb,
    "%s.mtz"%prefix,
    "macro_cycles=1",
    "ramachandran_plot_restraints.enabled=false",
    "write_all_states=true",
    "macro_cycles=1",
    "secondary_structure.enabled=True"]
  r = run_real_space_refine(args = args, prefix = prefix,
      assert_is_similar_hierarchy=False)
  #
  assert os.path.isfile(r.pdb)
  assert os.path.isfile(r.pdb_all)
  assert os.path.isfile(r.geo_ini)

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK time =%8.3f"%(time.time() - t0))
