[ ![wwPDB][1]][2]

![][3]

PDB FORMAT Version 2.3

[Main Index][4]

[ATOM][5]

[MODEL][6]

[SIGATM][7]

[ANISOU][8]

[SIGUIJ][9]

[TER][10]

[HETATM][11]

[ENDMDL][11]

# Coordinate Section

The Coordinate Section contains the collection of atomic coordinates as well as the MODEL and ENDMDL records.

* * *

## MODEL

**Overview**

The MODEL record specifies the model serial number when multiple structures are presented in a single coordinate entry, as is often the case with structures determined by NMR.

**Record Format**


    COLUMNS     DATA TYPE       FIELD       DEFINITION
    -------------------------------------------------------------
     1 - 6      Record name     "MODEL "
    11 - 14     Integer         serial      Model serial number.


**Details**

* This record is used only when more than one model appears in an entry. Generally, it is employed only for NMR structures. The chemical connectivity should be the same for each model. ATOM, HETATM, SIGATM, SIGUIJ, ANISOU, and TER records for each model structure are interspersed as needed between MODEL and ENDMDL records.
* The numbering of models is sequential beginning with 1.
* If a collection contains more than 99,999 total atoms, then more than one entry must be made. In such a case the collection is divided between models (between an ENDMDL and the following MODEL record) and the model numbering is sequential throughout such a set of entries.

**Verification/Validation/Value Authority Control**

Entries with multiple structures in the EXPDTA record are checked for corresponding pairs of MODEL/ ENDMDL records, and for consecutively numbered models.

**Relationships to Other Record Types**

Each MODEL must have a corresponding ENDMDL record.

In the case of an NMR entry the EXPDTA record states the number of model structures that are present in the individual entry.

**Example**


             1         2         3         4         5         6         7         8
    12345678901234567890123456789012345678901234567890123456789012345678901234567890
    MODEL        1
    ATOM      1  N   ALA     1      11.104   6.134  -6.504  1.00  0.00           N
    ATOM      2  CA  ALA     1      11.639   6.071  -5.147  1.00  0.00           C
    ...
    ...
    ATOM    293 1HG  GLU    18     -14.861  -4.847   0.361  1.00  0.00           H
    ATOM    294 2HG  GLU    18     -13.518  -3.769   0.084  1.00  0.00           H
    TER     295      GLU    18
    ENDMDL
    MODEL        2
    ATOM    296  N   ALA     1      10.883   6.779  -6.464  1.00  0.00           N
    ATOM    297  CA  ALA     1      11.451   6.531  -5.142  1.00  0.00           C
    ...
    ...
    ATOM    588 1HG  GLU    18     -13.363  -4.163  -2.372  1.00  0.00           H
    ATOM    589 2HG  GLU    18     -12.634  -3.023  -3.475  1.00  0.00           H
    TER     590      GLU    18
    ENDMDL


* * *

## ATOM

**Overview**

The ATOM records present the atomic coordinates for standard residues (see http://deposit.pdb.org/public-component-erf.cif). They also present the occupancy and temperature factor for each atom. Heterogen coordinates use the HETATM record type. The element symbol is always present on each ATOM record; segment identifier and charge are optional.

**Record Format**


    COLUMNS      DATA TYPE        FIELD      DEFINITION
    ------------------------------------------------------
     1 -  6      Record name      "ATOM    "
     7 - 11      Integer          serial     Atom serial number.
    13 - 16      Atom             name       Atom name.
    17           Character        altLoc     Alternate location indicator.
    18 - 20      Residue name     resName    Residue name.
    22           Character        chainID    Chain identifier.
    23 - 26      Integer          resSeq     Residue sequence number.
    27           AChar            iCode      Code for insertion of residues.
    31 - 38      Real(8.3)        x          Orthogonal coordinates for X in
                                             Angstroms
    39 - 46      Real(8.3)        y          Orthogonal coordinates for Y in
                                             Angstroms
    47 - 54      Real(8.3)        z          Orthogonal coordinates for Z in
                                             Angstroms
    55 - 60      Real(6.2)        occupancy  Occupancy.
    61 - 66      Real(6.2)        tempFactor Temperature factor.
    77 - 78      LString(2)       element    Element symbol, right-justified.
    79 - 80      LString(2)       charge     Charge on the atom.


**Details**

* ATOM records for proteins are listed from amino to carboxyl terminus.
* Nucleic acid residues are listed from the 5' to the 3' terminus.
* No ordering is specified for polysaccharides.
* The list of ATOM records in a chain is terminated by a TER record.
* If more than one model is present in the entry, each model is delimited by MODEL and ENDMDL records.
* If an atom is provided in more than one position, then a non-blank alternate location indicator must be used as the alternate location indicator for each of the positions. Within a residue all atoms that are associated with each other in a given conformation are assigned the same alternate position indicator.
* For atoms that are in alternate sites indicated by the alternate site indicator, sorting of atoms in the ATOM/ HETATM list uses the following general rules:

In the simple case that involves a few atoms or a few residues with alternate sites, the coordinates occur one after the other in the entry.

In the case of a large heterogen groups which are disordered, the atoms for each conformer are listed together.

* The insertion code is commonly used in sequence numbering
* If the depositor provides the data, then the isotropic B value is given for the temperature factor.
* If there are neither isotropic B values from the depositor, nor anisotropic temperature factors in ANISOU, then the default value of 0.0 is used for the temperature factor.
* Columns 77 - 78 contain the atom's element symbol (as given in the periodic table), right-justified.
* Columns 79 - 80 indicate any charge on the atom, e.g., 2+, 1-. In most cases these are blank.

**Verification/Validation/Value Authority Control**

PDB checks ATOM/HETATM records for PDB format, sequence information, and packing. The PDB reserves the right to return deposited coordinates to the author for transformation into PDB format.

**Relationships to Other Record Types**

The ATOM records are compared to the corresponding sequence database. Residue discrepancies appear in the SEQADV record. Missing atoms are annotated in the remarks. HETATM records are formatted in the same way as ATOM records. The sequence implied by ATOM records must be identical to that given in SEQRES, with the exception that residues that have no coordinates, e.g., due to disorder, must appear in SEQRES.

**Example **


             1         2         3         4         5         6         7         8
    12345678901234567890123456789012345678901234567890123456789012345678901234567890
    ATOM    145  N   VAL A  25      32.433  16.336  57.540  1.00 11.92           N
    ATOM    146  CA  VAL A  25      31.132  16.439  58.160  1.00 11.85           C
    ATOM    147  C   VAL A  25      30.447  15.105  58.363  1.00 12.34           C
    ATOM    148  O   VAL A  25      29.520  15.059  59.174  1.00 15.65           O
    ATOM    149  CB AVAL A  25      30.385  17.437  57.230  0.28 13.88           C
    ATOM    150  CB BVAL A  25      30.166  17.399  57.373  0.72 15.41           C
    ATOM    151  CG1AVAL A  25      28.870  17.401  57.336  0.28 12.64           C
    ATOM    152  CG1BVAL A  25      30.805  18.788  57.449  0.72 15.11           C
    ATOM    153  CG2AVAL A  25      30.835  18.826  57.661  0.28 13.58           C
    ATOM    154  CG2BVAL A  25      29.909  16.996  55.922  0.72 13.25           C


**Known Problems**

No distinction is made between ribo- and deoxyribonucleotides in the SEQRES records. These residues are identified with the same residue name (i.e., A, C, G, T, U).

* * *

## SIGATM

**Overview**

The SIGATM records present the standard deviation of atomic parameters as they appear in ATOM and HETATM records.

**Record Format **


    COLUMNS      DATA TYPE       FIELD       DEFINITION
    -----------------------------------------------------------------------
     1 -  6      Record name     "SIGATM"
     7 - 11      Integer         serial       Atom serial number.
    13 - 16      Atom            name         Atom name.
    17           Character       altLoc       Alternate location indicator.
    18 - 20      Residue name    resName      Residue name.
    22           Character       chainID      Chain identifier.
    23 - 26      Integer         resSeq       Residue sequence number.
    27           AChar           iCode        Insertion code.
    31 - 38      Real(8.3)       sigX         Standard deviations of the stored
                                              coordinates (Angstroms).
    39 - 46      Real(8.3)       sigY         Standard deviations of the stored
                                              coordinates (Angstroms).
    47 - 54      Real(8.3)       sigZ         Standard deviations of the stored
                                              coordinates (Angstroms).
    55 - 60      Real(6.2)       sigOcc       Standard deviation of occupancy.
    61 - 66      Real(6.2)       sigTemp      Standard deviation of temperature
                                              factor.
    77 - 78      LString(2)      element      Element symbol, right-justified.
    79 - 80      LString(2)      charge       Charge on the atom.


**Details**

* Columns 7 - 27 and 73 - 80 are identical to the corresponding ATOM/HETATM record.
* Each SIGATM record immediately follows the corresponding ATOM/HETATM record.
* SIGATM is provided only for ATOM/HETATM records for which values are supplied by the depositor and only when the value is not zero (0).

**Verification/Validation/Value Authority Control **

The depositor provides SIGATM records, PDB verifies their format.

**Relationships to Other Record Types **

SIGATM is related to the immediately preceding ATOM/HETATM record.

**Example**


             1         2         3         4         5         6         7         8
    12345678901234567890123456789012345678901234567890123456789012345678901234567890
    ATOM    230  N   PRO    15      20.860  29.640  13.460  1.00 12.20           N
    SIGATM  230  N   PRO    15       0.040   0.030   0.030  0.00  0.00           N
    ATOM    231  CA  PRO    15      22.180  29.010  12.960  1.00 14.70           C
    SIGATM  231  CA  PRO    15       0.060   0.040   0.050  0.00  0.00           C
    ATOM    232  C   PRO    15      23.170  30.090  12.670  1.00 19.10           C
    SIGATM  232  C   PRO    15       0.080   0.070   0.060  0.00  0.00           C
    ATOM    233  O   PRO    15      24.360  29.860  12.670  1.00 17.50           O
    SIGATM  233  O   PRO    15       0.040   0.030   0.030  0.00  0.00           O
    ATOM    234  CB  PRO    15      21.710  28.220  11.640  1.00 17.70           C
    SIGATM  234  CB  PRO    15       0.060   0.040   0.050  0.00  0.00           C
    ATOM    235  CG  PRO    15      20.470  28.710  11.590  1.00 23.90           C
    SIGATM  235  CG  PRO    15       0.080   0.060   0.060  0.00  0.00           C
    ATOM    236  CD  PRO    15      19.640  29.320  12.660  1.00 15.50           C
    SIGATM  236  CD  PRO    15       0.060   0.040   0.050  0.00  0.00           C
    ATOM    237  HA  PRO    15      22.630  28.400  13.620  1.00 14.70           H
    ATOM    238 1HB  PRO    15      22.240  28.540  10.860  1.00 17.70           H
    ATOM    239 2HB  PRO    15      21.670  27.240  11.840  1.00 17.70           H
    ATOM    240 1HG  PRO    15      20.360  29.240  10.740  1.00 23.90           H
    ATOM    241 2HG  PRO    15      19.900  28.120  11.020  1.00 23.90           H
    ATOM    242 1HD  PRO    15      19.230  30.160  12.320  1.00 15.50           H
    ATOM    243 2HD  PRO    15      19.120  28.600  13.120  1.00 15.50           H


* * *

## ANISOU

**Overview**

The ANISOU records present the anisotropic temperature factors.

**Record Format**


    COLUMNS     DATA TYPE         FIELD          DEFINITION
    --------------------------------------------------------
     1 - 6      Record name       "ANISOU"
     7 - 11     Integer           serial         Atom serial number.
    13 - 16     Atom              name           Atom name.
    17          Character         altLoc         Alternate location indicator
    18 - 20     Residue name      resName        Residue name.
    22          Character         chainID        Chain identifier.
    23 - 26     Integer           resSeq         Residue sequence number.
    27          AChar             iCode          Insertion code.
    29 - 35     Integer           u[0][0]        U(1,1)
    36 - 42     Integer           u[1][1]        U(2,2)
    43 - 49     Integer           u[2][2]        U(3,3)
    50 - 56     Integer           u[0][1]        U(1,2)
    57 - 63     Integer           u[0][2]        U(1,3)
    64 - 70     Integer           u[1][2]        U(2,3)
    77 - 78     LString(2)        element        Element symbol, right-justified.
    79 - 80     LString(2)        charge         Charge on the atom.


**Details**

* Columns 7 - 27 and 73 - 80 are identical to the corresponding ATOM/HETATM record.
* The anisotropic temperature factors (columns 29 - 70) are scaled by a factor of 10**4 (Angstroms**2) and are presented as integers.
* The anisotropic temperature factors are stored in the same coordinate frame as the atomic coordinate records.
* ANISOU values are listed only if they have been provided by the depositor.

**Verification/Validation/Value Authority Control **

The depositor provides ANISOU records, and the PDB verifies their format.

**Relationships to Other Record Types **

The anisotropic temperature factors are related to the corresponding ATOM/HETATM isotropic temperature factors as B(eq), as described in the ATOM and HETATM sections.

**Example**


             1         2         3         4         5         6         7         8
    12345678901234567890123456789012345678901234567890123456789012345678901234567890
    ATOM    107  N   GLY    13      12.681  37.302 -25.211 1.000 15.56           N
    ANISOU  107  N   GLY    13     2406   1892   1614    198    519   -328       N
    ATOM    108  CA  GLY    13      11.982  37.996 -26.241 1.000 16.92           C
    ANISOU  108  CA  GLY    13     2748   2004   1679    -21    155   -419       C
    ATOM    109  C   GLY    13      11.678  39.447 -26.008 1.000 15.73           C
    ANISOU  109  C   GLY    13     2555   1955   1468     87    357   -109       C
    ATOM    110  O   GLY    13      11.444  40.201 -26.971 1.000 20.93           O
    ANISOU  110  O   GLY    13     3837   2505   1611    164   -121    189       O
    ATOM    111  N   ASN    14      11.608  39.863 -24.755 1.000 13.68           N
    ANISOU  111  N   ASN    14     2059   1674   1462     27    244    -96       N


* * *

## SIGUIJ

**Overview**

The SIGUIJ records present the standard deviations of anisotropic temperature factors scaled by a factor of 10**4 (Angstroms**2).

**Record Format**


    COLUMNS      DATA TYPE        FIELD         DEFINITION
    ------------------------------------------------------------
     1 - 6       Record name      "SIGUIJ"
     7 - 11      Integer          serial        Atom serial number.
    13 - 16      Atom             name          Atom name.
    17           Character        altLoc        Alternate location indicator.
    18 - 20      Residue name     resName       Residue name.
    22           Character        chainID       Chain identifier.
    23 - 26      Integer          resSeq        Residue sequence number.
    27           AChar            iCode         Insertion code.
    29 - 35      Integer          sig[1][1]     Sigma U(1,1)
    36 - 42      Integer          sig[2][2]     Sigma U(2,2)
    43 - 49      Integer          sig[3][3]     Sigma U(3,3)
    50 - 56      Integer          sig[1][2]     Sigma U(1,2)
    57 - 63      Integer          sig[1][3]     Sigma U(1,3)
    64 - 70      Integer          sig[2][3]     Sigma U(2,3)
    77 - 78      LString(2)       element       Element symbol, right-justified.
    79 - 80      LString(2)       charge        Charge on the atom.


**Details **

* Columns 7 - 27 and 73 - 80 are identical to the corresponding ATOM/HETATM record.
* SIGUIJ are listed only if they have been provided by the depositor and only if they are not zero.

**Verification/Validation/Value Authority Control **

The depositor provides SIGUIJ records, PDB verifies their format.

**Relationships to Other Record Types **

The standard deviations for the anisotropic temperature factors are related to the corresponding ATOM/ HETATM ANISOU temperature factors.

**Example**


             1         2         3         4         5         6         7         8
    12345678901234567890123456789012345678901234567890123456789012345678901234567890
    ATOM    107  N   GLY    13      12.681  37.302 -25.211 1.000 15.56           N
    ANISOU  107  N   GLY    13     2406   1892   1614    198    519   -328       N
    SIGUIJ  107  N   GLY    13       10     10     10     10    10      10       N
    ATOM    108  CA  GLY    13      11.982  37.996 -26.241 1.000 16.92           C
    ANISOU  108  CA  GLY    13     2748   2004   1679    -21    155   -419       C
    SIGUIJ  108  CA  GLY    13       10     10     10     10    10      10       C
    ATOM    109  C   GLY    13      11.678  39.447 -26.008 1.000 15.73           C
    ANISOU  109  C   GLY    13     2555   1955   1468     87    357   -109       C
    SIGUIJ  109  C   GLY    13       10     10     10     10    10      10       C
    ATOM    110  O   GLY    13      11.444  40.201 -26.971 1.000 20.93           O
    ANISOU  110  O   GLY    13     3837   2505   1611    164   -121    189       O
    SIGUIJ  110  O   GLY    13       10     10     10     10    10      10       O
    ATOM    111  N   ASN    14      11.608  39.863 -24.755 1.000 13.68           N
    ANISOU  111  N   ASN    14     2059   1674   1462     27    244    -96       N
    SIGUIJ  111  N   ASN    14       10     10     10     10    10      10       N


* * *

## TER

**Overview**

The TER record indicates the end of a list of ATOM/HETATM records for a chain.

**Record Format**


    COLUMNS     DATA TYPE         FIELD           DEFINITION
    ------------------------------------------------------
     1 - 6      Record name       "TER     "
     7 - 11     Integer           serial          Serial number.
    18 - 20     Residue name      resName         Residue name.
    22          Character         chainID         Chain identifier.
    23 - 26     Integer           resSeq          Residue sequence
                                                  number.
    27          AChar             iCode           Insertion code.


**Details**

* Every chain of ATOM/HETATM records presented on SEQRES records is terminated with a TER record.
* The TER records occur in the coordinate section of the entry, and indicate the last residue presented for each polypeptide and/or nucleic acid chain for which there are coordinates. For proteins, the residue defined on the TER record is the carboxy-terminal residue; for nucleic acids it is the 3'-terminal residue.
* For a cyclic molecule, the choice of termini is arbitrary.
* Terminal oxygen atoms are presented as OXT for proteins, and as O5T or O3T for nucleic acids.
* The TER record has the same residue name, chain identifier, sequence number and insertion code as the terminal residue. The serial number of the TER record is one number greater than the serial number of the ATOM/HETATM preceding the TER.

**Verification/Validation/Value Authority Control**

TER must appear at the end carboxy or 3' of a chain. For proteins, there is usually a terminal oxygen, labeled OXT. The validation program checks for the occurrence of TER and OXT records.

**Relationships to Other Record Types **

The residue name appearing on the TER record must be the same as the residue name of the immediately preceding ATOM or non-water HETATM record.

**Example**


             1         2         3         4         5         6         7         8
    12345678901234567890123456789012345678901234567890123456789012345678901234567890
    ATOM   4150  H   ALA A 431       8.674  16.036  12.858  1.00  0.00           H
    TER    4151      ALA A 431
    ATOM   1403  O   PRO P  22      12.701  33.564  15.827  1.09 18.03           O
    ATOM   1404  CB  PRO P  22      13.512  32.617  18.642  1.09  9.32           C
    ATOM   1405  CG  PRO P  22      12.828  33.382  19.740  1.09 12.23           C
    ATOM   1406  CD  PRO P  22      12.324  34.603  18.985  1.09 11.47           C
    HETATM 1407  CA  BLE P   1      14.625  32.240  14.151  1.09 16.76           C
    HETATM 1408  CB  BLE P   1      15.610  33.091  13.297  1.09 16.56           C
    HETATM 1409  CG  BLE P   1      15.558  34.629  13.373  1.09 14.27           C
    HETATM 1410  CD1 BLE P   1      16.601  35.208  12.440  1.09 14.75           C
    HETATM 1411  CD2 BLE P   1      14.209  35.160  12.930  1.09 15.60           C
    HETATM 1412  N   BLE P   1      14.777  32.703  15.531  1.09 14.79           N
    HETATM 1413  B   BLE P   1      14.921  30.655  14.194  1.09 15.56           B
    HETATM 1414  O1  BLE P   1      14.852  30.178  12.832  1.09 16.10           O
    HETATM 1415  O2  BLE P   1      13.775  30.147  14.862  1.09 20.95           O
    TER    1416      BLE P   1


* * *

## HETATM

**Overview**

The HETATM records present the atomic coordinate records for atoms within "non-standard" groups. These records are used for water molecules and atoms presented in HET groups (see http://deposit.pdb.org/public-component-erf.cif).

**Record Format**


    COLUMNS     DATA TYPE        FIELD         DEFINITION
    --------------------------------------------------------------
     1 - 6      Record name      "HETATM"
     7 - 11     Integer          serial        Atom serial number.
    13 - 16     Atom             name          Atom name.
    17          Character        altLoc        Alternate location indicator.
    18 - 20     Residue name     resName       Residue name.
    22          Character        chainID       Chain identifier.
    23 - 26     Integer          resSeq        Residue sequence number.
    27          AChar            iCode         Code for insertion of residues.
    31 - 38     Real(8.3)        x             Orthogonal coordinates for X.
    39 - 46     Real(8.3)        y             Orthogonal coordinates for Y.
    47 - 54     Real(8.3)        z             Orthogonal coordinates for Z.
    55 - 60     Real(6.2)        occupancy     Occupancy.
    61 - 66     Real(6.2)        tempFactor    Temperature factor.
    77 - 78     LString(2)       element       Element symbol; right-justified.
    79 - 80     LString(2)       charge        Charge on the atom.


**Details**

* The x, y, z coordinates are in Angstrom units.
* No ordering is specified for polysaccharides.
* See the HET section of this document regarding naming of heterogens. See the HET dictionary for residue names, formulas, and topology of the HET groups that have appeared so far in the PDB (see http://deposit.pdb.org/public-component-erf.cif).
* If the depositor provides the data, then the isotropic B value is given for the temperature factor.
* If there are neither isotropic B values from the depositor, nor anisotropic temperature factors in ANISOU, then the default value of 0.0 is used for the temperature factor.
* Insertion codes, segment id, and element naming are fully described in the ATOM section of this document.

**Verification/Validation/Value Authority Control**

PDB processing programs check ATOM/HETATM records for PDB format, sequence information, and packing. The PDB reserves the right to return deposited coordinates to the author for transformation into PDB format.

**Relationships to Other Record Types**

HETATM records must have corresponding HET, HETNAM, FORMUL and CONECT records, except for waters.

**Example**


             1         2         3         4         5         6         7         8
    12345678901234567890123456789012345678901234567890123456789012345678901234567890
    HETATM 1357 MG    MG   168       4.669  34.118  19.123  1.00  3.16          MG2+
    HETATM 3835 FE   HEM     1      17.140   3.115  15.066  1.00 14.14          FE3+


* * *

## ENDMDL

**Overview**

The ENDMDL records are paired with MODEL records to group individual structures found in a coordinate entry.

**Record Format**


    COLUMNS    DATA TYPE       FIELD        DEFINITION
    --------------------------------------------------
     1 - 6     Record name     "ENDMDL"


**Details**

* MODEL/ENDMDL records are used only when more than one structure is presented in the entry, as is often the case with NMR entries.
* All the models in a multi-model entry must represent the same structure.
* Every MODEL record has an associated ENDMDL record.

**Verification/Validation/Value Authority Control **

Entries with multiple structures in the EXPDTA record are checked for corresponding pairs of MODEL/ ENDMDL records, and for consecutively numbered models.

**Relationships to Other Record Types**

There must be a corresponding MODEL record.

In the case of an NMR entry the EXPDTA record states the number of model structures that are present in the individual entry.

**Example**


             1         2         3         4         5         6         7         8
    12345678901234567890123456789012345678901234567890123456789012345678901234567890
    ...
    ...
    ATOM  14550 1HG  GLU   122     -14.364  14.787 -14.258  1.00  0.00           H
    ATOM  14551 2HG  GLU   122     -13.794  13.738 -12.961  1.00  0.00           H
    TER   14552      GLU   122
    ENDMDL
    MODEL        9
    ATOM  14553  N   SER     1     -28.280   1.567  12.004  1.00  0.00           N
    ATOM  14554  CA  SER     1     -27.749   0.392  11.256  1.00  0.00           C
    ...
    ...
    ATOM  16369 1HG  GLU   122      -3.757  18.546  -8.439  1.00  0.00           H
    ATOM  16370 2HG  GLU   122      -3.066  17.166  -7.584  1.00  0.00           H
    TER   16371      GLU   122
    ENDMDL
    MODEL       10
    ATOM  16372  N   SER     1     -22.285   7.041  10.003  1.00  0.00           N
    ATOM  16373  CA  SER     1     -23.026   6.872   8.720  1.00  0.00           C
    ...
    ...
    ATOM  18188 1HG  GLU   122      -1.467  18.282 -17.144  1.00  0.00           H
    ATOM  18189 2HG  GLU   122      -2.711  18.067 -15.913  1.00  0.00           H
    TER   18190      GLU   122
    ENDMDL


* * *

# 2007 wwPDB

   [1]: http://www.wwpdb.org/images/logo_wwpdb.gif
   [2]: http://www.wwpdb.org/index.html
   [3]: http://www.wwpdb.org/images/spacer.gif
   [4]: v2.3.html
   [5]: #ATOM
   [6]: #MODEL
   [7]: #SIGATM
   [8]: #ANISOU
   [9]: #SIGUIJ
   [10]: #TER
   [11]: #HETATM
