from __future__ import print_function

from phenix_regression.command_line import list

def exercise () :
  args=["tst_list"]
  listed_tests=list.run(args)
  print(listed_tests)  # should be libtbx.python xxxx/list.py
  assert len(listed_tests)==1
  print("OK")

if (__name__ == "__main__") :
  exercise()
