#! /bin/bash

export PHENIX_REGRESSION_DIR="`libtbx.find_in_repositories phenix_regression`"

iotbx.poscar_as_xray_structure "$PHENIX_REGRESSION_DIR"/poscar/POSCAR-* | libtbx.assert_stdin_contains_strings 'Number of scatterers: '

echo "DONE: $0"
