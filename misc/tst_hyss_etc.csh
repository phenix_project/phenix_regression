#! /bin/csh -fe

setenv LIBTBX_FULL_TESTING
set PHENIX_REGRESSION_DIR="`libtbx.find_in_repositories phenix_regression`"

#
touch gere_MAD_000
rm -f gere_MAD*

phenix.xtriage "$PHENIX_REGRESSION_DIR"/reflection_files/gere_MAD.mtz obs_labels=SEpeak log=gere_MAD.log | libtbx.assert_stdin_contains_strings 'No twinning is suspected.'

phenix.xtriage "$PHENIX_REGRESSION_DIR"/reflection_files/sec17_data_peak.sca log=sec17.log | libtbx.assert_stdin_contains_strings 'No twinning is suspected'

phenix.hyss "$PHENIX_REGRESSION_DIR"/reflection_files/gere_MAD.mtz 2 se --molecular_weight=8000 --solvent_content=0.70 --random_seed=1

iotbx.pdb.insert_scale_records $PHENIX_REGRESSION_DIR/pdb/phe.pdb | grep 'SCALE3' | libtbx.assert_stdin 'SCALE3      0.000000  0.000000  0.066667        0.00000'

iotbx.pdb.superpose_centers_of_mass $PHENIX_REGRESSION_DIR/pdb/phe.pdb $PHENIX_REGRESSION_DIR/pdb/phe.pdb reference.atom_selection='name ca' other.atom_selection='name c' output.file=tmp.pdb output.atom_selection='name c*' --space_group=p4 --unit_cell=20,20,21 | grep Rotation | libtbx.assert_stdin 'Rotation in fractional space: x,y,z'

iotbx.reflection_file_reader gere_MAD_anom_diffs.hkl=amplitudes --symmetry=gere_MAD_hyss_consensus_model.pdb --pickle=gere_MAD_anom_diffs.pickle | libtbx.assert_stdin_contains_strings 'Writing all Miller arrays to file: gere_MAD_anom_diffs.pickle'

tar zxf $PHENIX_REGRESSION_DIR/reflection_files/structure_lib_data.tgz

iotbx.python "`libtbx.show_dist_paths iotbx`"/scalepack/no_merge_original_index.py structure_lib_data | libtbx.assert_stdin_ends_with 'OK'

echo "DONE: $0"
