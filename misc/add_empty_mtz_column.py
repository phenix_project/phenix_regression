from iotbx import mtz
import sys

def run(file_name):
  mtz_object = mtz.object(file_name=file_name)
  mtz_object.crystals()[1].datasets()[0].add_column(label="EMPTY", type="F")
  mtz_object.write(file_name=file_name)

if (__name__ == "__main__"):
  run(sys.argv[1])
