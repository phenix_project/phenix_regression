#! /bin/csh -f
set PHENIX_REGRESSION_DIR="`libtbx.find_in_repositories phenix_regression`"
libtbx.parallel_simple
libtbx.parallel_simple "$PHENIX_REGRESSION_DIR"/echo4_parallel_forward | grep 'JOB wall clock time:' | libtbx.assert_line_count 4
libtbx.parallel_simple "$PHENIX_REGRESSION_DIR"/echo4_parallel_backward -j 1 | grep 'JOB wall clock time:' | libtbx.assert_line_count 4
libtbx.parallel_simple "$PHENIX_REGRESSION_DIR"/echo4_parallel_forward -j 2 | grep 'JOB wall clock time:' | libtbx.assert_line_count 4
libtbx.parallel_simple --command='echo $(MULTI:1-3)' | grep 'JOB wall clock time:' | libtbx.assert_line_count 3
libtbx.parallel_simple --command='echo $(MULTI:3-1)' | grep 'JOB wall clock time:' | libtbx.assert_line_count 3
libtbx.parallel_simple --command='echo $(MULTI:1:3)' | grep 'JOB wall clock time:' | libtbx.assert_line_count 2
libtbx.parallel_simple --command='echo $(MULTI:3:1)' | grep 'JOB wall clock time:' | libtbx.assert_line_count 2
unsetenv n
libtbx.parallel_simple --command='echo $(MULTI:0:$n)' n=2 | grep 'JOB wall clock time:' | libtbx.assert_line_count 2
libtbx.parallel_simple --command='echo $(MULTI:0:$n)' n=3 | grep 'JOB wall clock time:' | libtbx.assert_line_count 3
unsetenv b
unsetenv e
libtbx.parallel_simple --command='echo $(MULTI:$b:$e)' b=5 e=1 | grep 'JOB wall clock time:' | libtbx.assert_line_count 4
touch tst_parallel_simpleXXX ; rm -rf tst_parallel_simple???
mkdir -p tst_parallel_simple000
libtbx.parallel_simple "$PHENIX_REGRESSION_DIR"/echo4_parallel_backward --force_clean_dirs --dirs tst_parallel_simple | grep 'JOB wall clock time:' | libtbx.assert_line_count 4
cat tst_parallel_simple???/log | libtbx.assert_line_count 8
echo "DONE: $0"
