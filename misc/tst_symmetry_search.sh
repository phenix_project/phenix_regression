#! /bin/bash

export PHENIX_REGRESSION_DIR="`libtbx.find_in_repositories phenix_regression`"

iotbx.symmetry_search 2>&1 | libtbx.assert_stdin_contains_strings 'Usage: iotbx.symmetry_search pdb_file|poscar_file [parameters]'

iotbx.symmetry_search "$PHENIX_REGRESSION_DIR"/pdb/pdb1zff.ent structure_factor_d_min=3 | libtbx.assert_stdin_contains_strings 'C 1 2 1 (x-y,x+y,z)'

iotbx.symmetry_search "$PHENIX_REGRESSION_DIR"/poscar/POSCAR-Al | libtbx.assert_stdin_contains_strings 'P m -3 m'

echo "DONE: $0"
