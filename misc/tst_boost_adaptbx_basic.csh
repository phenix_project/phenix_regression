#! /bin/csh -fe

boost_adaptbx.show_environ_usage | grep _FE_ | libtbx.assert_line_count 3
boost_adaptbx.show_platform_info
boost.gcd 12 15 | libtbx.assert_stdin 3
boost.lcm 3 4 | libtbx.assert_stdin 12

echo "OK"
