from __future__ import print_function

from libtbx import test_utils

commands = [
  "cctbx.version",
  "phenix.version",
  "phenix.maps",
  "phenix.remove_outliers",
  "phenix.twin_map_utils",
  "phenix.xmanip",
]

def exercise () :
  for cmd in commands :
    test_utils.test_usage(cmd)
  print("OK")

if (__name__ == "__main__") :
  exercise()
