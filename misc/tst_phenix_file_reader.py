
from __future__ import division
from __future__ import print_function
from phenix.file_reader import any_file
import libtbx.load_env
import os

def exercise () :
  # The rest of the tests are for compatability with iotbx.file_reader.
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/enk.pdb",
    test=os.path.isfile)
  f_in = any_file(pdb_file)
  f_in.assert_file_type("pdb")
  mtz_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/enk.mtz",
    test=os.path.isfile)
  f_in = any_file(mtz_file)
  f_in.assert_file_type("hkl")
  hkl_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/enk.hkl",
    test=os.path.isfile)
  f_in = any_file(hkl_file)
  f_in.assert_file_type("hkl")
  seq_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/wizards/data/seq.dat",
    test=os.path.isfile)
  f_in = any_file(seq_file)
  f_in.assert_file_type("seq")
  cif_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/misc/dna_cns_cy5.cif",
    test=os.path.isfile)
  f_in = any_file(cif_file)
  f_in.assert_file_type("cif")
  print("OK")

if __name__ == "__main__" :
  exercise()
