
import sys

if (__name__ == "__main__") :
  from phenix.command_line import hyss
  hyss.run_phil(config_file=sys.argv[1])
