from __future__ import division
from __future__ import print_function
import sys,os
import libtbx.load_env
from libtbx.utils import Sorry

import time,sys

def tst_01(args=None):
  print("testing get_test_and_module_info")

  from phenix_regression.command_line.get_test_and_module_info import run
  from six.moves import cStringIO as StringIO
  f=StringIO()
  sys_stdout=sys.stdout
  sys.stdout=f
  test_dir="get_test_and_module_info"
  if os.path.isdir(test_dir):
    raise Sorry("Please run in directory without get_test_and_module_info dir")
  os.mkdir(test_dir)
  home_dir=os.getcwd()
  os.chdir(test_dir)

  try:
    run(args)
    output=f.getvalue()
    sys.stdout=sys_stdout
  except Exception as e:
    output=f.getvalue()
    sys.stdout=sys_stdout
    os.chdir(home_dir)
    assert 0, "%s\n%s\nget_test_and_module_info failed to run" %(output,e)

  os.chdir(home_dir)
  expected_file=os.path.join(test_dir,'test_and_module_info.pkl')
  assert os.path.isfile(expected_file)

  args=["search_text=%s" %("ANY"),
     "phenix_test_database_file=%s" %(expected_file),
     "search_tests_by=function_called",
     "search_type=tests",
     ]
  try:
    from iotbx.cli_parser import run_program
    from phenix.programs import find_program as run
  except Exception as e:
    print("Find_program not available...skipping")
    return

  results=run_program(program_class=run.Program,args=args)
  print("Results for find_program after get_test_and_module_info:")
  for key in results.programs.keys():
    print(results.programs[key])
  for key in results.tests.keys():
    print(results.tests[key])

  # make sure it found something
  assert results.tests_summary_text.find("MODULE")>-1

  print("OK")

if __name__=="__main__":
  t0 = time.time()
  args="max_tests=1 tests_to_skip=tst_scheduling,Auto,build,Ligand,autosol,ligand max_tests_per_module=1".split()

  tst_01(args)
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
