from __future__ import division
from __future__ import print_function
import sys,os
import libtbx.load_env
from libtbx.test_utils import approx_equal

import time,sys

def tst_01(text):
  print("testing find_program with %s" %(text))
  args=["search_text=%s" %(text),]
  try:
    from iotbx.cli_parser import run_program
    from phenix.programs import find_program as run
  except Exception as e:
    print("Find_program not available...skipping")
    return

  results=run_program(program_class=run.Program,args=args)
  print("Results for %s" %(text))
  for key in results.programs.keys():
    print(results.programs[key])
  for key in results.tests.keys():
    print(results.tests[key])

  # Quick check...if called with a program name should get it back
  found=False
  for key in results.programs.keys():
    entry=results.programs[key]
    if entry.program_name=="phenix.%s" %text:
       found=True
  assert found

  print("OK")

if __name__=="__main__":
  t0 = time.time()
  tst_01("trace_and_build")
  tst_01("autosol")
  tst_01("map_to_model")
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
