from __future__ import print_function
from cctbx.array_family import flex
from mmtbx import utils
from six.moves import cStringIO as StringIO
import mmtbx.dynamics.cartesian_dynamics as cartesian_dynamics
import mmtbx.restraints
import time, math, os
from libtbx.test_utils import approx_equal
import libtbx.load_env

pdb_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="pdb",
  test=os.path.isdir)

def run(pdb_file):
  # read in and process PDB file
  processed_pdb_files_srv = utils.process_pdb_file_srv(log = StringIO())
  processed_pdb_file, pdb_inp = \
    processed_pdb_files_srv.process_pdb_files(pdb_file_names = [pdb_file])
  xray_structure = processed_pdb_file.xray_structure(show_summary = False)
  assert xray_structure is not None
  # set up geometry restraints manager (energy term)
  sctr_keys = xray_structure.scattering_type_registry().type_count_dict().keys()
  has_hd = "H" in sctr_keys or "D" in sctr_keys
  geometry = processed_pdb_file.geometry_restraints_manager(
    show_energies                = False,
    assume_hydrogens_all_missing = not has_hd)
  restraints_manager = mmtbx.restraints.manager(
    geometry      = geometry,
    normalization = True)
  # do some MD simulation
  xray_structure_start = xray_structure.deep_copy_scatterers() # since code below modifies it in-place
  gradients_calculator=cartesian_dynamics.gradients_calculator_reciprocal_space(
      restraints_manager = restraints_manager,
      sites_cart         = xray_structure.sites_cart(),
      wc                 = 1)
  cartesian_dynamics.run(
    gradients_calculator             = gradients_calculator,
    xray_structure                   = xray_structure,
    temperature                      = 300,
    n_steps                          = 1000,
    time_step                        = 0.0005,
    initial_velocities_zero_fraction = 0,
    n_print                          = 100,
    stop_cm_motion                   = True,
    log                              = None,
    verbose                          = -1)
  # see if something happened after MD
  result = xray_structure_start.distances(other =
    xray_structure).min_max_mean().as_tuple()
  cm1 = xray_structure_start.center_of_mass()
  cm2 = xray_structure.center_of_mass()
  dist = math.sqrt( (cm1[0]-cm2[0])**2 + (cm1[1]-cm2[1])**2 + (cm1[2]-cm2[2])**2 )
  if 0:
    print("\nShift w.r.t. original: min=%-8.3f max=%-8.3f mean=%-8.3f\n"%result)
    print("CM start: %8.3f %8.3f %8.3f"%cm1)
    print("CM final: %8.3f %8.3f %8.3f"%cm2)
    print("CM (start-final distance): %8.3f"%dist)
    print()
  assert approx_equal(dist, 0.0, 0.01)
  if 0:
    # write out modified structure
    output_file_object = open("md_simulated.pdb", "w")
    utils.write_pdb_file(
      xray_structure = xray_structure,
      pdb_hierarchy  = processed_pdb_file.all_chain_proxies.pdb_hierarchy,
      out            = output_file_object)

if (__name__ == "__main__"):
  t0 = time.time()
  pdbin = os.path.join(pdb_dir, "enk.pdb")
  for i in range(10):
    run(pdb_file = pdbin)
  print("Time: %8.2f"%(time.time() - t0))
