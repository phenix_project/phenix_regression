'''#! /bin/csh -fe

if (! $?COD_SVN_WORKING_COPY) exit 0 # no "skipping" message

set PHENIX_REGRESSION_DIR="`libtbx.find_in_repositories phenix_regression`"
set omz="`libtbx.show_dist_paths cctbx`/omz"

set optimizers=dev+ls_simple+ls_lm
if ("`libtbx.show_full_command_path shelxl`" !~ "None") then
  set optimizers=$optimizers+shelxl_fm+shelxl_cg
endif
if ("`libtbx.show_full_command_path shelx76`" !~ "None") then
  set optimizers=$optimizers+shelx76
endif
cctbx.python "$omz/cod_refine.py" tmp_ma_xs optimizers=$optimizers | libtbx.assert_stdin_contains_strings 'Number of exceptions caught: 0'
cctbx.python "$omz/cod_refine.py" tmp_ma_xs optimizers=$optimizers target.obs_type=I | libtbx.assert_stdin_contains_strings 'Number of exceptions caught: 0'
'''

import os, sys
import libtbx.load_env
from libtbx import easy_run
from libtbx.test_utils import contains_lines

def main():
  if not os.environ.get('COD_SVN_WORKING_COPY', False): return 0

  omz_dir = libtbx.env.find_in_repositories(relative_path=os.path.join('cctbx', 'omz'))
  cmd = 'iotbx.python %s %s %s' % (os.path.join(omz_dir, 'cif_refine.py'),
                                os.path.join(os.environ.get('COD_SVN_WORKING_COPY', False),
                                             'hkl','2','10','36','2103614.hkl'),
                                os.path.join(os.environ.get('COD_SVN_WORKING_COPY', False),
                                             'cif','2','10','36','2103614.cif'),
                                )
  print('\n ~> %s\n' % cmd)
  rc = easy_run.go(cmd)
  assert rc.return_code==0
  test = 'Number of variables: 3'
  print('testing %s' % test)
  assert contains_lines('\n'.join(rc.stdout_lines), test)
  cmd += ' reset_u_iso=0.05'
  print('\n ~> %s\n' % cmd)
  rc = easy_run.go(cmd)
  assert rc.return_code==0
  test = 'Number of variables: 3'
  print('testing %s' % test)
  assert contains_lines('\n'.join(rc.stdout_lines), test)

  cmd = 'cctbx.cod_select_and_pickle 2103614 --pickle_dir=tmp_ma_xs'
  print('\n ~> %s\n' % cmd)
  rc = easy_run.go(cmd)
  assert rc.return_code==0
  test = 'Number of exceptions caught: 0'
  print('testing %s' % test)
  assert contains_lines('\n'.join(rc.stdout_lines), test)

  cmd = 'cctbx.python %s tmp_ma_xs optimizers=dev target.type=cc' % (
    os.path.join(omz_dir, 'cod_refine.py'))
  print('\n ~> %s\n' % cmd)
  rc = easy_run.go(cmd)
  assert rc.return_code==0
  test = 'Number of exceptions caught: 0'
  print('testing %s' % test)
  assert contains_lines('\n'.join(rc.stdout_lines), test)
  cmd = 'cctbx.python %s tmp_ma_xs optimizers=dev target.type=r1 use_line_search=True' % (
    os.path.join(omz_dir, 'cod_refine.py'))
  print('\n ~> %s\n' % cmd)
  rc = easy_run.go(cmd)
  assert rc.return_code==0
  test = 'Number of exceptions caught: 0'
  print('testing %s' % test)
  assert contains_lines('\n'.join(rc.stdout_lines), test)

if __name__ == '__main__':
  main()
