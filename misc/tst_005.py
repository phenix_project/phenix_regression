from __future__ import print_function
from libtbx.test_utils import run_command, approx_equal
import libtbx.load_env
import time, sys, os
from phenix_regression.refinement import get_r_factors

def exercise_02(verbose):
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/pdb1akg_very_well_phenix_refined_001.pdb",
    test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/1akg.mtz",
    test=os.path.isfile)
  cmd = " ".join([
    'phenix.remove_outliers',
    'data.file=%s'%hkl,
    'model.file=%s'%pdb,
    'output.logfile=tst_outliers_rejection_exercise_02.log > tst_outliers_rejection_exercise_02_stdout'])
  run_command(
    command=cmd,
    log_file_name="tst_outliers_rejection_exercise_02.log",
    stdout_file_name="tst_outliers_rejection_exercise_02_stdout",
    show_diff_log_stdout=False,
    verbose=verbose)


if (__name__ == "__main__"):
  verbose = "--verbose" in sys.argv[1:]
  t0 = time.time()
  exercise_02(verbose=verbose)
  print("OK Time: %8.3f"%(time.time()-t0))
