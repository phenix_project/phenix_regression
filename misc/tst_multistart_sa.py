
from __future__ import division
from __future__ import print_function
from libtbx import easy_run
import os.path as op

def exercise () :
  pdb_in = """\
CRYST1   24.225   23.655   29.176  90.00  90.00  90.00 P 1
ATOM         N   TRP A   1      12.996  11.322  12.366  1.00 20.00           N
ATOM         CA  TRP A   1      11.680  10.883  12.812  1.00 20.00           C
ATOM         C   TRP A   1      10.782  10.571  11.620  1.00 20.00           C
ATOM         O   TRP A   1      10.933  11.155  10.547  1.00 20.00           O
ATOM         CB  TRP A   1      11.029  11.956  13.687  1.00 20.00           C
ATOM         CG  TRP A   1      11.819  12.285  14.916  1.00 20.00           C
ATOM         CD1 TRP A   1      12.786  13.240  15.039  1.00 20.00           C
ATOM         CD2 TRP A   1      11.709  11.659  16.201  1.00 20.00           C
ATOM         NE1 TRP A   1      13.285  13.247  16.319  1.00 20.00           N
ATOM         CE2 TRP A   1      12.640  12.286  17.052  1.00 20.00           C
ATOM         CE3 TRP A   1      10.913  10.629  16.713  1.00 20.00           C
ATOM         CZ2 TRP A   1      12.798  11.918  18.386  1.00 20.00           C
ATOM         CZ3 TRP A   1      11.071  10.265  18.038  1.00 20.00           C
ATOM         CH2 TRP A   1      12.006  10.908  18.860  1.00 20.00           C
TER
END
"""
  pdb_file = "tst_multistart_sa_in.pdb"
  open(pdb_file, "w").write(pdb_in)
  mtz_file = "tst_multistart_sa_in.mtz"
  args = [
    "phenix.fmodel",
    pdb_file,
    "high_resolution=3.0",
    "type=real",
    "label=FMODEL",
    "r_free_flags_fraction=0.1",
    "random_seed=12345",
    "output.file_name=%s" % mtz_file,
  ]
  rc = easy_run.fully_buffered(" ".join(args)).raise_if_errors().return_code
  assert (rc == 0)
  args2 = [
    "phenix.multistart_sa",
    pdb_file,
    mtz_file,
    "nproc=1",
    "temperature=500",
    "n_jobs=2",
  ] 
  result = easy_run.fully_buffered(" ".join(args2)).raise_if_errors()
  assert (result.return_code == 0), "\n".join(result.stdout_lines)
  assert op.isfile("SA_combined.pdb")
  assert op.isfile("SA_combined_maps.mtz")

if (__name__ == "__main__") :
  exercise()
  print("OK")
