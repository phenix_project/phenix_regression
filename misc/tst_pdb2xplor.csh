#! /bin/csh -f

set PHENIX_REGRESSION_DIR="`libtbx.find_in_repositories phenix_regression`"

rm -rf tst_pdb2xplor
mkdir tst_pdb2xplor
cd tst_pdb2xplor
cp "$PHENIX_REGRESSION_DIR"/misc/1CL7I.pdb .
phenix.python "`libtbx.show_dist_paths phenix`"/phenix/pdb_tools/pdb2xplor.py .
if (! -f 1CL7I.xplor) then
  echo "ERROR: tst_pdb2xplor"
endif
cd ..

echo "OK"
