import os
from libtbx import easy_run
import libtbx.load_env
from libtbx.test_utils import approx_equal

def run1():
  repository_dir = libtbx.env.dist_path("phenix_regression")
  cmd = 'phenix.hyss'
  cmd += ' %s 2 se' % (os.path.join(repository_dir, 'reflection_files', 'gere_MAD.mtz'))
  cmd += ' --molecular_weight=8000 --solvent_content=0.70 --random_seed=1'
  print('\n ~> %s\n' % cmd)
  rc = easy_run.go(cmd)
  assert rc.return_code==0
  for line in rc.stdout_lines:
    if line.find('Correlation coefficient for consensus model (12 sites, after truncation):')>-1:
      v=line.split()[-1]
      assert approx_equal(float(v),0.4, 0.05)

def run2():
  repository_dir = libtbx.env.dist_path("phenix")

  cmd = 'iotbx.reflection_file_reader'
  cmd += ' gere_MAD_anom_diffs.hkl=amplitudes'
  cmd += ' --symmetry=gere_MAD_hyss_consensus_model.pdb'
  cmd += ' --pickle=gere_MAD_anom_diffs.pickle'
  print('\n ~> %s\n' % cmd)
  rc = easy_run.go(cmd)
  # assert rc.return_code==0
  for line in rc.stdout_lines:
    print(line)
    if line.find('Writing all Miller arrays to file: gere_MAD_anom_diffs.pickle')>-1:
      break
  else:
    assert 0

  cmd = 'phenix.python'
  cmd += ' %s' % (os.path.join(repository_dir, 'phenix', 'heavy_search', 'pc_multi.py'))
  cmd += ' gere_MAD_anom_diffs.pickle gere_MAD_hyss_consensus_model.pickle'
  print('\n ~> %s\n' % cmd)
  rc = easy_run.go(cmd)
  assert rc.return_code==0
  for line in rc.stdout_lines:
    if line.find('Correlation coefficient for consensus model (12 sites, after truncation):')>-1:
      v=line.split()[-1]
      assert approx_equal(float(v),0.4, 0.05)

def run3():
  repository_dir = libtbx.env.dist_path("phenix_regression")
  cmd = 'iotbx.pdb.as_xray_structure --symmetry=gere_MAD_anom_diffs.ins'
  cmd += ' %s --pickle=12_se --fake_f_obs_and_r_free_flags_d_min=12' % (
    os.path.join(repository_dir, 'pdb', '12_se.pdb'))
  rc = easy_run.go(cmd)
  assert rc.return_code==0
  for line in rc.stdout_lines:
    if line.find('Writing all xray structures to file: 12_se.pickle')>-1:
      break
  else:
    assert 0

  cmd = 'cctbx.euclidean_model_matching 12_se.pickle gere_MAD_hyss_consensus_model.pickle'
  print('\n ~> %s\n' % cmd)
  rc = easy_run.go(cmd)
  assert rc.return_code==0
  for line in rc.stdout_lines:
    print(line)
    if line.find('match_list:')>-1:
      print(line)
      v = line.split()[-1]
      v = v.replace('[','')
      v = v.replace(']','')
      v,w = v.split('/')
      assert v in ['11','12']
      assert w in ['11','12']

def main():
  run1()
  run2()
  run3()
  print("OK")

if __name__ == '__main__':
  main()
