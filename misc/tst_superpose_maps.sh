#!/bin/sh

DATA_DIR=`libtbx.find_in_repositories  phenix_regression/validation`
tar xzf $DATA_DIR/pka_compare.tar.gz
phenix.superpose_maps 3fhi.pdb 3fjq.pdb 3fhi_map_coeffs.mtz \
  3fjq_map_coeffs.mtz \
  labels_1="2FOFCWT,PHI2FOFCWT" labels_2="2FOFCWT,PHI2FOFCWT" | \
  libtbx.assert_stdin_contains_strings "Wrote 3fjq_map_coeffs_superposed.ccp4"
echo "OK"
