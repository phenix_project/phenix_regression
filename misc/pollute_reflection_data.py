from __future__ import print_function
from cctbx import sgtbx
from cctbx.array_family import flex
from libtbx import easy_pickle
import sys

def pollute(miller_array):
  indices = miller_array.indices().deep_copy()
  data = miller_array.data().deep_copy()
  if (miller_array.sigmas() is None):
    sigmas = None
  else:
    sigmas = miller_array.sigmas().deep_copy()
  complete_set = miller_array \
    .reflection_intensity_symmetry() \
    .complete_set() \
    .customized_copy(crystal_symmetry=miller_array)
  sel_integral = complete_set.sys_absent_flags(integral_only=True).data()
  indices.extend(
    complete_set.indices().select(sel_integral.iselection()[:100]))
  sel_intrinsic = complete_set.sys_absent_flags().data() & ~sel_integral
  indices.extend(
    complete_set.indices().select(sel_intrinsic.iselection()[:100]))
  while (data.size() < indices.size()):
    data.extend(data)
  data = data[:indices.size()]
  if (sigmas is not None):
    while (sigmas.size() < indices.size()):
      sigmas.extend(sigmas)
    sigmas = sigmas[:indices.size()]
  work = miller_array.customized_copy(
    indices=indices,
    data=data,
    sigmas=sigmas)
  work = work.expand_to_p1().select(
    flex.random_permutation(size=work.indices().size())[:100])
  indices.extend(work.indices())
  data.extend(work.data())
  if (sigmas is not None):
    sigmas.extend(work.sigmas())
  return miller_array.customized_copy(
    indices=indices,
    data=data,
    sigmas=sigmas) \
      .set_info(miller_array.info()) \
      .set_observation_type(miller_array)

def run(args):
  polluted_arrays = []
  for arg in args:
    for miller_array in easy_pickle.load(arg):
      polluted_array = pollute(miller_array=miller_array)
      polluted_arrays.append(polluted_array)
  print("Writing: polluted_miller_arrays.pickle")
  easy_pickle.dump("polluted_miller_arrays.pickle", polluted_arrays)

if (__name__ == "__main__"):
  run(sys.argv[1:])
