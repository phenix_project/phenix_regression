from __future__ import print_function
from six.moves import cStringIO as StringIO
import sys

class hello:

  def show(self, f=None):
    if (f is None): f = sys.stdout
    print("Hello, world.", file=f)

def exercise_something(verbose=0):
  if (verbose): f = sys.stdout
  else: f = StringIO()
  hello().show(f=f)

def exercise(args):
  verbose = "--verbose" in args
  exercise_something(verbose=verbose)
  print("OK")

if (__name__ == "__main__"):
  exercise(sys.argv[1:])
