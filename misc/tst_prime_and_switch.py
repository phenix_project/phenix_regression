
from __future__ import division
from __future__ import print_function
from libtbx import easy_run
import libtbx.load_env
import os.path

def exercise () :
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/1yjp_h.pdb",
    test=os.path.isfile)
  mtz_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/1yjp.mtz",
    test=os.path.isfile)
  cmd = [
    "phenix.prime_and_switch_map",
    pdb_file,
    mtz_file,
    "output_file=tst_prime_and_switch.mtz",
  ]
  result = easy_run.fully_buffered(" ".join(cmd)).raise_if_errors()
  assert os.path.isfile("tst_prime_and_switch.mtz")

if (__name__ == "__main__") :
  exercise()
  print("OK")
