from __future__ import print_function
from libtbx import easy_run
import sys, os
op = os.path

def run(args):
  if (len(args) == 0):
    selected_resnames = None
  else:
    selected_resnames = set(args)
  for node in sorted(os.listdir(".")):
    if (not node.endswith(".pdb")): continue
    if (node.endswith(".pdb_modified.pdb")): continue
    input_pdb = node
    resname = input_pdb[:3]
    if (resname == "MSE"):
      continue
    if (selected_resnames is not None and resname not in selected_resnames):
      continue
    rotamer_id_inp = input_pdb[4:-4]
    output_pdb = input_pdb + "_modified.pdb"
    if (op.exists(output_pdb)):
      os.remove(output_pdb)
    assert not op.exists(output_pdb)
    cmd = " ".join([
      "phenix.pdbtools",
      "--geometry-regularization",
      "geometry_minimization.macro_cycles=5",
      input_pdb,
      ">",
      input_pdb[:-4]+".geo_log"])
    easy_run.call(command=cmd)
    assert op.isfile(output_pdb)
    cmd = "phenix.rotalyze %s" % output_pdb
    rotalyze_output = easy_run.fully_buffered(
      command=cmd).raise_if_errors().stdout_lines
    output_iter = iter(rotalyze_output)
    for line in output_iter:
      if (line.startswith("residue:score%:")):
        rotamer_id_out = output_iter.next().split(":")[-1]
        if (resname == "PRO" and rotamer_id_out in ["Cg_endo", "Cg_exo"]):
          rotamer_id_out = rotamer_id_out[3:]
        break
    else:
      print("\n".join(rotalyze_output))
      raise RuntimeError("expected line not found in phenix.rotalyze output.")
    if (rotamer_id_out != rotamer_id_inp):
      annotation = "MISMATCH"
    else:
      annotation = "inp=out"
    print(resname, rotamer_id_inp, rotamer_id_out, annotation)
    sys.stdout.flush()

if (__name__ == "__main__"):
  run(args=sys.argv[1:])
