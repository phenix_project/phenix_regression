#! /bin/csh -f

set PHENIX_REGRESSION_DIR="`libtbx.find_in_repositories phenix_regression`"

if (-e $HOME/focus-etc/cshrc) then
  source $HOME/focus-etc/cshrc
  iotbx.python $HOME/focus-etc/cs_tools/strudat_as_fis.py "$PHENIX_REGRESSION_DIR"/misc/strudat_zeolite_atlas --coseq "$COSEQ_DB" > tmp_strudat.log
  grep "Unknown" tmp_strudat.log | libtbx.assert_line_count 0
  tail -1 tmp_strudat.log | libtbx.assert_stdin "Number of errors: 0"
endif

echo "OK"
