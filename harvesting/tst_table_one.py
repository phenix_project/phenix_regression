
from __future__ import division
from __future__ import print_function
from mmtbx.command_line import table_one
from libtbx import easy_pickle
import libtbx.load_env
from six.moves import cStringIO as StringIO
import os
import sys

def exercise_01 (extra_args=()) :
  base_dir = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/harvesting")
  mtz_file = os.path.join(base_dir, "lores.mtz")
  unmerged_file = os.path.join(base_dir, "unmerged.sca")
  pdb_file = os.path.join(base_dir, "model.pdb")
  f = open("1aho_table1.eff", "w")
  f.write("""
table_one {
  output.base_name = 1aho_table1
  output.format = *txt *csv *rtf
  structure {
    name = 1aho_fake
    pdb_file = %s
    mtz_file = %s
    wavelength = 0.8
    unmerged_data = %s
  }
}""" % (pdb_file, mtz_file, unmerged_file))
  f.close()
  args = ["1aho_table1.eff"] #+ list(extra_args)
  table1 = table_one.run(
    args=args,
    out=StringIO(),
    use_current_directory_if_not_specified=True)
  pkl = easy_pickle.dumps(table1)
  lines = open("1aho_table1.txt")
  for line in lines :
    if ("R-merge" in line) :
      fields = line.strip().split()
      assert (len(fields) == 3)
    elif ("Clashscore" in line) :
      fields = line.strip().split()
      assert (fields[1] == "15.62")

if (__name__ == "__main__") :
  exercise_01(sys.argv[1:])
  print("OK")
