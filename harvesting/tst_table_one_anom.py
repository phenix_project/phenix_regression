
from __future__ import division
from __future__ import print_function
from mmtbx.command_line import table_one
import iotbx.mtz
from libtbx import easy_pickle
import libtbx.load_env
from six.moves import cStringIO as StringIO
import os
import sys

def exercise_02 () :
  base_dir = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/harvesting")
  mtz_file = os.path.join(base_dir, "lores.mtz")
  ma = iotbx.mtz.object(mtz_file).as_miller_arrays()
  fobs = ma[0].generate_bijvoet_mates()
  flags = ma[1]
  mtz_out = fobs.as_mtz_dataset(column_root_label="F")
  mtz_out.add_miller_array(flags, column_root_label="FreeR_flag")
  mtz_out.mtz_object().write("tmp_table1_data.mtz")
  mtz_file = "tmp_table1_data.mtz"
  unmerged_file = os.path.join(base_dir, "unmerged.sca")
  pdb_file = os.path.join(base_dir, "model.pdb")
  for anom in [False, True] :
    eff_file = "1aho_table1_anom_%s.eff" % int(anom)
    f = open(eff_file, "w")
    f.write("""
table_one {
  processing.count_anomalous_pairs_separately = %s
  output.base_name = 1aho_table1_anom%s
  structure {
    name = 1aho_fake
    pdb_file = %s
    mtz_file = %s
    wavelength = 0.8
    unmerged_data = %s
  }
}""" % (anom, int(anom), pdb_file, mtz_file, unmerged_file))
    f.close()
    args = [ eff_file ] #+ list(extra_args)
    table1 = table_one.run(
      args=args,
      out=StringIO(),
      use_current_directory_if_not_specified=True)
    pkl = easy_pickle.dumps(table1)
    # No text file output anymore but numbers below may be useful!
    """
    txt_file = "1aho_table1_anom%s.txt" % int(anom)
    print txt_file
    lines = open(txt_file).read().splitlines()
    for line in lines :
      if ("Unique reflections" in line) :
        if (anom) :
          assert ("7370 (724)" in line)
        else :
          assert ("4111 (393)" in line)
      elif ("Mean I/sigma(I)" in line) :
        if (anom) :
          assert ("7.12 (0.70)" in line)
        else :
          assert ("9.59 (0.96)" in line)
    """

if (__name__ == "__main__") :
  exercise_02()
  print("OK")
