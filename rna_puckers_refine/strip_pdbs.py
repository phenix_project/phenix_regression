from __future__ import print_function
import iotbx.pdb
from libtbx import dict_with_default_0
import sys, os
op = os.path

def run(args):
  for file_name in args:
    pdb_inp = iotbx.pdb.input(file_name=file_name)
    pdb_hierarchy = pdb_inp.construct_hierarchy()
    model = pdb_hierarchy.only_model()
    for chain in model.chains():
      for residue_group in chain.residue_groups():
        for atom_group in residue_group.atom_groups():
          if (atom_group.resname != "HOH"):
            break
        else:
          chain.remove_residue_group(residue_group)
      if (chain.residue_groups_size() == 0):
        model.remove_chain(chain)
    for atom in pdb_hierarchy.atoms():
      if (atom.charge == " +"):
        atom.charge = "  "
    rna_resnames = set(["A", "C", "G", "U"])
    for chain in model.chains():
      chain_resname_counts = dict_with_default_0()
      for residue_group in chain.residue_groups():
        for atom_group in residue_group.atom_groups():
          chain_resname_counts[atom_group.resname.strip()] += 1
      chain_resnames = set(chain_resname_counts.keys())
      if (    len(rna_resnames.intersection(chain_resnames)) != 0
          and not rna_resnames.issuperset(chain_resnames)):
        print(file_name, chain_resname_counts)
    if (0):
      pdb_hierarchy.write_pdb_file(
        file_name=op.basename(file_name),
        crystal_symmetry=pdb_inp.crystal_symmetry(),
        append_end=True)

if (__name__ == "__main__"):
  run(args=sys.argv[1:])
