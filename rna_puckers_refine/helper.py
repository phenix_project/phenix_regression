from __future__ import print_function
import libtbx.load_env
import sys, os
op = os.path

def run(args):
  if (len(args) == 0):
    mode = "refine"
  elif (args[0] in ["refine", "pdb_interpretation"]):
    mode = args[0]
    args = args[1:]
  target_codes = set(args)
  data_dir = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/rna_puckers_refine",
    optional=False)
  files_by_code = {}
  for file_name in sorted(os.listdir(data_dir)):
    if (file_name[-4:] not in [".pdb", ".mtz", ".cif"]): continue
    code = file_name.split(".")[0]
    files_by_code.setdefault(code, []).append(file_name)
  for code,file_names in files_by_code.items():
    if (len(target_codes) != 0 and code not in target_codes): continue
    if (mode == "refine"):
      cmd = ["phenix.refine", "--overwrite"]
      cmd.extend([op.join(data_dir, file_name) for file_name in file_names])
      cmd.extend(["output.write_maps=False", "--dry-run"])
    elif (mode == "pdb_interpretation"):
      cmd = [
        "phenix.pdb_interpretation",
        "build_geometry_restraints_manager=False",
        "build_xray_structure=False"]
      for file_name in file_names:
        if (file_name.endswith(".mtz")): continue
        cmd.append(op.join(data_dir, file_name))
    else:
      raise RuntimeError
    print(" ".join(cmd))

if (__name__ == "__main__"):
  run(args=sys.argv[1:])
