from __future__ import print_function
from libtbx import easy_run
import time
import iotbx.pdb
from scitbx.array_family import flex
import os
from libtbx.test_utils import approx_equal

pdb_str = """\
CRYST1   30.410   14.706   21.507  90.00  90.00  90.00 P 1
SCALE1      0.032884  0.000000  0.000000        0.00000
SCALE2      0.000000  0.067999  0.000000        0.00000
SCALE3      0.000000  0.000000  0.046496        0.00000
ATOM      1  N   GLY A   1       5.043   9.706  12.193  1.00 16.77           N
ATOM      2  CA  GLY A   1       5.000   9.301  10.742  1.00 16.57           C
ATOM      3  C   GLY A   1       6.037   8.234  10.510  1.00 16.16           C
ATOM      4  O   GLY A   1       6.529   7.615  11.472  1.00 16.78           O
ATOM      5  N   ASN A   2       6.396   8.017   9.246  1.00 15.02           N
ATOM      6  CA  ASN A   2       7.530   7.132   8.922  1.00 14.10           C
ATOM      7  C   ASN A   2       8.811   7.631   9.518  1.00 13.13           C
ATOM      8  O   ASN A   2       9.074   8.836   9.517  1.00 11.91           O
ATOM      9  CB  ASN A   2       7.706   6.975   7.432  1.00 15.38           C
ATOM     10  CG  ASN A   2       6.468   6.436   6.783  1.00 14.08           C
ATOM     11  OD1 ASN A   2       6.027   5.321   7.107  1.00 17.46           O
ATOM     12  ND2 ASN A   2       5.848   7.249   5.922  1.00 11.72           N
ATOM     13  N   ASN A   3       9.614   6.684   9.996  1.00 12.26           N
ATOM     14  CA  ASN A   3      10.859   6.998  10.680  1.00 11.74           C
ATOM     15  C   ASN A   3      12.097   6.426   9.986  1.00 11.10           C
ATOM     16  O   ASN A   3      12.180   5.213   9.739  1.00 10.42           O
ATOM     17  CB  ASN A   3      10.793   6.472  12.133  1.00 12.15           C
ATOM     18  CG  ASN A   3      12.046   6.833  12.952  1.00 12.82           C
ATOM     19  OD1 ASN A   3      12.350   8.019  13.163  1.00 15.05           O
ATOM     20  ND2 ASN A   3      12.781   5.809  13.397  1.00 13.48           N
ATOM     21  N   GLN A   4      13.047   7.322   9.689  1.00 10.29           N
ATOM     22  CA  GLN A   4      14.436   6.982   9.290  1.00 10.53           C
ATOM     23  C   GLN A   4      15.487   7.700  10.179  1.00 10.24           C
ATOM     24  O   GLN A   4      15.599   8.937  10.206  1.00  8.86           O
ATOM     25  CB  GLN A   4      14.708   7.242   7.802  1.00  9.80           C
ATOM     26  CG  GLN A   4      15.996   6.552   7.304  1.00 10.25           C
ATOM     27  CD  GLN A   4      16.556   7.138   6.002  1.00 12.43           C
ATOM     28  OE1 GLN A   4      16.796   8.362   5.901  1.00 14.62           O
ATOM     29  NE2 GLN A   4      16.802   6.255   5.000  1.00  9.05           N
ATOM     30  N   GLN A   5      16.206   6.915  10.962  1.00 10.38           N
ATOM     31  CA  GLN A   5      17.322   7.455  11.731  1.00 11.39           C
ATOM     32  C   GLN A   5      18.646   6.862  11.263  1.00 11.52           C
ATOM     33  O   GLN A   5      18.820   5.640  11.145  1.00 12.05           O
ATOM     34  CB  GLN A   5      17.108   7.277  13.238  1.00 11.96           C
ATOM     35  CG  GLN A   5      15.881   8.044  13.738  1.00 10.81           C
ATOM     36  CD  GLN A   5      15.396   7.508  15.045  1.00 13.10           C
ATOM     37  OE1 GLN A   5      14.826   6.419  15.093  1.00 10.65           O
ATOM     38  NE2 GLN A   5      15.601   8.281  16.130  1.00 12.30           N
ATOM     39  N   ASN A   6      19.566   7.758  10.947  1.00 11.99           N
ATOM     40  CA  ASN A   6      20.883   7.404  10.409  1.00 12.30           C
ATOM     41  C   ASN A   6      21.906   7.855  11.415  1.00 13.40           C
ATOM     42  O   ASN A   6      22.271   9.037  11.465  1.00 13.92           O
ATOM     43  CB  ASN A   6      21.117   8.110   9.084  1.00 12.13           C
ATOM     44  CG  ASN A   6      20.013   7.829   8.094  1.00 12.77           C
ATOM     45  OD1 ASN A   6      19.850   6.698   7.642  1.00 14.27           O
ATOM     46  ND2 ASN A   6      19.247   8.841   7.770  1.00 10.07           N
ATOM     47  N   TYR A   7      22.344   6.911  12.238  1.00 14.70           N
ATOM     48  CA  TYR A   7      23.211   7.238  13.390  1.00 15.18           C
ATOM     49  C   TYR A   7      24.655   7.425  12.976  1.00 15.91           C
ATOM     50  O   TYR A   7      25.093   6.905  11.946  1.00 15.76           O
ATOM     51  CB  TYR A   7      23.113   6.159  14.460  1.00 15.35           C
ATOM     52  CG  TYR A   7      21.717   6.023  14.993  1.00 14.45           C
ATOM     53  CD1 TYR A   7      20.823   5.115  14.418  1.00 15.68           C
ATOM     54  CD2 TYR A   7      21.262   6.850  16.011  1.00 14.80           C
ATOM     55  CE1 TYR A   7      19.532   5.000  14.887  1.00 13.46           C
ATOM     56  CE2 TYR A   7      19.956   6.743  16.507  1.00 14.33           C
ATOM     57  CZ  TYR A   7      19.099   5.823  15.922  1.00 15.09           C
ATOM     58  OH  TYR A   7      17.818   5.683  16.382  1.00 14.39           O
ATOM     59  OXT TYR A   7      25.410   8.093  13.703  1.00 17.49           O
TER
END
"""

def run(prefix=os.path.basename(__file__).replace(".py","_douse")):
  """
  Exercise phenix.douse in case there no new nor existing water.
  """
  pdb_file = "%s.pdb"%prefix
  with open(pdb_file,"w") as fo:
    fo.write(pdb_str)
  #
  cmd = "phenix.model_map %s output_file_name_prefix=%s > %s.map.zlog"%(
    pdb_file, prefix, prefix)
  assert easy_run.call(cmd)==0
  #
  for i, mode in enumerate(["whole","per_chain"]):
    cmd = " ".join([
      "phenix.douse",
      "%s"%pdb_file,
      "%s.ccp4"%prefix,
      "resolution=2",
      "--overwrite",
      ">%s_%d.zlog"%(prefix,i)])
    print(cmd)
    assert easy_run.call(cmd)==0

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
