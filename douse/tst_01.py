from __future__ import print_function
from libtbx import easy_run
import time
import iotbx.pdb
from scitbx.array_family import flex
import os
from libtbx.test_utils import approx_equal

pdb_str_answer = """\
CRYST1   38.448   20.321   26.625  90.00  90.00  90.00 P 1
SCALE1      0.026009  0.000000  0.000000        0.00000
SCALE2      0.000000  0.049210  0.000000        0.00000
SCALE3      0.000000  0.000000  0.037559        0.00000
ATOM      1  N   GLY A   1       6.293   9.706  12.193  1.00 10.00           N
ATOM      2  CA  GLY A   1       6.250   9.301  10.742  1.00 10.00           C
ATOM      3  C   GLY A   1       7.287   8.234  10.510  1.00 10.00           C
ATOM      4  O   GLY A   1       7.779   7.615  11.472  1.00 10.00           O
ATOM      5  N   ASN A   2       7.646   8.017   9.246  1.00 10.00           N
ATOM      6  CA  ASN A   2       8.780   7.132   8.922  1.00 10.00           C
ATOM      7  C   ASN A   2      10.061   7.631   9.518  1.00 10.00           C
ATOM      8  O   ASN A   2      10.324   8.836   9.517  1.00 10.00           O
ATOM      9  CB  ASN A   2       8.956   6.975   7.432  1.00 10.00           C
ATOM     10  CG  ASN A   2       7.718   6.436   6.783  1.00 10.00           C
ATOM     11  OD1 ASN A   2       7.277   5.321   7.107  1.00 10.00           O
ATOM     12  ND2 ASN A   2       7.098   7.249   5.922  1.00 10.00           N
ATOM     13  N   ASN A   3      10.864   6.684   9.996  1.00 10.00           N
ATOM     14  CA  ASN A   3      12.109   6.998  10.680  1.00 10.00           C
ATOM     15  C   ASN A   3      13.347   6.426   9.986  1.00 10.00           C
ATOM     16  O   ASN A   3      13.430   5.213   9.739  1.00 10.00           O
ATOM     17  CB  ASN A   3      12.043   6.472  12.133  1.00 10.00           C
ATOM     18  CG  ASN A   3      13.296   6.833  12.952  1.00 10.00           C
ATOM     19  OD1 ASN A   3      13.600   8.019  13.163  1.00 10.00           O
ATOM     20  ND2 ASN A   3      14.031   5.809  13.397  1.00 10.00           N
ATOM     21  N   GLN A   4      14.297   7.322   9.689  1.00 10.00           N
ATOM     22  CA  GLN A   4      15.686   6.982   9.290  1.00 10.00           C
ATOM     23  C   GLN A   4      16.737   7.700  10.179  1.00 10.00           C
ATOM     24  O   GLN A   4      16.849   8.937  10.206  1.00 10.00           O
ATOM     25  CB  GLN A   4      15.958   7.242   7.802  1.00 10.00           C
ATOM     26  CG  GLN A   4      17.246   6.552   7.304  1.00 10.00           C
ATOM     27  CD  GLN A   4      17.806   7.138   6.002  1.00 10.00           C
ATOM     28  OE1 GLN A   4      18.046   8.362   5.901  1.00 10.00           O
ATOM     29  NE2 GLN A   4      18.052   6.255   5.000  1.00 10.00           N
ATOM     30  N   GLN A   5      17.456   6.915  10.962  1.00 10.00           N
ATOM     31  CA  GLN A   5      18.572   7.455  11.731  1.00 10.00           C
ATOM     32  C   GLN A   5      19.896   6.862  11.263  1.00 10.00           C
ATOM     33  O   GLN A   5      20.070   5.640  11.145  1.00 10.00           O
ATOM     34  CB  GLN A   5      18.358   7.277  13.238  1.00 10.00           C
ATOM     35  CG  GLN A   5      17.131   8.044  13.738  1.00 10.00           C
ATOM     36  CD  GLN A   5      16.646   7.508  15.045  1.00 10.00           C
ATOM     37  OE1 GLN A   5      16.076   6.419  15.093  1.00 10.00           O
ATOM     38  NE2 GLN A   5      16.851   8.281  16.130  1.00 10.00           N
ATOM     39  N   ASN A   6      20.816   7.758  10.947  1.00 10.00           N
ATOM     40  CA  ASN A   6      22.133   7.404  10.409  1.00 10.00           C
ATOM     41  C   ASN A   6      23.156   7.855  11.415  1.00 10.00           C
ATOM     42  O   ASN A   6      23.521   9.037  11.465  1.00 10.00           O
ATOM     43  CB  ASN A   6      22.367   8.110   9.084  1.00 10.00           C
ATOM     44  CG  ASN A   6      21.263   7.829   8.094  1.00 10.00           C
ATOM     45  OD1 ASN A   6      21.100   6.698   7.642  1.00 10.00           O
ATOM     46  ND2 ASN A   6      20.497   8.841   7.770  1.00 10.00           N
ATOM     47  N   TYR A   7      23.594   6.911  12.238  1.00 10.00           N
ATOM     48  CA  TYR A   7      24.461   7.238  13.390  1.00 10.00           C
ATOM     49  C   TYR A   7      25.905   7.425  12.976  1.00 10.00           C
ATOM     50  O   TYR A   7      26.343   6.905  11.946  1.00 10.00           O
ATOM     51  CB  TYR A   7      24.363   6.159  14.460  1.00 10.00           C
ATOM     52  CG  TYR A   7      22.967   6.023  14.993  1.00 10.00           C
ATOM     53  CD1 TYR A   7      22.512   6.850  16.011  1.00 10.00           C
ATOM     54  CD2 TYR A   7      22.073   5.115  14.418  1.00 10.00           C
ATOM     55  CE1 TYR A   7      21.206   6.743  16.507  1.00 10.00           C
ATOM     56  CE2 TYR A   7      20.782   5.000  14.887  1.00 10.00           C
ATOM     57  CZ  TYR A   7      20.349   5.823  15.922  1.00 10.00           C
ATOM     58  OH  TYR A   7      19.068   5.683  16.382  1.00 10.00           O
ATOM     59  OXT TYR A   7      26.660   8.093  13.703  1.00 10.00           O
TER
HETATM   60  O   HOH A   8       8.831  10.321  13.215  1.00 10.00           O
HETATM   61  O   HOH A   9      26.348   6.473   9.686  1.00 10.00           O
HETATM   62  O   HOH A  10       5.000   7.292   5.091  1.00 10.00           O
HETATM   63  O   HOH A  11      27.105   9.231  15.697  1.00 10.00           O
HETATM   64  O   HOH A  12      28.448   7.244  15.396  1.00 10.00           O
HETATM   65  O   HOH A  13      13.023   7.731  16.243  1.00 10.00           O
HETATM   66  O   HOH A  14      14.398   5.950  16.625  1.00 10.00           O
TER
ATOM     67  N   GLY B   1      11.293  14.706  17.193  1.00 10.00           N
ATOM     68  CA  GLY B   1      11.250  14.301  15.742  1.00 10.00           C
ATOM     69  C   GLY B   1      12.287  13.234  15.510  1.00 10.00           C
ATOM     70  O   GLY B   1      12.779  12.615  16.472  1.00 10.00           O
ATOM     71  N   ASN B   2      12.646  13.017  14.246  1.00 10.00           N
ATOM     72  CA  ASN B   2      13.780  12.132  13.922  1.00 10.00           C
ATOM     73  C   ASN B   2      15.061  12.631  14.518  1.00 10.00           C
ATOM     74  O   ASN B   2      15.324  13.836  14.517  1.00 10.00           O
ATOM     75  CB  ASN B   2      13.956  11.975  12.432  1.00 10.00           C
ATOM     76  CG  ASN B   2      12.718  11.436  11.783  1.00 10.00           C
ATOM     77  OD1 ASN B   2      12.277  10.321  12.107  1.00 10.00           O
ATOM     78  ND2 ASN B   2      12.098  12.249  10.922  1.00 10.00           N
ATOM     79  N   ASN B   3      15.864  11.684  14.996  1.00 10.00           N
ATOM     80  CA  ASN B   3      17.109  11.998  15.680  1.00 10.00           C
ATOM     81  C   ASN B   3      18.347  11.426  14.986  1.00 10.00           C
ATOM     82  O   ASN B   3      18.430  10.213  14.739  1.00 10.00           O
ATOM     83  CB  ASN B   3      17.043  11.472  17.133  1.00 10.00           C
ATOM     84  CG  ASN B   3      18.296  11.833  17.952  1.00 10.00           C
ATOM     85  OD1 ASN B   3      18.600  13.019  18.163  1.00 10.00           O
ATOM     86  ND2 ASN B   3      19.031  10.809  18.397  1.00 10.00           N
ATOM     87  N   GLN B   4      19.297  12.322  14.689  1.00 10.00           N
ATOM     88  CA  GLN B   4      20.686  11.982  14.290  1.00 10.00           C
ATOM     89  C   GLN B   4      21.737  12.700  15.179  1.00 10.00           C
ATOM     90  O   GLN B   4      21.849  13.937  15.206  1.00 10.00           O
ATOM     91  CB  GLN B   4      20.958  12.242  12.802  1.00 10.00           C
ATOM     92  CG  GLN B   4      22.246  11.552  12.304  1.00 10.00           C
ATOM     93  CD  GLN B   4      22.806  12.138  11.002  1.00 10.00           C
ATOM     94  OE1 GLN B   4      23.046  13.362  10.901  1.00 10.00           O
ATOM     95  NE2 GLN B   4      23.052  11.255  10.000  1.00 10.00           N
ATOM     96  N   GLN B   5      22.456  11.915  15.962  1.00 10.00           N
ATOM     97  CA  GLN B   5      23.572  12.455  16.731  1.00 10.00           C
ATOM     98  C   GLN B   5      24.896  11.862  16.263  1.00 10.00           C
ATOM     99  O   GLN B   5      25.070  10.640  16.145  1.00 10.00           O
ATOM    100  CB  GLN B   5      23.358  12.277  18.238  1.00 10.00           C
ATOM    101  CG  GLN B   5      22.131  13.044  18.738  1.00 10.00           C
ATOM    102  CD  GLN B   5      21.646  12.508  20.045  1.00 10.00           C
ATOM    103  OE1 GLN B   5      21.076  11.419  20.093  1.00 10.00           O
ATOM    104  NE2 GLN B   5      21.851  13.281  21.130  1.00 10.00           N
ATOM    105  N   ASN B   6      25.816  12.758  15.947  1.00 10.00           N
ATOM    106  CA  ASN B   6      27.133  12.404  15.409  1.00 10.00           C
ATOM    107  C   ASN B   6      28.156  12.855  16.415  1.00 10.00           C
ATOM    108  O   ASN B   6      28.521  14.037  16.465  1.00 10.00           O
ATOM    109  CB  ASN B   6      27.367  13.110  14.084  1.00 10.00           C
ATOM    110  CG  ASN B   6      26.263  12.829  13.094  1.00 10.00           C
ATOM    111  OD1 ASN B   6      26.100  11.698  12.642  1.00 10.00           O
ATOM    112  ND2 ASN B   6      25.497  13.841  12.770  1.00 10.00           N
ATOM    113  N   TYR B   7      28.594  11.911  17.238  1.00 10.00           N
ATOM    114  CA  TYR B   7      29.461  12.238  18.390  1.00 10.00           C
ATOM    115  C   TYR B   7      30.905  12.425  17.976  1.00 10.00           C
ATOM    116  O   TYR B   7      31.343  11.905  16.946  1.00 10.00           O
ATOM    117  CB  TYR B   7      29.363  11.159  19.460  1.00 10.00           C
ATOM    118  CG  TYR B   7      27.967  11.023  19.993  1.00 10.00           C
ATOM    119  CD1 TYR B   7      27.512  11.850  21.011  1.00 10.00           C
ATOM    120  CD2 TYR B   7      27.073  10.115  19.418  1.00 10.00           C
ATOM    121  CE1 TYR B   7      26.206  11.743  21.507  1.00 10.00           C
ATOM    122  CE2 TYR B   7      25.782  10.000  19.887  1.00 10.00           C
ATOM    123  CZ  TYR B   7      25.349  10.823  20.922  1.00 10.00           C
ATOM    124  OH  TYR B   7      24.068  10.683  21.382  1.00 10.00           O
ATOM    125  OXT TYR B   7      31.660  13.093  18.703  1.00 10.00           O
TER
HETATM  126  O   HOH B   8      13.831  15.321  18.215  1.00 10.00           O
HETATM  127  O   HOH B   9      31.348  11.473  14.686  1.00 10.00           O
HETATM  128  O   HOH B  10      10.000  12.292  10.091  1.00 10.00           O
HETATM  129  O   HOH B  11      32.105  14.231  20.697  1.00 10.00           O
HETATM  130  O   HOH B  12      33.448  12.244  20.396  1.00 10.00           O
HETATM  131  O   HOH B  13      18.023  12.731  21.243  1.00 10.00           O
HETATM  132  O   HOH B  14      19.398  10.950  21.625  1.00 10.00           O
END
"""

pdb_str_poor = """\
CRYST1   38.448   20.321   26.625  90.00  90.00  90.00 P 1
SCALE1      0.026009  0.000000  0.000000        0.00000
SCALE2      0.000000  0.049210  0.000000        0.00000
SCALE3      0.000000  0.000000  0.037559        0.00000
ATOM      1  N   GLY A   1       6.293   9.706  12.193  1.00 10.00           N
ATOM      2  CA  GLY A   1       6.250   9.301  10.742  1.00 10.00           C
ATOM      3  C   GLY A   1       7.287   8.234  10.510  1.00 10.00           C
ATOM      4  O   GLY A   1       7.779   7.615  11.472  1.00 10.00           O1-
ATOM      5  N   ASN A   2       7.646   8.017   9.246  1.00 10.00           N1+
ATOM      6  CA  ASN A   2       8.780   7.132   8.922  1.00 10.00           C
ATOM      7  C   ASN A   2      10.061   7.631   9.518  1.00 10.00           C
ATOM      8  O   ASN A   2      10.324   8.836   9.517  1.00 10.00           O
ATOM      9  CB  ASN A   2       8.956   6.975   7.432  1.00 10.00           C
ATOM     10  CG  ASN A   2       7.718   6.436   6.783  1.00 10.00           C
ATOM     11  OD1 ASN A   2       7.277   5.321   7.107  1.00 10.00           O
ATOM     12  ND2 ASN A   2       7.098   7.249   5.922  1.00 10.00           N
ATOM     13  N   ASN A   3      10.864   6.684   9.996  1.00 10.00           N
ATOM     14  CA  ASN A   3      12.109   6.998  10.680  1.00 10.00           C
ATOM     15  C   ASN A   3      13.347   6.426   9.986  1.00 10.00           C
ATOM     16  O   ASN A   3      13.430   5.213   9.739  1.00 10.00           O
ATOM     17  CB  ASN A   3      12.043   6.472  12.133  1.00 10.00           C
ATOM     18  CG  ASN A   3      13.296   6.833  12.952  1.00 10.00           C
ATOM     19  OD1 ASN A   3      13.600   8.019  13.163  1.00 10.00           O
ATOM     20  ND2 ASN A   3      14.031   5.809  13.397  1.00 10.00           N
ATOM     21  N   GLN A   4      14.297   7.322   9.689  1.00 10.00           N
ATOM     22  CA  GLN A   4      15.686   6.982   9.290  1.00 10.00           C
ATOM     23  C   GLN A   4      16.737   7.700  10.179  1.00 10.00           C
ATOM     24  O   GLN A   4      16.849   8.937  10.206  1.00 10.00           O
ATOM     25  CB  GLN A   4      15.958   7.242   7.802  1.00 10.00           C
ATOM     26  CG  GLN A   4      17.246   6.552   7.304  1.00 10.00           C
ATOM     27  CD  GLN A   4      17.806   7.138   6.002  1.00 10.00           C
ATOM     28  OE1 GLN A   4      18.046   8.362   5.901  1.00 10.00           O
ATOM     29  NE2 GLN A   4      18.052   6.255   5.000  1.00 10.00           N
ATOM     30  N   GLN A   5      17.456   6.915  10.962  1.00 10.00           N
ATOM     31  CA  GLN A   5      18.572   7.455  11.731  1.00 10.00           C
ATOM     32  C   GLN A   5      19.896   6.862  11.263  1.00 10.00           C
ATOM     33  O   GLN A   5      20.070   5.640  11.145  1.00 10.00           O
ATOM     34  CB  GLN A   5      18.358   7.277  13.238  1.00 10.00           C
ATOM     35  CG  GLN A   5      17.131   8.044  13.738  1.00 10.00           C
ATOM     36  CD  GLN A   5      16.646   7.508  15.045  1.00 10.00           C
ATOM     37  OE1 GLN A   5      16.076   6.419  15.093  1.00 10.00           O
ATOM     38  NE2 GLN A   5      16.851   8.281  16.130  1.00 10.00           N
ATOM     39  N   ASN A   6      20.816   7.758  10.947  1.00 10.00           N
ATOM     40  CA  ASN A   6      22.133   7.404  10.409  1.00 10.00           C
ATOM     41  C   ASN A   6      23.156   7.855  11.415  1.00 10.00           C
ATOM     42  O   ASN A   6      23.521   9.037  11.465  1.00 10.00           O
ATOM     43  CB  ASN A   6      22.367   8.110   9.084  1.00 10.00           C
ATOM     44  CG  ASN A   6      21.263   7.829   8.094  1.00 10.00           C
ATOM     45  OD1 ASN A   6      21.100   6.698   7.642  1.00 10.00           O
ATOM     46  ND2 ASN A   6      20.497   8.841   7.770  1.00 10.00           N
ATOM     47  N   TYR A   7      23.594   6.911  12.238  1.00 10.00           N
ATOM     48  CA  TYR A   7      24.461   7.238  13.390  1.00 10.00           C
ATOM     49  C   TYR A   7      25.905   7.425  12.976  1.00 10.00           C
ATOM     50  O   TYR A   7      26.343   6.905  11.946  1.00 10.00           O
ATOM     51  CB  TYR A   7      24.363   6.159  14.460  1.00 10.00           C
ATOM     52  CG  TYR A   7      22.967   6.023  14.993  1.00 10.00           C
ATOM     53  CD1 TYR A   7      22.512   6.850  16.011  1.00 10.00           C
ATOM     54  CD2 TYR A   7      22.073   5.115  14.418  1.00 10.00           C
ATOM     55  CE1 TYR A   7      21.206   6.743  16.507  1.00 10.00           C
ATOM     56  CE2 TYR A   7      20.782   5.000  14.887  1.00 10.00           C
ATOM     57  CZ  TYR A   7      20.349   5.823  15.922  1.00 10.00           C
ATOM     58  OH  TYR A   7      19.068   5.683  16.382  1.00 10.00           O
ATOM     59  OXT TYR A   7      26.660   8.093  13.703  1.00 10.00           O
TER
HETATM   60  O   HOH A   8       8.831  10.321  13.215  1.00 10.00           O
HETATM   61  O   HOH A   9      26.348   6.473   9.686  1.00 10.00           O
HETATM   62  O   HOH A  10       5.000   7.292   5.091  1.00 10.00           O
HETATM   63  O   HOH A  11      27.105   9.231  15.697  1.00 10.00           O
HETATM   64  O   HOH A  12      28.448   7.244  15.396  1.00 10.00           O
HETATM   65  O   HOH A  13      13.023   7.731  16.243  1.00 10.00           O
HETATM   66  O   HOH A  14      14.398   5.950  16.625  1.00 10.00           O
TER
ATOM     67  N   GLY B   1      11.293  14.706  17.193  1.00 10.00           N
ATOM     68  CA  GLY B   1      11.250  14.301  15.742  1.00 10.00           C
ATOM     69  C   GLY B   1      12.287  13.234  15.510  1.00 10.00           C
ATOM     70  O   GLY B   1      12.779  12.615  16.472  1.00 10.00           O
ATOM     71  N   ASN B   2      12.646  13.017  14.246  1.00 10.00           N
ATOM     72  CA  ASN B   2      13.780  12.132  13.922  1.00 10.00           C
ATOM     73  C   ASN B   2      15.061  12.631  14.518  1.00 10.00           C
ATOM     74  O   ASN B   2      15.324  13.836  14.517  1.00 10.00           O
ATOM     75  CB  ASN B   2      13.956  11.975  12.432  1.00 10.00           C
ATOM     76  CG  ASN B   2      12.718  11.436  11.783  1.00 10.00           C
ATOM     77  OD1 ASN B   2      12.277  10.321  12.107  1.00 10.00           O
ATOM     78  ND2 ASN B   2      12.098  12.249  10.922  1.00 10.00           N
ATOM     79  N   ASN B   3      15.864  11.684  14.996  1.00 10.00           N
ATOM     80  CA  ASN B   3      17.109  11.998  15.680  1.00 10.00           C
ATOM     81  C   ASN B   3      18.347  11.426  14.986  1.00 10.00           C
ATOM     82  O   ASN B   3      18.430  10.213  14.739  1.00 10.00           O
ATOM     83  CB  ASN B   3      17.043  11.472  17.133  1.00 10.00           C
ATOM     84  CG  ASN B   3      18.296  11.833  17.952  1.00 10.00           C
ATOM     85  OD1 ASN B   3      18.600  13.019  18.163  1.00 10.00           O
ATOM     86  ND2 ASN B   3      19.031  10.809  18.397  1.00 10.00           N
ATOM     87  N   GLN B   4      19.297  12.322  14.689  1.00 10.00           N
ATOM     88  CA  GLN B   4      20.686  11.982  14.290  1.00 10.00           C
ATOM     89  C   GLN B   4      21.737  12.700  15.179  1.00 10.00           C
ATOM     90  O   GLN B   4      21.849  13.937  15.206  1.00 10.00           O
ATOM     91  CB  GLN B   4      20.958  12.242  12.802  1.00 10.00           C
ATOM     92  CG  GLN B   4      22.246  11.552  12.304  1.00 10.00           C
ATOM     93  CD  GLN B   4      22.806  12.138  11.002  1.00 10.00           C
ATOM     94  OE1 GLN B   4      23.046  13.362  10.901  1.00 10.00           O
ATOM     95  NE2 GLN B   4      23.052  11.255  10.000  1.00 10.00           N
ATOM     96  N   GLN B   5      22.456  11.915  15.962  1.00 10.00           N
ATOM     97  CA  GLN B   5      23.572  12.455  16.731  1.00 10.00           C
ATOM     98  C   GLN B   5      24.896  11.862  16.263  1.00 10.00           C
ATOM     99  O   GLN B   5      25.070  10.640  16.145  1.00 10.00           O
ATOM    100  CB  GLN B   5      23.358  12.277  18.238  1.00 10.00           C
ATOM    101  CG  GLN B   5      22.131  13.044  18.738  1.00 10.00           C
ATOM    102  CD  GLN B   5      21.646  12.508  20.045  1.00 10.00           C
ATOM    103  OE1 GLN B   5      21.076  11.419  20.093  1.00 10.00           O
ATOM    104  NE2 GLN B   5      21.851  13.281  21.130  1.00 10.00           N
ATOM    105  N   ASN B   6      25.816  12.758  15.947  1.00 10.00           N
ATOM    106  CA  ASN B   6      27.133  12.404  15.409  1.00 10.00           C
ATOM    107  C   ASN B   6      28.156  12.855  16.415  1.00 10.00           C
ATOM    108  O   ASN B   6      28.521  14.037  16.465  1.00 10.00           O
ATOM    109  CB  ASN B   6      27.367  13.110  14.084  1.00 10.00           C
ATOM    110  CG  ASN B   6      26.263  12.829  13.094  1.00 10.00           C
ATOM    111  OD1 ASN B   6      26.100  11.698  12.642  1.00 10.00           O
ATOM    112  ND2 ASN B   6      25.497  13.841  12.770  1.00 10.00           N
ATOM    113  N   TYR B   7      28.594  11.911  17.238  1.00 10.00           N
ATOM    114  CA  TYR B   7      29.461  12.238  18.390  1.00 10.00           C
ATOM    115  C   TYR B   7      30.905  12.425  17.976  1.00 10.00           C
ATOM    116  O   TYR B   7      31.343  11.905  16.946  1.00 10.00           O
ATOM    117  CB  TYR B   7      29.363  11.159  19.460  1.00 10.00           C
ATOM    118  CG  TYR B   7      27.967  11.023  19.993  1.00 10.00           C
ATOM    119  CD1 TYR B   7      27.512  11.850  21.011  1.00 10.00           C
ATOM    120  CD2 TYR B   7      27.073  10.115  19.418  1.00 10.00           C
ATOM    121  CE1 TYR B   7      26.206  11.743  21.507  1.00 10.00           C
ATOM    122  CE2 TYR B   7      25.782  10.000  19.887  1.00 10.00           C
ATOM    123  CZ  TYR B   7      25.349  10.823  20.922  1.00 10.00           C
ATOM    124  OH  TYR B   7      24.068  10.683  21.382  1.00 10.00           O
ATOM    125  OXT TYR B   7      31.660  13.093  18.703  1.00 10.00           O
TER
HETATM  126  O   HOH B   8      13.831  15.321  18.215  1.00 10.00           O
HETATM  127  O   HOH B   9      31.348  11.473  14.686  1.00 10.00           O
END
"""

def run(prefix=os.path.basename(__file__).replace(".py","_douse")):
  """
  Exercise basic phenix.douse.
  """
  pdb_file_name_answer = "%s_answer.pdb"%prefix
  of=open(pdb_file_name_answer, "w")
  print(pdb_str_answer, file=of)
  of.close()
  xrs_answer = iotbx.pdb.input(file_name=
   pdb_file_name_answer).xray_structure_simple()
  #
  pdb_file_name_poor = "%s_poor.pdb"%prefix
  of=open(pdb_file_name_poor, "w")
  print(pdb_str_poor, file=of)
  of.close()
  #
  cmd = "phenix.model_map %s output_file_name_prefix=%s > %s.map.zlog"%(
    pdb_file_name_answer, prefix, prefix)
  assert easy_run.call(cmd)==0
  #
  for i, mode in enumerate(["whole","per_chain"]):
    cmd = " ".join([
      "phenix.douse",
      "%s"%pdb_file_name_poor,
      "%s.ccp4"%prefix,
      "resolution=1",
      "output.filename=%s_%d"%(prefix,i),
      " keep_input_water=true",
      "--overwrite",
      ">%s_%d.zlog"%(prefix,i)])
    assert easy_run.call(cmd)==0
  #
  xrs_1 = iotbx.pdb.input(file_name="%s_%d.pdb"%(prefix,0)).xray_structure_simple()
  xrs_2 = iotbx.pdb.input(file_name="%s_%d.pdb"%(prefix,1)).xray_structure_simple()
  #
  fc  = xrs_answer.structure_factors(d_min=2, algorithm="direct").f_calc()
  fc1 = fc.structure_factors_from_scatterers(
    xray_structure=xrs_1, algorithm="direct").f_calc()
  fc2 = fc.structure_factors_from_scatterers(
    xray_structure=xrs_2, algorithm="direct").f_calc()
  #
  def get_r(x, y):
    x = abs(x).data()
    y = abs(y).data()
    return flex.sum(flex.abs(x-y)) / flex.sum(flex.abs(x+y)) * 2 * 100
  for i,fi in enumerate([fc, fc1, fc2]):
    for j,fj in enumerate([fc, fc1, fc2]):
      r = get_r(fi, fj)
      if(not 0 in [i,j]): assert approx_equal(r, 0)
      else:               r < 0.5

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
