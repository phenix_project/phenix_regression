import sys, os, time
from libtbx import easy_run
import libtbx.load_env
import run_examples
from phenix_regression.refinement import check_r_factors


def run(args):
  assert len(args) == 2
  start, end = int(args[0]), int(args[1])
  for suffix in run_examples.keys[start:end]:
    for line in open("refinement_examples/example_%-s/run"%suffix, "r").readlines():
      cmd = line.strip()
      if(cmd.startswith("phenix.refine")): cmd += " --quiet "
      if(cmd.startswith("phenix.elbow")): cmd += " --silent "
      easy_run.call(
        "cd refinement_examples/example_%-s ; %s ; cd .."%(suffix, cmd))
    file = "refinement_examples/example_%-s/%-s_001.pdb"%(suffix,suffix)
    time.sleep(5)
    assert check_r_factors(
      file_name        = file,
      r_work_final_ref = run_examples.examples[suffix][0],
      r_free_final_ref = run_examples.examples[suffix][1],
      eps              = 0.02,
      info_low_eps     = 0.03)

if(__name__ == "__main__"):
  run(sys.argv[1:])
