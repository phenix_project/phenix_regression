from __future__ import division
from __future__ import print_function
import sys,os
import libtbx.load_env
from libtbx.test_utils import approx_equal

import time,sys

data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="segment_and_split_map",
  test=os.path.isdir)

helix_map_file=os.path.join(data_dir,"helix.ccp4")
d_seven_map_file=os.path.join(data_dir,"D7.ccp4")

def tst_01():
  print("testing map_symmetry with D7 symmetry")
  args=["map_file=%s" %(d_seven_map_file),
         "symmetry=D7",]
  try:
    from iotbx.cli_parser import run_program
    from phenix.programs import map_symmetry as run
  except Exception as e:
    print("Map_symmetry not available...skipping")
    return

  results=run_program(program_class=run.Program,args=args)

  ncs_object=results.ncs_object
  cc=results.cc
  print("CC value %.2f " %(cc))
  print("Number of operators: %d" %(ncs_object.max_operators()))
  assert approx_equal(float("%.2f" %(cc)),0.98)
  assert approx_equal(ncs_object.max_operators(),14)
  print("OK")

def tst_02():
  print("testing map_symmetry with helical symmetry")
  args=["map_file=%s" %(helix_map_file),
         "min_ncs_cc=0.75", 
         "symmetry=helical","resolution=2"]
  try:
    from iotbx.cli_parser import run_program
    from phenix.programs import map_symmetry as run
  except Exception as e:
    print("Map_symmetry not available...skipping")
    return

  results=run_program(program_class=run.Program,args=args)

  ncs_object=results.ncs_object
  cc=results.cc
  if cc is None: cc=0.0
  print("CC value %.2f " %(cc))
  print("Number of operators: %d" %(ncs_object.max_operators()))
  assert approx_equal(float("%.2f" %(cc)),0.80)
  assert approx_equal(ncs_object.max_operators(),5)
  print("OK")



if __name__=="__main__":
  t0 = time.time()
  tst_01()
  tst_02()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
