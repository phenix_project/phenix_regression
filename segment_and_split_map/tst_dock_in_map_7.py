from __future__ import division
from __future__ import print_function
import sys,os
import libtbx.load_env
from libtbx.test_utils import approx_equal

import time,sys

data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="segment_and_split_map",
  test=os.path.isdir)

small_pdb=os.path.join(data_dir,"short_p21.pdb")
small_map_file=os.path.join(data_dir,"short_p21.ccp4")

def tst_01():
  print("testing dock_in_map with small model and map in P21")
  args=["%s" %(small_map_file),
        "%s" %(small_pdb),
         "align_moments=False",
         "ssm_search=False",
         "output_files.temp_dir=dom_1",
         "dock_with_mr=false",
          "quick=false",
          "run_in_boxes=true",
          "target_boxes=4",
          "target_box_size=16",
         "resolution=3",]
  print ("phenix.dock_in_map %s" %(" ".join(args)))
  try:
    from iotbx.cli_parser import run_program
    from phenix.programs import dock_in_map as run
  except Exception as e:
    print("Dock in map not available...skipping")
    return

  if not os.path.isdir("dock_in_map_1"):
    os.mkdir("dock_in_map_1")
  os.chdir("dock_in_map_1")
  
  results=run_program(program_class=run.Program,args=args)

  final_model=results.final_model
  r,t=results.final_r,results.final_t
  print(tuple(r),tuple(t))

  assert approx_equal ( tuple(r), (-0.9999870647590131, -0.003276536182634794, -0.0038903245747922044, -0.003270933507876724, 0.9999936055299243, -0.0014456463080531816, 0.003895036410663757, -0.001432902615261601, -0.9999913877036416) ,
   eps= 0.2)

  assert approx_equal ( tuple(t), (25.186739280163096, 0.04985010816394819, 19.989356060150172), eps=0.4)
  print("OK")

if __name__=="__main__":
  t0 = time.time()
  tst_01()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
