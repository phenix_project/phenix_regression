from __future__ import division
from __future__ import print_function
import sys,os
import libtbx.load_env
from libtbx.test_utils import approx_equal
import random
from scitbx.array_family import flex

import time,sys

data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="segment_and_split_map",
  test=os.path.isdir)

multi_file=os.path.join(data_dir,"multi.pdb")
file_prefix=os.path.join(data_dir,"multi_")

def tst_01():

  print("Testing get_struct_fact_from_md with separate models.")
  args=[
      "template=%s" %( file_prefix),
         "first=1",
         "last=7",
         "d_min=3",
         ]
  cmd="phenix.get_struct_fact_from_md %s" %(" ".join(args))
  print (cmd)

  from libtbx import easy_run
  out = easy_run.fully_buffered(cmd).raise_if_errors()
  print ("\n".join(out.stdout_lines))
  assert "  -4   -2    1       107.47" in out.stdout_lines


def tst_02():

  print("Testing get_struct_fact_from_md with multiple models in one file")
  args=[
      "pdb_file=%s" %( multi_file),
         "d_min=3",
         ]
  cmd="phenix.get_struct_fact_from_md %s" %(" ".join(args))
  print (cmd)

  from libtbx import easy_run
  out = easy_run.fully_buffered(cmd).raise_if_errors()
  print ("\n".join(out.stdout_lines))
  assert "  -4   -2    1       107.47" in out.stdout_lines



if __name__=="__main__":

  t0 = time.time()
  tst_01()
  tst_02()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
