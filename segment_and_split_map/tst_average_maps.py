from __future__ import division
from __future__ import print_function
import sys,os
import libtbx.load_env
from libtbx.test_utils import approx_equal
import random
from scitbx.array_family import flex

import time,sys

data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="segment_and_split_map",
  test=os.path.isdir)

files=[]
for f in "half_map_a_12.ccp4 half_map_a_1.ccp4 half_map_a_2.ccp4 half_map_b_12.ccp4 half_map_b_1.ccp4 half_map_b_2.ccp4 model_A30_B30.ccp4".split():
   files.append(os.path.join(data_dir,f))

def tst_01():

  print("Testing average_maps with randomized map.")

  cmd="phenix.average_maps %s map_file_types='map half_map_1 half_map_2 map half_map_1 half_map_2 target_map' resolution=2.1 half_map_averaging=true resolution=4 n_bins=4" %(" ".join(files))
  print(cmd)

  from libtbx import easy_run
  #out = easy_run.fully_buffered(cmd).raise_if_errors()
  out = easy_run.fully_buffered(cmd)
  data_line=None
  for line in out.stdout_lines:
    if line.startswith("FSCref = 0.5:"):
      data_line=line
  data=[]
  for x in data_line.replace("FSCref = 0.5:","").split():
    data.append(float(x))
  print(data)
  assert approx_equal(tuple(data),(4.49,3.84,3.99,3.91,3.65,0.00))

if __name__=="__main__":

  t0 = time.time()
  tst_01()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
