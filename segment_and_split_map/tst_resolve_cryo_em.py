from __future__ import division
from __future__ import print_function
import os
import libtbx.load_env
from libtbx.test_utils import approx_equal
import random
from scitbx.array_family import flex

import time

data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="segment_and_split_map",
  test=os.path.isdir)

map_file_1_orig=os.path.join(data_dir,"random_1.ccp4")
map_file_2_orig=os.path.join(data_dir,"random_2.ccp4")
target_map_file_orig=os.path.join(data_dir,"model_A30_trans_100.ccp4")
target_model_file_orig=os.path.join(data_dir,"model_A30_trans_100.pdb")
seq_file=os.path.join(data_dir,"model_A30_trans_100.seq")

map_file_1_shifted=os.path.join(data_dir,"random_1_shifted.ccp4")
map_file_2_shifted=os.path.join(data_dir,"random_2_shifted.ccp4")
target_map_file_shifted=os.path.join(data_dir,"model_A30_trans_100_shifted.ccp4")
target_model_file_shifted=os.path.join(data_dir,"model_A30_trans_100_shifted.pdb")

def tst_01(map_file_1,map_file_2,target_map_file,seq_file,output_file):

  print("Testing resolve_cryo_em with randomized map.")
  args=["nproc=1","calculate_local_resolution=False",
      "final_scale_with_fsc_ref=False",
       "sharpen_map_data_before_scaling=False",
      "%s" %( map_file_1),
          "%s" %(map_file_2),
          "seq_file=%s" %(seq_file),
         "target_map_file_name=%s" %(target_map_file),
         "resolution=3",
         "verbose=true",
         "output_files.denmod_map_file_name=%s" %output_file]
  print ("phenix.resolve_cryo_em %s" %(" ".join(args)))
  try:
    from iotbx.cli_parser import run_program
    from phenix.programs import resolve_cryo_em as run
  except Exception as e:
    print("resolve_cryo_em not available...skipping")
    return

  from six.moves import cStringIO as StringIO
  f=StringIO()
  results=run_program(program_class=run.Program,args=args,logger=f)
  text=f.getvalue()
  print (text)
  assert approx_equal(float("%.2f" %(results.d_cc_star_half_avg)),3.04,eps=0.1)

  from iotbx.map_model_manager import map_model_manager
  mm2=results.target_map_map_manager
  mm1=results.denmod_map_manager
  mam=map_model_manager(map_manager=mm1,extra_map_manager_list=[mm2],extra_map_manager_id_list=['other'])
  cc=mam.map_map_cc('map_manager','other')
  print("Map-map cc: ",cc)
  return cc

def tst_02(map_file_1,map_file_2,target_map_file,target_model_file,seq_file,output_file):

  random.seed(1)
  flex.set_random_seed(1)

  print("Testing resolve_cryo_em with randomized map and model.")
  args=[
      "%s" %( map_file_1),
          "%s" %(map_file_2),
          "seq_file=%s" %(seq_file),
          "model_1=%s" %(target_model_file),
          "density_modify_with_model=True",
          "verbose=true",
          "minimum_model_files=2",
          "randomize_single_model=False",
          "vary_blur_and_spectral_scaling_with_model=False",
         "target_map_file_name=%s" %(target_map_file),
         "resolution=3",
         "macro_cycles=1",
         "output_files.denmod_map_file_name=%s" %output_file]
  print ("phenix.resolve_cryo_em %s" %(" ".join(args)))
  try:
    from iotbx.cli_parser import run_program
    from phenix.programs import resolve_cryo_em as run
  except Exception as e:
    print("resolve_cryo_em not available...skipping")
    return

  print("Running now")
  from six.moves import cStringIO as StringIO
  f=StringIO()
  results=run_program(program_class=run.Program,args=args,logger=f)
  text=f.getvalue()
  #print (text)
  from iotbx.map_model_manager import map_model_manager
  mm2=results.target_map_map_manager
  mm1=results.denmod_map_manager
  mam=map_model_manager(map_manager=mm1,extra_map_manager_list=[mm2],extra_map_manager_id_list=['other'])
  cc=mam.map_map_cc('map_manager','other')
  print(("Map-map cc: ",cc))

  return cc

def tst_03(map_file_1,map_file_2,target_map_file,seq_file,output_file):

  print("Testing resolve_cryo_em with randomized map and restore_full_size.")
  from iotbx.data_manager import DataManager
  dm=DataManager()
  m1=dm.get_real_map(map_file_1)
  m2=dm.get_real_map(map_file_2)
  from iotbx.map_model_manager import map_model_manager
  mam=map_model_manager(map_manager_1=m1,map_manager_2=m2)
  mam.box_all_maps_with_bounds_and_shift_origin(lower_bounds=(10,10,10),
    upper_bounds=(80,80,80))
  mam.write_map('map1.mrc',map_id='map_manager_1')
  mam.write_map('map2.mrc',map_id='map_manager_2')

  args=[
      'map1.mrc',
      'map2.mrc',
          "seq_file=%s" %(seq_file),
         "resolution=3",
         "restore_full_size=True",
         "output_files.denmod_map_file_name=%s" %output_file]
  print ("phenix.resolve_cryo_em %s" %(" ".join(args)))
  try:
    from iotbx.cli_parser import run_program
    from phenix.programs import resolve_cryo_em as run
  except Exception as e:
    print("resolve_cryo_em not available...skipping")
    return


  from six.moves import cStringIO as StringIO
  f=StringIO()
  results=run_program(program_class=run.Program,args=args,logger=f)
  text=f.getvalue()
  #print text

  mm1=results.denmod_map_manager
  print(mm1.map_data().origin(),mm1.map_data().all())
  assert mm1.map_data().origin()==(0,0,0)
  assert mm1.map_data().all()== (64, 54, 64)


if __name__=="__main__":

  t0 = time.time()
  cc_orig=tst_01(map_file_1_orig,map_file_2_orig,target_map_file_orig,seq_file,
   'tst_01a.ccp4')
  cc_shifted=tst_01(map_file_1_shifted,map_file_2_shifted,target_map_file_shifted,seq_file,'tst_01b.ccp4')
  assert approx_equal(cc_orig,cc_shifted)
  cc_orig=tst_02(map_file_1_orig,map_file_2_orig,target_map_file_orig,target_model_file_orig,seq_file, 'tst_01aa.ccp4')
  print("CCoriginal",cc_orig)
  cc_shifted=tst_02(map_file_1_shifted,map_file_2_shifted,target_map_file_shifted,target_model_file_shifted,seq_file,'tst_01bb.ccp4')
  print("CCnew",cc_shifted)
  assert approx_equal(cc_orig,cc_shifted,eps=0.001)
  tst_03(map_file_1_orig,map_file_2_orig,target_map_file_orig,seq_file,
   'tst_03a.ccp4')

  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
