from __future__ import division
from __future__ import print_function
import sys,os
import libtbx.load_env
from libtbx.test_utils import approx_equal
import random
from scitbx.array_family import flex

import time,sys

data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="segment_and_split_map",
  test=os.path.isdir)

seq_file=os.path.join (data_dir,'model_A30_trans_100.seq')
map_file_1_shifted=os.path.join(data_dir,"random_1_shifted.ccp4")
map_file_2_shifted=os.path.join(data_dir,"random_2_shifted.ccp4")
target_map_file_shifted=os.path.join(data_dir,"model_A30_trans_100_shifted.ccp4")
target_model_file_shifted=os.path.join(data_dir,"model_A30_trans_100_shifted.pdb")

def tst_01():


  print("Testing resolve_cryo_em with randomized shifted map and model in distant directory.")
  args=[
      "%s" %( map_file_1_shifted),
          "%s" %(map_file_2_shifted),
          "seq_file=%s" %(seq_file),
         "target_map_file_name=%s" %(target_map_file_shifted),
         "model_1=%s" %(target_model_file_shifted),
         "randomize_single_model=False",
         "vary_blur_and_spectral_scaling_with_model=False",
         "minimum_model_files=2",
         "macro_cycles=1",
         "resolution=3",
         "box_with_model_if_present=False",

         "temp_dir=%s" %("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"+
             "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"+
             "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"),
         "verbose=true",
         "output_files.denmod_map_file_name=%s" %("tst_01.ccp4")]
  print ("phenix.resolve_cryo_em %s" %(" ".join(args)))
  try:
    from iotbx.cli_parser import run_program
    from phenix.programs import resolve_cryo_em as run
  except Exception as e:
    print("resolve_cryo_em not available...skipping")
    return

  from six.moves import cStringIO as StringIO
  f=StringIO()
  results=run_program(program_class=run.Program,args=args,logger=f)
  text=f.getvalue()
  print (text)

  from iotbx.map_model_manager import map_model_manager
  mm2=results.target_map_map_manager
  mm1=results.denmod_map_manager

  mam=map_model_manager(map_manager=mm1,extra_map_manager_list=[mm2],extra_map_manager_id_list=['other'])
  cc=mam.map_map_cc('map_manager','other')
  print("Map-map cc: ",cc)
  assert approx_equal(cc,0.90,eps=0.05)
  return cc

if __name__=="__main__":

  t0 = time.time()
  tst_01()

  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
