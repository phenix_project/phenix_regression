from __future__ import division
from __future__ import print_function
import sys,os
import libtbx.load_env
from libtbx.utils import null_out

import time,sys

input_value_dict={43.10284229:658643,
63.10284229:219182,
83.10284229:7551,
103.1028423:1976,
123.1028423:803,
143.1028423:453,
163.1028423:346,
183.1028423:278,
203.1028423:246,}


from scitbx.math.breakpoint import run as breakpoint

def tst_01():
  print("testing breakpoint ")
  breakpoint_value=breakpoint(value_dict=input_value_dict,use_log=True,
    target_offset=1)
  assert ("%7.2f" %breakpoint_value).strip()=="67.48"

if __name__=="__main__":
  t0 = time.time()
  tst_01()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
