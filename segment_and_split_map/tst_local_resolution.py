from __future__ import division
from __future__ import print_function
import os
import libtbx.load_env
from libtbx.test_utils import approx_equal
import random
from scitbx.array_family import flex

import time

data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="segment_and_split_map",
  test=os.path.isdir)

map_file_1_orig=os.path.join(data_dir,"random_1.ccp4")
map_file_2_orig=os.path.join(data_dir,"random_2.ccp4")
target_map_file_orig=os.path.join(data_dir,"model_A30_trans_100.ccp4")
target_model_file_orig=os.path.join(data_dir,"model_A30_trans_100.pdb")
seq_file=os.path.join(data_dir,"model_A30_trans_100.seq")

map_file_1_shifted=os.path.join(data_dir,"random_1_shifted.ccp4")
map_file_2_shifted=os.path.join(data_dir,"random_2_shifted.ccp4")
target_map_file_shifted=os.path.join(data_dir,"model_A30_trans_100_shifted.ccp4")
target_model_file_shifted=os.path.join(data_dir,"model_A30_trans_100_shifted.pdb")

def tst_01(map_file_1,map_file_2,target_map_file,seq_file,output_file):

  print("Testing local_resolution with randomized map.")
  args=["nproc=1",
      "%s" %( map_file_1),
          "%s" %(map_file_2),
         "resolution=3",
         "output.file_name=%s" %output_file]
  print ("phenix.local_resolution %s" %(" ".join(args)))
  try:
    from iotbx.cli_parser import run_program
    from phenix.programs import local_resolution as run
  except Exception as e:
    print("local_resolution not available...skipping")
    return

  from six.moves import cStringIO as StringIO
  f=StringIO()
  results=run_program(program_class=run.Program,args=args,logger=f)
  text=f.getvalue()
  print (text)
  values = (
      results.max_resolution, results.mean_resolution, results.min_resolution)
  print(values)
  assert approx_equal(values,
     (6.545271150491988, 4.880471236366378, 3.5603415431040975),
     eps = 0.1)


if __name__=="__main__":

  t0 = time.time()
  tst_01(map_file_1_orig,map_file_2_orig,target_map_file_orig,seq_file,
   'tst_01a.ccp4')

  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
