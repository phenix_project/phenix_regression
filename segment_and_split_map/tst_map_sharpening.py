from __future__ import division
from __future__ import print_function
import sys,os
import libtbx.load_env
from libtbx.test_utils import approx_equal
import random
from scitbx.array_family import flex
from iotbx.data_manager import DataManager

import time,sys

data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="segment_and_split_map",
  test=os.path.isdir)

def tst_01():

  print("Testing map_sharpening with map and model")

  # Set up a data_manager to read and write files
  dm=DataManager()

  # Read in a cut_out map and a model and refit a loop

  try:
    from iotbx.cli_parser import run_program
    from phenix.programs import map_sharpening as run
  except Exception as e:
    print("map_sharpening not available...skipping")
    return

  dd = 'tst_map_sharpening'
  if not os.path.isdir(dd):
    os.mkdir(dd)
  os.chdir(dd)
  fn1 = os.path.join(data_dir,'half_map_a_1.ccp4')
  fn2 = os.path.join(data_dir,'A.pdb')

  args = ['full_map=%s' %(fn1),
          'model=%s' %(fn2),
          'model_sharpen=True']
  print("phenix.map_sharpening %s" %(" ".join(args)))

  results = run_program(program_class=run.Program,args=args)
  print(results)
  final_map_model_cc = results.final_map_model_cc
  assert approx_equal(final_map_model_cc, 0.66, eps=0.2)

def tst_02():

  print("Testing map_sharpening with two half-maps")

  # Set up a data_manager to read and write files
  dm=DataManager()

  # Read in a cut_out map and a model and refit a loop

  try:
    from iotbx.cli_parser import run_program
    from phenix.programs import map_sharpening as run
  except Exception as e:
    print("map_sharpening not available...skipping")
    return

  dd = 'tst_map_sharpening'
  if not os.path.isdir(dd):
    os.mkdir(dd)
  os.chdir(dd)
  fn1 = os.path.join(data_dir,'half_map_a_1.ccp4')
  fn2 = os.path.join(data_dir,'half_map_a_2.ccp4')
  seq_file = os.path.join(data_dir,'short.seq')

  args = ['half_map=%s' %(fn1),
          'half_map=%s' %(fn2),
           'anisotropic_sharpen=False',
          'seq_file=%s' %(seq_file),]
  print("phenix.map_sharpening %s" %(" ".join(args)))

  results = run_program(program_class=run.Program,args=args)
  final_d_fsc_half_map_143 = results.final_d_fsc_half_map_143
  assert approx_equal(final_d_fsc_half_map_143, 2.0, eps=1.5)

if __name__=="__main__":

  t0 = time.time()
  tst_01()
  tst_02()

  print("Time: %6.4f"%(time.time()-t0))
  print("OK")

