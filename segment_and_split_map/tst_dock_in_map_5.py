from __future__ import division
from __future__ import print_function
import os
import libtbx.load_env
from libtbx.test_utils import approx_equal

import time

data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="segment_and_split_map",
  test=os.path.isdir)

small_first_part=os.path.join(data_dir,"placed_model_1-20.pdb")
small_second_part=os.path.join(data_dir,"placed_model_21-30.pdb")
small_pdb=os.path.join(data_dir,"small_model.pdb")
small_pdb_two_models=os.path.join(data_dir,"small_model_two_models.pdb")
small_start_pdb=os.path.join(data_dir,"start.pdb")
small_map_file=os.path.join(data_dir,"small_map.ccp4")

def tst_01():
  print("testing dock_in_map with small model and map")
  args=["%s" %(small_map_file),
        "%s" %(small_pdb),
         "output_files.temp_dir=dom_5",
         "resolution=3",]
  print(' '.join(args))
  try:
    from iotbx.cli_parser import run_program
    from phenix.programs import dock_in_map as run
  except Exception as e:
    print("Dock in map not available...skipping")
    return

  results=run_program(program_class=run.Program,args=args)

  final_model=results.final_model
  r,t=results.final_r,results.final_t
  print(tuple(r),tuple(t))

  assert approx_equal ( tuple(r),(1.0000092118571706, -0.001136872777244828, 0.0036734241713625437, 0.001207922448047554, 0.9999816275000978, -0.0008448179734570488, -0.0035942903505721546, 0.0008532431643868826, 1.0000008593073026),eps=0.3)
  assert approx_equal ( tuple(t),   (0.011180890314316017, 0.03260699343865703, -0.07368853605880687),eps=0.5)
  print("OK")


if __name__=="__main__":
  t0 = time.time()
  tst_01()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
