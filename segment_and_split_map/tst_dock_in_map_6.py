from __future__ import division
from __future__ import print_function
import sys,os
import libtbx.load_env
from libtbx.test_utils import approx_equal

import time,sys

data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="segment_and_split_map",
  test=os.path.isdir)

small_first_part=os.path.join(data_dir,"placed_model_1-20.pdb")
small_second_part=os.path.join(data_dir,"placed_model_21-30.pdb")
small_pdb=os.path.join(data_dir,"placed_model_1-20.pdb")
small_pdb_two_models=os.path.join(data_dir,"small_model_two_models.pdb")
small_start_pdb=os.path.join(data_dir,"start.pdb")
small_map_file=os.path.join(data_dir,"small_map.ccp4")

def tst_01():
  print("testing dock_in_map with small model with 2 models and map")
  args=["%s" %(small_map_file),
        "%s" %(small_pdb),
        "%s" %(small_second_part),
         "resolution=3",
         "output_files.temp_dir=dom_6",
         "dock_with_mr=True",
         "dock_chains_individually=False",
         "refine_cycles=0",
         "search.rigid_body_refinement=false"]
  try:
    from iotbx.cli_parser import run_program
    from phenix.programs import dock_in_map as run
  except Exception as e:
    print("Dock in map not available...skipping")
    return

  results=run_program(program_class=run.Program,args=args)

  final_model=results.final_model
  assert approx_equal(results.final_cc, 0.7, eps=0.3)
  assert final_model.overall_counts().n_residues==29
  print("OK")

if __name__=="__main__":
  t0 = time.time()
  tst_01()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
