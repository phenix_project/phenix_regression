
import libtbx.load_env
import re
import os

def get_wizard_test_file (file_name) :
  full_path = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/wizards/%s" % file_name,
    test=os.path.isfile)
  return full_path

def get_wizard_run_dir (lines, wizard_name) :
  for line in lines :
    if (line.startswith("Working directory:")) and (wizard_name in line) :
      dir_name = re.sub("Working directory:\ *", "", line).strip()
      assert (os.path.isdir(dir_name))
      return dir_name
  return None
