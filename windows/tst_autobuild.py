from __future__ import print_function
from phenix_regression.windows import *
from libtbx import easy_run
import os

def exercise () :
  model_file = get_wizard_test_file("coords.pdb")
  hkl_file = get_wizard_test_file("perfect.mtz")
  seq_file = get_wizard_test_file("seq.dat")
  if (None in [model_file, seq_file, hkl_file]) :
    print("Files not found, skipping")
    return False
  args = ["model=\"%s\"" % model_file,
          "seq_file=\"%s\"" % seq_file,
          "data=\"%s\"" % hkl_file,
          "map_file=\"%s\"" % hkl_file,
          "resolution=3.0",
          "solvent_fraction=0.6",
          "input_labels=FP SIGFP PHIC FOM",
          "input_map_labels=FP PHIC FOM",
          "n_cycle_rebuild_min=1",
          "n_cycle_rebuild_max=1",
          "start_chains_list=92",
          "number_of_parallel_models=1",
          "rebuild_in_place=True",]
  stdout_lines = easy_run.fully_buffered(
    ["phenix.autobuild"]+args).raise_if_errors().stdout_lines
  dir_name = get_wizard_run_dir(stdout_lines, "AutoBuild")
  assert (dir_name is not None)
  pdb_file = os.path.join(dir_name, "overall_best.pdb")
  assert (os.path.isfile(pdb_file))
  from phenix.refinement.runtime import extract_r_factors_and_rmsds
  rw, rf, b, a = extract_r_factors_and_rmsds(pdb_file, as_floats=True)
  #print "r_free=%.4f" % rf
  assert (rf < 0.1)
  print("OK")

if (__name__ == "__main__") :
  exercise()
