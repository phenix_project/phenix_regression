from __future__ import print_function

from phenix_regression.windows import *
from libtbx import easy_run
import os

def exercise () :
  model_file = get_wizard_test_file("partial.pdb")
  lig_file = get_wizard_test_file("side.pdb")
  hkl_file = get_wizard_test_file("perfect.mtz")
  if (None in [model_file, lig_file, hkl_file]) :
    print("Files not found, skipping")
    return False
  args = ["model=\"%s\"" % model_file,
          "ligand=\"%s\"" % lig_file,
          "data=\"%s\"" % hkl_file,
          "resolution=3.0",
          "n_group_search=1",
          "input_labels=FP PHIC FOM",
          "ligand_cc_min=0.5"]
  stdout_lines = easy_run.fully_buffered(
    ["phenix.ligandfit"]+args).raise_if_errors().stdout_lines
  dir_name = get_wizard_run_dir(stdout_lines, "LigandFit")
  assert (dir_name is not None)
  pdb_file = os.path.join(dir_name, "ligand_fit_1.pdb")
  assert (os.path.isfile(pdb_file))
  summary = os.path.join(dir_name, "LigandFit_summary.dat")
  for line in open(summary).readlines() :
    if ("side.pdb" in line) :
      fields = line.strip().split()
      print("CC =", fields[2])
      break
  print("OK")

if (__name__ == "__main__") :
  exercise()
