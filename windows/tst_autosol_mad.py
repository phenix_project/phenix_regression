from __future__ import print_function
from phenix_regression.windows import *
from libtbx import easy_run
import os

def exercise () :
  for file_name in ["seq.dat", "peak.sca", "inf.sca"] :
    full_path = get_wizard_test_file(file_name)
    assert (full_path is not None)
    open(file_name, "w").write(open(full_path).read())
  eff_file = get_wizard_test_file("mad.eff")
  stdout_lines = easy_run.fully_buffered(
    ["phenix.autosol", eff_file]).raise_if_errors().stdout_lines
  dir_name = get_wizard_run_dir(stdout_lines, "AutoSol")
  assert (dir_name is not None)
  pdb_file = os.path.join(dir_name, "overall_best.pdb")
  assert (os.path.isfile(pdb_file))
  from phenix.refinement.runtime import extract_r_factors_and_rmsds
  rw, rf, b, a = extract_r_factors_and_rmsds(pdb_file, as_floats=True)
  print("r_free=%.4f" % rf)
  print("OK")

if (__name__ == "__main__") :
  exercise()
