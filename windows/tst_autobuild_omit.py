from __future__ import print_function
from phenix_regression.windows import get_wizard_test_file, get_wizard_run_dir
from libtbx import easy_run
import os

def exercise () :
  model_file = get_wizard_test_file("help_tests/test_help/coords.pdb")
  hkl_file = get_wizard_test_file("help_tests/test_help/perfect.mtz")
  seq_file = get_wizard_test_file("help_tests/test_help/seq.dat")
  assert None not in [model_file, seq_file, hkl_file]
  args = ["model=\"%s\"" % model_file,
          "seq_file=\"%s\"" % seq_file,
          "data=\"%s\"" % hkl_file,
          "resolution=3.0",
          "solvent_fraction=0.6",
          "input_labels=FP SIGFP",
          "n_cycle_rebuild_min=1",
          "n_cycle_rebuild_max=1",
          "start_chains_list=92",
          "number_of_parallel_models=1",
          "rebuild_in_place=True",
          "build_type=resolve",
          "omit_box_pdb_list=\"%s\"" % model_file,
          "omit_res_start_list=93 95",
          "omit_res_end_list=93 95",
          "maps_only=True",
          "composite_omit_type=simple_omit"]
  # XXX for some reason raise_if_errors() does indeed raise an error even when
  # the job completes successfully on windows...
  stdout_lines = easy_run.fully_buffered(
    ["phenix.autobuild"]+args).stdout_lines
  dir_name = get_wizard_run_dir(stdout_lines, "AutoBuild")
  assert (dir_name is not None)
  map_file = os.path.join(dir_name, "OMIT", "resolve_composite_map.mtz")
  assert (os.path.isfile(map_file))
  from iotbx import mtz
  mtz.object(map_file)
  print("OK")

if (__name__ == "__main__") :
  exercise()
