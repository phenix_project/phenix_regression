from __future__ import print_function

from phenix_regression.windows import *
from libtbx import easy_run
import os

def exercise () :
  seq_file = get_wizard_test_file("seq.dat")
  sca_file = get_wizard_test_file("native.sca")
  pdb_file = get_wizard_test_file("coords.pdb")
  if (seq_file is None) or (sca_file is None) or (pdb_file is None) :
    print("Files not found, skipping")
    return False
  if (os.path.isfile("PHASER.1.pdb")) :
    os.remove("PHASER.1.pdb")
  args = [
    "coords=\"%s\"" % pdb_file,
    "data=\"%s\"" % sca_file,
    "seq_file=\"%s\"" % seq_file,
    "component_copies=1",
    "copies_to_find=1",
    "RMS=0.85",
    "build=False",]
  automr_out = easy_run.fully_buffered(
    ["phenix.automr"]+args).raise_if_errors().stdout_lines
  dir_name = get_wizard_run_dir(automr_out, "AutoMR")
  assert (dir_name is not None)
  pdb_file = os.path.join(dir_name, "MR.1.pdb")
  assert (os.path.isfile(pdb_file))
  print("OK")

if (__name__ == "__main__") :
  exercise()
