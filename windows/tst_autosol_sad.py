from __future__ import print_function

from phenix_regression.windows import *
from libtbx import easy_run
import os

def exercise () :
  seq_file = get_wizard_test_file("seq.dat")
  sca_file = get_wizard_test_file("w1.sca")
  if (seq_file is None) or (sca_file is None) :
    print("Files not found, skipping")
    return False
  args = [seq_file, sca_file, "atom_type=Se"]
  stdout_lines = easy_run.fully_buffered(
    ["phenix.autosol"]+args).raise_if_errors().stdout_lines
  dir_name = get_wizard_run_dir(stdout_lines, "AutoSol")
  assert (dir_name is not None)
  pdb_file = os.path.join(dir_name, "overall_best.pdb")
  assert (os.path.isfile(pdb_file))
  from phenix.refinement.runtime import extract_r_factors_and_rmsds
  rw, rf, b, a = extract_r_factors_and_rmsds(pdb_file, as_floats=True)
  print("r_free=%.4f" % rf)
  print("OK")

if (__name__ == "__main__") :
  exercise()
