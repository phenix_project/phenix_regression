
from libtbx import test_utils
import libtbx.load_env

def run() :
  tst_list = (
    "$D/windows/tst_autosol_sad.py",
    "$D/windows/tst_autosol_mad.py",
    "$D/windows/tst_autobuild.py",
    "$D/windows/tst_autobuild_omit.py",
    "$D/windows/tst_ligandfit.py",
#    "$D/refinement/tst_refinement_comprehensive.py",
    "$D/refinement/tst_refinement_nqh_flips.py",
#    "$D/validation/tst_molprobity_protein.py",
#    "$D/validation/tst_molprobity_rna.py",
#    "$D/validation/tst_geometry.py",
    "$D/validation/tst_comprehensive.py",
    "$D/validation/tst_suitename.py",
    "$D/HKLviewer/tst_HKLviewerOSbrowserXtricorderTNCS.py",
    "$D/HKLviewer/tst_HKLviewerQtGuiXtricorderTNCS.py",
  )
  build_dir = libtbx.env.under_build("phenix_regression")
  dist_dir = libtbx.env.dist_path("phenix_regression")
  test_utils.run_tests(build_dir, dist_dir, tst_list)

if (__name__ == "__main__"):
  run()
