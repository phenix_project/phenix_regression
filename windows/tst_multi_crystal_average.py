from __future__ import print_function

# XXX This does NOT work!  Need different files, currently they are bundled into
# a .tgz file.

from phenix_regression.windows import *
from libtbx import easy_run
import os

def exercise () :
  for file_name in ["gene-5.pdb", "resolve_1_offset.mtz", "phaser_1_offset.mtz",
                    "perfect.mtz", "multi.pdb", "multi.pdb.mtz"] :
    full_path = get_wizard_test_file(file_name)
    assert (full_path is not None)
    open(file_name, "wb").write(open(full_path).read())
  eff_file = get_wizard_test_file("run_multi_new.eff")
  assert (eff_file is not None)
  stdout_lines = easy_run.fully_buffered(
    ["phenix.multi_crystal_average", eff_file]).raise_if_errors().stdout_lines
  print("\n".join(stdout_lines))
  print("OK")

if (__name__ == "__main__") :
  exercise()
