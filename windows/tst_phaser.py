from __future__ import print_function

from phenix_regression.windows import get_wizard_test_file, get_wizard_run_dir
from libtbx import easy_run
import os

def exercise () :
  seq_file = get_wizard_test_file("help_tests/test_help/seq.dat")
  sca_file = get_wizard_test_file("help_tests/test_help/native.sca")
  pdb_file = get_wizard_test_file("help_tests/test_help/coords.pdb")
  assert None not in [seq_file, sca_file, pdb_file]
  if (os.path.isfile("PHASER.1.pdb")) :
    os.remove("PHASER.1.pdb")
  args = [pdb_file, seq_file, sca_file, "model_rmsd=0.85",]
  phaser_out = easy_run.fully_buffered(
    ["phenix.phaser"]+args).raise_if_errors().stdout_lines
  success = False
  for line in phaser_out :
    if ("EXIT STATUS: SUCCESS" in line) :
      success = True
      break
  assert (success)
  assert (os.path.isfile("PHASER.1.pdb"))
  from iotbx import pdb
  pdb.input("PHASER.1.pdb").construct_hierarchy()
  print("OK")

if (__name__ == "__main__") :
  exercise()
