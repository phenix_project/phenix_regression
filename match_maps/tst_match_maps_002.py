from __future__ import division
from __future__ import print_function
import time,os
from phenix_regression.refinement import run_fmodel
import iotbx.pdb
import libtbx.load_env
from libtbx import easy_run
from iotbx import mtz
import mmtbx.f_model
from cctbx import maptbx, miller
from scitbx.array_family import flex
from libtbx.test_utils import approx_equal

def ft(mc):
  crystal_gridding = maptbx.crystal_gridding(
    unit_cell        = mc.unit_cell(),
    step             = 1.0,
    symmetry_flags   = maptbx.use_space_group_symmetry,
    space_group_info = mc.crystal_symmetry().space_group_info())
  fft_map = miller.fft_map(
    crystal_gridding     = crystal_gridding,
    fourier_coefficients = mc)
  fft_map.apply_sigma_scaling()
  return fft_map.real_map_unpadded()

def get_map_from_map_coeffs(hkl):
  mtz_obj = mtz.object(file_name=hkl)
  mc = mtz_obj.as_miller_arrays()[0]
  return ft(mc)

def get_map_values(map_data, sites_frac):
  result = flex.double()
  for sf in sites_frac:
    result.append(map_data.tricubic_interpolation(sf))
  return result

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Make sure runing with and without giving fixed data produces the same results.
  """
  prefix_A = "A_%s"%prefix
  prefix_B = "B_%s"%prefix
  A_pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/match_maps/A2.pdb",
    test=os.path.isfile)
  B_pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/match_maps/B2.pdb",
    test=os.path.isfile)
  #
  args = [A_pdb, "high_res=2 type=real r_free=0.1 label=F-obs"]
  r = run_fmodel(args = args, prefix = prefix_A)
  args = [B_pdb, "high_res=2 type=real r_free=0.1 label=F-obs"]
  r = run_fmodel(args = args, prefix = prefix_B)
  #
  cmd = " ".join([
    "phenix.match_maps",
    A_pdb,
    B_pdb,
    "output.prefix=%s"%prefix,
    "%s.mtz"%prefix_A,
    "%s.mtz"%prefix_B,
    ">%s_1.log"%prefix
  ])
  assert not easy_run.call(cmd)
  #
  map_BonA = get_map_from_map_coeffs("%s_000_BonA.mtz"%prefix)
  sites_frac_BonA_1 = iotbx.pdb.input(
    file_name="%s_000_BonA.pdb"%prefix).xray_structure_simple().sites_frac()
  map_values_BonA_in_BonA_1 = get_map_values(map_BonA, sites_frac_BonA_1)
  #
  cmd = " ".join([
    "phenix.match_maps",
    A_pdb,
    B_pdb,
    "output.prefix=%s"%prefix,
    "%s.mtz"%prefix_A,
    "%s.mtz"%prefix_B,
    ">%s_2.log"%prefix
  ])
  assert not easy_run.call(cmd)
  map_BonA = get_map_from_map_coeffs("%s_000_BonA.mtz"%prefix)
  sites_frac_BonA_2 = iotbx.pdb.input(
    file_name="%s_000_BonA.pdb"%prefix).xray_structure_simple().sites_frac()
  map_values_BonA_in_BonA_2 = get_map_values(map_BonA, sites_frac_BonA_2)
  #
  assert approx_equal(sites_frac_BonA_1, sites_frac_BonA_2)
  assert flex.linear_correlation(
    map_values_BonA_in_BonA_1,
    map_values_BonA_in_BonA_2).coefficient() > 0.99

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
