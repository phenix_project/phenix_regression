from __future__ import division
from __future__ import print_function
import time,os
import libtbx.load_env
from libtbx import easy_run
from libtbx.test_utils import approx_equal
from iotbx.data_manager import DataManager


data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="segment_and_split_map",
  test=os.path.isdir)

target_pdb=os.path.join(data_dir,"A.pdb")
rotated_pdb=os.path.join(data_dir,"A_rt.pdb")
map_poor_at_b=os.path.join(data_dir,"model_A30_B200.ccp4")
map_poor_at_a=os.path.join(data_dir,"model_A200_B30_rt.ccp4")


def run(prefix = os.path.basename(__file__).replace(".py","")):

  prefix_A = "A_%s"%prefix
  prefix_B = "B_%s"%prefix
  A_pdb = target_pdb
  B_pdb = rotated_pdb
  A_ccp4 = map_poor_at_b
  B_ccp4 = map_poor_at_a
  #
  cmd = " ".join([
    "phenix.match_maps",
    A_pdb,
    B_pdb,
    "output.prefix=%s"%prefix,
    A_ccp4,
    B_ccp4,
    ">%s.log"%prefix
  ])
  print (cmd)
  assert not easy_run.call(cmd)
  #

  model_file_name="%s_000_BonA.pdb"%prefix
  map_file_name ="BonA.mrc"
  dm = DataManager()
  mmm = dm.get_map_model_manager(model_file=model_file_name,
    map_files=map_file_name)
  mmm.set_resolution(3)
  cc=mmm.map_model_cc()
  assert approx_equal (cc,0.65,eps=0.05)
if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
