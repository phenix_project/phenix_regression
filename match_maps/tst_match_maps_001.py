from __future__ import division
from __future__ import print_function
import time,os
from phenix_regression.refinement import run_fmodel
import iotbx.pdb
import libtbx.load_env
from libtbx import easy_run
from iotbx import mtz
import mmtbx.f_model
from cctbx import maptbx, miller
from scitbx.array_family import flex

def ft(mc):
  crystal_gridding = maptbx.crystal_gridding(
    unit_cell        = mc.unit_cell(),
    step             = 1.0,
    symmetry_flags   = maptbx.use_space_group_symmetry,
    space_group_info = mc.crystal_symmetry().space_group_info())
  fft_map = miller.fft_map(
    crystal_gridding     = crystal_gridding,
    fourier_coefficients = mc)
  fft_map.apply_sigma_scaling()
  return fft_map.real_map_unpadded()

def get_map(pdb, hkl):
  mtz_obj = mtz.object(file_name=hkl)
  f_obs, flags = mtz_obj.as_miller_arrays()
  flags = flags.array(data = flags.data()==1)
  xrs = iotbx.pdb.input(file_name=pdb).xray_structure_simple()
  assert f_obs.crystal_symmetry().is_similar_symmetry(
    xrs.crystal_symmetry())
  fmodel = mmtbx.f_model.manager(
    f_obs          = f_obs,
    r_free_flags   = flags,
    xray_structure = xrs)
  fmodel.update_all_scales()
  mc = mmtbx.map_tools.electron_density_map(fmodel=fmodel).map_coefficients(
    map_type     = "2mFo-DFc",
    isotropize   = True,
    fill_missing = True)
  return ft(mc)

def get_map_from_map_coeffs(hkl):
  mtz_obj = mtz.object(file_name=hkl)
  mc = mtz_obj.as_miller_arrays()[0]
  return ft(mc), mc.crystal_symmetry()

def get_map_values(map_data, sites_frac):
  result = flex.double()
  for sf in sites_frac:
    result.append(map_data.tricubic_interpolation(sf))
  return result

def get_sites_frac(pdb_file):
  pi = iotbx.pdb.input(file_name=pdb_file)
  sites_frac = pi.xray_structure_simple().sites_frac()
  return sites_frac, pi.crystal_symmetry()

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Make sure everything converts to isotropic except selected atoms.
  """
  prefix_A = "A_%s"%prefix
  prefix_B = "B_%s"%prefix
  A_pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/match_maps/A.pdb",
    test=os.path.isfile)
  B_pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/match_maps/B.pdb",
    test=os.path.isfile)
  #
  args = [A_pdb, "high_res=1.5 type=real r_free=0.1 label=F-obs"]
  r = run_fmodel(args = args, prefix = prefix_A)
  args = [B_pdb, "high_res=3 type=real r_free=0.1 label=F-obs"]
  r = run_fmodel(args = args, prefix = prefix_B)
  #
  cmd = " ".join([
    "phenix.match_maps",
    A_pdb,
    B_pdb,
    "output.prefix=%s"%prefix,
    "%s.mtz"%prefix_A,
    "%s.mtz"%prefix_B,
    ">%s.log"%prefix
  ])
  print (cmd)
  assert not easy_run.call(cmd)
  #
  map_B = get_map(pdb=B_pdb, hkl="%s.mtz"%prefix_B)
  sites_frac_B, cs_B = get_sites_frac(pdb_file=B_pdb)
  map_values_B_in_B = get_map_values(map_B, sites_frac_B)
  #
  map_BonA, cs_BonA_1 = get_map_from_map_coeffs("%s_000_BonA.mtz"%prefix)
  sites_frac_BonA, cs_BonA_2 = get_sites_frac(pdb_file="%s_000_BonA.pdb"%prefix)
  map_values_BonA_in_BonA = get_map_values(map_BonA, sites_frac_BonA)
  #
  sites_frac_A, cs_A = get_sites_frac(pdb_file=A_pdb)
  map_values_BonA_in_A = get_map_values(map_BonA, sites_frac_A)
  #
  assert cs_BonA_1.is_similar_symmetry(cs_BonA_2)
  assert cs_BonA_1.is_similar_symmetry(cs_A)
  #
  cc = flex.linear_correlation(map_values_B_in_B,
                               map_values_BonA_in_BonA).coefficient()
  print (cc)
  assert cc > 0.95, cc
  cc = flex.linear_correlation(map_values_BonA_in_BonA,
                               map_values_BonA_in_A).coefficient()
  print (cc)
  assert cc > 0.999999, cc
  #
  diff = flex.abs(map_values_B_in_B - map_values_BonA_in_BonA)
  sel = flex.sort_permutation(diff, reverse=True)
  cc1 = flex.linear_correlation(
    map_values_B_in_B.select(sel)[:200],
    map_values_BonA_in_BonA.select(sel)[:200]).coefficient()
  print (cc1)
  assert cc1 > 0.85, cc1 # seems to be unstable
  sel = flex.sort_permutation(diff, reverse=True)
  cc2 = flex.linear_correlation(
    map_values_B_in_B.select(sel)[200:],
    map_values_BonA_in_BonA.select(sel)[200:]).coefficient()
  print (cc2)
  assert cc2 > 0.98, cc2

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
