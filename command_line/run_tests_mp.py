from __future__ import print_function
from libtbx import easy_run
import libtbx.option_parser
import libtbx.introspection
import libtbx.utils
import libtbx.load_env
import multiprocessing
import time
import sys, os
op = os.path

def get_test_names(log="MLOG"):
  done = []
  todo = []
  for node in os.listdir("."):
    if (not op.isdir(node)): continue
    if (not node.startswith("test_")): continue
    if (not op.isfile(op.join(node, node+".com"))): continue
    if (op.isfile(op.join(node, log))):
      done.append(node)
    else:
      todo.append(node)
  return libtbx.group_args(done=done, todo=todo)

def run_test(test_name):
  cmd = "cd %s; ./%s.com" % ((test_name,)*2)
  print(cmd)
  sys.stdout.flush()
  wt0 = time.time()
  os.environ["OMP_NUM_THREADS"] = "1"
  buffers = easy_run.fully_buffered(command=cmd)
  wt = time.time() - wt0
  print("%.2f" % wt, file=open("%s/wall_clock_time" % test_name, "w"))
  print("\n".join(buffers.stdout_lines), file=open("%s/MLOG" % test_name, "w"))
  for line in buffers.stderr_lines:
    if (not (line.startswith("STOP at ") and line.endswith(")"))):
      print("\n".join(
        buffers.stderr_lines), file=open("%s/MERR" % test_name, "w"))
      break

def run(args):
  command_line = (libtbx.option_parser.option_parser(
    usage="python run_tests_mp.py"
         +" [options]")
    .option(None, "--force",
      action="store_true",
      default=False,
      help="run even if MLOG exists already")
  ).process(args=args, nargs=0)
  co = command_line.options
  test_names = get_test_names()
  print("Number of tests: %3d" % (len(test_names.done) + len(test_names.todo)))
  print("          to do: %3d" % len(test_names.todo))
  if (len(test_names.todo) != 0):
    tab_wt = {}
    if (op.isfile("tab_wall_clock_times")):
      for line in open("tab_wall_clock_times").read().splitlines():
        test_name, wt = line.split()
        tab_wt[test_name] = float(wt)
    def compare_test_names(a, b):
      c = cmp(tab_wt.get(b, 100000), tab_wt.get(a, 100000))
      if (c == 0):
        c = cmp(a, b)
      return c
    test_names.todo.sort(compare_test_names)
    n_proc = libtbx.introspection.number_of_processors()
    print("Number of processors:", n_proc)
    show_times = libtbx.utils.show_times(time_start="now")
    sys.stdout.flush()
    mp_pool = multiprocessing.Pool(processes=min(n_proc, len(test_names.todo)))
    mp_pool.map(run_test, test_names.todo)
    show_times()
  sys.stdout.flush()

if (__name__ == "__main__"):
  run(args=sys.argv[1:])
