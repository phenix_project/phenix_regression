from __future__ import division, print_function
from libtbx.utils import to_str


# This is allocation only. If the test appears in failures or
# expected failures it will be marked with 'value'.
test_allocation_dict = {
  'modules/phenix_regression/amber/tst_amber_00.py' : 'NWM',
  'modules/phenix_regression/amber/tst_amber_01.py' : 'NWM/BKP/DAC',
  'modules/cctbx_project/mmtbx/hydrogens/tst_add_hydrogen_5.py' : 'rdkit on anaconda - BKP/NWM/DL',
  'modules/cctbx_project/mmtbx/hydrogens/tst_add_hydrogen_6.py' : 'rdkit on anaconda - BKP/NWM/DL',
  'modules/phenix_regression/ready_set/tst_add_hydrogens_to_fragments_01.py' : 'NWM',
  'modules/cctbx_project/mmtbx/conformation_dependent_library/tst_pH_mechanism.py' : 'NWM',

  # assign expected failures
  'modules/cctbx_project/iotbx/regression/ncs/tst_ncs_reordered_chains.py' : 'OVS',
  'modules/cctbx_project/boost_adaptbx/tests/tst_python_streambuf.py' : 'BKP',
  'modules/cctbx_project/libtbx/tst_xmlrpc_utils.py' : 'BKP',
  'modules/phenix/phenix/regression/programs/tst_table_one.py' : 'CJS',
}

# Expected failures from phenix_regression.
# Others are handled in cctbx modules in run_tests.py
expected_failures = [
  'modules/phenix_regression/amber/tst_amber_00.py',
  'modules/phenix_regression/amber/tst_amber_01.py',
  'modules/cctbx_project/mmtbx/hydrogens/tst_add_hydrogen_5.py',
  'modules/cctbx_project/mmtbx/hydrogens/tst_add_hydrogen_6.py',
  'modules/cctbx_project/mmtbx/conformation_dependent_library/tst_pH_mechanism.py'
]

unstable_tests = [
  'modules/phenix_regression/refinement/ion_placement/tst_refine_calcium_simple.py',
  'modules/phenix_regression/maps/tst_composite_omit_map.py',
]

class main:
  lf = 'run_tests_parallel_zlog'

  def __init__(self):
    with open(self.lf, 'rb') as f:
      lines = to_str(f.read())
    self.failures = set()
    self.happened_expected_fails = set()
    self.warnings = set()
    for line in lines.splitlines():
      if line.find('[FAIL]')>-1 or line.find('[EXPECTED FAIL]')>-1 or line.find('[WARNING]')>-1:
        splitline = line.split('modules')
        head, tail = splitline[0], ''.join(splitline[1:])
        path = 'modules%s' % tail.split('"')[0]
        if path.find('[FAIL]')>-1: path = path.split()[0]
        if path.find('[EXPECTED FAIL]')>-1: path = path.split()[0]
        if path.find('[WARNING]')>-1: path = path.split()[0]
        if line.find('[FAIL]')>-1:
          if path not in self.failures: self.failures.add(path)
        if line.find('[EXPECTED FAIL]')>-1:
          if path not in self.happened_expected_fails: self.happened_expected_fails.add(path)
        if line.find('[WARNING]')>-1:
          if path not in self.warnings: self.warnings.add(path)
    print('='*79)

    def _print_test_list(test_set):
      for test in sorted(test_set):
        if test in test_allocation_dict:
          msg = '--> ' + test_allocation_dict[test]
          print(test, msg)
        else:
          print(test)


    print('Failures\n')
    _print_test_list(self.failures)

    # --------------------------------------
    print('\ntotal: ', len(self.failures)) # don't change this line; otherwise cronscript won't work!!!!
    # --------------------------------------

    fails_no_unstable = set(list(self.failures)+list(self.happened_expected_fails))
    fixed_tests = set(expected_failures) - fails_no_unstable

    if fixed_tests:
      print('\nThe following appear to be fixed. Remove from list.')
      _print_test_list(fixed_tests)

    print()
    print('='*79)
    print('Expected failures\n')
    _print_test_list(self.happened_expected_fails)

    print('='*79)
    print('List of warnings - Please check if the warning is actually a hidden failure\n')
    for warn in self.warnings:
      print(warn)
    print('='*79)
    return

if __name__ == '__main__':
  main()
