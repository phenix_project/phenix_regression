#! /bin/csh -fe
if (-f 1NSF.pdb || -f nsf_d2_peak.sca || -f commands || -f test_log) then
  echo
  echo "Please run this command in a new, empty directory."
  echo
  exit 1
endif
set PHENIX_REGRESSION_DIR="`libtbx.find_in_repositories phenix_regression`"
set verbose
cp "$PHENIX_REGRESSION_DIR/test_apps/1NSF.pdb" .
cp "$PHENIX_REGRESSION_DIR/test_apps/nsf_d2_peak.sca" .
cp "$PHENIX_REGRESSION_DIR/test_apps/commands" .
./commands |& tee test_log
phenix_regression.find_errors_and_warnings test_log
