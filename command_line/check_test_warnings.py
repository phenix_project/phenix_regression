import os, sys
from libtbx import easy_run
from phenix_regression.command_line import find_errors_and_warnings

lf = 'run_tests_parallel_zlog'

def main(file_name=None):
  if file_name is None: file_name=lf
  print('scanning', file_name)
  f=open(file_name, 'r')
  lines=f.read()
  del f
  done=[]
  for line in lines.splitlines():
    if line.find('[WARNING]')>-1:
      print(line)
      test = line.split()[1].replace('"','')
      if test in done: continue
      done.append(test)
      log='check_test_warnings_%s.log.%02d' % (os.path.basename(test),
                                               len(done))
      cmd = 'python %s > %s' % (test, log)
      print('\n ~> %s\n' % cmd)
      easy_run.go(cmd)
      find_errors_and_warnings.run([log])
      print('>'*80)

if __name__ == '__main__':
  main(*tuple(sys.argv[1:]))
