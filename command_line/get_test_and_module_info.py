from __future__ import print_function

import os,sys
import libtbx.load_env
from libtbx.utils import Sorry

"""
   Purpose:  Run regression tests with the python trace command on to
   create a database of all phenix / cctbx tests and which modules
   are actually run by each test.

   Output of this script is the file test_and_module_info.pkl.

   Copy this file to modules/phenix_regression/misc/test_and_module_info.pkl
   and check it in when done.

   This database file is read by phenix.find_program (FindProgram) and
   can be used to find tests for a module.

   TT  2020-04-02
"""

def list_all_modules(dir_name = None, output_file_name = None,
        required_suffix_list = ['.py','.csh'],
        skip_prefix_list = ['tst','test'],
        directory_names_to_skip = ['examples','regression','tests','dox'],
        require_text_from_this_list = None,
        log = sys.stdout):

  print("Finding all modules in directory '%s'" %(dir_name), file = log)

  if require_text_from_this_list  and\
       require_text_from_this_list[0].upper() not in ["ALL","ANY"]:
    print("Only looking at directories containing any of the following: %s" %(
      " ".join(require_text_from_this_list)), file = log)
  else:
    require_text_from_this_list = None

  all_modules = []
  for sub_dir, directories, filenames in os.walk(dir_name):
    path_names = sub_dir.split(os.path.sep)
    skip_it = False
    for pn in path_names:
      if pn in directory_names_to_skip: skip_it = True
    if require_text_from_this_list:
      have_required_text = False
      for pn in path_names:
        if pn in require_text_from_this_list:
          have_required_text = True
      if not have_required_text:
        skip_it = True
    if skip_it: continue
    for filename in filenames:
      ok_filename = True
      skip_due_to_prefix = False
      has_required_suffix = False
      if skip_prefix_list:
        for skip in skip_prefix_list:
          if filename.startswith(skip): skip_due_to_prefix = True
      if required_suffix_list:
        for suff in required_suffix_list:
          if filename.endswith(suff):  has_required_suffix = True
      else:
        has_required_suffix = True
      if skip_due_to_prefix or (not has_required_suffix):
        continue

      full_file_name = os.path.join(dir_name,sub_dir,filename)
      for module_id in get_module_list(full_file_name):
        name_with_path = "%s::%s" %(full_file_name,module_id)
        text = name_with_path.split("%s%s" %("modules",os.path.sep))[1]
        all_modules.append(text)
  if output_file_name:
    f = open(output_file_name,'w')
    for m in all_modules:
      print(m, file = f)
    f.close()
    print("Wrote %s module names to %s" %(
      len(all_modules), output_file_name), file = log)
  return all_modules


def get_module_list(file_name):
  module_list = []
  module_name = None
  method_name = None
  class_name = None
  short_file = ".".join(os.path.split(file_name)[-1].split(".")[:-1])
  module_list.append("%s::%s" %(short_file,'<module>'))
  for line in open(file_name).readlines():
    spl = line.replace("("," ").split()
    if len(spl) < 2: continue
    if line.startswith("class"):
      method_name = spl[1]
      module_list.append("%s::%s" %(short_file,method_name))
      class_name = method_name
    elif line.startswith("def "): # method
      method_name = spl[1]
      module_list.append("%s::%s" %(short_file,method_name))
    elif line.strip().startswith("def "): # method of class
      method_name = spl[1]
      module_list.append("%s::%s:.%s" %(short_file,class_name,method_name))
  return module_list


def run(args):
  if not args or "--help" in args or "help" in args:
    print("phenix_regression.get_test_and_module_info [nproc=<nproc>] [output_pickle_file=<output_pickle_file>] [start_test=<start_test>] [max_tests_per_module=<max_tests_per_module>] [max_tests=<max_tests>] [tests_to_skip=<tests_to_skip>]")
    return

  new_args=[]
  nproc=1
  output_pickle_file='test_and_module_info.pkl'
  max_tests_per_module=10
  max_tests=None
  start_test=None
  tests_to_skip=['tst_scheduling']
  python_keyword_text=" -m trace --listfuncs -C . "
  for arg in args:
    if arg.startswith("nproc="): # Number of processors
      nproc=int(arg.replace("nproc=",""))
    elif arg.startswith("output_pickle_file="): # output file
      output_pickle_file=arg.replace("output_pickle_file=","")
    elif arg.startswith("max_tests_per_module="): #tests to report per module
      max_tests_per_module=int(arg.replace("max_tests_per_module=",""))
    elif arg.startswith("max_tests="):  #maximum tests to run
      max_tests=int(arg.replace("max_tests=",""))
    elif arg.startswith("start_test="):  #maximum tests to run
      start_test=int(arg.replace("start_test=",""))
    elif arg.startswith("tests_to_skip="):  #tests to not run
      tests_to_skip=arg.replace("tests_to_skip=","").replace(","," ")
      tests_to_skip=tests_to_skip.split()
    else:
      new_args.append(args)
  args=[ 'nproc=%s' %(nproc),
         ]+new_args

  print("Getting tests and python modules called with ")
  print("nproc=%s output_pickle_file=%s max_tests=%s start_test=%s tests_to_skip=%s max_tests_per_module=%s and args %s" %(
    nproc,output_pickle_file,max_tests,start_test,tests_to_skip,max_tests_per_module,args))

  from phenix_regression.command_line.test_all_parallel import run as test_cctbx
  from phenix_regression.wizards.test_all_parallel \
      import run as test_wizards

  cctbx_dir='cctbx_dir'
  wizards_dir='wizards_dir'
  if os.path.isdir(cctbx_dir) or os.path.isdir(wizards_dir):
    raise Sorry("Please run in a directory that does not have cctbx_dir or "+
       "wizards_dir subdirectories")

  os.mkdir(cctbx_dir)
  os.mkdir(wizards_dir)

  results_file_name='run_tests_parallel_zlog'

  home_dir=os.getcwd()

  os.chdir(cctbx_dir)
  print("Running cctbx tests in %s" %(os.getcwd()))
  try:
    test_cctbx(args,
     max_tests=max_tests, # maximum number to run
     start_test=start_test, # start_test,
     tests_to_skip=tests_to_skip,
     python_keyword_text=python_keyword_text,)
  except Exception as e:
     print(e)
  print("Done running cctbx tests")
  cctbx_results_file=os.path.join(os.getcwd(),results_file_name)

  if not os.path.isfile(cctbx_results_file):
    raise Sorry("The file %s was not created?")
  os.chdir(home_dir)
  os.chdir(wizards_dir)
  print("Running wizards tests in %s" %(os.getcwd()))
  try:
    test_wizards(args,
     max_tests=max_tests,
     tests_to_skip=tests_to_skip,
     python_keyword_text=python_keyword_text,)
  except Exception as e:
     print(e)
  print("Done running wizards tests")
  wizards_results_file=os.path.join(os.getcwd(),results_file_name)
  if not os.path.isfile(wizards_results_file):
    raise Sorry("The file %s was not created?")
  os.chdir(home_dir)

  print("Appending results from wizard tests in %s to %s" %(
    wizards_results_file,cctbx_results_file))
  f=open(cctbx_results_file,'a')
  print(open(wizards_results_file).read(), file=f)
  f.close()
  print("Running read_trace_log to analyze run information")

  from phenix_regression.command_line.read_trace_log import \
     run as read_trace_log

  args= ['%s' %(cctbx_results_file),
       'output_pickle_file=%s' %(output_pickle_file),
       'max_tests_per_module=%s' %(max_tests_per_module),
       ]
  read_trace_log(args)

  print(80*"=")
  print("All done... ")
  print(" copy %s to modules/phenix_regression/misc/test_and_module_info.pkl" %(
           output_pickle_file))

if __name__=="__main__":
  run(sys.argv[1:])
