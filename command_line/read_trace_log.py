from __future__ import print_function

import os,sys
from operator import itemgetter
from libtbx import group_args
from libtbx.utils import Sorry

"""
   Purpose:  read file created by the command:
   phenix_regression.test_all_parallel trace nproc=64 > ~/t48_trace.log
   to extract test names and the functions that they access

   TT  2020-03-31

   The output has groups of lines like this:

   libtbx.python  -m trace --listfuncs -C .  "/net/anaconda/raid1/terwill/misc/PHENIX/modules/cctbx_project/omptbx/tst.py" [WARNING] 6.0s
   ...
   functions called:
   ...
filename: /net/anaconda/raid1/terwill/misc/PHENIX/build/../conda_base/lib/python2.7/UserDict.py, modulename: UserDict, funcname: UserDict.__contains__

filename: /net/anaconda/raid1/terwill/misc/PHENIX/modules/cctbx_project/omptbx/__init__.py, modulename: __init__, funcname: nested
"""

def run(args):

  # Get the data
  print("Getting data on modules accessed by each test...")
  data_info=get_data(args)
  if not data_info: return
  entry_list=data_info.entry_list
  module_entry_dict=data_info.module_entry_dict
  max_tests_per_module=data_info.max_tests_per_module
  output_pickle_file=data_info.output_pickle_file
  #  each entry is one test
  #   entry.modules_called : dict keyed by module called
  #   entry.test_path,len(entry.modules_called),entry.time

  print("Total of %s tests analyzed" %(len(entry_list)))

  print("Getting list of modules and all tests that run them...")
  entry_by_module_id_dict=get_entry_by_module_id_dict(entry_list)

  print("Total of %s modules have at least one test...(max_tests_per_module=%s)" %(
      len(entry_by_module_id_dict.keys()),max_tests_per_module))

  # Write out text or pkl file with all modules and their tests (sort
  #  by time for tests and number of modules accessed by each test

  # Rank tests by time (short to long) and report up to top max_tests_per_module

  print("Setting up module test info...")

  from phenix.programs.get_module_test_info import module_tests
  module_test_info=module_tests()

  module_id_list=list(entry_by_module_id_dict.keys())
  module_id_list.sort()
  for module_id in module_id_list:
    module_test_info.add_module(
      module_id=module_id,
      module_entry=module_entry_dict[module_id],
      entry_list=get_top_tests(
       module_entry_list=entry_by_module_id_dict[module_id],
       max_tests_per_module=max_tests_per_module)
    )

  # Get rid of unnecessary information
  for entry in entry_list:
    del entry.modules_called
  module_test_info.show_summary()

  if output_pickle_file:
    write_pickle_file(output_pickle_file,result=module_test_info)


def get_top_tests(module_entry_list=None,max_tests_per_module=None):
  sort_list=[]
  entries_used=[]
  for entry in module_entry_list:
    if not entry in entries_used:
      t = 9999
      if entry.time is not None:
        t = entry.time
      sort_list.append([t, entry])
      entries_used.append(entry)
  sort_list.sort(key=itemgetter(0)) # short to long time
  top_entry_list=[]
  for t,entry in sort_list:
    if (max_tests_per_module is not None) and \
        len(top_entry_list)>=max_tests_per_module:
      break
    top_entry_list.append(entry)
  return top_entry_list

def get_entry_by_module_id_dict(entry_list):
  # ID which tests access which modules
  entry_by_module_id_dict={}
  for entry in entry_list:
    keys=entry.modules_called.keys()
    for key in keys:
      module_entry=entry.modules_called[key]
      if not module_entry.module_id in entry_by_module_id_dict.keys():
        entry_by_module_id_dict[module_entry.module_id]=[]
      if not entry in entry_by_module_id_dict[module_entry.module_id]:
        entry_by_module_id_dict[module_entry.module_id].append(entry)

  return entry_by_module_id_dict

def get_data(args):
  if not args or not os.path.isfile(args[0]):
    print("phenix.python read_trace_log.py <file_name> [output_pickle_file=<output_pickle_file.pkl>] [max_entries=<max_entries>] [max_tests_per_module=<max_tests_per_module>]")
    return

  file_name=args[0]
  if not os.path.isfile(file_name):
    raise Sorry("Need input file name with output from all parallel tests")

  args=args[1:]
  max_entries=None
  max_tests_per_module=None
  output_pickle_file=None
  for arg in args:
    text="max_entries="
    if arg.find(text)>-1:
       max_entries=int(arg.split(text)[1])
    text="max_tests_per_module="
    if arg.find(text)>-1:
       max_tests_per_module=int(arg.split(text)[1])
    text="output_pickle_file="
    if arg.find(text)>-1:
       output_pickle_file=arg.split(text)[1]

  # list of all tests and what modules they access (entry= one test)
  entry_list,module_entry_dict=get_entry_list(file_name,max_entries=max_entries)
  return group_args(
    entry_list=entry_list,
    module_entry_dict=module_entry_dict,
    max_tests_per_module=max_tests_per_module,
    max_entries=max_entries,
    output_pickle_file=output_pickle_file
   )

def write_pickle_file(output_pickle_file,result=None):
    from libtbx import easy_pickle
    easy_pickle.dump(output_pickle_file,result)
    print("Wrote pkl file: %s" %(output_pickle_file))

def get_entry_list(file_name,max_entries=None):
  if file_name.endswith(".pkl"):
    from libtbx import easy_pickle
    return easy_pickle.load(file_name)
  entry_list=[]
  module_entry_dict={}
  value_dict={}
  working_on_group=False
  for line in open(file_name).readlines():
    line=line.strip()
    if line.startswith("libtbx.python"):
      test_path=get_test_path(line,require_python=True) # cctbx_project/omptbx/tst.py
      if value_dict: # save what we have
        entry=get_entry_from_value_dict(value_dict)
        if entry:
          entry_list.append(get_entry_from_value_dict(value_dict))
          if max_entries and len(entry_list)>=max_entries:  # all done
            working_on_group=False
            value_dict={}
            break
      value_dict={}  # new value dict
      if not test_path:
        working_on_group=False
        continue  # skip this whole group note empty value_dict
      # Set up new value dict
      value_dict['modules_called']={}
      value_dict['time']=get_time(line)  # 6.0
      value_dict['test_path']=test_path
      working_on_group=True
    elif working_on_group and line.startswith("filename:"):
      module_path=get_module_path(line,require_phenix_or_cctbx=True)
      if not module_path: continue
      module_entry=group_args(
        group_args_type='module_entry',
        module_path=module_path,
        module_name=get_module_name(line),
        function_name=get_function_name(line),
        module_id=None
       )
      module_entry.module_id=get_module_id(module_entry)
      if not module_entry.module_id in \
          value_dict['modules_called'].keys():
        value_dict['modules_called'][module_entry.module_id]=module_entry
      if not module_entry.module_id in module_entry_dict.keys():
        module_entry_dict[module_entry.module_id]=module_entry
  if value_dict:
    entry=get_entry_from_value_dict(value_dict)
    if entry:
      entry_list.append(get_entry_from_value_dict(value_dict))
  return entry_list,module_entry_dict

def get_entry_from_value_dict(value_dict):
  entry=group_args(
    group_args_type='entry',
    test_path=value_dict['test_path'],
    modules_called=value_dict['modules_called'],
    time=value_dict['time'],
     )
  if len(entry.modules_called)>0:
    return entry
  else:
    return None


def get_time(line):
  time=None
  if line.endswith("s"):
    line=line.replace("s","")
    try:
      time=float(line.split()[-1])
    except Exception as e:
      time=None
  return time

def get_test_path(line,require_python=None,split_at_modules=True):

  # libtbx.python  -m trace --listfuncs -C .  "/net/anaconda/raid1/terwill/misc/PHENIX/modules/cctbx_project/omptbx/tst.py" [WARNING] 6.0s
  spl=line.split('"')
  if len(spl)!=3:
    return None
  text=spl[1]

  python_name=line.split()[0]
  if require_python and python_name != 'libtbx.python':
    return None

  if split_at_modules:
    m="%s%s%s" %(os.path.sep,'modules',os.path.sep)
    spl=text.split(m)
    if len(spl)==2:
      text=spl[1]
    else:
      return None
  return text.strip()

def get_module_path(line,require_phenix_or_cctbx=None,skip_boost=True,
   skip_tst=True,split_at_modules=True):
  #  filename: /net/anaconda/raid1/terwill/misc/PHENIX/build/../conda_base/lib/python2.7/UserDict.py,
  if line.find("filename:")>-1 and line.find(",")>-1:
    text=line.split("filename:")[-1].split(",")[0]
  else:
    return None

  if require_phenix_or_cctbx:
    if (not line.find("phenix")>-1) and (not line.find("cctbx_project")>-1):
      return None

  if skip_boost:
    if line.find("boost")>-1 and line.find("python")>-1:
      return None

  if skip_tst:
    if line and os.path.split(line)[-1].startswith("tst_"):
      return None
  if split_at_modules:
    text=text.strip()
    m="%s%s%s" %(os.path.sep,'modules',os.path.sep)
    spl=text.split(m)
    if len(spl)==2:
      text=spl[1]
    else:
      return None

  return text.strip()

def get_function_name(line):
  if line.find("funcname:")>-1 and line.find(",")>-1:
    text=line.split("funcname:")[-1].split(",")[0]  # funcname: __init__,
    return text.strip()
  else:
    return None

def get_module_name(line):
  if line.find("modulename:")>-1 and line.find(",")>-1:
    text=line.split("modulename:")[-1].split(",")[0]  # modulename: __init__,
    return text.strip()
  else:
    return None

def get_module_id(entry):
  module_id="%s::%s::%s" %(
    entry.module_path,entry.module_name,entry.function_name)
  return module_id

if __name__=="__main__":
  run(sys.argv[1:])
