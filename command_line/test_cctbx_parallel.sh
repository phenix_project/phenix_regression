#!/bin/sh

CCTBX_DIST=`libtbx.find_in_repositories cctbx_project`
PHENIX_DIST=`libtbx.find_in_repositories phenix`
REGRESSION=`libtbx.find_in_repositories phenix_regression`

libtbx.run_tests_parallel \
  module=libtbx \
  module=boost_adaptbx \
  module=scitbx \
  module=cctbx \
  module=iotbx \
  module=mmtbx \
  directory="${CCTBX_DIST}/omptbx" \
  directory="${REGRESSION}/libtbx" \
  directory="${REGRESSION}/scitbx" \
  directory="${REGRESSION}/cctbx" \
  directory="${REGRESSION}/iotbx" \
  directory="${REGRESSION}/mmtbx" \
  directory="${REGRESSION}/misc" \
  directory="${REGRESSION}/tardy_action" \
  directory="${REGRESSION}/rna_puckers" \
  directory="${REGRESSION}/real_space_correlation" \
  directory="${REGRESSION}/fo_minus_fo_map" \
  directory="${REGRESSION}/maps" \
  directory="${REGRESSION}/bulk_solvent_and_scaling" \
  directory="${REGRESSION}/extract_box_around_model_and_map" \
  directory="${REGRESSION}/map_rot_trans" \
  directory="${REGRESSION}/cif_as_mtz" \
  directory="${REGRESSION}/fmodel_cmd" \
  directory="${REGRESSION}/phenix_masks" \
  directory="${REGRESSION}/map_value_at_point" \
  directory="${REGRESSION}/reciprocal_space_arrays" \
  directory="${REGRESSION}/fake_f_obs" \
  directory="${REGRESSION}/double_step_filtration" \
  $@
