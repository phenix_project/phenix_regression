#!/bin/sh -e
CCTBX_DIST=`libtbx.find_in_repositories cctbx_project`
PHENIX_DIST=`libtbx.find_in_repositories phenix`
REGRESSION=`libtbx.find_in_repositories phenix_regression`
# MPTBX=`libtbx.find_in_repositories mptbx`

#script="${REGRESSION}/refinement/ion_placement/tst_refine_calcium_simply.py" \
#script="${REGRESSION}/refinement/ion_placement/tst_refine_cadmium_chloride.py" \
# module=libtbx \
# module=boost_adaptbx \
# module=scitbx \
# module=cctbx \
# module=iotbx \
# module=mmtbx \

# temp remove from testing
#  script="${REGRESSION}/ligand_pipeline/tst_main.py" \
#  script="${REGRESSION}/ligand_pipeline/tst_phaser_multiple_models.py" \

# removed on 6/29 by bkpoon
# failing due to change in how NCS groups are stored
# error occurs in mmtbx/ncs/ligands.py around line 121 where a list of NCS
# groups is created
#  script="${REGRESSION}/ligand_pipeline/tst_pipeline_ligand_ncs.py" \

libtbx.run_tests_parallel \
  script="${REGRESSION}/ligand_pipeline/tst_select_model.py" \
  script="${REGRESSION}/ligand_pipeline/tst_fo_minus_fo.py" \
  script="${REGRESSION}/ligand_pipeline/tst_no_ligand.py" \
  script="${REGRESSION}/ligand_pipeline/tst_modules.py" \
  directory="${REGRESSION}/libtbx" \
  directory="${REGRESSION}/scitbx" \
  directory="${REGRESSION}/cctbx" \
  directory="${REGRESSION}/iotbx" \
  directory="${REGRESSION}/mmtbx" \
  directory="${REGRESSION}/misc" \
  directory="${REGRESSION}/validation" \
  directory="${REGRESSION}/model_vs_data" \
  directory="${REGRESSION}/ligand" \
  directory="${REGRESSION}/xtriage" \
  directory="${REGRESSION}/gui" \
  nproc=Auto
exit $?
