#!/bin/sh

CCTBX_DIST=`libtbx.find_in_repositories cctbx_project`
PHENIX_DIST=`libtbx.find_in_repositories phenix`
REGRESSION=`libtbx.find_in_repositories phenix_regression`

# nuke old .pyc files to ensure that deleted modules aren't still being
# imported by mistake
find ${CCTBX_DIST} -name "*.pyc" | xargs rm -f
find ${PHENIX_DIST} -name "*.pyc" | xargs rm -f
find ${REGRESSION} -name "*.pyc" | xargs rm -f

libtbx.run_tests_parallel directory="${REGRESSION}/ligand_pipeline" $@
