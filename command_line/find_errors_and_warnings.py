from __future__ import print_function

def is_error_or_warning(l, ll):
  sw = l.startswith
  ew = l.endswith
  lf = l.find
  if (lf(" Sorry - No solution with all components") >= 0): return False
  if (lf("ignore_errors_in_subprocess")): return False

  if (lf("Sorry") >= 0):
    return True
  if (sw("Traceback")): return True
  if (ew("Undefined variable.")): return True
  if (sw("INFO LOW VALUE:")): return True
  if (sw("INFO HIGH VALUE:")): return True
  if (sw("python $CCTBX_DIST/cctbx/web/replay.py")): return False
  if (sw("libtbx.python $CCTBX_DIST/cctbx/web/replay.py")): return False
  if (sw("libtbx.python $CCTBX_DIST/web/replay.py")): return False
  if (sw("customize_source([")): return False
  if (ew("Ignoring phenix_regression.find_errors_and_warnings ")):
    return False
  if (lf("is not recognized as an internal or external command") >= 0):
    return True
  if (lf("can't open file") >= 0): return True
  llf = ll.find
  if (llf("abort") >= 0): return True
  if (llf("floating exception") >= 0): return True
  if (lf("Errno") >= 0): return True
  if (lf("No such file or directory") >= 0): return True
  if (lf("Permission denied.") >= 0): return True
  if (lf("Command not found.") >= 0): return True
  if (lf("Undefined variable.") >= 0): return True
  if (lf("'NoneType' object has no attribute") >= 0): return True
  if (llf("fault") >= 0):
    if (l == "Ignoring boost_adaptbx.segmentation_fault "): return False
    if (lf("inflating: cci_apps_sources/boost_adaptbx/command_line/"
             "segmentation_fault.py") >= 0):
      return False
    if (ll.find("default") < 0):
      return True
  if (1): # solve_resolve ifort-related errors
    if (    lf("\000") >= 0
        and (   lf("solve") >= 0
             or lf("apple") >= 0
             or lf("dylib") >= 0
             or lf("dynamic") >= 0
             or lf("symbol ___") >= 0)): return False
    if (lf("/usr/bin/ld: warning") >= 0 and lf("solve") >= 0): return False
  i_error = llf("error")
  if (i_error >= 0):
    if (l == "Type error for _space_group_IT_number: 'one hundred and eighty' "\
             "is not interpretable as type 'numb'"):
      return False
    if (l == "grep -i error"): return False
    if (l == "grep -i error web_replay_log"): return False
    if (l == "Ignoring scitbx.generate_error_h "): return False
    if (l == "tst_error.cpp"): return False
    if (l == "error.cpp"): return False
    if (l == "errors.cpp"): return False
    if (l == "Errors.cc"): return False
    if (l == "error_code.cpp"): return False
    if (l == "get_model_error.cpp"): return False
    if (ew("multiprocessing import error:"
             " This platform lacks a functioning sem_open implementation")):
      return False
    if (ew(" ... ok")): return False
    if (sw("Fatal Python error: PyThreadState_Get: no current thread")):
      return False
    if (sw("  cctbx Error: Brick is not available")): return False
    if (lf("add_random_error_to_amplitudes_percent") >= 0): return False
    if (sw("lbfgs error: Line search failed:")): return False
    if (sw("    phase_errors                   = ")): return False
    if (sw("mean_weighted_phase_error: ")): return False
    if (lf("ML estimate for coordinates error: ") >= 0): return False
    if (lf('Error           :') >-1): return False
    if (lf("This is not a fatal error, but if we can't recalculate the reported")>-1): return False
    if (lf("coordinate error (max.-lik. estimate):") >= 0):
      return False
    if (sw("| Input coordinate error | ")): return False
    if (lf("Relative error") >=0): return False
    if (lf("correlated errors") >=0): return False
    if (lf("have errors") >=0): return False
    if (lf("some errors") >=0): return False
    if (lf("errors that") >=0): return False
    if (lf("Errors (all normalized") >=0): return False
    if (lf(", errors") >=0): return False
    if (lf("very_high_error") >=0): return False
    if (lf("Error estimate") >=0): return False
    if (lf("estimated error") >=0): return False
    if (lf("model error") >=0): return False
    if (lf("model error") >=0): return False
    if (lf("error model") >=0): return False
    if (lf("RMS error") >=0): return False
    if (lf(" error free ") >= 0): return False
    if (lf(" Error Norm:") >= 0): return False
    if (lf("\tError Norm ") >= 0): return False
    if (lf("max relative error ") >= 0): return False
    if (lf("phenix.get_patterson_skew: Get the skew and anomalous error ratio for a datafile") >= 0): return False
    if (llf("phase error") >= 0): return False
    if (llf("coordinate error") >= 0): return False
    if (lf("ignore_errors False") >= 0): return False
    if (llf("start_xyz_error") >= 0): return False
    if (lf("PhaserError.cc") >= 0): return False
    if (lf("/PhaserError.") >= 0): return False
    if (lf("\\PhaserError.") >= 0): return False
    if (lf("Warning: Patterson Pathology in NMOL") >= 0): return False
    if (lf("measurement error") >=0): return False
    if (lf("EXPERIMENTAL ERROR") >=0): return False
    if (lf("observational error") >=0): return False
    if (l == "\tERROR: different number of elements"): return False
    if (l == "Test result 'atoms_count': ERROR: '1' == '0'"): return False
    if (lf("range        work  test        error factor") >= 0): return False
    if (lf("ing: cci_apps_") >= 0): return False
    if (ew("test_find_coordinate_error.py")): return False
    if (lf("/usr/bin/ld: warning suggest use") >= 0): return False
    if (lf("resolve/get_model_error") >= 0): return False
    if (sw("ftsyn=") and lf("fable/test") >= 0): return False
    if (sw("ftsem=") and lf("fable/test") >= 0): return False
    if (lf("|  includes some error-checking") >= 0): return False
    if (sw("lib ") and lf("get_model_error.obj") >= 0): return False
    if (sw("cl ") and lf("get_model_error.cpp") >= 0): return False
    if (i_error == 0): return True
    c = ll[i_error-1]
    if (c != "/" and c != "\\"): return True
  if (llf("warn") >= 0):
    if (l == "Warning: case-sensitive match failure for value 'Monoclinic' for "\
             "'_space_group_crystal_system'"):
      return False
    if (l == "  Reduce warnings"): return False
    if (sw("**  WARNING")): return False
    if (lf("This warning is")): return False
    if (sw("WARNING: atom") and lf(" will be treated as ") >= 0): return False
    if (lf("symmetry_safety_check=warning ") >= 0): return False
    if (sw("Warning: lines: ") and lf(" data : ") >= 0): return False
    if (l == "LINK : warning LNK4089: all references to"
             + " 'MSVCP71.dll' discarded by /OPT:REF"): return False
    if (sw("fable.read --warnings ")): return False
    if (sw("Warning level: ")): return False
    if (sw("Warning: ADP tensor is incompatible")): return False
    if (sw("ifort -warn all ")): return False
    if (sw("f90 -std90 -warn unused")): return False
    if (sw("f77 -fullwarn ")): return False
    if (sw("f90 -fullwarn ")): return False
    if (sw("f77 -old_f77 -warn ")): return False
    if (    lf("): warning #") >= 0
        and lf("antlr3/src/antlr3baserecognizer.c") >= 0
        and lf("pointless comparison of unsigned integer with zero") >= 0):
      return False
    if (lf("f_model warning") >= 0): return False
    if (lf("ar: Warning: creating ") >= 0): return False
    if (lf("warning LNK4221: no public symbols found;") >= 0): return False
    if (lf("libm.so is not used for resolving any symbol.") >= 0): return False
    if (lf("Warning: very small nonbonded interaction distances.") >= 0):
      return False
    if (lf("WARNING: Shortness of bond") >= 0): return False
    if (sw("Warning: Different bond value ")): return False
    if (llf("overlay_distl.py.reserve") >= 0): return False
    if (lf("not in HETATM Connection Database. Hydrogens not added.") >= 0):
      return False
    if (sw("WARNING:   450 CIT O4 B H atom too close to    450 CIT C5 B by ")):
      return False
    if (lf(" appear unbonded") >= 0): return False
    if (lf(" will be treated as hydrogen") >= 0): return False
    if (l == "scons: warning: you do not seem to have the"
             " pywin32 extensions installed;"): return False
    if (l == "  WARNING: Final number of reflections for first resolution"
             " zone is greater"): return False
    if (l == "| *** WARNING: more than 30 % of atoms with small"
             " occupancy (< 0.1)       *** |"): return False
    if (l.find(
      r"cbflib_0.7.8.1_sauter_nounistd\src\cbf.c(6672) : warning C4700") >= 0):
        return False
    if (lf("ing: cci_apps_") >= 0): return False
    if (l.replace(" ","") == "##WARNING:##"): return False
    if (1): # solve_resolve ifort-related errors
      if (lf("/usr/bin/ld: warning suggest use") >= 0): return False
      if (sw("/usr/bin/ld: warning -L: directory name ")): return False
    if (sw("scons: warning: Support for pre-2.4 Python")): return False
    if (    lf("scons_subprocess.py") >= 0
        and lf("sys.maxint will return positive values") >= 0): return False
    if (lf("resolve/warn_of_bias") >= 0): return False
    if (lf("WARNING: No SIGFP input data--output SIGFP column") >= 0):
      return False
    if (    lf("warning #266: function declared implicitly") >= 0
        and (lf("ccp4io") >= 0 or lf("ccp4lib_cci")) >= 0):
      return False
    if (    lf("ld: warning -L: directory name (") >= 0
        and lf(") does not exist") >= 0):
      return False
    if (lf("complex_to_complex_") >= 0 and lf("warning: '<anonymous>'") >= 0):
      return False
    if (    (lf("cifParser.h") >= 0 or lf("cifLexer.h") >= 0)
        and lf('incompatible redefinition of macro "VERSION"') >= 0):
      return False
    if (    (lf("cifParser.cpp") >= 0 or lf("cifLexer.cpp") >= 0)
        and lf("type qualifier is meaningless on cast type") >= 0):
      return False
    if (lf("warning: the use of `mktemp' is dangerous,"
           " better use `mkstemp'") >= 0):
      return False
    if (lf(".obj : warning LNK4221:"
           " This object file does not define any previously"
           " undefined public symbols") >= 0):
      if (   lf("map_interp.obj") >= 0
          or lf("derivs.obj") >= 0
          or lf("container_map.obj") >= 0):
        return False
    if (    lf("array subscript is above array bounds") >= 0
        and lf("fable/fem/utils/string.hpp") >= 0):
      return False
    return True
  if (llf("fail") >= 0):
    if (lf("next search fails") >=0): return False
    if (lf("Solution stored for possible reversion if next search fails") >=0): return False
    if (lf(", failures: 0, unknowns: 0") >= 0): return False
    if (lf("Convergence failure! (Divergence)") >= 0): return False
    if (lf("Epic FAIL: failed to fix rama outlier") >=0 ): return False
    if (lf("Failed to converge internal coordinates to cartesian") >= 0):
      return False
    if (sw("SSM alignment failed")): return False
    if (sw("skip_remainder_on_failures")): return False
    if (l == "Automatic NCS search failed:"): return False
    if (lf("ing: cci_apps_") >= 0): return False
    return True
  return False

def run(args):
  print()
  import os
  op = os.path
  file_names = args
  assert len(file_names) != 0
  n_found = 0
  for file_name in file_names:
    print("%s:" % file_name)
    if (not op.isfile(file_name)):
      print("  LOG FILE DOES NOT EXIST!")
    else:
      skipping_shown = set()
      for i_line,l in enumerate(open(file_name).read().splitlines()):
        ll = l.lower()
        if (is_error_or_warning(l, ll)):
          print("%s(%d): %s" % (file_name, i_line+1, l))
          n_found += 1
        else:
          lf = l.find
          llf = ll.find
          lsw = l.startswith
          if (   (lf("INFO") >= 0
                    and l != "  INFO: Number of resolution zones reset to 1.")
              or lsw("Total time tests:")
              or lsw("JOB wall clock time:")):
            print(l)
          elif (llf("skipping") >= 0 and (
            lf("skipping Hydrogenise ...") < 0 and
            lf("skipping optimization") < 0)):
            if (l not in skipping_shown):
              print(l)
              skipping_shown.add(l)
    print()
  import libtbx.load_env
  if (libtbx.env.is_development_environment()):
    print("""\
============================================================================
Reminder: Please do not forget: libtbx.find_clutter
          See also: cctbx_project/libtbx/development/dev_guidelines.txt
============================================================================
""")
  print("Number of lines with errors or warnings:", n_found)
  print()

if (__name__ == "__main__"):
  import sys
  run(args=sys.argv[1:])
