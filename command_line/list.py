from __future__ import division
from __future__ import print_function
import libtbx.load_env
import sys, os
import os.path
import glob
from libtbx.command_line import run_tests_parallel
from phenix_regression.command_line.test_all_parallel import cmdargs
from phenix_regression.wizards.test_all_parallel import cmdargs as wiz_cmdargs

# Purpose: list all tests or any test matching supplied text string

def matches_any_entry(text,entries): # hardwired for format of test run
  for x in entries:
    if text.find(x)>-1:
      return True
  return False

def run(args,out=sys.stdout):
  print("\nListing of available Phenix regression tests", file=out)
  all_tests=run_tests_parallel.run(
     args=cmdargs+wiz_cmdargs+['skip_missing=True'],
     return_list_of_tests=True)
  print("\nTotal of %s tests available. \n" %( len(all_tests )), file=out)
  listed_tests=[]
  for test in all_tests:
    if not args or matches_any_entry(test,args):
      listed_tests.append(test)
  if args:
    print("Listing %s tests matching of the following strings: %s\n" %(
      len(listed_tests)," ".join(args)), file=out)

  for test in listed_tests:
      print(test, file=out)

  return listed_tests

if __name__=="__main__":
  args=sys.argv[1:]
  run(args)

