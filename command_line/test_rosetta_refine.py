
from __future__ import division
from __future__ import print_function
from libtbx.utils import null_out
import libtbx.load_env
import os.path

def exercise () :
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/1yjp_h.pdb",
    test=os.path.isfile)
  mtz_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/1yjp.mtz",
    test=os.path.isfile)
  assert (not None in [pdb_file, mtz_file])
  args = [
    pdb_file,
    mtz_file,
    "nproc=1",
    "protocol=hires",
    "number_of_models=1",
    "output.prefix=tst_rosetta_refine",
    "--debug",
  ]
  for file_name in os.listdir(".") :
    if file_name.startswith("tst_rosetta_refine") :
      os.remove(file_name)
  from phenix.command_line import rosetta_refine
  rosetta_refine.run(
    args=args,
    out=null_out(),
    create_dir=False)

if (__name__ == "__main__") :
  exercise()
  print("OK")
