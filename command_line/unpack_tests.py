from __future__ import print_function
from libtbx import easy_run
import libtbx.option_parser
import libtbx.load_env
import distutils.dir_util
import sys, os
op = os.path

def run(args):
  if (len(args) == 0): args = ["--help"]
  command_line = (libtbx.option_parser.option_parser(
    usage="python unpack_tests.py"
         +" [options] resolve|solve|resolve_pattern")
    .option(None, "--force",
      action="store_true",
      default=False,
      help="remove existing directories")
  ).process(args=args, nargs=1)
  assert command_line.args[0] in ["resolve", "solve", "resolve_pattern"]
  appl = command_line.args[0]
  co = command_line.options
  dir_with_tgz = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/wizards/%s_tests" % appl,
    optional=False)
  wt = op.join(dir_with_tgz, "wall_clock_times")
  if (op.isfile(wt)):
    open("tab_wall_clock_times", "w").write(open(wt).read())
  test_names = []
  for node in os.listdir(dir_with_tgz):
    if (not node.endswith(".tgz")): continue
    test_name = node[:-4]
    if (test_name == "test_bad"): continue
    test_names.append(test_name)
    if (op.isdir(test_name)):
      if (not co.force):
        raise RuntimeError("""\
Directory exists already: %s
  Add --force to remove existing directories.
""" % test_name)
      print("Note: removing directory %s" % test_name)
      distutils.dir_util.remove_tree(test_name)
      assert not op.isdir(test_name)
  print("Number of tests:", len(test_names))
  for test_name in test_names:
    cmd = "mkdir %s; cd %s; tar zxf %s" % (
      test_name, test_name, op.join(dir_with_tgz, test_name+".tgz"))
    easy_run.fully_buffered(command=cmd).raise_if_errors_or_output()
    assert op.isdir(test_name)
    assert op.isfile(op.join(test_name, test_name+".com"))
    open(op.join(test_name, "qjob"), "w").write("""\
#$ -cwd -j y -N %s
source %s/setpaths.csh
./%s >& QLOG
""" % (test_name[5:], libtbx.env.build_path, test_name))

if (__name__ == "__main__"):
  run(args=sys.argv[1:])
