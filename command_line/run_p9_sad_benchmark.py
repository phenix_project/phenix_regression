
from __future__ import division
from __future__ import print_function
from libtbx import easy_run
import libtbx.load_env
import os.path
import shutil
import time

def run () :
  examples_dir = libtbx.env.find_in_repositories(
    relative_path="phenix_examples/p9-sad",
    test=os.path.isdir)
  assert (examples_dir is not None)
  args = [
    "phenix.autosol",
    "\"%s\"" % os.path.join(examples_dir, "p9.sca"),
    "seq_file=\"%s\"" % os.path.join(examples_dir, "seq.dat"),
    "atom_type=Se",
    "lambda=0.9792",
    "crystal_info.resolution=2.5",
  ]
  if os.path.exists("tst_p9_sad_benchmark") :
    shutil.rmtree("tst_p9_sad_benchmark")
  os.mkdir("tst_p9_sad_benchmark")
  os.chdir("tst_p9_sad_benchmark")
  t0 = time.time()
  assert easy_run.call(" ".join(args)) == 0
  t1 = time.time()
  print("RUNTIME: %.1fs" % (t1 - t0))

if (__name__ == "__main__") :
  run()
