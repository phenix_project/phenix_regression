
from __future__ import division
from __future__ import print_function
from libtbx import easy_run
import libtbx.load_env
import os.path
import shutil
import time

def run () :
  examples_dir = libtbx.env.find_in_repositories(
    relative_path="phenix_examples/hipip-refine",
    test=os.path.isdir)
  assert (examples_dir is not None)
  args1 = [
    "phenix.ready_set",
    "\"%s\"" % os.path.join(examples_dir, "hipip.pdb"),
  ]
  t0 = time.time()
  assert easy_run.call(" ".join(args1)) == 0
  args2 = [
    "phenix.refine",
    "hipip.updated.pdb",
    "\"%s\"" % os.path.join(examples_dir, "hipip.mtz"),
  ]
  assert easy_run.call(" ".join(args2)) == 0
  t1 = time.time()
  print("RUNTIME: %.1fs" % (t1 - t0))

if (__name__ == "__main__") :
  run()
