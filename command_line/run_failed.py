from __future__ import print_function
import os, sys

def run():
  tests = []
  f = open("run_tests_parallel_zlog", "rb")
  lines=f.read()
  f.close()
  for line in lines.splitlines():
    if line.find("[FAIL]")>-1:
      print(line)
      line = line.replace("[FAIL]", "")
      tests.append(line)
  for test in tests:
    print("_"*80)
    print("\n  RUNNING %s\n" % test)
    os.system(test)

if __name__=="__main__":
  run()#sys.argv[1])
