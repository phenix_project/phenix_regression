from __future__ import print_function
import run_tests_mp
import libtbx.option_parser
from libtbx import easy_run
import sys, os
op = os.path

def run(args):
  if (len(args) == 0): args = ["--help"]
  command_line = (libtbx.option_parser.option_parser(
    usage="python unpack_tests.py"
         +" [options] resolve|solve|resolve_pattern")
  ).process(args=args, nargs=1)
  appl = command_line.args[0]
  compare_file_py_path = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/wizards/compare_files.py",
    test=os.path.isfile,
    optional=False)
  compare_file_length_py_path = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/wizards/compare_file_length.py",
    test=os.path.isfile,
    optional=False)
  failing = []
  test_names = run_tests_mp.get_test_names()
  for test_name in sorted(test_names.done):
    merr_path = "%s/MERR" % test_name
    if (op.isfile(merr_path)):
      failing.append(test_name)
      print("STDERR output:", test_name)
      sys.stdout.write(open(merr_path).read())
      print()
      continue
    #
    cmd = "cd %s; libtbx.python %s %s.log MLOG" % (
      test_name, compare_file_length_py_path, test_name)
    try:
      easy_run.fully_buffered(command=cmd).raise_if_errors_or_output()
    except:
      failing.append(test_name)
      print("File lengths differ:", test_name)
      print()
      continue
    #
    cmd = "cd %s; diff -a -b %s.log MLOG > diff.dat" % ((test_name,)*2)
    easy_run.fully_buffered(command=cmd).raise_if_errors_or_output()
    cmd = "cd %s; libtbx.python %s diff.dat %s" % (
      test_name, compare_file_py_path, appl)
    cmp_result = easy_run.fully_buffered(
      command=cmd).raise_if_errors().stdout_lines
    if (cmp_result != ["OK"]):
      failing.append(test_name)
      print(test_name)
      print("\n".join(cmp_result))
      print()
  print("Number of failing tests:", len(failing))
  for test_name in failing:
    print(test_name)
  wtimes = []
  for test_name in test_names.done:
    wtimes.append(test_name+" "+open(test_name+"/wall_clock_time").read())
  open("wall_clock_times", "w").write("".join(wtimes))

if (__name__ == "__main__"):
  run(args=sys.argv[1:])
