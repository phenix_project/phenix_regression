from __future__ import division
from __future__ import print_function
import libtbx.load_env
import sys, os
import os.path
import glob
from libtbx.command_line import run_tests_parallel
from libtbx.utils import Sorry
from phenix_regression.command_line import check_test_fails

fir = libtbx.env.find_in_repositories
CCTBX_DIST  = fir(relative_path='cctbx_project',     test=os.path.exists)
PHENIX_DIST = fir(relative_path='phenix',            test=os.path.exists)
REGRESSION  = fir(relative_path='phenix_regression', test=os.path.exists)

# check for installed location
if CCTBX_DIST is None and libtbx.env.installed:
  assert os.path.isdir(PHENIX_DIST)
  CCTBX_DIST = os.path.abspath(os.path.join(PHENIX_DIST, '..'))

cmdargs = [
  # This will harvest tests from run_tests.py from each listed module below
  "module=mmtbx",
  "module=libtbx",
  "module=boost_adaptbx",
  "module=scitbx",
  "module=cctbx",
  "module=cctbx_website",
  "module=iotbx",
  "module=phenix_dev_doc",
  # This will harvest files starting with tst* and ending with *sh *csh
  "directory=" + os.path.join(CCTBX_DIST,"omptbx"),
  "directory=" + PHENIX_DIST,
]

exclude = ["windows","command_line"]

# Fetch tests from phenix_regression
for f in os.listdir(REGRESSION):
  if(f in exclude): continue
  full_f = os.path.join(REGRESSION,f)
  if(os.path.isfile(full_f) and f.startswith("tst_")):
    raise Sorry("No test files allowed in top level: %s"%str(full_f))
  if(os.path.isdir(full_f)):
    arg = "directory=" + full_f
    cmdargs.append(arg)

def find_keyword(args, kw, return_list = False):
  new_args = []
  value = None
  for arg in args:
    if arg.find("=") > -1 and arg.split("=")[0] == kw:
      value = arg.split("=")[1]
    else:
      new_args.append(arg)
  if return_list and value:
    value=value.replace(","," ").split()
  return value, new_args

def run(args,
     python_keyword_text=None,
     max_tests=None,
     start_test=None,
     tests_to_skip=None,
     tests_to_run=None):

  if tests_to_run is None:
    tests_to_run, args = find_keyword(args, 'tests_to_run',
      return_list = True) # tests with this text
  if tests_to_skip is None:
    tests_to_skip, args = find_keyword(args, 'tests_to_skip',
      return_list = True) # skip tests with this text

  # ======================================================================
  #   Allow finding and running all tests affected by changes in git and
  #   Sorting based on timings of regression tests
  # ======================================================================

  git = ('git' in args)  #  Run tests that are affected by changes in git
  sort = ('sort' in args)
  if (git or sort):
    if git:
      args.remove('git')
      print("Running tests that are affected by changes in git")
      git_directories_to_search, args = find_keyword(args,"directories")
    if sort:
      args.remove('sort')
      git_directories_to_search = None

    from iotbx.cli_parser import run_program
    try:
      from phenix.programs import find_program
    except Exception as e: # could not import
      raise Sorry("You cannot use the keyword 'git' without Phenix")
    if git:
      local_args = ["git=True"]
    else:
      local_args = ["all_tests=True"]
    if git_directories_to_search:
      git_directories_to_search = git_directories_to_search.replace(","," ")
      local_args.append("directories_to_search=%s" %(git_directories_to_search))
    result = run_program(program_class=find_program.Program,
        args = local_args, logger = sys.stdout)
    git_tests = result.tests['all']
  else:
    git_tests = None
    git_directories_to_search = None
  # ======================================================================
  # ======================================================================


  if python_keyword_text:
    print("Running with %s " %(python_keyword_text))
  if max_tests:
    print("Running only %s tests" %(max_tests))
  if start_test:
    print("Starting with test %s " %(start_test))
  if tests_to_skip:
    print("Skipping tests containing these words: %s " %str(tests_to_skip))
  if tests_to_run:
    print("Running tests containing these words: %s " %str(tests_to_run))

  # nuke old .pyc files
  files = []
  files.extend(glob.glob(CCTBX_DIST + "*.pyc"))
  files.extend(glob.glob(REGRESSION + "*.pyc"))
  files.extend(glob.glob(PHENIX_DIST + "*.pyc"))
  for f in files:
    try:
      os.remove(f)
    except Exception as m:
      raise Sorry(traceback.format_exc())
  return run_tests_parallel.run(cmdargs + args,
     python_keyword_text=python_keyword_text,
     max_tests=max_tests,
     start_test=start_test,
     tests_to_skip=tests_to_skip,
     tests_to_run=tests_to_run,
     expected_failures_from_phenix_regression=
         check_test_fails.expected_failures,
     unstables_from_phenix_regression=check_test_fails.unstable_tests,
     supplied_list_of_tests = git_tests)

if __name__ == '__main__':
  import sys
  run(sys.argv[1:])
  failures = check_test_fails.main()
