#!/bin/sh

MODEL=`libtbx.find_in_repositories phenix_regression/pdb/nsf-d2_noligand.pdb`
DATA=`libtbx.find_in_repositories phenix_regression/reflection_files/nsf-d2.mtz`
NCPU=2

TESTDIR='tst_ligand_identification'

if [ -d $TESTDIR ] 
then
    rm -rf $TESTDIR 
    mkdir $TESTDIR
else
    mkdir $TESTDIR
fi

cd $TESTDIR

CMD='phenix.ligand_identification'

#test1 basic inputs, external ligand lib as 3-letter codes, faster fit
echo "$CMD mtz_in=$DATA nproc=$NCPU model=$MODEL input_labels=F use_ligandfit=False ligand_list='ATP CMP'"
$CMD mtz_in=$DATA nproc=$NCPU model=$MODEL input_labels=F use_ligandfit=False \
  ligand_list='ATP CMP' > tst_ligand1.log

#test2 difference map, external lib,faster fit, use ligandfit
cp resolve_diff.mtz  datain.mtz
mv ligand_files mylib
echo "$CMD mtz_in=datain.mtz nproc=$NCPU model=$MODEL ligand_dir=mylib input_labels='FP PHIM'  mtz_type=diffmap "
$CMD mtz_in=datain.mtz nproc=$NCPU model=$MODEL ligand_dir=mylib input_labels='FP PHIM' \
  mtz_type=diffmap  > tst_ligand2.log

cd ..
pwd
echo "OK"





