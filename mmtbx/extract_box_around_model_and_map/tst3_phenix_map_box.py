from __future__ import division
from __future__ import print_function
import iotbx.mtz
import iotbx.xplor.map
from cctbx.array_family import flex
import time
import iotbx.pdb
import iotbx.mtz
from cctbx import miller
from cctbx import maptbx

pdb_str_answer="""\
CRYST1   24.085   25.037   23.421  90.00  90.00  90.00 P 21 21 21
remark CRYST1   24.085   25.037   23.421  90.00  90.00  90.00 P 1
ATOM     17  N   TYR A  20      15.637  14.593  10.430  1.00 20.00           N
ATOM     18  CA  TYR A  20      16.015  13.652   9.382  1.00 20.00           C
ATOM     19  C   TYR A  20      14.868  12.702   9.050  1.00 20.00           C
ATOM     20  O   TYR A  20      13.763  12.840   9.573  1.00 20.00           O
ATOM     21  CB  TYR A  20      17.261  12.860   9.791  1.00 20.00           C
ATOM     22  CG  TYR A  20      17.104  12.073  11.074  1.00 20.00           C
ATOM     23  CD1 TYR A  20      16.687  10.748  11.053  1.00 20.00           C
ATOM     24  CD2 TYR A  20      17.378  12.653  12.306  1.00 20.00           C
ATOM     25  CE1 TYR A  20      16.544  10.025  12.223  1.00 20.00           C
ATOM     26  CE2 TYR A  20      17.237  11.938  13.481  1.00 20.00           C
ATOM     27  CZ  TYR A  20      16.820  10.624  13.433  1.00 20.00           C
ATOM     28  OH  TYR A  20      16.679   9.909  14.600  1.00 20.00           O
TER
END
"""

pdb_str_box="""\n
"""

def map_hkl_map(map_data, crystal_gridding, xrs, d_min=1.5):
  ma = xrs.structure_factors(d_min = d_min).f_calc()
  map_coefficients = ma.structure_factors_from_map(
    map            = map_data,
    use_scale      = True,
    anomalous_flag = False,
    use_sg         = True)
  fft_map = miller.fft_map(
    crystal_gridding     = crystal_gridding,
    fourier_coefficients = map_coefficients)
  fft_map.apply_sigma_scaling()
  return fft_map.real_map_unpadded(), ma

def exercise(d_min=1.5, resolution_factor = 0.3, prefix="tst_map_box",
   density_command="",zlog_prefix="zlog",space_group_str="P 21 21 21",keep_origin=False, write_mask = False, invert_hand = False):
  """
  Test to exercise phenix.map_box
  """
  #
  pdbf,mtzf,mapf = "%s_model.pdb"%prefix, "%s_map.mtz"%prefix, "%s_map.ccp4"%prefix
  edited_pdb_str_answer=pdb_str_answer.replace("P 21 21 21",space_group_str)
  pdb_inp = iotbx.pdb.input(source_info=None, lines=edited_pdb_str_answer)
  pdb_inp.write_pdb_file(file_name = pdbf)
  xrs = pdb_inp.xray_structure_simple()
  f_calc = xrs.structure_factors(d_min = d_min).f_calc()
  fft_map = f_calc.fft_map(resolution_factor=resolution_factor)
  fft_map.apply_sigma_scaling()
  map_data = fft_map.real_map_unpadded()
  mtz_dataset = f_calc.as_mtz_dataset(column_root_label = "FCmap")
  mtz_object = mtz_dataset.mtz_object()
  mtz_object.write(file_name = mtzf)
  from iotbx import ccp4_map
  ccp4_map.write_ccp4_map(
    file_name      = mapf,
    unit_cell      = xrs.unit_cell(),
    space_group    = xrs.space_group(),
    map_data       = map_data,
    labels         = flex.std_string([" "]))
  from libtbx import easy_run
  if keep_origin:
    files=[mapf]
  else:
    files=[mapf, mtzf]

  for i, mf in enumerate(files):
    output_file_name_prefix="%s_%s"%(prefix,str(i))
    if keep_origin:
     output_format=" output_format='*ccp4  *xplor' "
    else:
     output_format=" output_format='*ccp4 *mtz *xplor' "
    cmd = " ".join([
      "phenix.map_box", pdbf, mf,
      "label=FCmap selection_radius=1 box_cushion=1",
      "selection='chain A and resname TYR' ",
      "write_mask_file=True" if write_mask else "",
      "keep_map_size=True" if invert_hand else "",
      "invert_hand=True" if invert_hand else "",
      "mask_atoms=True" if write_mask else "",
      "%s" %(density_command),
      "output_file_name_prefix=%s"%output_file_name_prefix,
      "keep_origin=%s" %(keep_origin),
      output_format,
      ">%s%s"%(zlog_prefix,str(i))])

    print("\nRunning with: \n\n %s\n " %(cmd))
    ierr=easy_run.call(cmd)
    assert ierr==0
    #
    o = output_file_name_prefix

    real_map = iotbx.ccp4_map.map_reader(file_name=o+".ccp4")
    m_ccp4 = real_map.data
    full_unit_cell_grid=real_map.unit_cell_grid
    m_xpl  = iotbx.xplor.map.reader(file_name=o+".xplor").data
    if keep_origin:
      mtz_in=None
      ma=None
    else:
      mtz_in = iotbx.mtz.object(file_name=o+".mtz")
      ma = mtz_in.as_miller_arrays()[0]
    crystal_symmetry=real_map.crystal_symmetry()

    xrs    = iotbx.pdb.input(file_name=o+".pdb").xray_structure_simple(
     crystal_symmetry=crystal_symmetry )

    if invert_hand:
      assert m_ccp4.all() == map_data.all()
      nx,ny,nz = m_ccp4.all()

      from libtbx.test_utils import approx_equal
      assert approx_equal(map_data[0,0,5] , m_ccp4[0,0,nz-1-5])
      print ("OK")
      return

    if keep_origin:
      print("focus, origin, all for each map as written by map_box:")
      print("ccp4: ", m_ccp4.focus(), m_ccp4.origin(), m_ccp4.all())
      print("xplor:",m_xpl.focus(),  m_xpl.origin(),  m_xpl.all())
      # shift model and all maps  and map_coeffs to make them zero-based
      print("Shifting origin from ",m_ccp4.origin()," to (0,0,0)")
      from scitbx.matrix import col
      origin_shift_frac=tuple(
        -flex.double(m_ccp4.origin())/flex.double( m_ccp4.all()))

      origin_shift_cart=xrs.crystal_symmetry().unit_cell().orthogonalize(
        origin_shift_frac)
      print("Origin shift (fractional): (%.3f, %.3f, %.3f)" %(origin_shift_frac))
      print("Origin shift (Cart ): (%.3f, %.3f, %.3f)" %(origin_shift_cart))
      m_ccp4=m_ccp4.shift_origin()
      m_xpl=m_xpl.shift_origin()
      #ma=ma.translational_shift(col(origin_shift_frac))
      sites_frac=xrs.sites_frac()+col(origin_shift_frac)
      xrs.set_sites_frac(sites_frac)

    cs = xrs.crystal_symmetry()
    from cctbx.maptbx import crystal_gridding
    cg = crystal_gridding(
      unit_cell             = cs.unit_cell(),
      space_group_info      = cs.space_group_info(),
      pre_determined_n_real = full_unit_cell_grid)

    f_calc_from_xrs = xrs.structure_factors(d_min = d_min).f_calc()
    fft_map_from_xrs = f_calc_from_xrs.fft_map(
      resolution_factor=resolution_factor,
      crystal_gridding     = cg)
    fft_map_from_xrs.apply_sigma_scaling()
    m_xrs = fft_map_from_xrs.real_map_unpadded()

    if 0: #ma:
      fft_map = ma.fft_map(resolution_factor=resolution_factor,
       crystal_gridding     = cg)
      fft_map.apply_sigma_scaling()
      m_mtz = fft_map.real_map_unpadded()
    else:
      m_mtz=None
    #
    print("Working focus, origin, all for each map:")
    print("ccp4: ", m_ccp4.focus(), m_ccp4.origin(), m_ccp4.all())
    print("xplor:",m_xpl.focus(),  m_xpl.origin(),  m_xpl.all())
    if m_mtz:
      print("mtz:  ", m_mtz.focus(),  m_mtz.origin(),  m_mtz.all())
    print("xrs:  ",m_xrs.focus(),  m_xrs.origin(),  m_xrs.all())
    #
    m_ccp4, ma1 = map_hkl_map(map_data=m_ccp4.as_double(), crystal_gridding=cg, xrs=xrs, d_min=d_min)
    m_xpl , ma2 = map_hkl_map(map_data=m_xpl.as_double(),  crystal_gridding=cg, xrs=xrs, d_min=d_min)
    if m_mtz:
      m_mtz , ma3 = map_hkl_map(map_data=m_mtz.as_double(),  crystal_gridding=cg, xrs=xrs, d_min=d_min)
    m_xrs , ma4 = map_hkl_map(map_data=m_xrs.as_double(),  crystal_gridding=cg, xrs=xrs, d_min=d_min)
    #
    assert m_xpl.all()==m_xpl.focus()
    assert m_xpl.all()==m_ccp4.all()
    assert m_xpl.all()==m_xrs.all()
    if m_mtz:
      assert m_xpl.all()==m_mtz.all()
    sel = maptbx.grid_indices_around_sites(
      unit_cell  = xrs.unit_cell(),
      fft_n_real = m_xpl.focus(),
      fft_m_real = m_xpl.all(),
      sites_cart = xrs.sites_cart(),
      site_radii = flex.double(xrs.scatterers().size(), 1.5))
    m_ccp4 = m_ccp4.select(sel)
    m_xpl  = m_xpl .select(sel)
    if m_mtz:
      m_mtz  = m_mtz .select(sel)
    m_xrs  = m_xrs .select(sel)
    #
    assert m_ccp4.size() == m_xpl.size()
    assert m_ccp4.size() == m_xrs.size()
    cc_ccp4_xrs = flex.linear_correlation(x=m_ccp4.as_1d(),  y=m_xrs.as_1d()).coefficient()
    if m_mtz:
      assert m_mtz.size()  == m_ccp4.size()
      cc_mtz_xrs = flex.linear_correlation(x=m_mtz.as_1d(),  y=m_xrs.as_1d()).coefficient()
      cc_mtz_ccp4 = flex.linear_correlation(x=m_mtz.as_1d(),  y=m_ccp4.as_1d()).coefficient()
      cc_mtz_xpl  = flex.linear_correlation(x=m_mtz.as_1d(),  y=m_xpl.as_1d()).coefficient()

    cc_ccp4_xpl = flex.linear_correlation(x=m_ccp4.as_1d(), y=m_xpl.as_1d()).coefficient()
    print(cc_ccp4_xpl, cc_ccp4_xrs)
    assert cc_ccp4_xpl > 0.99
    assert cc_ccp4_xrs > 0.99
    if m_mtz:
      print(cc_mtz_ccp4, cc_mtz_xpl, cc_mtz_xrs)
      assert cc_mtz_xrs  > 0.99
      assert cc_mtz_ccp4 > 0.99
      assert cc_mtz_xpl  > 0.99

if(__name__ == "__main__"):
  t0 = time.time()
  exercise(invert_hand=True)
  exercise()
  exercise(keep_origin=True)
  exercise(write_mask=True)
  exercise(keep_origin=True,space_group_str="P 1")
  exercise(density_command="density_select=true",
    prefix="tst_map_box_density_select",zlog_prefix="zlog_density_select",
    space_group_str="P 1")
  print("Time: %6.4f"%(time.time()-t0))
