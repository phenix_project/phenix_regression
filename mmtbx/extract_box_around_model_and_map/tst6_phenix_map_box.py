from __future__ import division
from __future__ import print_function
from scitbx.array_family import flex
import iotbx.pdb
import time
import mmtbx.maps.map_model_cc
from libtbx import group_args

pdb_str = """
CRYST1   14.755   15.940   19.523  90.00  90.00  90.00 P 1
ATOM      1  N   ALA L 139       5.232   6.337   5.079  1.00 25.00           N
ATOM      2  CA  ALA L 139       5.342   5.476   6.250  1.00 25.00           C
ATOM      3  C   ALA L 139       6.027   6.204   7.402  1.00 25.00           C
ATOM      4  O   ALA L 139       7.033   6.886   7.206  1.00 25.00           O
ATOM      5  N   TRP L 140       5.477   6.054   8.602  1.00 25.00           N
ATOM      6  CA  TRP L 140       6.034   6.696   9.786  1.00 25.00           C
ATOM      7  C   TRP L 140       6.303   5.675  10.888  1.00 25.00           C
ATOM      8  O   TRP L 140       5.382   5.017  11.372  1.00 25.00           O
ATOM      9  CB  TRP L 140       5.092   7.787  10.298  1.00 30.00           C
ATOM     10  CG  TRP L 140       4.833   8.873   9.299  1.00 30.00           C
ATOM     11  CD1 TRP L 140       3.836   8.910   8.369  1.00 30.00           C
ATOM     12  CD2 TRP L 140       5.584  10.082   9.131  1.00 30.00           C
ATOM     13  NE1 TRP L 140       3.919  10.066   7.631  1.00 30.00           N
ATOM     14  CE2 TRP L 140       4.984  10.803   8.079  1.00 30.00           C
ATOM     15  CE3 TRP L 140       6.705  10.624   9.766  1.00 30.00           C
ATOM     16  CZ2 TRP L 140       5.467  12.037   7.649  1.00 30.00           C
ATOM     17  CZ3 TRP L 140       7.183  11.849   9.338  1.00 30.00           C
ATOM     18  CH2 TRP L 140       6.565  12.542   8.290  1.00 30.00           C
ATOM     19  N   ALA L 141       7.567   5.544  11.283  1.00 25.00           N
ATOM     20  CA  ALA L 141       8.649   6.331  10.700  1.00 25.00           C
ATOM     21  C   ALA L 141       9.725   5.427  10.110  1.00 25.00           C
ATOM     22  O   ALA L 141       9.623   4.991   8.963  1.00 25.00           O
TER
END
"""

def write_ccp4_map(map_data, cs, file_name):
  from iotbx import ccp4_map
  ccp4_map.write_ccp4_map(
    file_name = file_name,
    unit_cell = cs.unit_cell(),
    space_group = cs.space_group(),
    map_data = map_data,
    labels = flex.std_string([""]))


def get_data(prefix = None):
  # original (zero-origin) map and model
  pdb_inp = iotbx.pdb.input(source_info = None, lines = pdb_str)
  pdb_file_name = "%s_%s"%(prefix, "orig.pdb")
  pdb_inp.write_pdb_file(file_name = pdb_file_name)
  ph = pdb_inp.construct_hierarchy()
  xrs = pdb_inp.xray_structure_simple()
  fc = xrs.structure_factors(d_min = 2.5).f_calc()
  fft_map = fc.fft_map(resolution_factor = 0.25)
  fft_map.apply_sigma_scaling()
  map_data = fft_map.real_map_unpadded()
  assert map_data.all()  ==  (24, 25, 32)
  assert map_data.origin()  ==  (0, 0, 0)
  map_file_name = "%s_%s"%(prefix, "orig.ccp4")
  write_ccp4_map(map_data = map_data, cs = xrs.crystal_symmetry(),
    file_name = map_file_name)

  params = mmtbx.maps.map_model_cc.master_params().extract()
  params.map_model_cc.resolution = 2.5
  return group_args(
    ph = ph,
    map_data = map_data,
    xrs = xrs,
    params = params,
    pdb_file_name = pdb_file_name,
    map_file_name = map_file_name,
    ncs_object = None,
    full_unit_cell_grid = map_data.all(),
    full_unit_cell = xrs.crystal_symmetry().unit_cell(),
    expected_output_unit_cell_grid = map_data.all(),
    expected_output_origin = map_data.origin(),
    expected_output_map_grid = map_data.all()
)


def check_map_model_cc(inputs):

  # Shift origin to (0, 0, 0) if necessary and get map-model CC

  map_data = inputs.map_data.deep_copy()

  import mmtbx.model
  model =  mmtbx.model.manager(
      pdb_hierarchy = inputs.ph.deep_copy(),
      crystal_symmetry = inputs.xrs.crystal_symmetry())

  print("Full cell:  (%.2f, %.2f, %.2f, %.0f, %.0f %.0f) A" %(
      inputs.full_unit_cell.parameters()))
  print("Full grid:  (%s, %s, %s)" %(inputs.full_unit_cell_grid))
  print("Origin: ", map_data.origin(), " Extent: ", map_data.all())
  print("PDB symmetry (%.2f, %.2f, %.2f, %.0f, %.0f %.0f) A" %(
      inputs.xrs.crystal_symmetry().unit_cell().parameters()))
  print("PDB record:\n%s" %("\n".join(model.get_hierarchy().as_pdb_string().splitlines()[:2])))

  from cctbx import crystal
  from cctbx import sgtbx

  full_cell_crystal_symmetry = crystal.symmetry(
    unit_cell = inputs.full_unit_cell,
    space_group_info = sgtbx.space_group_info(symbol = str('p1')))
  from iotbx.map_manager import map_manager
  mm = map_manager(map_data = map_data,
    unit_cell_crystal_symmetry = full_cell_crystal_symmetry,
    unit_cell_grid = inputs.full_unit_cell_grid,
    wrapping=inputs.full_unit_cell_grid == map_data.all())

  if inputs.expected_output_unit_cell_grid:
    assert inputs.expected_output_unit_cell_grid == inputs.full_unit_cell_grid
  if inputs.expected_output_map_grid:
    assert inputs.expected_output_map_grid == map_data.all()
  if inputs.expected_output_origin:
    assert inputs.expected_output_origin == map_data.origin()


  from iotbx import map_model_manager
  base = map_model_manager.map_model_manager(
    map_manager = mm,
    model           = model)

  task_obj = mmtbx.maps.map_model_cc.map_model_cc(
    map_data        = base.map_data(),
    pdb_hierarchy   = base.hierarchy(),
    crystal_symmetry = base.crystal_symmetry(),
    params          = inputs.params.map_model_cc)
  task_obj.validate()
  task_obj.run()
  result = task_obj.get_results()
  assert result.cc_mask  >0.96
  assert result.cc_peaks >0.96
  assert result.cc_volume>0.96
  assert result.cc_side_chain.cc>0.96
  assert result.cc_main_chain.cc>0.96
  return result.cc_mask

def get_map_box(inputs = None,
    prefix = None,
    keep_origin = None,
    lower_bounds = None,
    upper_bounds = None,
    keep_input_unit_cell_and_grid = None,
    output_unit_cell_grid = None,
    output_unit_cell = None,
    output_origin_grid_units = None,
    expected_output_unit_cell_grid = None,
    expected_output_map_grid = None,
    expected_output_origin = None,
    log = None):
  from mmtbx.command_line.map_box import run as map_box

  args=['output_format=ccp4',
    'output_file_name_prefix=%s' %(prefix),
    'keep_origin=%s' %(keep_origin),
    'keep_input_unit_cell_and_grid=%s' %(keep_input_unit_cell_and_grid),
    'ccp4_map_file=%s' %(inputs.map_file_name),
    'pdb_file=%s' %(inputs.pdb_file_name),
    ]
  if output_unit_cell_grid:
    args.append("output_unit_cell_grid=%s, %s, %s" %(output_unit_cell_grid))
  if output_unit_cell:
    args.append('output_unit_cell=%.3f, %.3f, %.3f' %(output_unit_cell))
  if output_origin_grid_units:
    args.append('output_origin_grid_units=%s, %s, %s' %(output_origin_grid_units))
  print ("phenix.map_box %s" %" ".join(args))
  map_box(args,
     lower_bounds=lower_bounds,
     upper_bounds = upper_bounds,
     write_output_files = True,
     log = log)
  from copy import deepcopy
  result = deepcopy(inputs)
  result.pdb_file_name = "%s.pdb"%prefix
  result.map_file_name = "%s.ccp4"%prefix
  # read in pdb and map:

  real_map = iotbx.mrcfile.map_reader(file_name = result.map_file_name)
  result.map_data = real_map.map_data()
  crystal_symmetry = real_map.crystal_symmetry()
  pdb_inp = iotbx.pdb.input(file_name = result.pdb_file_name)
  model = mmtbx.model.manager(model_input = pdb_inp,
     crystal_symmetry = crystal_symmetry)
  result.ph = model.get_hierarchy()
  result.xrs = model.get_xray_structure()
  result.full_unit_cell_grid = real_map.unit_cell_grid
  result.full_unit_cell = real_map.unit_cell_crystal_symmetry().unit_cell()

  result.expected_output_unit_cell_grid = expected_output_unit_cell_grid
  result.expected_output_map_grid = expected_output_map_grid
  result.expected_output_origin = expected_output_origin
  return result

def run(prefix = "tst_map_box_6"):

  # Set up small PDB file and map file and check map-model cc
  inputs = get_data(prefix = prefix)

  print("Checking initial model and map...")
  cc = check_map_model_cc(inputs)
  print("CC_mask: ", cc)
  print()

  count = 0
  for [lower_bounds,
      upper_bounds,
      keep_origin,
      keep_input_unit_cell_and_grid,
      output_unit_cell_grid,
      output_unit_cell,
      output_origin_grid_units,
      expected_output_unit_cell_grid,
      expected_output_map_grid,
      expected_output_origin,
      ] in (
    [None, None, True, False, (32, 32, 32), (19.674, 20.404, 19.524), None,
       (32, 32, 32), (21, 23, 22), (1, 3, 3)],
    [None, None, None, False, None, None, (8, 8, 8),
       (21, 23, 22), (21, 23, 22), (8, 8, 8)],
    [None, None, False, False, None, None, None,
       (21, 23, 22), (21, 23, 22), (0, 0, 0)],
    [None, None, True, True, None, None, None,
       (24, 25, 32), (21, 23, 22), (1, 3, 3) ],
    [(5, 5, 5), (20, 20, 20), True, True, None, None, None,
        (24, 25, 32), (16, 16, 16), (5, 5, 5)],
    [(5, 5, 5), (20, 20, 20), True, False, None, None, None,
        (16, 16, 16), (16, 16, 16), (5, 5, 5)],
    [(5, 5, 5), (20, 20, 20), False, False, None, None, None,
        (16, 16, 16), (16, 16, 16), (0, 0, 0)],
      ):
    count+= 1

    # Cut out part of the map with various options and re-check map-model cc
    print("Cutting out part of the map:")
    print("Lower bounds:", lower_bounds, " Upper bounds: ", upper_bounds)
    print("Keep_origin:", keep_origin, \
       " Keep_input_unit_cell_and_grid", keep_input_unit_cell_and_grid)

    print("output_unit_cell_grid:", output_unit_cell_grid)
    print("output_unit_cell", output_unit_cell)
    print("output_origin_grid_units:", output_origin_grid_units)
    print("expected_output_unit_cell_grid", expected_output_unit_cell_grid)
    print("expected_output_map_grid", expected_output_map_grid)
    print("expected_output_origin", expected_output_origin)

    import sys
    result = get_map_box(inputs = inputs,
      prefix = 'test_%s' %(count),
      keep_origin = keep_origin,
      lower_bounds = lower_bounds,
      upper_bounds = upper_bounds,
      keep_input_unit_cell_and_grid = keep_input_unit_cell_and_grid,
      output_unit_cell_grid = output_unit_cell_grid,
      output_unit_cell = output_unit_cell,
      output_origin_grid_units = output_origin_grid_units,
      expected_output_unit_cell_grid = expected_output_unit_cell_grid,
      expected_output_map_grid = expected_output_map_grid,
      expected_output_origin = expected_output_origin,
     log = sys.stdout) #null_out())
    cc = check_map_model_cc(result)
    print("CC_mask: ", cc)
    print()

  print("OK")


if (__name__  ==  "__main__"):
  t0 = time.time()
  run()
  print("Time: %6.3f"%(time.time()-t0))
  print("OK")
