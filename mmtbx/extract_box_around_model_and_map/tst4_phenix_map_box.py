from __future__ import division
from __future__ import print_function
import time
import iotbx.pdb
from cctbx import miller
from libtbx import easy_run
from iotbx import reflection_file_reader

pdb_str_answer="""\
CRYST1   24.085   25.037   23.421  90.00  90.00  90.00 P 21 21 21
remark CRYST1   24.085   25.037   23.421  90.00  90.00  90.00 P 1
ATOM      1  N   ASP A  18      19.085  14.927  14.341  1.00 20.00           N
ATOM      2  CA  ASP A  18      18.051  15.952  14.416  1.00 20.00           C
ATOM      3  C   ASP A  18      17.560  16.341  13.025  1.00 20.00           C
ATOM      4  O   ASP A  18      18.362  16.608  12.129  1.00 20.00           O
ATOM      5  CB  ASP A  18      18.572  17.186  15.155  1.00 20.00           C
ATOM      6  CG  ASP A  18      17.527  18.278  15.276  1.00 20.00           C
ATOM      7  OD1 ASP A  18      16.754  18.257  16.256  1.00 20.00           O
ATOM      8  OD2 ASP A  18      17.480  19.158  14.391  1.00 20.00           O
ATOM      9  N   ASN A  19      16.239  16.370  12.861  1.00 20.00           N
ATOM     10  CA  ASN A  19      15.604  16.724  11.593  1.00 20.00           C
ATOM     11  C   ASN A  19      16.078  15.846  10.436  1.00 20.00           C
ATOM     12  O   ASN A  19      16.827  16.297   9.568  1.00 20.00           O
ATOM     13  CB  ASN A  19      15.823  18.206  11.270  1.00 20.00           C
ATOM     14  CG  ASN A  19      14.830  18.739  10.251  1.00 20.00           C
ATOM     15  OD1 ASN A  19      14.313  17.995   9.417  1.00 20.00           O
ATOM     16  ND2 ASN A  19      14.560  20.037  10.315  1.00 20.00           N
ATOM     17  N   TYR A  20      15.637  14.593  10.430  1.00 20.00           N
ATOM     18  CA  TYR A  20      16.015  13.652   9.382  1.00 20.00           C
ATOM     19  C   TYR A  20      14.868  12.702   9.050  1.00 20.00           C
ATOM     20  O   TYR A  20      13.763  12.840   9.573  1.00 20.00           O
ATOM     21  CB  TYR A  20      17.261  12.860   9.791  1.00 20.00           C
ATOM     22  CG  TYR A  20      17.104  12.073  11.074  1.00 20.00           C
ATOM     23  CD1 TYR A  20      16.687  10.748  11.053  1.00 20.00           C
ATOM     24  CD2 TYR A  20      17.378  12.653  12.306  1.00 20.00           C
ATOM     25  CE1 TYR A  20      16.544  10.025  12.223  1.00 20.00           C
ATOM     26  CE2 TYR A  20      17.237  11.938  13.481  1.00 20.00           C
ATOM     27  CZ  TYR A  20      16.820  10.624  13.433  1.00 20.00           C
ATOM     28  OH  TYR A  20      16.679   9.909  14.600  1.00 20.00           O
ATOM     29  N   ARG A  21      15.140  11.738   8.176  1.00 20.00           N
ATOM     30  CA  ARG A  21      14.133  10.764   7.772  1.00 20.00           C
ATOM     31  C   ARG A  21      13.986   9.673   8.827  1.00 20.00           C
ATOM     32  O   ARG A  21      12.921   9.511   9.423  1.00 20.00           O
ATOM     33  CB  ARG A  21      14.493  10.156   6.413  1.00 20.00           C
ATOM     34  CG  ARG A  21      13.399   9.297   5.788  1.00 20.00           C
ATOM     35  CD  ARG A  21      13.605   7.816   6.075  1.00 20.00           C
ATOM     36  NE  ARG A  21      12.559   6.992   5.477  1.00 20.00           N
ATOM     37  CZ  ARG A  21      12.502   5.668   5.576  1.00 20.00           C
ATOM     38  NH1 ARG A  21      13.435   5.011   6.252  1.00 20.00           N
ATOM     39  NH2 ARG A  21      11.512   5.000   5.000  1.00 20.00           N
TER
ATOM     40  N   ASP B   3      13.719  13.364  17.611  1.00 20.00           N
ATOM     41  CA  ASP B   3      12.951  14.510  17.140  1.00 20.00           C
ATOM     42  C   ASP B   3      12.657  14.401  15.648  1.00 20.00           C
ATOM     43  O   ASP B   3      13.577  14.331  14.831  1.00 20.00           O
ATOM     44  CB  ASP B   3      13.696  15.813  17.437  1.00 20.00           C
ATOM     45  CG  ASP B   3      12.931  17.040  16.981  1.00 20.00           C
ATOM     46  OD1 ASP B   3      12.111  17.557  17.768  1.00 20.00           O
ATOM     47  OD2 ASP B   3      13.150  17.488  15.835  1.00 20.00           O
ATOM     48  N   ASN B   4      11.371  14.388  15.306  1.00 20.00           N
ATOM     49  CA  ASN B   4      10.920  14.288  13.919  1.00 20.00           C
ATOM     50  C   ASN B   4      11.477  13.067  13.189  1.00 20.00           C
ATOM     51  O   ASN B   4      12.266  13.198  12.254  1.00 20.00           O
ATOM     52  CB  ASN B   4      11.250  15.568  13.145  1.00 20.00           C
ATOM     53  CG  ASN B   4      10.589  16.796  13.742  1.00 20.00           C
ATOM     54  OD1 ASN B   4       9.513  16.710  14.333  1.00 20.00           O
ATOM     55  ND2 ASN B   4      11.233  17.947  13.589  1.00 20.00           N
ATOM     56  N   ARG B   5      11.060  11.883  13.624  1.00 20.00           N
ATOM     57  CA  ARG B   5      11.515  10.639  13.014  1.00 20.00           C
ATOM     58  C   ARG B   5      10.407   9.992  12.190  1.00 20.00           C
ATOM     59  O   ARG B   5      10.246   8.770  12.206  1.00 20.00           O
ATOM     60  CB  ARG B   5      12.009   9.666  14.087  1.00 20.00           C
ATOM     61  CG  ARG B   5      13.171  10.192  14.914  1.00 20.00           C
ATOM     62  CD  ARG B   5      13.610   9.178  15.957  1.00 20.00           C
ATOM     63  NE  ARG B   5      14.726   9.668  16.761  1.00 20.00           N
ATOM     64  CZ  ARG B   5      15.300   8.978  17.741  1.00 20.00           C
ATOM     65  NH1 ARG B   5      14.864   7.763  18.043  1.00 20.00           N
ATOM     66  NH2 ARG B   5      16.310   9.503  18.421  1.00 20.00           N
ATOM     67  N   ARG B   6       9.653  10.823  11.474  1.00 20.00           N
ATOM     68  CA  ARG B   6       8.549  10.367  10.630  1.00 20.00           C
ATOM     69  C   ARG B   6       7.524   9.555  11.417  1.00 20.00           C
ATOM     70  O   ARG B   6       6.678  10.114  12.115  1.00 20.00           O
ATOM     71  CB  ARG B   6       9.075   9.570   9.429  1.00 20.00           C
ATOM     72  CG  ARG B   6       8.016   9.199   8.398  1.00 20.00           C
ATOM     73  CD  ARG B   6       7.523   7.770   8.581  1.00 20.00           C
ATOM     74  NE  ARG B   6       6.508   7.412   7.594  1.00 20.00           N
ATOM     75  CZ  ARG B   6       5.913   6.225   7.533  1.00 20.00           C
ATOM     76  NH1 ARG B   6       6.230   5.276   8.403  1.00 20.00           N
ATOM     77  NH2 ARG B   6       5.000   5.986   6.601  1.00 20.00           N
TER
END
"""

def map_hkl_map(map_data, fft_map, xrs):
  ma = xrs.structure_factors(d_min = 1).f_calc()
  map_coefficients = ma.structure_factors_from_map(
    map            = map_data,
    use_scale      = True,
    anomalous_flag = False,
    use_sg         = True)
  fft_map = miller.fft_map(
    crystal_gridding     = fft_map,
    fourier_coefficients = map_coefficients)
  fft_map.apply_sigma_scaling()
  return fft_map.real_map_unpadded(), ma

def exercise(prefix="tst4_phenix_map_box"):
  """
  Test to exercise phenix.map_box
  """
  #
  pdbf, mtzf = "%s_model.pdb"%prefix, "%s_map.mtz"%prefix
  pdb_inp = iotbx.pdb.input(source_info=None, lines=pdb_str_answer)
  pdb_inp.write_pdb_file(file_name = pdbf)
  #
  xrs = pdb_inp.xray_structure_simple()
  f_obs = abs(xrs.structure_factors(d_min = 3).f_calc())
  mtz_dataset = f_obs.as_mtz_dataset(column_root_label="F-obs")
  mtz_dataset.add_miller_array(
    miller_array=f_obs.generate_r_free_flags(),
    column_root_label="R-free-flags")
  mtz_object = mtz_dataset.mtz_object()
  mtz_object.write(file_name = mtzf)
  #
  cmd = " ".join([
    "phenix.refine",
    "%s %s"%(pdbf, mtzf),
    "strategy=none",
    "main.number_of_macro_cycles=0",
    "output.prefix=%s"%prefix,
    "--overwrite",
    "--quiet"])
  assert not easy_run.call(cmd)
  miller_arrays = reflection_file_reader.any_reflection_file(
    file_name = "%s_001.mtz"%prefix).as_miller_arrays()
  labels = []
  for ma in miller_arrays:
    l = ",".join(ma.info().labels)
    if(ma.is_complex_array()): labels.append(l)
  assert len(labels) == 4
  #
  for i, l in enumerate(labels):
    cmd = " ".join([
      "phenix.map_box", pdbf, "%s_001.mtz"%prefix,
      "label='%s'"%l,
      ">zlog_%s_%s"%(str(i),prefix)])
    assert not easy_run.call(cmd)

if(__name__ == "__main__"):
  t0 = time.time()
  exercise()
  print("Time: %6.4f"%(time.time()-t0))
