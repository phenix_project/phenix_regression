from __future__ import division
from __future__ import print_function
import time
from libtbx import easy_run
import libtbx.load_env

def exercise(prefix="tst5",density_select=False,get_half_height_width=False,
     keep_map_size=False,keep_origin=False,extract_unique=False,
      output_origin_grid_units=False,
     soft_mask=False):
  """
  Test to exercise phenix.map_box with ncs file
  """
  import os
  pdbf_chain_b = libtbx.env.find_in_repositories(relative_path=\
    "phenix_regression/mmtbx/extract_box_around_model_and_map/%s_chainB.pdb" %prefix,
    test=os.path.isfile)
  pdbf_chain_a = libtbx.env.find_in_repositories(relative_path=\
    "phenix_regression/mmtbx/extract_box_around_model_and_map/%s_chainA.pdb" %prefix,
    test=os.path.isfile)
  pdbf = libtbx.env.find_in_repositories(relative_path=\
     "phenix_regression/mmtbx/extract_box_around_model_and_map/%s.pdb" %prefix,
    test=os.path.isfile)
  mapf = libtbx.env.find_in_repositories(relative_path=\
     "phenix_regression/mmtbx/extract_box_around_model_and_map/%s.ccp4" %prefix,
    test=os.path.isfile)
  ncsf = libtbx.env.find_in_repositories(relative_path=\
     "phenix_regression/mmtbx/extract_box_around_model_and_map/%s.ncs_spec" %prefix,
    test=os.path.isfile)
  assert not None in [pdbf, mapf, ncsf]


  if density_select and get_half_height_width:
    extra="density_select=True get_half_height_width=False"
    result_file_name="output_apply_ncs_density_select_half_height.pdb"
  elif density_select and soft_mask:
    extra="density_select=True soft_mask=True resolution=3"
    result_file_name="output_apply_ncs_density_select.pdb"
  elif density_select:
    extra="density_select=True "
    result_file_name="output_apply_ncs_density_select.pdb"
  elif keep_map_size:
    extra="keep_map_size=True "
    result_file_name="output_apply_ncs_keep_map_size.pdb"
  elif output_origin_grid_units:
    extra="output_origin_grid_units=113,108,136"
    result_file_name="output_apply_ncs_output_origin_grid_units.pdb"
  else:
    extra=""
    result_file_name="output_apply_ncs.pdb"

  if keep_origin:
    extra+=" keep_origin=True prefix=keep_origin output_format=ccp4 "
    result_file_name="output_keep_origin.pdb"
  else:
    extra+=" keep_origin=False"


  if extract_unique:
    extra+=" extract_unique=True prefix=extract_unique resolution=4 "+\
      " molecular_mass=4000 chain_type=RNA"
    result_file_name=None
    pdbf=pdbf_chain_a
  else:
    extra+=" extract_unique=False"

  print("Running map box on ",pdbf, mapf, ncsf,"%s" %(extra))
  cmd = " ".join([
    "phenix.map_box",
    "pdb_file=%s map_file=%s symmetry_file=%s %s" %(pdbf, mapf, ncsf,extra),
      ])
  print(cmd)
  assert not easy_run.call(cmd)

  pdb_file="%s_box.pdb" %prefix
  ncs_file="%s_box.ncs_spec" %prefix

  if not keep_origin and not extract_unique and not output_origin_grid_units:
    # run get_cc_mtz_pdb
    cmd = " ".join([
      "phenix.apply_ncs",
      "pdb_in=%s ncs_in=%s pdb_out=%s"%(pdb_file,ncs_file,result_file_name)
        ])
    assert not easy_run.call(cmd)
    assert os.path.isfile(result_file_name)

  else:
    # run map_model_cc
    assert keep_origin or extract_unique or output_origin_grid_units
    if keep_origin:
      cmd = "phenix.map_model_cc force keep_origin.ccp4 %s resolution=3 " %(pdbf)
    elif extract_unique:
      cmd = "phenix.map_model_cc force extract_unique.ccp4 extract_unique.pdb resolution=3 ignore_symmetry_conflicts=True "
    elif output_origin_grid_units:
      cmd = "phenix.map_model_cc force %s %s resolution=3 ignore_symmetry_conflicts=True"  %(pdb_file,mapf)
    print("Getting map_model cc with: %s" %(cmd))
    result=easy_run.fully_buffered(cmd)
    from six.moves import cStringIO as StringIO
    f=StringIO()
    result.show_stdout()
    result.show_stdout(out=f)
    print(f.getvalue())
    value=None
    for line in f.getvalue().splitlines():
     line=line.strip()
     if line and len(line.split())==3 and line.split()[0]=='CC_mask':
       value=float(line.split()[2])
    from libtbx.test_utils import approx_equal
    if keep_origin or output_origin_grid_units:
      expected_value=0.7118
    elif extract_unique:
      expected_value=0.7118
    print("VALUE:",value," EXPECTED:",expected_value)
    assert approx_equal(value,expected_value,eps=.01)
    print("OK")
    return



  if density_select and get_half_height_width:
    file_name="%s_apply_ncs_density_select_half_height.pdb"  %prefix
  elif density_select:
    file_name="%s_apply_ncs_density_select.pdb"  %prefix
  elif keep_map_size:
    file_name="%s_apply_ncs_keep_map_size.pdb"  %prefix
  elif output_origin_grid_units:
    file_name="%s_apply_ncs_output_origin_grid_units.pdb"  %prefix
  elif keep_origin:
    file_name="%s_apply_ncs.pdb"  %prefix

  else:
    file_name="%s_box_move_origin_apply_ncs.pdb" %prefix

  ncspdbf = libtbx.env.find_in_repositories(relative_path=\
  "phenix_regression/mmtbx/extract_box_around_model_and_map/%s" %file_name,
    test=os.path.isfile)
  text2=open(result_file_name).read().replace("\nTER","").replace("\n\n","\n")
  f=open('tmp.pdb','w')
  print(text2, file=f)
  f.close()
  print("Files:",file_name,ncspdbf,'tmp.pdb')
  text1=open(ncspdbf).read().replace("\nTER","").replace("\n\n","\n")
  for line1, line2 in zip(text1.splitlines(),text2.splitlines()):
    if line1.strip()!=line2.strip():
      print("Expected: ",line1)
      print("Found   : ",line2)
      assert line1.strip()==line2.strip()

  print("OK")

if(__name__ == "__main__"):
  t0 = time.time()
  exercise(extract_unique=True)
  """
  exercise()
  exercise(extract_unique=True)
  exercise(density_select=True)
  exercise(density_select=True, soft_mask=True)
  exercise(density_select=True,get_half_height_width=True)
  exercise(keep_map_size=True)
  exercise(keep_origin=True)
  exercise(output_origin_grid_units=True)
  """
  print("Time: %6.4f"%(time.time()-t0))
