cmd.bg_color("white")
                                                 #     model(s)
load ./AtomsInBox.pdb, mod1
select cmod1, (all)
cmd.hide("everything","mod1")
cmd.show("sticks"    ,"cmod1")
cmd.show("spheres"   ,"cmod1")

load ./map.xplor, map1, 1 , xplor
isomesh mesh1, map1, 2.5
cmd.color("blue","mesh1")
 
                                                 #
                                                 #     ball&stic settings
set stick_radius,0.04
#util.cbaw("pmod")
set_color cyan,[ 0.38, 0.78, 0.80]
set_color blue,[ 0.11, 0.25, 0.88]
set_color red,[ 1.00, 0.13, 0.13]
#color grey, mod1 
color black, cmod2
#color red, all # for the final picture only
#color black, name ca | name n | name c | name O
set sphere_scale,0.075
                                                  #    map settings
set mesh_width,0.15
set mesh_radius,0.001 #0.012
                                                  #    perfomance
set direct,1.
set orthoscopic,on
set ray_trace_fog_start,0.5
util.performance(0)
util.ray_shadows('light')
cmd.space('cmyk')
set antialias,on
                                                  #
#set_view (\
#     0.230349854,    0.278288841,    0.932475150,\
#    -0.418529630,    0.893415749,   -0.163239956,\
#    -0.878513455,   -0.352665156,    0.322270870,\
#     0.000000000,    0.000000000,  -65.040390015,\
#    20.769443512,  -19.281919479,  -19.682399750,\
#    50.228534698,   79.852233887,   20.000000000 )
#rebuild
#
