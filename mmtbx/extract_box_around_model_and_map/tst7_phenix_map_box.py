from __future__ import print_function

from libtbx import easy_run
from libtbx.test_utils import approx_equal
from iotbx.data_manager import DataManager

def exercise():

  # Get a map and model:

  from iotbx.map_model_manager import map_model_manager
  mmm=map_model_manager()
  mmm.generate_map()

  mmm_sav=mmm.deep_copy()

  # Write out original model and map (not shifted)
  pdbf='m1.pdb'
  mapf='m1.mrc'
  mmm.write_model(pdbf)
  mmm.write_map(mapf)



  # Run standard
  map_manager,model=run_one(pdbf,mapf,args=[],prefix='std')
  assert map_manager.unit_cell_crystal_symmetry().is_similar_symmetry(
     mmm_sav.map_manager().unit_cell_crystal_symmetry())
  assert model.crystal_symmetry().is_similar_symmetry(
      map_manager.unit_cell_crystal_symmetry())

  # Reset output_cell_dimensions
  map_manager,model=run_one(pdbf,mapf,args=[
    'keep_input_unit_cell_and_grid=False',
    'output_unit_cell=20,20,20','keep_map_size=True'],prefix='std')
  assert map_manager.unit_cell().parameters()[:3]==(20,20,20)
  assert model.crystal_symmetry().unit_cell().parameters()[:3]==(20,20,20)

  # Match a file with origin
  map_manager,model=run_one(pdbf,mapf,args=[
    'ignore_symmetry_conflicts=true',
    'output_origin_match_this_file=std.ccp4','keep_map_size=True'],
      prefix='std1')
  assert approx_equal(map_manager.unit_cell().parameters()[:3],
       (22.411, 28.923, 23.598))
  assert approx_equal(model.crystal_symmetry().unit_cell().parameters()[:3],
       (22.411, 28.923, 23.598))

  # Match a file with bounds
  map_manager,model=run_one(pdbf,mapf,args=[
    'ignore_symmetry_conflicts=true',
    'bounds_match_this_file=std.ccp4'],
      prefix='std2')
  assert approx_equal(map_manager.unit_cell().parameters()[:3],
       (22.410999298095703, 28.92300033569336, 23.597999572753906))
  assert approx_equal(model.crystal_symmetry().unit_cell().parameters()[:3],
       (22.410999298095703, 28.92300033569336, 23.597999572753906))


  # set it up as a match_map_model_ncs object so that we can set origin
  mmm=mmm.as_match_map_model_ncs()
  # write out model and map shifted
  mmm.set_original_origin_and_gridding(
       original_origin=(10,10,10))
  pdbf_offset='m1_offset.pdb'
  mapf_offset='m1_offset.mrc'
  mmm.write_model(pdbf_offset)
  mmm.write_map(mapf_offset)
  map_manager,model=run_one(pdbf_offset,mapf_offset,args=['keep_map_size=true'],
      prefix='shifted')
  assert map_manager.shift_cart() == (0.0, 0.0, 0.0)
  assert approx_equal(
     map_manager.unit_cell_crystal_symmetry().unit_cell().parameters(),
       (22.411, 28.923, 23.598, 90, 90, 90))
  assert approx_equal(
     map_manager.crystal_symmetry().unit_cell().parameters(),
       (22.411, 28.923, 23.598, 90, 90, 90))
  assert approx_equal(
    map_manager.origin_shift_grid_units,(0,0,0))

  assert approx_equal(
   map_manager.map_data().origin(),(10,10,10))

  assert approx_equal(
     model.crystal_symmetry().unit_cell().parameters(),
       (22.411, 28.923, 23.598, 90, 90, 90))

  # Create an ncs object, run map and model and make sure
  #  shift_cart is set and same as shift_cart for model and ncs
  from mmtbx.ncs.ncs import ncs
  ncs_object=ncs()

  import iotbx.map_model_manager
  mam=iotbx.map_model_manager.map_model_manager(map_manager=map_manager,
    model=model,
    ncs_object=ncs_object)
  assert approx_equal(mam.map_manager().shift_cart(),
    (-7.470333099365235, -7.230750083923341, -7.374374866485596))
  assert approx_equal(mam.ncs_object().shift_cart(),
           mam.map_manager().shift_cart())
  assert approx_equal(mam.ncs_object().shift_cart(),
         mam.model().shift_cart())

def run_one(pdbf,mapf,args=None,prefix=None):

  # run standard map_box to check
  if not args: args=[]
  args+=['prefix=%s' %(prefix)]
  print(("Running map box on ",pdbf, mapf,args))
  cmd = " ".join([
    "phenix.map_box",
    "pdb_file=%s map_file=%s" %(pdbf, mapf),
      ]+args)
  print(cmd)
  assert not easy_run.call(cmd)

  dm = DataManager(['model', 'real_map'])
  dm.set_overwrite(True)

  # Read in map and model

  dm.process_real_map_file("%s.ccp4" %(prefix))
  map_manager= dm.get_real_map("%s.ccp4" %(prefix))

  dm.process_model_file("%s.pdb" %(prefix))
  model = dm.get_model("%s.pdb" %(prefix))
  return map_manager,model


if (__name__ == "__main__"):
  import time
  t0 = time.time()
  exercise()
  print("Time: %6.4f"%(time.time()-t0))
