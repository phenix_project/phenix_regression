from __future__ import print_function
import iotbx.pdb
from scitbx.array_family import flex
from cctbx import maptbx
from libtbx.math_utils import ifloor, iceil, iround
from cctbx import uctbx
from cctbx import xray
from cctbx import crystal
import iotbx.xplor.map
from libtbx import adopt_init_args
from cctbx import miller
import mmtbx.utils
import time
from mmtbx import monomer_library
import mmtbx.monomer_library.server
import mmtbx.monomer_library.pdb_interpretation
import libtbx.load_env
import os

def exercise(file_name = "m.pdb",
             selection_string = "chain A and resseq 9:16",
             selection_radius=5,
             box_offset = 2,
             resolution_factor = 0.2,
             d_min = 1.5):
  file_name = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/mmtbx/extract_box_around_model_and_map/m.pdb",
    test=os.path.isfile)
  pdb_inp = iotbx.pdb.input(file_name = file_name)
  pdb_hierarchy = pdb_inp.construct_hierarchy()
  pdb_atoms = pdb_hierarchy.atoms()
  pdb_atoms.reset_i_seq()
  xray_structure = pdb_inp.xray_structure_simple()
  #
  mon_lib_srv = monomer_library.server.server()
  ener_lib = monomer_library.server.ener_lib()
  processed_pdb_file = monomer_library.pdb_interpretation.process(
     mon_lib_srv               = mon_lib_srv,
     ener_lib                  = ener_lib,
     file_name                 = file_name,
     raw_records               = None,
     force_symmetry            = True)
  pdb_hierarchy = processed_pdb_file.all_chain_proxies.pdb_hierarchy
  pdb_hierarchy_orig = pdb_hierarchy.deep_copy()
  geometry = processed_pdb_file.geometry_restraints_manager(
    show_energies = False, plain_pairs_radius = 5.0)
  # get a map (fcalc map) for whole unit cell
  f_calc = xray_structure.structure_factors(d_min = d_min).f_calc()
  fft_map = f_calc.fft_map(resolution_factor=resolution_factor)
  fft_map.apply_sigma_scaling()
  map_data = fft_map.real_map_unpadded()
  #
  selection_moving = pdb_hierarchy.atom_selection_cache().selection(
    string = selection_string)
  selection_within = xray_structure.selection_within(
    radius    = selection_radius,
    selection = selection_moving)
  sel = selection_moving.select(selection_within)
  # extact box
  selection = pdb_hierarchy.atom_selection_cache().selection(
    string = selection_string)
  selection = xray_structure.selection_within(
    radius    = selection_radius,
    selection = selection)
  r = mmtbx.utils.extract_box_around_model_and_map(
    xray_structure = xray_structure.deep_copy_scatterers().select(selection),
    map_data       = map_data,
    box_cushion    = box_offset)
  #
  ph_box = pdb_hierarchy.select(selection)
  ph_box.adopt_xray_structure(r.xray_structure_box)
  ph_box.write_pdb_file(file_name="AtomsInBox.pdb", crystal_symmetry =
    r.xray_structure_box.crystal_symmetry())
  #
  r.write_xplor_map(file_name="map.xplor")
  mc = r.map_coefficients(d_min=d_min, resolution_factor=resolution_factor,
    file_name="box.mtz")
  # get new grm corresponding to new crystal symmetry
  phb = ph_box.deep_copy()
  phb.adopt_xray_structure(r.xray_structure_box)
  phb.write_pdb_file(file_name="tmp.pdb",
    crystal_symmetry = r.xray_structure_box.crystal_symmetry())
  processed_pdb_file = monomer_library.pdb_interpretation.process(
   mon_lib_srv = mon_lib_srv,
   ener_lib    = ener_lib,
   file_name   = "tmp.pdb")
  geometry = processed_pdb_file.geometry_restraints_manager(
    show_energies = False, plain_pairs_radius = 5.0)

if (__name__ == "__main__"):
  t0 = time.time()
  exercise()
  print("Time: %6.4f"%(time.time()-t0))
