from __future__ import print_function
import iotbx.pdb
from scitbx.array_family import flex
from cctbx import maptbx
from libtbx.math_utils import ifloor, iceil, iround
from cctbx import uctbx
from cctbx import xray
from cctbx import crystal
import iotbx.xplor.map
from libtbx import adopt_init_args
from cctbx import miller
import mmtbx.utils
import time,os
from mmtbx import monomer_library
import mmtbx.monomer_library.server
import mmtbx.monomer_library.pdb_interpretation
import libtbx.load_env

def exercise(selection_string = "chain A and resseq 9:16",
             selection_radius=3,
             box_offset = 2,
             resolution_factor = 0.1,
             d_min = 2.0):
  file_name = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/mmtbx/extract_box_around_model_and_map/m.pdb",
    test=os.path.isfile)
  pdb_inp = iotbx.pdb.input(file_name = file_name)
  pdb_hierarchy = pdb_inp.construct_hierarchy()
  pdb_atoms = pdb_hierarchy.atoms()
  pdb_atoms.reset_i_seq()
  xray_structure = pdb_inp.xray_structure_simple()
  #
  mon_lib_srv = monomer_library.server.server()
  ener_lib = monomer_library.server.ener_lib()
  processed_pdb_file = monomer_library.pdb_interpretation.process(
     mon_lib_srv               = mon_lib_srv,
     ener_lib                  = ener_lib,
     file_name                 = file_name,
     raw_records               = None,
     force_symmetry            = True)
  pdb_hierarchy = processed_pdb_file.all_chain_proxies.pdb_hierarchy
  pdb_hierarchy_orig = pdb_hierarchy.deep_copy()
  geometry = processed_pdb_file.geometry_restraints_manager(
    show_energies = False, plain_pairs_radius = 5.0)

  # get a map (fcalc map) for whole unit cell
  f_calc = xray_structure.structure_factors(d_min = d_min).f_calc()
  fft_map = f_calc.fft_map(resolution_factor=resolution_factor)
  fft_map.apply_sigma_scaling()
  map_data = fft_map.real_map_unpadded()
  #
  selection = pdb_hierarchy.atom_selection_cache().selection(
    string = selection_string)
  sw = xray_structure.selection_within(
    radius    = selection_radius,
    selection = selection)
  #
  xray_structure_selected = xray_structure.select(
    selection=sw)
  mv1 = flex.double()
  for site_frac in xray_structure_selected.sites_frac():
    mv1.append(map_data.eight_point_interpolation(site_frac))
  #
  # extact box
  selection = pdb_hierarchy.atom_selection_cache().selection(
    string = selection_string)
  selection = xray_structure.selection_within(
    radius    = selection_radius,
    selection = selection)
  r = mmtbx.utils.extract_box_around_model_and_map(
    xray_structure = xray_structure.deep_copy_scatterers().select(selection),
    map_data       = map_data,
    box_cushion    = box_offset)
  mc = r.map_coefficients(d_min=d_min, resolution_factor=resolution_factor,
    file_name="box.mtz")
  #
  mv2 = flex.double()
  for site_frac in r.xray_structure_box.sites_frac():
    mv2.append(r.map_box.eight_point_interpolation(site_frac))
  #
  fft_map = mc.fft_map(resolution_factor=resolution_factor)
  fft_map.apply_sigma_scaling()
  map_data_box_from_mc = fft_map.real_map_unpadded()
  mv3 = flex.double()
  for site_frac in r.xray_structure_box.sites_frac():
    mv3.append(map_data_box_from_mc.eight_point_interpolation(site_frac))
  #
  mv4 = flex.double()
  rmb = r.map_box
  s = maptbx.statistics(rmb).sigma()
  rmb = rmb/s
  for site_frac in r.xray_structure_box.sites_frac():
    mv4.append(rmb.eight_point_interpolation(site_frac))
  #
  if(0):
    for m1,m2,m3,m4 in zip(mv1,mv2,mv3,mv4):
      print(m1,m2,m3,m4)
  #
  assert flex.sum(flex.abs(mv1-mv2))/mv2.size() < 0.25
  assert flex.sum(flex.abs(mv3-mv4))/mv3.size() < 0.20


if (__name__ == "__main__"):
  t0 = time.time()
  exercise()
  print("Time: %6.4f"%(time.time()-t0))
