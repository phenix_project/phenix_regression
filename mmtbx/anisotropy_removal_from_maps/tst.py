from __future__ import print_function
from cctbx.array_family import flex
import libtbx.load_env
from iotbx import reflection_file_reader
import iotbx.pdb
import os, time
from libtbx import easy_run
from libtbx.test_utils import approx_equal

overall_prefix="anisotropy_removal_from_maps"

def exercise_00():
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/mmtbx/anisotropy_removal_from_maps/m.pdb",
    test=os.path.isfile)
  hkl_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/mmtbx/anisotropy_removal_from_maps/d.mtz",
    test=os.path.isfile)
  #
  cmd = " ".join([
    "phenix.refine",
    "%s"%pdb_file,
    "%s"%hkl_file,
    "strategy=None",
    "main.number_of_mac=1",
    "output.prefix=%s"%overall_prefix,
    "--overwrite",
    "--quiet",
    "> %s.zlog"%overall_prefix
    ])
  print(cmd)
  assert not easy_run.call(cmd)
  assert os.path.getsize("%s.zlog"%overall_prefix) == 0
  #
  miller_arrays1 = reflection_file_reader.any_reflection_file(file_name =
    "%s_001.mtz"%overall_prefix).as_miller_arrays()
  miller_arrays2 = reflection_file_reader.any_reflection_file(file_name =
    hkl_file).as_miller_arrays()
  for ma1 in miller_arrays1:
    l1 = ma1.info().labels
    if(l1 == ['2FOFCWT', 'PH2FOFCWT']):                 ma1_2mFoDFc   = ma1.deep_copy()
    if(l1 == ['2FOFCWT_no_fill', 'PH2FOFCWT_no_fill']): ma1_2mFoDFc_f = ma1.deep_copy()
    for ma2 in miller_arrays2:
      l2 = ma2.info().labels
      if(l2 == ['2FOFCWT', 'PH2FOFCWT']):                 ma2_2mFoDFc        = ma2.deep_copy()
      if(l2 == ['2FOFCWT_no_fill', 'PH2FOFCWT_no_fill']): ma2_2mFoDFc_f      = ma2.deep_copy()
      if(l2 == ['FWT', 'PHWT']):                          ma2_2mFoDFc_refmac = ma2.deep_copy()
  #
  m2_2mFoDFc        = ma2_2mFoDFc       .fft_map(resolution_factor=1./3).real_map_unpadded()
  m2_2mFoDFc_f      = ma2_2mFoDFc_f     .fft_map(resolution_factor=1./3).real_map_unpadded()
  m2_2mFoDFc_refmac = ma2_2mFoDFc_refmac.fft_map(resolution_factor=1./3).real_map_unpadded()
  # Current maps
  cc1=flex.linear_correlation(x=m2_2mFoDFc.as_1d(),   y=m2_2mFoDFc_refmac.as_1d()).coefficient()
  cc2=flex.linear_correlation(x=m2_2mFoDFc_f.as_1d(), y=m2_2mFoDFc_refmac.as_1d()).coefficient()
  cc3=flex.linear_correlation(x=m2_2mFoDFc_f.as_1d(), y=m2_2mFoDFc.as_1d()).coefficient()
  assert approx_equal([cc1, cc2, cc3], [0.9544, 0.9459, 0.9884], 1.e-4)
  # Answer maps
  m1_2mFoDFc   = ma1_2mFoDFc.fft_map(resolution_factor=1./3).real_map_unpadded()
  m1_2mFoDFc_f = ma1_2mFoDFc_f.fft_map(resolution_factor=1./3).real_map_unpadded()
  #
  cc1=flex.linear_correlation(x=m1_2mFoDFc.as_1d(),   y=m2_2mFoDFc.as_1d()).coefficient()
  cc2=flex.linear_correlation(x=m1_2mFoDFc_f.as_1d(), y=m2_2mFoDFc_f.as_1d()).coefficient()
  assert approx_equal(cc1, 1.0, 0.1), cc1
  assert approx_equal(cc2, 1.0, 0.1), cc2

if (__name__ == "__main__"):
  t0 = time.time()
  exercise_00()
  print("Time: %6.4f" % (time.time()-t0))
