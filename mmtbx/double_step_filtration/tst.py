from __future__ import print_function
from scitbx.array_family import flex
import iotbx.pdb
from cctbx import miller
import mmtbx.f_model
import random
import libtbx.load_env
import os

random.seed(0)
flex.set_random_seed(0)

def map_cc(mc1, mc2):
  map_coeffs_1, map_coeffs_2 = mc1,mc2
  fft_map_1 = map_coeffs_1.fft_map(resolution_factor=0.3)
  map_1 = fft_map_1.real_map_unpadded()
  fft_map_2 = miller.fft_map(
    crystal_gridding = fft_map_1,
    fourier_coefficients = map_coeffs_2)
  map_2 = fft_map_2.real_map_unpadded()
  assert map_1.size() == map_2.size()
  m1 = flex.double(list(map_1))
  m2 = flex.double(list(map_2))
  return flex.linear_correlation(x = m1, y = m2).coefficient()

def option_0(fmodel):
  k = fmodel.k_isotropic()*fmodel.k_anisotropic()
  tmfmdfc = fmodel.electron_density_map(
    ).map_coefficients(map_type = "2mFo-DFc")
  tmfmdfc = tmfmdfc.customized_copy(data = tmfmdfc.data()*(1/k) )
  return tmfmdfc

def option_1(fmodel, complete_set, d_min=None, scale_to=True):
  k = fmodel.k_isotropic()*fmodel.k_anisotropic()
  f_obs = fmodel.f_obs().customized_copy(data = fmodel.f_obs().data()/k)
  tmfmdfc = fmodel.electron_density_map(
    ).map_coefficients(map_type = "2mFo-DFc")
  tmfmdfc = tmfmdfc.customized_copy(data = tmfmdfc.data()*(1/k) )

  if(scale_to):
    f_dsf = tmfmdfc.double_step_filtration(
      complete_set = complete_set,
      scale_to     = fmodel.f_obs())
  else:
    f_dsf = tmfmdfc.double_step_filtration(
      complete_set = complete_set)
  if(d_min is not None): f_dsf = f_dsf.resolution_filter(d_min = d_min)
  f_obs_completed = f_obs.complete_with(other = abs(f_dsf))
  fmodel_ = mmtbx.f_model.manager(
    f_obs          = f_obs_completed,
    xray_structure = fmodel.xray_structure)
  fmodel_.update_all_scales(update_f_part1=False)
  tmfmdfc = fmodel_.electron_density_map(
    ).map_coefficients(map_type = "2mFo-DFc")

  k = fmodel_.k_isotropic()*fmodel_.k_anisotropic()
  tmfmdfc = tmfmdfc.customized_copy(data = tmfmdfc.data()*(1/k) )

  return tmfmdfc

def option_2(fmodel, complete_set, d_min=None):
  k = fmodel.k_isotropic()*fmodel.k_anisotropic()
  f_obs = fmodel.f_obs().customized_copy(data = fmodel.f_obs().data()/k)
  tmfmdfc = fmodel.electron_density_map(
    ).map_coefficients(map_type = "2mFo-DFc")
  tmfmdfc = tmfmdfc.customized_copy(data = tmfmdfc.data()*(1/k) )
  f_dsf = tmfmdfc.double_step_filtration(
    complete_set = complete_set,
    scale_to     = fmodel.f_obs())
  if(d_min is not None): f_dsf = f_dsf.resolution_filter(d_min = d_min)
  tmfmdfc = tmfmdfc.complete_with(other=f_dsf)
  return tmfmdfc

def option_3(fmodel, complete_set, d_min=None):
  k = fmodel.k_isotropic()*fmodel.k_anisotropic()
  f_obs = fmodel.f_obs().customized_copy(data = fmodel.f_obs().data()/k)
  tmfmdfc = fmodel.electron_density_map(
    ).map_coefficients(map_type = "2mFo-DFc")
  tmfmdfc = tmfmdfc.customized_copy(data = tmfmdfc.data()*(1/k) )
  f_dsf = tmfmdfc.double_step_filtration(
    complete_set = complete_set)
  if(d_min is not None): f_dsf = f_dsf.resolution_filter(d_min = d_min)
  tmfmdfc = tmfmdfc.complete_with(other=f_dsf)
  return tmfmdfc

def option_4(fmodel, complete_set, d_min=None):
  k = fmodel.k_isotropic()*fmodel.k_anisotropic()
  f_obs = fmodel.f_obs().customized_copy(data = fmodel.f_obs().data()/k)
  tmfmdfc = fmodel.electron_density_map(
    ).map_coefficients(map_type = "2mFo-DFc")
  tmfmdfc = tmfmdfc.customized_copy(data = tmfmdfc.data()*(1/k) )
  f_dsf = tmfmdfc.double_step_filtration(
    complete_set = complete_set,
    scale_to     = fmodel.f_obs())
  if(d_min is not None): f_dsf = f_dsf.resolution_filter(d_min = d_min)
  f_obs_completed = f_obs.complete_with(other = abs(f_dsf))
  fmodel_ = mmtbx.f_model.manager(
    f_obs          = f_obs_completed,
    xray_structure = fmodel.xray_structure)
  fmodel_.update_all_scales(update_f_part1=False)
  tmfmdfc_ = fmodel_.electron_density_map(
    ).map_coefficients(map_type = "2mFo-DFc")

  k = fmodel_.k_isotropic()*fmodel_.k_anisotropic()
  tmfmdfc_ = tmfmdfc_.customized_copy(data = tmfmdfc_.data()*(1/k) )

  tmfmdfc = tmfmdfc.complete_with(other=tmfmdfc_)
  return tmfmdfc

def run():
  pdb_file_name = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/mmtbx/double_step_filtration/model.pdb",
    test=os.path.isfile)
  pdb_inp = iotbx.pdb.input(file_name = pdb_file_name)
  xrs = pdb_inp.xray_structure_simple()
  xrs.scattering_type_registry(table = "wk1995")
  for option in ["random", "smallest", "highest"]:
    print()
    print("data incompleteness:",option,"-"*30)
    #
    fmodel = mmtbx.f_model.manager(
      f_obs          = abs(xrs.structure_factors(d_min = 2.5).f_calc()),
      xray_structure = xrs,
      k_sol          = 0.35,
      b_sol          = 50.0,
      b_cart         = [2 ,3 ,-5 ,0,0,0]
      )
    f_exact = fmodel.f_model()
    f_exact = f_exact.customized_copy(data = f_exact.data()*5)
    #
    remove_fraction = 0.2
    if(option=="highest"):
      s = flex.sort_permutation(abs(f_exact).data(), reverse=True)
      f_exact = f_exact.select(s)
      n_remove = int(s.size()*remove_fraction)
      f_poor = f_exact.customized_copy(
        data    = f_exact.data()[n_remove:],
        indices = f_exact.indices()[n_remove:])
    elif(option == "smallest"):
      s = flex.sort_permutation(abs(f_exact).data(), reverse=True)
      f_exact = f_exact.select(s)
      n_remove = int(s.size()*remove_fraction)
      sz = f_exact.data().size()
      f_poor = f_exact.customized_copy(
        data    = f_exact.data()[:sz-n_remove],
        indices = f_exact.indices()[:sz-n_remove])
    elif(option == "random"):
      s = flex.random_bool(f_exact.data().size(), 1.-remove_fraction)
      f_poor = f_exact.select(s)
    #
    f_obs = abs(f_poor)
    #
    #print "number of all data:", f_exact.data().size()
    #print "number of incomplete data:", f_obs.data().size()
    print("start CC(exact_map, bad1): ", map_cc(mc1=f_exact, mc2=f_poor))
    #
    xrs_sh = xrs.deep_copy_scatterers()
    xrs_sh.shake_sites_in_place(mean_distance=2.0)
    xrs_sh.shake_adp()
    fmodel = mmtbx.f_model.manager(
      f_obs          = f_obs,
      xray_structure = xrs_sh)

    print("r(before):", fmodel.r_work())
    fmodel.update_all_scales(update_f_part1=False)
    #print "r(after) :", fmodel.r_work()
    poor_map = fmodel.electron_density_map(
      ).map_coefficients(map_type = "2mFo-DFc")
    print("start CC(exact_map, bad2): ", map_cc(mc1=f_exact, mc2=poor_map))
    #
    print()
    o0 = option_0(fmodel = fmodel.deep_copy())
    print("CC(exact_map, poor_map)1: ", map_cc(mc1=f_exact, mc2=o0))
    print()
    ####
    o1 = option_1(fmodel = fmodel.deep_copy(), complete_set=f_exact)
    print("CC(exact_map, poor_map)1: ", map_cc(mc1=f_exact, mc2=o1))
    #
    o1 = option_1(fmodel = fmodel.deep_copy(), complete_set=f_exact, scale_to=False)
    print("CC(exact_map, poor_map)11: ", map_cc(mc1=f_exact, mc2=o1))
    #
    o2 = option_2(fmodel = fmodel.deep_copy(), complete_set=f_exact)
    print("CC(exact_map, poor_map)2: ", map_cc(mc1=f_exact, mc2=o2))
    #
    o3 = option_3(fmodel = fmodel.deep_copy(), complete_set=f_exact)
    print("CC(exact_map, poor_map)3: ", map_cc(mc1=f_exact, mc2=o3))
    #
    o4 = option_4(fmodel = fmodel.deep_copy(), complete_set=f_exact)
    print("CC(exact_map, poor_map)3: ", map_cc(mc1=f_exact, mc2=o4))
    ######
    print()
    #
    o1 = option_1(fmodel = fmodel.deep_copy(), complete_set=f_exact, d_min=8)
    print("CC(exact_map, poor_map)1: ", map_cc(mc1=f_exact, mc2=o1))
    #
    o1 = option_1(fmodel = fmodel.deep_copy(), complete_set=f_exact, scale_to=False)
    print("CC(exact_map, poor_map)11: ", map_cc(mc1=f_exact, mc2=o1))
    #
    o2 = option_2(fmodel = fmodel.deep_copy(), complete_set=f_exact, d_min=8)
    print("CC(exact_map, poor_map)2: ", map_cc(mc1=f_exact, mc2=o2))
    #
    o3 = option_3(fmodel = fmodel.deep_copy(), complete_set=f_exact, d_min=8)
    print("CC(exact_map, poor_map)3: ", map_cc(mc1=f_exact, mc2=o3))
    #
    o4 = option_4(fmodel = fmodel.deep_copy(), complete_set=f_exact, d_min=8)
    print("CC(exact_map, poor_map)3: ", map_cc(mc1=f_exact, mc2=o4))


if (__name__ == "__main__"):
  run()
