#! /bin/csh -fe

set PHENIX_REGRESSION_DIR="`libtbx.find_in_repositories phenix_regression`"

mmtbx.python "`libtbx.show_dist_paths mmtbx`/examples/reindex.py" xray_data.file_name="$PHENIX_REGRESSION_DIR"/refinement/data/1yjp.mtz model.file_name="$PHENIX_REGRESSION_DIR"/refinement/data/1yjp.pdb standard_laws=invert chain_id_increment=3 | grep 'Writing log file' | libtbx.assert_stdin "Writing log file with name reindex.log"

echo "OK"
