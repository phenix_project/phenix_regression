from __future__ import print_function
import iotbx.pdb
from libtbx import easy_run
import libtbx.load_env
import time
import os

def test_result_from_command(cmd, lines, fname = "1ywf.pdb_ss.eff"):
  print(cmd)
  assert not easy_run.call(cmd)
  out = open(fname, "r")
  out_lines = out.readlines()
  out.close()
  for needed_string in lines:
    assert needed_string in out_lines, "'%s' not in output!" % needed_string

def test_result_with_sorry(cmd, lines):
  print(cmd)
  fb = easy_run.fully_buffered(cmd)
  for needed_string in lines:
    assert needed_string in fb.stderr_lines, "'%s' not in stderr!" % needed_string

def exercise_11(prefix="tst_ss_restr_11"):
  pdb_path = libtbx.env.find_in_repositories(
      relative_path="phenix_regression/pdb/1ywf.pdb",
      test=os.path.isfile)
  cmd = " ".join([
      "phenix.secondary_structure_restraints",
      "%s" % pdb_path,
      "prefix=%s" % prefix])
  lines = ["pdb_interpretation {\n",
           "    enabled = True\n",
           "        selection = chain 'A' and resid 16 through 18\n"]
  test_result_from_command(cmd, lines, fname="%s_ss.eff" % prefix)

def exercise_12(prefix="tst_ss_restr_12"):
  pdb_path = libtbx.env.find_in_repositories(
      relative_path="phenix_regression/pdb/1ywf.pdb",
      test=os.path.isfile)
  cmd = " ".join([
      "phenix.secondary_structure_restraints",
      "%s" % pdb_path,
      "format=phenix_refine",
      "prefix=%s" % prefix])
  lines= ["refinement.pdb_interpretation {\n",
          "    enabled = True\n",
          "        selection = chain 'A' and resid 16 through 18\n"]
  test_result_from_command(cmd, lines, fname="%s_ss.eff" % prefix)

def exercise_13(prefix="tst_ss_restr_13"):
  pdb_path = libtbx.env.find_in_repositories(
      relative_path="phenix_regression/pdb/1ywf.pdb",
      test=os.path.isfile)
  cmd = " ".join([
      "phenix.secondary_structure_restraints",
      "%s" % pdb_path,
      "format=pymol",
      "prefix=%s" % prefix])
  lines= ["dist chain \"A\" and resi 41 and name  N   and alt '', chain \"A\" and resi 37 and name  O   and alt ''\n",
          "dist chain \"A\" and resi 42 and name  N   and alt '', chain \"A\" and resi 38 and name  O   and alt ''\n",
          "angle a0, chain \"A\" and resi 37 and name  C   and alt '', chain \"A\" and resi 37 and name  O   and alt '', chain \"A\" and resi 41 and name  N   and alt ''\n"]
  test_result_from_command(cmd, lines, fname="%s_ss.pml" % prefix)

def exercise_2(prefix="tst_ss_restr_2"):
  """
  testing that cablam method doesn't fail
  """
  pdb_path = libtbx.env.find_in_repositories(
      relative_path="phenix_regression/pdb/1ywf.pdb",
      test=os.path.isfile)
  cmd = " ".join([
      "phenix.secondary_structure_restraints",
      "%s" % pdb_path,
      "search_method=cablam",
      "prefix=%s" % prefix])
  lines = ["pdb_interpretation {\n",
           "    enabled = True\n",
           "      search_method = ksdssp from_ca *cablam\n"]
  test_result_from_command(cmd, lines, fname="%s_ss.eff" % prefix)

def exercise_3(prefix="tst_ss_restr_3"):
  """
  testing option "ignore_annotation_in_file"
  """
  pdb_path = libtbx.env.find_in_repositories(
      relative_path="phenix_regression/pdb/1ywf.pdb",
      test=os.path.isfile)
  cmd = " ".join([
      "phenix.secondary_structure_restraints",
      "%s" % pdb_path,
      "ignore_annotation_in_file=True",
      "prefix=%s" % prefix])
  lines = ["        selection = chain 'A' and resid 15 through 17\n",
           "        selection = chain 'A' and resid 38 through 47\n",
           "        selection = chain 'A' and resid 58 through 64\n"]
  test_result_from_command(cmd, lines, fname="%s_ss.eff" % prefix)

def exercise_4(prefix="tst_ss_restr_4"):
  """
  Testing that setting options specific to from_ca search method lead to Sorry
  and works when search_method=from_ca
  """

  pdb_path = libtbx.env.find_in_repositories(
      relative_path="phenix_regression/pdb/1ywf.pdb",
      test=os.path.isfile)
  for i, (par, value) in enumerate([('ss_by_chain', 'False'),
        ('max_rmsd', '2'),
        ('from_ca_conservative', 'True'),
        ('use_representative_chains', 'False'),
        ('max_representative_chains', '2')]):
    cmd = " ".join([
        "phenix.secondary_structure_restraints",
        "%s" % pdb_path,
        "%s=%s" % (par, value)])
    lines = ["Sorry: %s parameter is only used when search_method=from_ca." % par,
            "Please do not set it for other methods to avoid confusion."]
    test_result_with_sorry(cmd, lines)
    cmd = " ".join([
        "phenix.secondary_structure_restraints",
        "%s" % pdb_path,
        "search_method=from_ca",
        "%s=%s" % (par, value),
        "prefix=%s%d"%(prefix, i)])
    lines = ["pdb_interpretation {\n",
            "    enabled = True\n",
            "        selection = chain 'A' and resid 16 through 18\n"]
    test_result_from_command(cmd, lines, fname="%s%d_ss.eff" % (prefix, i))

def exercise_5(prefix="tst_ss_restr_5"):
  """
  Testing that leaving default values for options specific to from_ca search
  method works
  """

  pdb_path = libtbx.env.find_in_repositories(
      relative_path="phenix_regression/pdb/1ywf.pdb",
      test=os.path.isfile)
  for i, (par, value) in enumerate([('ss_by_chain', 'True'),
        ('max_rmsd', '1'),
        ('from_ca_conservative', 'False'),
        ('use_representative_chains', 'True'),
        ('max_representative_chains', '100')]):
    cmd = " ".join([
        "phenix.secondary_structure_restraints",
        "%s" % pdb_path,
        "%s=%s" % (par, value),
        "prefix=%s%d"%(prefix, i)])
    lines = ["pdb_interpretation {\n",
            "    enabled = True\n",
            "        selection = chain 'A' and resid 16 through 18\n"]
    test_result_from_command(cmd, lines, fname="%s%d_ss.eff" % (prefix, i))


if (__name__ == "__main__"):
  t0 = time.time()
  exercise_11()
  exercise_12()
  exercise_13()
  exercise_2()
  exercise_3()
  exercise_4()
  exercise_5()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
