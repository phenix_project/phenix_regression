from __future__ import print_function

from libtbx.test_utils import approx_equal
import libtbx.load_env # import dependency
from libtbx import easy_run
import shutil
import os
import sys

def exercise (verbose=False) :
  base_dir = libtbx.env.find_in_repositories("phenix_regression/harvesting")
  assert (base_dir is not None)
  args = [
    os.path.join(base_dir, "refine.mtz"),
    #os.path.join(base_dir, "lores.mtz"),
    "f_obs_labels=F-obs,SIGF-obs",
    "unmerged_data=\"%s\"" % os.path.join(base_dir, "unmerged.sca"),
  ]
  result = easy_run.fully_buffered("phenix.cc_star %s" % " ".join(args)
    ).raise_if_errors()
  if (verbose) :
    print("\n".join(result.stdout_lines))
  n_checked = 0
  for line in result.stdout_lines :
    line = line.strip()
    if line.startswith("30.43    2.00") :
      fields = line.split()
      cc_star = float(fields[6])
      assert approx_equal(cc_star, 0.999)
      n_checked += 1
    elif line.startswith("30.43    5.42") :
      fields = line.split()
      r_work = float(fields[-2])
      assert approx_equal(r_work, 0.229, eps=0.01)
      n_checked += 1
  assert (n_checked == 2)
  print("OK")

if (__name__ == "__main__") :
  exercise(verbose=("--verbose" in sys.argv))
