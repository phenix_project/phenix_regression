from __future__ import print_function
from libtbx import easy_run
import libtbx.load_env
import os

def exercise_00():
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/jf.pdb",
    test=os.path.isfile,
    optional=False)
  pdb_file_noh = "jf_noH.pdb"
  from libtbx.utils import remove_files
  remove_files(paths=[
    pdb_file_noh,
    "tmp_fake_f_obs.log",
    "fake_model.pdb",
    "fake_f_obs.mtz",
    "fomvd.log"])
  assert not easy_run.call("phenix.reduce -trim %s > %s 2>err"%(pdb_file,pdb_file_noh))
  assert not easy_run.call("phenix.fake_f_obs %s ensemble_size=5> tmp_fake_f_obs.log"%pdb_file_noh)
  assert not easy_run.call("mmtbx.model_vs_data fake_model.pdb fake_f_obs.mtz > fomvd.log")
  r = float(easy_run.go(
   command="grep '  r_work:' "+"fomvd.log"
     ).stdout_lines[0].split()[1])
  assert r < 0.01, r
  print("OK")

if (__name__ == "__main__"):
  exercise_00()
