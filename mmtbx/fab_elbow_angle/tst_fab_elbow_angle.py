from __future__ import division
from __future__ import print_function
from mmtbx.utils.fab_elbow_angle import fab_elbow_angle
from libtbx.test_utils import approx_equal
from libtbx import easy_run
import libtbx.load_env
import iotbx.pdb
import os

def exercise0():
  """
  Exercise FAB elbow angle calculation
  target angles are taken from Stanfield, et al., JMB 2006
  """
  for it in [("1bbd", 127,114,118,7),
             ("1bbd", 127,107,113,2),
             ("7fab", 132,104,117,10),
             ("1dba", 183,107,113,11),
             ("1plg", 190,112,117,4),
             ("1nl0", 220,107,113,7)]:
    pdb_code, target_angle,limit_light, limit_heavy, tolerance = it
    pdb_file_name = libtbx.env.find_in_repositories(
      relative_path="phenix_regression/mmtbx/fab_elbow_angle/%s.pdb.gz"%pdb_code,
      test=os.path.isfile)
    ph = iotbx.pdb.input(file_name=pdb_file_name).construct_hierarchy()
    elbow_angle = fab_elbow_angle(
      pdb_hierarchy=ph,
      limit_light=limit_light,
      limit_heavy=limit_heavy).fab_elbow_angle
    print("inputs:", it, "output:", elbow_angle)
    assert approx_equal(elbow_angle, target_angle, tolerance), \
      [elbow_angle, target_angle]

def exercise1():
  """
  Test command line command
  """
  pdb_file_name = libtbx.env.find_in_repositories(
        relative_path="phenix_regression/mmtbx/fab_elbow_angle/7fab.pdb.gz",
        test=os.path.isfile)
  cmd = 'phenix.fab_elbow_angle %s light=L ' \
        'heavy=H limit_l=104 limit_h=117'%pdb_file_name
  print(cmd)
  tmp = easy_run.go(cmd).stdout_lines[0]
  print(tmp)
  assert len(tmp.split(':'))==2
  target_angle = float(tmp.split(':')[1])
  assert approx_equal(123.00, target_angle, 0.1)

if __name__ == "__main__":
  exercise0()
  exercise1()
