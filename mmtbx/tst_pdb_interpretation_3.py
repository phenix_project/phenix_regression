from __future__ import absolute_import, division, print_function
import os, time
import iotbx.pdb
from libtbx import easy_run

def exercise():
  model_filename = "tst_pdb_interpretation_3.pdb"
  with open(model_filename, "w") as f:
    f.write(raw_records)
  log_filename = "tst_phenix_regression_mmtbx_pdb_interpretation_3.log"
  cmd = " ".join(["phenix.pdb_interpretation",
    "%s >& %s" % (model_filename, log_filename)])
  r = easy_run.call(cmd)
  assert r == 0, 'FAILED - %s' % cmd
  os.remove(model_filename)
  os.remove(log_filename)

raw_records = """
REMARK from PDB model 3bet, H atom is at bogus position from hydrogenate
CRYST1   42.170   41.490   71.580  90.00 104.07  90.00 P 1 21 1
ATOM     30  N   HIS A 119     -11.950  -2.656  14.672  1.00  6.46           N
ATOM     31  CA  HIS A 119     -11.681  -1.459  15.446  1.00  6.32           C
ATOM     32  C   HIS A 119     -12.357  -0.251  14.801  1.00  6.37           C
ATOM     33  O   HIS A 119     -12.026   0.129  13.677  1.00  6.26           O
ATOM     34  CB  HIS A 119     -10.162  -1.233  15.553  1.00  5.63           C
ATOM     35  CG  HIS A 119      -9.468  -2.212  16.454  1.00  5.90           C
ATOM     36  ND1 HIS A 119      -8.098  -2.371  16.469  1.00  5.80           N
ATOM     37  CD2 HIS A 119      -9.952  -3.059  17.395  1.00  5.07           C
ATOM     38  CE1 HIS A 119      -7.769  -3.270  17.381  1.00  5.18           C
ATOM     39  NE2 HIS A 119      -8.876  -3.702  17.958  1.00  3.43           N
TER
HETATM   61 ZN    ZN A 262      -6.632  -1.589  15.193  1.00  8.64          Zn
HETATM   62  C1  CTF A 264      -4.408   2.677  14.581  1.00 17.66           C
HETATM   63  C10 CTF A 264      -0.468   5.260  12.724  1.00 20.09           C
HETATM   64  C11 CTF A 264      -0.974   3.931  13.354  1.00 17.17           C
HETATM   65  C12 CTF A 264      -3.134   7.695  14.321  1.00 20.11           C
HETATM   66  C13 CTF A 264      -2.553   9.082  13.908  1.00 21.75           C
HETATM   67  C14 CTF A 264      -1.030   9.111  13.877  1.00 21.98           C
HETATM   68  C15 CTF A 264      -0.516   7.890  13.024  1.00 21.60           C
HETATM   69  C16 CTF A 264      -0.507   9.160  15.337  1.00 21.41           C
HETATM   70  C17 CTF A 264      -0.362  10.191  12.946  1.00 23.12           C
HETATM   71  C18 CTF A 264       1.072   9.735  12.820  1.00 23.12           C
HETATM   72  C19 CTF A 264       0.999   8.201  12.883  1.00 21.81           C
HETATM   73  C2  CTF A 264      -5.171   3.876  14.679  1.00 17.22           C
HETATM   74  C20 CTF A 264      -0.380  11.590  13.503  1.00 25.78           C
HETATM   75  C21 CTF A 264      -1.634  12.328  13.181  1.00 29.26           C
HETATM   76  C3  CTF A 264      -4.539   5.089  14.336  1.00 16.77           C
HETATM   77  C4  CTF A 264      -3.168   5.175  13.893  1.00 16.93           C
HETATM   78  C5  CTF A 264      -2.433   3.951  13.820  1.00 17.71           C
HETATM   79  C6  CTF A 264      -3.049   2.723  14.156  1.00 18.14           C
HETATM   80  C7  CTF A 264      -6.808   4.359  16.406  1.00 14.61           C
HETATM   81  C8  CTF A 264      -2.545   6.511  13.494  1.00 18.63           C
HETATM   82  C9  CTF A 264      -0.961   6.482  13.526  1.00 20.82           C
HETATM   83  N1  CTF A 264      -5.261  -0.580  16.384  1.00 11.31           N
HETATM   84  N2  CTF A 264      -2.606  12.882  12.983  1.00 31.49           N
HETATM   85  O1  CTF A 264      -6.879   1.240  16.367  1.00 15.14           O
HETATM   86  O2  CTF A 264      -4.597   1.608  17.262  1.00 14.93           O
HETATM   87  O3  CTF A 264      -5.028   1.433  14.883  1.00 17.32           O
HETATM   88  O4  CTF A 264      -6.490   3.968  15.075  1.00 18.28           O
HETATM   89  S1  CTF A 264      -5.484   0.979  16.342  1.00 16.29           S
REMARK The atom below made the fragment crash
REMARK It is located in another ASU than its partner atom, failing
REMARK grm's add_new_bond_restraints_in_place invoked via metal linking
HETATM   65  HN1 CTF A 264      -2.318   6.448  14.788  0.00  0.00           H
END
"""

if (__name__ == "__main__"):
  t0 = time.time()
  exercise()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
