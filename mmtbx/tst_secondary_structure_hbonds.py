from __future__ import division
from __future__ import print_function
from mmtbx.secondary_structure import sec_str_master_phil_str, manager
import iotbx.pdb
import iotbx.pdb.secondary_structure as ioss
from six.moves import cStringIO as StringIO
from libtbx.utils import null_out
from libtbx.test_utils import show_diff

alpha_h1 = iotbx.pdb.input(source_info=None, lines="""\
ATOM      1  N   ALA     1       1.643  -2.366  -1.408  1.00  0.00           N
ATOM      2  CA  ALA     1       1.280  -3.608  -2.069  1.00  0.00           C
ATOM      3  C   ALA     1      -0.114  -3.466  -2.684  1.00  0.00           C
ATOM      4  O   ALA     1      -0.327  -3.827  -3.840  1.00  0.00           O
ATOM      5  CB  ALA     1       1.361  -4.762  -1.068  1.00  0.00           C
ATOM      6  N   ALA     2      -1.028  -2.938  -1.882  1.00  0.00           N
ATOM      7  CA  ALA     2      -2.395  -2.743  -2.332  1.00  0.00           C
ATOM      8  C   ALA     2      -2.396  -1.855  -3.579  1.00  0.00           C
ATOM      9  O   ALA     2      -3.059  -2.167  -4.567  1.00  0.00           O
ATOM     10  CB  ALA     2      -3.228  -2.150  -1.194  1.00  0.00           C
ATOM     11  N   ALA     3      -1.646  -0.767  -3.491  1.00  0.00           N
ATOM     12  CA  ALA     3      -1.551   0.168  -4.599  1.00  0.00           C
ATOM     13  C   ALA     3      -1.044  -0.568  -5.841  1.00  0.00           C
ATOM     14  O   ALA     3      -1.601  -0.419  -6.927  1.00  0.00           O
ATOM     15  CB  ALA     3      -0.646   1.337  -4.205  1.00  0.00           C
ATOM     16  N   ALA     4       0.008  -1.348  -5.639  1.00  0.00           N
ATOM     17  CA  ALA     4       0.597  -2.109  -6.728  1.00  0.00           C
ATOM     18  C   ALA     4      -0.466  -3.023  -7.340  1.00  0.00           C
ATOM     19  O   ALA     4      -0.611  -3.085  -8.559  1.00  0.00           O
ATOM     20  CB  ALA     4       1.808  -2.887  -6.211  1.00  0.00           C
ATOM     21  N   ALA     5      -1.184  -3.711  -6.463  1.00  0.00           N
ATOM     22  CA  ALA     5      -2.231  -4.619  -6.901  1.00  0.00           C
ATOM     23  C   ALA     5      -3.253  -3.847  -7.737  1.00  0.00           C
ATOM     24  O   ALA     5      -3.647  -4.296  -8.813  1.00  0.00           O
ATOM     25  CB  ALA     5      -2.864  -5.294  -5.683  1.00  0.00           C
ATOM     26  N   ALA     6      -3.654  -2.699  -7.211  1.00  0.00           N
ATOM     27  CA  ALA     6      -4.623  -1.860  -7.896  1.00  0.00           C
ATOM     28  C   ALA     6      -4.090  -1.499  -9.284  1.00  0.00           C
ATOM     29  O   ALA     6      -4.809  -1.602 -10.276  1.00  0.00           O
ATOM     30  CB  ALA     6      -4.919  -0.623  -7.045  1.00  0.00           C
ATOM     31  N   ALA     7      -2.831  -1.084  -9.309  1.00  0.00           N
ATOM     32  CA  ALA     7      -2.192  -0.708 -10.559  1.00  0.00           C
ATOM     33  C   ALA     7      -2.243  -1.890 -11.529  1.00  0.00           C
ATOM     34  O   ALA     7      -2.600  -1.727 -12.695  1.00  0.00           O
ATOM     35  CB  ALA     7      -0.761  -0.243 -10.281  1.00  0.00           C
ATOM     36  N   ALA     8      -1.881  -3.055 -11.012  1.00  0.00           N
ATOM     37  CA  ALA     8      -1.882  -4.264 -11.817  1.00  0.00           C
ATOM     38  C   ALA     8      -3.285  -4.496 -12.382  1.00  0.00           C
ATOM     39  O   ALA     8      -3.442  -4.772 -13.570  1.00  0.00           O
ATOM     40  CB  ALA     8      -1.391  -5.441 -10.972  1.00  0.00           C
ATOM     41  N   ALA     9      -4.269  -4.376 -11.503  1.00  0.00           N
ATOM     42  CA  ALA     9      -5.653  -4.568 -11.898  1.00  0.00           C
ATOM     43  C   ALA     9      -6.000  -3.590 -13.022  1.00  0.00           C
ATOM     44  O   ALA     9      -6.590  -3.978 -14.029  1.00  0.00           O
ATOM     45  CB  ALA     9      -6.561  -4.400 -10.678  1.00  0.00           C
ATOM     46  N   ALA    10      -5.617  -2.338 -12.812  1.00  0.00           N
ATOM     47  CA  ALA    10      -5.879  -1.301 -13.795  1.00  0.00           C
ATOM     48  C   ALA    10      -5.242  -1.695 -15.130  1.00  0.00           C
ATOM     49  O   ALA    10      -5.880  -1.605 -16.177  1.00  0.00           O
ATOM     50  CB  ALA    10      -5.358   0.040 -13.274  1.00  0.00           C
TER
""").construct_hierarchy()

sheet_h1 = iotbx.pdb.input(source_info=None, lines="""\
CRYST1  168.300  168.300  168.300  90.00  90.00  90.00 P 1
ATOM      1  N   ALA B  49      71.219 105.720  88.401  1.00 31.78           N
ATOM      2  CA  ALA B  49      72.646 105.920  88.177  1.00 32.37           C
ATOM      3  C   ALA B  49      73.348 106.336  89.465  1.00 34.85           C
ATOM      4  O   ALA B  49      72.699 106.657  90.461  1.00 36.04           O
ATOM      5  CB  ALA B  49      72.871 106.960  87.090  1.00 33.97           C
ATOM      6  N   ALA B  50      74.678 106.328  89.439  1.00 38.88           N
ATOM      7  CA  ALA B  50      75.465 106.705  90.604  1.00 41.26           C
ATOM      8  C   ALA B  50      75.497 108.221  90.747  1.00 37.96           C
ATOM      9  O   ALA B  50      75.858 108.936  89.807  1.00 36.60           O
ATOM     10  CB  ALA B  50      76.885 106.151  90.493  1.00 46.12           C
ATOM     11  N   ALA B  51      75.117 108.709  91.926  1.00  3.89           N
ATOM     12  CA  ALA B  51      75.095 110.142  92.211  1.00  0.01           C
ATOM     13  C   ALA B  51      75.584 110.351  93.636  1.00 26.04           C
ATOM     14  O   ALA B  51      74.824 110.156  94.590  1.00  1.76           O
ATOM     15  CB  ALA B  51      73.694 110.725  92.021  1.00 25.94           C
ATOM     16  N   ALA B  52      76.846 110.746  93.781  1.00 16.41           N
ATOM     17  CA  ALA B  52      77.456 110.988  95.080  1.00 14.22           C
ATOM     18  C   ALA B  52      77.839 112.456  95.188  1.00 14.85           C
ATOM     19  O   ALA B  52      78.514 112.993  94.302  1.00 18.19           O
ATOM     20  CB  ALA B  52      78.686 110.100  95.289  1.00 13.16           C
ATOM     21  N   ALA B  53      77.408 113.103  96.272  1.00 16.62           N
ATOM     22  CA  ALA B  53      77.691 114.520  96.516  1.00 18.62           C
ATOM     23  C   ALA B  53      78.007 114.679  98.002  1.00 22.88           C
ATOM     24  O   ALA B  53      77.109 114.893  98.821  1.00 21.73           O
ATOM     25  CB  ALA B  53      76.524 115.401  96.087  1.00 19.45           C
ATOM     26  N   ALA B  54      79.290 114.574  98.341  1.00 35.33           N
ATOM     27  CA  ALA B  54      79.764 114.705  99.714  1.00 41.91           C
ATOM     28  C   ALA B  54      80.614 115.965  99.809  1.00 43.44           C
ATOM     29  O   ALA B  54      81.725 116.013  99.269  1.00 40.40           O
ATOM     30  CB  ALA B  54      80.555 113.470 100.140  1.00 46.68           C
ATOM     31  N   ALA B  55      80.092 116.983 100.494  1.00 47.72           N
ATOM     32  CA  ALA B  55      80.775 118.265 100.668  1.00 47.10           C
ATOM     33  C   ALA B  55      80.838 118.564 102.163  1.00 47.31           C
ATOM     34  O   ALA B  55      79.862 119.033 102.754  1.00 50.92           O
ATOM     35  CB  ALA B  55      80.067 119.375  99.897  1.00 51.40           C
ATOM     36  N   ALA B  56      81.990 118.290 102.771  1.00 37.22           N
ATOM     37  CA  ALA B  56      82.219 118.530 104.192  1.00 38.16           C
ATOM     38  C   ALA B  56      83.410 119.470 104.331  1.00 38.78           C
ATOM     39  O   ALA B  56      84.535 119.115 103.961  1.00 32.58           O
ATOM     40  CB  ALA B  56      82.458 117.220 104.937  1.00 34.30           C
ATOM     41  N   ALA B  57      83.160 120.666 104.862  1.00 45.22           N
ATOM     42  CA  ALA B  57      84.192 121.680 105.043  1.00 50.78           C
ATOM     43  C   ALA B  57      84.030 122.305 106.420  1.00 56.67           C
ATOM     44  O   ALA B  57      83.027 122.977 106.684  1.00 58.45           O
ATOM     45  CB  ALA B  57      84.113 122.752 103.952  1.00 54.78           C
ATOM     46  N   ALA B  58      85.022 122.096 107.284  1.00 74.36           N
ATOM     47  CA  ALA B  58      85.005 122.616 108.653  1.00 79.83           C
ATOM     48  C   ALA B  58      86.366 122.453 109.334  1.00 84.51           C
ATOM     49  O   ALA B  58      86.760 121.337 109.679  1.00 86.28           O
ATOM     50  CB  ALA B  58      83.924 121.922 109.472  1.00 70.40           C
ATOM     51  N   ALA B 167      85.549 119.106 108.340  1.00 25.68           N
ATOM     52  CA  ALA B 167      86.332 118.170 107.541  1.00 17.31           C
ATOM     53  C   ALA B 167      86.857 118.865 106.293  1.00 13.75           C
ATOM     54  O   ALA B 167      86.782 120.086 106.187  1.00 19.05           O
ATOM     55  CB  ALA B 167      85.498 116.955 107.168  1.00 13.08           C
ATOM     56  N   ALA B 168      87.380 118.080 105.347  1.00 16.43           N
ATOM     57  CA  ALA B 168      87.899 118.598 104.087  1.00 11.55           C
ATOM     58  C   ALA B 168      87.677 117.529 103.013  1.00  6.03           C
ATOM     59  O   ALA B 168      88.585 116.814 102.590  1.00 10.71           O
ATOM     60  CB  ALA B 168      89.375 118.987 104.198  1.00  9.55           C
ATOM     61  N   ALA B 169      86.431 117.415 102.556  1.00  9.67           N
ATOM     62  CA  ALA B 169      86.062 116.431 101.548  1.00 15.37           C
ATOM     63  C   ALA B 169      85.123 117.071 100.539  1.00  6.59           C
ATOM     64  O   ALA B 169      84.145 117.720 100.922  1.00 14.84           O
ATOM     65  CB  ALA B 169      85.399 115.204 102.185  1.00  5.22           C
ATOM     66  N   ALA B 170      85.422 116.884  99.252  1.00 16.37           N
ATOM     67  CA  ALA B 170      84.607 117.427  98.160  1.00 15.38           C
ATOM     68  C   ALA B 170      84.572 116.379  97.048  1.00 17.30           C
ATOM     69  O   ALA B 170      85.399 116.400  96.133  1.00 18.49           O
ATOM     70  CB  ALA B 170      85.156 118.759  97.666  1.00 15.49           C
ATOM     71  N   ALA B 171      83.609 115.464  97.137  1.00 21.37           N
ATOM     72  CA  ALA B 171      83.443 114.387  96.166  1.00 24.64           C
ATOM     73  C   ALA B 171      82.128 114.599  95.429  1.00 26.32           C
ATOM     74  O   ALA B 171      81.053 114.550  96.038  1.00 27.79           O
ATOM     75  CB  ALA B 171      83.476 113.021  96.851  1.00 29.11           C
ATOM     76  N   ALA B 172      82.212 114.831  94.120  1.00 30.22           N
ATOM     77  CA  ALA B 172      81.041 115.042  93.276  1.00 29.97           C
ATOM     78  C   ALA B 172      81.148 114.121  92.068  1.00 27.91           C
ATOM     79  O   ALA B 172      81.934 114.384  91.152  1.00 30.27           O
ATOM     80  CB  ALA B 172      80.926 116.504  92.842  1.00 34.99           C
ATOM     81  N   ALA B 173      80.363 113.047  92.068  1.00 15.00           N
ATOM     82  CA  ALA B 173      80.361 112.074  90.986  1.00 13.78           C
ATOM     83  C   ALA B 173      78.938 111.862  90.494  1.00 15.81           C
ATOM     84  O   ALA B 173      78.016 111.673  91.293  1.00 15.96           O
ATOM     85  CB  ALA B 173      80.966 110.738  91.435  1.00 10.99           C
ATOM     86  N   ALA B 174      78.766 111.892  89.173  1.00 35.82           N
ATOM     87  CA  ALA B 174      77.459 111.706  88.549  1.00 38.64           C
ATOM     88  C   ALA B 174      77.646 110.860  87.298  1.00 37.11           C
ATOM     89  O   ALA B 174      78.046 111.377  86.250  1.00 40.25           O
ATOM     90  CB  ALA B 174      76.809 113.048  88.210  1.00 46.63           C
ATOM     91  N   ALA B 175      77.358 109.567  87.406  1.00 16.68           N
ATOM     92  CA  ALA B 175      77.495 108.654  86.277  1.00 13.08           C
ATOM     93  C   ALA B 175      76.368 107.627  86.267  1.00 10.10           C
ATOM     94  O   ALA B 175      76.340 106.714  87.093  1.00 14.69           O
ATOM     95  CB  ALA B 175      78.847 107.959  86.318  1.00 10.62           C
""").construct_hierarchy()

def exercise_helix_bonding():
  ioss_annotation = ioss.annotation.from_records(records = """\
HELIX    1   1 ALA      1  ALA     10 1                                  10""".split("\n"))
  ann = ioss_annotation.as_restraint_groups(prefix_scope="secondary_structure")
  defpars = iotbx.phil.parse(sec_str_master_phil_str)
  custom_pars = defpars.fetch(iotbx.phil.parse(ann))
  custom_pars_ex = custom_pars.extract()
  ss_manager = manager(
              alpha_h1,
              sec_str_from_pdb_file=ioss_annotation,
              params=custom_pars_ex.secondary_structure,
              verbose=-1)
  proxies_for_grm, hangles = ss_manager.create_protein_hbond_proxies(
    annotation = None,
    log        = None)
  buff = StringIO()
  for angle in hangles:
    # print dir(angle)
    print(list(angle.i_seqs), angle.angle_ideal, angle.weight, angle.origin_id, file=buff)
  res = buff.getvalue()
  print(res)
  assert not show_diff(res, """\
[2, 3, 20] 155.0 0.01 2
[21, 20, 3] 116.0 0.01 2
[17, 20, 3] 121.0 0.01 2
[7, 8, 25] 155.0 0.04 2
[26, 25, 8] 116.0 0.04 2
[22, 25, 8] 121.0 0.04 2
[12, 13, 30] 155.0 0.04 2
[31, 30, 13] 116.0 0.04 2
[27, 30, 13] 121.0 0.04 2
[17, 18, 35] 155.0 0.04 2
[36, 35, 18] 116.0 0.04 2
[32, 35, 18] 121.0 0.04 2
[22, 23, 40] 155.0 0.04 2
[41, 40, 23] 116.0 0.04 2
[37, 40, 23] 121.0 0.04 2
[27, 28, 45] 155.0 0.01 2
[46, 45, 28] 116.0 0.01 2
[42, 45, 28] 121.0 0.01 2
"""), res

def exercise_helix_bonding_angle_scaling():
  ioss_annotation = ioss.annotation.from_records(records = """\
HELIX    1   1 ALA      1  ALA     10 1                                  10""".split("\n"))
  ann = ioss_annotation.as_restraint_groups(prefix_scope="secondary_structure")
  defpars = iotbx.phil.parse(sec_str_master_phil_str)
  custom_pars = defpars.fetch(iotbx.phil.parse(ann))
  custom_pars_ex = custom_pars.extract()
  custom_pars_ex.secondary_structure.protein.helix[0].angle_sigma_scale = 2
  ss_manager = manager(
              alpha_h1,
              sec_str_from_pdb_file=ioss_annotation,
              params=custom_pars_ex.secondary_structure,
              verbose=-1)
  proxies_for_grm, hangles = ss_manager.create_protein_hbond_proxies(
    annotation = None,
    log        = None)
  buff = StringIO()
  for angle in hangles:
    # print dir(angle)
    print(list(angle.i_seqs), angle.angle_ideal, angle.weight, angle.origin_id, file=buff)
  res = buff.getvalue()
  print(res)
  assert not show_diff(res, """\
[2, 3, 20] 155.0 0.0025 2
[21, 20, 3] 116.0 0.0025 2
[17, 20, 3] 121.0 0.0025 2
[7, 8, 25] 155.0 0.01 2
[26, 25, 8] 116.0 0.01 2
[22, 25, 8] 121.0 0.01 2
[12, 13, 30] 155.0 0.01 2
[31, 30, 13] 116.0 0.01 2
[27, 30, 13] 121.0 0.01 2
[17, 18, 35] 155.0 0.01 2
[36, 35, 18] 116.0 0.01 2
[32, 35, 18] 121.0 0.01 2
[22, 23, 40] 155.0 0.01 2
[41, 40, 23] 116.0 0.01 2
[37, 40, 23] 121.0 0.01 2
[27, 28, 45] 155.0 0.0025 2
[46, 45, 28] 116.0 0.0025 2
[42, 45, 28] 121.0 0.0025 2
"""), res

def exercise_helix_bonding_angle_setting():
  ioss_annotation = ioss.annotation.from_records(records = """\
HELIX    1   1 ALA      1  ALA     10 1                                  10""".split("\n"))
  ann = ioss_annotation.as_restraint_groups(prefix_scope="secondary_structure")
  defpars = iotbx.phil.parse(sec_str_master_phil_str)
  custom_pars = defpars.fetch(iotbx.phil.parse(ann))
  custom_pars_ex = custom_pars.extract()
  custom_pars_ex.secondary_structure.protein.helix[0].angle_sigma_set = 2
  ss_manager = manager(
              alpha_h1,
              sec_str_from_pdb_file=ioss_annotation,
              params=custom_pars_ex.secondary_structure,
              verbose=-1)
  proxies_for_grm, hangles = ss_manager.create_protein_hbond_proxies(
    annotation = None,
    log        = None)
  buff = StringIO()
  for angle in hangles:
    # print dir(angle)
    print(list(angle.i_seqs), angle.angle_ideal, angle.weight, angle.origin_id, file=buff)
  res = buff.getvalue()
  print(res)
  assert not show_diff(res, """\
[2, 3, 20] 155.0 0.25 2
[21, 20, 3] 116.0 0.25 2
[17, 20, 3] 121.0 0.25 2
[7, 8, 25] 155.0 0.25 2
[26, 25, 8] 116.0 0.25 2
[22, 25, 8] 121.0 0.25 2
[12, 13, 30] 155.0 0.25 2
[31, 30, 13] 116.0 0.25 2
[27, 30, 13] 121.0 0.25 2
[17, 18, 35] 155.0 0.25 2
[36, 35, 18] 116.0 0.25 2
[32, 35, 18] 121.0 0.25 2
[22, 23, 40] 155.0 0.25 2
[41, 40, 23] 116.0 0.25 2
[37, 40, 23] 121.0 0.25 2
[27, 28, 45] 155.0 0.25 2
[46, 45, 28] 116.0 0.25 2
[42, 45, 28] 121.0 0.25 2
"""), res


def exercise_sheet_bonding():
  ioss_annotation = ioss.annotation.from_records(records = """\
SHEET    1   A 2 ALA B  50  ALA B  58  0
SHEET    2   A 2 ALA B 167  ALA B 175 -1  N  ALA B 175   O  ALA B  50""".split("\n"))
  ann = ioss_annotation.as_restraint_groups(prefix_scope="secondary_structure")
  defpars = iotbx.phil.parse(sec_str_master_phil_str)
  custom_pars = defpars.fetch(iotbx.phil.parse(ann))
  custom_pars_ex = custom_pars.extract()

  # sheet_h1.hierarchy.write_pdb_file(file_name="exercise_sheet_bonding.pdb")
  ss_manager = manager(
              sheet_h1,
              sec_str_from_pdb_file=ioss_annotation,
              params=custom_pars_ex.secondary_structure,
              verbose=-1)
  proxies_for_grm, hangles = ss_manager.create_protein_hbond_proxies(
    annotation = None,
    log        = None)
  atoms = sheet_h1.atoms()
  buff = StringIO()
  for angle in hangles:
    # print dir(angle)
    print("%s %.1f %.5f %d" % (list(angle.i_seqs), angle.angle_ideal, angle.weight, angle.origin_id), file=buff)
    # print list(angle.i_seqs), angle.angle_ideal, angle.weight, angle.origin_id,
    # print atoms[angle.i_seqs[0]].id_str(), "---",
    # print atoms[angle.i_seqs[1]].id_str(), "---",
    # print atoms[angle.i_seqs[2]].id_str()

    # print >> buff, list(angle.i_seqs), angle.angle_ideal, angle.weight, angle.origin_id
  res = buff.getvalue()
  # print res
  assert not show_diff(res, """\
[7, 8, 90] 155.0 0.01235 2
[91, 90, 8] 124.0 0.02041 2
[87, 90, 8] 113.0 0.02778 2
[82, 83, 15] 155.0 0.01235 2
[16, 15, 83] 124.0 0.02041 2
[12, 15, 83] 113.0 0.02778 2
[17, 18, 80] 155.0 0.01235 2
[81, 80, 18] 124.0 0.02041 2
[77, 80, 18] 113.0 0.02778 2
[72, 73, 25] 155.0 0.01235 2
[26, 25, 73] 124.0 0.02041 2
[22, 25, 73] 113.0 0.02778 2
[27, 28, 70] 155.0 0.01235 2
[71, 70, 28] 124.0 0.02041 2
[67, 70, 28] 113.0 0.02778 2
[62, 63, 35] 155.0 0.01235 2
[36, 35, 63] 124.0 0.02041 2
[32, 35, 63] 113.0 0.02778 2
[37, 38, 60] 155.0 0.01235 2
[61, 60, 38] 124.0 0.02041 2
[57, 60, 38] 113.0 0.02778 2
[52, 53, 45] 155.0 0.01235 2
[46, 45, 53] 124.0 0.02041 2
[42, 45, 53] 113.0 0.02778 2
"""), res

def exercise_sheet_bonding_angle_scaling():
  ioss_annotation = ioss.annotation.from_records(records = """\
SHEET    1   A 2 ALA B  50  ALA B  58  0
SHEET    2   A 2 ALA B 167  ALA B 175 -1  N  ALA B 175   O  ALA B  50""".split("\n"))
  ann = ioss_annotation.as_restraint_groups(prefix_scope="secondary_structure")
  defpars = iotbx.phil.parse(sec_str_master_phil_str)
  custom_pars = defpars.fetch(iotbx.phil.parse(ann))
  custom_pars_ex = custom_pars.extract()
  custom_pars_ex.secondary_structure.protein.sheet[0].angle_sigma_scale = 2

  # sheet_h1.hierarchy.write_pdb_file(file_name="exercise_sheet_bonding.pdb")
  ss_manager = manager(
              sheet_h1,
              sec_str_from_pdb_file=ioss_annotation,
              params=custom_pars_ex.secondary_structure,
              verbose=-1)
  proxies_for_grm, hangles = ss_manager.create_protein_hbond_proxies(
    annotation = None,
    log        = None)
  atoms = sheet_h1.atoms()
  buff = StringIO()
  for angle in hangles:
    # print dir(angle)
    print("%s %.1f %.5f %d" % (list(angle.i_seqs), angle.angle_ideal, angle.weight, angle.origin_id), file=buff)
    # print list(angle.i_seqs), angle.angle_ideal, angle.weight, angle.origin_id,
    # print atoms[angle.i_seqs[0]].id_str(), "---",
    # print atoms[angle.i_seqs[1]].id_str(), "---",
    # print atoms[angle.i_seqs[2]].id_str()

    # print >> buff, list(angle.i_seqs), angle.angle_ideal, angle.weight, angle.origin_id
  res = buff.getvalue()
  # print res
  assert not show_diff(res, """\
[7, 8, 90] 155.0 0.00309 2
[91, 90, 8] 124.0 0.00510 2
[87, 90, 8] 113.0 0.00694 2
[82, 83, 15] 155.0 0.00309 2
[16, 15, 83] 124.0 0.00510 2
[12, 15, 83] 113.0 0.00694 2
[17, 18, 80] 155.0 0.00309 2
[81, 80, 18] 124.0 0.00510 2
[77, 80, 18] 113.0 0.00694 2
[72, 73, 25] 155.0 0.00309 2
[26, 25, 73] 124.0 0.00510 2
[22, 25, 73] 113.0 0.00694 2
[27, 28, 70] 155.0 0.00309 2
[71, 70, 28] 124.0 0.00510 2
[67, 70, 28] 113.0 0.00694 2
[62, 63, 35] 155.0 0.00309 2
[36, 35, 63] 124.0 0.00510 2
[32, 35, 63] 113.0 0.00694 2
[37, 38, 60] 155.0 0.00309 2
[61, 60, 38] 124.0 0.00510 2
[57, 60, 38] 113.0 0.00694 2
[52, 53, 45] 155.0 0.00309 2
[46, 45, 53] 124.0 0.00510 2
[42, 45, 53] 113.0 0.00694 2
"""), res

def exercise_sheet_bonding_angle_setting():
  ioss_annotation = ioss.annotation.from_records(records = """\
SHEET    1   A 2 ALA B  50  ALA B  58  0
SHEET    2   A 2 ALA B 167  ALA B 175 -1  N  ALA B 175   O  ALA B  50""".split("\n"))
  ann = ioss_annotation.as_restraint_groups(prefix_scope="secondary_structure")
  defpars = iotbx.phil.parse(sec_str_master_phil_str)
  custom_pars = defpars.fetch(iotbx.phil.parse(ann))
  custom_pars_ex = custom_pars.extract()
  custom_pars_ex.secondary_structure.protein.sheet[0].angle_sigma_set = 2

  # sheet_h1.hierarchy.write_pdb_file(file_name="exercise_sheet_bonding.pdb")
  ss_manager = manager(
              sheet_h1,
              sec_str_from_pdb_file=ioss_annotation,
              params=custom_pars_ex.secondary_structure,
              verbose=-1)
  proxies_for_grm, hangles = ss_manager.create_protein_hbond_proxies(
    annotation = None,
    log        = None)
  atoms = sheet_h1.atoms()
  buff = StringIO()
  for angle in hangles:
    # print dir(angle)
    print(list(angle.i_seqs), angle.angle_ideal, angle.weight, angle.origin_id, file=buff)
    # print list(angle.i_seqs), angle.angle_ideal, angle.weight, angle.origin_id,
    # print atoms[angle.i_seqs[0]].id_str(), "---",
    # print atoms[angle.i_seqs[1]].id_str(), "---",
    # print atoms[angle.i_seqs[2]].id_str()

    # print >> buff, list(angle.i_seqs), angle.angle_ideal, angle.weight, angle.origin_id
  res = buff.getvalue()
  # print res
  assert not show_diff(res, """\
[7, 8, 90] 155.0 0.25 2
[91, 90, 8] 124.0 0.25 2
[87, 90, 8] 113.0 0.25 2
[82, 83, 15] 155.0 0.25 2
[16, 15, 83] 124.0 0.25 2
[12, 15, 83] 113.0 0.25 2
[17, 18, 80] 155.0 0.25 2
[81, 80, 18] 124.0 0.25 2
[77, 80, 18] 113.0 0.25 2
[72, 73, 25] 155.0 0.25 2
[26, 25, 73] 124.0 0.25 2
[22, 25, 73] 113.0 0.25 2
[27, 28, 70] 155.0 0.25 2
[71, 70, 28] 124.0 0.25 2
[67, 70, 28] 113.0 0.25 2
[62, 63, 35] 155.0 0.25 2
[36, 35, 63] 124.0 0.25 2
[32, 35, 63] 113.0 0.25 2
[37, 38, 60] 155.0 0.25 2
[61, 60, 38] 124.0 0.25 2
[57, 60, 38] 113.0 0.25 2
[52, 53, 45] 155.0 0.25 2
[46, 45, 53] 124.0 0.25 2
[42, 45, 53] 113.0 0.25 2
"""), res

if __name__ == "__main__":
  exercise_helix_bonding()
  exercise_helix_bonding_angle_scaling()
  exercise_helix_bonding_angle_setting()
  exercise_sheet_bonding()
  exercise_sheet_bonding_angle_scaling()
  exercise_sheet_bonding_angle_setting()
  print("OK")
