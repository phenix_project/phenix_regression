#! /bin/csh -fe

set PHENIX_REGRESSION_DIR="`libtbx.find_in_repositories phenix_regression`"
phenix.secondary_structure_restraints "$PHENIX_REGRESSION_DIR"/pdb/1ywf.pdb | libtbx.assert_stdin_contains_strings "chain 'A' and resid 52"
phenix.secondary_structure_restraints "$PHENIX_REGRESSION_DIR"/pdb/1u8d.pdb | libtbx.assert_stdin_contains_strings "base1 = chain 'A' and resid 56"
echo "OK"
