from __future__ import print_function
import libtbx.load_env
import os, time
from libtbx import easy_run
from libtbx.utils import search_for
from libtbx.test_utils import approx_equal

# =========================================
# Testing ramachandran restraints by running geometry_minimization
# =========================================

def exercise_00():
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/mmtbx/geometry_minimization/bad.pdb",
    test=os.path.isfile)
  cmd = "phenix.ramalyze %s > tmp_geometry_minimization.log 2>tmp_geometry_minimization.err"%pdb_file
  assert not easy_run.call(cmd)
  for pattern in ["SUMMARY: 52.76% outliers (Goal: < 0.2%)",
                  "SUMMARY: 25.98% favored (Goal: > 98%)"]:
    lines = search_for(pattern=pattern, mode="==", file_name="tmp_geometry_minimization.log")
    assert len(lines) == 1
  cmd = " ".join([
    "phenix.geometry_minimization",
    "cdl=False",
    "%s"%pdb_file,
    "ramachandran_plot_restraints.enabled=true",
    "ramachandran_plot_restraints.favored=oldfield",
    "ramachandran_plot_restraints.allowed=oldfield",
    "ramachandran_plot_restraints.outlier=oldfield",
    "max_iterations=100",
    "macro_cycles=10",
    "oldfield.dist_weight_max=20",
    "add_angle_and_dihedral_restraints_for_disulfides=False",
    "> tmp_geometry_minimization_1.log"])
  print(cmd)
  assert not easy_run.call(cmd)
  cmd = "phenix.ramalyze bad_minimized.pdb > tmp_geometry_minimization.log 2>tmp_geometry_minimization.err"
  assert not easy_run.call(cmd)
  ###
  fo = open("tmp_geometry_minimization.log", "r")
  found = 0
  for l in fo.readlines():
    if(l.count("SUMMARY:") and l.count("outliers")):
      percent_outliers = float(l.split()[1].strip("%"))
      print(percent_outliers, "goal < 9.0")
      assert (percent_outliers < 9.0), percent_outliers # XXX host-specific
      found += 1
    if(l.count("SUMMARY:") and l.count("favored")):
      percent_favored = float(l.split()[1].strip("%"))
      print(percent_favored, "goal > 49")
      assert (percent_favored > 49), percent_favored # XXX host-specific
      found += 1
  assert found == 2
  fo.close()
  ###
  assert not easy_run.call("rm -rf tmp_geometry_minimization.log")

def exercise_01():
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/mmtbx/geometry_minimization/polyala10.pdb",
    test=os.path.isfile)
  cmd = "phenix.ramalyze %s > tmp_geometry_minimization.log 2>tmp_geometry_minimization.err"%pdb_file
  assert not easy_run.call(cmd)
  ###
  fo = open("tmp_geometry_minimization.log", "r")
  found = 0
  for l in fo.readlines():
    if(l.count("SUMMARY:") and l.count("outliers")):
      assert approx_equal( float(l.split()[1].strip("%")), 42.86, 1.0)
      found += 1
    if(l.count("SUMMARY:") and l.count("favored")):
      assert approx_equal( float(l.split()[1].strip("%")), 42.86, 1.0)
      found += 1
  assert found == 2
  fo.close()
  ###
  cmd = " ".join([
    "phenix.geometry_minimization",
    "cdl=False",
    "%s"%pdb_file,
    "ramachandran_plot_restraints.enabled=true",
    "ramachandran_plot_restraints.favored=oldfield",
    "ramachandran_plot_restraints.allowed=oldfield",
    "ramachandran_plot_restraints.outlier=oldfield",
    "max_iterations=200",
    "macro_cycles=20",
    "> tmp_geometry_minimization.log"])
  print(cmd)
  assert not easy_run.call(cmd)
  cmd = "phenix.ramalyze polyala10_minimized.pdb > tmp_geometry_minimization.log 2>tmp_geometry_minimization.err"
  assert not easy_run.call(cmd)
  ###
  fo = open("tmp_geometry_minimization.log", "r")
  found = 0
  for l in fo.readlines():
    if(l.count("SUMMARY:") and l.count("outliers")):
      percent_outliers =  float(l.split()[1].strip("%"))
      print(percent_outliers, "goal = 0.1 %")
      assert percent_outliers < 0.1, percent_outliers
      found += 1
    if(l.count("SUMMARY:") and l.count("favored")):
      percent_favored = float(l.split()[1].strip("%"))
      print(percent_favored, "goal > 78.5 %")
      assert percent_favored > 78.5
      found += 1
  assert found == 2
  fo.close()
  ###
  assert not easy_run.call("rm -rf tmp_geometry_minimization.log")

def exercise_02():
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/3mku.pdb",
    test=os.path.isfile)
  cmd = "phenix.ramalyze %s > tmp_geometry_minimization.log 2>tmp_geometry_minimization.err"%pdb_file
  assert not easy_run.call(cmd)
  ###
  fo = open("tmp_geometry_minimization.log", "r")
  found = 0
  for l in fo.readlines():
    if(l.count("SUMMARY:") and l.count("outliers")):
      assert approx_equal( float(l.split()[1].strip("%")), 25.98, 1.0)
      found += 1
    if(l.count("SUMMARY:") and l.count("favored")):
      assert approx_equal( float(l.split()[1].strip("%")), 47.93, 1.0)
      found += 1
  assert found == 2
  fo.close()
  ###
  cmd = " ".join([
    "phenix.geometry_minimization",
    "cdl=False",
    "%s"%pdb_file,
    "ramachandran_plot_restraints.enabled=true",
    "ramachandran_plot_restraints.favored=oldfield",
    "ramachandran_plot_restraints.allowed=oldfield",
    "ramachandran_plot_restraints.outlier=oldfield",
    "max_iterations=100",
    "macro_cycles=3",
    "fix_rotamer_outliers=False",
    "> tmp_geometry_minimization.log"])
  assert not easy_run.call(cmd)
  cmd = "phenix.ramalyze 3mku_minimized.pdb > tmp_geometry_minimization.log 2>tmp_geometry_minimization.err"
  assert not easy_run.call(cmd)
  ###
  fo = open("tmp_geometry_minimization.log", "r")
  found = 0
  for l in fo.readlines():
    if(l.count("SUMMARY:") and l.count("outliers")):
      percent_outliers = float(l.split()[1].strip("%"))
      print(percent_outliers, "goal < 1.5")
      assert percent_outliers < 1.5
      found += 1
    if(l.count("SUMMARY:") and l.count("favored")):
      percent_favored = float(l.split()[1].strip("%"))
      print(percent_favored, "goal > 80")
      assert percent_favored > 80
      found += 1
  assert found == 2
  fo.close()
  ###
  assert not easy_run.call("rm -rf tmp_geometry_minimization.log")

def run():
  t0 = time.time()
  exercise_00()
  exercise_01()
  exercise_02()
  print("Time:%8.2f"%(time.time()-t0))
  print("OK")

if (__name__ == "__main__"):
  run()
