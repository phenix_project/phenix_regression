from __future__ import print_function
import sys
from six.moves import cStringIO as StringIO
from libtbx import easy_run

pdbs = {
  "ready_set_test_1.pdb" : """
CRYST1   59.918   96.953  104.888  90.00  90.00  90.00 P 21 21 21    8
ATOM      1  N   ALA A  29      39.275  80.811  32.916  1.00 46.37           N
ATOM      2  CA  ALA A  29      39.925  81.006  31.584  1.00 46.75           C
ATOM      3  C   ALA A  29      41.299  81.655  31.753  1.00 45.77           C
ATOM      4  O   ALA A  29      42.296  81.178  31.205  1.00 46.35           O
ATOM      5  CB  ALA A  29      39.033  81.872  30.690  1.00 46.37           C
TER
HETATM 4933  O   HOH B 498      72.194  95.405  77.758  1.00 31.14           O
END
""",
  }

def excercise_water():
  for opt in ["",
              "add_h_to_water=True",
              "add_d_to_water=True",
              "neutron_option=all_h",
              "neutron_option=all_d",
              "neutron_option=hd_and_h",
              "neutron_option=hd_and_d",
              "neutron_option=all_hd",
              #"perdeuterate=True",
              #"neutron_exchange_hydrogens=True",
              "remove_waters=True",
              "optimise_final_geometry_of_hydrogens=False",
              ]:
    cmd = "phenix.ready_set ready_set_test_1.pdb"
    cmd += " %s" % opt
    print(cmd)
    ero = easy_run.fully_buffered(command=cmd)
    err = StringIO()
    ero.show_stderr(out=err)
    found = False
    for i, line in enumerate(err.getvalue().split("\n")):
      print(line)
    assert not i

def run():
  for pdb_filename, pdb_lines in pdbs.items():
    f = open(pdb_filename, "w")
    f.write(pdb_lines)
    f.close()

  excercise_water()

if __name__=="__main__":
  args = sys.argv[1:]
  del sys.argv[1:]
  run(*tuple(args))
