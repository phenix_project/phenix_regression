from __future__ import print_function
from cctbx.array_family import flex
import iotbx.pdb
from libtbx import easy_run
from libtbx.test_utils import approx_equal
import time, math

pdb_str = """
CRYST1   43.436   43.436  118.182  90.00  90.00 120.00 P 31 2 1
ATOM      1  N   ARG A 293      -7.803  -7.554   4.432  1.00144.26           N
ATOM      2  CA  ARG A 293      -6.396  -7.088   4.560  1.00144.54           C
ATOM      3  C   ARG A 293      -6.290  -6.010   5.648  1.00132.16           C
ATOM      4  O   ARG A 293      -5.347  -6.070   6.444  1.00100.29           O
ATOM      5  CB  ARG A 293      -5.922  -6.506   3.223  1.00118.89           C
ATOM      6  CG  ARG A 293      -4.418  -6.545   2.992  1.00122.96           C
ATOM      7  CD  ARG A 293      -3.966  -5.723   1.801  1.00127.03           C
ATOM      8  NE  ARG A 293      -3.967  -6.452   0.538  1.00184.26           N
ATOM      9  CZ  ARG A 293      -3.399  -6.027  -0.593  1.00207.09           C
ATOM     10  NH1 ARG A 293      -3.295  -6.834  -1.637  1.00211.85           N
ATOM     11  NH2 ARG A 293      -2.938  -4.790  -0.680  1.00186.95           N
ATOM     12  H   ARG A 293      -8.105  -7.452   3.579  1.00173.16           H
ATOM     13  HA  ARG A 293      -5.826  -7.854   4.804  1.00173.49           H
ATOM     14  HB2 ARG A 293      -6.361  -6.999   2.499  1.00142.71           H
ATOM     15  HB3 ARG A 293      -6.221  -5.574   3.170  1.00142.71           H
ATOM     16  HG2 ARG A 293      -3.962  -6.212   3.795  1.00147.60           H
ATOM     17  HG3 ARG A 293      -4.139  -7.475   2.856  1.00147.60           H
ATOM     18  HD2 ARG A 293      -4.554  -4.942   1.715  1.00152.48           H
ATOM     19  HD3 ARG A 293      -3.059  -5.391   1.972  1.00152.48           H
ATOM     20  HE  ARG A 293      -4.370  -7.225   0.518  1.00221.16           H
ATOM     21 HH11 ARG A 293      -2.603  -7.372  -1.708  1.00254.27           H
ATOM     22 HH12 ARG A 293      -3.919  -6.831  -2.256  1.00254.27           H
ATOM     23 HH21 ARG A 293      -2.552  -4.420   0.000  1.00224.39           H
ATOM     24 HH22 ARG A 293      -2.999  -4.352  -1.442  1.00224.39           H
TER
END
"""

def exercise(prefix="tst_13"):
  """
  Exercise riding_h. Exotic case of H overlapping with its symmetry mate and
  restraints manager sets up two X-H bonds for x,y,z and other copies.
  """
  of = open("%s.pdb"%prefix, "w")
  print(pdb_str, file=of)
  of.close()
  cmd = " ".join([
    "phenix.geometry_minimization",
    "%s.pdb"%prefix,
    "output_file_name_prefix=%s_minimized"%prefix,
    "minimization.correct_special_position_tolerance=3.0",
    "allow_polymer_cross_special_position=True",
    "riding_h=true",
    "> zlog"])
  print(cmd)
  assert not easy_run.call(cmd)

if (__name__ == "__main__"):
  t0 = time.time()
  exercise()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
