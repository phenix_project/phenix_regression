from __future__ import print_function
import iotbx.pdb
from libtbx import easy_run
import time
from scitbx.array_family import flex

pdb_str = """
CRYST1   14.024   12.503   15.347  90.00  90.00  90.00 P 1
ATOM      1  N   ALA B  49       6.669   4.947   4.991  1.00 31.78           N
ATOM      2  CA  ALA B  49       7.615   5.163   6.079  1.00 32.37           C
ATOM      3  C   ALA B  49       7.196   6.346   6.945  1.00 34.85           C
ATOM      4  O   ALA B  49       7.363   7.502   6.556  1.00 36.04           O
ATOM      5  CB  ALA B  49       9.016   5.379   5.528  1.00 33.97           C
ATOM      6  N   ALA B  50       6.651   6.050   8.120  1.00 38.88           N
ATOM      7  CA  ALA B  50       6.207   7.088   9.042  1.00 41.26           C
ATOM      8  C   ALA B  50       7.335   7.510   9.977  1.00 37.96           C
ATOM      9  O   ALA B  50       8.167   6.693  10.371  1.00 36.60           O
ATOM     10  CB  ALA B  50       5.006   6.607   9.842  1.00 46.12           C
TER
END
"""

def exercise(suffix="gm_tst_09"):
  """
  Exercise Cartesian reference restraints.
  """
  pdb_inp = iotbx.pdb.input(source_info=None, lines = pdb_str)
  h = pdb_inp.construct_hierarchy()
  x = h.extract_xray_structure()
  answer_fn = "answer_%s.pdb"%suffix
  h.write_pdb_file(file_name = answer_fn)
  #
  x.shake_sites_in_place(mean_distance=1)
  h.adopt_xray_structure(x)
  poor_fn = "poor_%s.pdb"%suffix
  h.write_pdb_file(file_name = poor_fn)
  #
  cmd = " ".join([
    "phenix.geometry_minimization",
    "%s"%poor_fn,
    "macro_cycles=5",
    "pdb_interpretation.reference_coordinate_restraints.enabled=True",
    "pdb_interpretation.reference_coordinate_restraints.selection=all",
    "pdb_interpretation.reference_coordinate_restraints.sigma=0.001",
    "> zlog"])
  print(cmd)
  assert not easy_run.call(cmd)
  #
  pdb_inp = iotbx.pdb.input(file_name="poor_gm_tst_09_minimized.pdb")
  h = pdb_inp.construct_hierarchy()
  xr = h.extract_xray_structure()
  #
  d = xr.distances(x)
  print("value should be < 0.01", flex.mean(d))
  assert flex.mean(d) < 0.01, "mean %f not less than 0.01" % flex.mean(d)

if (__name__ == "__main__"):
  t0 = time.time()
  exercise()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
