from __future__ import print_function
import libtbx.load_env
import os, time, math
from libtbx import easy_run
import iotbx.pdb

prefix="geometry_minimization_tst_03"

def exercise():
  """
  Exercise links
  """
  path="phenix_regression/mmtbx/geometry_minimization"
  pdb1 = libtbx.env.find_in_repositories(
    relative_path="%s/A64-68_shake.pdb"%path, test=os.path.isfile)
  pdb2 = libtbx.env.find_in_repositories(
    relative_path="%s/A64-68.pdb"%path, test=os.path.isfile)
  cif1 = libtbx.env.find_in_repositories(
    relative_path="%s/CSY_links_N.cif"%path, test=os.path.isfile)
  cif2 = libtbx.env.find_in_repositories(
    relative_path="%s/CSY_links_S.cif"%path, test=os.path.isfile)
  cif3 = libtbx.env.find_in_repositories(
    relative_path="%s/CSY_ready_set.cif"%path, test=os.path.isfile)
  for i, cif in enumerate([cif1, cif2]):
    cmd = " ".join([
      "phenix.geometry_minimization",
      "%s"%pdb1,
      "%s"%cif,
      "link_distance_cutoff=10",
      "output_file_name_prefix=%s_%s"%(prefix, str(i)),
      "> zlog_%s" %(str(i))])
    print(cmd)
    assert not easy_run.call(cmd)
  cmd = " ".join([
      "phenix.geometry_minimization",
      "%s"%pdb2,
      "%s"%cif3,
      "link_distance_cutoff=10",
      "output_file_name_prefix=%s_%s"%(prefix, str(2)),
      "link_all=True",
      #"intra_residue_bond_cutoff=5",
      "> zlog"])
  print(cmd)
  assert not easy_run.call(cmd)
  # make sure output matches expectations
  pair_1 = ["N66", "C64", 1.37]
  pair_2 = ["C66", "N68", 1.33]
  tmpl = "geometry_minimization_tst_03_%s.pdb"
  cntr = 0
  for i in [0,1,2]:
    pdb_file_name = tmpl%str(i)
    awl = iotbx.pdb.input(file_name = tmpl%str(i)).atoms_with_labels()
    r1=[]
    r2=[]
    for awl_ in awl:
      lab = awl_.name.strip().upper()+awl_.resseq.strip().upper()
      if(lab in pair_1):
        r1.append(awl_.xyz)
      if(lab in pair_2):
        r2.append(awl_.xyz)
    for r in [r1,r2]:
      assert len(r)==2
      d=math.sqrt((r[0][0]-r[1][0])**2+(r[0][1]-r[1][1])**2+(r[0][2]-r[1][2])**2)
      print(d, pdb_file_name)
      cntr += 1
      assert d<1.5
  assert cntr == 6

def run():
  t0 = time.time()
  exercise()
  print("Time:%8.2f"%(time.time()-t0))

if (__name__ == "__main__"):
  run()
