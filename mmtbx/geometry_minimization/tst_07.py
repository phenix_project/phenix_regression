from __future__ import print_function
import iotbx.pdb
from libtbx import easy_run
from libtbx.test_utils import approx_equal
import time
from scitbx.array_family import flex

pdb_str_1 = """
CRYST1  168.300  168.300  168.300  90.00  90.00  90.00 P 1
SHEET    1   A 2 ALA B  50  ALA B  58  0
SHEET    2   A 2 ALA B 167  ALA B 175 -1  N  ALA B 175   O  ALA B  50
ATOM      1  N   ALA B  49      71.558 107.531  84.580  1.00 31.78           N
ATOM      2  CA  ALA B  49      72.514 107.671  85.662  1.00 32.37           C
ATOM      3  C   ALA B  49      72.042 108.819  86.553  1.00 34.85           C
ATOM      4  O   ALA B  49      72.189 109.980  86.163  1.00 36.04           O
ATOM      5  CB  ALA B  49      73.887 107.976  85.088  1.00 33.97           C
ATOM      6  N   ALA B  50      71.512 108.540  87.736  1.00 38.88           N
ATOM      7  CA  ALA B  50      71.067 109.628  88.599  1.00 41.26           C
ATOM      8  C   ALA B  50      72.218 110.034  89.493  1.00 37.96           C
ATOM      9  O   ALA B  50      73.004 109.195  89.927  1.00 36.60           O
ATOM     10  CB  ALA B  50      69.863 109.220  89.451  1.00 46.12           C
ATOM     11  N   ALA B  51      72.253 111.333  89.774  1.00  3.89           N
ATOM     12  CA  ALA B  51      73.289 111.945  90.581  1.00  0.01           C
ATOM     13  C   ALA B  51      72.732 112.315  91.950  1.00 26.04           C
ATOM     14  O   ALA B  51      71.664 112.913  92.077  1.00  1.76           O
ATOM     15  CB  ALA B  51      73.845 113.216  89.910  1.00 25.94           C
ATOM     16  N   ALA B  52      73.492 111.936  92.965  1.00 16.41           N
ATOM     17  CA  ALA B  52      73.160 112.137  94.342  1.00 14.22           C
ATOM     18  C   ALA B  52      74.425 112.737  94.882  1.00 14.85           C
ATOM     19  O   ALA B  52      75.527 112.339  94.506  1.00 18.19           O
ATOM     20  CB  ALA B  52      72.810 110.820  95.052  1.00 13.16           C
ATOM     21  N   ALA B  53      74.227 113.779  95.683  1.00 16.62           N
ATOM     22  CA  ALA B  53      75.296 114.561  96.273  1.00 18.62           C
ATOM     23  C   ALA B  53      75.119 114.660  97.756  1.00 22.88           C
ATOM     24  O   ALA B  53      74.078 115.072  98.210  1.00 21.73           O
ATOM     25  CB  ALA B  53      75.275 115.973  95.716  1.00 19.45           C
ATOM     26  N   ALA B  54      76.143 114.335  98.523  1.00 35.33           N
ATOM     27  CA  ALA B  54      76.060 114.540  99.968  1.00 41.91           C
ATOM     28  C   ALA B  54      77.312 115.251 100.395  1.00 43.44           C
ATOM     29  O   ALA B  54      78.290 115.316  99.655  1.00 40.40           O
ATOM     30  CB  ALA B  54      75.707 113.243 100.758  1.00 46.68           C
ATOM     31  N   ALA B  55      77.239 115.879 101.557  1.00 47.72           N
ATOM     32  CA  ALA B  55      78.362 116.651 102.017  1.00 47.10           C
ATOM     33  C   ALA B  55      79.140 116.034 103.127  1.00 47.31           C
ATOM     34  O   ALA B  55      78.606 115.228 103.900  1.00 50.92           O
ATOM     35  CB  ALA B  55      77.805 117.970 102.588  1.00 51.40           C
ATOM     36  N   ALA B  56      80.411 116.408 103.215  1.00 37.22           N
ATOM     37  CA  ALA B  56      81.213 115.926 104.322  1.00 38.16           C
ATOM     38  C   ALA B  56      81.609 117.184 105.066  1.00 38.78           C
ATOM     39  O   ALA B  56      82.098 118.162 104.418  1.00 32.58           O
ATOM     40  CB  ALA B  56      82.399 115.145 103.855  1.00 34.30           C
ATOM     41  N   ALA B  57      81.444 117.169 106.394  1.00 45.22           N
ATOM     42  CA  ALA B  57      81.820 118.336 107.165  1.00 50.78           C
ATOM     43  C   ALA B  57      83.294 118.250 107.530  1.00 56.67           C
ATOM     44  O   ALA B  57      83.751 117.221 108.025  1.00 58.45           O
ATOM     45  CB  ALA B  57      80.971 118.396 108.444  1.00 54.78           C
ATOM     46  N   ALA B  58      84.035 119.321 107.277  1.00 74.36           N
ATOM     47  CA  ALA B  58      85.460 119.357 107.600  1.00 79.83           C
ATOM     48  C   ALA B  58      85.774 119.141 109.064  1.00 84.51           C
ATOM     49  O   ALA B  58      84.913 119.393 109.910  1.00 86.28           O
ATOM     50  CB  ALA B  58      86.049 120.668 107.147  1.00 70.40           C
ATOM     51  N   ALA B 167      90.540 124.508 106.162  1.00 25.68           N
ATOM     52  CA  ALA B 167      90.411 124.081 104.779  1.00 17.31           C
ATOM     53  C   ALA B 167      89.492 122.873 104.643  1.00 13.75           C
ATOM     54  O   ALA B 167      89.346 122.088 105.565  1.00 19.05           O
ATOM     55  CB  ALA B 167      91.848 123.787 104.200  1.00 13.08           C
ATOM     56  N   ALA B 168      88.773 122.801 103.529  1.00 16.43           N
ATOM     57  CA  ALA B 168      87.869 121.686 103.247  1.00 11.55           C
ATOM     58  C   ALA B 168      88.264 120.989 101.970  1.00  6.03           C
ATOM     59  O   ALA B 168      88.580 121.669 100.990  1.00 10.71           O
ATOM     60  CB  ALA B 168      86.409 122.143 103.137  1.00  9.55           C
ATOM     61  N   ALA B 169      88.240 119.658 101.940  1.00  9.67           N
ATOM     62  CA  ALA B 169      88.618 119.002 100.677  1.00 15.37           C
ATOM     63  C   ALA B 169      87.339 118.408 100.112  1.00  6.59           C
ATOM     64  O   ALA B 169      86.536 117.816 100.874  1.00 14.84           O
ATOM     65  CB  ALA B 169      89.741 117.949 100.838  1.00  5.22           C
ATOM     66  N   ALA B 170      87.138 118.579  98.798  1.00 16.37           N
ATOM     67  CA  ALA B 170      85.924 118.064  98.181  1.00 15.38           C
ATOM     68  C   ALA B 170      86.163 116.918  97.219  1.00 17.30           C
ATOM     69  O   ALA B 170      86.542 117.099  96.066  1.00 18.49           O
ATOM     70  CB  ALA B 170      85.245 119.192  97.397  1.00 15.49           C
ATOM     71  N   ALA B 171      85.919 115.725  97.730  1.00 21.37           N
ATOM     72  CA  ALA B 171      86.041 114.481  96.986  1.00 24.64           C
ATOM     73  C   ALA B 171      84.796 114.270  96.087  1.00 26.32           C
ATOM     74  O   ALA B 171      83.629 114.089  96.599  1.00 27.79           O
ATOM     75  CB  ALA B 171      86.298 113.324  97.946  1.00 29.11           C
ATOM     76  N   ALA B 172      85.006 114.484  94.782  1.00 30.22           N
ATOM     77  CA  ALA B 172      83.948 114.310  93.781  1.00 29.97           C
ATOM     78  C   ALA B 172      84.163 113.035  92.973  1.00 27.91           C
ATOM     79  O   ALA B 172      85.087 112.961  92.164  1.00 30.27           O
ATOM     80  CB  ALA B 172      83.847 115.527  92.869  1.00 34.99           C
ATOM     81  N   ALA B 173      83.276 112.084  93.124  1.00 15.00           N
ATOM     82  CA  ALA B 173      83.415 110.878  92.377  1.00 13.78           C
ATOM     83  C   ALA B 173      82.106 110.424  91.807  1.00 15.81           C
ATOM     84  O   ALA B 173      81.067 110.828  92.261  1.00 15.96           O
ATOM     85  CB  ALA B 173      83.951 109.825  93.270  1.00 10.99           C
ATOM     86  N   ALA B 174      82.181 109.584  90.798  1.00 35.82           N
ATOM     87  CA  ALA B 174      81.039 109.127  90.006  1.00 38.64           C
ATOM     88  C   ALA B 174      80.993 107.612  89.956  1.00 37.11           C
ATOM     89  O   ALA B 174      82.026 106.945  90.031  1.00 40.25           O
ATOM     90  CB  ALA B 174      81.064 109.686  88.580  1.00 46.63           C
ATOM     91  N   ALA B 175      79.788 107.078  89.823  1.00 16.68           N
ATOM     92  CA  ALA B 175      79.563 105.646  89.829  1.00 13.08           C
ATOM     93  C   ALA B 175      78.254 105.368  89.137  1.00 10.10           C
ATOM     94  O   ALA B 175      77.489 106.291  88.856  1.00 14.69           O
ATOM     95  CB  ALA B 175      79.434 105.110  91.256  1.00 10.62           C
TER
END
"""

pdb_str_2 = """
CRYST1  168.300  168.300  168.300  90.00  90.00  90.00 P 1
ATOM      1  N   ALA B  49      71.558 107.531  84.580  1.00 31.78           N
ATOM      2  CA  ALA B  49      72.514 107.671  85.662  1.00 32.37           C
ATOM      3  C   ALA B  49      72.042 108.819  86.553  1.00 34.85           C
ATOM      4  O   ALA B  49      72.189 109.980  86.163  1.00 36.04           O
ATOM      5  CB  ALA B  49      73.887 107.976  85.088  1.00 33.97           C
ATOM      6  N   ALA B  50      71.512 108.540  87.736  1.00 38.88           N
ATOM      7  CA  ALA B  50      71.067 109.628  88.599  1.00 41.26           C
ATOM      8  C   ALA B  50      72.218 110.034  89.493  1.00 37.96           C
ATOM      9  O   ALA B  50      73.004 109.195  89.927  1.00 36.60           O
ATOM     10  CB  ALA B  50      69.863 109.220  89.451  1.00 46.12           C
ATOM     11  N   ALA B  51      72.253 111.333  89.774  1.00  3.89           N
ATOM     12  CA  ALA B  51      73.289 111.945  90.581  1.00  0.01           C
ATOM     13  C   ALA B  51      72.732 112.315  91.950  1.00 26.04           C
ATOM     14  O   ALA B  51      71.664 112.913  92.077  1.00  1.76           O
ATOM     15  CB  ALA B  51      73.845 113.216  89.910  1.00 25.94           C
ATOM     16  N   ALA B  52      73.492 111.936  92.965  1.00 16.41           N
ATOM     17  CA  ALA B  52      73.160 112.137  94.342  1.00 14.22           C
ATOM     18  C   ALA B  52      74.425 112.737  94.882  1.00 14.85           C
ATOM     19  O   ALA B  52      75.527 112.339  94.506  1.00 18.19           O
ATOM     20  CB  ALA B  52      72.810 110.820  95.052  1.00 13.16           C
ATOM     21  N   ALA B  53      74.227 113.779  95.683  1.00 16.62           N
ATOM     22  CA  ALA B  53      75.296 114.561  96.273  1.00 18.62           C
ATOM     23  C   ALA B  53      75.119 114.660  97.756  1.00 22.88           C
ATOM     24  O   ALA B  53      74.078 115.072  98.210  1.00 21.73           O
ATOM     25  CB  ALA B  53      75.275 115.973  95.716  1.00 19.45           C
ATOM     26  N   ALA B  54      76.143 114.335  98.523  1.00 35.33           N
ATOM     27  CA  ALA B  54      76.060 114.540  99.968  1.00 41.91           C
ATOM     28  C   ALA B  54      77.312 115.251 100.395  1.00 43.44           C
ATOM     29  O   ALA B  54      78.290 115.316  99.655  1.00 40.40           O
ATOM     30  CB  ALA B  54      75.707 113.243 100.758  1.00 46.68           C
ATOM     31  N   ALA B  55      77.239 115.879 101.557  1.00 47.72           N
ATOM     32  CA  ALA B  55      78.362 116.651 102.017  1.00 47.10           C
ATOM     33  C   ALA B  55      79.140 116.034 103.127  1.00 47.31           C
ATOM     34  O   ALA B  55      78.606 115.228 103.900  1.00 50.92           O
ATOM     35  CB  ALA B  55      77.805 117.970 102.588  1.00 51.40           C
ATOM     36  N   ALA B  56      80.411 116.408 103.215  1.00 37.22           N
ATOM     37  CA  ALA B  56      81.213 115.926 104.322  1.00 38.16           C
ATOM     38  C   ALA B  56      81.609 117.184 105.066  1.00 38.78           C
ATOM     39  O   ALA B  56      82.098 118.162 104.418  1.00 32.58           O
ATOM     40  CB  ALA B  56      82.399 115.145 103.855  1.00 34.30           C
ATOM     41  N   ALA B  57      81.444 117.169 106.394  1.00 45.22           N
ATOM     42  CA  ALA B  57      81.820 118.336 107.165  1.00 50.78           C
ATOM     43  C   ALA B  57      83.294 118.250 107.530  1.00 56.67           C
ATOM     44  O   ALA B  57      83.751 117.221 108.025  1.00 58.45           O
ATOM     45  CB  ALA B  57      80.971 118.396 108.444  1.00 54.78           C
ATOM     46  N   ALA B  58      84.035 119.321 107.277  1.00 74.36           N
ATOM     47  CA  ALA B  58      85.460 119.357 107.600  1.00 79.83           C
ATOM     48  C   ALA B  58      85.774 119.141 109.064  1.00 84.51           C
ATOM     49  O   ALA B  58      84.913 119.393 109.910  1.00 86.28           O
ATOM     50  CB  ALA B  58      86.049 120.668 107.147  1.00 70.40           C
ATOM     51  N   ALA B 167      90.540 124.508 106.162  1.00 25.68           N
ATOM     52  CA  ALA B 167      90.411 124.081 104.779  1.00 17.31           C
ATOM     53  C   ALA B 167      89.492 122.873 104.643  1.00 13.75           C
ATOM     54  O   ALA B 167      89.346 122.088 105.565  1.00 19.05           O
ATOM     55  CB  ALA B 167      91.848 123.787 104.200  1.00 13.08           C
ATOM     56  N   ALA B 168      88.773 122.801 103.529  1.00 16.43           N
ATOM     57  CA  ALA B 168      87.869 121.686 103.247  1.00 11.55           C
ATOM     58  C   ALA B 168      88.264 120.989 101.970  1.00  6.03           C
ATOM     59  O   ALA B 168      88.580 121.669 100.990  1.00 10.71           O
ATOM     60  CB  ALA B 168      86.409 122.143 103.137  1.00  9.55           C
ATOM     61  N   ALA B 169      88.240 119.658 101.940  1.00  9.67           N
ATOM     62  CA  ALA B 169      88.618 119.002 100.677  1.00 15.37           C
ATOM     63  C   ALA B 169      87.339 118.408 100.112  1.00  6.59           C
ATOM     64  O   ALA B 169      86.536 117.816 100.874  1.00 14.84           O
ATOM     65  CB  ALA B 169      89.741 117.949 100.838  1.00  5.22           C
ATOM     66  N   ALA B 170      87.138 118.579  98.798  1.00 16.37           N
ATOM     67  CA  ALA B 170      85.924 118.064  98.181  1.00 15.38           C
ATOM     68  C   ALA B 170      86.163 116.918  97.219  1.00 17.30           C
ATOM     69  O   ALA B 170      86.542 117.099  96.066  1.00 18.49           O
ATOM     70  CB  ALA B 170      85.245 119.192  97.397  1.00 15.49           C
ATOM     71  N   ALA B 171      85.919 115.725  97.730  1.00 21.37           N
ATOM     72  CA  ALA B 171      86.041 114.481  96.986  1.00 24.64           C
ATOM     73  C   ALA B 171      84.796 114.270  96.087  1.00 26.32           C
ATOM     74  O   ALA B 171      83.629 114.089  96.599  1.00 27.79           O
ATOM     75  CB  ALA B 171      86.298 113.324  97.946  1.00 29.11           C
ATOM     76  N   ALA B 172      85.006 114.484  94.782  1.00 30.22           N
ATOM     77  CA  ALA B 172      83.948 114.310  93.781  1.00 29.97           C
ATOM     78  C   ALA B 172      84.163 113.035  92.973  1.00 27.91           C
ATOM     79  O   ALA B 172      85.087 112.961  92.164  1.00 30.27           O
ATOM     80  CB  ALA B 172      83.847 115.527  92.869  1.00 34.99           C
ATOM     81  N   ALA B 173      83.276 112.084  93.124  1.00 15.00           N
ATOM     82  CA  ALA B 173      83.415 110.878  92.377  1.00 13.78           C
ATOM     83  C   ALA B 173      82.106 110.424  91.807  1.00 15.81           C
ATOM     84  O   ALA B 173      81.067 110.828  92.261  1.00 15.96           O
ATOM     85  CB  ALA B 173      83.951 109.825  93.270  1.00 10.99           C
ATOM     86  N   ALA B 174      82.181 109.584  90.798  1.00 35.82           N
ATOM     87  CA  ALA B 174      81.039 109.127  90.006  1.00 38.64           C
ATOM     88  C   ALA B 174      80.993 107.612  89.956  1.00 37.11           C
ATOM     89  O   ALA B 174      82.026 106.945  90.031  1.00 40.25           O
ATOM     90  CB  ALA B 174      81.064 109.686  88.580  1.00 46.63           C
ATOM     91  N   ALA B 175      79.788 107.078  89.823  1.00 16.68           N
ATOM     92  CA  ALA B 175      79.563 105.646  89.829  1.00 13.08           C
ATOM     93  C   ALA B 175      78.254 105.368  89.137  1.00 10.10           C
ATOM     94  O   ALA B 175      77.489 106.291  88.856  1.00 14.69           O
ATOM     95  CB  ALA B 175      79.434 105.110  91.256  1.00 10.62           C
TER
END
"""

ss_str = """
pdb_interpretation.secondary_structure.protein {
  sheet {
    first_strand = chain 'B' and resid 50 through 58
    strand {
      selection = chain 'B' and resid 167 through 175
      sense = parallel *antiparallel unknown
      bond_start_current = chain 'B' and resid 175 and name N
      bond_start_previous = chain 'B' and resid 50 and name O
    }
  }
}
"""

def get_interacting_o_n_distances(file_name):
  pdb_inp = iotbx.pdb.input(file_name = file_name)
  pdb_hierarchy = pdb_inp.construct_hierarchy()
  uc = pdb_inp.xray_structure_simple().unit_cell()
  res1 = list(pdb_hierarchy.residue_groups())
  res2 = list(pdb_hierarchy.residue_groups())
  sites_1 = []
  for i in [50,52,54,56,58]:
    for rg in res1:
      if(i==int(rg.resseq)):
        for a in rg.atoms():
          if(a.name.strip().upper()=="N"):
            sites_1.append(uc.fractionalize(a.xyz))
        for a in rg.atoms():
          if(a.name.strip().upper()=="O"):
            sites_1.append(uc.fractionalize(a.xyz))
  #
  sites_2 = []
  for i in [175,173,171,169,167]:
    for rg in res2:
      if(i==int(rg.resseq)):
        for a in rg.atoms():
          if(a.name.strip().upper()=="O"):
            sites_2.append(uc.fractionalize(a.xyz))
        for a in rg.atoms():
          if(a.name.strip().upper()=="N"):
            sites_2.append(uc.fractionalize(a.xyz))
  result = flex.double()
  for s1,s2 in zip(sites_1, sites_2):
    result.append(uc.distance(s1, s2))
  return result

def exercise(prefix="exerise_gm_sheet_antiparallel"):
  """
  Exercise sheet (antiparallel).
  """
  for suffix in [1,2]:
    prefix = "%s_%s"%(prefix, str(suffix))
    file_in  = "%s_poor.pdb"%prefix
    file_out = "%s_good.pdb"%prefix
    of = open(file_in, "w")
    if(suffix==1):
      print(pdb_str_1, file=of)
      of.close()
    else:
      print(pdb_str_2, file=of)
      of.close()
      of = open("%s.eff"%prefix, "w")
      print(ss_str, file=of)
      of.close()
    d_start = get_interacting_o_n_distances(file_name = file_in)
    assert d_start.size()==10
    assert d_start.all_gt(5)
    if(suffix==1):
      cmd = " ".join([
        "phenix.geometry_minimization",
        "%s"%file_in,
        "output_file_name_prefix=%s"%file_out[:-4],
        "macro_cycles=10",
        "secondary_structure.enabled=True",
        "secondary_structure.protein.remove_outliers=False",
        "> zlog"])
    else:
      cmd = " ".join([
        "phenix.geometry_minimization",
        "%s"%file_in,
        "%s.eff"%prefix,
        "output_file_name_prefix=%s"%file_out[:-4],
        "macro_cycles=10",
        "secondary_structure.enabled=True",
        "secondary_structure.protein.remove_outliers=False",
        "> zlog"])
    assert not easy_run.call(cmd)
    d_final = get_interacting_o_n_distances(file_name = file_out)
    assert approx_equal(d_final, flex.double(d_final.size(),2.9), 0.01)

if (__name__ == "__main__"):
  t0 = time.time()
  exercise()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
