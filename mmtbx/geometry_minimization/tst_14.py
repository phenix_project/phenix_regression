from __future__ import print_function
from cctbx.array_family import flex
import iotbx.pdb
from libtbx import easy_run
from libtbx.test_utils import approx_equal
import time, math

pdb_str = """
CRYST1   85.209   49.882   24.527  90.00  90.00  90.00 P 1
SCALE1      0.011736  0.000000  0.000000        0.00000
SCALE2      0.000000  0.020047  0.000000        0.00000
SCALE3      0.000000  0.000000  0.040771        0.00000
ATOM      1  N   GLY R 239      80.209  12.960  18.729  1.00 97.84           N
ATOM      2  CA  GLY R 239      79.282  13.766  19.527  1.00 91.56           C
ATOM      3  C   GLY R 239      77.860  13.664  18.999  1.00 84.76           C
ATOM      4  O   GLY R 239      77.077  14.612  19.096  1.00 89.38           O
ATOM      5  N   SER R 240      77.540  12.489  18.445  1.00 72.93           N
ATOM      6  CA  SER R 240      76.328  12.264  17.689  1.00 64.71           C
ATOM      7  C   SER R 240      75.916  10.795  17.849  1.00 54.49           C
ATOM      8  O   SER R 240      76.730   9.876  17.650  1.00 46.71           O
ATOM      9  CB  SER R 240      76.499  12.664  16.241  1.00 69.74           C
ATOM     10  OG  SER R 240      77.648  12.053  15.665  1.00 74.79           O
ATOM     11  N   GLN R 241      74.644  10.608  18.229  1.00 46.50           N
ATOM     12  CA  GLN R 241      74.103   9.371  18.794  1.00 41.11           C
ATOM     13  C   GLN R 241      74.468   8.147  17.922  1.00 33.40           C
ATOM     14  O   GLN R 241      73.577   7.374  17.562  1.00 33.74           O
ATOM     15  CB  GLN R 241      72.596   9.545  19.035  1.00 44.83           C
ATOM     16  CG  GLN R 241      71.802  10.165  17.873  1.00 45.42           C
ATOM     17  CD  GLN R 241      70.727  11.136  18.360  1.00 47.47           C
ATOM     18  OE1 GLN R 241      69.944  10.846  19.282  1.00 44.69           O
ATOM     19  NE2 GLN R 241      70.685  12.315  17.746  1.00 47.89           N
ATOM     20  N   UNK R 242      75.798   7.947  17.679  1.00 27.42           N
ATOM     21  CA  UNK R 242      76.430   6.760  16.934  1.00 25.57           C
ATOM     22  C   UNK R 242      75.507   6.216  15.722  1.00 23.38           C
ATOM     23  O   UNK R 242      75.218   5.000  15.544  1.00 21.44           O
ATOM     24  CB  UNK R 242      76.902   5.665  17.957  1.00 23.93           C
ATOM     25  N   UNK R 243      75.148   7.350  14.798  1.00 21.52           N
ATOM     26  CA  UNK R 243      74.138   7.295  13.587  1.00 20.68           C
ATOM     27  C   UNK R 243      74.803   6.789  12.274  1.00 20.37           C
ATOM     28  O   UNK R 243      75.317   7.600  11.366  1.00 20.28           O
ATOM     29  CB  UNK R 243      73.412   8.702  13.637  1.00 19.14           C
TER
ATOM     30  N   GLY C  95      10.976  36.649   5.000  1.00 37.65           N
ATOM     31  CA  GLY C  95      10.106  36.525   6.176  1.00 37.91           C
ATOM     32  C   GLY C  95      10.771  37.072   7.436  1.00 37.00           C
ATOM     33  O   GLY C  95      11.599  36.394   8.051  1.00 38.45           O
ATOM     34  N   HIS C  96      10.417  38.309   7.809  1.00 35.23           N
ATOM     35  CA  HIS C  96      10.915  38.955   9.025  1.00 34.90           C
ATOM     36  C   HIS C  96      10.304  38.264  10.255  1.00 35.24           C
ATOM     37  O   HIS C  96       9.138  38.495  10.600  1.00 34.19           O
ATOM     38  CB  HIS C  96      10.637  40.465   8.990  1.00 33.52           C
ATOM     39  N   LYS C  97      11.132  37.448  10.924  1.00 34.83           N
ATOM     40  CA  LYS C  97      10.720  36.476  11.926  1.00 34.46           C
ATOM     41  C   LYS C  97      11.059  37.011  13.327  1.00 35.53           C
ATOM     42  O   LYS C  97      12.186  36.845  13.789  1.00 36.46           O
ATOM     43  CB  LYS C  97      11.469  35.170  11.644  1.00 33.58           C
ATOM     44  CG  LYS C  97      10.644  33.898  11.655  1.00 34.37           C
ATOM     45  CD  LYS C  97      11.514  32.675  11.505  1.00 34.77           C
ATOM     46  CE  LYS C  97      10.785  31.389  11.825  1.00 36.10           C
ATOM     47  NZ  LYS C  97      11.732  30.258  11.972  1.00 37.52           N
TER
ATOM     48  N   UNK 1  42       8.385  40.420  10.965  1.00 76.80           N
ATOM     49  CA  UNK 1  42       8.099  41.823  10.593  1.00 84.31           C
ATOM     50  C   UNK 1  42       6.749  42.150  11.139  1.00 95.61           C
ATOM     51  O   UNK 1  42       5.747  42.085  10.423  1.00 97.46           O
ATOM     52  CB  UNK 1  42       8.103  41.986   9.080  1.00 84.82           C
ATOM     53  N   UNK 1  43       6.716  42.489  12.433  1.00110.03           N
ATOM     54  CA  UNK 1  43       5.483  42.832  13.139  1.00119.07           C
ATOM     55  C   UNK 1  43       5.000  44.193  12.726  1.00126.46           C
ATOM     56  O   UNK 1  43       5.656  44.882  11.942  1.00125.49           O
ATOM     57  CB  UNK 1  43       5.724  42.828  14.646  1.00120.51           C
TER
END
"""

def exercise(prefix="tst_14"):
  """
  Don't output spurious LINK records.
  """
  of = open("%s.pdb"%prefix, "w")
  print(pdb_str, file=of)
  of.close()
  cmd = " ".join([
    "phenix.geometry_minimization",
    "%s.pdb"%prefix,
    "output_file_name_prefix=%s_minimized"%prefix,
    "> %s.zlog"%prefix])
  print(cmd)
  assert not easy_run.call(cmd)
  #
  fo = open("%s_minimized.pdb"%prefix,"r")
  n_LINK=0
  for l in fo.readlines():
    l = l.strip()
    if(l.startswith("LINK")): n_LINK+=1
  assert n_LINK==0

if (__name__ == "__main__"):
  t0 = time.time()
  exercise()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
