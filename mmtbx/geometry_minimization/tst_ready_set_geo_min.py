
# this test checks whether heavy atom positions are left unchanged after
# running phenix.ready_set (with integrated geometry minimization)

from __future__ import division
from __future__ import print_function
import iotbx.pdb
from libtbx import easy_run

def exercise () :
  pdb_raw = """\
ATOM    420  N   SER A  51      20.460  35.757  67.683  1.00 11.71           N
ATOM    421  CA  SER A  51      20.935  35.346  66.374  1.00 11.71           C
ATOM    422  C   SER A  51      22.063  36.248  65.903  1.00 11.71           C
ATOM    423  O   SER A  51      22.304  37.298  66.471  1.00 11.71           O
ATOM    424  CB  SER A  51      19.784  35.313  65.357  1.00 11.71           C
ATOM    425  OG  SER A  51      19.299  36.600  65.051  1.00 11.71           O
ATOM    426  N   CYS A  52      22.775  35.817  64.871  1.00 11.71           N
ATOM    427  CA  CYS A  52      23.940  36.540  64.393  1.00 11.71           C
ATOM    428  C   CYS A  52      23.754  37.157  63.010  1.00 11.71           C
ATOM    429  O   CYS A  52      22.844  36.814  62.263  1.00 11.71           O
ATOM    430  CB  CYS A  52      25.167  35.631  64.372  1.00 11.71           C
ATOM    431  SG  CYS A  52      25.248  34.471  63.010  1.00 11.71           S
ATOM    432  N   PHE A  53      24.613  38.111  62.692  1.00 11.71           N
ATOM    433  CA  PHE A  53      24.772  38.582  61.329  1.00 11.71           C
ATOM    434  C   PHE A  53      25.869  37.748  60.703  1.00 11.71           C
ATOM    435  O   PHE A  53      27.039  37.897  61.049  1.00 11.71           O
ATOM    436  CB  PHE A  53      25.158  40.063  61.314  1.00 11.71           C
ATOM    437  CG  PHE A  53      24.000  40.992  61.534  1.00 11.71           C
ATOM    438  CD1 PHE A  53      23.540  41.281  62.808  1.00 11.71           C
ATOM    439  CD2 PHE A  53      23.353  41.559  60.450  1.00 11.71           C
ATOM    440  CE1 PHE A  53      22.462  42.127  62.981  1.00 11.71           C
ATOM    441  CE2 PHE A  53      22.290  42.395  60.629  1.00 11.71           C
ATOM    442  CZ  PHE A  53      21.839  42.668  61.886  1.00 11.71           C
ATOM    443  N   HIS A  54      25.502  36.869  59.783  1.00 11.71           N
ATOM    444  CA  HIS A  54      26.438  35.892  59.271  1.00 11.71           C
ATOM    445  C   HIS A  54      27.247  36.358  58.056  1.00 11.71           C
ATOM    446  O   HIS A  54      28.244  35.733  57.709  1.00 11.71           O
ATOM    447  CB  HIS A  54      25.715  34.587  58.958  1.00 11.71           C
ATOM    448  CG  HIS A  54      24.743  34.702  57.833  1.00 11.71           C
ATOM    449  ND1 HIS A  54      23.461  35.169  58.007  1.00 11.71           N
ATOM    450  CD2 HIS A  54      24.883  34.461  56.509  1.00 11.71           C
ATOM    451  CE1 HIS A  54      22.840  35.178  56.840  1.00 11.71           C
ATOM    452  NE2 HIS A  54      23.681  34.750  55.920  1.00 11.71           N
ATOM    453  N   ARG A  55      26.805  37.427  57.393  1.00 11.71           N
ATOM    454  CA  ARG A  55      27.463  37.888  56.182  1.00 11.71           C
ATOM    455  C   ARG A  55      27.475  39.409  56.182  1.00 11.71           C
ATOM    456  O   ARG A  55      26.423  40.031  56.193  1.00 11.71           O
ATOM    457  CB  ARG A  55      26.713  37.335  54.964  1.00 11.71           C
ATOM    458  CG  ARG A  55      27.370  37.624  53.622  1.00 11.71           C
ATOM    459  CD  ARG A  55      26.674  36.844  52.489  1.00 11.71           C
ATOM    460  NE  ARG A  55      27.546  36.720  51.314  1.00 11.71           N
ATOM    461  CZ  ARG A  55      27.349  37.300  50.132  1.00 11.71           C
ATOM    462  NH1 ARG A  55      26.281  38.061  49.902  1.00 11.71           N
ATOM    463  NH2 ARG A  55      28.236  37.104  49.161  1.00 11.71           N
"""
  open("tst_ready_set_geo_min_in.pdb", "w").write(pdb_raw)
  cmd = "phenix.ready_set tst_ready_set_geo_min_in.pdb"
  rc = easy_run.fully_buffered(cmd).raise_if_errors().return_code
  pdb_in = iotbx.pdb.input("tst_ready_set_geo_min_in.pdb")
  hierarchy_in = pdb_in.construct_hierarchy()
  xrs_in = pdb_in.xray_structure_simple()
  hd_sel = xrs_in.hd_selection()
  sites_start = hierarchy_in.atoms().extract_xyz().select(~hd_sel)
  pdb_out = iotbx.pdb.input("tst_ready_set_geo_min_in.updated.pdb")
  hierarchy_out = pdb_out.construct_hierarchy()
  xrs_out = pdb_out.xray_structure_simple()
  hd_sel = xrs_out.hd_selection()
  sites_end = hierarchy_out.atoms().extract_xyz().select(~hd_sel)
  assert (sites_end.rms_difference(sites_start) == 0)
  print("OK")

if (__name__ == "__main__") :
  exercise()
