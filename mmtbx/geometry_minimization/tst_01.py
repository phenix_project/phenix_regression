from __future__ import print_function
import libtbx.load_env
import os, time
from libtbx import easy_run
import iotbx.pdb

test_prefix="geometry_minimization_tst_01"

def exercise_00():
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/pdb1etn.ent",
    test=os.path.isfile)
  cif_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/misc/multiple_monomers.cif",
    test=os.path.isfile)
  cmd = " ".join([
    "phenix.geometry_minimization",
    "%s %s"%(pdb_file, cif_file),
    "max_iterations=20",
    "macro_cycles=1",
    "output_file_name_prefix=%s_00 > %s_00.zlog"%(test_prefix, test_prefix)])
  assert not easy_run.call(cmd)
  assert os.path.isfile("geometry_minimization_tst_01_00.pdb")

def exercise_01():
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/misc/weird_atom_names.pdb",
    test=os.path.isfile)
  cif_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/misc/weird_atom_names.cif",
    test=os.path.isfile)
  cmd = "phenix.geometry_minimization %s %s macro_cycles=1 max_iterations=20> %s_01.log"%(
    pdb_file, cif_file, test_prefix)
  print(cmd)
  assert not easy_run.call(cmd)
  assert os.path.isfile("weird_atom_names_minimized.pdb")

def exercise_02():
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/misc/max_reasonable_bond_distance.pdb",
    test=os.path.isfile)
  cmd = " ".join([
    "phenix.geometry_minimization",
    "%s"%pdb_file,
    "max_iterations=20",
    "macro_cycles=1",
    "proceed_with_excessive_length_bonds=True",
    "output_file_name_prefix=%s_00 > %s_02.zlog"%(test_prefix, test_prefix)])
  assert not easy_run.call(cmd)
  cntr = 0
  for line in open("geometry_minimization_tst_01_02.zlog","r").readlines():
    line = line.strip()
    if(line.count("1.231 199.384 -198.153 2.00e-02 2.50e+03 9.82e+07")): cntr+=1
  assert cntr== 1
  #
  cmd = " ".join([
    "phenix.geometry_minimization",
    "%s"%pdb_file,
    "max_iterations=20",
    "macro_cycles=1",
    "proceed_with_excessive_length_bonds=False",
    "output_file_name_prefix=%s_00 > %s_02.zlog"%(test_prefix, test_prefix)])
  result = easy_run.fully_buffered(cmd)
  assert result.stderr_lines == ['Sorry: Number of bonds with excessive lengths: 1']
  assert len(result.stdout_lines) == 0
  cntr = 0
  for line in open("geometry_minimization_tst_01_02.zlog","r").readlines():
    line = line.strip()
    if(line.count("Bonds with excessive lengths:")): cntr+=1
    if(line.count("Distance model: 199.384 (ideal: 1.231)")): cntr+=1
  assert cntr==2

def exercise_03():
  file_name = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/polypro_Simon_noCRYST1.pdb",
    test=os.path.isfile)
  log = "exercise_02.log"
  assert file_name.find('"') < 0
  cmd = 'phenix.geometry_minimization "%s" > %s' % (file_name, log)
  assert not easy_run.call(cmd)
  xrs = iotbx.pdb.input(file_name = "polypro_Simon_noCRYST1_minimized.pdb").\
    xray_structure_simple()
  assert xrs.scatterers().size() == 70

def run():
  t0 = time.time()
  exercise_00()
  exercise_01()
  exercise_02()
  exercise_03()
  print("Time:%8.2f"%(time.time()-t0))

if (__name__ == "__main__"):
  run()
