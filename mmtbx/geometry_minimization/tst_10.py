from __future__ import print_function
import iotbx.pdb
from libtbx import easy_run
import time
from scitbx.array_family import flex

pdb_str = """
CRYST1   10.000   10.000   10.000  90.00  90.00  90.00 P 1
HETATM 3241  IR A IR Z   3       5.000   5.000   5.000  0.30 41.55          IR4+
HETATM 3241  MG B MG Z   3       5.000   5.000   5.000  0.70 41.55          MG2+
TER
END
"""

def exercise(suffix="gm_tst_10"):
  """
  Make sure it runs and input=output.
  """
  pdb_inp = iotbx.pdb.input(source_info=None, lines = pdb_str)
  h = pdb_inp.construct_hierarchy()
  x = h.extract_xray_structure()
  answer_fn = "answer_%s.pdb"%suffix
  h.write_pdb_file(file_name = answer_fn)
  #
  cmd = " ".join([
    "phenix.geometry_minimization",
    "%s"%answer_fn,
    "> %s.zlog"%suffix])
  print(cmd)
  assert not easy_run.call(cmd)
  #
  pdb_inp = iotbx.pdb.input(file_name="answer_gm_tst_10_minimized.pdb")
  h = pdb_inp.construct_hierarchy()
  xr = h.extract_xray_structure()
  #
  d = xr.distances(x)
  print("value should be < 0.01", flex.mean(d))
  assert flex.mean(d) < 0.01, "mean %f not less than 0.01" % flex.mean(d)

if (__name__ == "__main__"):
  t0 = time.time()
  exercise()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
