from __future__ import print_function
from cctbx.array_family import flex
import iotbx.pdb
from libtbx import easy_run
from libtbx.test_utils import approx_equal
import time

pdb_str = """
CRYST1   27.475   35.191   27.490  90.00  90.00  90.00 P 21 21 21
HELIX    1   1 ALA E    1  ALA E   16  1                                  15
ATOM      1  N   ALA E   1       1.759  31.941   9.925  1.00 40.00           N
ATOM      2  CA  ALA E   1       3.062  32.633  10.148  1.00 40.00           C
ATOM      3  C   ALA E   1       3.992  31.964   9.219  1.00 40.00           C
ATOM      4  O   ALA E   1       4.361  30.892   9.486  1.00 40.00           O
ATOM      5  CB  ALA E   1       3.578  32.593  11.629  1.00 40.00           C
ATOM      6  N   ALA E   2       4.389  32.532   8.073  1.00 40.00           N
ATOM      7  CA  ALA E   2       4.997  31.670   7.007  1.00 40.00           C
ATOM      8  C   ALA E   2       6.344  32.226   6.827  1.00 40.00           C
ATOM      9  O   ALA E   2       6.457  33.374   6.459  1.00 40.00           O
ATOM     10  CB  ALA E   2       4.186  31.712   5.801  1.00 40.00           C
ATOM     11  N   ALA E   3       7.323  31.383   6.722  1.00 40.00           N
ATOM     12  CA  ALA E   3       8.731  31.854   6.676  1.00 40.00           C
ATOM     13  C   ALA E   3       9.508  31.272   5.451  1.00 40.00           C
ATOM     14  O   ALA E   3       9.129  31.527   4.320  1.00 40.00           O
ATOM     15  CB  ALA E   3       9.420  31.572   8.120  1.00 40.00           C
ATOM     16  N   ALA E   4      10.621  30.589   5.705  1.00 40.00           N
ATOM     17  CA  ALA E   4      11.014  29.454   4.925  1.00 40.00           C
ATOM     18  C   ALA E   4      10.993  28.115   5.761  1.00 40.00           C
ATOM     19  O   ALA E   4      11.488  27.073   5.256  1.00 40.00           O
ATOM     20  CB  ALA E   4      12.420  29.496   4.395  1.00 40.00           C
ATOM     21  N   ALA E   5      10.635  28.201   7.056  1.00 40.00           N
ATOM     22  CA  ALA E   5      10.697  27.184   8.067  1.00 40.00           C
ATOM     23  C   ALA E   5      10.153  25.946   7.644  1.00 40.00           C
ATOM     24  O   ALA E   5       9.079  25.531   8.089  1.00 40.00           O
ATOM     25  CB  ALA E   5      10.021  27.633   9.389  1.00 40.00           C
ATOM     26  N   ALA E   6      10.903  25.376   6.736  1.00 40.00           N
ATOM     27  CA  ALA E   6      10.419  24.081   6.259  1.00 40.00           C
ATOM     28  C   ALA E   6      11.466  23.185   5.699  1.00 40.00           C
ATOM     29  O   ALA E   6      11.208  22.085   5.358  1.00 40.00           O
ATOM     30  CB  ALA E   6       9.391  24.300   5.059  1.00 40.00           C
ATOM     31  N   ALA E   7      12.620  23.744   5.427  1.00 40.00           N
ATOM     32  CA  ALA E   7      13.712  23.003   4.826  1.00 40.00           C
ATOM     33  C   ALA E   7      14.263  21.771   5.723  1.00 40.00           C
ATOM     34  O   ALA E   7      14.241  20.569   5.342  1.00 40.00           O
ATOM     35  CB  ALA E   7      14.898  23.934   4.430  1.00 40.00           C
ATOM     36  N   ALA E   8      14.804  22.149   6.883  1.00 40.00           N
ATOM     37  CA  ALA E   8      14.886  21.255   8.032  1.00 40.00           C
ATOM     38  C   ALA E   8      13.776  21.338   9.025  1.00 40.00           C
ATOM     39  O   ALA E   8      13.598  22.439   9.414  1.00 40.00           O
ATOM     40  CB  ALA E   8      16.108  21.508   8.689  1.00 40.00           C
ATOM     41  N   ALA E   9      12.997  20.362   9.346  1.00 40.00           N
ATOM     42  CA  ALA E   9      11.950  20.437  10.207  1.00 40.00           C
ATOM     43  C   ALA E   9      12.278  19.286  11.156  1.00 40.00           C
ATOM     44  O   ALA E   9      12.694  19.626  12.369  1.00 40.00           O
ATOM     45  CB  ALA E   9      10.546  20.258   9.392  1.00 40.00           C
ATOM     46  N   ALA E  10      12.062  18.096  10.682  1.00 40.00           N
ATOM     47  CA  ALA E  10      12.247  16.893  11.433  1.00 40.00           C
ATOM     48  C   ALA E  10      13.481  16.037  11.024  1.00 40.00           C
ATOM     49  O   ALA E  10      13.462  15.218  10.069  1.00 40.00           O
ATOM     50  CB  ALA E  10      11.008  16.040  11.642  1.00 40.00           C
ATOM     51  N   ALA E  11      14.565  16.368  11.711  1.00 40.00           N
ATOM     52  CA  ALA E  11      15.841  15.689  11.590  1.00 40.00           C
ATOM     53  C   ALA E  11      16.245  14.526  12.581  1.00 40.00           C
ATOM     54  O   ALA E  11      16.001  13.360  12.345  1.00 40.00           O
ATOM     55  CB  ALA E  11      16.946  16.712  11.606  1.00 40.00           C
ATOM     56  N   ALA E  12      16.906  15.007  13.651  1.00 40.00           N
ATOM     57  CA  ALA E  12      17.201  14.155  14.824  1.00 40.00           C
ATOM     58  C   ALA E  12      16.166  14.467  15.926  1.00 40.00           C
ATOM     59  O   ALA E  12      16.031  13.720  16.937  1.00 40.00           O
ATOM     60  CB  ALA E  12      18.674  14.435  15.292  1.00 40.00           C
ATOM     61  N   ALA E  13      15.446  15.554  15.816  1.00 40.00           N
ATOM     62  CA  ALA E  13      14.475  15.957  16.664  1.00 40.00           C
ATOM     63  C   ALA E  13      13.176  15.008  16.622  1.00 40.00           C
ATOM     64  O   ALA E  13      12.046  15.543  16.623  1.00 40.00           O
ATOM     65  CB  ALA E  13      14.030  17.556  16.484  1.00 40.00           C
ATOM     66  N   ALA E  14      13.426  13.632  16.586  1.00 40.00           N
ATOM     67  CA  ALA E  14      12.382  12.548  16.667  1.00 40.00           C
ATOM     68  C   ALA E  14      12.662  11.572  17.847  1.00 40.00           C
ATOM     69  O   ALA E  14      12.005  11.694  18.842  1.00 40.00           O
ATOM     70  CB  ALA E  14      12.345  11.676  15.316  1.00 40.00           C
ATOM     71  N   ALA E  15      13.635  10.654  17.657  1.00 40.00           N
ATOM     72  CA  ALA E  15      14.068   9.871  18.845  1.00 40.00           C
ATOM     73  C   ALA E  15      15.166   8.955  18.307  1.00 40.00           C
ATOM     74  O   ALA E  15      15.066   8.379  17.148  1.00 40.00           O
ATOM     75  CB  ALA E  15      12.921   9.047  19.517  1.00 40.00           C
ATOM     76  N   ALA E  16      16.221   8.912  19.126  1.00 40.00           N
ATOM     77  CA  ALA E  16      17.365   8.057  18.883  1.00 40.00           C
ATOM     78  C   ALA E  16      17.209   6.988  17.701  1.00 40.00           C
ATOM     79  O   ALA E  16      18.176   6.806  16.935  1.00 40.00           O
ATOM     80  CB  ALA E  16      17.822   7.363  20.241  1.00 40.00           C
TER
END
"""

def get_interacting_o_n_distances(file_name):
  pdb_inp = iotbx.pdb.input(file_name = file_name)
  pdb_hierarchy = pdb_inp.construct_hierarchy()
  sn = pdb_hierarchy.atom_selection_cache().selection(string = "name N")
  so = pdb_hierarchy.atom_selection_cache().selection(string = "name O")
  xrs = pdb_inp.xray_structure_simple()
  uc = xrs.unit_cell()
  sites_frac = xrs.sites_frac()
  sfo = sites_frac.select(so)
  sfn = sites_frac.select(sn)
  assert sfo.size() == sfn.size()
  distances = flex.double()
  for i in range(sfo.size()):
    if(i+4>=sfo.size()): break
    distances.append(uc.distance(sfo[i], sfn[i+4]))
  return distances

def exercise(file_in  = "exerise_gm_ssr_helix_poor.pdb",
             file_out = "exerise_gm_ssr_helix_good.pdb"):
  """
  Exercise secondary structure restraints in geometry minimization:
  helix only, restraints defined in PDB file header.
  """
  of = open(file_in, "w")
  print(pdb_str, file=of)
  of.close()
  d = get_interacting_o_n_distances(file_name = file_in)
  assert flex.min(d) > 5.0
  cmd = " ".join([
    "phenix.geometry_minimization",
    "%s"%file_in,
    "output_file_name_prefix=%s"%file_out[:-4],
    "secondary_structure.enabled=true",
    "secondary_structure.protein.remove_outliers=False",
    "macro_cycles=5",
    "> zlog"])
  print(cmd)
  assert not easy_run.call(cmd)
  d = get_interacting_o_n_distances(file_name = file_out)
  print(d.min_max_mean().as_tuple())
  assert approx_equal(d.min_max_mean().as_tuple(), [2.9,2.9,2.9], 0.02)

if (__name__ == "__main__"):
  t0 = time.time()
  exercise()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
