from __future__ import print_function
import iotbx.pdb
from libtbx import easy_run
from libtbx.test_utils import approx_equal
import time

pdb_str_answer = """
CRYST1   15.183   12.883   13.723  90.00  90.00  90.00 P 1
SCALE1      0.065863  0.000000  0.000000        0.00000
SCALE2      0.000000  0.077622  0.000000        0.00000
SCALE3      0.000000  0.000000  0.072870        0.00000
HETATM    1  N   DGL C   1       7.598   5.061   5.224  1.00  3.10           N
HETATM    2  CA  DGL C   1       7.004   6.114   6.039  1.00  3.82           C
HETATM    3  CB  DGL C   1       7.154   5.790   7.527  1.00  1.62           C
HETATM    4  CG  DGL C   1       8.596   5.645   7.993  1.00  1.46           C
HETATM    5  CD  DGL C   1       9.361   6.954   7.951  1.00  5.50           C
HETATM    6  OE1 DGL C   1      10.607   6.912   7.878  1.00  6.96           O
HETATM    7  OE2 DGL C   1       8.720   8.025   7.998  1.00  3.35           O
HETATM    8  C   DGL C   1       5.533   6.317   5.692  1.00  3.43           C
HETATM    9  O   DGL C   1       4.767   6.874   6.479  1.00  2.22           O
HETATM   10  OXT DGL C   1       5.075   5.928   4.618  1.00  2.97           O
TER
"""

def exercise(prefix="exerise_gm_08"):
  """
  Exercise using D-amino-acid and CB restraints.
  """
  sel_fixed="not (name O or name OXT or name OE2 or name OE1 or name CG or name CB)"
  file_name = "%s.pdb"%prefix
  of = open(file_name, "w")
  print(pdb_str_answer, file=of)
  of.close()
  xrs_answer = iotbx.pdb.input(source_info=None,
    lines = pdb_str_answer).construct_hierarchy().extract_xray_structure()
  for i, cb_opt in enumerate(["true","false"]):
    ofnp = "%s_%s"%(prefix, str(i))
    cmd = " ".join([
      "phenix.geometry_minimization",
      "output_file_name_prefix=%s"%ofnp,
      "%s"%file_name,
      "selection='%s'"%sel_fixed,
      "c_beta_restraints=%s"%cb_opt,
      "> %s.zlog"%prefix])
    print(cmd)
    assert not easy_run.call(cmd)
    xrs = iotbx.pdb.input(file_name="%s.pdb"%ofnp).construct_hierarchy().\
        extract_xray_structure()
    d = xrs.distances(other=xrs_answer)
    dmmm = d.min_max_mean().as_tuple()
    assert approx_equal(dmmm, [0,0,0], 0.006), dmmm

if (__name__ == "__main__"):
  t0 = time.time()
  exercise()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
