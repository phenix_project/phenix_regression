from __future__ import print_function
import iotbx.pdb
from libtbx import easy_run
from libtbx.test_utils import approx_equal
import time, math

pdb_str_1 = """
CRYST1   13.368   15.183   12.784  90.00  90.00  90.00 P 1
ATOM  29578  C   ALA A  21       5.310  12.183   6.515  1.00 72.11           C
ATOM  29579  O   ALA A  21       4.155  11.817   6.734  1.00 63.91           O
ATOM  29586  N   CYS A  22       6.343  11.347   6.493  1.00 77.78           N
ATOM  29587  CA  CYS A  22       6.675  10.510   7.639  1.00 69.11           C
ATOM  29588  C   CYS A  22       6.562  11.291   8.944  1.00 70.95           C
ATOM  29590  CB  CYS A  22       8.085   9.934   7.491  1.00 67.39           C
ATOM  29591  SG  CYS A  22       8.321   8.905   6.023  1.00 78.04           S
TER
ATOM  30013 CU    Cu X  87       6.970   7.432   6.082  1.00 64.26          Cu
TER
ATOM  29750  N   HIS D  33       3.940   3.301   6.533  1.00 57.29           N
ATOM  29751  CA  HIS D  33       3.822   4.546   5.783  1.00 59.38           C
ATOM  29752  C   HIS D  33       4.730   4.538   4.557  1.00 59.29           C
ATOM  29753  O   HIS D  33       4.255   4.524   3.421  1.00 63.31           O
ATOM  29754  CB  HIS D  33       4.154   5.743   6.676  1.00 51.68           C
ATOM  29755  CG  HIS D  33       4.050   7.064   5.979  1.00 54.11           C
ATOM  29756  ND1 HIS D  33       5.122   7.662   5.352  1.00 61.83           N
ATOM  29757  CD2 HIS D  33       3.000   7.902   5.809  1.00 50.67           C
ATOM  29758  CE1 HIS D  33       4.738   8.811   4.827  1.00 65.11           C
ATOM  29759  NE2 HIS D  33       3.454   8.981   5.090  1.00 63.60           N
ATOM  29767  N   CYS D  34       6.037   4.547   4.795  1.00 58.94           N
ATOM  29768  CA  CYS D  34       7.013   4.541   3.712  1.00 57.77           C
ATOM  29769  C   CYS D  34       8.152   3.569   4.001  1.00 55.14           C
ATOM  29770  O   CYS D  34       8.264   3.040   5.107  1.00 54.27           O
ATOM  29771  CB  CYS D  34       7.568   5.949   3.483  1.00 54.11           C
ATOM  29772  SG  CYS D  34       8.404   6.664   4.918  1.00 51.96           S
ATOM  29777  N   GLN D  35       8.995   3.339   3.000  1.00 51.91           N
ATOM  29810  N   CYS D  37      10.368   4.493   6.688  1.00 62.48           N
ATOM  29811  CA  CYS D  37       9.700   4.437   7.983  1.00 52.29           C
ATOM  29812  C   CYS D  37       9.577   3.000   8.479  1.00 55.25           C
ATOM  29814  CB  CYS D  37       8.317   5.086   7.901  1.00 49.91           C
ATOM  29815  SG  CYS D  37       8.336   6.826   7.412  1.00 68.61           S
TER
END
"""

pdb_str_2 = """
CRYST1   13.368   15.183   12.784  90.00  90.00  90.00 P 1
ATOM  29578  C   ALA A  21       5.541  12.475   6.752  1.00 72.11           C
ATOM  29579  O   ALA A  21       4.397  12.053   6.919  1.00 63.91           O
ATOM  29586  N   CYS A  22       6.617  11.699   6.830  1.00 77.78           N
ATOM  29587  CA  CYS A  22       6.745  10.668   7.852  1.00 69.11           C
ATOM  29588  C   CYS A  22       6.684  11.268   9.253  1.00 70.95           C
ATOM  29590  CB  CYS A  22       8.051   9.891   7.670  1.00 67.39           C
ATOM  29591  SG  CYS A  22       8.204   9.051   6.076  1.00 78.04           S
TER
ATOM  30013 CU    CU X  87       7.170   7.393   5.653  1.00 64.26          Cu2+
TER
ATOM  29750  N  AALA D  32       1.012   3.872   6.048  0.70 57.29           N
ATOM  29751  CA AALA D  32       2.030   3.522   5.064  0.70 59.38           C
ATOM  29752  C  AALA D  32       3.311   3.042   5.741  0.70 59.29           C
ATOM  29753  O  AALA D  32       3.365   1.927   6.260  0.70 63.31           O
ATOM  29750  N  BALA D  32       5.023   1.442   6.617  0.30 57.29           N
ATOM  29751  CA BALA D  32       3.632   1.615   6.214  0.30 59.38           C
ATOM  29752  C  BALA D  32       3.427   2.948   5.501  0.30 59.29           C
ATOM  29753  O  BALA D  32       2.462   3.122   4.757  0.30 63.31           O
ATOM  29750  N   HIS D  33       4.340   3.885   5.735  1.00 57.29           N
ATOM  29751  CA  HIS D  33       4.260   5.204   5.115  1.00 59.38           C
ATOM  29752  C   HIS D  33       5.288   5.352   3.999  1.00 59.29           C
ATOM  29753  O   HIS D  33       4.950   5.725   2.876  1.00 63.31           O
ATOM  29754  CB  HIS D  33       4.459   6.301   6.163  1.00 51.68           C
ATOM  29755  CG  HIS D  33       4.385   7.689   5.607  1.00 54.11           C
ATOM  29756  ND1 HIS D  33       5.497   8.375   5.166  1.00 61.83           N
ATOM  29757  CD2 HIS D  33       3.332   8.519   5.419  1.00 50.67           C
ATOM  29758  CE1 HIS D  33       5.132   9.567   4.731  1.00 65.11           C
ATOM  29759  NE2 HIS D  33       3.823   9.680   4.874  1.00 63.60           N
ATOM  29767  N   CYS D  34       6.545   5.056   4.316  1.00 58.94           N
ATOM  29768  CA  CYS D  34       7.624   5.155   3.341  1.00 57.77           C
ATOM  29769  C   CYS D  34       8.667   4.064   3.559  1.00 55.14           C
ATOM  29770  O   CYS D  34       8.767   3.497   4.647  1.00 54.27           O
ATOM  29771  CB  CYS D  34       8.284   6.534   3.410  1.00 54.11           C
ATOM  29772  SG  CYS D  34       9.010   6.935   5.017  1.00 51.96           S
ATOM  29777  N   GLN D  35       9.441   3.776   2.518  1.00 51.91           N
ATOM  29810  N   CYS D  37      10.261   4.666   6.664  1.00 62.48           N
ATOM  29811  CA  CYS D  37       9.595   4.596   7.959  1.00 52.29           C
ATOM  29812  C   CYS D  37       9.546   3.163   8.477  1.00 55.25           C
ATOM  29814  CB  CYS D  37       8.180   5.171   7.866  1.00 49.91           C
ATOM  29815  SG  CYS D  37       8.109   6.902   7.349  1.00 68.61           S
TER
END
"""

pdb_str_3 = """
CRYST1   13.368   15.183   12.784  90.00  90.00  90.00 P 1
ATOM  29578  C   ALA A  21       5.541  12.475   6.752  1.00 72.11
ATOM  29579  O   ALA A  21       4.397  12.053   6.919  1.00 63.91
ATOM  29586  N   CYS A  22       6.617  11.699   6.830  1.00 77.78
ATOM  29587  CA  CYS A  22       6.745  10.668   7.852  1.00 69.11
ATOM  29588  C   CYS A  22       6.684  11.268   9.253  1.00 70.95
ATOM  29590  CB  CYS A  22       8.051   9.891   7.670  1.00 67.39
ATOM  29591  SG  CYS A  22       8.204   9.051   6.076  1.00 78.04
TER
ATOM  29750  N  AALA D  32       1.012   3.872   6.048  0.70 57.29
ATOM  29751  CA AALA D  32       2.030   3.522   5.064  0.70 59.38
ATOM  29752  C  AALA D  32       3.311   3.042   5.741  0.70 59.29
ATOM  29753  O  AALA D  32       3.365   1.927   6.260  0.70 63.31
ATOM  29750  N  BALA D  32       5.023   1.442   6.617  0.30 57.29
ATOM  29751  CA BALA D  32       3.632   1.615   6.214  0.30 59.38
ATOM  29752  C  BALA D  32       3.427   2.948   5.501  0.30 59.29
ATOM  29753  O  BALA D  32       2.462   3.122   4.757  0.30 63.31
ATOM  29750  N   HIS D  33       4.340   3.885   5.735  1.00 57.29
ATOM  29751  CA  HIS D  33       4.260   5.204   5.115  1.00 59.38
ATOM  29752  C   HIS D  33       5.288   5.352   3.999  1.00 59.29
ATOM  29753  O   HIS D  33       4.950   5.725   2.876  1.00 63.31
ATOM  29754  CB  HIS D  33       4.459   6.301   6.163  1.00 51.68
ATOM  29755  CG  HIS D  33       4.385   7.689   5.607  1.00 54.11
ATOM  29756  ND1 HIS D  33       5.497   8.375   5.166  1.00 61.83
ATOM  29757  CD2 HIS D  33       3.332   8.519   5.419  1.00 50.67
ATOM  29758  CE1 HIS D  33       5.132   9.567   4.731  1.00 65.11
ATOM  29759  NE2 HIS D  33       3.823   9.680   4.874  1.00 63.60
ATOM  29767  N   CYS D  34       6.545   5.056   4.316  1.00 58.94
ATOM  29768  CA  CYS D  34       7.624   5.155   3.341  1.00 57.77
ATOM  29769  C   CYS D  34       8.667   4.064   3.559  1.00 55.14
ATOM  29770  O   CYS D  34       8.767   3.497   4.647  1.00 54.27
ATOM  29771  CB  CYS D  34       8.284   6.534   3.410  1.00 54.11
ATOM  29772  SG  CYS D  34       9.010   6.935   5.017  1.00 51.96
ATOM  29777  N   GLN D  35       9.441   3.776   2.518  1.00 51.91
ATOM  29810  N   CYS D  37      10.261   4.666   6.664  1.00 62.48
ATOM  29811  CA  CYS D  37       9.595   4.596   7.959  1.00 52.29
ATOM  29812  C   CYS D  37       9.546   3.163   8.477  1.00 55.25
ATOM  29814  CB  CYS D  37       8.180   5.171   7.866  1.00 49.91
ATOM  29815  SG  CYS D  37       8.109   6.902   7.349  1.00 68.61
ATOM  30013 CU    CU D  87       7.170   7.393   5.653  1.00 64.26
TER

END
"""

edits_str = """
  geometry_restraints.edits {
    bond {
      action = *add
      atom_selection_1 = resseq 22 and name SG
      atom_selection_2 = resseq 87 and element CU
      distance_ideal = 2.0
      sigma = 0.01
    }
    bond {
      action = *add
      atom_selection_1 = resseq 37 and name SG
      atom_selection_2 = resseq 87 and element CU
      distance_ideal = 2.0
      sigma = 0.01
    }
    bond {
      action = *add
      atom_selection_1 = resseq 34 and name SG
      atom_selection_2 = resseq 87 and element CU
      distance_ideal = 2.0
      sigma = 0.01
    }
    #
    bond {
      action = *add
      atom_selection_1 = resseq 22 and name SG
      atom_selection_2 = resseq 37 and name SG
      distance_ideal = 2.5
      sigma = 0.01
    }
    bond {
      action = *add
      atom_selection_1 = resseq 22 and name SG
      atom_selection_2 = resseq 34 and name SG
      distance_ideal = 2.5
      sigma = 0.01
    }
    bond {
      action = *add
      atom_selection_1 = resseq 37 and name SG
      atom_selection_2 = resseq 34 and name SG
      distance_ideal = 2.5
      sigma = 0.01
    }
    #
    bond {
      action = *add
      atom_selection_1 = resseq 33 and name ND1
      atom_selection_2 = resseq 87 and element CU
      distance_ideal = 2.0
      sigma = 0.01
    }
}
"""

def get_interacting_distances(file_name):
  pdb_inp = iotbx.pdb.input(file_name = file_name)
  asc = pdb_inp.construct_hierarchy().atom_selection_cache()
  s1 = asc.selection(string = "resseq 22 and name SG")
  s2 = asc.selection(string = "resseq 37 and name SG")
  s3 = asc.selection(string = "resseq 34 and name SG")
  sz = asc.selection(string = "resseq 87 and name CU")
  xrs = pdb_inp.xray_structure_simple()
  uc = xrs.unit_cell()
  sites_frac = xrs.sites_frac()
  s1 = sites_frac.select(s1)[0]
  s2 = sites_frac.select(s2)[0]
  s3 = sites_frac.select(s3)[0]
  sz = sites_frac.select(sz)[0]
  return uc.distance(s1, s2),\
         uc.distance(s1, s3),\
         uc.distance(s2, s3),\
         uc.distance(s1, sz),\
         uc.distance(s2, sz),\
         uc.distance(s3, sz)

def exercise(prefix="exerise_gm_disulfides_exclusion"):
  """
  Exercise SS bonds exclusion.
  """
  for i_pdb, pdb_str in enumerate([pdb_str_1, pdb_str_2, pdb_str_3]):
    file_in  = "%s_start%s.pdb"%(prefix, str(i_pdb))
    file_out = "%s_refined%s.pdb"%(prefix, str(i_pdb))
    edits_file = "%s.edits"%prefix
    of = open(file_in, "w")
    print(pdb_str, file=of)
    of.close()
    of = open(edits_file, "w")
    print(edits_str, file=of)
    of.close()
    d_start = get_interacting_distances(file_name = file_in)
    assert approx_equal(d_start,[2.5,2.5,2.5, 2,2,2], 1.e-3)
    #
    sel_sg = "resseq 22 and name SG or resseq 37 and name SG or resseq 34 and name SG"
    #
    def run(cmd):
      cmd += " > zlog"
      print(cmd)
      assert not easy_run.call(cmd)
    #
    cmd_base = " ".join([
      "phenix.geometry_minimization",
      "cdl=False",
      "%s"%file_in,
      "output_file_name_prefix=%s"%file_out[:-4],
      "add_angle_and_dihedral_restraints_for_disulfides=False",
      "macro_cycles=3"])
    # CASE 1 ---------------------------------
    cmd = " ".join([cmd_base, "exclusion_distance_cutoff=0"])
    print(cmd)
    run(cmd)
    d_final = get_interacting_distances(file_name = file_out)
    assert approx_equal(d_final[:3], [2.03, 2.03, 2.03], 0.01)
    # CASE 2 ---------------------------------
    cmd = " ".join([cmd_base, ""])
    run(cmd)
    d_final = get_interacting_distances(file_name = file_out)
    assert d_final[0]>3.5 and d_final[1]>3.5 and d_final[2]>3.5
    # CASE 3 ---------------------------------
    cmd = " ".join([cmd_base, "%s"%edits_file])
    run(cmd)
    d_final = get_interacting_distances(file_name = file_out)
    assert approx_equal(d_final,[2.5,2.5,2.5, 2,2,2], 1.e-2)
    # CASE 4 ---------------------------------
    cmd = " ".join([cmd_base, "%s"%edits_file, "exclusion_distance_cutoff=0"])
    run(cmd)
    d_final = get_interacting_distances(file_name = file_out)
    assert approx_equal(d_final,[2.5,2.5,2.5, 2,2,2], 1.e-2)
    # CASE 5 ---------------------------------
    cmd = " ".join([cmd_base,
                    "disulfide_bond_exclusions_selection_string='%s'"%sel_sg])
    run(cmd)
    d_final = get_interacting_distances(file_name = file_out)
    assert d_final[0]>3.5 and d_final[1]>3.5 and d_final[2]>3.5
    # CASE 5 ---------------------------------
    cmd = " ".join([cmd_base,
                    "%s"%edits_file,
                    "disulfide_bond_exclusions_selection_string='%s'"%sel_sg])
    run(cmd)
    d_final = get_interacting_distances(file_name = file_out)
    assert approx_equal(d_final,[2.5,2.5,2.5, 2,2,2], 1.e-2)

def exercise_1(prefix="tst_6_exercise_1"):
  pdb_in = """
CRYST1   14.040   14.657   18.525  90.00  90.00  90.00 P 1
ATOM    789  N   CYS B   7       5.818   8.421  12.562  1.00 16.68      B
ATOM    790  CA  CYS B   7       6.512   8.470  11.300  1.00 13.05      B
ATOM    791  C   CYS B   7       7.587   7.389  11.279  1.00 12.40      B
ATOM    792  O   CYS B   7       8.295   7.156  12.259  1.00 13.61      B
ATOM    793  CB  CYS B   7       7.092   9.888  11.154  1.00 16.29      B
ATOM    794  SG  CYS B   7       5.886  11.175  10.891  1.00 17.05      B
ATOM   1488  N   CYS B  96       6.286   9.723   6.188  1.00 13.99      B
ATOM   1489  CA  CYS B  96       6.929  10.836   6.925  1.00 12.14      B
ATOM   1490  C   CYS B  96       8.113  10.324   7.765  1.00 14.37      B
ATOM   1491  O   CYS B  96       8.264   9.090   7.838  1.00 15.57      B
ATOM   1492  CB  CYS B  96       5.986  11.657   7.822  1.00 14.33      B
ATOM   1493  SG  CYS B  96       5.029  10.705   9.098  1.00 17.05      B
TER
"""
  of = open("%s.pdb"%prefix, "w")
  print(pdb_in, file=of)
  of.close()
  cmd = " ".join([
    "phenix.geometry_minimization",
    "%s.pdb"%prefix,
    "output_file_name_prefix=%s_minimized"%prefix,
    "> %s.zlog"%prefix])
  print(cmd)
  assert not easy_run.call(cmd)
  pdb_inp = iotbx.pdb.input(file_name="%s_minimized.pdb"%prefix)
  h = pdb_inp.construct_hierarchy()
  sg1,sg2, cb1,cb2 = [None,]*4
  for a in h.atoms():
    n = a.name.strip()
    if(  n=="SG" and sg1 is None): sg1 = a
    elif(n=="SG" and sg2 is None): sg2 = a
    elif(n=="CB" and cb1 is None): cb1 = a
    elif(n=="CB" and cb2 is None): cb2 = a
  assert approx_equal(math.sqrt(
    (sg1.xyz[0]-sg2.xyz[0])**2+
    (sg1.xyz[1]-sg2.xyz[1])**2+
    (sg1.xyz[2]-sg2.xyz[2])**2), 2.03, eps=0.01)
  assert approx_equal(sg1.angle(cb1,sg2,True), 104.05, 1)
  assert approx_equal(sg2.angle(cb2,sg1,True), 104.05, 1)

if (__name__ == "__main__"):
  t0 = time.time()
  exercise()
  exercise_1()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
