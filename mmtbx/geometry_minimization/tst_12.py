from __future__ import print_function
from cctbx.array_family import flex
import iotbx.pdb
from libtbx import easy_run
from libtbx.test_utils import approx_equal
import time, math

pdb_str = """
CRYST1   17.955   13.272   13.095  90.00  90.00  90.00 P 1           0
ATOM   1942  N   TYR A 139      10.241   7.920   5.000  1.00 10.00           N
ATOM   1943  CA  TYR A 139      10.853   7.555   6.271  1.00 10.00           C
ATOM   1944  C   TYR A 139      12.362   7.771   6.227  1.00 10.00           C
ATOM   1945  O   TYR A 139      12.955   8.272   7.181  1.00 10.00           O
ATOM   1946  CB  TYR A 139      10.540   6.098   6.617  1.00 10.00           C
ATOM   1947  CG  TYR A 139       9.063   5.805   6.749  1.00 10.00           C
ATOM   1948  CD1 TYR A 139       8.316   5.391   5.654  1.00 10.00           C
ATOM   1949  CD2 TYR A 139       8.414   5.943   7.969  1.00 10.00           C
ATOM   1950  CE1 TYR A 139       6.966   5.122   5.770  1.00 10.00           C
ATOM   1951  CE2 TYR A 139       7.064   5.676   8.095  1.00 10.00           C
ATOM   1952  CZ  TYR A 139       6.345   5.266   6.993  1.00 10.00           C
ATOM   1953  OH  TYR A 139       5.000   5.000   7.113  1.00 10.00           O
ATOM      0  HA  TYR A 139      10.480   8.127   6.960  1.00 10.00           H   new
ATOM      0  HB2 TYR A 139      10.915   5.524   5.931  1.00 10.00           H   new
ATOM      0  HB3 TYR A 139      10.982   5.870   7.450  1.00 10.00           H   new
ATOM      0  HD1 TYR A 139       8.732   5.293   4.828  1.00 10.00           H   new
ATOM      0  HD2 TYR A 139       8.896   6.220   8.714  1.00 10.00           H   new
ATOM      0  HE1 TYR A 139       6.479   4.845   5.028  1.00 10.00           H   new
ATOM      0  HE2 TYR A 139       6.643   5.772   8.919  1.00 10.00           H   new
ATOM      0  HH  TYR A 139       4.759   5.128   7.907  1.00 10.00           H   new
TER
END

"""

def check(file_name):
  atoms = iotbx.pdb.input(file_name = file_name).atoms()
  distances = flex.double()
  for i, a1 in enumerate(atoms):
    for j, a2 in enumerate(atoms):
      if(i<j):
        e1 = a1.element.strip()
        e2 = a2.element.strip()
        r1 = a1.xyz
        r2 = a2.xyz
        d = math.sqrt((r1[0]-r2[0])**2+(r1[1]-r2[1])**2+(r1[2]-r2[2])**2)
        if(d<1.5 and d>0.01 and [e1,e2].count("H")==1):
          distances.append(d)
  return distances

def exercise(prefix="tst_12"):
  """
  Exercise riding_h.
  """
  of = open("%s.pdb"%prefix, "w")
  print(pdb_str, file=of)
  of.close()
  for i, opt in enumerate(["true","false"]):
    cmd = " ".join([
      "phenix.geometry_minimization",
      "%s.pdb"%prefix,
      "output_file_name_prefix=%s_minimized"%prefix,
      "riding_h=true",
      "use_neutron_distances=%s"%opt, 
      "> zlog"])
    print(cmd)
    assert not easy_run.call(cmd)
    d = check(file_name="%s_minimized.pdb"%prefix)
    if(i==0): assert flex.mean(d)>1.
    else:     assert flex.mean(d)<1.

if (__name__ == "__main__"):
  t0 = time.time()
  exercise()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
