from __future__ import print_function
import iotbx.pdb
from libtbx import easy_run
import libtbx.load_env
import time
import os

import iotbx.pdb
from libtbx import easy_run
import libtbx.load_env
import time
import os
import iotbx.pdb
from libtbx.test_utils import approx_equal

from mmtbx.secondary_structure import ss_validation
import mmtbx.model

def test_result_from_command(cmd, lines, fname):
  print(cmd)
  assert not easy_run.call(cmd)
  out = open(fname, "r")
  out_lines = out.readlines()
  out.close()
  for needed_string in lines:
    assert needed_string in out_lines, "'%s' not in output!" % needed_string

def exercise(prefix=""):
  out_fname = "validation_result.txt"
  pdb_path = libtbx.env.find_in_repositories(
      relative_path="phenix_regression/pdb/1ywf.pdb",
      test=os.path.isfile)
  cmd = " ".join([
      "phenix.secondary_structure_validation",
      "%s" % pdb_path,
      ">%s" % out_fname])
  lines = ["  Total declared H-bonds         : 109\n",
           "  Total Ramachandran outliers    : 0\n",
           "  Total mediocre H-bonds (3.0-3.5A): 44\n",
           "Bad annotation found:\n"]
  test_result_from_command(cmd, lines, out_fname)

  out_fname = "validation_result2.txt"
  cmd = " ".join([
      "phenix.secondary_structure_validation",
      "%s" % pdb_path,
      "bad_hbond_cutoff=3.7",
      ">%s" % out_fname])
  lines = ["  Total declared H-bonds         : 109\n",
           "  Total Ramachandran outliers    : 0\n",
           "  Total mediocre H-bonds (3.0-3.7A): 45\n",
           "Bad annotation found:\n"]
  test_result_from_command(cmd, lines, out_fname)

def exercise2():
  """ The same as exercise, but using python calling"""
  params = ss_validation.validate.get_default_params()
  params.ss_validation.bad_hbond_cutoff = 3.5 # default
  pdb_path = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/1ywf.pdb",
    test=os.path.isfile)
  pdb_inp = iotbx.pdb.input(file_name=pdb_path)
  model = mmtbx.model.manager(model_input=pdb_inp)
  r = ss_validation.validate(
      model=model, params=params.ss_validation).get_results()
  print(r)
  assert approx_equal(r.n1, 62.5)
  assert approx_equal(r.n2, 50.0)
  assert approx_equal(r.n3, 0.0)
  assert approx_equal(r.n4, 5.5045871559)
  assert approx_equal(r.n5, 25.0)

  assert r.n_bad_hbonds == 6, r.n_bad_hbonds
  assert r.n_mediocre_hbonds == 44, r.n_mediocre_hbonds
  assert r.n_hbonds == 109, r.n_hbonds
  assert r.n_bad_helix_sheet_records == 10, r.n_bad_helix_sheet_records

def exercise3():
  """ The same as exercise2, but changing def params"""
  params = ss_validation.validate.get_default_params()
  params.ss_validation.bad_hbond_cutoff = 3.7 # default
  pdb_path = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/1ywf.pdb",
    test=os.path.isfile)
  pdb_inp = iotbx.pdb.input(file_name=pdb_path)
  model = mmtbx.model.manager(model_input=pdb_inp)
  r = ss_validation.validate(
      model=model, params=params.ss_validation).get_results()
  print(r)
  assert approx_equal(r.n1, 56.25)
  assert approx_equal(r.n2, 50.0)
  assert approx_equal(r.n3, 0.0)
  assert approx_equal(r.n4, 4.587155963303)
  assert approx_equal(r.n5, 18.75)

  assert r.n_bad_hbonds == 5, r.n_bad_hbonds
  assert r.n_mediocre_hbonds == 45, r.n_mediocre_hbonds
  assert r.n_hbonds == 109, r.n_hbonds
  assert r.n_bad_helix_sheet_records == 9, r.n_bad_helix_sheet_records


if (__name__ == "__main__"):
  t0 = time.time()
  exercise()
  exercise2()
  exercise3()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
