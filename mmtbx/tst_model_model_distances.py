from __future__ import print_function
from libtbx import easy_run
import os

m1_1="""
CRYST1   27.475   35.191   27.490  90.00  90.00  90.00 P 21 21 21
ATOM      1  N   ALA E   1       6.670  34.962  11.314  1.00 20.00           N
ATOM      2  CA  ALA E   1       6.479  33.967  10.265  1.00 20.00           C
ATOM      3  C   ALA E   1       6.562  32.548  10.822  1.00 20.00           C
ATOM      4  O   ALA E   1       7.426  32.244  11.644  1.00 20.00           O
ATOM      5  CB  ALA E   1       7.500  34.164   9.155  1.00 20.00           C
ATOM      6  N   HIS E   2       5.659  31.685  10.368  1.00 20.00           N
ATOM      7  CA  HIS E   2       5.651  30.290  10.795  1.00 20.00           C
ATOM      8  C   HIS E   2       6.795  29.509  10.158  1.00 20.00           C
ATOM      9  O   HIS E   2       7.315  28.565  10.751  1.00 20.00           O
ATOM     10  CB  HIS E   2       4.311  29.623  10.463  1.00 20.00           C
ATOM     11  CG  HIS E   2       4.166  28.239  11.019  1.00 20.00           C
ATOM     12  ND1 HIS E   2       4.713  27.131  10.407  1.00 20.00           N
ATOM     13  CD2 HIS E   2       3.545  27.784  12.133  1.00 20.00           C
ATOM     14  CE1 HIS E   2       4.431  26.054  11.118  1.00 20.00           C
ATOM     15  NE2 HIS E   2       3.723  26.422  12.170  1.00 20.00           N
"""

m2_1="""
CRYST1   27.475   35.191   27.490  90.00  90.00  90.00 P 21 21 21
ATOM      1  N   ALA E   1       6.709  31.817   5.664  1.00 20.00           N
ATOM      2  CA  ALA E   1       7.794  30.914   6.029  1.00 20.00           C
ATOM      3  C   ALA E   1       7.267  29.691   6.773  1.00 20.00           C
ATOM      4  O   ALA E   1       7.942  29.146   7.647  1.00 20.00           O
ATOM      5  CB  ALA E   1       8.831  31.643   6.870  1.00 20.00           C
ATOM      6  N   HIS E   2       6.057  29.267   6.421  1.00 20.00           N
ATOM      7  CA  HIS E   2       5.434  28.110   7.053  1.00 20.00           C
ATOM      8  C   HIS E   2       6.176  26.824   6.701  1.00 20.00           C
ATOM      9  O   HIS E   2       6.477  26.012   7.576  1.00 20.00           O
ATOM     10  CB  HIS E   2       3.965  28.000   6.640  1.00 20.00           C
ATOM     11  CG  HIS E   2       3.140  29.191   7.016  1.00 20.00           C
ATOM     12  ND1 HIS E   2       2.481  29.289   8.222  1.00 20.00           N
ATOM     13  CD2 HIS E   2       2.869  30.335   6.345  1.00 20.00           C
ATOM     14  CE1 HIS E   2       1.838  30.441   8.278  1.00 20.00           C
ATOM     15  NE2 HIS E   2       2.057  31.096   7.152  1.00 20.00           N
"""

m1_2="""
CRYST1   27.475   35.191   27.490  90.00  90.00  90.00 P 21 21 21
ATOM      1  N   ALA E   1       6.670  34.962  11.314  1.00 20.00           N
ATOM      2  CA  ALA E   1       6.479  33.967  10.265  1.00 20.00           C
ATOM      3  C   ALA E   1       6.562  32.548  10.822  1.00 20.00           C
ATOM      4  O   ALA E   1       7.426  32.244  11.644  1.00 20.00           O
ATOM      5  CB  ALA E   1       7.500  34.164   9.155  1.00 20.00           C
ATOM      6  N   HIS B   2       5.659  31.685  10.368  1.00 20.00           N
ATOM      7  CA  HIS B   2       5.651  30.290  10.795  1.00 20.00           C
ATOM      8  C   HIS B   2       6.795  29.509  10.158  1.00 20.00           C
ATOM      9  O   HIS B   2       7.315  28.565  10.751  1.00 20.00           O
ATOM     10  CB  HIS B   2       4.311  29.623  10.463  1.00 20.00           C
ATOM     11  CG  HIS B   2       4.166  28.239  11.019  1.00 20.00           C
ATOM     12  ND1 HIS B   2       4.713  27.131  10.407  1.00 20.00           N
ATOM     13  CD2 HIS B   2       3.545  27.784  12.133  1.00 20.00           C
ATOM     14  CE1 HIS B   2       4.431  26.054  11.118  1.00 20.00           C
ATOM     15  NE2 HIS B   2       3.723  26.422  12.170  1.00 20.00           N
"""

m2_2="""
CRYST1   27.475   35.191   27.490  90.00  90.00  90.00 P 21 21 21
ATOM      1  N   ALA E   1       6.709  31.817   5.664  1.00 20.00           N
ATOM      2  CA  ALA E   1       7.794  30.914   6.029  1.00 20.00           C
ATOM      3  C   ALA E   1       7.267  29.691   6.773  1.00 20.00           C
ATOM      4  O   ALA E   1       7.942  29.146   7.647  1.00 20.00           O
ATOM      5  CB  ALA E   1       8.831  31.643   6.870  1.00 20.00           C
ATOM      6  N   HIS B   2       6.057  29.267   6.421  1.00 20.00           N
ATOM      7  CA  HIS B   2       5.434  28.110   7.053  1.00 20.00           C
ATOM      8  C   HIS B   2       6.176  26.824   6.701  1.00 20.00           C
ATOM      9  O   HIS B   2       6.477  26.012   7.576  1.00 20.00           O
ATOM     10  CB  HIS B   2       3.965  28.000   6.640  1.00 20.00           C
ATOM     11  CG  HIS B   2       3.140  29.191   7.016  1.00 20.00           C
ATOM     12  ND1 HIS B   2       2.481  29.289   8.222  1.00 20.00           N
ATOM     13  CD2 HIS B   2       2.869  30.335   6.345  1.00 20.00           C
ATOM     14  CE1 HIS B   2       1.838  30.441   8.278  1.00 20.00           C
ATOM     15  NE2 HIS B   2       2.057  31.096   7.152  1.00 20.00           N
"""

m1_3="""
CRYST1   27.475   35.191   27.490  90.00  90.00  90.00 P 21 21 21
MODEL        1
ATOM      1  N   ALA E   1       6.670  34.962  11.314  1.00 20.00           N
ATOM      2  CA  ALA E   1       6.479  33.967  10.265  1.00 20.00           C
ATOM      3  C   ALA E   1       6.562  32.548  10.822  1.00 20.00           C
ATOM      4  O   ALA E   1       7.426  32.244  11.644  1.00 20.00           O
ATOM      5  CB  ALA E   1       7.500  34.164   9.155  1.00 20.00           C
ENDMDL
MODEL        2
ATOM      6  N   HIS E   2       5.659  31.685  10.368  1.00 20.00           N
ATOM      7  CA  HIS E   2       5.651  30.290  10.795  1.00 20.00           C
ATOM      8  C   HIS E   2       6.795  29.509  10.158  1.00 20.00           C
ATOM      9  O   HIS E   2       7.315  28.565  10.751  1.00 20.00           O
ATOM     10  CB  HIS E   2       4.311  29.623  10.463  1.00 20.00           C
ATOM     11  CG  HIS E   2       4.166  28.239  11.019  1.00 20.00           C
ATOM     12  ND1 HIS E   2       4.713  27.131  10.407  1.00 20.00           N
ATOM     13  CD2 HIS E   2       3.545  27.784  12.133  1.00 20.00           C
ATOM     14  CE1 HIS E   2       4.431  26.054  11.118  1.00 20.00           C
ATOM     15  NE2 HIS E   2       3.723  26.422  12.170  1.00 20.00           N
ENDMDL
"""

m2_3="""
CRYST1   27.475   35.191   27.490  90.00  90.00  90.00 P 21 21 21
MODEL        1
ATOM      1  N   ALA E   1       6.709  31.817   5.664  1.00 20.00           N
ATOM      2  CA  ALA E   1       7.794  30.914   6.029  1.00 20.00           C
ATOM      3  C   ALA E   1       7.267  29.691   6.773  1.00 20.00           C
ATOM      4  O   ALA E   1       7.942  29.146   7.647  1.00 20.00           O
ATOM      5  CB  ALA E   1       8.831  31.643   6.870  1.00 20.00           C
ENDMDL
MODEL        2
ATOM      6  N   HIS E   2       6.057  29.267   6.421  1.00 20.00           N
ATOM      7  CA  HIS E   2       5.434  28.110   7.053  1.00 20.00           C
ATOM      8  C   HIS E   2       6.176  26.824   6.701  1.00 20.00           C
ATOM      9  O   HIS E   2       6.477  26.012   7.576  1.00 20.00           O
ATOM     10  CB  HIS E   2       3.965  28.000   6.640  1.00 20.00           C
ATOM     11  CG  HIS E   2       3.140  29.191   7.016  1.00 20.00           C
ATOM     12  ND1 HIS E   2       2.481  29.289   8.222  1.00 20.00           N
ATOM     13  CD2 HIS E   2       2.869  30.335   6.345  1.00 20.00           C
ATOM     14  CE1 HIS E   2       1.838  30.441   8.278  1.00 20.00           C
ATOM     15  NE2 HIS E   2       2.057  31.096   7.152  1.00 20.00           N
ENDMDL
"""

def exercise_00(prefix="tst_model_model_distances"):
  for i,p in enumerate([[m1_1,m2_1], [m1_2,m2_2], [m1_3,m2_3]]):
    fns = []
    for j in [1,2]:
      fn="%s_%s_%s.pdb"%(str(i), prefix, str(j))
      fns.append(fn)
      of = open(fn, "w")
      print(p[j-1], file=of)
      of.close()
    cmd = "phenix.model_model_distances %s %s > %s_%s.zlog"%(fns[0],fns[1],
      str(i),prefix)
    print(cmd)
    assert not easy_run.call(cmd)

if (__name__ == "__main__") :
  exercise_00()

  print("OK")
