#! /bin/csh -fe

set PHENIX_REGRESSION_DIR="`libtbx.find_in_repositories phenix_regression`"

mmtbx.super $PHENIX_REGRESSION_DIR/pdb/phe.pdb $PHENIX_REGRESSION_DIR/pdb/phe.pdb moved=zap.pdb alignment_style=local matrix=dayhoff > tmp1

mmtbx.super fixed=$PHENIX_REGRESSION_DIR/pdb/phe.pdb moving=$PHENIX_REGRESSION_DIR/pdb/phe.pdb moved=zap.pdb alignment_style=local matrix=dayhoff > tmp2

diff tmp1 tmp2 | libtbx.assert_stdin ""

echo "OK"
