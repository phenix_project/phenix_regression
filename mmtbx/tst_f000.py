from __future__ import print_function
import iotbx.pdb
from libtbx import easy_run
import time

pdb_str = """
CRYST1   14.024   12.503   15.347  90.00  90.00  90.00 P 1
ATOM      1  C   ALA B  49       6.669   4.947   4.991  1.00 31.78           C
TER
END
"""

def exercise(prefix="tst_f000"):
  """
  Exercise phenix.f000 tool.
  """
  of=open("%s.pdb"%prefix,"w")
  print(pdb_str, file=of)
  of.close()
  result = easy_run.fully_buffered(
    "phenix.f000 %s.pdb mean_solvent_density=0"%prefix).raise_if_errors()
  found = False
  for line in result.stdout_lines:
    if(line.startswith("Estimate of F(0,0,0)=")):
      line.split()[2] == "F(0,0,0)=6.000"
      found = True
  assert found
  assert not easy_run.call("phenix.f000 %s.pdb"%prefix)

if (__name__ == "__main__"):
  t0 = time.time()
  exercise()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
