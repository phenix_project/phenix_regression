from __future__ import print_function
from libtbx import easy_run
import libtbx.load_env
import os
from scitbx.array_family import flex
from libtbx.test_utils import approx_equal

def exercise_00():
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/mmtbx/map_value_at_point/model.pdb",
    test=os.path.isfile)
  cmd = " ".join([
    "phenix.fmodel",
    "%s"%pdb_file,
    "high_res=1.0",
    "label='2mFo-DFc'",
    "output.file_name='e00.mtz'"
    ">tmp_map_value_at_point.log"
  ])
  assert not easy_run.call(cmd)
  for label in ["None", '2mFo-DFc']:
    cmd = " ".join([
      "phenix.map_value_at_point",
      "e00.mtz",
      "point='5 5 5'",
      "point='10 10 10'",
      "point='0 0 5'",
      "point='1 1 1'",
      "point='8 8 8'",
      "grid_step=0.1",
      "miller_array.labels.name='%s'"%label,
      "> tmp_map_value_at_point.log"
    ])
    assert not easy_run.call(cmd)
    counter = 0
    for line in open("tmp_map_value_at_point.log","r").readlines():
      line = line.strip()
      if(line.startswith("Input point: (5.000,5.000,5.000)")):
        v = float(line.split()[7])
        assert v > 40. and v < 46.5, v
        counter += 1
      if(line.startswith("Input point: (10.000,10.000,10.000)")):
        v = float(line.split()[7])
        assert v > 18. and v < 24., v
        counter += 1
      if(line.startswith("Input point: (0.000,0.000,5.000)")):
        v = float(line.split()[7])
        assert v > 17. and v < 22., v
        counter += 1
      if(line.startswith("Input point: (1.000,1.000,1.000)")):
        v = float(line.split()[7])
        assert v > 0. and v < 2., v
        counter += 1
      if(line.startswith("Input point: (8.000,8.000,8.000)")):
        v = float(line.split()[7])
        assert v > -1. and v < 1., v
        counter += 1
    assert counter == 5

def exercise_01():
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/mmtbx/map_value_at_point/model.pdb",
    test=os.path.isfile)
  cmd = " ".join([
    "phenix.fmodel",
    "%s"%pdb_file,
    "high_res=1.0",
    "label='f-obs'",
    "r_free=0.05",
    "type=real",
    "output.file_name='e01.mtz'",
    ">tmp_map_value_at_point.log"
  ])
  assert not easy_run.call(cmd)
  cmd = " ".join([
    "phenix.refine",
    "%s"%pdb_file,
    "e01.mtz",
    "strategy=none",
    "main.number_of_mac=0",
    "output.prefix='r01'",
    "--overwrite",
    ">tmp_map_value_at_point.log"
  ])
  assert not easy_run.call(cmd)
  cmd = " ".join([
    "phenix.map_value_at_point",
    "r01_001.mtz",
    "point='5 5 5'",
    "point='10 10 10'",
    "point='0 0 5'",
    "point='1 1 1'",
    "point='8 8 8'",
    "grid_step=0.1",
    "miller_array.labels.name='%s'"%"2FOFCWT_no_fill",
    "> tmp_map_value_at_point.log"
  ])
  assert not easy_run.call(cmd)
  counter = 0
  for line in open("tmp_map_value_at_point.log","r").readlines():
    line = line.strip()
    if(line.startswith("Input point: (5.000,5.000,5.000)")):
      v = float(line.split()[7])
      assert v > 43. and v < 48.
      counter += 1
    if(line.startswith("Input point: (10.000,10.000,10.000)")):
      v = float(line.split()[7])
      assert v > 20. and v < 24.
      counter += 1
    if(line.startswith("Input point: (0.000,0.000,5.000)")):
      v = float(line.split()[7])
      assert v > 17. and v < 22.
      counter += 1
    if(line.startswith("Input point: (1.000,1.000,1.000)")):
      v = float(line.split()[7])
      assert v > 0. and v < 2.
      counter += 1
    if(line.startswith("Input point: (8.000,8.000,8.000)")):
      v = float(line.split()[7])
      assert v > -1. and v < 1.
      counter += 1
  assert counter == 5

params = """\
point=5 5 5
point=10 10 10
point=0 0 5
point=1 1 1
point=8 8 8
grid_step=0.1
"""

def exercise_02():
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/mmtbx/map_value_at_point/model.pdb",
    test=os.path.isfile)
  cmd = " ".join([
    "phenix.fmodel",
    "%s"%pdb_file,
    "high_res=1.0",
    "label='2mFo-DFc'",
    "output.file_name='e03.mtz'"
    ">tmp_map_value_at_point.log"
  ])
  assert not easy_run.call(cmd)
  ofn = open("e03.params","w").write(params)
  for label in ["None", '2mFo-DFc']:
    cmd = " ".join([
      "phenix.map_value_at_point",
      "e03.mtz",
      "e03.params",
      "grid_step=0.1",
      "miller_array.labels.name='%s'"%label,
      "> tmp_map_value_at_point.log"
    ])
    assert not easy_run.call(cmd)
    counter = 0
    for line in open("tmp_map_value_at_point.log","r").readlines():
      line = line.strip()
      if(line.startswith("Input point: (5.000,5.000,5.000)")):
        v = float(line.split()[7])
        assert v > 43. and v < 48.
        counter += 1
      if(line.startswith("Input point: (10.000,10.000,10.000)")):
        v = float(line.split()[7])
        assert v > 20. and v < 24.
        counter += 1
      if(line.startswith("Input point: (0.000,0.000,5.000)")):
        v = float(line.split()[7])
        assert v > 17. and v < 22.
        counter += 1
      if(line.startswith("Input point: (1.000,1.000,1.000)")):
        v = float(line.split()[7])
        assert v > 0. and v < 2.
        counter += 1
      if(line.startswith("Input point: (8.000,8.000,8.000)")):
        v = float(line.split()[7])
        assert v > -1. and v < 1.
        counter += 1
    assert counter == 5

def exercise_03():
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/mmtbx/map_value_at_point/model.pdb",
    test=os.path.isfile)
  cmd = " ".join([
    "phenix.fmodel",
    "%s"%pdb_file,
    "high_res=1.0",
    "label='2mFo-DFc'",
    "output.file_name='e10.mtz'"
    ">tmp_map_value_at_point.log"
  ])
  assert not easy_run.call(cmd)
  for label in ["None", '2mFo-DFc']:
    cmd = " ".join([
      "phenix.map_value_at_point",
      "e10.mtz",
      "%s"%pdb_file,
      "grid_step=0.1",
      "miller_array.labels.name='%s'"%label,
      "> tmp_map_value_at_point.log"
    ])
    assert not easy_run.call(cmd)
    result = flex.double()
    for line in open("tmp_map_value_at_point.log","r").readlines():
      line = line.strip()
      if(line.count("Input point: (")):
        result.append(float(line.split()[7]))
    assert result.size() == 4
    assert 45 < result[0] < 46.5
    assert 43 < result[1] < 46.5
    assert 21 < result[2] < 23
    assert 19 < result[3] < 19.5

def exercise_04():
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/mmtbx/map_value_at_point/model.pdb",
    test=os.path.isfile)
  ofn = open("e04.params","w").write(params)
  cmd = " ".join([
    "phenix.fmodel",
    "%s"%pdb_file,
    "high_res=1.0",
    "label='2mFo-DFc'",
    "output.file_name='e04.mtz'"
    ">tmp_map_value_at_point.log"
  ])
  assert not easy_run.call(cmd)
  cmd = "phenix.mtz2map e04.mtz"
  assert not easy_run.call(cmd)
  cmd = " ".join([
    "phenix.map_value_at_point",
    "e04_1.ccp4",
    "e04.params",
    "> tmp_map_value_at_point.log"
  ])
  print(cmd)
  assert not easy_run.call(cmd)
  counter = 0
  for line in open("tmp_map_value_at_point.log","r").readlines():
    line = line.strip()
    if(line.startswith("Input point: (5.000,5.000,5.000)")):
      v = float(line.split()[7])
      assert v > 43. and v < 48.
      counter += 1
    if(line.startswith("Input point: (10.000,10.000,10.000)")):
      v = float(line.split()[7])
      assert v > 20. and v < 24.
      counter += 1
    if(line.startswith("Input point: (0.000,0.000,5.000)")):
      v = float(line.split()[7])
      assert v > 17. and v < 22.
      counter += 1
    if(line.startswith("Input point: (1.000,1.000,1.000)")):
      v = float(line.split()[7])
      assert v > 0. and v < 2.
      counter += 1
    if(line.startswith("Input point: (8.000,8.000,8.000)")):
      v = float(line.split()[7])
      assert v > -1. and v < 1.
      counter += 1
  assert counter == 5

if (__name__ == "__main__"):
  exercise_00()
  exercise_01()
  exercise_02()
  exercise_03()
  exercise_04()
  from libtbx.utils import format_cpu_times
  print(format_cpu_times())
  print("OK")
