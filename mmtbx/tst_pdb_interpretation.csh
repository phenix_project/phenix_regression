#! /bin/csh -f

set PHENIX_REGRESSION_DIR="`libtbx.find_in_repositories phenix_regression`"

mmtbx.python "`libtbx.show_dist_paths mmtbx`"/monomer_library/pdb_interpretation.py "$PHENIX_REGRESSION_DIR"/pdb/pdb139l.ent | grep '^  Number of atoms' | libtbx.assert_stdin "  Number of atoms with unknown scattering type symbols: 8   Number of atoms with unknown nonbonded energy type symbols: 8"

phenix.pdb_interpretation "$PHENIX_REGRESSION_DIR"/rna_puckers_refine/2z75.pdb strict_processing=True
phenix.pdb_interpretation "$PHENIX_REGRESSION_DIR"/rna_puckers_refine/2z75.{pdb,cif} strict_processing=True | libtbx.assert_stdin_contains_strings 'Bond restraints: 4990'

phenix.pdb_interpretation "$PHENIX_REGRESSION_DIR"/pdb/ot1_ot2.pdb | grep 'Unexpected atoms' | libtbx.assert_stdin ""

phenix.pdb_interpretation "$PHENIX_REGRESSION_DIR"/pdb/explicit_break.pdb | egrep ' Classifications: | Chain breaks: ' | libtbx.assert_stdin "          Classifications: {'peptide': 2}           Chain breaks: 1"

phenix.pdb_interpretation "$PHENIX_REGRESSION_DIR/pdb/1yjp_h.pdb" | grep Modifications | sed 's/^ *//g' | libtbx.assert_stdin "Modifications used: {'COO': 1, 'NH3': 1}"

phenix.pdb_interpretation "$PHENIX_REGRESSION_DIR"/pdb/large_bond.pdb | & tail -1 | libtbx.assert_stdin 'Sorry: Number of bonds with excessive lengths: 1'

phenix.pdb_interpretation "$PHENIX_REGRESSION_DIR"/pdb/1ee3_stripped.pdb | grep "Ad-hoc single atom residues: {' CD': 4}" | libtbx.assert_line_count 2

# Phenix does not accept the old naming for RNA residues or atom names
#phenix.pdb_interpretation "$PHENIX_REGRESSION_DIR"/pdb/ccp4_mon_lib_rna_dna.pdb | grep 'Classifications: ' | sed 's/   *//' | libtbx.assert_stdin "Classifications: {'DNA': 6} Classifications: {'RNA': 4}"

echo "Done: $0"
echo ""
