#! /bin/csh -fe

set PHENIX_REGRESSION_DIR="`libtbx.find_in_repositories phenix_regression`"

mmtbx.mon_lib_cif_triage "`libtbx.find_in_repositories chem_data`"/geostd/t/data_TYR.cif

echo "OK"
