from __future__ import print_function
from libtbx import easy_run

pdb="""
CRYST1  716.800  716.800  716.800  90.00  90.00  90.00 P 1
ATOM      2  OD1 ASPAA  43     352.865 385.764 386.065  1.00 79.54           O
ATOM      3  CB  SERAA  44     353.873 382.079 387.224  1.00 87.28           C
ATOM      4  OG  SERAA  44     352.655 382.655 387.660  1.00 87.28           O
HETATM    5  C1  ZMPAA 101     335.920 376.895 380.425  1.00 67.10           C
HETATM    6  C10 ZMPAA 101     340.225 380.547 389.139  1.00 67.10           C
HETATM    7  C11 ZMPAA 101     342.287 382.070 390.153  1.00 67.10           C
HETATM    8  C12 ZMPAA 101     343.418 382.282 391.153  1.00 67.10           C
HETATM    9  C13 ZMPAA 101     345.110 384.053 391.577  1.00 67.10           C
HETATM   10  C14 ZMPAA 101     345.409 383.344 392.905  1.00 67.10           C
HETATM   11  C15 ZMPAA 101     346.817 383.702 393.378  1.00 67.10           C
HETATM   12  C16 ZMPAA 101     348.619 384.039 391.752  1.00 67.10           C
HETATM   13  C17 ZMPAA 101     349.614 383.371 390.794  1.00 67.10           C
HETATM   14  C18 ZMPAA 101     349.017 383.012 389.423  1.00 67.10           C
HETATM   15  C19 ZMPAA 101     348.039 384.081 388.942  1.00 67.10           C
HETATM   16  C2  ZMPAA 101     335.734 378.008 381.457  1.00 67.10           C
HETATM   17  C20 ZMPAA 101     348.324 381.652 389.470  1.00 67.10           C
HETATM   18  C21 ZMPAA 101     350.176 382.914 388.443  1.00 67.10           C
HETATM   19  C22 ZMPAA 101     335.936 377.502 379.024  1.00 67.10           C
HETATM   20  C23 ZMPAA 101     336.023 376.483 377.887  1.00 67.10           C
HETATM   21  C3  ZMPAA 101     335.928 377.436 382.857  1.00 67.10           C
HETATM   22  C4  ZMPAA 101     336.113 378.558 383.870  1.00 67.10           C
HETATM   23  C5  ZMPAA 101     337.140 378.136 384.917  1.00 67.10           C
HETATM   24  C6  ZMPAA 101     337.254 379.265 385.933  1.00 67.10           C
HETATM   25  C7  ZMPAA 101     338.326 378.943 386.969  1.00 67.10           C
HETATM   26  C8  ZMPAA 101     338.040 379.775 388.217  1.00 67.10           C
HETATM   27  C9  ZMPAA 101     339.106 379.514 389.273  1.00 67.10           C
HETATM   28  N1  ZMPAA 101     344.160 383.502 390.802  1.00 67.10           N
HETATM   29  N2  ZMPAA 101     347.806 383.210 392.412  1.00 67.10           N
HETATM   30  O1  ZMPAA 101     340.249 381.347 388.204  1.00 67.10           O
HETATM   31  O2  ZMPAA 101     345.716 385.077 391.271  1.00 67.10           O
HETATM   32  O3  ZMPAA 101     348.626 385.258 391.927  1.00 67.10           O
HETATM   33  O4  ZMPAA 101     350.122 382.177 391.390  1.00 67.10           O
HETATM   34  O5  ZMPAA 101     351.064 384.006 388.686  1.00 67.10           O
HETATM   35  O6  ZMPAA 101     353.304 385.103 388.879  1.00 67.10           O
HETATM   36  O7  ZMPAA 101     352.954 382.809 389.857  1.00 67.10           O
HETATM   37  P1  ZMPAA 101     352.654 383.799 388.628  1.00 67.10           P
HETATM   38  S1  ZMPAA 101     341.434 380.472 390.398  1.00 67.10           S
TER
END
"""

cif="""
# electronic Ligand Builder and Optimisation Workbench (eLBOW)
#   - a module of PHENIX version dev-svn-
#   - file written: Tue Aug  6 14:42:51 2019
#
#   Input file: /Users/pafonine/phenix/modules/chem_data/chemical_components/z/data_ZMP.cif
#   Random seed: 3628800
#
data_comp_list
loop_
_chem_comp.id
_chem_comp.three_letter_code
_chem_comp.name
_chem_comp.group
_chem_comp.number_atoms_all
_chem_comp.number_atoms_nh
_chem_comp.desc_level
ZMP        ZMP 'Unknown                  ' ligand 84 37 .
#
data_comp_ZMP
#
loop_
_chem_comp_atom.comp_id
_chem_comp_atom.atom_id
_chem_comp_atom.type_symbol
_chem_comp_atom.type_energy
_chem_comp_atom.charge
_chem_comp_atom.partial_charge
_chem_comp_atom.x
_chem_comp_atom.y
_chem_comp_atom.z
ZMP         O7     O   OP    -1    .      -0.1239   16.0358    2.4778
ZMP         P1     P   P      0    .      -0.1760   15.9156    3.9820
ZMP         O6     O   O      0    .      -1.5768   16.2105    4.4622
ZMP         O5     O   O2     0    .       0.2461   14.3857    4.4257
ZMP         C21    C   CH2    0    .      -0.3533   13.3099    3.7623
ZMP         C18    C   CT     0    .       0.1726   11.9982    4.3409
ZMP         C19    C   CH3    0    .       1.6995   11.9989    4.2988
ZMP         C20    C   CH3    0    .      -0.2966   11.8553    5.7871
ZMP         C17    C   CH1    0    .      -0.3585   10.8290    3.5159
ZMP         O4     O   OH1    0    .      -1.7561   10.8960    3.4571
ZMP         C16    C   C      0    .       0.0610    9.5141    4.1681
ZMP         O3     O   O      0    .       0.1831    9.4490    5.3449
ZMP         N2     N   NH1    0    .       0.2960    8.3399    3.3482
ZMP         C15    C   CH2    0    .       0.3222    7.0223    3.9577
ZMP         C14    C   CH2    0    .       0.7980    5.9958    2.9315
ZMP         C13    C   C      0    .       0.1469    4.6450    3.2229
ZMP         O2     O   O      0    .      -0.7722    4.5790    3.9673
ZMP         N1     N   NH1    0    .       0.6756    3.4378    2.6135
ZMP         C12    C   CH2    0    .      -0.0534    2.1874    2.7284
ZMP         C11    C   CH2    0    .       0.7723    1.0585    2.1157
ZMP         S1     S   S2     0    .      -0.3168   -0.3373    1.7114
ZMP         C10    C   C      0    .       0.6401   -1.6252    0.8618
ZMP         O1     O   O      0    .       1.6801   -1.3576    0.3617
ZMP         C9     C   CH2    0    .       0.1049   -3.0539    0.7930
ZMP         C8     C   CH2    0    .       0.4702   -3.6698   -0.5556
ZMP         C7     C   CH2    0    .       0.0314   -5.1326   -0.5828
ZMP         C6     C   CH2    0    .       0.2437   -5.7020   -1.9841
ZMP         C5     C   CH2    0    .      -0.1394   -7.1807   -1.9962
ZMP         C4     C   CH2    0    .       0.0004   -7.7314   -3.4145
ZMP         C3     C   CH2    0    .      -0.3255   -9.2239   -3.4156
ZMP         C2     C   CH2    0    .      -0.1876   -9.7746   -4.8347
ZMP         C1     C   CH2    0    .      -0.4792  -11.2744   -4.8295
ZMP         C22    C   CH2    0    .      -0.3781  -11.8182   -6.2544
ZMP         C23    C   CH2    0    .      -0.7345  -13.3051   -6.2589
ZMP         C24    C   CH2    0    .      -0.5488  -13.8693   -7.6671
ZMP         C25    C   CH3    0    .      -1.0422  -15.3152   -7.7057
ZMP         H3     H   HCH2   0    .      -0.1161   13.3595    2.7050
ZMP         H4     H   HCH2   0    .      -1.4293   13.3577    3.8942
ZMP         H5     H   HCH3   0    .       2.0795   12.7751    4.9569
ZMP         H6     H   HCH3   0    .       2.0713   11.0324    4.6264
ZMP         H7     H   HCH3   0    .       2.0336   12.1900    3.2830
ZMP         H8     H   HCH3   0    .      -1.2187   11.2816    5.8135
ZMP         H9     H   HCH3   0    .      -0.4708   12.8403    6.2101
ZMP         H10    H   HCH3   0    .       0.4657   11.3422    6.3660
ZMP         H11    H   HCH1   0    .       0.0479   10.8805    2.5132
ZMP         H12    H   HOH1   0    .      -2.0350   10.8656    2.5556
ZMP         H13    H   HNH1   0    .       0.4529    8.4395    2.3648
ZMP         H14    H   HCH2   0    .      -0.6750    6.7603    4.2956
ZMP         H15    H   HCH2   0    .       1.0009    7.0289    4.8041
ZMP         H16    H   HCH2   0    .       1.8768    5.8977    2.9916
ZMP         H17    H   HCH2   0    .       0.5197    6.3241    1.9358
ZMP         H18    H   HNH1   0    .       1.5333    3.4715    2.0982
ZMP         H19    H   HCH2   0    .      -0.9987    2.2704    2.2027
ZMP         H20    H   HCH2   0    .      -0.2395    1.9722    3.7752
ZMP         H21    H   HCH2   0    .       1.5261    0.7352    2.8260
ZMP         H22    H   HCH2   0    .       1.2558    1.4136    1.2118
ZMP         H23    H   HCH2   0    .       0.5443   -3.6434    1.5907
ZMP         H24    H   HCH2   0    .      -0.9740   -3.0416    0.9046
ZMP         H25    H   HCH2   0    .      -0.0321   -3.1269   -1.3490
ZMP         H26    H   HCH2   0    .       1.5436   -3.6116   -0.7009
ZMP         H27    H   HCH2   0    .       0.6195   -5.6999    0.1311
ZMP         H28    H   HCH2   0    .      -1.0188   -5.1999   -0.3206
ZMP         H29    H   HCH2   0    .      -0.3764   -5.1619   -2.6917
ZMP         H30    H   HCH2   0    .       1.2865   -5.5964   -2.2630
ZMP         H31    H   HCH2   0    .      -1.1664   -7.2908   -1.6652
ZMP         H32    H   HCH2   0    .       0.5166   -7.7297   -1.3288
ZMP         H33    H   HCH2   0    .      -0.6859   -7.2110   -4.0737
ZMP         H34    H   HCH2   0    .       1.0174   -7.5831   -3.7614
ZMP         H35    H   HCH2   0    .      -1.3422   -9.3723   -3.0673
ZMP         H36    H   HCH2   0    .       0.3618   -9.7445   -2.7574
ZMP         H37    H   HCH2   0    .      -0.8926   -9.2714   -5.4880
ZMP         H38    H   HCH2   0    .       0.8221   -9.6038   -5.1923
ZMP         H39    H   HCH2   0    .       0.2420  -11.7807   -4.1973
ZMP         H40    H   HCH2   0    .      -1.4794  -11.4470   -4.4468
ZMP         H41    H   HCH2   0    .       0.6351  -11.6881   -6.6201
ZMP         H42    H   HCH2   0    .      -1.0665  -11.2793   -6.8969
ZMP         H43    H   HCH2   0    .      -1.7676  -13.4301   -5.9526
ZMP         H44    H   HCH2   0    .      -0.0861  -13.8345   -5.5689
ZMP         H45    H   HCH2   0    .       0.5025  -13.8387   -7.9331
ZMP         H46    H   HCH2   0    .      -1.1179  -13.2741   -8.3730
ZMP         H47    H   HCH3   0    .      -0.4623  -15.9144   -7.0095
ZMP         H48    H   HCH3   0    .      -0.9241  -15.7113   -8.7101
ZMP         H49    H   HCH3   0    .      -2.0911  -15.3474   -7.4248
ZMP         O8     O   OP    -1    .       0.7860   16.9023    4.5994
#
loop_
_chem_comp_bond.comp_id
_chem_comp_bond.atom_id_1
_chem_comp_bond.atom_id_2
_chem_comp_bond.type
_chem_comp_bond.value_dist
_chem_comp_bond.value_dist_esd
_chem_comp_bond.value_dist_neutron
ZMP   C25     C24   single        1.528 0.020     1.528
ZMP   O7      P1    deloc         1.510 0.020     1.510
ZMP   C23     C24   single        1.528 0.020     1.528
ZMP   C23     C22   single        1.529 0.020     1.529
ZMP   P1      O6    deloc         1.510 0.020     1.510
ZMP   P1      O5    single        1.648 0.020     1.648
ZMP   C1      C22   single        1.529 0.020     1.529
ZMP   C1      C2    single        1.528 0.020     1.528
ZMP   C20     C18   single        1.527 0.020     1.527
ZMP   C19     C18   single        1.528 0.020     1.528
ZMP   O5      C21   single        1.399 0.020     1.399
ZMP   C21     C18   single        1.527 0.020     1.527
ZMP   C2      C3    single        1.528 0.020     1.528
ZMP   C3      C4    single        1.528 0.020     1.528
ZMP   C18     C17   single        1.526 0.020     1.526
ZMP   C4      C5    single        1.528 0.020     1.528
ZMP   O1      C10   double        1.185 0.020     1.185
ZMP   C9      C8    single        1.527 0.020     1.527
ZMP   C9      C10   single        1.527 0.020     1.527
ZMP   C6      C5    single        1.528 0.020     1.528
ZMP   C6      C7    single        1.527 0.020     1.527
ZMP   C8      C7    single        1.527 0.020     1.527
ZMP   C17     C16   single        1.527 0.020     1.527
ZMP   C17     O4    single        1.400 0.020     1.400
ZMP   C10     S1    single        1.816 0.020     1.816
ZMP   O3      C16   double        1.185 0.020     1.185
ZMP   C16     N2    single        1.451 0.020     1.451
ZMP   C12     N1    single        1.452 0.020     1.452
ZMP   C12     C11   single        1.527 0.020     1.527
ZMP   N1      C13   single        1.452 0.020     1.452
ZMP   C13     O2    double        1.185 0.020     1.185
ZMP   C13     C14   single        1.528 0.020     1.528
ZMP   C14     C15   single        1.527 0.020     1.527
ZMP   N2      C15   single        1.452 0.020     1.452
ZMP   S1      C11   single        1.816 0.020     1.816
ZMP   C21     H3    single        0.970 0.020     1.090
ZMP   C21     H4    single        0.970 0.020     1.090
ZMP   C19     H5    single        0.970 0.020     1.090
ZMP   C19     H6    single        0.970 0.020     1.090
ZMP   C19     H7    single        0.970 0.020     1.090
ZMP   C20     H8    single        0.970 0.020     1.090
ZMP   C20     H9    single        0.970 0.020     1.090
ZMP   C20     H10   single        0.970 0.020     1.090
ZMP   C17     H11   single        0.970 0.020     1.090
ZMP   O4      H12   single        0.850 0.020     0.980
ZMP   N2      H13   single        0.860 0.020     1.020
ZMP   C15     H14   single        0.970 0.020     1.090
ZMP   C15     H15   single        0.970 0.020     1.090
ZMP   C14     H16   single        0.970 0.020     1.090
ZMP   C14     H17   single        0.970 0.020     1.090
ZMP   N1      H18   single        0.860 0.020     1.020
ZMP   C12     H19   single        0.970 0.020     1.090
ZMP   C12     H20   single        0.970 0.020     1.090
ZMP   C11     H21   single        0.970 0.020     1.090
ZMP   C11     H22   single        0.970 0.020     1.090
ZMP   C9      H23   single        0.970 0.020     1.090
ZMP   C9      H24   single        0.970 0.020     1.090
ZMP   C8      H25   single        0.970 0.020     1.090
ZMP   C8      H26   single        0.970 0.020     1.090
ZMP   C7      H27   single        0.970 0.020     1.090
ZMP   C7      H28   single        0.970 0.020     1.090
ZMP   C6      H29   single        0.970 0.020     1.090
ZMP   C6      H30   single        0.970 0.020     1.090
ZMP   C5      H31   single        0.970 0.020     1.090
ZMP   C5      H32   single        0.970 0.020     1.090
ZMP   C4      H33   single        0.970 0.020     1.090
ZMP   C4      H34   single        0.970 0.020     1.090
ZMP   C3      H35   single        0.970 0.020     1.090
ZMP   C3      H36   single        0.970 0.020     1.090
ZMP   C2      H37   single        0.970 0.020     1.090
ZMP   C2      H38   single        0.970 0.020     1.090
ZMP   C1      H39   single        0.970 0.020     1.090
ZMP   C1      H40   single        0.970 0.020     1.090
ZMP   C22     H41   single        0.970 0.020     1.090
ZMP   C22     H42   single        0.970 0.020     1.090
ZMP   C23     H43   single        0.970 0.020     1.090
ZMP   C23     H44   single        0.970 0.020     1.090
ZMP   C24     H45   single        0.970 0.020     1.090
ZMP   C24     H46   single        0.970 0.020     1.090
ZMP   C25     H47   single        0.970 0.020     1.090
ZMP   C25     H48   single        0.970 0.020     1.090
ZMP   C25     H49   single        0.970 0.020     1.090
ZMP   P1      O8    deloc         1.510 0.020     1.510
#
loop_
_chem_comp_angle.comp_id
_chem_comp_angle.atom_id_1
_chem_comp_angle.atom_id_2
_chem_comp_angle.atom_id_3
_chem_comp_angle.value_angle
_chem_comp_angle.value_angle_esd
ZMP   O8      P1      O5          109.47 3.000
ZMP   O8      P1      O6          109.47 3.000
ZMP   O5      P1      O6          109.47 3.000
ZMP   O8      P1      O7          109.47 3.000
ZMP   O5      P1      O7          109.47 3.000
ZMP   O6      P1      O7          109.47 3.000
ZMP   C21     O5      P1          118.46 3.000
ZMP   H4      C21     H3          109.47 3.000
ZMP   H4      C21     C18         109.47 3.000
ZMP   H3      C21     C18         109.47 3.000
ZMP   H4      C21     O5          109.47 3.000
ZMP   H3      C21     O5          109.47 3.000
ZMP   C18     C21     O5          109.47 3.000
ZMP   C17     C18     C20         109.47 3.000
ZMP   C17     C18     C19         109.47 3.000
ZMP   C20     C18     C19         109.47 3.000
ZMP   C17     C18     C21         109.47 3.000
ZMP   C19     C18     C21         109.47 3.000
ZMP   C20     C18     C21         109.47 3.000
ZMP   H7      C19     H6          109.47 3.000
ZMP   H7      C19     H5          109.47 3.000
ZMP   H6      C19     H5          109.47 3.000
ZMP   H7      C19     C18         109.47 3.000
ZMP   H6      C19     C18         109.47 3.000
ZMP   H5      C19     C18         109.47 3.000
ZMP   H10     C20     H9          109.47 3.000
ZMP   H10     C20     H8          109.47 3.000
ZMP   H9      C20     H8          109.47 3.000
ZMP   H10     C20     C18         109.47 3.000
ZMP   H9      C20     C18         109.47 3.000
ZMP   H8      C20     C18         109.47 3.000
ZMP   H11     C17     C16         109.47 3.000
ZMP   H11     C17     O4          109.47 3.000
ZMP   C16     C17     O4          109.47 3.000
ZMP   H11     C17     C18         109.47 3.000
ZMP   O4      C17     C18         109.47 3.000
ZMP   C16     C17     C18         109.47 3.000
ZMP   H12     O4      C17         109.47 3.000
ZMP   N2      C16     O3          120.00 3.000
ZMP   N2      C16     C17         120.00 3.000
ZMP   O3      C16     C17         120.00 3.000
ZMP   H13     N2      C15         120.00 3.000
ZMP   H13     N2      C16         120.00 3.000
ZMP   C15     N2      C16         120.00 3.000
ZMP   H15     C15     H14         109.47 3.000
ZMP   H15     C15     C14         109.47 3.000
ZMP   H14     C15     C14         109.47 3.000
ZMP   H15     C15     N2          109.47 3.000
ZMP   H14     C15     N2          109.47 3.000
ZMP   C14     C15     N2          109.48 3.000
ZMP   H17     C14     H16         109.47 3.000
ZMP   H17     C14     C13         109.47 3.000
ZMP   H16     C14     C13         109.47 3.000
ZMP   H17     C14     C15         109.47 3.000
ZMP   H16     C14     C15         109.47 3.000
ZMP   C13     C14     C15         109.47 3.000
ZMP   N1      C13     O2          119.99 3.000
ZMP   O2      C13     C14         119.99 3.000
ZMP   N1      C13     C14         119.99 3.000
ZMP   H18     N1      C12         120.00 3.000
ZMP   H18     N1      C13         120.00 3.000
ZMP   C12     N1      C13         120.00 3.000
ZMP   H20     C12     H19         109.47 3.000
ZMP   H20     C12     C11         109.47 3.000
ZMP   H19     C12     C11         109.47 3.000
ZMP   H20     C12     N1          109.47 3.000
ZMP   H19     C12     N1          109.47 3.000
ZMP   C11     C12     N1          109.47 3.000
ZMP   H22     C11     H21         109.47 3.000
ZMP   H22     C11     S1          109.47 3.000
ZMP   H21     C11     S1          109.47 3.000
ZMP   H22     C11     C12         109.47 3.000
ZMP   H21     C11     C12         109.47 3.000
ZMP   S1      C11     C12         109.47 3.000
ZMP   C10     S1      C11         109.47 3.000
ZMP   C9      C10     O1          120.00 3.000
ZMP   C9      C10     S1          120.00 3.000
ZMP   O1      C10     S1          120.00 3.000
ZMP   H24     C9      H23         109.47 3.000
ZMP   H24     C9      C8          109.47 3.000
ZMP   H23     C9      C8          109.47 3.000
ZMP   H24     C9      C10         109.47 3.000
ZMP   H23     C9      C10         109.47 3.000
ZMP   C8      C9      C10         109.47 3.000
ZMP   H26     C8      H25         109.47 3.000
ZMP   H26     C8      C7          109.47 3.000
ZMP   H25     C8      C7          109.47 3.000
ZMP   H26     C8      C9          109.47 3.000
ZMP   H25     C8      C9          109.47 3.000
ZMP   C7      C8      C9          109.47 3.000
ZMP   H28     C7      H27         109.47 3.000
ZMP   H28     C7      C6          109.47 3.000
ZMP   H27     C7      C6          109.47 3.000
ZMP   H28     C7      C8          109.47 3.000
ZMP   H27     C7      C8          109.47 3.000
ZMP   C6      C7      C8          109.47 3.000
ZMP   H30     C6      H29         109.47 3.000
ZMP   H30     C6      C5          109.47 3.000
ZMP   H29     C6      C5          109.47 3.000
ZMP   H30     C6      C7          109.47 3.000
ZMP   H29     C6      C7          109.47 3.000
ZMP   C5      C6      C7          109.47 3.000
ZMP   H32     C5      H31         109.47 3.000
ZMP   H32     C5      C4          109.47 3.000
ZMP   H31     C5      C4          109.47 3.000
ZMP   H32     C5      C6          109.47 3.000
ZMP   H31     C5      C6          109.47 3.000
ZMP   C4      C5      C6          109.47 3.000
ZMP   H34     C4      H33         109.47 3.000
ZMP   H34     C4      C3          109.47 3.000
ZMP   H33     C4      C3          109.47 3.000
ZMP   H34     C4      C5          109.47 3.000
ZMP   H33     C4      C5          109.47 3.000
ZMP   C3      C4      C5          109.47 3.000
ZMP   H36     C3      H35         109.47 3.000
ZMP   H36     C3      C2          109.47 3.000
ZMP   H35     C3      C2          109.47 3.000
ZMP   H36     C3      C4          109.47 3.000
ZMP   H35     C3      C4          109.47 3.000
ZMP   C2      C3      C4          109.47 3.000
ZMP   H38     C2      H37         109.47 3.000
ZMP   H38     C2      C1          109.47 3.000
ZMP   H37     C2      C1          109.47 3.000
ZMP   H38     C2      C3          109.47 3.000
ZMP   H37     C2      C3          109.47 3.000
ZMP   C1      C2      C3          109.47 3.000
ZMP   H40     C1      H39         109.47 3.000
ZMP   H40     C1      C22         109.47 3.000
ZMP   H39     C1      C22         109.47 3.000
ZMP   H40     C1      C2          109.47 3.000
ZMP   H39     C1      C2          109.47 3.000
ZMP   C22     C1      C2          109.48 3.000
ZMP   H42     C22     H41         109.47 3.000
ZMP   H42     C22     C23         109.47 3.000
ZMP   H41     C22     C23         109.47 3.000
ZMP   H42     C22     C1          109.47 3.000
ZMP   H41     C22     C1          109.47 3.000
ZMP   C23     C22     C1          109.47 3.000
ZMP   H44     C23     H43         109.47 3.000
ZMP   H44     C23     C24         109.47 3.000
ZMP   H43     C23     C24         109.47 3.000
ZMP   H44     C23     C22         109.47 3.000
ZMP   H43     C23     C22         109.47 3.000
ZMP   C24     C23     C22         109.48 3.000
ZMP   H46     C24     H45         109.47 3.000
ZMP   H46     C24     C25         109.47 3.000
ZMP   H45     C24     C25         109.47 3.000
ZMP   H46     C24     C23         109.47 3.000
ZMP   H45     C24     C23         109.47 3.000
ZMP   C25     C24     C23         109.47 3.000
ZMP   H49     C25     H48         109.47 3.000
ZMP   H49     C25     H47         109.47 3.000
ZMP   H48     C25     H47         109.47 3.000
ZMP   H49     C25     C24         109.47 3.000
ZMP   H48     C25     C24         109.47 3.000
ZMP   H47     C25     C24         109.47 3.000
#
loop_
_chem_comp_tor.comp_id
_chem_comp_tor.id
_chem_comp_tor.atom_id_1
_chem_comp_tor.atom_id_2
_chem_comp_tor.atom_id_3
_chem_comp_tor.atom_id_4
_chem_comp_tor.value_angle
_chem_comp_tor.value_angle_esd
_chem_comp_tor.period
ZMP CONST_01       C15     N2      C16     C17        -162.67   0.0 0
ZMP CONST_02       C15     N2      C16     O3           16.66   0.0 0
ZMP CONST_03       C12     N1      C13     C14        -170.93   0.0 0
ZMP CONST_04       C12     N1      C13     O2           10.61   0.0 0
ZMP CONST_05       H13     N2      C16     C17          18.19   0.0 0
ZMP CONST_06       H18     N1      C13     C14           7.81   0.0 0
ZMP Var_01         C18     C21     O5      P1         -179.47  30.0 3
ZMP Var_02         C20     C18     C21     O5           66.35  30.0 3
ZMP Var_03         O8      P1      O5      C21        -167.94  30.0 2
ZMP Var_04         C14     C15     N2      C16        -171.26  30.0 3
ZMP Var_05         C13     C14     C15     N2         -149.22  30.0 2
ZMP Var_06         N1      C13     C14     C15        -164.67  30.0 1
ZMP Var_07         C11     C12     N1      C13        -174.93  30.0 2
ZMP Var_08         S1      C11     C12     N1         -159.82  30.0 3
ZMP Var_09         O1      C10     S1      C11         -18.82  30.0 1
ZMP Var_10         C8      C9      C10     S1          143.80  30.0 1
ZMP Var_11         C7      C8      C9      C10         175.61  30.0 3
ZMP Var_12         C6      C7      C8      C9          173.30  30.0 3
ZMP Var_13         C5      C6      C7      C8          177.62  30.0 1
ZMP Var_14         C4      C5      C6      C7          176.94  30.0 3
ZMP Var_15         C3      C4      C5      C6          177.62  30.0 1
ZMP Var_16         C2      C3      C4      C5          179.92  30.0 1
ZMP Var_17         C1      C2      C3      C4          178.58  30.0 1
ZMP Var_18         C22     C1      C2      C3          178.50  30.0 1
ZMP Var_19         C23     C22     C1      C2         -177.34  30.0 3
ZMP Var_20         C24     C23     C22     C1         -176.48  30.0 1
ZMP Var_21         C25     C24     C23     C22        -174.15  30.0 3
ZMP Var_22         H8      C20     C18     C21          93.83  30.0 3
ZMP Var_23         H5      C19     C18     C21          65.06  30.0 3
ZMP Var_24         H47     C25     C24     C23         -60.84  30.0 3
#
loop_
_chem_comp_chir.comp_id
_chem_comp_chir.id
_chem_comp_chir.atom_id_centre
_chem_comp_chir.atom_id_1
_chem_comp_chir.atom_id_2
_chem_comp_chir.atom_id_3
_chem_comp_chir.volume_sign
ZMP chir_01   C17     C18     C16     O4    negativ
#
loop_
_chem_comp_plane_atom.comp_id
_chem_comp_plane_atom.plane_id
_chem_comp_plane_atom.atom_id
_chem_comp_plane_atom.dist_esd
ZMP plan-1    C17 0.020
ZMP plan-1    C16 0.020
ZMP plan-1     O3 0.020
ZMP plan-1     N2 0.020
ZMP plan-1    C15 0.020
ZMP plan-1    H13 0.020
ZMP plan-2    C14 0.020
ZMP plan-2    C13 0.020
ZMP plan-2     O2 0.020
ZMP plan-2     N1 0.020
ZMP plan-2    C12 0.020
ZMP plan-2    H18 0.020
ZMP plan-3     S1 0.020
ZMP plan-3    C10 0.020
ZMP plan-3     O1 0.020
ZMP plan-3     C9 0.020
"""

link="""
global_
#
data_link_list
#
data_link_ZMP-SER
#
loop_
_chem_link_bond.link_id
_chem_link_bond.atom_1_comp_id
_chem_link_bond.atom_id_1
_chem_link_bond.atom_2_comp_id
_chem_link_bond.atom_id_2
_chem_link_bond.type
_chem_link_bond.value_dist
_chem_link_bond.value_dist_esd
ZMP-SER  1  P1      2  OG        coval       1.500000    0.020
#
loop_
_chem_link_angle.link_id
_chem_link_angle.atom_1_comp_id
_chem_link_angle.atom_id_1
_chem_link_angle.atom_2_comp_id
_chem_link_angle.atom_id_2
_chem_link_angle.atom_3_comp_id
_chem_link_angle.atom_id_3
_chem_link_angle.value_angle
_chem_link_angle.value_angle_esd
ZMP-SER  2  CB       2  OG     1  P1      120.635000    3.000
#
"""


def exercise_00(prefix="tst_pdb_interpretation"):
  model, restraints, links = "%s.pdb"%prefix, "%s.cif"%prefix, "%s_link.cif"%prefix
  of=open(links,"w")
  print(link, file=of)
  of.close()
  of=open(restraints,"w")
  print(cif, file=of)
  of.close()
  of=open(model,"w")
  print(pdb, file=of)
  of.close()
  cmd = "phenix.pdb_interpretation %s %s %s > %s.zlog"%(
    model, restraints, links, prefix)
  print(cmd)
  assert not easy_run.call(cmd)

if (__name__ == "__main__") :
  exercise_00()
  print("OK")
