from __future__ import print_function
from scitbx.array_family import flex
import os, time
from iotbx import reflection_file_utils
from iotbx import reflection_file_reader
import iotbx.pdb
import libtbx.load_env
from libtbx.test_utils import approx_equal
import mmtbx.f_model
from mmtbx import bulk_solvent
import boost_adaptbx.boost.python as bp
ext = bp.import_ext("mmtbx_f_model_ext")
from mmtbx.bulk_solvent import scaler
from six.moves import cStringIO as StringIO
from cctbx import miller
from cctbx import adptbx
from libtbx.str_utils import format_value
from libtbx import easy_run
from iotbx import extract_xtal_data

data = {
"1k9w" : (0.2552,0.2611,0.4482),
"1ave" : (0.1683,0.3522,0.1965),
"1d56" : (0.2244,0.2354,0.3204),
"1m1s" : (0.2918,0.2745,0.5011),
"1xwf" : (0.2085,0.0935,0.3244),
"1bzs" : (0.1777,0.3462,0.2182),
"1ykv" : (0.2618,0.2008,0.3030),
"3hzq" : (0.2909,0.2770,0.3666),
"3gk8" : (0.2663,0.2713,0.3632),
"2b4l" : (0.2986,0.1751,0.3969),
"1r6j" : (0.0885,0.1305,0.1687),
"1hqw" : (0.1779,0.2052,0.2722),
"3jqh" : (0.1928,0.3010,0.2307),
"1ake" : (0.2069,0.2764,0.3591),
"1bzh" : (0.2093,0.5235,0.2278),
"1t98" : (0.2317,0.3269,0.2982),
"1f6c" : (0.2507,0.4066,0.3410),
"1gbm" : (0.1381,0.1758,0.1899),
"2ji0" : (0.2537,0.1913,0.4140),
"1is7" : (0.2422,0.2147,0.3670),
"1ota" : (0.1250,0.2349,0.1242),
"1yxp" : (0.2630,0.1836,0.4222),
"1ctt" : (0.1854,0.4270,0.2842),
"3hay" : (0.2949,0.4354,0.3408),
"mike" : (0.2875,0.3834,0.3967),
"1jbr" : (0.2285,0.1678,0.3677),
"1kng" : (0.1450,0.2872,0.2365),
"1nm9" : (0.1643,0.1709,0.1712),
"1sv2" : (0.0887,0.0243,0.1072),
"fina" : (0.1837,0.4256,0.2006),
"1kwn" : (0.1212,0.3474,0.1493),
"1ous" : (0.1940,0.2209,0.2393),
"eren" : (0.0509,0.1068,0.0520),
"2bwx" : (0.1880,0.2446,0.2653),
"1h8a" : (0.2372,0.1963,0.4160),
"1z9k" : (0.2594,0.4106,0.3864)
}

def run(use_asserts=True, show=False):
  for pdb_code in data.keys():
    #print "-"*79, pdb_code
    pdb_file = libtbx.env.find_in_repositories(
      relative_path="phenix_regression/mmtbx/bulk_sol_and_scaling_fast/%s"%(pdb_code+".pdb"),
      test=os.path.isfile)
    hkl_file = libtbx.env.find_in_repositories(
      relative_path="phenix_regression/mmtbx/bulk_sol_and_scaling_fast/%s"%(pdb_code+".mtz"),
      test=os.path.isfile)
    if(hkl_file is None):
      hkl_file = libtbx.env.find_in_repositories(
        relative_path="phenix_regression/mmtbx/bulk_sol_and_scaling_fast/%s"%(pdb_code+".cif.gz"),
        test=os.path.isfile)
      assert not easy_run.call("cp %s ."%hkl_file)
      assert not easy_run.call("gunzip -f %s"%os.path.basename(hkl_file))
      hkl_file = pdb_code+".cif"
    pdb_inp = iotbx.pdb.input(file_name = pdb_file)
    xrs = pdb_inp.xray_structure_simple()
    xrs.scattering_type_registry(table = "wk1995")
    xrs.tidy_us() # XXX !!!
    #
    reflection_file = reflection_file_reader.any_reflection_file(
      file_name=hkl_file, ensure_read_access=False)
    rfs = reflection_file_utils.reflection_file_server(
      crystal_symmetry=xrs.crystal_symmetry(),
      force_symmetry=True,
      reflection_files=[reflection_file],
      err=StringIO())
    determine_data_and_flags_result = extract_xtal_data.run(
      reflection_file_server = rfs,
      keep_going             = True)
    f_obs = determine_data_and_flags_result.f_obs
    #
    if(pdb_code=="1hqw"): # Info from file header
      sel = f_obs.data()/f_obs.sigmas()
      print(sel.size(), (sel<1).count(True))
      f_obs = f_obs.select(~(sel<1))
    #
    sfp = mmtbx.f_model.sf_and_grads_accuracy_master_params.extract()
    #sfp.algorithm="direct"
    fmodel = mmtbx.f_model.manager(
      f_obs = f_obs,
      sf_and_grads_accuracy_params = sfp,
      xray_structure = xrs)
    fmodel.remove_outliers()
    f_obs = fmodel.f_obs()
    r_free_flags =  miller.array(miller_set=f_obs,
      data=flex.bool(f_obs.data().size(), False))
    #
    d_spacings = fmodel.f_obs().d_spacings().data()
    bin_selections = fmodel.f_obs().log_binning()
    ts = 0
    for sel in bin_selections:
      ts += sel.count(True)
    assert ts == fmodel.f_obs().d_spacings().data().size(), \
      [ts, fmodel.f_obs().d_spacings().data().size()]
    #
    t0 = time.time()
    aso = scaler.run(
      f_obs              = fmodel.f_obs(),
      f_calc             = fmodel.f_calc(),
      f_mask             = fmodel.f_masks(),
      r_free_flags       = r_free_flags,
      scale_method       = "combo",
      #try_expanal=True,
      #try_poly   =False,
      #try_expmin =False,
      bin_selections     = bin_selections,
      ss                 = fmodel.ss,
      verbose            = show)
    t1 = time.time()
    r_final = aso.r_final
    r_low   = aso.r_low
    r_high  = aso.r_high
    r_target = data[pdb_code]
    rc  = bulk_solvent.r_factor(fmodel.f_obs().data(), aso.core.f_model.data())
    rc1 = bulk_solvent.r_factor(fmodel.f_obs().data(), aso.core.f_model.data(),1)
    if(use_asserts):
      assert approx_equal(r_final, rc)
      assert approx_equal(r_final, rc1)
    fmt="%4s : (%6.4f,%6.4f,%6.4f) rt=%6.4f rtl=%6.4f rth=%6.4f time: %6.3f atoms: %5d refl.: %6d"
    suffix = "    "
    if((r_target[0]<r_final and abs(r_target[0]-r_final)>0.01) or
       (r_target[1]<r_low   and abs(r_target[1]-r_low)  >0.01) or
       (r_target[2]<r_high  and abs(r_target[2]-r_high) >0.01) ): suffix = "<<<<"

    if((r_target[0]>r_final and abs(r_target[0]-r_final)>0.01) or
       (r_target[1]>r_low   and abs(r_target[1]-r_low)  >0.01) or
       (r_target[2]>r_high  and abs(r_target[2]-r_high) >0.01) ): suffix = "****"

    if(use_asserts):
      tmp = pdb_code, r_final, r_low, r_high, r_target[0], r_target[1], r_target[2]
      assert approx_equal(r_final, r_target[0], 0.1), [tmp, f_obs.d_min()]
      assert approx_equal(r_low,   r_target[1], 0.1), [tmp, f_obs.d_min()]
      assert approx_equal(r_high,  r_target[2], 0.1), [tmp, f_obs.d_min()]
    #
    fmodel.update_core(
      k_mask        = aso.core.k_masks,
      k_isotropic   = aso.core.k_isotropic*aso.core.k_isotropic_exp,
      k_anisotropic = aso.core.k_anisotropic)
    if(use_asserts):
      assert approx_equal(fmodel.k_anisotropic(), aso.core.k_anisotropic)
      assert approx_equal(fmodel.k_isotropic(), aso.core.k_isotropic*aso.core.k_isotropic_exp)
      assert approx_equal(fmodel.f_model().data(), aso.core.f_model.data())
    #
    r = aso.apply_back_trace_of_overall_exp_scale_matrix(
      xray_structure = fmodel.xray_structure.deep_copy_scatterers())
    if(r is not None):
      r1 = fmodel.r_work()
      fmodel.update_xray_structure(
        xray_structure = r.xray_structure,
        update_f_calc  = True)
      fmodel.update_core(k_isotropic = r.k_isotropic, k_mask = r.k_mask, k_anisotropic = r.k_anisotropic)
      r2 = fmodel.r_work()
      if(use_asserts):
        assert approx_equal(r1, r2)
    if(r is None): r = False
    else: r = True
    print(fmt%(pdb_code, r_final, r_low, r_high, r_target[0], r_target[1], r_target[2], t1-t0,
      xrs.scatterers().size(), f_obs.data().size()), suffix, \
      "%s %s"%(format_value("%6.3f",aso.k_exp_overall), format_value("%6.3f",aso.b_exp_overall)), \
        fmodel.f_obs().d_min())


if (__name__ == "__main__"):
  run()
  print("OK")
