from __future__ import print_function
import os
import sys

import libtbx.load_env # import dependency
from libtbx import easy_run
import iotbx.cif

def exercise(verbose=False):
  base_dir = libtbx.env.find_in_repositories("phenix_regression/harvesting")
  assert (base_dir is not None)
  args = [
    os.path.join(base_dir, "model.pdb"),
    os.path.join(base_dir, "refine.mtz"),
    "overwrite=true",
    "main.number_of_macro_cycles=1",
    "strategy=None",
    "write_model_cif_file=True",
  ]
  assert not easy_run.call("phenix.refine %s" % " ".join(args))
  if (verbose) :
    print("\n".join(result.stdout_lines))
  assert os.path.exists("model_refine_001.cif")
  args = [
    "model_refine_001.cif",
    os.path.join(base_dir, "sequence.fa"),
    # "unmerged_data=%s" %os.path.join(base_dir, "unmerged.sca"),
    # "data=model_refine_001.mtz",
    # "f_obs_labels=F-obs-filtered,SIGF-obs-filtered"
  ]
  output_filename = "model_refine_001.deposit_000.cif"
  if (os.path.exists(output_filename)):
    os.remove(output_filename)
  assert not easy_run.call(
    "mmtbx.prepare_pdb_deposition %s" % " ".join(args)
    )
  if (verbose) :
    print("\n".join(result.stdout_lines))
  assert os.path.exists(output_filename)
  cif_model = iotbx.cif.reader(file_path=output_filename).model()
  cif_block = list(cif_model.blocks.values())[0]
  expected_loops = (
    "_entity", "_entity_poly", "_entity_poly_seq", "_atom_site",
    "_space_group_symop")
    # "_atom_type", "_refine_ls_restr", "_refine_ls_shell", "_reflns_shell",
  for expected in expected_loops:
    #print expected, (expected in cif_block.loops)
    assert expected in cif_block.loops
  assert len(cif_block["_entity_poly_seq.mon_id"]) == 64
  assert cif_block["_entity_poly.pdbx_seq_one_letter_code"][0].strip() == \
         "VKDGYIVDDVNCTYFCGRNAYCNEECTKLKGESGYCQWASPYGNACYCYKLPDHVRTKGPGRCH"
  assert list(cif_block['_entity_poly.type']) == ['polypeptide(L)']

def run(verbose=False):
  exercise(verbose=verbose)
  print("OK")

if __name__ == '__main__':
  run(verbose=("--verbose" in sys.argv))
