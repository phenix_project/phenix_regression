"""
Duplicate of mmtbx/bulk_solvent_and_scaling/tst2.py with the only difference
that b_cart is not zero.
"""
from __future__ import print_function
import mmtbx.utils
from iotbx import pdb
from cctbx.array_family import flex
import os
from libtbx.test_utils import approx_equal
from libtbx.utils import format_cpu_times
import mmtbx.bulk_solvent.bulk_solvent_and_scaling as bss
import random
import mmtbx.f_model
from cctbx import sgtbx
import libtbx.load_env
from cctbx import adptbx
import boost_adaptbx.boost.python as bp
from mmtbx import bulk_solvent
from cctbx.development import random_structure
import sys

ext = bp.import_ext("mmtbx_f_model_ext")

random.seed(0)
flex.set_random_seed(0)

def get_xrs(file_name):
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/%s"%file_name, test=os.path.isfile)
  xray_structure = pdb.input(file_name=pdb_file).xray_structure_simple()
  return xray_structure

def get_sf(k_sol, b_sol, xrs, b_cart, miller_set=None, d_min=None,
           twin_law=None, sfg_params=None):
  random.seed(0)
  flex.set_random_seed(0)
  if(miller_set is None):
    assert d_min is not None
    f_dummy = abs(xrs.structure_factors(d_min = d_min,
      anomalous_flag = False).f_calc())
  else:
    f_dummy = miller_set
    assert d_min is None
  r_free_flags = f_dummy.generate_r_free_flags(fraction = 0.1)
  fmodel = mmtbx.f_model.manager(
    r_free_flags   = r_free_flags,
    f_obs          = f_dummy,
    sf_and_grads_accuracy_params = sfg_params,
    xray_structure = xrs,
    twin_law       = twin_law)
  ss = 1./flex.pow2(r_free_flags.d_spacings().data()) / 4.
  k_mask = mmtbx.f_model.ext.k_mask(ss, k_sol, b_sol)
  u_star = adptbx.u_cart_as_u_star(xrs.unit_cell(), adptbx.b_as_u(b_cart))
  k_anisotropic=mmtbx.f_model.ext.k_anisotropic(r_free_flags.indices(),u_star)
  fmodel.update_xray_structure(
    xray_structure = xrs,
    update_f_calc  = True,
    update_f_mask  = True)
  fmodel.update_core(
    k_mask        = k_mask,
    k_anisotropic = k_anisotropic)
  f_obs = abs(fmodel.f_model())
  return f_obs, r_free_flags

def exercise_00(
      d_min = 3,
      twin_law = "h,-k,-l",
      twin_fraction=0.0,
      k_sol = 0.38,
      b_sol = 53.0,
      b_cart = [2.78, 2.78, -8.55, 0, 0, 0]):
  """
  Twinning case. Recover k_sol, b_sol and b_cart when twin_fraction=0.
  """
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/mmtbx/bulk_solvent_and_scaling/pdb1l2h.ent",
    test=os.path.isfile)
  xrs = pdb.input(file_name=pdb_file).xray_structure_simple()
  f_obs1, r_free_flags = get_sf(
    d_min   = d_min,
    k_sol   = k_sol,
    b_sol   = b_sol,
    b_cart  = b_cart,
    twin_law=twin_law,
    xrs     = xrs)
  #
  twin_law_xyz = sgtbx.rt_mx(symbol=twin_law, r_den=12, t_den=144)
  twin_law_matrix = twin_law_xyz.as_double_array()[0:9]
  twin_mi = mmtbx.utils.create_twin_mate(
    miller_indices  = f_obs1.indices(),
    twin_law_matrix = twin_law_matrix)
  twin_set = f_obs1.customized_copy(
    indices = twin_mi,
    crystal_symmetry = f_obs1.crystal_symmetry())
  f_obs2, dum = get_sf(
    k_sol  = k_sol,
    b_sol  = b_sol,
    b_cart = b_cart,
    twin_law=twin_law,
    xrs    = xrs,
    miller_set = twin_set)
  data = flex.sqrt(f_obs1.data()*f_obs1.data()*(1.-twin_fraction) + \
                   f_obs2.data()*f_obs2.data()*twin_fraction)
  #
  f_obs_twin = f_obs1.array(data = data)
  fmodel = mmtbx.f_model.manager(
    r_free_flags   = r_free_flags,
    f_obs          = f_obs_twin,
    twin_law       = twin_law,
    xray_structure = xrs)
  assert fmodel.r_work() > 0.35
  o = fmodel.update_all_scales()
  assert approx_equal(o.k_sol, k_sol)
  assert approx_equal(o.b_sol, b_sol, 1)
  assert approx_equal(fmodel.twin_fraction, twin_fraction)
  assert approx_equal(fmodel.r_work(), 0, 0.0006)

def exercise_01(
      d_min = 3,
      twin_law = "h,-k,-l",
      twin_fraction=0.37,
      k_sol = 0.38,
      b_sol = 53.0,
      b_cart = [2.78, 2.78, -8.55, 0, 0, 0]):
  """
  Twinning case. Recover k_sol, b_sol and b_cart when twin_fraction != 0.
  """
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/mmtbx/bulk_solvent_and_scaling/pdb1l2h.ent",
    test=os.path.isfile)
  xrs = pdb.input(file_name=pdb_file).xray_structure_simple()
  f_obs1, r_free_flags = get_sf(
    d_min   = d_min,
    k_sol   = k_sol,
    b_sol   = b_sol,
    b_cart  = b_cart,
    twin_law=twin_law,
    xrs     = xrs)
  #
  twin_law_xyz = sgtbx.rt_mx(symbol=twin_law, r_den=12, t_den=144)
  twin_law_matrix = twin_law_xyz.as_double_array()[0:9]
  twin_mi = mmtbx.utils.create_twin_mate(
    miller_indices  = f_obs1.indices(),
    twin_law_matrix = twin_law_matrix)
  twin_set = f_obs1.customized_copy(
    indices = twin_mi,
    crystal_symmetry = f_obs1.crystal_symmetry())
  f_obs2, dum = get_sf(
    k_sol  = k_sol,
    b_sol  = b_sol,
    b_cart = b_cart,
    twin_law=twin_law,
    xrs    = xrs,
    miller_set = twin_set)
  data = flex.sqrt(f_obs1.data()*f_obs1.data()*(1.-twin_fraction) + \
                   f_obs2.data()*f_obs2.data()*twin_fraction)
  #
  f_obs_twin = f_obs1.array(data = data)
  fmodel = mmtbx.f_model.manager(
    r_free_flags   = r_free_flags,
    f_obs          = f_obs_twin,
    twin_law       = twin_law,
    xray_structure = xrs)
  for fast in [True, False]:
    fmodel_ = fmodel.deep_copy()
    assert fmodel_.r_work() > 0.39
    o = fmodel_.update_all_scales(fast=fast, update_f_part1=False)
    assert approx_equal(o.k_sol, k_sol)
    assert approx_equal(o.b_sol, b_sol, 1)
    assert approx_equal(fmodel_.twin_fraction, twin_fraction)
    assert approx_equal(fmodel_.r_work(), 0, 0.0006)
  #
  ss = 1./flex.pow2(r_free_flags.d_spacings().data()) / 4.
  k_mask = mmtbx.f_model.ext.k_mask(ss, k_sol, b_sol)
  u_star = adptbx.u_cart_as_u_star(xrs.unit_cell(), adptbx.b_as_u(b_cart))
  k_anisotropic = mmtbx.f_model.ext.k_anisotropic(r_free_flags.indices(),u_star)
  fmodel = mmtbx.f_model.manager(
    r_free_flags   = r_free_flags,
    f_obs          = f_obs_twin,
    k_mask         = k_mask,
    k_anisotropic  = k_anisotropic,
    twin_fraction  = twin_fraction,
    twin_law       = twin_law,
    xray_structure = xrs)
  assert approx_equal(fmodel.r_work(),0)
  assert approx_equal(fmodel.k_masks()[0], k_mask)
  assert approx_equal(fmodel.k_anisotropic(), k_anisotropic)
  assert approx_equal(fmodel.twin_fraction, twin_fraction)
  assert fmodel.twin_law == "h,-k,-l"

def exercise_02(
      d_min = 3.0,
      twin_law = "h,-k,-l",
      twin_fraction=0.35,
      k_sol  = 0.47,
      b_sol  = 63.0,
      b_cart = [2.78, 2.78, -8.55, 0, 0, 0]):
  """
  Twinning case. Random structure, Recover k_sol, b_sol and b_cart when
  twin_fraction != 0.
  Various scattering tables.
  """
  for sct in ["wk1995","neutron","electron"]:
    random.seed(0)
    flex.set_random_seed(0)
    xrs = random_structure.xray_structure(
      space_group_info       = sgtbx.space_group_info(symbol="P 43"),
      elements               =(("O","N","C")*100),
      volume_per_atom        = 200,
      min_distance           = 1.5,
      general_positions_only = True,
      random_u_iso           = True,
      random_occupancy       = False)
    xrs.scattering_type_registry(table=sct)
    f_obs1, r_free_flags = get_sf(
      d_min  = d_min,
      k_sol  = k_sol,
      b_sol  = b_sol,
      b_cart = b_cart,
      twin_law=twin_law,
      xrs    = xrs)
    #
    twin_law_xyz = sgtbx.rt_mx(symbol=twin_law, r_den=12, t_den=144)
    twin_law_matrix = twin_law_xyz.as_double_array()[0:9]
    twin_mi = mmtbx.utils.create_twin_mate(
      miller_indices  = f_obs1.indices(),
      twin_law_matrix = twin_law_matrix)
    twin_set = f_obs1.customized_copy(
      indices = twin_mi,
      crystal_symmetry = f_obs1.crystal_symmetry())
    f_obs2, dum = get_sf(
      k_sol  = k_sol,
      b_sol  = b_sol,
      b_cart = b_cart,
      twin_law=twin_law,
      xrs    = xrs,
      miller_set = twin_set)
    data = flex.sqrt(f_obs1.data()*f_obs1.data()*(1.-twin_fraction) + \
                     f_obs2.data()*f_obs2.data()*twin_fraction)
    f_obs_twin = f_obs1.array(data = data)
    #
    fmodel = mmtbx.f_model.manager(
      f_obs          = f_obs_twin,
      r_free_flags   = r_free_flags,
      twin_law       = twin_law,
      xray_structure = xrs)
    assert fmodel.r_work() > 0.3
    o = fmodel.update_all_scales()
    assert fmodel.r_work() < 0.0008
    assert approx_equal(o.k_sol, k_sol)
    assert approx_equal(o.b_sol, b_sol)
    assert approx_equal(fmodel.twin_fraction, twin_fraction)
    assert approx_equal(fmodel.r_work(), 0, 0.001)

def exercise_03(
      d_min = 3.0,
      twin_law = "h,-k,-l",
      twin_fraction=0.37,
      k_sol = 0.38,
      b_sol = 53.0,
      b_cart = [2.78, 2.78, -8.55, 0, 0, 0]
      ):
  """
  Twinning case. Recover k_sol, b_sol and b_cart when twin_fraction != 0.
  Random structure with H.
  """
  random.seed(0)
  flex.set_random_seed(0)
  xrs = random_structure.xray_structure(
    space_group_info       = sgtbx.space_group_info(symbol="P 43"),
    elements               =(("O","N","C","H","H")*10),
    volume_per_atom        = 200,
    min_distance           = 1.5,
    general_positions_only = True,
    random_u_iso           = True,
    random_occupancy       = False)
  xrs.scattering_type_registry(table="wk1995")
  xrs.shift_us(b_shift=10)
  hd_sel = xrs.hd_selection()
  b_isos = xrs.select(~hd_sel).extract_u_iso_or_u_equiv()*adptbx.u_as_b(1.)
  mmm = b_isos.min_max_mean().as_tuple()
  b_h = int(mmm[2])
  xrs.scattering_type_registry(table="wk1995")
  sfg_params = mmtbx.f_model.sf_and_grads_accuracy_master_params.extract()
  sfg_params.algorithm = "direct"
  xrs.set_occupancies(value=0.74, selection = xrs.hd_selection())
  xrs = xrs.set_b_iso(value=b_h, selection = xrs.hd_selection())
  #
  f_obs1, r_free_flags = get_sf(
    d_min   = d_min,
    k_sol   = k_sol,
    b_sol   = b_sol,
    b_cart  = b_cart,
    twin_law=twin_law,
    sfg_params = sfg_params,
    xrs     = xrs)
  #
  twin_law_xyz = sgtbx.rt_mx(symbol=twin_law, r_den=12, t_den=144)
  twin_law_matrix = twin_law_xyz.as_double_array()[0:9]
  twin_mi = mmtbx.utils.create_twin_mate(
    miller_indices  = f_obs1.indices(),
    twin_law_matrix = twin_law_matrix)
  twin_set = f_obs1.customized_copy(
    indices = twin_mi,
    crystal_symmetry = f_obs1.crystal_symmetry())
  f_obs2, dum = get_sf(
    k_sol  = k_sol,
    b_sol  = b_sol,
    b_cart = b_cart,
    twin_law=twin_law,
    sfg_params = sfg_params,
    xrs    = xrs,
    miller_set = twin_set)
  data = flex.sqrt(f_obs1.data()*f_obs1.data()*(1.-twin_fraction) + \
                   f_obs2.data()*f_obs2.data()*twin_fraction)
  #
  f_obs_twin = f_obs1.array(data = data)
  xrs.set_occupancies(value=0.0, selection = xrs.hd_selection())
  fmodel = mmtbx.f_model.manager(
    r_free_flags   = r_free_flags,
    f_obs          = f_obs_twin,
    twin_law       = twin_law,
    sf_and_grads_accuracy_params = sfg_params,
    xray_structure = xrs)
  #
  assert fmodel.r_all() > 0.4
  o = fmodel.update_all_scales()
  assert approx_equal(o.k_sol, k_sol)
  assert approx_equal(o.b_sol, b_sol)
  assert approx_equal(o.k_h, 0.74, 0.02)
  assert approx_equal(o.b_h, b_h, 1.)
  assert approx_equal(fmodel.twin_fraction, twin_fraction)
  assert approx_equal(fmodel.r_work(), 0, 0.003)

def exercise_04(
      d_min = 1.5,
      k_sol  = 0.47,
      b_sol  = 63.0,
      b_cart = [3, 4, 8, 0, 0, 0]
      ):
  """
  Recover k_sol, b_sol and b_cart.
  Random model. H are present.
  """
  random.seed(0)
  flex.set_random_seed(0)
  xrs = random_structure.xray_structure(
    space_group_info       = sgtbx.space_group_info(symbol="P212121"),
    elements               =(("O","N","C","H")*10),
    volume_per_atom        = 200,
    min_distance           = 1.5,
    general_positions_only = True,
    random_u_iso           = True,
    random_occupancy       = False)
  xrs.shift_us(b_shift=10)
  hd_sel = xrs.hd_selection()
  b_isos = xrs.select(~hd_sel).extract_u_iso_or_u_equiv()*adptbx.u_as_b(1.)
  mmm = b_isos.min_max_mean().as_tuple()
  b_mean = int(mmm[2])
  xrs.scattering_type_registry(table="wk1995")
  sfg_params = mmtbx.f_model.sf_and_grads_accuracy_master_params.extract()
  sfg_params.algorithm = "direct"
  xrs.set_occupancies(value=0.7, selection = xrs.hd_selection())
  xrs = xrs.set_b_iso(value=b_mean, selection = xrs.hd_selection())
  f_obs, r_free_flags = get_sf(
    d_min      = d_min,
    k_sol      = k_sol,
    b_sol      = b_sol,
    b_cart     = b_cart,
    sfg_params = sfg_params,
    xrs        = xrs)
  #
  xrs.set_occupancies(value=0.0, selection = xrs.hd_selection())
  fmodel = mmtbx.f_model.manager(
    f_obs                        = f_obs,
    r_free_flags                 = r_free_flags,
    sf_and_grads_accuracy_params = sfg_params,
    xray_structure               = xrs)
  # TEST 1
  print("1")
  fmodel_ = fmodel.deep_copy()
  r = fmodel_.update_all_scales(fast=False, apply_back_trace=False,
    log=sys.stdout)
  assert approx_equal(r.k_sol[0], k_sol, 0.01)
  assert approx_equal(r.b_sol[0], b_sol, 1.0)
  assert approx_equal(r.b_cart, b_cart, 0.15)
  assert approx_equal(r.k_h, 0.7)
  assert approx_equal(r.b_h, b_mean)
  assert r.b_adj is None
  assert fmodel_.r_all() < 0.0025
  assert b_mean == 19
  # TEST 2
  print("2")
  fmodel_ = fmodel.deep_copy()
  r = fmodel_.update_all_scales(fast=False, apply_back_trace=True,
    log=sys.stdout)
  assert approx_equal(r.k_sol[0], k_sol, 0.01)
  assert approx_equal(r.b_sol[0], b_sol+5, 1.0)
  assert approx_equal(r.b_cart, list(flex.double(b_cart)-
    flex.double([5,5,5,0,0,0])), 0.1)
  assert approx_equal(r.k_h, 0.7)
  assert approx_equal(r.b_h, b_mean+5)
  assert approx_equal(r.b_adj, 5., 0.1)
  assert fmodel_.r_all() < 0.0025, fmodel_.r_all()
  assert b_mean == 19
  # TEST 3
  print("3")
  fmodel_ = fmodel.deep_copy()
  r = fmodel_.update_all_scales(fast=True, apply_back_trace=False,
    log=sys.stdout)
  assert r.k_sol  is None
  assert r.b_sol  is None
  assert r.b_cart is None
  assert approx_equal(r.k_h, 0.7, 0.1)
  assert approx_equal(r.b_h, b_mean, 5)
  assert r.b_adj is None
  assert fmodel_.r_all() < 0.028, fmodel_.r_all()
  assert b_mean == 19
  # TEST 4
  print("4")
  fmodel_ = fmodel.deep_copy()
  r = fmodel_.update_all_scales(fast=True, apply_back_trace=True,
    log=sys.stdout)
  assert r.k_sol  is None
  assert r.b_sol  is None
  assert r.b_cart is None
  assert approx_equal(r.k_h, 0.7, 0.1)
  assert approx_equal(r.b_h, b_mean, 5)
  assert approx_equal(r.b_adj, 5., 2.)
  assert fmodel_.r_all() < 0.028, fmodel_.r_all()
  assert b_mean == 19

def run():
  exercise_00()
  exercise_01()
  exercise_02()
  exercise_03()
  exercise_04()
  print("OK: ",format_cpu_times())

if (__name__ == "__main__"):
  run()
