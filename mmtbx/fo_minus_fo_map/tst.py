from __future__ import division, print_function
import libtbx.load_env
import os, time
import iotbx.pdb
from libtbx import easy_run
from iotbx import reflection_file_reader

ddir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="mmtbx/fo_minus_fo_map",
  test=os.path.isdir)

def exercise_00():
  pdb_file = ddir+"/1akg_TYR_removed.pdb"
  pdb_file_ref = ddir+"/1akg.pdb"
  log = "zlog_fo_minus_fo_map"
  cmd1 = " ".join([
    "phenix.fobs_minus_fobs_map",
    "%s"%pdb_file,
    "f_obs_1_file=%s/%s"%(ddir,"1akg_1.mtz"),
    "f_obs_2_file_name=%s/%s"%(ddir,"1akg_TYR_removed.hkl"),
    "find_peaks_holes=True",
    "--silent",
    "> %s"%log])
  cmd2 = " ".join([
    "phenix.fobs_minus_fobs_map",
    "high_res=1.7",
    "low_resolution=6.5",
    "sigma_cutoff=1",
    "%s"%pdb_file,
    "f_obs_2_file=%s/%s"%(ddir,"1akg_2.hkl"),
    "f_obs_1_file_name=%s/%s"%(ddir,"1akg_TYR_removed.mtz"),
    "--silent",
    "> %s"%log])
  cmd3 = " ".join([
    "phenix.fobs_minus_fobs_map",
    "f_obs_1_label=U12",
    "f_obs_2_label=F-obs",
    "%s"%pdb_file,
    "f_obs_1_file=%s/%s"%(ddir,"1akg_1.hkl"),
    "f_obs_2_file_name=%s/%s"%(ddir,"1akg_TYR_removed.mtz"),
    "--silent",
    "> %s"%log])
  cmd4 = " ".join([
    "phenix.fobs_minus_fobs_map",
    "f_obs_1_label=XXX",
    "f_obs_2_label=F-obs",
    "%s"%pdb_file,
    "f_obs_1_file=%s/%s"%(ddir,"1akg_1.hkl"),
    "f_obs_2_file_name=%s/%s"%(ddir,"1akg_TYR_removed.mtz"),
    "--silent",
    "> %s"%log])
  #
  for cmd in [cmd1, cmd2, cmd3, cmd4]:
    print(cmd)
    print()
    assert not easy_run.call(cmd)
    j_miller_arrays = reflection_file_reader.any_reflection_file(file_name =
      "FoFoPHFc.mtz").as_miller_arrays()
    counter = 0
    for ma in j_miller_arrays:
      l = ma.info().labels[0]
      if(l == "FoFo"):
        fft_map = ma.fft_map()
        fft_map.apply_sigma_scaling()
        map_data = fft_map.real_map_unpadded()
        sel = []
        pdb_inp = iotbx.pdb.input(file_name = pdb_file_ref)
        atoms = pdb_inp.construct_hierarchy().atoms_with_labels()
        uc = iotbx.pdb.input(file_name = pdb_file_ref).xray_structure_simple().unit_cell()
        for a in atoms:
          edv = abs(map_data.eight_point_interpolation(uc.fractionalize(a.xyz)))
          if(a.resname=="TYR"):
            counter += 1
            assert edv > 5, edv
          else:
            assert edv < 3, edv
    assert counter == 12
    assert os.path.getsize(log) == 0
    easy_run.call("rm -rf %s %s"%(log,"FoFoPHFc.mtz"))

def exercise_01():
  pdb_file = ddir+"/coords.pdb"
  d1 = ddir+"/peak.mtz"
  d2 = ddir+"/peak.sca"
  d3 = ddir+"/native.mtz"
  d4 = ddir+"/native.sca"
  cmd1 = "phenix.fobs_minus_fobs_map %s %s phase_source=%s --silent"%(d1,d3,pdb_file)
  cmd2 = "phenix.fobs_minus_fobs_map %s %s %s --silent"%(d2,d4,pdb_file)
  for cmd in [cmd1, cmd2]:
    print(cmd)
    print()
    assert not easy_run.call(cmd)

def exercise_02():
  pdb_file = ddir+"/rpai.pdb"
  d1 = ddir+"/rpai.mtz"
  d2 = ddir+"/rpai.sca"
  cmd1 = "phenix.fobs_minus_fobs_map %s %s %s --silent > tmp_fo_minus_fo.log"%(d1,d2,pdb_file)
  cmd2 = "phenix.fobs_minus_fobs_map f_obs_1_file=%s f_obs_2_file=%s phase_source=%s --silent > tmp_fo_minus_fo.log"%(d1,d2,pdb_file)
  cmd3 = "phenix.fobs_minus_fobs_map f_obs_1_file=%s f_obs_2_file=%s %s --silent > tmp_fo_minus_fo.log"%(d1,d2,pdb_file)
  cmd4 = "phenix.fobs_minus_fobs_map %s f_obs_2_file=%s %s --silent > tmp_fo_minus_fo.log"%(d1,d2,pdb_file)
  sorry_strings = []
  for cmd in [cmd1, cmd2, cmd3, cmd4]:
    print(cmd)
    print()
    result = easy_run.fully_buffered(command=cmd).stderr_lines
    assert result[0]=='Sorry: Crystal symmetry mismatch between different files.', result

def exercise_03 () :
  pdb_file = ddir+"/1akg.pdb"
  cmd1 = " ".join([
    "phenix.fobs_minus_fobs_map",
    "%s"%pdb_file,
    "f_obs_1_file=%s/%s"%(ddir,"1akg_1.mtz"),
    "f_obs_2_file_name=%s/%s"%(ddir,"1akg_TYR_removed_non_isomorphous.mtz"),
    "find_peaks_holes=True",
    "--silent",
    "> tmp_fo_minus_fo1.log"])
  print(cmd1)
  print()
  result = easy_run.fully_buffered(command=cmd1).stderr_lines
  result[0]=='Sorry: Crystal symmetry mismatch between different files.'
  cmd2 = " ".join([
    "phenix.fobs_minus_fobs_map",
    "%s"%pdb_file,
    "f_obs_1_file=%s/%s"%(ddir,"1akg_1.mtz"),
    "f_obs_2_file_name=%s/%s"%(ddir,"1akg_TYR_removed_non_isomorphous.mtz"),
    "ignore_non_isomorphous_unit_cells=True",
    "--silent",
    "> tmp_fo_minus_fo1.log"])
  print(cmd2)
  print()
  result = easy_run.fully_buffered(command=cmd2).stderr_lines
  assert (result[0].startswith("Sorry: The file parameters "))
  cmd3 = " ".join([
    "phenix.fobs_minus_fobs_map",
    "phase_source=%s"%pdb_file,
    "f_obs_1_file=%s/%s"%(ddir,"1akg_1.mtz"),
    "f_obs_2_file_name=%s/%s"%(ddir,"1akg_TYR_removed_non_isomorphous.mtz"),
    "ignore_non_isomorphous_unit_cells=True",
    "output_file=tmp_fo_minus_fo.mtz",
    "--silent",
    "> tmp_fo_minus_fo1.log"])
  if (os.path.exists("tmp_fo_minus_fo.mtz")) :
    os.remove("tmp_fo_minus_fo.mtz")
  print(cmd3)
  print()
  result = easy_run.fully_buffered(command=cmd3).stderr_lines
  assert (len(result) == 0)
  assert os.path.isfile("tmp_fo_minus_fo.mtz")
  from iotbx import crystal_symmetry_from_any
  symm = crystal_symmetry_from_any.extract_from("tmp_fo_minus_fo.mtz")
  assert (str(symm.unit_cell()) == "(14.6, 26.1, 29.2, 90, 90, 90)")

def run():
  exercise_00()
  exercise_01()
  exercise_02()
  exercise_03()

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK", "time: %.2f"%(time.time()-t0))
