from __future__ import division
from __future__ import print_function
import iotbx.pdb
import time
import sys, os
from iotbx import reflection_file_reader
from libtbx.utils import null_out
import mmtbx.utils
from iotbx import reflection_file_utils
import mmtbx.f_model
from libtbx.test_utils import approx_equal
import libtbx.load_env

def reflection_file_server(crystal_symmetry, reflection_files):
  return reflection_file_utils.reflection_file_server(
    crystal_symmetry=crystal_symmetry,
    force_symmetry=True,
    reflection_files=reflection_files,
    err=null_out())

def exercise(args):
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/mmtbx/update_f_part1/model.pdb",
    test=os.path.isfile)
  hkl_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/mmtbx/update_f_part1/data.mtz",
    test=os.path.isfile)
  #
  pdb_inp = iotbx.pdb.input(file_name=pdb_file)
  xrs = pdb_inp.xray_structure_simple()
  #
  miller_arrays = reflection_file_reader.any_reflection_file(file_name =
    hkl_file).as_miller_arrays()
  for ma in miller_arrays:
    l = ma.info().labels
    if(l == ['F-obs', 'SIGF-obs']): f_obs = ma
    if(l == ['R-free-flags']): r_free_flags = ma
  f_obs, r_free_flags = f_obs.common_sets(r_free_flags)
  assert f_obs.indices().all_eq(r_free_flags.indices())
  r_free_flags = r_free_flags.customized_copy(
    data = r_free_flags.data()==0)
  #
  fmodel = mmtbx.f_model.manager(
    f_obs          = f_obs,
    r_free_flags   = r_free_flags,
    xray_structure = xrs)
  fmodel.update_all_scales(remove_outliers = False, update_f_part1=True)
  #
  fmodel.update_all_scales(remove_outliers = False, update_f_part1=False)
  rwn, rfn = fmodel.r_work(), fmodel.r_free()
  print("n:", rwn, rfn)
  #
  fmodel.update_all_scales(remove_outliers = False, update_f_part1=True)
  rwr, rfr = fmodel.r_work(), fmodel.r_free()
  print("r:", rwr, rfr)
  #
  fmodel.update_all_scales(remove_outliers = False, update_f_part1=True)
  rwm, rfm = fmodel.r_work(), fmodel.r_free()
  print("m:", rwm, rfm)
  #
  fmodel.update_all_scales(remove_outliers = False, update_f_part1=True)
  rwr_, rfr_ = fmodel.r_work(), fmodel.r_free()
  print("r:", rwr_, rfr_)
  assert approx_equal(rwr, rwr_)
  assert approx_equal(rfr, rfr_)
  #
  fmodel.update_all_scales(remove_outliers = False, update_f_part1=False)
  rwn_, rfn_ = fmodel.r_work(), fmodel.r_free()
  print("n:", rwn_, rfn_)
  assert approx_equal(rwn, rwn_)
  assert approx_equal(rfn, rfn_)
  #
  fmodel.update_all_scales(remove_outliers = False, update_f_part1=True)
  rwm_, rfm_ = fmodel.r_work(), fmodel.r_free()
  print("m:", rwm_, rfm_)
  assert approx_equal(rwm, rwm_)
  assert approx_equal(rfm, rfm_)

if (__name__ == "__main__"):
  t0 = time.time()
  exercise(sys.argv[1:])
  print("Total time: %-8.4f"%(time.time()-t0))
  print("OK")
