from __future__ import print_function
from libtbx import easy_run
import libtbx.load_env
import os
from scitbx.array_family import flex
from libtbx.test_utils import approx_equal

def exercise_00():
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/mmtbx/reciprocal_space_arrays/model.pdb",
    test=os.path.isfile)
  hkl_1 = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/mmtbx/reciprocal_space_arrays/data_1.mtz",
    test=os.path.isfile)
  hkl_2 = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/mmtbx/reciprocal_space_arrays/data_2.mtz",
    test=os.path.isfile)
  hkl_3 = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/mmtbx/reciprocal_space_arrays/fobs1.cv",
    test=os.path.isfile)
  hkl_4 = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/mmtbx/reciprocal_space_arrays/hl.hkl",
    test=os.path.isfile)
  #
  for hkl in [hkl_1, hkl_2, "%s %s"%(hkl_3, hkl_4)]:
    cmd = " ".join([
      "phenix.reciprocal_space_arrays",
      "%s"%pdb_file,
      "%s"%hkl,
      ">tmp_arrays.log"
    ])
    assert not easy_run.call(cmd)
  #
  cmd = " ".join([
    "phenix.reciprocal_space_arrays",
    "%s"%pdb_file,
    "%s"%hkl,
    "remove_f_obs_outliers=False",
    "bulk_solvent_and_scaling=False",
    "output_file_name='reciprocal_space_arrays_test.mtz'"
    ">tmp_arrays.log"
  ])
  assert not easy_run.call(cmd)
  #
  for fn in ["model_data_1.mtz", "model_data_2.mtz", "model_fobs1.mtz",
             "reciprocal_space_arrays_test.mtz"]:
    assert os.path.isfile(fn)
    assert not easy_run.call("phenix.mtz.dump %s >tmp_arrays.log"%fn)


if (__name__ == "__main__"):
  exercise_00()
  from libtbx.utils import format_cpu_times
  print(format_cpu_times())
