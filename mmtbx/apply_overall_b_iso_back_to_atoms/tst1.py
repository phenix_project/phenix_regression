from __future__ import print_function
from libtbx.test_utils import approx_equal
import mmtbx.f_model
import random, time, os
from scitbx.array_family import flex
from mmtbx import bulk_solvent
from cctbx import adptbx
from cctbx import sgtbx
from cctbx.development import random_structure
import boost_adaptbx.boost.python as bp
ext = bp.import_ext("mmtbx_f_model_ext")
from mmtbx.bulk_solvent import scaler
import libtbx.load_env
import iotbx.pdb

if(1):
  random.seed(0)
  flex.set_random_seed(0)


def run_0():
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/mmtbx/apply_overall_b_iso_back_to_atoms/m1.pdb",
    test=os.path.isfile)
  xrs = iotbx.pdb.input(file_name = pdb_file).xray_structure_simple()
  #
  b_cart = [2,3,-5,0,0,0]
  u_star = adptbx.u_cart_as_u_star(xrs.unit_cell(), adptbx.b_as_u(b_cart))
  #
  F = xrs.structure_factors(d_min = 2.5).f_calc()
  k_anisotropic = mmtbx.f_model.ext.k_anisotropic(F.indices(), u_star)
  bin_selections = F.log_binning(50)
  print(len(bin_selections))
  #
  d_spacings = F.d_spacings().data()
  ss = 1./flex.pow2(d_spacings) / 4.
  #
  # Use exp model
  #k_mask1 = mmtbx.f_model.ext.k_mask(ss, 0.35, 50.)
  #k_mask2 = 0.35 * flex.exp(-50.*ss)
  #assert approx_equal(k_mask1, k_mask2)
  #k_mask = k_mask1
  #
  # Use array
  k_mask = flex.double(d_spacings.size())

  km = 0.65
  step=km/(len(bin_selections)-1)
  for bs in bin_selections:
    print(km, flex.min(d_spacings.select(bs)), flex.max(d_spacings.select(bs)))
    k_mask = k_mask.set_selected(bs, km)
    km = km-step
    if km<0: km=0
  #
  k_isotropic = 5*flex.exp(-15.*ss)
  #
  fmodel = mmtbx.f_model.manager(
    xray_structure = xrs,
    f_obs          = abs(F),
    k_isotropic    = k_isotropic,
    k_anisotropic  = k_anisotropic,
    k_mask         = k_mask)
  f_calc  = fmodel.f_calc()
  f_masks = fmodel.f_masks()
  f_model = fmodel.f_model()
  f_obs   = abs(f_model)
  r_free_flags = f_obs.generate_r_free_flags()
  #
  assert approx_equal(bulk_solvent.r_factor(f_obs.data(), f_model.data()), 0)
  aso = scaler.run(
    f_obs            = f_obs,
    f_calc           = f_calc,
    f_mask           = f_masks,
    r_free_flags     = r_free_flags,
    bin_selections   = bin_selections,
    number_of_cycles = 80,
    auto_convergence = False,
    auto_convergence_tolerance = 1.e-9,
    ss             = ss,
    bulk_solvent   = True,
    try_poly       = True,
    try_expanal    = True,
    try_expmin     = True,
    verbose        = True)
  aso.show()
  aso.apply_back_trace_of_overall_exp_scale_matrix()
  k,b = aso.overall_isotropic_kb_estimate()
  assert approx_equal(k,5,0.3)
  assert approx_equal(b,15,0.3)
  assert approx_equal(aso.k_exp_overall, k, 0.3)
  assert approx_equal(aso.b_exp_overall, b, 1.5)
  print(aso.overall_isotropic_kb_estimate())
  print(aso.r_final)
  print(aso.r_low  )
  print(aso.r_high )
  #
  assert aso.r_final < 0.0060
  assert aso.r_low   < 0.0037
  assert aso.r_high  < 0.009
  assert approx_equal(
    bulk_solvent.r_factor(f_obs.data(), abs(aso.core.f_model).data(), 1),
    bulk_solvent.r_factor(f_obs.data(), abs(aso.core.f_model).data()), 0.005)

if (__name__ == "__main__"):
  t0 = time.time()
  run_0()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
