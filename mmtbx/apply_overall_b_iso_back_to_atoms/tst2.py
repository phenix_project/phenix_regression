from __future__ import print_function
from libtbx.test_utils import approx_equal
import time, os
from scitbx.array_family import flex
import libtbx.load_env
import iotbx.pdb
from libtbx import easy_run
from cctbx import adptbx
from phenix_regression.refinement import run_phenix_refine

overall_prefix="apply_overall_b_iso_back_to_atoms"

# If test becomes unstable try bigger model (m2.pdb) or increase the resolution

def run(b_cart, b_target_1, b_target_2, b_start):
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/mmtbx/apply_overall_b_iso_back_to_atoms/m2.pdb",
    test=os.path.isfile)
  #
  mtz_file_name="data_%s.mtz"%overall_prefix
  cmd = " ".join([
    "phenix.fmodel",
    "%s"%pdb_file,
    "high_res=2",
    "type=real",
    "r_free=0.1",
    "label=f-obs",
    "b_cart='%s'"%b_cart,
    "output.file_name=%s"%mtz_file_name,
    "> %s.zlog"%overall_prefix])
  assert not easy_run.call(cmd)
  #
  pdb_file_2 = "model_%s.pdb"%overall_prefix
  cmd = " ".join([
    "phenix.pdbtools",
    "set_b_iso=%s"%b_start,
    "%s"%pdb_file,
    "suffix=none",
    "output.prefix=%s"%pdb_file_2.replace(".pdb",""),
    "> %s.zlog"%overall_prefix])
  assert not easy_run.call(cmd)
  #
  args = [
    pdb_file_2,
    mtz_file_name,
    "strategy=individual_adp",
    "main.number_of_macro_cycles=2",
    "apply_overall_isotropic_scale_to_adp=False"]
  r = run_phenix_refine(args = args, prefix = overall_prefix)
  output_pdb_file_name = r.pdb
  b_isos = iotbx.pdb.input(file_name=output_pdb_file_name
    ).xray_structure_simple().extract_u_iso_or_u_equiv()*adptbx.u_as_b(1)
  assert approx_equal(flex.min(b_isos), b_target_1)
  assert approx_equal(flex.max(b_isos), b_target_1)
  assert approx_equal(r.r_work_final,0)
  #
  args = [
    pdb_file_2,
    mtz_file_name,
    "strategy=individual_adp",
    "main.number_of_macro_cycles=2",
    "apply_overall_isotropic_scale_to_adp=True"]
  r = run_phenix_refine(args = args, prefix = overall_prefix)
  output_pdb_file_name = r.pdb
  b_isos = iotbx.pdb.input(file_name=output_pdb_file_name
    ).xray_structure_simple().extract_u_iso_or_u_equiv()*adptbx.u_as_b(1)
  assert approx_equal(flex.min(b_isos), b_target_2, 0.5)
  assert approx_equal(flex.max(b_isos), b_target_2, 0.5)
  assert approx_equal(r.r_work_final,r.r_free_final)
  print("r.r_work_final", r.r_work_final)
  assert approx_equal(r.r_work_final, 0)
  #
  os.remove(mtz_file_name)

if (__name__ == "__main__"):
  t0 = time.time()
  run(b_cart='0 0 0 0 0 0', b_target_1=10, b_target_2=30, b_start=10)
  run(b_cart='0 0 0 0 0 0', b_target_1=50, b_target_2=30, b_start=50)
  run(b_cart='5 5 5 0 0 0', b_target_1=10, b_target_2=35, b_start=10)
  run(b_cart='-5 -5 -5 0 0 0', b_target_1=10, b_target_2=25, b_start=10)
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
