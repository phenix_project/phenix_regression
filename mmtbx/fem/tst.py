from __future__ import print_function
from libtbx import easy_run
import libtbx.load_env
import os

def exercise_00():
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/mmtbx/fem/m.pdb",
    test=os.path.isfile,
    optional=False)
  hkl_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/mmtbx/fem/d.mtz",
    test=os.path.isfile,
    optional=False)
  assert not easy_run.call("phenix.fem %s %s "%(pdb_file,hkl_file))
  print("OK")

if (__name__ == "__main__"):
  exercise_00()
