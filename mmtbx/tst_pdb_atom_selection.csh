#! /bin/csh -fe

set PHENIX_REGRESSION_DIR="`libtbx.find_in_repositories phenix_regression`"
phenix.pdb_atom_selection "$PHENIX_REGRESSION_DIR"/pdb/chains_mixed_case.pdb 'chain "A" and name OD1 and resid 2' | libtbx.assert_stdin_contains_strings '  1 atom selected'
phenix.pdb_atom_selection "$PHENIX_REGRESSION_DIR"/pdb/chains_mixed_case.pdb 'chain "A" and name OD1 and resid 2' --write-pdb-file=tmp_sel.pdb | libtbx.assert_stdin_contains_strings 'Writing file: "tmp_sel.pdb"'
cat tmp_sel.pdb | libtbx.assert_stdin_contains_strings 'CRYST1   21.937    4.866   23.477  90.00 107.08  90.00 P 1 21 1'
phenix.pdb_atom_selection "$PHENIX_REGRESSION_DIR"/pdb/chains_mixed_case.pdb 'chain "A" and name OD1 and resid 2' --write-pdb-file=tmp_sel.pdb --cryst1-replacement-buffer-layer=5 | libtbx.assert_stdin_contains_strings 'Writing file: "tmp_sel.pdb"'
cat tmp_sel.pdb | libtbx.assert_stdin_contains_strings 'CRYST1   10.000   10.000   10.000  90.00  90.00  90.00 P 1'
echo "OK"
