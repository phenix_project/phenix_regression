from __future__ import division
from __future__ import print_function
import time
from libtbx import easy_run
import libtbx.load_env

def exercise(prefix="tst1",keep_origin=False,output_origin_grid_units=None,
     d_min=3,resolution_factor=0.25):
  """
  Test to exercise phenix.map_to_structure_factors with origin shifts
  """
  import os
  pdbf = libtbx.env.find_in_repositories(relative_path=\
     "phenix_regression/mmtbx/map_to_structure_factors/%s.pdb" %prefix,
    test=os.path.isfile)
  mapf = libtbx.env.find_in_repositories(relative_path=\
     "phenix_regression/mmtbx/map_to_structure_factors/%s.ccp4" %prefix,
    test=os.path.isfile)
  assert not None in [pdbf, mapf]

  extra=""
  if keep_origin:
    extra+=" \nkeep_origin=True "
    result_file_name="output_keep_origin.pdb"
  else:
    extra+=" keep_origin=False\n"
  if output_origin_grid_units:
    extra+="\noutput_origin_grid_units='%d %d %d'" %(output_origin_grid_units)

  for f in ["map_to_structure_factors_1.ccp4","map_to_structure_factors.mtz"]:
    if os.path.isfile(f):
      os.remove(f)

  # get structure factors
  cmd = " ".join([
    "phenix.map_to_structure_factors",
    " %s "%( mapf),
    " d_min=3 ",
    extra
      ])
  assert not easy_run.call(cmd)


  # calculate map
  cmd = " ".join([
    "phenix.fft map_to_structure_factors.mtz"
      ])
  easy_run.call(cmd)

  map_file="map_to_structure_factors_1.ccp4"

  # This map is always 0-based. verify that map matches correct PDB
  if output_origin_grid_units==(0,0,0):
    pdb_file_name="tst1_origin_at_zero.pdb"
  elif output_origin_grid_units=="109,108,136":
    pdb_file_name="tst1.pdb"
  elif keep_origin:
    pdb_file_name="tst1.pdb"
  else:
    pdb_file_name="tst1_origin_at_zero.pdb"

  pdb_file_name  = libtbx.env.find_in_repositories(relative_path=\
     "phenix_regression/mmtbx/map_to_structure_factors/%s" %pdb_file_name,
    test=os.path.isfile)



  import iotbx.ccp4_map
  import iotbx.pdb
  m_ccp4 = iotbx.ccp4_map.map_reader(file_name=map_file).data.as_double()
  xrs    = iotbx.pdb.input(file_name=pdb_file_name).xray_structure_simple()

  cs = xrs.crystal_symmetry()
  from cctbx.maptbx import crystal_gridding
  cg = crystal_gridding(
    unit_cell             = cs.unit_cell(),
    space_group_info      = cs.space_group_info(),
    pre_determined_n_real = m_ccp4.all())

  f_calc_from_xrs = xrs.structure_factors(d_min = d_min).f_calc()
  fft_map_from_xrs = f_calc_from_xrs.fft_map(
    resolution_factor=resolution_factor,
    crystal_gridding     = cg)
  fft_map_from_xrs.apply_sigma_scaling()
  m_xrs = fft_map_from_xrs.real_map_unpadded().as_double()

  from cctbx.maptbx import grid_indices_around_sites
  from cctbx.array_family import flex

  sel = grid_indices_around_sites(
      unit_cell  = xrs.unit_cell(),
      fft_n_real = m_ccp4.focus(),
      fft_m_real = m_ccp4.all(),
      sites_cart = xrs.sites_cart(),
      site_radii = flex.double(xrs.scatterers().size(), 1.5))

  m_ccp4 = m_ccp4.select(sel)
  m_xrs  = m_xrs .select(sel)

  value = flex.linear_correlation(x=m_ccp4.as_1d(),
      y=m_xrs.as_1d()).coefficient()
  print("output_origin_grid_units:",output_origin_grid_units)
  print("keep_origin:",keep_origin)
  print("map_file:",map_file)
  print("pdb_file_name:",pdb_file_name)
  print("CC: %.3f" %(value))
  from libtbx.test_utils import approx_equal
  if keep_origin and output_origin_grid_units is None:
    expected_value=0.591
  elif (not keep_origin) and output_origin_grid_units is None:
    expected_value=0.59
  elif output_origin_grid_units == (0,0,0):
    expected_value=0.59
  elif output_origin_grid_units == (109, 108, 136):
    expected_value=0.59

  assert approx_equal(value,expected_value,eps=0.1)
  print("OK")
  return


if(__name__ == "__main__"):
  t0 = time.time()
  exercise(keep_origin=True)
  exercise(keep_origin=False)
  exercise(output_origin_grid_units=(0,0,0))
  exercise(output_origin_grid_units=(109, 108, 136))
  print("Time: %6.4f"%(time.time()-t0))
