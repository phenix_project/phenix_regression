from __future__ import print_function
import iotbx.pdb
from libtbx import easy_run
import libtbx.load_env
import time
import os

def exercise():
  pdb_path = libtbx.env.find_in_repositories(
      relative_path="phenix_regression/pdb/for_pdb_interpretation_test.pdb",
      test=os.path.isfile)
  cmd = " ".join(["phenix.pdb_interpretation",
    "%s >& tst_phenix_regression_mmtbx_pdb_interpretation_2.log" % pdb_path])
  r = easy_run.call(cmd)
  assert r == 0, 'FAILED - %s' % cmd

if (__name__ == "__main__"):
  t0 = time.time()
  exercise()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
