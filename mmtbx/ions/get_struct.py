#!/usr/bin/env phenix.python
# -*- coding: utf-8; py-indent-offset: 2 -*-
from __future__ import division

import sys

import iotbx.pdb
from mmtbx.command_line import fetch_pdb
from mmtbx import ions

def _main(args):
  radius = 6
  for pdb_id in args:
    if all(i.isdigit() or i in ["."] for i in pdb_id):
      radius = float(pdb_id)
      continue

    pdb_id = pdb_id.replace(".pdb", "")
    fetch_pdb.run2([pdb_id])

    pdb_in = iotbx.pdb.input(pdb_id + ".pdb")
    pdb_hierarchy = pdb_in.construct_hierarchy()
    xray_structure = pdb_in.xray_structure_simple()

    metals = pdb_hierarchy.atom_selection_cache().selection(
      " or ".join("resname " + i
                  for i in ions.DEFAULT_IONS + ions.TRANSITION_METALS))
    within = xray_structure.selection_within(
      radius = radius, selection = metals)

    new_hierarchy = pdb_hierarchy.select(within)
    new_hierarchy.write_pdb_file(pdb_id + ".pdb",
                                 crystal_symmetry = pdb_in.crystal_symmetry())



if __name__ == "__main__":
  _main(sys.argv[1:])
