#! /bin/csh -fe

set PHENIX_REGRESSION_DIR="`libtbx.find_in_repositories phenix_regression`"

phenix.xmanip xray_data.file_name="$PHENIX_REGRESSION_DIR"/reflection_files/1167B_peak_cut.sca model.file_name="$PHENIX_REGRESSION_DIR"/refinement/data/1yjp.pdb | grep 'Writing log file ' | libtbx.assert_stdin 'Writing log file with name xmanip.log'

echo "OK"
