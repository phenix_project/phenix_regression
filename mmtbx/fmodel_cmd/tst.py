from __future__ import print_function
import libtbx.load_env
import os
from libtbx.test_utils import approx_equal, run_command
from libtbx.utils import show_times_at_exit
from iotbx import reflection_file_reader
import iotbx.pdb
from scitbx.array_family import flex
import mmtbx.f_model
from libtbx import easy_run
import mmtbx.bulk_solvent.bulk_solvent_and_scaling as bss

def exercise_f_model_option_default(pdb_dir, verbose):
  file_name = os.path.join(pdb_dir, "phe_e.pdb")
  high_resolution = 3.0
  cmd = " ".join(['phenix.fmodel',
                  '%s'%file_name,
                  'high_resolution=%s'%str(high_resolution)])
  run_command(command=cmd, verbose=verbose)
  reflection_file = reflection_file_reader.any_reflection_file(
    file_name          = os.path.basename(file_name)+".mtz",
    ensure_read_access = False)
  miller_arrays = reflection_file.as_miller_arrays()
  assert len(miller_arrays) == 1
  ma = miller_arrays[0]
  assert "%s"%ma.info() == \
    "%s:FMODEL,PHIFMODEL"%(os.path.basename(file_name)+".mtz")
  xray_structure = iotbx.pdb.input(file_name=file_name).xray_structure_simple()
  xray_structure.scattering_type_registry(table="n_gaussian",d_min =ma.d_min())
  assert ma.is_complex_array()
  assert approx_equal(ma.d_min(), high_resolution, 0.1)
  fmodel = mmtbx.f_model.manager(
    xray_structure = xray_structure,
    r_free_flags   = ma.generate_r_free_flags(fraction = 0.1),
    target_name    = "ls_wunit_k1",
    f_obs          = abs(ma))
  r1 = abs(ma).data().min_max_mean().as_tuple()
  r2 = abs(fmodel.f_model()).data().min_max_mean().as_tuple()
  r3 = abs(fmodel.f_calc()).data().min_max_mean().as_tuple()
  r4 = abs(fmodel.f_obs()).data().min_max_mean().as_tuple()
  assert approx_equal(r1, r2, 1.e-5)
  assert approx_equal(r3, r4, 1.e-5)
  assert approx_equal(r1, r4, 1.e-5)

def exercise_f_model_option_custom(pdb_dir, verbose):
  file_name = os.path.join(pdb_dir, "enk_gbr.pdb")
  high_resolution = 2.0
  format = "cns"
  type = "real"
  label = "Fobs"
  low_resolution = 6.0
  algorithm = "direct"
  par = (0.35,60,3,[1,2,-3,0,0,0])
  par_str = 'k_sol=%s b_sol=%s scale=%s b_cart="%s"'%(par[0],
    par[1], par[2], " ".join([str(i) for i in par[3]]).strip())
  for type in ["real", "complex"]:
    for table in ["wk1995", "neutron"]:
      cmd = " ".join(
        ['phenix.fmodel',
         '%s'%file_name,
         'high_resolution=%s'%str(high_resolution),
         'format=%s'%format,
         'type=%s'%type,
         'label=%s'%label,
         'low_resolution=%s'%str(low_resolution),
         'algorithm=%s'%algorithm,
         'scattering_table=%s'%table,
         '%s'%par_str])
      run_command(command=cmd, verbose=verbose)
      xray_structure=iotbx.pdb.input(file_name=file_name).xray_structure_simple()
      reflection_file = reflection_file_reader.any_reflection_file(
        file_name          = os.path.basename(file_name)+".hkl",
        ensure_read_access = False)
      miller_arrays = reflection_file.as_miller_arrays(crystal_symmetry =
        xray_structure.crystal_symmetry())
      assert len(miller_arrays) == 1
      ma = miller_arrays[0]
      if(table == "neutron"):
        xray_structure.switch_to_neutron_scattering_dictionary()
      else:
        xray_structure.scattering_type_registry(table = table,d_min = ma.d_min())
      if(type == "real"): assert ma.is_real_array()
      if(type == "complex"): assert ma.is_complex_array()
      d_max, d_min = ma.d_max_min()
      assert approx_equal(d_min, high_resolution, 0.1)
      assert approx_equal(d_max, low_resolution, 0.1)
      assert "%s"%ma.info() == "%s:FOBS"%(os.path.basename(file_name)+".hkl")
      sf_calc_params = mmtbx.f_model.sf_and_grads_accuracy_master_params.extract()
      sf_calc_params.algorithm = algorithm
      fmodel = mmtbx.f_model.manager(
        xray_structure = xray_structure,
        sf_and_grads_accuracy_params = sf_calc_params,
        target_name    = "ml",
        f_obs          = abs(ma))
      params = bss.master_params.extract()
      params.number_of_macro_cycles=5
      result = fmodel.update_all_scales(fast=False, params=params)
      # tolerances MUST be small, otherwise ring a bell
      assert approx_equal(fmodel.r_work(),        0, 5.e-3)
      assert approx_equal(result.k_sol[0], par[0], 0.03)
      assert approx_equal(result.k_sol[0], result.k_sol[0])
      assert approx_equal(result.b_sol[0],    par[1], 3)
      assert approx_equal(result.b_cart,   par[3], 1.e-2)

def exercise_01(pdb_dir, verbose):
  file_name = os.path.join(pdb_dir, "t.pdb")
  high_resolution = 3.0
  cmd = " ".join(['phenix.fmodel',
                  '%s'%file_name,
                  'high_resolution=%s'%str(high_resolution)])
  result = run_command(command=cmd, verbose=verbose, sorry_expected = True,
    join_stdout_stderr = True)
  sorry_found = False
  for line in result.stdout_lines:
    if(line.startswith("Sorry: Symmetry information in model file is incomplete or missing")):
      sorry_found = True
  assert sorry_found == True

def exercise_f_model_given_external_f_obs(pdb_dir, verbose):
  pdb_file = os.path.join(pdb_dir, "pdb1akg_very_well_phenix_refined_001_noH.pdb")
  mtz_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/1akg.mtz",
    test=os.path.isfile)
  cmd = " ".join([
    'phenix.fmodel',
    '%s'%pdb_file,
    '%s'%mtz_file,
    'type=real',
    'label=f-obs'])
  run_command(command=cmd, verbose=verbose)

def exercise_f_model_given_external_f_obs_2(pdb_dir, verbose):
  pdb_file = os.path.join(pdb_dir, "polypro_Simon_noCRYST1.pdb")
  mtz_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/1akg.mtz",
    test=os.path.isfile)
  cmd = " ".join([
    'phenix.fmodel',
    '%s'%pdb_file,
    '%s'%mtz_file,
    'type=real',
    'label=f-obs'])
  run_command(command=cmd, verbose=verbose)
  reflection_file = reflection_file_reader.any_reflection_file(
    file_name = "polypro_Simon_noCRYST1.pdb.mtz")
  miller_arrays = reflection_file.as_miller_arrays()
  assert len(miller_arrays) == 1
  ma_out = miller_arrays[0]
  reflection_file = reflection_file_reader.any_reflection_file(
    file_name = mtz_file)
  miller_arrays = reflection_file.as_miller_arrays()
  for ma_in in miller_arrays:
    assert ma_out.indices().all_eq(ma_in.indices())

def exercise_f_model_and_sigmas_if_f_is_real(pdb_dir, verbose):
  pdb_file = os.path.join(pdb_dir, "pdb1akg_very_well_phenix_refined_001_noH.pdb")
  cmd = " ".join([
    'phenix.fmodel',
    '%s'%pdb_file,
    "high_res=3.5",
    'type=real',
    "add_sigmas=True",
    'label=FOBS'])
  run_command(command=cmd, verbose=verbose)
  reflection_file = reflection_file_reader.any_reflection_file(
    file_name = "pdb1akg_very_well_phenix_refined_001_noH.pdb.mtz")
  miller_arrays = reflection_file.as_miller_arrays()
  for ma in miller_arrays:
    assert ma.sigmas() is not None and ma.sigmas().all_eq(1)

def exercise_f_model_and_sigmas_if_f_is_complex(pdb_dir, verbose):
  pdb_file = os.path.join(pdb_dir, "pdb1akg_very_well_phenix_refined_001_noH.pdb")
  cmd = " ".join([
    'phenix.fmodel',
    '%s'%pdb_file,
    "high_res=3.5",
    "add_sigmas=True",
    'label=FOBS'])
  result = easy_run.fully_buffered(command=cmd)
  assert result.stderr_lines == \
    ["Sorry: Sigma values only supported when the output type is 'real'."]

def exercise_f_model_and_sigmas_and_anom(pdb_dir, verbose):
  p_str = """
high_resolution = 2
low_resolution = 12
r_free_flags_fraction = 0.1
add_sigmas = True
add_random_error_to_amplitudes_percent = 5
scattering_table = wk1995 it1992 *n_gaussian neutron
fmodel {
  k_sol = 0.35
  b_sol = 50.0
}
output {
  type = *real complex
  file_name = fake.mtz
  label = F-obs(fake)
}
anomalous_scatterers {
  group {
    selection = element Au
    f_prime = 1
    f_double_prime = -8
  }
}
  """
  pdb_file = os.path.join(pdb_dir, "lysozyme_nohoh_plus6H.pdb")
  cmd = " ".join([
    'phenix.fmodel',
    '%s'%pdb_file,
    "params"])
  pf = open("params", "w")
  pf.write(p_str)
  pf.close()
  result = easy_run.fully_buffered(command=cmd)
  reflection_file = reflection_file_reader.any_reflection_file(
    file_name = "fake.mtz")
  miller_arrays = reflection_file.as_miller_arrays()
  counter = 0
  for ma in miller_arrays:
    if(ma.info().labels == ['F-obs(fake)(+)', 'SIGF-obs(fake)(+)',
                            'F-obs(fake)(-)', 'SIGF-obs(fake)(-)']): counter+=1
    if(ma.info().labels == ['R-free-flags(+)', 'R-free-flags(-)']): counter+=1
  assert counter == 2

def exercise_tolarance_to_input_data_type_and_misisng_or_wrong_inputs():
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/mmtbx/fmodel_cmd/model.pdb",
    test=os.path.isfile)
  mtz_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/mmtbx/fmodel_cmd/data.mtz",
    test=os.path.isfile)
  #
  cmd = "phenix.fmodel %s %s > zlog" % (mtz_file, pdb_file)
  assert not easy_run.call(cmd)
  #
  cmd = "phenix.fmodel %s %s > zlog" % (mtz_file, pdb_file+"XXX")
  result = run_command(command=cmd,sorry_expected=True,join_stdout_stderr=True)
  sorry_found = False
  for line in result.stdout_lines:
    if(line == "Sorry: The following arguments are not recognized:"):
      sorry_found = True
  assert sorry_found == True
  # bad or missing input data file
  cmd = "phenix.fmodel %s %s > zlog" % (mtz_file+"XXX", pdb_file)
  print(cmd)
  result = run_command(command=cmd,sorry_expected=True,join_stdout_stderr=True)
  sorry_found = False
  for line in result.stdout_lines:
    if(line == "Sorry: The following arguments are not recognized:"):
      sorry_found = True
  assert sorry_found == True

def exercise_f_model_twin():
  file_base="tmp_fmodel_twin_law"
  open("%s.pdb"%file_base, "w").write("""\
CRYST1   12.000    5.000   12.000  90.00  90.00  90.00 P 1           1
ATOM     39  N   ASN A   6       5.514   2.664   4.856  1.00 11.99           N
ATOM     40  CA  ASN A   6       6.831   2.310   4.318  1.00 12.30           C
ATOM     41  C   ASN A   6       7.854   2.761   5.324  1.00 13.40           C
ATOM     42  O   ASN A   6       8.219   3.943   5.374  1.00 13.92           O
ATOM     43  CB  ASN A   6       7.065   3.016   2.993  1.00 12.13           C
ATOM     44  CG  ASN A   6       5.961   2.735   2.003  1.00 12.77           C
ATOM     45  OD1 ASN A   6       5.798   1.604   1.551  1.00 14.27           O
ATOM     46  ND2 ASN A   6       5.195   3.747   1.679  1.00 10.07           N
ATOM     47  N   TYR A   7       8.292   1.817   6.147  1.00 14.70           N
ATOM     48  CA  TYR A   7       9.159   2.144   7.299  1.00 15.18           C
ATOM     49  C   TYR A   7      10.603   2.331   6.885  1.00 15.91           C
ATOM     50  O   TYR A   7      11.041   1.811   5.855  1.00 15.76           O
ATOM     51  CB  TYR A   7       9.061   1.065   8.369  1.00 15.35           C
ATOM     52  CG  TYR A   7       7.665   0.929   8.902  1.00 14.45           C
ATOM     53  CD1 TYR A   7       6.771   0.021   8.327  1.00 15.68           C
ATOM     54  CD2 TYR A   7       7.210   1.756   9.920  1.00 14.80           C
ATOM     55  CE1 TYR A   7       5.480  -0.094   8.796  1.00 13.46           C
ATOM     56  CE2 TYR A   7       5.904   1.649  10.416  1.00 14.33           C
ATOM     57  CZ  TYR A   7       5.047   0.729   9.831  1.00 15.09           C
ATOM     58  OH  TYR A   7       3.766   0.589  10.291  1.00 14.39           O
ATOM     59  OXT TYR A   7      11.358   2.999   7.612  1.00 17.49           O
""")
  # make sure array is complex and matches R-free flags
  cmd = " ".join([
    "phenix.fmodel",
    "%s.pdb"%file_base,
    "twin_law='l,-k,h'",
    "twin_fraction=0.3",
    "high_res=1.5",
    "r_free=0.1",
    "output.file_name=%s.mtz"%file_base,
    "> %s.zlog"%file_base])
  assert not easy_run.call(cmd)
  miller_arrays = reflection_file_reader.any_reflection_file(
    file_name = "%s.mtz"%file_base).as_miller_arrays()
  assert isinstance(miller_arrays[0].data(), flex.double) # type double is because twinning
  assert isinstance(miller_arrays[1].data(), flex.int)
  assert miller_arrays[0].indices().all_eq(miller_arrays[1].indices())
  # make sure it's actually twinned
  cmd = " ".join([
    "phenix.model_vs_data",
    "%s.pdb"%file_base,
    "%s.mtz"%file_base,
    "twin_law='l,-k,h' ",
    "> %s.zlog"%file_base])
  print(cmd)
  assert not easy_run.call(cmd)
  cntr = 0
  for line in open("%s.zlog"%file_base, "r").readlines():
    if(line.startswith("  r_work: 0.0000")): cntr+=1
  assert cntr == 1

def exercise():
  pdb_dir = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb", test=os.path.isdir)
  eargs = {"pdb_dir": pdb_dir, "verbose": False}
  exercise_f_model_option_default(**eargs)
  exercise_f_model_option_custom(**eargs)
  exercise_01(**eargs)
  exercise_f_model_given_external_f_obs(**eargs)
  exercise_f_model_given_external_f_obs_2(**eargs)
  exercise_f_model_and_sigmas_if_f_is_real(**eargs)
  exercise_f_model_and_sigmas_if_f_is_complex(**eargs)
  exercise_f_model_and_sigmas_and_anom(**eargs)
  exercise_tolarance_to_input_data_type_and_misisng_or_wrong_inputs()
  exercise_f_model_twin()
  print("OK")

if (__name__ == "__main__"):
  show_times_at_exit()
  exercise()
