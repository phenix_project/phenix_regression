from __future__ import print_function

import libtbx.load_env
import os

def exercise () :
  hkl_file_1 = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/xds_correct_unmerged.hkl",
    test=os.path.isfile)
  hkl_file_2 = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/xscale_unmerged.hkl",
    test=os.path.isfile)
  from iotbx import reflection_file_reader
  hkl_in = reflection_file_reader.any_reflection_file(
    file_name=hkl_file_1)
  miller_arrays = hkl_in.as_miller_arrays()
  assert (miller_arrays[0].indices().size() == 5)
  miller_arrays = hkl_in.as_miller_arrays(merge_equivalents=False)
  assert (miller_arrays[0].is_xray_intensity_array())
  assert (miller_arrays[0].indices().size() == 10)
  assert (not miller_arrays[0].is_unique_set_under_symmetry())
  assert miller_arrays[0].anomalous_flag()
  batches = hkl_in.file_content().batch_as_miller_array()
  assert batches.indices().all_eq(miller_arrays[0].indices())
  assert (batches.data()[0] == 271.8)
  hkl_in = reflection_file_reader.any_reflection_file(
    file_name=hkl_file_2)
  miller_arrays = hkl_in.as_miller_arrays(merge_equivalents=False)
  assert (miller_arrays[0].is_xray_intensity_array())
  assert (miller_arrays[0].indices().size() == 12)
  assert (not miller_arrays[0].is_unique_set_under_symmetry())
  batches = hkl_in.file_content().batch_as_miller_array()
  assert batches.indices().all_eq(miller_arrays[0].indices())
  assert (batches.data()[0] == 94.1)
  print("OK")

if (__name__ == "__main__") :
  exercise()
