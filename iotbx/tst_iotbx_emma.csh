#! /bin/csh -fe

set build="`libtbx.show_build_path`"
source "$build/unsetpaths.csh"
source "$build/setpaths.csh"
cd "`libtbx.find_in_repositories phenix_regression/misc`"

phenix.emma gere_MAD_hyss_consensus_model.pdb gere_MAD_hyss_consensus_model.sdb --diffraction_index_equivalent | grep Pairs | libtbx.assert_stdin "  Pairs: 12"

phenix.emma gere_MAD_hyss_consensus_model.pdb gere_MAD_hyss_consensus_model.xyz | grep Pairs | libtbx.assert_stdin "  Pairs: 12"

phenix.emma gere_MAD_hyss_consensus_model.sdb gere_MAD_anom_diffs.res | grep Pairs | libtbx.assert_stdin "  Pairs: 12"

phenix.emma gere_MAD_hyss_consensus_model.pdb gere_MAD_hyss_consensus_model.strudat | grep Pairs | libtbx.assert_stdin "  Pairs: 12"

phenix.emma gere_MAD_anom_diffs.pdb gere_MAD_hyss_consensus_model.xyz --space_group=C2 | grep Pairs | libtbx.assert_stdin "  Pairs: 12"

phenix.emma gere_MAD_hyss_consensus_model.pdb gere_MAD_anom_diffs.res --tolerance=0.2 | grep Pairs | libtbx.assert_stdin "  Pairs: 5"

echo "OK"
