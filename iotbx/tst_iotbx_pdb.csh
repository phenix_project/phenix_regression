#!/bin/csh -f

set PHENIX_REGRESSION_DIR="`libtbx.find_in_repositories phenix_regression`"
#Gset verbose

iotbx.pdb_labels_diff |& libtbx.assert_line_count 1
iotbx.pdb_labels_diff "$PHENIX_REGRESSION_DIR"/pdb/diff_{first,first}.pdb | libtbx.assert_line_count 0
iotbx.pdb_labels_diff "$PHENIX_REGRESSION_DIR"/pdb/diff_{first,second}.pdb | libtbx.assert_line_count 19

iotbx.pdb_labels_comparison |& libtbx.assert_line_count 1
iotbx.pdb_labels_comparison "$PHENIX_REGRESSION_DIR"/pdb/diff_{first,first}.pdb | libtbx.assert_stdin_contains_strings ' 13 matching atom labels' ' 0 missing in '
iotbx.pdb_labels_comparison "$PHENIX_REGRESSION_DIR"/pdb/diff_{first,second}.pdb | libtbx.assert_stdin_contains_strings ' 4 missing in ' ' 5 missing in '

iotbx.pdb.box_around_molecule "$PHENIX_REGRESSION_DIR/protein_pdb_files/gly_chain_all_h_1ozo_v3.ent" buffer_layer=10 | libtbx.assert_stdin_contains_strings "Created file gly_chain_all_h_1ozo_v3_box_000.pdb"

iotbx.pdb.join_fragment_files --exercise

phenix.pdb.b_factor_stats "$PHENIX_REGRESSION_DIR/pdb/1akg.pdb"
phenix.pdb.b_factor_stats "$PHENIX_REGRESSION_DIR/pdb/2ERL.pdb"

iotbx.pdb.link_as_geometry_restraints_edits "$PHENIX_REGRESSION_DIR/pdb/pdb13gs.ent" | grep 'atom_selection_' | libtbx.assert_line_count 4
iotbx.pdb_as_fasta $PHENIX_REGRESSION_DIR/misc/fake_ala_to_val_chain_with_hydrogens.ent | libtbx.assert_stdin ">chain ' A' ARNDCQEGHILKMFPSTWYV"
iotbx.pdb.superpose_centers_of_mass --show_defaults=all.help
iotbx.pdb.superpose_centers_of_mass --show_defaults=0.more
iotbx.pdb.superpose_centers_of_mass --show_defaults=2.all
iotbx.pdb.as_xray_structure "$PHENIX_REGRESSION_DIR"/refinement/anom_diff_map/1mdg.pdb | libtbx.assert_stdin_contains_strings 'At special positions: 3'
iotbx.pdb.as_xray_structure "$PHENIX_REGRESSION_DIR"/refinement/anom_diff_map/1mdg.pdb --ignore_occ_for_site_symmetry | libtbx.assert_stdin_contains_strings 'At special positions: 8'
iotbx.pdb.superpose_atoms_by_name "$PHENIX_REGRESSION_DIR"/pdb/a{d,t}p.pdb | libtbx.assert_stdin_contains_strings 'Number of superposed atoms: 27' 'RMSD: 1.901'

#unset verbose
echo "DONE: $0"
