#! /bin/csh -f

set PHENIX_REGRESSION_DIR="`libtbx.find_in_repositories phenix_regression`"

iotbx.show_distances "$PHENIX_REGRESSION_DIR"/misc/strudat_zeolite_atlas --tag=EUO --distance_cutoff=3.5 | libtbx.assert_stdin_contains_strings 'x,-y+1/2,z'

iotbx.show_distances "$PHENIX_REGRESSION_DIR"/misc/strudat_zeolite_atlas --tag=NAT --cs=10 | libtbx.assert_stdin_contains_strings 'TD10: 1661.40'

iotbx.distance_least_squares "$PHENIX_REGRESSION_DIR"/misc/strudat_zeolite_atlas --tag=AFX | libtbx.assert_stdin_contains_strings 'Final target value (i_trial=0): 0.00'

iotbx.show_distances "$PHENIX_REGRESSION_DIR"/misc/vj1132sup1.cif --distance_cutoff=2 | libtbx.assert_stdin_contains_strings 'Pair counts: [2, 3, 7, 1, 3, 4, 3, 3, 3, 4, 5, 5, 5]'

iotbx.show_distances "$PHENIX_REGRESSION_DIR"/misc/vj1132sup1.cif --cif_data_block_name=I --distance_cutoff=2 | libtbx.assert_stdin_contains_strings 'Pair counts: [2, 3, 7, 1, 3, 4, 3, 3, 3, 4, 5, 5, 5]'

iotbx.show_distances "$PHENIX_REGRESSION_DIR"/misc/vj1132sup1.cif --cif_data_block_name=dummy --distance_cutoff=2 |& libtbx.assert_stdin_contains_strings 'Sorry: cif_data_block_name "dummy" not found in any input files'

iotbx.show_distances "$PHENIX_REGRESSION_DIR"/pdb/phe.pdb --show_cartesian | libtbx.assert_stdin_contains_strings 'Pair counts: [14, 14, 13, 11, 10, 8, 7, 11, 10, 9, 12, 10, 12, 14, 13]'

iotbx.show_distances "$PHENIX_REGRESSION_DIR"/misc/gere_MAD_anom_diffs.res | libtbx.assert_stdin_contains_strings 'SE16  pair count:'

echo "DONE: $0"
