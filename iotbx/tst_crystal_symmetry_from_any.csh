#! /bin/csh -fe

set PHENIX_REGRESSION_DIR="`libtbx.find_in_repositories phenix_regression`"

foreach file (merge.sca no_merge.sca xds_ascii_merge.hkl dtscale.ref i_anomalous.mtz shelx.ins cns.inp heavy_search_1.sdb hyss.pdb solve.setup ../misc/cns.map)
  iotbx.crystal_symmetry_from_any "$PHENIX_REGRESSION_DIR"/reflection_files/$file --niggli_cell
end

echo "OK"
