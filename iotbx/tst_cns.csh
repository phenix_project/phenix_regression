#! /bin/csh -fe

set PHENIX_REGRESSION_DIR="`libtbx.find_in_repositories phenix_regression`"

iotbx.cns.sdb_reader "$PHENIX_REGRESSION_DIR"/reflection_files/heavy_search_1.sdb

cp "$PHENIX_REGRESSION_DIR"/reflection_files/cns.inp .
iotbx.cns.transfer_crystal_symmetry "$PHENIX_REGRESSION_DIR"/reflection_files/dano.mtz cns.inp | libtbx.assert_stdin 'Info: 5 changes (sg, a, b, c, gamma), writing modified file "cns.inp".'
iotbx.cns.transfer_crystal_symmetry "$PHENIX_REGRESSION_DIR"/reflection_files/dano.mtz cns.inp | libtbx.assert_stdin 'Info: no changes, "cns.inp" was not modified.'

iotbx.xplor.map.show_summary "$PHENIX_REGRESSION_DIR"/misc/cns.map

phenix.cns_as_mtz

phenix.cns_as_mtz "$PHENIX_REGRESSION_DIR"/reflection_files/l.cv --symmetry "$PHENIX_REGRESSION_DIR"/reflection_files/l.mtz | grep 'Writing MTZ file' | libtbx.assert_stdin "Writing MTZ file: l.mtz"

echo "OK"
