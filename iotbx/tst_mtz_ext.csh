#! /bin/csh -fe

set PHENIX_REGRESSION_DIR="`libtbx.find_in_repositories phenix_regression`"

# phase_noanom.mtz = 0.7 Mb
iotbx.python "`libtbx.show_dist_paths iotbx`"/mtz/tst_ext.py "$PHENIX_REGRESSION_DIR"/reflection_files/phase_noanom.mtz | tail -2 | head -1 | libtbx.assert_stdin "    FOM    17711 100.00%   0.00    0.97 W: weight (of some sort)"

#
# This fails in Py3 because phenix_regression/reflection_files/gere_MAD.mtz
# has some characters which are not utf-8, therefore cannot be properly decoded
# when trying to print scitbx_array_family_flex_ext.std_string
# in cctbx_project/iotbx/mtz/__init__.py, line 337
#
iotbx.python "`libtbx.show_dist_paths iotbx`"/mtz/tst_ext.py --walk "$PHENIX_REGRESSION_DIR" --verbose | grep '^    H    ' | tail -1 | libtbx.assert_line_count 1

phenix.mtz.dump "$PHENIX_REGRESSION_DIR"/reflection_files/dano.mtz --show_column_data > tmp_mtz1.log
phenix.mtz.dump "$PHENIX_REGRESSION_DIR"/reflection_files/dano.mtz --show_column_data --column_data_format=h > tmp_mtz2.log
diff tmp_mtz1.log tmp_mtz2.log | libtbx.assert_stdin ""

phenix.mtz.dump "$PHENIX_REGRESSION_DIR"/reflection_files/dano.mtz --show_column_data --column_data_format=m | tail -2 | head -1 | libtbx.assert_stdin "End of column data."

phenix.mtz.dump "$PHENIX_REGRESSION_DIR"/reflection_files/dano.mtz --show_column_data --column_data_format=s > tmp_mtz3.log
head -1 tmp_mtz3.log | libtbx.assert_stdin "H,K,L,Frem,SIGFrem,DANOrem,SIGDANOrem,ISYMrem,Finf,SIGFinf,DANOinf,SIGDANOinf,ISYMinf,Fabs,SIGFabs,DANOabs,SIGDANOabs,ISYMabs"

echo "OK"
