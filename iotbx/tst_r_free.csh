#! /bin/csh -fe

set PHENIX_REGRESSION_DIR="`libtbx.find_in_repositories phenix_regression`"

iotbx.r_free_flags_accumulation "$PHENIX_REGRESSION_DIR"/reflection_files/l.mtz | grep 'Writing file:' | libtbx.assert_stdin 'Writing file: "l.mtz.r_free_flags_accumulation"'

iotbx.r_free_flags_completion_simple "$PHENIX_REGRESSION_DIR"/reflection_files/l.mtz 1.49 | grep 'Writing file:' | libtbx.assert_stdin 'Writing file: "l_mtz_extended_r_free_flags.mtz"'

echo "OK"
