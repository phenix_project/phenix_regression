from __future__ import division, print_function
from libtbx.test_utils import show_diff

from phenix.utilities.citations import clear_citations, add_citations, \
    add_citation, show_citations, show, format_citation_html, citations_as_cif_block
from six.moves import cStringIO as StringIO

def exercise () :
  expected_out = """
Afonine PV, Grosse-Kunstleve RW, Adams PD, Lunin VY, Urzhumtsev A. (2007) On
macromolecular refinement at subatomic resolution with interatomic scatterers.
Acta Crystallogr D Biol Crystallogr 63:1194-7.

Liebschner D, Afonine PV, Baker ML, Bunkóczi G, Chen VB, Croll TI, Hintze B,
Hung LW, Jain S, McCoy AJ, Moriarty NW, Oeffner RD, Poon BK, Prisant MG, Read
RJ, Richardson JS, Richardson DC, Sammito MD, Sobolev OV, Stockwell DH,
Terwilliger TC, Urzhumtsev AG, Videau LL, Williams CJ, Adams PD. (2019)
Macromolecular structure determination using X-rays, neutrons and electrons:
recent developments in Phenix. Acta Cryst. D75:861-877.

McCoy AJ, Grosse-Kunstleve RW, Adams PD, Winn MD, Storoni LC, Read RJ. (2007)
Phaser crystallographic software. J Appl Crystallogr 40:658-674.

Williams CJ, Hintze BJ, Headd JJ, Moriarty NW, Chen VB, Jain S, Prisant MG
Lewis SM, Videau LL, Keedy DA, Deis LN, Arendall WB III, Verma V, Snoeyink JS,
Adams PD, Lovell SC, Richardson JS, Richardson DC. (2018) MolProbity: More and
better reference data for improved all-atom structure validation. Protein
Science 27:293-315.

"""
  out = StringIO()
  show_citations(["phenix", "phaser", "molprobity", "ias"], out=out)
  assert not show_diff(out.getvalue(), expected_out)

  expected_out = """
Liebschner D., Afonine P.V., Baker M.L., Bunkóczi G., Chen V.B., Croll T.I.,
Hintze B., Hung L.W., Jain S., McCoy A.J., Moriarty N.W., Oeffner R.D., Poon
B.K., Prisant M.G., Read R.J., Richardson J.S., Richardson D.C., Sammito M.D.,
Sobolev O.V., Stockwell D.H., Terwilliger T.C., Urzhumtsev A.G., Videau L.L.,
Williams C.J., & Adams P.D. (2019). Acta Cryst. D75, 861-877.

McCoy A.J., Grosse-Kunstleve R.W., Adams P.D., Winn M.D., Storoni L.C., & Read
R.J. (2007). J Appl Crystallogr 40, 658-674.

"""
  out = StringIO()
  show_citations(["phenix", "phaser"], out=out, format="iucr")
  assert not show_diff(out.getvalue(), expected_out)
  expected_out = """
Liebschner D., Afonine P.V., Baker M.L., Bunkóczi G., Chen V.B., Croll T.I.,
Hintze B., Hung L.W., Jain S., McCoy A.J., Moriarty N.W., Oeffner R.D., Poon
B.K., Prisant M.G., Read R.J., Richardson J.S., Richardson D.C., Sammito M.D.,
Sobolev O.V., Stockwell D.H., Terwilliger T.C., Urzhumtsev A.G., Videau L.L.,
Williams C.J., and Adams P.D. (2019). Macromolecular structure determination
using X-rays, neutrons and electrons: recent developments in Phenix. Acta
Cryst. D75, 861-877.

McCoy A.J., Grosse-Kunstleve R.W., Adams P.D., Winn M.D., Storoni L.C., and
Read R.J. (2007). Phaser crystallographic software. J Appl Crystallogr 40,
658-674.

"""
  out = StringIO()
  show_citations(["phenix", "phaser"], out=out, format="cell")
  assert not show_diff(out.getvalue(), expected_out)
  clear_citations()
  add_citation("phenix")
  add_citation("phaser")
  add_citations(["molprobity", "ias", "phenix"])
  out = StringIO()
  show(out=out)
#  print "'%s'" % "\n".join(out.getvalue().splitlines()[2:])
#  assert out.getvalue() == expected_out
  clear_citations()
  out = StringIO()
  show(out=out)
  assert out.getvalue() == ""
  html_out = format_citation_html("autobuild_omit")
  assert (html_out == """<b>Iterative-build OMIT maps: map improvement by iterative model building and refinement without model bias.</b> T.C. Terwilliger, R.W. Grosse-Kunstleve, P.V. Afonine, N.W. Moriarty, P.D. Adams, R.J. Read, P.H. Zwart, and L.-W. Hung. <a href="http://dx.doi.org/doi:10.1107/S0907444908004319"><i>Acta Cryst.</i> D<b>64</b>, 515-524 (2008)</a>."""), html_out
  cif_block = citations_as_cif_block(article_ids=["phenix", "phenix.refine", "phaser", "molprobity"])
  assert list(cif_block['_citation.id']) == [
    '1', '2', '3', '4'], list(cif_block['_citation.id'])
  assert list(cif_block['_citation.journal_id_CSD']) == [
    '0766', '0766', '0228', '?'], list(cif_block['_citation.journal_id_CSD'])
  assert list(cif_block['_citation.journal_volume']) == [
      '75', '68', '40', '27'], list(cif_block['_citation.journal_volume'])
  expected_keys = ('_citation.id', '_citation.title', '_citation.journal_abbrev',
                   '_citation.journal_volume', '_citation.page_first',
                   '_citation.page_last', '_citation.year',
                   '_citation.journal_id_ASTM', '_citation.journal_id_ISSN',
                   '_citation.journal_id_CSD')
  for key in expected_keys:
    assert key in cif_block

if __name__ == "__main__" :
  exercise()
  print("OK")
