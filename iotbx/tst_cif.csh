#! /bin/csh -fe

set PHENIX_REGRESSION_DIR="`libtbx.find_in_repositories phenix_regression`"

iotbx.cif.as_xray_structure "$PHENIX_REGRESSION_DIR"/misc/vj1132sup1.cif | libtbx.assert_stdin_contains_strings "Space group: P n a 21 (No. 33)"

iotbx.cif.as_xray_structure "$PHENIX_REGRESSION_DIR"/misc/vj1132Isup2.hkl | libtbx.assert_stdin_contains_strings '  Could not extract an xray.structure from the given input'

iotbx.cif.as_miller_arrays "$PHENIX_REGRESSION_DIR"/misc/vj1132Isup2.hkl | libtbx.assert_stdin_contains_strings "Centric reflections: 83"

iotbx.cif.as_miller_arrays "$PHENIX_REGRESSION_DIR"/misc/vj1132sup1.cif | libtbx.assert_line_count 0

echo "tst_cif.csh: Done."
