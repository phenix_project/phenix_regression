#! /bin/csh -f

set build="`libtbx.show_build_path`"
source "$build/unsetpaths.csh"
source "$build/setpaths.csh"
set PHENIX_REGRESSION_DIR="`libtbx.find_in_repositories phenix_regression`"

phenix.reflection_file_converter "$PHENIX_REGRESSION_DIR"/reflection_files/2ERL.hkl=amplitudes --symmetry "$PHENIX_REGRESSION_DIR"/pdb/2ERL.pdb --resolution=4 --shelx 2ERL.hkl.shelx --low_resolution=15 | grep '^Writing' | libtbx.assert_stdin "Writing SHELX HKLF 3 (amplitude) file: 2ERL.hkl.shelx"

phenix.reflection_file_converter "$PHENIX_REGRESSION_DIR"/reflection_files/hklf_3.shelx --unit_cell="10,10,10,90,90,90" --space_group="P1" --mtz . |& grep Sorry | cut -d: -f-2 | libtbx.assert_stdin 'Sorry: Unresolved amplitude/intensity ambiguity'

phenix.reflection_statistics "$PHENIX_REGRESSION_DIR"/reflection_files/2ERL.hkl=hklf3 --symmetry "$PHENIX_REGRESSION_DIR"/pdb/2ERL.pdb 2ERL.hkl.shelx=amplitudes | grep '^bin 10' | tail -1 | cut -d']' -f2 | libtbx.assert_stdin "  1.000"

phenix.reflection_file_converter hklf3="$PHENIX_REGRESSION_DIR"/reflection_files/2ERL.hkl --symmetry "$PHENIX_REGRESSION_DIR"/pdb/2ERL.pdb  --sca . --scale_max=9999 | grep '^Writing' | libtbx.assert_stdin "Writing Scalepack file: 2ERL.hkl.sca"

phenix.reflection_statistics "$PHENIX_REGRESSION_DIR"/reflection_files/2ERL.hkl=amplitudes --symmetry "$PHENIX_REGRESSION_DIR"/pdb/2ERL.pdb 2ERL.hkl.sca | grep '^bin 10' | tail -1 | cut -d']' -f2 | libtbx.assert_stdin "  1.000"

phenix.reflection_file_converter "$PHENIX_REGRESSION_DIR"/reflection_files/2ERL.hkl=amplitudes --symmetry "$PHENIX_REGRESSION_DIR"/pdb/2ERL.pdb  --cns . --scale_factor=10 | grep '^Writing' | libtbx.assert_stdin "Writing CNS file: 2ERL.hkl.cns"

phenix.reflection_statistics "$PHENIX_REGRESSION_DIR"/reflection_files/2ERL.hkl=amplitudes --symmetry "$PHENIX_REGRESSION_DIR"/pdb/2ERL.pdb 2ERL.hkl.cns | grep '^bin 10' | tail -1 | cut -d']' -f2 | libtbx.assert_stdin "  1.000"

phenix.reflection_file_converter "$PHENIX_REGRESSION_DIR"/reflection_files/2ERL.hkl=amplitudes --symmetry "$PHENIX_REGRESSION_DIR"/pdb/2ERL.pdb  --mtz . | grep '^Writing' | libtbx.assert_stdin "Writing MTZ file: 2ERL.hkl.mtz"

phenix.reflection_statistics "$PHENIX_REGRESSION_DIR"/reflection_files/2ERL.hkl=amplitudes --symmetry "$PHENIX_REGRESSION_DIR"/pdb/2ERL.pdb 2ERL.hkl.mtz | grep '^bin 10' | tail -1 | cut -d']' -f2 | libtbx.assert_stdin "  1.000"

phenix.reflection_file_converter "$PHENIX_REGRESSION_DIR"/reflection_files/dano.mtz --label=fabs --sca . | grep '^Writing' | libtbx.assert_stdin "Writing Scalepack file: dano.mtz.sca"

phenix.reflection_statistics "$PHENIX_REGRESSION_DIR"/reflection_files/dano.mtz dano.mtz.sca | grep 'bin 10' | tail -1 | cut -d']' -f2 | libtbx.assert_stdin "  1.000"

phenix.reflection_file_converter "$PHENIX_REGRESSION_DIR"/reflection_files/dano.mtz --label=fabs --cns . | grep '^Writing' | libtbx.assert_stdin "Writing CNS file: dano.mtz.cns"

phenix.reflection_statistics "$PHENIX_REGRESSION_DIR"/reflection_files/dano.mtz dano.mtz.cns --symmetry dano.mtz.sca | grep 'bin 10' | tail -1 | cut -d']' -f2 | libtbx.assert_stdin "  1.000"

phenix.reflection_file_converter "$PHENIX_REGRESSION_DIR"/reflection_files/dano.mtz --label=fabs --shelx dano.mtz.shelx | grep '^Writing' | libtbx.assert_stdin "Writing SHELX HKLF 3 (amplitude) file: dano.mtz.shelx"

phenix.reflection_statistics "$PHENIX_REGRESSION_DIR"/reflection_files/dano.mtz dano.mtz.shelx=hklf3 --symmetry dano.mtz.sca | grep 'bin 10' | tail -1 | cut -d']' -f2 | libtbx.assert_stdin "  1.000"

phenix.reflection_file_converter "$PHENIX_REGRESSION_DIR"/reflection_files/dano.mtz --label=fabs --mtz . | grep '^Writing' | libtbx.assert_stdin "Writing MTZ file: dano.mtz"

phenix.reflection_statistics "$PHENIX_REGRESSION_DIR"/reflection_files/dano.mtz dano.mtz --bins_twinning_test=10 | grep 'bin 10' | tail -1 | cut -d']' -f2 | libtbx.assert_stdin "  1.000"

phenix.reflection_file_converter "$PHENIX_REGRESSION_DIR"/reflection_files/fp_phi_fom.mtz --mtz tmp.mtz --label=FP | tail -2 | head -1 | libtbx.assert_stdin "Writing MTZ file: tmp.mtz"

phenix.reflection_file_converter "$PHENIX_REGRESSION_DIR"/reflection_files/dtscale_no_symmetry.ref --space_group=10 --unit_cell="29.834 55.932 72.267 90 90 90" --label=Intensity+-,SigmaI+- --sca=. | grep '^Merging symmetry-equivalent values:' | wc -l | sed 's/ //g' | libtbx.assert_stdin "1"

phenix.reflection_file_converter "$PHENIX_REGRESSION_DIR"/reflection_files/merge.sca --change_of_basis='2*l,h,-k' --mtz=cb1 | grep '^    Unit cell: ' | libtbx.assert_stdin "    Unit cell: (88.302, 115.986, 115.986, 60, 90, 90)"
phenix.reflection_file_converter cb1.mtz --change_of_basis='y,-z,2*x' --mtz=cb2 | grep 'Determinant' | libtbx.assert_stdin "  Determinant: -2"
phenix.reflection_statistics "$PHENIX_REGRESSION_DIR"/reflection_files/merge.sca cb2.mtz | grep 'CC Obs 1 2  1.000' | cut -c-17 | libtbx.assert_stdin "CC Obs 1 2  1.000"
phenix.reflection_file_converter cb1.mtz --change_of_basis=to_reference_setting | grep 'Determinant' | libtbx.assert_stdin "  Determinant: 2"
phenix.reflection_file_converter cb1.mtz --change_of_basis=to_primitive_setting | grep 'Determinant' | libtbx.assert_stdin "  Determinant: 2"
phenix.reflection_file_converter cb1.mtz --change_of_basis=to_niggli_cell | grep 'Determinant' | libtbx.assert_stdin "  Determinant: 2"
(phenix.reflection_file_converter cb1.mtz --change_of_basis=to_inverse_hand --mtz=tmp | grep 'W A R N I N G' | libtbx.assert_stdin "")
phenix.reflection_file_converter cb1.mtz --change_of_basis=-h,-k,-l --mtz=tmp | grep 'W A R N I N G' | libtbx.assert_stdin '  W A R N I N G: This change of basis operator changes the hand!'

phenix.reflection_file_converter cb2.mtz --expand_to_p1 --change_to_space_group=p112 --mtz=tmp | grep 'Number of Miller indices:' | libtbx.assert_stdin "Number of Miller indices: 183   Number of Miller indices: 1098     Number of Miller indices: 549"
phenix.reflection_file_converter cb2.mtz --change_to_space_group=p622 --mtz=tmp | grep Merging | libtbx.assert_stdin "  Merging values symmetry-equivalent under new symmetry:"
phenix.reflection_file_converter cb2.mtz --change_to_space_group=p4 --mtz=tmp | grep 'Adapted' | libtbx.assert_stdin "    Adapted unit cell parameters: (115.986, 115.986, 44.151, 90, 90, 90)"

# FIXME unstable, result varies by +/- 1 on different systems
phenix.reflection_file_converter "$PHENIX_REGRESSION_DIR"/reflection_files/2ERL.hkl=amplitudes --symmetry "$PHENIX_REGRESSION_DIR"/pdb/2ERL.pdb --cns . --mtz . --generate_r_free_flags --random_seed=12 | grep overall | cut -c35-46 | libtbx.assert_stdin_contains_strings 'overall 12'
# FIXME unstable, result varies by +/- 1 on different systems
phenix.reflection_file_converter "$PHENIX_REGRESSION_DIR"/reflection_files/2ERL.hkl=amplitudes --random_seed=13 --symmetry "$PHENIX_REGRESSION_DIR"/pdb/2ERL.pdb --cns r_free_flags_2.cns --generate_r_free_flags | grep overall | cut -c35-46 | libtbx.assert_stdin_contains_strings 'overall 12'
grep '^INDE' 2ERL.hkl.cns > inde1.cns
grep '^INDE' r_free_flags_2.cns > inde2.cns
libtbx.assert_line_count 13659 < inde1.cns
libtbx.assert_line_count 13659 < inde2.cns
diff --brief inde1.cns inde2.cns | libtbx.assert_stdin "Files inde1.cns and inde2.cns differ"

phenix.reflection_file_converter 2ERL.hkl.cns --label=fobs --r_free_label=test --symmetry "$PHENIX_REGRESSION_DIR"/pdb/2ERL.pdb --cns . --mtz . | grep '^Writing' | libtbx.assert_stdin 'Writing MTZ file: 2ERL.hkl.cns.mtz Writing CNS file: 2ERL.hkl.cns.cns'

phenix.reflection_file_converter 2ERL.hkl.cns --label=fobs --r_free_label=test --symmetry "$PHENIX_REGRESSION_DIR"/pdb/2ERL.pdb --mtz . --change_of_basis=y,z,x --mtz_root_label=Fobs| grep '^Writing' | libtbx.assert_stdin 'Writing MTZ file: 2ERL.hkl.cns.mtz'

phenix.reflection_file_converter 2ERL.hkl.cns.mtz --change_of_basis=z,x,y --label=Fobs,SIGFobs --r_free_label=R-free-flags --mtz . | grep '^Writing' | libtbx.assert_stdin 'Writing MTZ file: 2ERL.hkl.cns.mtz.mtz'

phenix.reflection_file_converter 2ERL.hkl.cns --symmetry "$PHENIX_REGRESSION_DIR"/pdb/2ERL.pdb --label=test --cns orig | grep '^Writing' | libtbx.assert_stdin 'Writing CNS file: orig.cns'
phenix.reflection_file_converter 2ERL.hkl.cns.mtz.mtz --label=r-free-flags --cns restored | grep '^Writing' | libtbx.assert_stdin 'Writing CNS file: restored.cns'
awk '/NREFlections/,/NOMATCH/' orig.cns > orig
awk '/NREFlections/,/NOMATCH/' restored.cns > restored
(diff orig restored) | libtbx.assert_stdin ""

tar zxf $PHENIX_REGRESSION_DIR/reflection_files/data_for_tst_iotbx_reflection_file.tgz

phenix.reflection_file_converter data_for_tst_iotbx_reflection_file/scale.hkl --symmetry data_for_tst_iotbx_reflection_file/sec17.pdb --label=F_NAT,S_NAT --r_free_label=TEST --resolution=5 --mtz sec17_5angstrom --non_anomalous | grep '^Writing' | libtbx.assert_stdin 'Writing MTZ file: sec17_5angstrom.mtz'

phenix.mtz.dump sec17_5angstrom.mtz | grep 'Number of Miller indices' | libtbx.assert_stdin 'Number of Miller indices: 2422'

# FIXME unstable, result varies by +/- 1 on different systems
phenix.reflection_file_converter "$PHENIX_REGRESSION_DIR"/reflection_files/2ERL.hkl=amplitudes --random_seed=13 --symmetry "$PHENIX_REGRESSION_DIR"/pdb/2ERL.pdb --cns r_free_flags_3.cns --generate_r_free_flags | grep overall | cut -c35-46 | libtbx.assert_stdin_contains_strings 'overall 12'
grep '^INDE' r_free_flags_3.cns > inde3.cns
libtbx.assert_line_count 13659 < inde3.cns
diff inde2.cns inde3.cns | libtbx.assert_line_count 0

phenix.reflection_file_converter "$PHENIX_REGRESSION_DIR"/reflection_files/2ERL.hkl=amplitudes --symmetry "$PHENIX_REGRESSION_DIR"/pdb/2ERL.pdb --cns . --mtz . --generate_r_free_flags --r_free_flags_format=ccp4 --random_seed=13 | grep "convention" | libtbx.assert_stdin "  convention: CCP4 (test=0, work=1-9)"

phenix.reflection_file_converter data_for_tst_iotbx_reflection_file/peak.sca --unit_cell=113.949,113.949,32.474,90,90,90 --mtz=tmpf --write_mtz_amplitudes --non_anomalous --mtz_root_label=FPEAK --label="I(+),SIGI(+),I(-),SIGI(-)"

echo "OK"
