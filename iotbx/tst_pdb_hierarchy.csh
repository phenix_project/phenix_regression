#! /bin/csh -f

set PHENIX_REGRESSION_DIR="`libtbx.find_in_repositories phenix_regression`"

phenix.pdb.hierarchy "$PHENIX_REGRESSION_DIR/pdb/pdb13gs.ent" --details=atom_group --prefix="T@" | grep -v '^T@' | libtbx.assert_stdin ""

rm -f tmp_tst_hierarchy.pdb
phenix.pdb.hierarchy "$PHENIX_REGRESSION_DIR"/pdb/phe_adp_refinement.pdb --write_pdb_file=tmp_tst_hierarchy.pdb --no_anisou
libtbx.assert_line_count 15 < tmp_tst_hierarchy.pdb

phenix.pdb.hierarchy "$PHENIX_REGRESSION_DIR"/pdb/Build_combine_extend_4.pdb --write_pdb_file=tmp_tst_hierarchy.pdb --set_element_simple

phenix.pdb.hierarchy tmp_tst_hierarchy.pdb | grep '    " C  " 427' | libtbx.assert_line_count 1

iotbx.python "`libtbx.show_dist_paths iotbx`/examples/pdb_hierarchy.py" "$PHENIX_REGRESSION_DIR"/pdb/phe_hoh_good.pdb | grep '^END$' | libtbx.assert_stdin "END"

echo "OK"
