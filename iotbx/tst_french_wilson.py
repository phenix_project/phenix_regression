from __future__ import division
from __future__ import print_function
from cctbx import french_wilson
from libtbx import easy_run
import libtbx.load_env
from libtbx.test_utils import Exception_expected, approx_equal
import os

def exercise():
  regression_mtz = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/1dm6.mtz",
    test=os.path.isfile)
  if (regression_mtz is None):
    print("Skipping exercise(): input mtz (1dm6.mtz) not available")
    return
  out_log = "french_wilson.log"
  args = [
    "phenix.french_wilson",
    "\"%s\"" % regression_mtz,
    "wavelength=1.08",
  ]
  cmd = " ".join(args)
  result = easy_run.fully_buffered(cmd)
  assert (result.return_code == 0)
  assert ('Number of bins = 60' in result.stdout_lines)
  assert ("** Total # rejected intensities: 68 **" in result.stdout_lines)
  from iotbx import mtz
  mtz_in = mtz.object("french_wilson.mtz")
  assert approx_equal(mtz_in.as_miller_arrays()[0].info().wavelength,
                      1.08)
  regression_mtz = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/2fgp-sf.cif",
    test=os.path.isfile)
  if (regression_mtz is None):
    print("Skipping exercise(): input cif (2fgp-sf.cif) not available")
    return
  out_log = "french_wilson.log"
  args = [
    "phenix.french_wilson",
    "\"%s\"" % regression_mtz,
  ]
  cmd = " ".join(args)
  result = easy_run.fully_buffered(cmd)
  assert (result.return_code == 0)
  assert ("** Total # rejected intensities: 0 **" in result.stdout_lines)

  sigma_iobs_rejection_criterion = -4.0
  #acentric tests
  # h = 2.02
  I = 201.0
  sigma_I = 50.0
  mean_intensity = 25.0
  J, sigma_J, F, sigma_F = french_wilson.fw_acentric(
                             I,
                             sigma_I,
                             mean_intensity,
                             sigma_iobs_rejection_criterion)
  assert J < I
  assert J > 0

  # h = -2
  I = -10.0
  sigma_I = 5.0
  mean_intensity = 5.0
  J, sigma_J, F, sigma_F = french_wilson.fw_acentric(
                             I,
                             sigma_I,
                             mean_intensity,
                             sigma_iobs_rejection_criterion)
  assert J > 0

  # h = 5
  I = 240.0
  sigma_I = 40.0
  mean_intensity = 40.0
  J, sigma_J, F, sigma_F = french_wilson.fw_acentric(
                             I,
                             sigma_I,
                             mean_intensity,
                             sigma_iobs_rejection_criterion)
  assert J < I
  assert J > 0

  # h = -7
  I = -240.0
  sigma_I = 40.0
  mean_intensity = 40.0
  J, sigma_J, F, sigma_F = french_wilson.fw_acentric(
                             I,
                             sigma_I,
                             mean_intensity,
                             sigma_iobs_rejection_criterion)
  assert J == -1

  #centric tests
  # h = 3.02
  I = 201.0
  sigma_I = 50.0
  mean_intensity = 25.0
  J, sigma_J, F, sigma_F = french_wilson.fw_centric(
                             I,
                             sigma_I,
                             mean_intensity,
                             sigma_iobs_rejection_criterion)
  assert J < I
  assert J > 0

  # h = -2.5
  I = -10.0
  sigma_I = 5.0
  mean_intensity = 5.0
  J, sigma_J, F, sigma_F = french_wilson.fw_centric(
                             I,
                             sigma_I,
                             mean_intensity,
                             sigma_iobs_rejection_criterion)
  assert J > 0

  # h = 5.9
  I = 240.0
  sigma_I = 40.0
  mean_intensity = 200.0
  J, sigma_J, F, sigma_F = french_wilson.fw_centric(
                             I,
                             sigma_I,
                             mean_intensity,
                             sigma_iobs_rejection_criterion)
  assert J < I
  assert J > 0

  # h = -6.1
  I = -240.0
  sigma_I = 40.0
  mean_intensity = 200.0
  J, sigma_J, F, sigma_F = french_wilson.fw_centric(
                             I,
                             sigma_I,
                             mean_intensity,
                             sigma_iobs_rejection_criterion)
  assert J == -1
  # test bad input data
  sca_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/scalepack_bad.sca",
    test=os.path.isfile)
  cmd = "phenix.french_wilson \"%s\"" % sca_file
  out = easy_run.fully_buffered(cmd)
  assert (out.return_code != 0)
  for line in out.stderr_lines :
    if ("Sorry" in line) :
      break
  else :
    raise Exception_expected
  print("OK")

if __name__ == "__main__" :
  exercise()
