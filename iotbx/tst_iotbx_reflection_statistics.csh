#! /bin/csh -fe

setenv LIBTBX_FULL_TESTING
set PHENIX_REGRESSION_DIR="`libtbx.find_in_repositories phenix_regression`"
#set verbose

phenix.reflection_statistics "$PHENIX_REGRESSION_DIR"/reflection_files/fobs_sigma_anomalous.cns | libtbx.assert_stdin_contains_strings 'Unknown unit cell parameters: None'

phenix.reflection_statistics "$PHENIX_REGRESSION_DIR"/reflection_files/fobs_sigma_anomalous.cns --unit_cell=12,12,12.1,89,90,92 | libtbx.assert_stdin_contains_strings 'Unknown space group: None'

phenix.reflection_statistics "$PHENIX_REGRESSION_DIR"/reflection_files/gpd_nat_resolution_8.mtz | libtbx.assert_stdin_contains_strings 'Possible twin laws:' '  l,-k,h'



  phenix.reflection_statistics "$PHENIX_REGRESSION_DIR"/reflection_files/gere_MAD.mtz --resolution 4 --bins 8 | libtbx.assert_stdin_contains_strings 'CC Ano 7 8  0.980  h,k,l'

  phenix.reflection_statistics "$PHENIX_REGRESSION_DIR"/reflection_files/rnase25.mtz --quick | libtbx.assert_stdin_does_not_contain_strings 'CC Obs ' 'CC Ano '

  phenix.reflection_statistics "$PHENIX_REGRESSION_DIR"/reflection_files/infl.sca --unit_cell=114,114,32,90,90,90 --space_group=I4 --weak_symmetry --resolution=4 | libtbx.assert_stdin_contains_strings 'Space group: I 41 (No. 80)'

  phenix.reflection_statistics "$PHENIX_REGRESSION_DIR"/reflection_files/infl.sca --unit_cell=114,114,32,90,90,90 --space_group=I4                 --resolution=4 | libtbx.assert_stdin_contains_strings 'Space group: I 4 (No. 79)'



#unset verbose
echo "DONE: $0"
