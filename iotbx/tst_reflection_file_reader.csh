#! /bin/csh -fe

set PHENIX_REGRESSION_DIR="`libtbx.find_in_repositories phenix_regression`"

iotbx.reflection_file_reader "$PHENIX_REGRESSION_DIR"/reflection_files/{cns.inp,dano.mtz,density_modify.cns,dtscale.ref,fobs_sigma_anomalous.cns,fobs_sigma_no_header.cns,fp_phi_fom.mtz,heavy_search_1.sdb,hklf_3.shelx=hklf3,hklf_3_alpha.shelx=hklf3,hklf_3_anom.shelx=hklf3,hyss.pdb,i_anomalous.mtz,madbst.xplor,merge.sca,no_merge.sca,shelx.ins,solve.setup,solve_fpfm.fmt,xds_ascii_merge.hkl}

iotbx.reflection_file_reader "$PHENIX_REGRESSION_DIR"/reflection_files/enk_1.5.hkl --symmetry="$PHENIX_REGRESSION_DIR"/pdb/enk.pdb

iotbx.reflection_file_reader "$PHENIX_REGRESSION_DIR"/reflection_files/density_modify.cns | grep Centric | tail -n 1 | libtbx.assert_stdin 'Centric reflections: 35'

echo "OK"
