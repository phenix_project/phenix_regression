#! /bin/csh -f

dot -V > & /dev/null
if ($status == 0) then
  rm -f graph.png
  phenix.explore_metric_symmetry --unit_cell "68.4 68.4 68.3 109.5 109.4 109.5" --space_group=p1 --graph graph.png > tmp_e_m_s.log
  if (! -f graph.png) then
    echo "Error: graph.png not found."
    exit 1
  endif
else
  phenix.explore_metric_symmetry --unit_cell "68.4 68.4 68.3 109.5 109.4 109.5" --space_group=p1 > tmp_e_m_s.log
  if ($? != 0) then
    exit 1
  endif
endif
grep 'Spacegroup candidates' tmp_e_m_s.log | libtbx.assert_line_count 30

echo "OK"
