from __future__ import print_function
import libtbx.load_env
from libtbx import easy_run
import iotbx.pdb

def exercise_join_fragment_files():
  phenix_regression_dir = libtbx.env.find_in_repositories("phenix_regression")

  args = ["%s/protein_pdb_files/gly_chain_all_h_1ozo_v3.ent" %phenix_regression_dir,
          "%s/rna_dna_pdb_files/g_5ter_3ter_v3.ent" %phenix_regression_dir,
          "reset_atom_serial=False"]
  cmd = "iotbx.pdb.join_fragment_files %s" %(" ".join(args))
  print(cmd)
  result = easy_run.fully_buffered(cmd).raise_if_errors()
  pdb_inp = iotbx.pdb.input(lines=result.stdout_lines, source_info=None)
  atoms = pdb_inp.atoms()
  assert atoms[0].serial.strip() == '2871'
  assert atoms[-1].serial.strip() == '999'
  assert pdb_inp.file_type() == 'pdb'

  args = ["%s/protein_pdb_files/gly_chain_all_h_1ozo_v3.ent" %phenix_regression_dir,
          "%s/rna_dna_pdb_files/g_5ter_3ter_v3.ent" %phenix_regression_dir,
          "reset_atom_serial=True",
          "format=mmcif"]
  cmd = "iotbx.pdb.join_fragment_files %s" %(" ".join(args))
  print(cmd)
  result = easy_run.fully_buffered(cmd).raise_if_errors()
  pdb_inp = iotbx.pdb.input(lines=result.stdout_lines, source_info=None)
  atoms = pdb_inp.atoms()
  assert atoms[0].serial.strip() == '1'
  assert atoms[-1].serial.strip() == '40'
  assert pdb_inp.file_type() == 'mmcif'

def run():
  exercise_join_fragment_files()
  print("OK")

if __name__ == '__main__':
  run()
