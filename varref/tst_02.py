from __future__ import print_function
from libtbx import easy_run
import time
import iotbx.pdb
import os
from libtbx import easy_run
from libtbx.test_utils import approx_equal

pdb_str = """\
REMARK m.pdb
CRYST1   15.702   16.886   17.565  90.00  90.00  90.00 P 1
SCALE1      0.063686  0.000000  0.000000        0.00000
SCALE2      0.000000  0.059221  0.000000        0.00000
SCALE3      0.000000  0.000000  0.056931        0.00000
HETATM    1  P   2MA I2503       5.937   8.736   5.612  1.00 49.82      J    P
HETATM    2  OP1 2MA I2503       4.991   7.884   4.904  1.00 49.82      J    O
HETATM    3  OP2 2MA I2503       5.698   8.912   7.032  1.00 49.82      J    O
HETATM    4  O5' 2MA I2503       7.281   7.945   5.673  1.00 49.82      J    O
HETATM    5  C5' 2MA I2503       8.216   8.231   6.689  1.00 49.82      J    C
HETATM    6  C4' 2MA I2503       8.652   6.999   7.424  1.00 49.82      J    C
HETATM    7  O4' 2MA I2503       9.641   7.363   8.403  1.00 49.82      J    O
HETATM    8  C3' 2MA I2503       7.562   6.268   8.178  1.00 49.82      J    C
HETATM    9  O3' 2MA I2503       7.851   4.886   8.190  1.00 49.82      J    O
HETATM   10  C2' 2MA I2503       7.714   6.799   9.580  1.00 49.82      J    C
HETATM   11  O2' 2MA I2503       7.240   5.928  10.560  1.00 49.82      J    O
HETATM   12  C1' 2MA I2503       9.215   6.977   9.680  1.00 49.82      J    C
HETATM   13  N9  2MA I2503       9.600   8.039  10.577  1.00 49.82      J    N
HETATM   14  C8  2MA I2503      10.465   7.769  11.532  1.00 49.82      J    C
HETATM   15  N7  2MA I2503      10.661   8.903  12.198  1.00 49.82      J    N
HETATM   16  C5  2MA I2503       9.920   9.812  11.630  1.00 49.82      J    C
HETATM   17  C6  2MA I2503       9.689  11.265  11.881  1.00 49.82      J    C
HETATM   18  N6  2MA I2503      10.264  11.834  12.793  1.00 49.82      J    N
HETATM   19  N1  2MA I2503       8.809  11.974  11.068  1.00 49.82      J    N
HETATM   20  C2  2MA I2503       8.145  11.336  10.007  1.00 49.82      J    C
HETATM   21  N3  2MA I2503       8.359   9.969   9.760  1.00 49.82      J    N
HETATM   22  C4  2MA I2503       9.274   9.256  10.623  1.00 49.82      J    C
HETATM   23  CM2 2MA I2503       7.198  12.080   9.110  1.00 49.82      J    C
TER
END
"""

pdb_str1 = """\
REMARK m1.pdb
CRYST1   15.702   16.886   17.565  90.00  90.00  90.00 P 1
SCALE1      0.063686  0.000000  0.000000        0.00000
SCALE2      0.000000  0.059221  0.000000        0.00000
SCALE3      0.000000  0.000000  0.056931        0.00000
HETATM    1  P   2MA I2503       5.920   9.053   6.102  1.00 58.97      J    P
HETATM    2  OP1 2MA I2503       4.965   8.224   5.377  1.00 58.97      J    O
HETATM    3  OP2 2MA I2503       5.735   9.139   7.538  1.00 58.97      J    O
HETATM    4  O5' 2MA I2503       7.288   8.301   6.059  1.00 58.97      J    O
HETATM    5  C5' 2MA I2503       8.255   8.549   7.056  1.00 58.97      J    C
HETATM    6  C4' 2MA I2503       8.734   7.286   7.707  1.00 58.97      J    C
HETATM    7  O4' 2MA I2503       9.745   7.606   8.679  1.00 58.97      J    O
HETATM    8  C3' 2MA I2503       7.676   6.498   8.446  1.00 58.97      J    C
HETATM    9  O3' 2MA I2503       7.993   5.124   8.379  1.00 58.97      J    O
HETATM   10  C2' 2MA I2503       7.851   6.964   9.868  1.00 58.97      J    C
HETATM   11  O2' 2MA I2503       7.405   6.043  10.816  1.00 58.97      J    O
HETATM   12  C1' 2MA I2503       9.352   7.154   9.944  1.00 58.97      J    C
HETATM   13  N9  2MA I2503       9.747   8.179  10.882  1.00 58.97      J    N
HETATM   14  C8  2MA I2503      10.652   7.876  11.790  1.00 58.97      J    C
HETATM   15  N7  2MA I2503      10.847   8.977  12.511  1.00 58.97      J    N
HETATM   16  C5  2MA I2503      10.073   9.902  12.020  1.00 58.97      J    C
HETATM   17  C6  2MA I2503       9.820  11.335  12.357  1.00 58.97      J    C
HETATM   18  N6  2MA I2503      10.413  11.868  13.277  1.00 58.97      J    N
HETATM   19  N1  2MA I2503       8.899  12.073  11.618  1.00 58.97      J    N
HETATM   20  C2  2MA I2503       8.215  11.478  10.548  1.00 58.97      J    C
HETATM   21  N3  2MA I2503       8.447  10.131  10.223  1.00 58.97      J    N
HETATM   22  C4  2MA I2503       9.401   9.385  11.008  1.00 58.97      J    C
HETATM   23  CM2 2MA I2503       7.222  12.243   9.723  1.00 58.97      J    C
TER
END
"""

pdb_str2 = """\
REMARK m2.pdb
CRYST1   15.702   16.886   17.565  90.00  90.00  90.00 P 1
SCALE1      0.063686  0.000000  0.000000        0.00000
SCALE2      0.000000  0.059221  0.000000        0.00000
SCALE3      0.000000  0.000000  0.056931        0.00000
HETATM    1  P   2MA I2503       5.957   8.392   5.131  1.00 49.93      J    P
HETATM    2  OP1 2MA I2503       5.032   7.505   4.433  1.00 49.93      J    O
HETATM    3  OP2 2MA I2503       5.666   8.626   6.532  1.00 49.93      J    O
HETATM    4  O5' 2MA I2503       7.283   7.583   5.284  1.00 49.93      J    O
HETATM    5  C5' 2MA I2503       8.185   7.908   6.318  1.00 49.93      J    C
HETATM    6  C4' 2MA I2503       8.579   6.703   7.120  1.00 49.93      J    C
HETATM    7  O4' 2MA I2503       9.537   7.095   8.118  1.00 49.93      J    O
HETATM    8  C3' 2MA I2503       7.454   6.019   7.862  1.00 49.93      J    C
HETATM    9  O3' 2MA I2503       7.741   4.641   7.958  1.00 49.93      J    O
HETATM   10  C2' 2MA I2503       7.551   6.626   9.239  1.00 49.93      J    C
HETATM   11  O2' 2MA I2503       7.019   5.819  10.246  1.00 49.93      J    O
HETATM   12  C1' 2MA I2503       9.049   6.791   9.395  1.00 49.93      J    C
HETATM   13  N9  2MA I2503       9.416   7.900  10.244  1.00 49.93      J    N
HETATM   14  C8  2MA I2503      10.246   7.668  11.240  1.00 49.93      J    C
HETATM   15  N7  2MA I2503      10.437   8.834  11.851  1.00 49.93      J    N
HETATM   16  C5  2MA I2503       9.736   9.726  11.210  1.00 49.93      J    C
HETATM   17  C6  2MA I2503       9.520  11.194  11.374  1.00 49.93      J    C
HETATM   18  N6  2MA I2503      10.073  11.802  12.273  1.00 49.93      J    N
HETATM   19  N1  2MA I2503       8.683  11.873  10.492  1.00 49.93      J    N
HETATM   20  C2  2MA I2503       8.046  11.190   9.445  1.00 49.93      J    C
HETATM   21  N3  2MA I2503       8.249   9.808   9.282  1.00 49.93      J    N
HETATM   22  C4  2MA I2503       9.115   9.125  10.213  1.00 49.93      J    C
HETATM   23  CM2 2MA I2503       7.145  11.892   8.471  1.00 49.93      J    C
TER
END
"""

cif_str = """
data_comp_list
loop_
  _chem_comp.id
  _chem_comp.three_letter_code
  _chem_comp.name
  _chem_comp.group
  _chem_comp.number_atoms_all
  _chem_comp.number_atoms_nh
  _chem_comp.desc_level
  2MA  2MA  'Unknown                  '  RNA  38  24  .

data_comp_2MA
loop_
  _chem_comp_atom.comp_id
  _chem_comp_atom.atom_id
  _chem_comp_atom.type_symbol
  _chem_comp_atom.type_energy
  _chem_comp_atom.charge
  _chem_comp_atom.partial_charge
  _chem_comp_atom.x
  _chem_comp_atom.y
  _chem_comp_atom.z
  2MA  P     P  P      0  .   4.6327   1.7111  -0.3702
  2MA  OP1   O  O      0  .   4.9167   0.7447   0.7550
  2MA  OP2   O  OP    -1  .   4.0643   2.9908   0.1966
  2MA  OP3   O  OP    -1  .   5.9139   2.0134  -1.1091
  2MA  O5'   O  O2     0  .   3.5442   1.0395  -1.4098
  2MA  C5'   C  CH2    0  .   2.4444   0.3586  -0.8759
  2MA  C4'   C  CH1    0  .   2.0915  -0.8121  -1.7716
  2MA  O4'   O  O2     0  .   1.4007  -1.9868  -0.9043
  2MA  C3'   C  CH1    0  .   1.2001  -0.4503  -2.6526
  2MA  O3'   O  OH1    0  .   1.3726  -1.2393  -3.9105
  2MA  C2'   C  CH1    0  .  -0.1667  -0.7696  -2.0105
  2MA  O2'   O  OH1    0  .  -0.9452  -1.4571  -2.8841
  2MA  C1'   C  CH1    0  .   0.1483  -1.6423  -0.7772
  2MA  N9    N  NR5    0  .  -0.0404  -0.8602   0.4600
  2MA  C8    C  CR15   0  .   0.7572  -1.3884   1.4098
  2MA  N7    N  N      0  .   0.1852  -1.1612   2.6102
  2MA  C5    C  CR56   0  .  -0.9502  -0.5018   2.4065
  2MA  C6    C  CR6    0  .  -2.0205   0.0278   3.3721
  2MA  N6    N  N      0  .  -1.8729  -0.1101   4.6075
  2MA  N1    N  NR16   0  .  -3.1861   0.6787   2.8589
  2MA  C2    C  CR6    0  .  -3.3386   0.8500   1.4377
  2MA  CM2   C  CH3    0  .  -4.6454   1.4013   0.8711
  2MA  N3    N  N      0  .  -2.2671   0.4384   0.5341
  2MA  C4    C  CR56   0  .  -1.0916  -0.3136   1.0628
  2MA  H5'   H  HCH2   0  .   1.5972   1.0336  -0.8122
  2MA  H5''  H  HCH2   0  .   2.6915  -0.0061   0.1155
  2MA  H4'   H  HCH1   0  .   2.9775  -1.1894  -2.2710
  2MA  H3'   H  HCH1   0  .   1.2810   0.6128  -2.8533
  2MA  HO3'  H  HOH1   0  .   1.7192  -0.6810  -4.5867
  2MA  H2'   H  HCH1   0  .  -0.6572   0.1462  -1.7035
  2MA  HO2'  H  HOH1   0  .  -1.6394  -0.8973  -3.1951
  2MA  H1'   H  HCH1   0  .  -0.4836  -2.5240  -0.7717
  2MA  H8    H  HCR5   0  .   1.6962  -1.9011   1.2392
  2MA  HN6   H  H      0  .  -2.5719   0.2358   5.2377
  2MA  HN1   H  HNR6   0  .  -3.8920   1.0131   3.4844
  2MA  HM21  H  HCH3   0  .  -4.9768   2.2401   1.4760
  2MA  HM22  H  HCH3   0  .  -4.4856   1.7328  -0.1509
  2MA  HM23  H  HCH3   0  .  -5.4033   0.6227   0.8851

loop_
  _chem_comp_bond.comp_id
  _chem_comp_bond.atom_id_1
  _chem_comp_bond.atom_id_2
  _chem_comp_bond.type
  _chem_comp_bond.value_dist
  _chem_comp_bond.value_dist_esd
  _chem_comp_bond.value_dist_neutron
  2MA  P    OP1   deloc     1.510  0.020  1.510
  2MA  P    OP2   deloc     1.511  0.020  1.511
  2MA  P    OP3   deloc     1.510  0.020  1.510
  2MA  P    O5'   single    1.648  0.020  1.648
  2MA  O5'  C5'   single    1.399  0.020  1.399
  2MA  C5'  C4'   single    1.516  0.020  1.516
  2MA  C5'  H5'   single    0.970  0.020  1.090
  2MA  C5'  H5''  single    0.970  0.020  1.090
  2MA  C4'  O4'   single    1.615  0.020  1.615
  2MA  C4'  C3'   single    1.305  0.020  1.305
  2MA  C4'  H4'   single    0.970  0.020  1.090
  2MA  O4'  C1'   single    1.305  0.020  1.305
  2MA  C3'  O3'   single    1.495  0.020  1.495
  2MA  C3'  C2'   single    1.543  0.020  1.543
  2MA  C3'  H3'   single    0.970  0.020  1.090
  2MA  O3'  HO3'  single    0.850  0.020  0.980
  2MA  C2'  O2'   single    1.357  0.020  1.357
  2MA  C2'  C1'   single    1.543  0.020  1.543
  2MA  C2'  H2'   single    0.970  0.020  1.090
  2MA  O2'  HO2'  single    0.850  0.020  0.980
  2MA  C1'  N9    single    1.476  0.020  1.476
  2MA  C1'  H1'   single    0.970  0.020  1.090
  2MA  N9   C8    aromatic  1.348  0.020  1.348
  2MA  N9   C4    aromatic  1.329  0.020  1.329
  2MA  C8   N7    aromatic  1.349  0.020  1.349
  2MA  C8   H8    single    0.930  0.020  1.080
  2MA  N7   C5    aromatic  1.329  0.020  1.329
  2MA  C5   C6    single    1.536  0.020  1.536
  2MA  C5   C4    aromatic  1.364  0.020  1.364
  2MA  C6   N6    double    1.252  0.020  1.252
  2MA  C6   N1    single    1.430  0.020  1.430
  2MA  N6   HN6   single    0.890  0.020  1.040
  2MA  N1   C2    single    1.440  0.020  1.440
  2MA  N1   HN1   single    0.860  0.020  1.020
  2MA  C2   CM2   single    1.527  0.020  1.527
  2MA  C2   N3    double    1.461  0.020  1.461
  2MA  CM2  HM21  single    0.970  0.020  1.090
  2MA  CM2  HM22  single    0.970  0.020  1.090
  2MA  CM2  HM23  single    0.970  0.020  1.090
  2MA  N3   C4    single    1.492  0.020  1.492

loop_
  _chem_comp_angle.comp_id
  _chem_comp_angle.atom_id_1
  _chem_comp_angle.atom_id_2
  _chem_comp_angle.atom_id_3
  _chem_comp_angle.value_angle
  _chem_comp_angle.value_angle_esd
  2MA  O5'   P    OP3   109.47  3.000
  2MA  O5'   P    OP2   109.47  3.000
  2MA  OP3   P    OP2   109.47  3.000
  2MA  O5'   P    OP1   109.47  3.000
  2MA  OP3   P    OP1   109.47  3.000
  2MA  OP2   P    OP1   109.47  3.000
  2MA  C5'   O5'  P     118.46  3.000
  2MA  H5''  C5'  H5'   109.47  3.000
  2MA  H5''  C5'  C4'   109.47  3.000
  2MA  H5'   C5'  C4'   109.47  3.000
  2MA  H5''  C5'  O5'   109.47  3.000
  2MA  H5'   C5'  O5'   109.47  3.000
  2MA  C4'   C5'  O5'   109.47  3.000
  2MA  H4'   C4'  C3'   110.10  3.000
  2MA  H4'   C4'  O4'   110.10  3.000
  2MA  C3'   C4'  O4'   105.79  3.000
  2MA  H4'   C4'  C5'   110.52  3.000
  2MA  C3'   C4'  C5'   110.12  3.000
  2MA  O4'   C4'  C5'   110.12  3.000
  2MA  C1'   O4'  C4'   105.71  3.000
  2MA  H3'   C3'  C2'   110.23  3.000
  2MA  H3'   C3'  O3'   110.67  3.000
  2MA  C2'   C3'  O3'   110.06  3.000
  2MA  H3'   C3'  C4'   110.23  3.000
  2MA  C2'   C3'  C4'   105.47  3.000
  2MA  O3'   C3'  C4'   110.06  3.000
  2MA  HO3'  O3'  C3'   109.47  3.000
  2MA  H2'   C2'  C1'   110.12  3.000
  2MA  H2'   C2'  O2'   110.55  3.000
  2MA  C1'   C2'  O2'   110.18  3.000
  2MA  H2'   C2'  C3'   110.12  3.000
  2MA  C1'   C2'  C3'   105.59  3.000
  2MA  O2'   C2'  C3'   110.18  3.000
  2MA  HO2'  O2'  C2'   109.47  3.000
  2MA  H1'   C1'  N9    110.61  3.000
  2MA  H1'   C1'  C2'   110.17  3.000
  2MA  N9    C1'  C2'   110.13  3.000
  2MA  H1'   C1'  O4'   110.18  3.000
  2MA  N9    C1'  O4'   110.13  3.000
  2MA  C2'   C1'  O4'   105.51  3.000
  2MA  C4    N9   C8    108.03  3.000
  2MA  C4    N9   C1'   134.36  3.000
  2MA  C8    N9   C1'   107.90  3.000
  2MA  H8    C8   N7    125.97  3.000
  2MA  H8    C8   N9    125.97  3.000
  2MA  N7    C8   N9    108.06  3.000
  2MA  C5    N7   C8    108.03  3.000
  2MA  C4    C5   C6    119.97  3.000
  2MA  C4    C5   N7    107.94  3.000
  2MA  C6    C5   N7    132.09  3.000
  2MA  N1    C6   N6    120.02  3.000
  2MA  N1    C6   C5    119.95  3.000
  2MA  N6    C6   C5    120.03  3.000
  2MA  HN6   N6   C6    120.00  3.000
  2MA  HN1   N1   C2    120.17  3.000
  2MA  HN1   N1   C6    120.17  3.000
  2MA  C2    N1   C6    119.65  3.000
  2MA  N3    C2   CM2   119.99  3.000
  2MA  N3    C2   N1    119.96  3.000
  2MA  CM2   C2   N1    119.99  3.000
  2MA  HM23  CM2  HM22  109.47  3.000
  2MA  HM23  CM2  HM21  109.47  3.000
  2MA  HM22  CM2  HM21  109.47  3.000
  2MA  HM23  CM2  C2    109.47  3.000
  2MA  HM22  CM2  C2    109.47  3.000
  2MA  HM21  CM2  C2    109.47  3.000
  2MA  C4    N3   C2    120.04  3.000
  2MA  N3    C4   C5    120.01  3.000
  2MA  N3    C4   N9    132.02  3.000
  2MA  C5    C4   N9    107.94  3.000

loop_
  _chem_comp_tor.comp_id
  _chem_comp_tor.id
  _chem_comp_tor.atom_id_1
  _chem_comp_tor.atom_id_2
  _chem_comp_tor.atom_id_3
  _chem_comp_tor.atom_id_4
  _chem_comp_tor.value_angle
  _chem_comp_tor.value_angle_esd
  _chem_comp_tor.period
  2MA  CONST_01  C5    N7   C8   N9      0.00   0.0  0
  2MA  CONST_02  N7    C5   C4   N9     -0.00   0.0  0
  2MA  CONST_03  C5    C4   N9   C8      0.00   0.0  0
  2MA  CONST_04  C4    C5   N7   C8      0.00   0.0  0
  2MA  CONST_05  C4    N9   C8   N7     -0.00   0.0  0
  2MA  CONST_06  N7    C8   N9   C1'  -151.38   0.0  0
  2MA  CONST_07  C5    C4   N9   C1'   140.40   0.0  0
  2MA  CONST_08  N3    C4   N9   C1'   -41.56   0.0  0
  2MA  CONST_09  C2    N1   C6   C5     -0.89   0.0  0
  2MA  CONST_10  N3    C2   N1   C6     -3.96   0.0  0
  2MA  CONST_11  C4    N3   C2   N1      7.69   0.0  0
  2MA  CONST_12  CM2   C2   N1   C6    173.19   0.0  0
  2MA  CONST_13  C2    N1   C6   N6    179.11   0.0  0
  2MA  CONST_14  C4    N3   C2   CM2  -169.46   0.0  0
  2MA  CONST_15  H8    C8   N9   C1'    28.91   0.0  0
  2MA  CONST_16  HN1   N1   C6   C5    179.10   0.0  0
  2MA  CONST_17  HN6   N6   C6   C5    180.00   0.0  0
  2MA  Var_01    C2'   C1'  O4'  C4'    27.16  30.0  1
  2MA  Var_02    C1'   C2'  C3'  C4'   -12.10  30.0  1
  2MA  Var_03    C2'   C3'  C4'  O4'    27.17  30.0  1
  2MA  Var_04    C3'   C2'  C1'  O4'   -12.13  30.0  1
  2MA  Var_05    C8    N9   C1'  O4'   -37.54  30.0  2
  2MA  Var_06    O4'   C4'  C5'  O5'   151.79  30.0  3
  2MA  Var_07    HM21  CM2  C2   N1     43.51  30.0  3
  2MA  Var_08    C4'   C5'  O5'  P    -145.41  30.0  3

loop_
  _chem_comp_chir.comp_id
  _chem_comp_chir.id
  _chem_comp_chir.atom_id_centre
  _chem_comp_chir.atom_id_1
  _chem_comp_chir.atom_id_2
  _chem_comp_chir.atom_id_3
  _chem_comp_chir.volume_sign
  2MA  chir_01  C4'  C5'  O4'  C3'  negativ
  2MA  chir_02  C3'  O3'  C4'  C2'  positiv
  2MA  chir_03  C2'  O2'  C3'  C1'  positiv
  2MA  chir_04  C1'  O4'  N9   C2'  negativ

loop_
  _chem_comp_plane_atom.comp_id
  _chem_comp_plane_atom.plane_id
  _chem_comp_plane_atom.atom_id
  _chem_comp_plane_atom.dist_esd
  2MA  plan-1  C1'  0.020
  2MA  plan-1  N9   0.020
  2MA  plan-1  C8   0.020
  2MA  plan-1  N7   0.020
  2MA  plan-1  C5   0.020
  2MA  plan-1  C6   0.020
  2MA  plan-1  N6   0.020
  2MA  plan-1  N1   0.020
  2MA  plan-1  C2   0.020
  2MA  plan-1  CM2  0.020
  2MA  plan-1  N3   0.020
  2MA  plan-1  C4   0.020
  2MA  plan-1  H8   0.020
  2MA  plan-1  HN6  0.020
  2MA  plan-1  HN1  0.020

"""

def run(prefix=os.path.basename(__file__).replace(".py","_varref")):
  """
  Basic test. Just make sure it runs. Ensure CRYST1 is present in output PDB.
  """
  # model
  pdb_file = "m_%s.pdb"%prefix
  with open(pdb_file, "w") as fo:
    fo.write(pdb_str)
  pdb_file1 = "m1_%s.pdb"%prefix
  with open(pdb_file1, "w") as fo:
    fo.write(pdb_str1)
  pdb_file2 = "m2_%s.pdb"%prefix
  with open(pdb_file2, "w") as fo:
    fo.write(pdb_str2)
  # restraints cif
  cif_file = "%s.cif"%prefix
  with open(cif_file, "w") as fo:
    fo.write(cif_str)
  #
  map_files = []
  for i, pdb_file in enumerate([pdb_file,pdb_file1,pdb_file2]):
    fn = "m%s_%s"%(i, prefix)
    cmd = " ".join([
      "phenix.model_map",
      pdb_file,
      "output_file_name_prefix=%s"%fn,
      "grid_step=1",
      ">%s.zlog"%prefix])
    assert easy_run.call(cmd)==0
    map_files.append( fn+".ccp4" )
  #
  args = ["phenix.varref", pdb_file, cif_file] + map_files + \
    ["models_per_map=3","nproc=8", "resolution=3",
     "macro_cycles=1", "shake_amplitude=0.1", "output.prefix=%s"%prefix]
  cmd = " ".join(args)
  assert not easy_run.call(cmd), cmd
  pdb_inp = iotbx.pdb.input(file_name="%s_000_all.pdb"%prefix)
  cs = pdb_inp.crystal_symmetry()
  assert approx_equal(cs.unit_cell().parameters()[:3], [15.702, 16.886, 17.565])

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
