from __future__ import print_function
import time
import iotbx.pdb
from scitbx.array_family import flex
import os, sys
from iotbx.cli_parser import run_program
from phenix.programs import varref
from libtbx.utils import multi_out
from six.moves import cStringIO as StringIO
from iotbx import ccp4_map
import math
from libtbx.test_utils import approx_equal

model_str = """\
CRYST1   13.636   12.808   18.233  90.00  90.00  90.00 P 1
ATOM      1  N   GLY B  87       5.977   4.685   6.854  1.00 20.00           N
ATOM      2  CA  GLY B  87       6.859   4.106   7.849  1.00 20.00           C
ATOM      3  C   GLY B  87       7.683   5.138   8.594  1.00 20.00           C
ATOM      4  O   GLY B  87       8.360   4.817   9.571  1.00 20.00           O
ATOM      5  N   GLY B  88       7.629   6.385   8.123  1.00 20.00           N
ATOM      6  CA  GLY B  88       8.375   7.446   8.779  1.00 20.00           C
ATOM      7  C   GLY B  88       7.881   7.732  10.184  1.00 20.00           C
ATOM      8  O   GLY B  88       8.680   7.927  11.105  1.00 20.00           O
ATOM      9  N   GLY B  89       6.565   7.759  10.370  1.00 20.00           N
ATOM     10  CA  GLY B  89       5.984   8.024  11.674  1.00 20.00           C
ATOM     11  C   GLY B  89       5.853   6.783  12.535  1.00 20.00           C
ATOM     12  O   GLY B  89       6.320   6.754  13.673  1.00 20.00           O
TER
ATOM     13  O   HOH S   1       8.636   7.070   5.000  1.00 20.00           O
END
"""

map_1_str = """
CRYST1   13.636   12.808   18.233  90.00  90.00  90.00 P 1
ATOM      1  N   GLY B  87       6.281   5.901   6.178  1.00 20.00           N
ATOM      2  CA  GLY B  87       6.962   5.000   7.089  1.00 20.00           C
ATOM      3  C   GLY B  87       7.602   5.708   8.266  1.00 20.00           C
ATOM      4  O   GLY B  87       8.080   5.067   9.202  1.00 20.00           O
ATOM      5  N   GLY B  88       7.616   7.041   8.214  1.00 20.00           N
ATOM      6  CA  GLY B  88       8.197   7.808   9.304  1.00 20.00           C
ATOM      7  C   GLY B  88       7.431   7.658  10.604  1.00 20.00           C
ATOM      8  O   GLY B  88       8.029   7.519  11.675  1.00 20.00           O
ATOM      9  N   GLY B  89       6.104   7.683  10.532  1.00 20.00           N
ATOM     10  CA  GLY B  89       5.273   7.549  11.714  1.00 20.00           C
ATOM     11  C   GLY B  89       5.000   6.107  12.095  1.00 20.00           C
ATOM     12  O   GLY B  89       5.235   5.701  13.233  1.00 20.00           O
TER
ATOM     13  O   HOH S   1       8.636   7.070   5.000  1.00 20.00           O
END
"""

map_2_str = """
CRYST1   13.636   12.808   18.233  90.00  90.00  90.00 P 1
ATOM      1  N   GLY B  87       6.947   7.134   6.252  1.00 20.00           N
ATOM      2  CA  GLY B  87       7.628   6.233   7.163  1.00 20.00           C
ATOM      3  C   GLY B  87       8.268   6.941   8.340  1.00 20.00           C
ATOM      4  O   GLY B  87       8.746   6.300   9.276  1.00 20.00           O
ATOM      5  N   GLY B  88       8.282   8.274   8.288  1.00 20.00           N
ATOM      6  CA  GLY B  88       8.863   9.041   9.378  1.00 20.00           C
ATOM      7  C   GLY B  88       8.097   8.891  10.678  1.00 20.00           C
ATOM      8  O   GLY B  88       8.695   8.752  11.749  1.00 20.00           O
ATOM      9  N   GLY B  89       6.770   8.916  10.606  1.00 20.00           N
ATOM     10  CA  GLY B  89       5.939   8.782  11.788  1.00 20.00           C
ATOM     11  C   GLY B  89       5.666   7.340  12.169  1.00 20.00           C
ATOM     12  O   GLY B  89       5.901   6.934  13.307  1.00 20.00           O
TER
ATOM     13  O   HOH S   1      10.069   7.769   5.385  1.00 20.00           O
END
"""

map_3_str = """
CRYST1   13.636   12.808   18.233  90.00  90.00  90.00 P 1
ATOM      1  N   GLY B  87       5.773   4.910   6.142  1.00 20.00           N
ATOM      2  CA  GLY B  87       6.454   4.009   7.053  1.00 20.00           C
ATOM      3  C   GLY B  87       7.094   4.717   8.230  1.00 20.00           C
ATOM      4  O   GLY B  87       7.572   4.076   9.166  1.00 20.00           O
ATOM      5  N   GLY B  88       7.108   6.050   8.178  1.00 20.00           N
ATOM      6  CA  GLY B  88       7.689   6.817   9.268  1.00 20.00           C
ATOM      7  C   GLY B  88       6.923   6.667  10.568  1.00 20.00           C
ATOM      8  O   GLY B  88       7.521   6.528  11.639  1.00 20.00           O
ATOM      9  N   GLY B  89       5.596   6.692  10.496  1.00 20.00           N
ATOM     10  CA  GLY B  89       4.765   6.558  11.678  1.00 20.00           C
ATOM     11  C   GLY B  89       4.492   5.116  12.059  1.00 20.00           C
ATOM     12  O   GLY B  89       4.727   4.710  13.197  1.00 20.00           O
TER
ATOM     13  O   HOH S   1       7.963   5.118   5.103  1.00 20.00           O
END
"""

params_str = """\
geometry_restraints {
  edits {
    bond {
      atom_selection_1 = chain B and resseq 87 and name N
      atom_selection_2 = chain S and resseq 1 and resname HOH and name O
      distance_ideal = 2.0
      sigma = 0.01
    }
  }
}
"""

def make_map(pdb_str, map_file_name):
  pdb_inp = iotbx.pdb.input(lines = pdb_str, source_info=None)
  xrs = pdb_inp.xray_structure_simple()
  cs = xrs.crystal_symmetry()
  fc = xrs.structure_factors(d_min=2.5).f_calc()
  fft_map = fc.fft_map(resolution_factor=1/4.)
  fft_map.apply_sigma_scaling()
  map_data = fft_map.real_map_unpadded()
  #
  ccp4_map.write_ccp4_map(
    file_name   = map_file_name,
    unit_cell   = cs.unit_cell(),
    space_group = cs.space_group(),
    map_data    = map_data,
    labels      = flex.std_string([""]))

def run(prefix=os.path.basename(__file__).replace(".py","_varref")):
  """
  Make sure custom bonds are used.
  """
  # model
  pdb_file = "%s.pdb"%prefix
  with open(pdb_file, "w") as fo:
    fo.write(model_str)
  # parameters
  eff_file = "%s.eff"%prefix
  with open(eff_file, "w") as fo:
    fo.write(params_str)
  #
  map_files = []
  for i, pdb_str in enumerate([map_1_str, map_2_str, map_3_str]):
    map_file_name = "%s_%d.map"%(prefix, i)
    make_map(pdb_str=pdb_str, map_file_name=map_file_name)
    map_files.append(map_file_name)
  #
  args = [pdb_file, eff_file] + map_files + [\
    "resolution=3",
    "models_per_map=1",
    "macro_cycles=3",
    "shake_amplitude=0.5",
    "output.prefix=%s"%prefix]
  print("Args to phenix.varref:", args)
  old_stdout = sys.stdout
  logger = multi_out()
  logger.register('stdout', sys.stdout)
  f = open("%s.zlog"%prefix, 'w')
  logger.register('parser_log', f)
  result = run_program(program_class=varref.Program, args=args, logger=logger)
  logger.close()
  sys.stdout = old_stdout
  #
  def dist(r1,r2):
    return math.sqrt( (r1[0]-r2[0])**2 + (r1[1]-r2[1])**2 + (r1[2]-r2[2])**2 )
  pdb_inp = iotbx.pdb.input(file_name = "%s_000_all.pdb"%prefix)
  result = []
  for model in pdb_inp.construct_hierarchy().models():
    a1, a2 = None,None
    for atom in model.atoms():
      if(atom.parent().parent().resseq_as_int()==87 and
         atom.name.strip().upper()=="N"):
        xyz1 = atom.xyz
      if(atom.parent().resname=="HOH"):
        xyz2 = atom.xyz
    result.append( dist(xyz1,xyz2) )
  assert approx_equal(result, [2.0, 2.0, 2.0], 0.05)

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
