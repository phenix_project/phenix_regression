from __future__ import print_function
from libtbx import easy_run
import time
import iotbx.pdb
from scitbx.array_family import flex
from libtbx.test_utils import show_diff
import os, sys
from phenix_regression.real_space_refine import run_real_space_refine
from iotbx.cli_parser import run_program
from phenix.programs import varref
from libtbx.utils import multi_out
from six.moves import cStringIO as StringIO

pdb_str = """\
CRYST1   15.861   12.346   16.697  90.00  90.00  90.00 P 1
SCALE1      0.063048  0.000000  0.000000        0.00000
SCALE2      0.000000  0.080998  0.000000        0.00000
SCALE3      0.000000  0.000000  0.059891        0.00000
HETATM    1  C1  RHQ B 601       6.380   4.872   8.934  1.00111.30           C
HETATM    2  C10 RHQ B 601       9.838   4.134   7.744  1.00111.30           C
HETATM    3  C11 RHQ B 601      10.602   4.338   6.610  1.00111.30           C
HETATM    4  C12 RHQ B 601      10.020   4.864   5.487  1.00111.30           C
HETATM    5  C13 RHQ B 601       8.678   5.189   5.488  1.00111.30           C
HETATM    6  C14 RHQ B 601       5.902   5.368   5.422  1.00111.30           C
HETATM    7  C15 RHQ B 601       5.581   4.200   4.738  1.00111.30           C
HETATM    8  C16 RHQ B 601       4.930   4.269   3.509  1.00111.30           C
HETATM    9  C17 RHQ B 601       4.602   5.506   2.961  1.00111.30           C
HETATM   10  C18 RHQ B 601       4.925   6.677   3.641  1.00111.30           C
HETATM   11  C19 RHQ B 601       5.581   6.609   4.865  1.00111.30           C
HETATM   12  C2  RHQ B 601       5.845   5.343   7.948  1.00111.30           C
HETATM   13  C20 RHQ B 601      10.864   5.095   4.212  1.00111.30           C
HETATM   14  C21 RHQ B 601       2.576   6.666   8.844  1.00111.30           C
HETATM   15  C22 RHQ B 601      12.861   4.146   7.804  1.00111.30           C
HETATM   16  C23 RHQ B 601      13.001   2.777   8.502  1.00111.30           C
HETATM   17  C24 RHQ B 601       5.139   4.578  12.799  1.00111.30           C
HETATM   18  C25 RHQ B 601       4.345   4.474  14.123  1.00111.30           C
HETATM   19  C26 RHQ B 601       5.587   8.152   4.983  1.00111.30           C
HETATM   20  C28 RHQ B 601       7.937   8.298   4.707  1.00111.30           C
HETATM   21  C29 RHQ B 601       9.053   9.358   4.628  1.00111.30           C
HETATM   22  C3  RHQ B 601       4.781   5.845   7.925  1.00111.30           C
HETATM   23  C4  RHQ B 601       3.952   5.988   9.017  1.00111.30           C
HETATM   24  C5  RHQ B 601       4.396   5.446  10.485  1.00111.30           C
HETATM   25  C6  RHQ B 601       5.860   4.833  10.193  1.00111.30           C
HETATM   26  C7  RHQ B 601       8.447   4.473   7.742  1.00111.30           C
HETATM   27  C8  RHQ B 601       7.887   4.981   6.663  1.00111.30           C
HETATM   28  C9  RHQ B 601       6.570   5.297   6.670  1.00111.30           C
HETATM   29  N1  RHQ B 601      12.036   3.992   6.595  1.00111.30           N
HETATM   30  N2  RHQ B 601       4.303   5.210  11.749  1.00111.30           N
HETATM   31  O1  RHQ B 601       7.698   4.278   8.844  1.00111.30           O
HETATM   32  O2  RHQ B 601       6.677   8.905   4.490  1.00111.30           O
HETATM   33  O27 RHQ B 601       4.670   8.706   5.497  1.00111.30           O
HETATM   34  H31 RHQ B 601       4.395   6.238   6.992  1.00111.30           H
HETATM   35  H61 RHQ B 601       6.459   4.378  10.972  1.00111.30           H
HETATM   36 H101 RHQ B 601      10.294   3.717   8.640  1.00111.30           H
HETATM   37 H131 RHQ B 601       8.230   5.606   4.591  1.00111.30           H
HETATM   38 H151 RHQ B 601       5.837   3.232   5.165  1.00111.30           H
HETATM   39 H161 RHQ B 601       4.676   3.354   2.976  1.00111.30           H
HETATM   40 H171 RHQ B 601       4.092   5.559   2.000  1.00111.30           H
HETATM   41 H181 RHQ B 601       4.669   7.644   3.213  1.00111.30           H
HETATM   42 H201 RHQ B 601      11.409   4.186   3.967  1.00111.30           H
HETATM   43 H202 RHQ B 601      11.571   5.906   4.386  1.00111.30           H
HETATM   44 H203 RHQ B 601      10.207   5.358   3.385  1.00111.30           H
HETATM   45 H211 RHQ B 601       2.040   6.192   8.022  1.00111.30           H
HETATM   46 H212 RHQ B 601       2.717   7.720   8.622  1.00111.30           H
HETATM   47 H213 RHQ B 601       2.000   6.560   9.762  1.00111.30           H
HETATM   48 H221 RHQ B 601      12.391   4.857   8.483  1.00111.30           H
HETATM   49 H222 RHQ B 601      13.847   4.512   7.525  1.00111.30           H
HETATM   50 H231 RHQ B 601      12.100   2.568   9.076  1.00111.30           H
HETATM   51 H232 RHQ B 601      13.861   2.796   9.170  1.00111.30           H
HETATM   52 H233 RHQ B 601      13.141   2.000   7.752  1.00111.30           H
HETATM   53 H241 RHQ B 601       6.036   5.177  12.962  1.00111.30           H
HETATM   54 H242 RHQ B 601       5.424   3.570  12.492  1.00111.30           H
HETATM   55 H251 RHQ B 601       3.288   4.313  13.920  1.00111.30           H
HETATM   56 H252 RHQ B 601       4.470   5.391  14.697  1.00111.30           H
HETATM   57 H253 RHQ B 601       4.725   3.631  14.697  1.00111.30           H
HETATM   58 H281 RHQ B 601       8.107   7.539   3.946  1.00111.30           H
HETATM   59 H282 RHQ B 601       7.950   7.837   5.694  1.00111.30           H
HETATM   60 H291 RHQ B 601       8.634  10.346   4.818  1.00111.30           H
HETATM   61 H292 RHQ B 601       9.504   9.339   3.637  1.00111.30           H
HETATM   62 H293 RHQ B 601       9.810   9.133   5.376  1.00111.30           H
HETATM   63 HN11 RHQ B 601      12.456   3.647   5.749  1.00111.30           H
HETATM   64 HN21 RHQ B 601       3.551   5.418  12.372  1.00111.30           H
END
"""

cif_str = """
data_comp_list
loop_
  _chem_comp.id
  _chem_comp.three_letter_code
  _chem_comp.name
  _chem_comp.group
  _chem_comp.number_atoms_all
  _chem_comp.number_atoms_nh
  _chem_comp.desc_level
  RHQ  RHQ  'Unknown                  '  ligand  64  33  .

data_comp_RHQ
loop_
  _chem_comp_atom.comp_id
  _chem_comp_atom.atom_id
  _chem_comp_atom.type_symbol
  _chem_comp_atom.type_energy
  _chem_comp_atom.charge
  _chem_comp_atom.partial_charge
  _chem_comp_atom.x
  _chem_comp_atom.y
  _chem_comp_atom.z
  RHQ  C1    C  CR66  0  .  -1.2669   0.8097   1.7075
  RHQ  C2    C  CR66  0  .  -0.0676   0.8433   1.7078
  RHQ  C3    C  CR16  0  .   0.5926   0.8887   2.6583
  RHQ  C4    C  CR6   0  .   0.0970   0.9114   3.9466
  RHQ  C5    C  CR6   0  .  -1.4968   0.8779   4.3379
  RHQ  C6    C  CR16  0  .  -2.0515   0.8194   2.8202
  RHQ  O1    O  O     0  .  -1.9939   0.7082   0.4574
  RHQ  C7    C  CR66  0  .  -1.3225   0.6508  -0.7154
  RHQ  C8    C  CR66  0  .  -0.0039   0.6874  -0.7194
  RHQ  C9    C  CR6   0  .   0.6677   0.7821   0.4489
  RHQ  C10   C  CR16  0  .  -2.0391   0.5066  -1.9459
  RHQ  C11   C  CR6   0  .  -1.3497   0.4055  -3.1400
  RHQ  C12   C  CR6   0  .   0.0204   0.4435  -3.1445
  RHQ  C13   C  CR16  0  .   0.7114   0.5829  -1.9545
  RHQ  N1    N  NH1   0  .  -2.0907   0.3362  -4.4167
  RHQ  C14   C  CR6   0  .   2.0712   0.9541   0.4430
  RHQ  C15   C  CR16  0  .   2.6157   2.2321   0.3391
  RHQ  C16   C  CR16  0  .   3.9980   2.4007   0.3192
  RHQ  C17   C  CR16  0  .   4.8361   1.2918   0.4031
  RHQ  C18   C  CR16  0  .   4.2918   0.0131   0.5070
  RHQ  C19   C  CR6   0  .   2.9100  -0.1559   0.5270
  RHQ  C20   C  CH3   0  .   0.7932   0.2642  -4.4699
  RHQ  C21   C  CH3   0  .   1.1165   0.9700   5.1046
  RHQ  C22   C  CH2   0  .  -3.2387  -0.5772  -4.5531
  RHQ  C23   C  CH3   0  .  -3.6817  -0.6308  -6.0328
  RHQ  N2    N  NC1   1  .  -2.5628   1.5902   4.2799
  RHQ  C24   C  CH2   0  .  -3.7659   0.7402   4.1282
  RHQ  C25   C  CH3   0  .  -4.6560   0.8776   5.3849
  RHQ  C26   C  C     0  .   2.6645  -1.6807   0.5004
  RHQ  O27   O  O     0  .   1.8293  -2.1615   1.2007
  RHQ  O2    O  O2    0  .   3.4692  -2.5123  -0.3138
  RHQ  C28   C  CH2   0  .   2.7815  -3.6270  -0.8526
  RHQ  C29   C  CH3   0  .   3.7714  -4.5113  -1.6388
  RHQ  H31   H  HCR6  0  .   1.6715   0.9152   2.5277
  RHQ  H61   H  HCR6  0  .  -3.1274   0.7852   2.6780
  RHQ  H101  H  HCR6  0  .  -3.1274   0.4763  -1.9416
  RHQ  H131  H  HCR6  0  .   1.7973   0.6132  -1.9575
  RHQ  HN11  H  HNH1  0  .  -1.8416   0.9490  -5.1775
  RHQ  H151  H  HCR6  0  .   1.9610   3.0989   0.2736
  RHQ  H161  H  HCR6  0  .   4.4231   3.3989   0.2380
  RHQ  H171  H  HCR6  0  .   5.9168   1.4238   0.3875
  RHQ  H181  H  HCR6  0  .   4.9470  -0.8534   0.5726
  RHQ  H201  H  HCH3  0  .   0.4381  -0.6302  -4.9789
  RHQ  H202  H  HCH3  0  .   0.6295   1.1335  -5.1064
  RHQ  H203  H  HCH3  0  .   1.8573   0.1637  -4.2592
  RHQ  H211  H  HCH3  0  .   1.4905  -0.0337   5.3110
  RHQ  H212  H  HCH3  0  .   0.6321   1.3667   5.9946
  RHQ  H213  H  HCH3  0  .   1.9468   1.6163   4.8248
  RHQ  H221  H  HCH2  0  .  -4.0638  -0.2174  -3.9400
  RHQ  H222  H  HCH2  0  .  -2.9500  -1.5752  -4.2241
  RHQ  H231  H  HCH3  0  .  -4.2355  -1.5521  -6.2132
  RHQ  H232  H  HCH3  0  .  -4.3191   0.2247  -6.2523
  RHQ  H233  H  HCH3  0  .  -2.8035  -0.6035  -6.6757
  RHQ  HN21  H  HNC1  0  .  -2.8520   1.7290   3.3331
  RHQ  H241  H  HCH2  0  .  -4.3273   1.0584   3.2488
  RHQ  H242  H  HCH2  0  .  -3.4631  -0.3012   4.0092
  RHQ  H251  H  HCH3  0  .  -5.6290   0.4252   5.1922
  RHQ  H252  H  HCH3  0  .  -4.1801   0.3714   6.2243
  RHQ  H253  H  HCH3  0  .  -4.7870   1.9326   5.6237
  RHQ  H281  H  HCH2  0  .   1.9942  -3.2795  -1.5218
  RHQ  H282  H  HCH2  0  .   2.3396  -4.2081  -0.0422
  RHQ  H291  H  HCH3  0  .   4.3549  -3.8890  -2.3172
  RHQ  H292  H  HCH3  0  .   4.4399  -5.0147  -0.9427
  RHQ  H293  H  HCH3  0  .   3.2176  -5.2549  -2.2131

loop_
  _chem_comp_bond.comp_id
  _chem_comp_bond.atom_id_1
  _chem_comp_bond.atom_id_2
  _chem_comp_bond.type
  _chem_comp_bond.value_dist
  _chem_comp_bond.value_dist_esd
  _chem_comp_bond.value_dist_neutron
  RHQ  C1   C2    aromatic  1.200  0.020  1.200
  RHQ  C1   C6    aromatic  1.362  0.020  1.362
  RHQ  C1   O1    aromatic  1.450  0.020  1.450
  RHQ  C2   C3    aromatic  1.158  0.020  1.158
  RHQ  C2   C9    aromatic  1.459  0.020  1.459
  RHQ  C3   C4    aromatic  1.381  0.020  1.381
  RHQ  C3   H31   single    0.930  0.020  1.080
  RHQ  C4   C5    aromatic  1.641  0.020  1.641
  RHQ  C4   C21   single    1.544  0.020  1.544
  RHQ  C5   C6    aromatic  1.617  0.020  1.617
  RHQ  C5   N2    double    1.283  0.020  1.283
  RHQ  C6   H61   single    0.930  0.020  1.080
  RHQ  O1   C7    aromatic  1.353  0.020  1.353
  RHQ  C7   C8    aromatic  1.319  0.020  1.319
  RHQ  C7   C10   aromatic  1.431  0.020  1.431
  RHQ  C8   C9    aromatic  1.351  0.020  1.351
  RHQ  C8   C13   aromatic  1.431  0.020  1.431
  RHQ  C9   C14   single    1.414  0.020  1.414
  RHQ  C10  C11   aromatic  1.383  0.020  1.383
  RHQ  C10  H101  single    0.930  0.020  1.080
  RHQ  C11  C12   aromatic  1.371  0.020  1.371
  RHQ  C11  N1    single    1.478  0.020  1.478
  RHQ  C12  C13   aromatic  1.383  0.020  1.383
  RHQ  C12  C20   single    1.545  0.020  1.545
  RHQ  C13  H131  single    0.930  0.020  1.080
  RHQ  N1   C22   single    1.473  0.020  1.473
  RHQ  N1   HN11  single    0.860  0.020  1.020
  RHQ  C14  C15   aromatic  1.393  0.020  1.393
  RHQ  C14  C19   aromatic  1.394  0.020  1.394
  RHQ  C15  C16   aromatic  1.393  0.020  1.393
  RHQ  C15  H151  single    0.930  0.020  1.080
  RHQ  C16  C17   aromatic  1.393  0.020  1.393
  RHQ  C16  H161  single    0.930  0.020  1.080
  RHQ  C17  C18   aromatic  1.394  0.020  1.394
  RHQ  C17  H171  single    0.930  0.020  1.080
  RHQ  C18  C19   aromatic  1.392  0.020  1.392
  RHQ  C18  H181  single    0.930  0.020  1.080
  RHQ  C19  C26   single    1.545  0.020  1.545
  RHQ  C20  H201  single    0.970  0.020  1.090
  RHQ  C20  H202  single    0.970  0.020  1.090
  RHQ  C20  H203  single    0.970  0.020  1.090
  RHQ  C21  H211  single    0.970  0.020  1.090
  RHQ  C21  H212  single    0.970  0.020  1.090
  RHQ  C21  H213  single    0.970  0.020  1.090
  RHQ  C22  C23   single    1.545  0.020  1.545
  RHQ  C22  H221  single    0.970  0.020  1.090
  RHQ  C22  H222  single    0.970  0.020  1.090
  RHQ  C23  H231  single    0.970  0.020  1.090
  RHQ  C23  H232  single    0.970  0.020  1.090
  RHQ  C23  H233  single    0.970  0.020  1.090
  RHQ  N2   C24   single    1.481  0.020  1.481
  RHQ  N2   HN21  single    0.890  0.020  1.040
  RHQ  C24  C25   single    1.546  0.020  1.546
  RHQ  C24  H241  single    0.970  0.020  1.090
  RHQ  C24  H242  single    0.970  0.020  1.090
  RHQ  C25  H251  single    0.970  0.020  1.090
  RHQ  C25  H252  single    0.970  0.020  1.090
  RHQ  C25  H253  single    0.970  0.020  1.090
  RHQ  C26  O27   double    1.191  0.020  1.191
  RHQ  C26  O2    single    1.415  0.020  1.415
  RHQ  O2   C28   single    1.416  0.020  1.416
  RHQ  C28  C29   single    1.543  0.020  1.543
  RHQ  C28  H281  single    0.970  0.020  1.090
  RHQ  C28  H282  single    0.970  0.020  1.090
  RHQ  C29  H291  single    0.970  0.020  1.090
  RHQ  C29  H292  single    0.970  0.020  1.090
  RHQ  C29  H293  single    0.970  0.020  1.090

loop_
  _chem_comp_angle.comp_id
  _chem_comp_angle.atom_id_1
  _chem_comp_angle.atom_id_2
  _chem_comp_angle.atom_id_3
  _chem_comp_angle.value_angle
  _chem_comp_angle.value_angle_esd
  RHQ  O1    C1   C6    114.60  3.000
  RHQ  O1    C1   C2    120.24  3.000
  RHQ  C6    C1   C2    125.14  3.000
  RHQ  C9    C2   C3    114.99  3.000
  RHQ  C9    C2   C1    120.15  3.000
  RHQ  C3    C2   C1    124.83  3.000
  RHQ  H31   C3   C4    117.90  3.000
  RHQ  H31   C3   C2    117.91  3.000
  RHQ  C4    C3   C2    124.18  3.000
  RHQ  C21   C4   C5    117.59  3.000
  RHQ  C21   C4   C3    117.61  3.000
  RHQ  C5    C4   C3    124.80  3.000
  RHQ  N2    C5   C6     72.10  3.000
  RHQ  N2    C5   C4    141.67  3.000
  RHQ  C6    C5   C4     96.32  3.000
  RHQ  H61   C6   C5    117.65  3.000
  RHQ  H61   C6   C1    117.63  3.000
  RHQ  C5    C6   C1    124.73  3.000
  RHQ  C7    O1   C1    120.11  3.000
  RHQ  C10   C7   C8    120.04  3.000
  RHQ  C10   C7   O1    120.08  3.000
  RHQ  C8    C7   O1    119.84  3.000
  RHQ  C13   C8   C9    120.20  3.000
  RHQ  C13   C8   C7    120.01  3.000
  RHQ  C9    C8   C7    119.76  3.000
  RHQ  C14   C9   C8    119.90  3.000
  RHQ  C14   C9   C2    119.91  3.000
  RHQ  C8    C9   C2    119.90  3.000
  RHQ  H101  C10  C11   119.99  3.000
  RHQ  H101  C10  C7    119.99  3.000
  RHQ  C11   C10  C7    120.02  3.000
  RHQ  N1    C11  C12   119.98  3.000
  RHQ  N1    C11  C10   119.97  3.000
  RHQ  C12   C11  C10   119.95  3.000
  RHQ  C20   C12  C13   120.00  3.000
  RHQ  C20   C12  C11   119.98  3.000
  RHQ  C13   C12  C11   119.96  3.000
  RHQ  H131  C13  C12   119.99  3.000
  RHQ  H131  C13  C8    120.00  3.000
  RHQ  C12   C13  C8    120.01  3.000
  RHQ  HN11  N1   C22   119.97  3.000
  RHQ  HN11  N1   C11   119.97  3.000
  RHQ  C22   N1   C11   119.98  3.000
  RHQ  C19   C14  C15   119.99  3.000
  RHQ  C19   C14  C9    120.02  3.000
  RHQ  C15   C14  C9    119.99  3.000
  RHQ  H151  C15  C16   119.99  3.000
  RHQ  H151  C15  C14   120.00  3.000
  RHQ  C16   C15  C14   120.01  3.000
  RHQ  H161  C16  C17   120.00  3.000
  RHQ  H161  C16  C15   120.00  3.000
  RHQ  C17   C16  C15   120.01  3.000
  RHQ  H171  C17  C18   120.00  3.000
  RHQ  H171  C17  C16   120.00  3.000
  RHQ  C18   C17  C16   120.01  3.000
  RHQ  H181  C18  C19   120.00  3.000
  RHQ  H181  C18  C17   119.99  3.000
  RHQ  C19   C18  C17   120.00  3.000
  RHQ  C26   C19  C18   106.10  3.000
  RHQ  C26   C19  C14   133.58  3.000
  RHQ  C18   C19  C14   119.99  3.000
  RHQ  H203  C20  H202  109.47  3.000
  RHQ  H203  C20  H201  109.47  3.000
  RHQ  H202  C20  H201  109.47  3.000
  RHQ  H203  C20  C12   109.48  3.000
  RHQ  H202  C20  C12   109.47  3.000
  RHQ  H201  C20  C12   109.47  3.000
  RHQ  H213  C21  H212  109.47  3.000
  RHQ  H213  C21  H211  109.47  3.000
  RHQ  H212  C21  H211  109.47  3.000
  RHQ  H213  C21  C4    109.48  3.000
  RHQ  H212  C21  C4    109.47  3.000
  RHQ  H211  C21  C4    109.47  3.000
  RHQ  H222  C22  H221  109.47  3.000
  RHQ  H222  C22  C23   109.47  3.000
  RHQ  H221  C22  C23   109.47  3.000
  RHQ  H222  C22  N1    109.47  3.000
  RHQ  H221  C22  N1    109.47  3.000
  RHQ  C23   C22  N1    109.48  3.000
  RHQ  H233  C23  H232  109.47  3.000
  RHQ  H233  C23  H231  109.47  3.000
  RHQ  H232  C23  H231  109.46  3.000
  RHQ  H233  C23  C22   109.47  3.000
  RHQ  H232  C23  C22   109.48  3.000
  RHQ  H231  C23  C22   109.48  3.000
  RHQ  HN21  N2   C24    75.38  3.000
  RHQ  HN21  N2   C5    111.11  3.000
  RHQ  C24   N2   C5    111.16  3.000
  RHQ  H242  C24  H241  109.47  3.000
  RHQ  H242  C24  C25   109.47  3.000
  RHQ  H241  C24  C25   109.46  3.000
  RHQ  H242  C24  N2    109.48  3.000
  RHQ  H241  C24  N2    109.47  3.000
  RHQ  C25   C24  N2    109.48  3.000
  RHQ  H253  C25  H252  109.47  3.000
  RHQ  H253  C25  H251  109.47  3.000
  RHQ  H252  C25  H251  109.47  3.000
  RHQ  H253  C25  C24   109.47  3.000
  RHQ  H252  C25  C24   109.48  3.000
  RHQ  H251  C25  C24   109.47  3.000
  RHQ  O2    C26  O27   119.98  3.000
  RHQ  O2    C26  C19   119.98  3.000
  RHQ  O27   C26  C19   119.98  3.000
  RHQ  C28   O2   C26   113.91  3.000
  RHQ  H282  C28  H281  109.47  3.000
  RHQ  H282  C28  C29   109.46  3.000
  RHQ  H281  C28  C29   109.47  3.000
  RHQ  H282  C28  O2    109.47  3.000
  RHQ  H281  C28  O2    109.47  3.000
  RHQ  C29   C28  O2    109.48  3.000
  RHQ  H293  C29  H292  109.47  3.000
  RHQ  H293  C29  H291  109.47  3.000
  RHQ  H292  C29  H291  109.47  3.000
  RHQ  H293  C29  C28   109.47  3.000
  RHQ  H292  C29  C28   109.47  3.000
  RHQ  H291  C29  C28   109.48  3.000

loop_
  _chem_comp_tor.comp_id
  _chem_comp_tor.id
  _chem_comp_tor.atom_id_1
  _chem_comp_tor.atom_id_2
  _chem_comp_tor.atom_id_3
  _chem_comp_tor.atom_id_4
  _chem_comp_tor.value_angle
  _chem_comp_tor.value_angle_esd
  _chem_comp_tor.period
  RHQ  CONST_01  C4    C3   C2   C1      0.00   0.0  0
  RHQ  CONST_02  C8    C9   C2   C1     -0.00   0.0  0
  RHQ  CONST_03  C14   C9   C2   C1    173.78   0.0  0
  RHQ  CONST_04  C4    C5   C6   C1      0.00   0.0  0
  RHQ  CONST_05  C8    C7   O1   C1      0.00   0.0  0
  RHQ  CONST_06  C10   C7   O1   C1    177.96   0.0  0
  RHQ  CONST_07  C5    C6   C1   C2     -0.00   0.0  0
  RHQ  CONST_08  C7    O1   C1   C2     -0.00   0.0  0
  RHQ  CONST_09  C5    C4   C3   C2     -0.00   0.0  0
  RHQ  CONST_10  C7    C8   C9   C2     -0.00   0.0  0
  RHQ  CONST_11  C13   C8   C9   C2   -177.95   0.0  0
  RHQ  CONST_12  C15   C14  C9   C2    -87.98   0.0  0
  RHQ  CONST_13  C19   C14  C9   C2     92.69   0.0  0
  RHQ  CONST_14  C6    C1   C2   C3      0.00   0.0  0
  RHQ  CONST_15  O1    C1   C2   C3   -177.91   0.0  0
  RHQ  CONST_16  C8    C9   C2   C3    178.11   0.0  0
  RHQ  CONST_17  C14   C9   C2   C3     -8.11   0.0  0
  RHQ  CONST_18  C6    C5   C4   C3      0.00   0.0  0
  RHQ  CONST_19  C9    C2   C3   C4   -178.01   0.0  0
  RHQ  CONST_20  O1    C1   C6   C5    178.01   0.0  0
  RHQ  CONST_21  C9    C2   C1   C6    177.91   0.0  0
  RHQ  CONST_22  C7    O1   C1   C6   -178.12   0.0  0
  RHQ  CONST_23  C9    C2   C1   O1      0.00   0.0  0
  RHQ  CONST_24  C9    C8   C7   O1      0.00   0.0  0
  RHQ  CONST_25  C13   C8   C7   O1    177.96   0.0  0
  RHQ  CONST_26  C11   C10  C7   O1   -177.95   0.0  0
  RHQ  CONST_27  C14   C9   C8   C7   -173.78   0.0  0
  RHQ  CONST_28  C12   C13  C8   C7     -0.00   0.0  0
  RHQ  CONST_29  C12   C11  C10  C7     -0.00   0.0  0
  RHQ  CONST_30  C11   C10  C7   C8      0.00   0.0  0
  RHQ  CONST_31  C15   C14  C9   C8     85.80   0.0  0
  RHQ  CONST_32  C19   C14  C9   C8    -93.53   0.0  0
  RHQ  CONST_33  C11   C12  C13  C8      0.00   0.0  0
  RHQ  CONST_34  C10   C7   C8   C9   -177.96   0.0  0
  RHQ  CONST_35  C12   C13  C8   C9    177.95   0.0  0
  RHQ  CONST_36  C16   C15  C14  C9   -179.33   0.0  0
  RHQ  CONST_37  C18   C19  C14  C9    179.33   0.0  0
  RHQ  CONST_38  C13   C8   C7   C10     0.00   0.0  0
  RHQ  CONST_39  C13   C12  C11  C10     0.00   0.0  0
  RHQ  CONST_40  C14   C9   C8   C13     8.26   0.0  0
  RHQ  CONST_41  C17   C16  C15  C14    -0.00   0.0  0
  RHQ  CONST_42  C17   C18  C19  C14    -0.00   0.0  0
  RHQ  CONST_43  C18   C19  C14  C15     0.00   0.0  0
  RHQ  CONST_44  C18   C17  C16  C15     0.00   0.0  0
  RHQ  CONST_45  C19   C14  C15  C16     0.00   0.0  0
  RHQ  CONST_46  C19   C18  C17  C16    -0.00   0.0  0
  RHQ  CONST_47  N2    C5   C6   C1    142.52   0.0  0
  RHQ  CONST_48  C21   C4   C3   C2    179.89   0.0  0
  RHQ  CONST_49  N2    C5   C4   C3    -69.00   0.0  0
  RHQ  CONST_50  C21   C4   C5   C6   -179.89   0.0  0
  RHQ  CONST_51  N1    C11  C10  C7   -176.43   0.0  0
  RHQ  CONST_52  C20   C12  C13  C8   -177.13   0.0  0
  RHQ  CONST_53  C26   C19  C14  C9      6.94   0.0  0
  RHQ  CONST_54  C20   C12  C11  C10   177.13   0.0  0
  RHQ  CONST_55  N1    C11  C12  C13   176.43   0.0  0
  RHQ  CONST_56  C26   C19  C14  C15  -172.39   0.0  0
  RHQ  CONST_57  C26   C19  C18  C17   174.27   0.0  0
  RHQ  CONST_58  H31   C3   C2   C1    180.00   0.0  0
  RHQ  CONST_59  H61   C6   C1   C2   -180.00   0.0  0
  RHQ  CONST_60  H101  C10  C7   O1      2.04   0.0  0
  RHQ  CONST_61  H131  C13  C8   C7    180.00   0.0  0
  RHQ  CONST_62  H151  C15  C14  C9      0.68   0.0  0
  RHQ  CONST_63  H161  C16  C15  C14   180.00   0.0  0
  RHQ  CONST_64  H181  C18  C19  C14   180.00   0.0  0
  RHQ  CONST_65  H171  C17  C16  C15  -180.00   0.0  0
  RHQ  CONST_66  C24   N2   C5   C4    148.47   0.0  0
  RHQ  CONST_67  C24   N2   C5   C6     71.29   0.0  0
  RHQ  CONST_68  C22   N1   C11  C10   -46.87   0.0  0
  RHQ  CONST_69  C22   N1   C11  C12   136.70   0.0  0
  RHQ  CONST_70  HN21  N2   C5   C4     66.55   0.0  0
  RHQ  CONST_71  HN11  N1   C11  C10   129.86   0.0  0
  RHQ  Var_01    O27   C26  C19  C14   -49.25  30.0  2
  RHQ  Var_02    H211  C21  C4   C3    -82.01  30.0  2
  RHQ  Var_03    H201  C20  C12  C11   -51.00  30.0  2
  RHQ  Var_04    C25   C24  N2   C5    114.92  30.0  3
  RHQ  Var_05    C23   C22  N1   C11  -170.69  30.0  3
  RHQ  Var_06    C29   C28  O2   C26  -176.16  30.0  3
  RHQ  Var_07    H231  C23  C22  N1    157.99  30.0  3
  RHQ  Var_08    H251  C25  C24  N2    168.25  30.0  3
  RHQ  Var_09    H291  C29  C28  O2    -48.95  30.0  3

loop_
  _chem_comp_plane_atom.comp_id
  _chem_comp_plane_atom.plane_id
  _chem_comp_plane_atom.atom_id
  _chem_comp_plane_atom.dist_esd
  RHQ  plan-1  C9    0.020
  RHQ  plan-1  C14   0.020
  RHQ  plan-1  C15   0.020
  RHQ  plan-1  C16   0.020
  RHQ  plan-1  C17   0.020
  RHQ  plan-1  C18   0.020
  RHQ  plan-1  C19   0.020
  RHQ  plan-1  C26   0.020
  RHQ  plan-1  H151  0.020
  RHQ  plan-1  H161  0.020
  RHQ  plan-1  H171  0.020
  RHQ  plan-1  H181  0.020
  RHQ  plan-2  O1    0.020
  RHQ  plan-2  C7    0.020
  RHQ  plan-2  C8    0.020
  RHQ  plan-2  C9    0.020
  RHQ  plan-2  C10   0.020
  RHQ  plan-2  C11   0.020
  RHQ  plan-2  C12   0.020
  RHQ  plan-2  C13   0.020
  RHQ  plan-2  N1    0.020
  RHQ  plan-2  C20   0.020
  RHQ  plan-2  H101  0.020
  RHQ  plan-2  H131  0.020
  RHQ  plan-3  C11   0.020
  RHQ  plan-3  N1    0.020
  RHQ  plan-3  C22   0.020
  RHQ  plan-3  HN11  0.020
  RHQ  plan-4  C1    0.020
  RHQ  plan-4  C2    0.020
  RHQ  plan-4  C3    0.020
  RHQ  plan-4  C4    0.020
  RHQ  plan-4  C5    0.020
  RHQ  plan-4  C6    0.020
  RHQ  plan-4  O1    0.020
  RHQ  plan-4  C9    0.020
  RHQ  plan-4  C21   0.020
  RHQ  plan-4  N2    0.020
  RHQ  plan-4  C24   0.020
  RHQ  plan-4  H31   0.020
  RHQ  plan-4  H61   0.020
  RHQ  plan-4  HN21  0.020
  RHQ  plan-5  C19   0.020
  RHQ  plan-5  C26   0.020
  RHQ  plan-5  O27   0.020
  RHQ  plan-5  O2    0.020
"""

def run(prefix=os.path.basename(__file__).replace(".py","_varref")):
  """
  Basic test. Just make sure it runs.
  """
  # model
  pdb_file = "%s.pdb"%prefix
  with open(pdb_file, "w") as fo:
    fo.write(pdb_str)
  # restraints cif
  cif_file = "%s.cif"%prefix
  with open(cif_file, "w") as fo:
    fo.write(cif_str)
  #
  map_files = []
  for i in [1,2,3]:
    cmd = " ".join([
      "phenix.model_map",
      pdb_file,
      "output_file_name_prefix=%s_%s"%(prefix,i),
      "grid_step=1",
      ">%s.zlog"%prefix])
    assert easy_run.call(cmd)==0
    map_files.append( "%s_%s.ccp4"%(prefix,i) )
  #
  args = [pdb_file, cif_file] + map_files + [\
    "resolution=3",
    "models_per_map=1",
    "ramachandran_plot_restraints.enabled=False",
    "macro_cycles=0",
    "shake_amplitude=0.1"]
  old_stdout = sys.stdout
  logger = multi_out()
  logger.register('stdout', sys.stdout)
  logger.register('parser_log', StringIO())
  f = open("%s.zlog"%prefix, 'w')
  logger.register('parser_log', f)
  result = run_program(program_class=varref.Program, args=args, logger=logger)
  logger.close()
  sys.stdout = old_stdout
  assert result is not None and len(result) == 3

  # repeat with nproc
  args.append('nproc=3')
  logger = multi_out()
  logger.register('stdout', sys.stdout)
  logger.register('parser_log', StringIO())
  f = open("%s.zlog"%prefix, 'w')
  logger.register('parser_log', f)
  result = run_program(program_class=varref.Program, args=args, logger=logger)
  logger.close()
  sys.stdout = old_stdout
  assert result is not None and len(result) == 3

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
