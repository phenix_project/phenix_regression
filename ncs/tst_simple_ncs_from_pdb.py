from __future__ import division
from __future__ import print_function
from libtbx import easy_run
from libtbx.test_utils import assert_lines_in_file
import time
import sys
from phenix.command_line import simple_ncs_from_pdb
import iotbx.pdb

pdb_str_1 = """\
ATOM      1  N   GLY A   1      42.375 -12.180  24.780  1.00 35.31
ATOM      2  CA  GLY A   1      43.603 -11.488  24.325  1.00 35.57
ATOM      3  C   GLY A   1      43.288 -10.171  23.615  1.00 34.64
ATOM      4  O   GLY A   1      42.111  -9.896  23.277  1.00 35.82
ATOM      5  N   ILE A   2      44.323  -9.391  23.299  1.00 32.23
ATOM      6  CA  ILE A   2      44.200  -8.183  22.475  1.00 27.55
ATOM      7  C   ILE A   2      43.750  -8.629  21.119  1.00 24.92
ATOM      8  O   ILE A   2      43.068  -7.904  20.409  1.00 23.73
ATOM      9  CB  ILE A   2      45.525  -7.320  22.425  1.00 30.10
ATOM     10  CG1 ILE A   2      45.924  -6.837  23.820  1.00 29.64
ATOM     11  CG2 ILE A   2      45.555  -6.173  21.386  1.00 30.54
ATOM     12  CD1 ILE A   2      44.837  -6.338  24.762  1.00 32.44
ATOM     13  N   GLY A   3      44.161  -9.867  20.749  1.00 22.69
ATOM     14  CA  GLY A   3      43.999 -10.264  19.329  1.00 21.05
ATOM     15  C   GLY A   3      42.433 -10.405  19.166  1.00 22.08
ATOM     16  O   GLY A   3      41.912 -10.061  18.096  1.00 22.86
ATOM     17  N   ALA A   4      41.862 -10.961  20.191  1.00 21.60
ATOM     18  CA  ALA A   4      40.378 -11.260  20.106  1.00 21.80
ATOM     19  C   ALA A   4      39.584  -9.950  20.087  1.00 21.67
ATOM     20  O   ALA A   4      38.676  -9.747  19.278  1.00 22.21
ATOM     21  CB  ALA A   4      40.061 -12.080  21.350  1.00 22.97
ATOM     22  N   VAL A   5      39.936  -9.001  20.956  1.00 22.13
ATOM     23  CA  VAL A   5      39.355  -7.658  21.083  1.00 19.34
ATOM     24  C   VAL A   5      39.536  -6.896  19.795  1.00 18.81
ATOM     25  O   VAL A   5      38.626  -6.314  19.126  1.00 17.22
ATOM     26  CB  VAL A   5      39.843  -6.933  22.338  1.00 20.40
ATOM     27  CG1 VAL A   5      39.237  -5.519  22.413  1.00 23.06
ATOM     28  CG2 VAL A   5      39.745  -7.587  23.653  1.00 21.67
"""

pdb_str_2 = """\
ATOM      2  CA  GLY A   1      43.603 -11.488  24.325  1.00 35.57      2MLT 113
ATOM      6  CA  ILE A   2      44.200  -8.183  22.475  1.00 27.55      2MLT 117
ATOM     14  CA  GLY A   3      43.999 -10.264  19.329  1.00 21.05      2MLT 125
ATOM     18  CA  ALA A   4      40.378 -11.260  20.106  1.00 21.80      2MLT 129
ATOM     23  CA  VAL A   5      39.355  -7.658  21.083  1.00 19.34      2MLT 134
ATOM     30  CA  LEU A   6      41.062  -6.432  17.957  1.00 17.59      2MLT 141
ATOM     38  CA  LYS A   7      39.079  -8.646  15.636  1.00 22.55      2MLT 149
ATOM     47  CA  VAL A   8      35.792  -7.369  17.211  1.00 20.52      2MLT 158
ATOM     54  CA  LEU A   9      36.899  -3.759  16.725  1.00 16.83      2MLT 165
ATOM     62  CA  THR A  10      37.338  -4.508  13.084  1.00 19.41      2MLT 173
ATOM     69  CA  THR A  11      34.132  -6.405  12.343  1.00 24.14      2MLT 180
ATOM     76  CA  GLY A  12      31.584  -6.595  15.140  1.00 24.17      2MLT 187
ATOM     80  CA  LEU A  13      31.923  -2.919  16.364  1.00 23.24      2MLT 191
ATOM     88  CA  PRO A  14      31.026  -1.278  13.030  1.00 17.52      2MLT 199
ATOM     95  CA  ALA A  15      27.822  -3.418  12.724  1.00 17.10      2MLT 206
ATOM    100  CA  LEU A  16      26.958  -2.649  16.351  1.00 18.20      2MLT 211
ATOM    108  CA  ILE A  17      27.343   1.056  15.618  1.00 20.41      2MLT 219
ATOM    116  CA  SER A  18      24.825   0.827  12.744  1.00 19.98      2MLT 227
ATOM    122  CA  TRP A  19      22.492  -1.085  15.081  1.00 15.72      2MLT 233
ATOM    136  CA  ILE A  20      22.628   1.633  17.766  1.00 15.67      2MLT 247
ATOM    144  CA  LYS A  21      21.888   4.236  15.157  1.00 19.84      2MLT 255
ATOM    153  CA  ARG A  22      18.740   2.273  14.020  1.00 20.38      2MLT 264
ATOM    164  CA  LYS A  23      17.500   1.928  17.550  1.00 22.62      2MLT 275
ATOM    173  CA  ARG A  24      18.059   5.674  18.276  1.00 27.11      2MLT 284
ATOM    184  CA  GLN A  25      15.836   6.730  15.339  1.00 37.50      2MLT 295
ATOM    193  CA  GLN A  26      13.132   4.360  16.583  1.00 46.66      2MLT 304
ATOM    204  CA  GLY B 101      26.196  11.215  25.772  1.00 31.28      2MLT 315
ATOM    208  CA  ILE B 102      25.695   8.457  23.127  1.00 26.61      2MLT 319
ATOM    216  CA  GLY B 103      25.775  10.608  19.984  1.00 20.83      2MLT 327
ATOM    220  CA  ALA B 104      29.261  12.192  20.662  1.00 22.19      2MLT 331
ATOM    225  CA  VAL B 105      30.560   8.605  21.561  1.00 20.43      2MLT 336
ATOM    232  CA  LEU B 106      29.339   7.258  18.306  1.00 15.03      2MLT 343
ATOM    240  CA  LYS B 107      30.789  10.122  16.413  1.00 20.35      2MLT 351
ATOM    249  CA  VAL B 108      34.238   9.637  18.027  1.00 23.38      2MLT 360
ATOM    256  CA  LEU B 109      34.150   5.918  17.183  1.00 20.22      2MLT 367
ATOM    264  CA  THR B 110      33.081   6.344  13.562  1.00 23.85      2MLT 375
ATOM    271  CA  THR B 111      35.833   8.796  12.896  1.00 26.41      2MLT 382
ATOM    278  CA  GLY B 112      38.482   7.602  15.318  1.00 22.53      2MLT 389
ATOM    282  CA  LEU B 113      38.268   3.810  15.278  1.00 23.61      2MLT 393
ATOM    290  CA  PRO B 114      39.945   3.149  11.872  1.00 20.15      2MLT 401
ATOM    297  CA  ALA B 115      43.145   4.994  13.009  1.00 22.26      2MLT 408
ATOM    302  CA  LEU B 116      43.070   3.167  16.352  1.00 18.86      2MLT 413
ATOM    310  CA  ILE B 117      42.952  -0.212  14.601  1.00 14.88      2MLT 421
ATOM    318  CA  SER B 118      45.891   0.689  12.345  1.00 17.71      2MLT 429
ATOM    324  CA  TRP B 119      47.907   1.909  15.276  1.00 17.53      2MLT 435
ATOM    338  CA  ILE B 120      47.296  -1.149  17.359  1.00 14.07      2MLT 449
ATOM    346  CA  LYS B 121      48.314  -3.437  14.512  1.00 20.04      2MLT 457
ATOM    355  CA  ARG B 122      51.517  -1.513  14.068  1.00 25.56      2MLT 466
ATOM    366  CA  LYS B 123      52.367  -1.651  17.742  1.00 23.85      2MLT 477
ATOM    375  CA  ARG B 124      51.670  -5.431  17.781  1.00 28.09      2MLT 486
ATOM    386  CA  GLN B 125      54.153  -6.012  14.932  1.00 40.40      2MLT 497
ATOM    395  CA  GLN B 126      56.818  -4.124  16.883  1.00 45.23      2MLT 506
  """
pdb_str_3 = """\
CRYST1  203.106   83.279  178.234  90.00 106.67  90.00 C 1 2 1      12
ATOM      1  N   ASP A   5      91.286 -31.834  73.572  1.00 77.83           N
ATOM      2  CA  ASP A   5      90.511 -32.072  72.317  1.00 78.04           C
ATOM      3  C   ASP A   5      90.136 -30.762  71.617  1.00 77.70           C
ATOM      4  O   ASP A   5      89.553 -29.857  72.225  1.00 77.56           O
ATOM      9  N   THR A   6      91.286 -31.834  73.572  1.00 77.83           N
ATOM     10  CA  THR A   6      90.511 -32.072  72.317  1.00 78.04           C
TER
ATOM   2517  N   GLY B 501      91.286 -31.834  73.572  1.00 77.83           N
ATOM   2518  CA  GLY B 501      90.511 -32.072  72.317  1.00 78.04           C
ATOM   2519  C   GLY B 501      90.136 -30.762  71.617  1.00 77.70           C
ATOM   2520  O   GLY B 501      89.553 -29.857  72.225  1.00 77.56           O
TER
ATOM   3802  N   ASP D   5      92.487   3.543  81.144  1.00 70.91           N
ATOM   3803  CA  ASP D   5      93.100   3.556  79.781  1.00 70.52           C
ATOM   3804  C   ASP D   5      92.161   2.961  78.728  1.00 70.38           C
ATOM   3805  O   ASP D   5      91.661   1.839  78.880  1.00 69.56           O
ATOM   3810  N   THR D   6      92.487   3.543  81.144  1.00 70.91           N
ATOM   3811  CA  THR D   6      93.100   3.556  79.781  1.00 70.52           C
TER
ATOM   6318  N   GLY E 501      92.487   3.543  81.144  1.00 70.91           N
ATOM   6319  CA  GLY E 501      93.100   3.556  79.781  1.00 70.52           C
ATOM   6320  C   GLY E 501      92.161   2.961  78.728  1.00 70.38           C
ATOM   6321  O   GLY E 501      91.661   1.839  78.880  1.00 69.56           O
TER
ATOM   3953  N   ARGAd   6     314.882 195.854 106.123  1.00 50.00           N
ATOM   3954  CA  ARGAd   6     313.875 195.773 107.215  1.00 50.00           C
ATOM   3955  C   ARGAd   6     313.239 197.116 107.471  1.00 50.00           C
ATOM   3956  O   ARGAd   6     313.460 198.078 106.736  1.00 50.00           O
TER
ATOM   5463  N   PHEAe   6     261.525 165.024 118.275  1.00 50.00           N
ATOM   5464  CA  PHEAe   6     260.185 165.283 118.746  1.00 50.00           C
ATOM   5465  C   PHEAe   6     259.365 164.014 118.787  1.00 50.00           C
ATOM   5466  O   PHEAe   6     258.673 163.762 119.769  1.00 50.00           O
TER
ATOM   6662  N   ARGAf   6     313.035 232.818 109.051  1.00 50.00           N
ATOM   6663  CA  ARGAf   6     312.124 232.379 110.143  1.00 50.00           C
ATOM   6664  C   ARGAf   6     311.048 233.405 110.399  1.00 50.00           C
ATOM   6665  O   ARGAf   6     310.909 234.383 109.665  1.00 50.00           O
"""

def exercise_1(prefix="tst_simple_ncs_from_pdb_1"):
  f = open("%s.pdb" % prefix, "w")
  f.write(pdb_str_1)
  f.close()
  cmd = " ".join([
      "phenix.simple_ncs_from_pdb",
      "%s.pdb" % prefix,
      ">%s.log" % prefix
      ])
  print(cmd)
  assert not easy_run.call(cmd)
  assert_lines_in_file(
    file_name="%s.log" % prefix,
    lines="  No NCS relation were found !!!")

def exercise_2(prefix="tst_simple_ncs_from_pdb_2"):
  log = sys.stdout
  expected_result="""\
ncs_group {
  reference = chain 'A'
  selection = chain 'B'
}"""
  file_name='%s.pdb' % prefix
  f=open(file_name,'w')
  f.write(pdb_str_2)
  f.close()
  args=[file_name]
  args.append("chain_similarity_threshold=0.10")
  args.append("write_ncs_phil=True")
  run_simple_ncs_from_pdb=simple_ncs_from_pdb.run(args=args,log=log)
  assert_lines_in_file(
      file_name='%s_simple_ncs_from_pdb.phil' % prefix,
      lines=expected_result)

def exercise_3(prefix="tst_simple_ncs_from_pdb_3"):
  pdb_str = """\
ATOM      7  O   HOH A   2      12.635   9.340   2.565  1.00 10.00           O
"""
  f = open("%s.pdb" % prefix, "w")
  f.write(pdb_str)
  f.close()
  cmd = " ".join([
      "phenix.simple_ncs_from_pdb",
      "%s.pdb" % prefix,
      ">%s.log" % prefix
      ])
  print(cmd)
  assert not easy_run.call(cmd)
  assert_lines_in_file(
    file_name="%s.log" % prefix,
    lines="No NCS found from the chains")

def exercise_4(prefix="tst_simple_ncs_from_pdb_4"):
  """
  """
  fn = prefix+".pdb"
  open(fn,'w').write(pdb_str_3)
  stem = 'test_create_ncs_domain_pdb_files_'
  obj = simple_ncs_from_pdb.run(
    args=["pdb_in=%s" % fn,
          "write_ncs_domain_pdb=True",
          "ncs_domain_pdb_stem=%s" % stem])

  fn_gr0 = stem + '_group_1.pdb'
  fn_gr1 = stem + '_group_2.pdb'
  fn_gr2 = stem + '_group_3.pdb'

  assert obj.ncs_obj.number_of_ncs_groups == 3
  pdb_inp_0 = iotbx.pdb.input(file_name=fn_gr0)
  pdb_inp_1 = iotbx.pdb.input(file_name=fn_gr1)
  pdb_inp_2 = iotbx.pdb.input(file_name=fn_gr2)
  assert pdb_inp_0.atoms().size() == 12
  assert pdb_inp_1.atoms().size() == 8
  assert pdb_inp_2.atoms().size() == 8

if (__name__ == "__main__"):
  t0 = time.time()
  exercise_1()
  exercise_2()
  exercise_3()
  exercise_4()
  print("OK time =%8.3f"%(time.time() - t0))
