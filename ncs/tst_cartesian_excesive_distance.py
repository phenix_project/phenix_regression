from __future__ import division
from __future__ import print_function

from libtbx import easy_run

import os, sys

pdb_str = """\
CRYST1   44.230   58.595   47.188  90.00  90.00  90.00 P 1
SCALE1      0.022609  0.000000  0.000000        0.00000
SCALE2      0.000000  0.017066  0.000000        0.00000
SCALE3      0.000000  0.000000  0.021192        0.00000
ATOM      1  N   PRO A   1      18.825  52.493   7.758  1.00 28.41           N
ATOM      2  CA  PRO A   1      18.508  52.372   6.323  1.00 26.16           C
ATOM      3  C   PRO A   1      18.004  50.998   5.924  1.00 19.76           C
ATOM      4  O   PRO A   1      17.188  50.882   5.000  1.00 08.30           O
ATOM      5  CB  PRO A   1      19.844  52.694   5.643  1.00 31.26           C
ATOM      6  CG  PRO A   1      20.526  53.595   6.587  1.00 34.22           C
ATOM      7  CD  PRO A   1      20.141  53.130   7.962  1.00 28.84           C
ATOM      8  N   ASN A   2      18.474  49.947   6.595  1.00 39.84           N
ATOM      9  CA  ASN A   2      18.014  48.600   6.281  1.00 55.18           C
ATOM     10  C   ASN A   2      16.573  48.377   6.726  1.00 53.00           C
ATOM     11  O   ASN A   2      15.836  47.629   6.073  1.00 56.43           O
ATOM     12  CB  ASN A   2      18.953  47.572   6.919  1.00 69.26           C
ATOM     13  CG  ASN A   2      18.416  46.159   6.837  1.00 70.96           C
ATOM     14  OD1 ASN A   2      18.326  45.457   7.779  1.00 69.01           O
ATOM     15  ND2 ASN A   2      18.087  45.763   5.707  1.00 69.05           N
ATOM     16  N   VAL A   3      16.157  49.007   7.828  1.00 42.14           N
ATOM     17  CA  VAL A   3      14.770  48.895   8.274  1.00 38.71           C
ATOM     18  C   VAL A   3      13.818  49.491   7.246  1.00 30.59           C
ATOM     19  O   VAL A   3      12.707  48.983   7.045  1.00 16.93           O
ATOM     20  CB  VAL A   3      14.598  49.539   9.665  1.00 44.39           C
ATOM     21  CG1 VAL A   3      13.145  49.473  10.108  1.00 49.90           C
ATOM     22  CG2 VAL A   3      15.498  48.858  10.683  1.00 51.07           C
TER
ATOM     23  N   PRO B   1       5.576  24.356  29.098  1.00 16.59           N
ATOM     24  CA  PRO B   1       6.849  23.972  29.735  1.00 12.34           C
ATOM     25  C   PRO B   1       7.915  25.048  29.661  1.00 11.02           C
ATOM     26  O   PRO B   1       8.750  25.155  30.570  1.00 03.75           O
ATOM     27  CB  PRO B   1       7.263  22.719  28.953  1.00 19.57           C
ATOM     28  CG  PRO B   1       5.990  22.129  28.503  1.00 19.26           C
ATOM     29  CD  PRO B   1       5.075  23.285  28.213  1.00 11.11           C
ATOM     30  N   ASN B   2       7.914  25.851  28.597  1.00 18.69           N
ATOM     31  CA  ASN B   2       8.897  26.919  28.473  1.00 31.44           C
ATOM     32  C   ASN B   2       8.625  28.052  29.457  1.00 28.70           C
ATOM     33  O   ASN B   2       9.568  28.693  29.936  1.00 30.19           O
ATOM     34  CB  ASN B   2       8.921  27.430  27.030  1.00 40.01           C
ATOM     35  CG  ASN B   2       9.734  28.695  26.871  1.00 47.49           C
ATOM     36  OD1 ASN B   2       9.310  29.666  26.355  1.00 41.36           O
ATOM     37  ND2 ASN B   2      10.896  28.654  27.301  1.00 52.38           N
ATOM     38  N   VAL B   3       7.353  28.313  29.767  1.00 23.06           N
ATOM     39  CA  VAL B   3       7.020  29.337  30.755  1.00 23.36           C
ATOM     40  C   VAL B   3       7.562  28.963  32.127  1.00 14.53           C
ATOM     41  O   VAL B   3       7.991  29.835  32.894  1.00 09.24           O
ATOM     42  CB  VAL B   3       5.498  29.580  30.785  1.00 33.16           C
ATOM     43  CG1 VAL B   3       5.141  30.610  31.847  1.00 36.05           C
ATOM     44  CG2 VAL B   3       5.000  30.014  29.418  1.00 42.06           C
TER
ATOM     45  N   PRO C   1      37.846  32.992   8.327  1.00 30.00           N
ATOM     46  CA  PRO C   1      37.366  32.920   6.935  1.00 22.60           C
ATOM     47  C   PRO C   1      36.991  31.523   6.488  1.00 20.46           C
ATOM     48  O   PRO C   1      36.081  31.356   5.664  1.00 17.63           O
ATOM     49  CB  PRO C   1      38.550  33.466   6.130  1.00 26.70           C
ATOM     50  CG  PRO C   1      39.230  34.388   7.061  1.00 29.59           C
ATOM     51  CD  PRO C   1      39.086  33.787   8.426  1.00 27.70           C
ATOM     52  N   ASN C   2      37.674  30.506   7.007  1.00 30.63           N
ATOM     53  CA  ASN C   2      37.344  29.132   6.636  1.00 42.17           C
ATOM     54  C   ASN C   2      36.012  28.684   7.233  1.00 40.48           C
ATOM     55  O   ASN C   2      35.292  27.891   6.613  1.00 39.56           O
ATOM     56  CB  ASN C   2      38.474  28.197   7.076  1.00 51.42           C
ATOM     57  CG  ASN C   2      38.120  26.727   6.936  1.00 50.25           C
ATOM     58  OD1 ASN C   2      38.275  25.953   7.809  1.00 42.19           O
ATOM     59  ND2 ASN C   2      37.683  26.377   5.845  1.00 49.53           N
ATOM     60  N   VAL C   3      35.663  29.183   8.424  1.00 37.59           N
ATOM     61  CA  VAL C   3      34.372  28.854   9.024  1.00 28.18           C
ATOM     62  C   VAL C   3      33.225  29.382   8.174  1.00 11.43           C
ATOM     63  O   VAL C   3      32.170  28.743   8.070  1.00 06.27           O
ATOM     64  CB  VAL C   3      34.301  29.382  10.472  1.00 32.54           C
ATOM     65  CG1 VAL C   3      32.935  29.104  11.071  1.00 38.14           C
ATOM     66  CG2 VAL C   3      35.399  28.758  11.321  1.00 48.78           C
TER
ATOM     67  N   PRO D   1      30.702   7.113  38.403  1.00 65.80           N
ATOM     68  CA  PRO D   1      32.095   6.881  38.823  1.00 65.15           C
ATOM     69  C   PRO D   1      32.988   8.097  38.677  1.00 66.53           C
ATOM     70  O   PRO D   1      33.930   8.272  39.459  1.00 64.49           O
ATOM     71  CB  PRO D   1      32.549   5.737  37.907  1.00 73.90           C
ATOM     72  CG  PRO D   1      31.314   5.000  37.605  1.00 70.11           C
ATOM     73  CD  PRO D   1      30.221   6.026  37.526  1.00 69.40           C
ATOM     74  N   ASN D   2      32.725   8.952  37.695  1.00 58.79           N
ATOM     75  CA  ASN D   2      33.552  10.138  37.521  1.00 59.10           C
ATOM     76  C   ASN D   2      33.264  11.187  38.592  1.00 59.86           C
ATOM     77  O   ASN D   2      34.162  11.948  38.969  1.00 68.24           O
ATOM     78  CB  ASN D   2      33.355  10.684  36.108  1.00 67.27           C
ATOM     79  CG  ASN D   2      33.909  12.088  35.928  1.00 74.95           C
ATOM     80  OD1 ASN D   2      34.751  12.491  36.454  1.00 72.92           O
ATOM     81  ND2 ASN D   2      33.404  12.723  35.186  1.00 78.55           N
ATOM     82  N   VAL D   3      32.024  11.249  39.086  1.00 57.54           N
ATOM     83  CA  VAL D   3      31.709  12.168  40.176  1.00 59.39           C
ATOM     84  C   VAL D   3      32.493  11.809  41.432  1.00 52.15           C
ATOM     85  O   VAL D   3      32.914  12.695  42.188  1.00 51.32           O
ATOM     86  CB  VAL D   3      30.189  12.205  40.434  1.00 70.37           C
ATOM     87  CG1 VAL D   3      29.868  13.117  41.606  1.00 73.91           C
ATOM     88  CG2 VAL D   3      29.448  12.658  39.187  1.00 71.17           C
TER
END
"""

params_str = """\
refinement.main.number_of_macro_cycles=0
refinement.ncs.type=cartesian
refinement.ncs.excessive_distance_limit = 1
refinement.pdb_interpretation.ncs_search.enabled=True
refinement.pdb_interpretation.ncs_group {
  reference = "chain A"
  selection = "chain B"
  selection = "chain C"
  selection = "chain D"
}
"""

def preparation(prefix="tst_cartesian_excessive_dist"):
  pdb_fname = "%s.pdb" % prefix
  mtz_fname = "%s.pdb.mtz" % prefix
  params_name = "%s.eff" % prefix
  with open(pdb_fname, 'w') as f:
    f.write(pdb_str)
  with open("%s.eff" % prefix, 'w') as f:
    f.write(params_str)

  cmd = " ".join([
      "phenix.fmodel",
      "%s" % pdb_fname,
      "r_free_flags_fraction=0.1",
      "high_resolution=3",
      "label='FOBS'",
      "type=real",])
  print(cmd)
  assert not easy_run.call(cmd)
  return pdb_fname, mtz_fname, params_name

def ex1(pdb_fname, mtz_fname, params_name, prefix="tst_cartesian_excessive_dist_1"):
  """
  We want to get .geo file even if we are failing
  """
  cmd = " ".join([
      "phenix.refine",
      pdb_fname,
      mtz_fname,
      params_name,
      "output.prefix=1",
      ])
  print(cmd)
  easy_run.fully_buffered(cmd)
  geo_fname = "1_001.geo"
  assert os.path.isfile(geo_fname) # rightfully fails with Sorry
  lines = open(geo_fname, 'r').readlines()
  assert len(lines) > 0
  n_exc = 0
  for l in lines:
    if l.find("EXCESSIVE") > 0:
      n_exc += 1
  assert n_exc == 2, n_exc
  print("OK")

def ex2(pdb_fname, mtz_fname, params_name, prefix="tst_cartesian_excessive_dist_2"):
  """
  We want the failure even if the .geo is not requested.
  """
  pdb_fname, mtz_fname, params_name = preparation()
  cmd = " ".join([
      "phenix.refine",
      pdb_fname,
      mtz_fname,
      params_name,
      "write_geo_file=False",
      "output.prefix=2",
      ])
  print(cmd)
  easy_run.fully_buffered(cmd) # rightfully fails with Sorry
  geo_fname = "2_001.geo"
  assert not os.path.isfile(geo_fname)
  print("OK")

if (__name__ == "__main__"):
  pdb_fname, mtz_fname, params_name = preparation()
  ex1(pdb_fname, mtz_fname, params_name)
  ex2(pdb_fname, mtz_fname, params_name)
