from __future__ import absolute_import, division, print_function

self.write_dispatcher(
  source_dir=self.env.under_dist("phenix_regression", "phenix_doc"),
  file_name="test_phenix_html.csh",
  suppress_warning=False,
  target_file_name_infix=".phenix_doc")
for infix,file_name in [
      (".wizards", "test_all_parallel.py"),
      (".wizards", "list.py"),
      (".wizards", "update_wizard_tests.py"),
      (".wizards", "run_wizard_test.py"),
      (".wizards", "compare_files.py"),
      ]:
  self.write_dispatcher(
    source_dir=self.env.under_dist("phenix_regression", "wizards"),
    file_name=file_name,
    suppress_warning=False,
    target_file_name_infix=infix)
