from __future__ import print_function
import os, sys
import hashlib
import libtbx.load_env
from libtbx import easy_run
from libtbx.test_utils import approx_equal

preamble = "cdl_test"

def run():
  d = os.path.join(libtbx.env.dist_path("phenix_regression"), "restraints")
  cmd = "phenix.geometry_minimization %s.pdb cdl=True fix_rotamer_outliers=False correct_hydrogens=False> %s.log" % (
    os.path.join(d, preamble),
    preamble,
    )
  print(cmd)
  assert not easy_run.call(cmd)
  f = open("%s_minimized.geo" % preamble, "r")
  split_lines = f.read().splitlines()
  f.close()
  expected_values = [1.333, 1.333,  0.0001, 0.0127, 6200.0, 0.000508]
  #expected_values = [1.334, 1.334, 0.0001, 0.0132, 5740.0, 0.00028]
  indices_of_values_to_test = [1,1,0,1,1,0]
  eps = 0.001
  for i,l in enumerate(split_lines):
    if 'bond pdb=" C   ASN A  18 ' in l:
      if 'pdb=" N   VAL A  19 "' in split_lines[i+1]:
        data =  map(float,split_lines[i+3].split())
        # make sure all values are within eps from expected
        test_set = zip(expected_values,data,indices_of_values_to_test)
        test = [(use*abs(x-y)/x <= eps) for (x,y,use) in test_set]
        assert test.count(False) == 0, "%d out of %d" %(test.count(False), len(test))
        break
  # Removing unclear part of test. Don't see any justification to measure
  # bond residual after the first macro-cycle. Makes more sence to check
  # starting values.

  f = open("%s.log" % preamble, "r")
  split_lines=f.read().splitlines()
  for i, l in enumerate(split_lines):
    if l.startswith("Minimization"):
      assert split_lines[i+3].find("bond_residual_sum (n=45)") > 0
      # assert approx_equal(float(split_lines[i+3].split(":")[-1]), 39.54, eps=0.1)
      assert approx_equal(float(split_lines[i+3].split(":")[-1]), 6.14, eps=0.1)
      assert split_lines[i+4].find("nonbonded_residual_sum (n=304)") > 0
      assert approx_equal(float(split_lines[i+4].split(":")[-1]), 5.13, eps=0.1)
      assert split_lines[i+5].find("angle_residual_sum (n=80)") > 0
      # assert approx_equal(float(split_lines[i+5].split(":")[-1]), 47.82, eps=1.)
      assert approx_equal(float(split_lines[i+5].split(":")[-1]), 39.22, eps=1.)
      # assert split_lines[i+6].find("dihedral_residual_sum (n=17)") > 0
      assert split_lines[i+6].find("dihedral_residual_sum (n=20)") > 0
      assert approx_equal(float(split_lines[i+6].split(":")[-1]), 26.64, eps=1.)
      assert split_lines[i+7].find("chirality_residual_sum (n=5)") > 0
      assert approx_equal(float(split_lines[i+7].split(":")[-1]), 2.575, eps=0.1)
      assert split_lines[i+8].find("planarity_residual_sum (n=5)") > 0
      assert approx_equal(float(split_lines[i+8].split(":")[-1]), 0.97, eps=0.1)
      assert split_lines[i+9].find("parallelity_residual_sum (n=0): 0") > 0
  f.close()

if __name__=="__main__":
  run()
