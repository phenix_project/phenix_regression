from __future__ import division
from __future__ import print_function
from libtbx import easy_run
from libtbx.test_utils import assert_lines_in_file
import sys

pdb1 = """
CRYST1   10.000   10.000   10.000  90.00  90.00  90.00 P 1           1
ATOM      1  N   ALA A   2      -7.656   2.923   3.155  1.00 15.02           N
ATOM      2  CA  ALA A   2      -6.522   2.038   2.831  1.00 14.10           C
ATOM      3  C   ALA A   2      -5.241   2.537   3.427  1.00 13.13           C
ATOM      4  O   ALA A   2      -4.978   3.742   3.426  1.00 11.91           O
ATOM      5  CB  ALA A   2      -6.346   1.881   1.341  1.00 15.38           C
ATOM      6  H   ALA A   2      -8.111   3.328   2.362  1.00 15.02           H
ATOM      7  N   ALA A   3      -4.438   1.590   3.905  1.00 12.26           N
ATOM      8  CA  ALA A   3      -3.193   1.904   4.589  1.00 11.74           C
ATOM      9  C   ALA A   3      -1.955   1.332   3.895  1.00 11.10           C
ATOM     10  O   ALA A   3      -1.872   0.119   3.648  1.00 10.42           O
ATOM     11  CB  ALA A   3      -3.259   1.378   6.042  1.00 12.15           C
ATOM     12  H   ALA A   3      -4.629   0.611   3.830  1.00 12.26           H
ATOM     13  N   ALA A   4      -1.005   2.228   3.598  1.00 10.29           N
ATOM     14  CA  ALA A   4       0.384   1.888   3.199  1.00 10.53           C
ATOM     15  C   ALA A   4       1.435   2.606   4.088  1.00 10.24           C
ATOM     16  O   ALA A   4       1.547   3.843   4.115  1.00  8.86           O
ATOM     17  CB  ALA A   4       0.656   2.148   1.711  1.00  9.80           C
ATOM     18  H   ALA A   4      -1.172   3.214   3.626  1.00 10.29           H
END
"""

def exercise1():
  # Test the addition of ramachandran restraints in phenix.refine
  f = open("ramachandran_test.pdb",'w')
  f.write(pdb1)
  f.close()
  cmd = "phenix.fmodel ramachandran_test.pdb high_resolution=3 "
  cmd += "r_free_flags_fraction=0.1 type=real"
  res = easy_run.fully_buffered(command=cmd)
  cmd = "phenix.refine ramachandran_test.pdb ramachandran_test.pdb.mtz "
  cmd += "ramachandran_plot_restraints.enabled=True main.number_of_macro_cycles=0 overwrite=true"
  print(cmd)
  res = easy_run.call(command=cmd)
  assert_lines_in_file("ramachandran_test_refine_001.log",
      "2 Ramachandran restraints generated.")

def exercise2():
  # Test the addition of ramachandran restraints in phenix.refine
  f = open("ramachandran_test.pdb",'w')
  f.write(pdb1)
  f.close()
  cmd = "phenix.fmodel ramachandran_test.pdb high_resolution=3 "
  cmd += "r_free_flags_fraction=0.1 type=real"
  res = easy_run.fully_buffered(command=cmd)
  cmd = "phenix.refine ramachandran_test.pdb ramachandran_test.pdb.mtz "
  cmd += "ramachandran_plot_restraints.enabled=True main.number_of_macro_cycles=0 overwrite=true "
  cmd += "ramachandran_plot_restraints.outlier=None"
  print(cmd)
  res = easy_run.call(command=cmd)
  assert_lines_in_file("ramachandran_test_refine_001.log",
      """2 Ramachandran restraints generated.
      1 Oldfield, 0 Emsley, 1 emsley8k and 0 Phi/Psi/2.""")

def exercise3():
  # Test the addition of phi-psi-2 ramachandran restraints in phenix.refine
  f = open("ramachandran_test.pdb",'w')
  f.write(pdb1)
  f.close()
  cmd = "phenix.fmodel ramachandran_test.pdb high_resolution=3 "
  cmd += "r_free_flags_fraction=0.1 type=real"
  res = easy_run.fully_buffered(command=cmd)
  cmd = "phenix.refine ramachandran_test.pdb ramachandran_test.pdb.mtz "
  cmd += " ramachandran_plot_restraints.enabled=True main.number_of_macro_cycles=0 overwrite=true "
  cmd += " ramachandran_plot_restraints.favored=phi_psi_2"
  cmd += " ramachandran_plot_restraints.allowed=phi_psi_2"
  cmd += " ramachandran_plot_restraints.outlier=phi_psi_2"
  print(cmd)
  res = easy_run.call(command=cmd)
  assert_lines_in_file("ramachandran_test_refine_001.log",
      """1 Ramachandran restraints generated.
      0 Oldfield, 0 Emsley, 0 emsley8k and 1 Phi/Psi/2.""")


def run(args):
  exercise1()
  exercise2()
  exercise3()

if __name__=="__main__":
    run(sys.argv[1:])
