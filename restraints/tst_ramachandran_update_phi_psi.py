from __future__ import division
from __future__ import print_function
from libtbx import easy_run
import sys, os
import libtbx.load_env

def exercise_1(prefix="tst_ramachandran_update_phi_psi"):
  file_name = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/3ifk.pdb",
    test=os.path.isfile)
  if (file_name is None) :
    print("Skipping test.")
    return

  cmd = " ".join([
    "phenix.fmodel",
    file_name,
    "high_res=3.0",
    "type=real",
    "label=F-obs",
    "r_free=0.1",
    "output.file_name=%s.mtz"%prefix,
    "> zlog"])
  print(cmd)
  assert not easy_run.call(cmd)

  cmd = " ".join([
    "phenix.refine",
    "%s.mtz"%prefix,
    file_name,
    "xray_data.r_free_flags.ignore_r_free_flags=True",
    "main.number_of_mac=3",
    "main.max_number_of_iterations=10",
    "ramachandran_plot_restraints.enabled=true",
    "ramachandran_plot_restraints.outlier=None",
    "ramachandran_plot_restraints.allowed=None",
    "write_map_coefficients=False",
    "write_eff_file=False",
    "write_geo_file=False",
    "write_def_file=False",
    "output.prefix=%s"%prefix,
    "--overwrite --quiet"])
  print(cmd)
  assert not easy_run.call(cmd)

  log = open("%s_001.log" % prefix, 'r')
  loglines = log.readlines()
  nres = []
  for l in loglines:
    if l.find("Ramachandran restraints generated") > 0:
      nres.append(int(l.split()[0]))
  assert nres[0] == 328, nres[0]


if __name__ == "__main__" :
  exercise_1()
