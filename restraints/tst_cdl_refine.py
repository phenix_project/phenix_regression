from __future__ import print_function
import os
import libtbx.load_env
from libtbx import easy_run

preamble = "cdl_test"

def run():
  d = os.path.join(libtbx.env.dist_path("phenix_regression"), "restraints")
  cmd = " ".join([
    "phenix.refine",
    "strategy=individual_sites",
    "refinement.main.number_of_macro_cycles=2",
    "cdl=True",
    "%s.pdb"%os.path.join(d, preamble),
    "%s.mtz"%os.path.join(d, preamble),
    "write_model_cif=True",
    "> %s.junk.zlog"%preamble
  ])
  print(cmd)
  assert not easy_run.call(cmd)
  f = open("%s_refine_001.pdb" % preamble, "r")
  lines=f.readlines()
  f.close()
  for line in lines:
    if line.find("bonds =")>-1:
      tmp = line.split()
      rmsd_angle = float(tmp[-1])
      assert (
        abs(rmsd_angle-1.819)<0.1 or
        abs(rmsd_angle-0.869)<0.1
        ), "Angle rmsd (%0.3f) is not 1.819 or 0.756" % rmsd_angle
  f = open("%s_refine_001.pdb" % preamble, "r")
  lines=f.read()
  f.close()
  assert lines.find("CDL V1.2")>-1
  f = open("%s_refine_001.cif" % preamble, "r")
  lines=f.read()
  f.close()
  assert lines.find("CDL v1.2")>-1
  print("OK")

if __name__=="__main__":
  run()
