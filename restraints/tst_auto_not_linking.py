from __future__ import print_function
pdbs = {('ETA A  16', 'DLE B   4') : """CRYST1   25.780   31.510   25.770  90.00  92.00  90.00 P 1 21 1
SCALE1      0.038790  0.000000  0.001355        0.00000
SCALE2      0.000000  0.031736  0.000000        0.00000
SCALE3      0.000000  0.000000  0.038828        0.00000
HETATM    1  C   FOR A   0       2.928  13.552   8.532  1.00 24.48           C
HETATM    2  O   FOR A   0       3.110  14.698   7.991  1.00 25.89           O
ATOM      3  N   VAL A   1       3.807  13.026   8.358  1.00 25.24           N
ATOM      4  CA  VAL A   1       5.198  12.428   8.264  1.00  8.40           C
ATOM      5  C   VAL A   1       5.335  11.151   7.395  1.00  7.61           C
ATOM      6  N   GLY A   2       6.188  11.215   6.370  1.00  6.15           N
ATOM      7  CA  GLY A   2       6.368  10.045   5.531  1.00  6.91           C
ATOM      8  C   GLY A   2       5.288   9.922   4.467  1.00  7.46           C
ATOM      9  O   GLY A   2       4.701  10.908   4.014  1.00 11.71           O
ATOM     10  CZ2 TRP A   9       0.567  -4.450  -1.537  1.00 14.62           C
HETATM   11  C   DLE A  14       7.130 -13.441   2.129  1.00  8.12           C
HETATM   12  O   DLE A  14       6.295 -12.534   2.099  1.00  6.91           O
ATOM     13  N   TRP A  15       6.836 -14.681   1.794  1.00  7.22           N
ATOM     14  CA  TRP A  15       5.520 -14.977   1.284  1.00  8.08           C
ATOM     15  C   TRP A  15       5.407 -16.282   1.707  1.00 30.00           C
ATOM     16  O   TRP A  15       5.853 -17.439   1.423  1.00 30.00           O
ATOM     17  CB  TRP A  15       5.643 -15.406  -0.197  1.00  9.79           C
ATOM     18  CG  TRP A  15       5.899 -14.177  -1.078  1.00  7.18           C
HETATM   19  N   ETA A  16       4.852 -16.680   2.997  1.00 30.00           N
HETATM   20  CA  ETA A  16       4.701 -17.575   3.621  1.00 30.00           C
HETATM   21  O   ETA A  16       3.075 -18.221   3.707  1.00 21.42           O
HETATM   22  CB  ETA A  16       3.551 -17.543   4.605  1.00 30.00           C
TER
ATOM     23  N   VAL B   1       8.076 -19.266   1.913  1.00  9.30           N
ATOM     24  C   VAL B   1       9.291 -17.614   3.168  1.00  8.60           C
ATOM     25  O   VAL B   1       8.865 -16.589   2.635  1.00  8.98           O
ATOM     26  C   GLY B   2       8.126 -16.632   6.062  1.00  5.12           C
ATOM     27  O   GLY B   2       7.577 -17.720   6.284  1.00  6.79           O
ATOM     28  N   ALA B   3       7.604 -15.471   6.445  1.00  6.73           N
ATOM     29  CA  ALA B   3       6.420 -15.421   7.289  1.00  5.56           C
ATOM     30  C   ALA B   3       5.453 -14.356   6.819  1.00  6.77           C
ATOM     31  O   ALA B   3       5.876 -13.294   6.317  1.00  6.17           O
HETATM   32  N   DLE B   4       4.160 -14.641   6.976  1.00  6.51           N
HETATM   33  CA  DLE B   4       3.176 -13.699   6.460  1.00  8.36           C
HETATM   34  C   DLE B   4       3.313 -13.481   4.934  1.00  7.79           C
HETATM   35  O   DLE B   4       3.610 -14.437   4.123  1.00 10.96           O
HETATM   36  CB  DLE B   4       1.748 -14.120   6.843  1.00  7.60           C
ATOM     37  CE2 TRP B  11      -1.097   0.050  -0.065  1.00 10.92           C
ATOM     38  CZ2 TRP B  11      -1.746  -0.655  -1.072  1.00 14.55           C
ATOM     39  CZ3 TRP B  11      -2.365  -2.284   0.604  1.00 14.13           C
ATOM     40  CH2 TRP B  11      -2.371  -1.818  -0.717  1.00 13.68           C
ATOM     41  O   TRP B  15       1.706  10.655   7.626  1.00  8.67           O
TER
END
""",
}

import os, sys
import time
from libtbx import easy_run
from six.moves import cStringIO as StringIO
from iotbx import pdb
from elbow.formats import refine_geo_parser

def check_links_are_not_made(pdb_filename, exclude_keys):
  t0=time.time()
  cmd = "phenix.pdb_interpretation %s" % pdb_filename
  # cmd += " secondary_structure.enabled=1"
  cmd += " write_geo_files=1"
  print(cmd)
  ero = easy_run.fully_buffered(command=cmd)
  sio = StringIO()
  err = StringIO()
  ero.show_stdout(out=sio)
  ero.show_stderr(out=err)
  #
  pdb_inp = pdb.input(pdb_filename)
  print(exclude_keys)
  for j, line in enumerate(exclude_keys):
    print('-'*80)
    print(j+1, line)
    selection1 = 'resname %s and chain %s and resid %s' % (tuple(line[0].split()))
    selection2 = 'resname %s and chain %s and resid %s' % (tuple(line[1].split()))
    selection1 = '%s %s %s' % (tuple(line[0].split()))
    selection2 = '%s %s %s' % (tuple(line[1].split()))
    print("'%s'" % selection1)
    print("'%s'" % selection2)
    print(os.path.basename(pdb_filename))
    cmd = '\n\telbow.refine_geo_display %s.geo "%s"\n' % (
      os.path.basename(pdb_filename),
      'top=5',
      )
    print(cmd)
    rc = refine_geo_parser.run("%s.geo" % os.path.basename(pdb_filename),
                               selection1 = selection1,
                               selection2 = '!',
                               return_bonds = True,
                               exclude_duplicate_symmetry = True,
      )
    print(rc)
    assert len(rc)==1
    rc = refine_geo_parser.run("%s.geo" % os.path.basename(pdb_filename),
                               selection1 = selection1,
                               selection2 = selection2,
                               return_bonds = True,
                               exclude_duplicate_symmetry = True,
      )
    print(rc)
    assert len(rc)==0
  return [pdb_filename, j+1, time.time()-t0]

def run(only_i=None):
  print('pdbs',len(pdbs))
  try: only_i=int(only_i)
  except: only_i=None
  t0=time.time()
  rcs = []
  for i, (key, lines) in enumerate(pdbs.items()):
    if only_i is not None and only_i!=i+1: continue
    pdb_filename = "auto_not_link_test_%03d.pdb" % (i+1)
    f = open(pdb_filename, "w")
    f.write(lines)
    f.close()
    rc = check_links_are_not_made(pdb_filename, [key])
    print(rc)
    rcs.append(rc)

  print("\nResults : %0.1fs" % (time.time()-t0))
  for rc in rcs:
    print("%s %d %0.1f" % tuple(rc))


if __name__=="__main__":
  if len(sys.argv)>1:
    run(sys.argv[1])
  else:
    run()
