#! /usr/bin/env python

from __future__ import print_function
import time
import sys, os

this_pid = str(os.getpid())

user = os.environ["USER"]
if (sys.platform == "darwin"):
  ps_cmd = "ps -w -w -U %s -o pid,command" % user
else:
  ps_cmd = "ps -u %s" % user

def get_ps_output():
  result = []
  if (sys.platform == "darwin"):
    for line in os.popen(ps_cmd, "r").read().splitlines():
      flds = line.split()
      if (flds[0] == "PID"): continue
      assert len(flds) >= 2
      cmd = os.path.basename(flds[1])
      if (cmd == "csh"):
        cmd = os.path.basename(flds[2])
        if (cmd == "-f"):
          cmd = os.path.basename(flds[3])
      result.append((flds[0], cmd))
  else:
    for line in os.popen(ps_cmd, "r").read().splitlines():
      flds = line.split(None, 3)
      if (flds[0] == "PID"): continue
      assert len(flds) == 4
      result.append((flds[0], flds[3]))
  return result

def kill(pid):
  if (pid == this_pid): return
  cmd = "kill %s" % pid
  print(cmd)
  sys.stdout.flush()
  os.system(cmd)

def run(args):
  assert len(args) == 0
  for flds in get_ps_output():
    if (flds[1] == "build_cctbx.csh"):
      kill(flds[0])
      time.sleep(0.1)
      break
  for flds in get_ps_output():
    if (   flds[1].startswith("test_")
        or flds[1].startswith("all_tests_")
        or flds[1].startswith("tst_phenix_")
        or flds[1].startswith("testwizard")
        or flds[1].startswith("temporary.com")
        or flds[1].startswith("fresolve")
        or flds[1].startswith("cresolve")
        or flds[1].startswith("fsolve")
        or flds[1].startswith("csolve")
        or flds[1] == "csh"
        or flds[1] == "python"):
      kill(flds[0])
  time.sleep(0.5)
  for flds in get_ps_output():
    print("%6s %s" % flds)
  sys.stdout.flush()

if (__name__ == "__main__"):
  run(sys.argv[1:])
