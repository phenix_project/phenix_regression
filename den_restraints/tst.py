from __future__ import division
from __future__ import print_function
import libtbx.load_env
import os, time
from libtbx import easy_run

def exercise_00():
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/den_restraints/start2.pdb",
    test=os.path.isfile)
  # XXX Looks like not used anywhere!
  #ref_file = libtbx.env.find_in_repositories(
  #  relative_path="phenix_regression/den_restraints/reference2.pdb",
  #  test=os.path.isfile)
  mtz_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/den_restraints/data2.mtz",
    test=os.path.isfile)
  cmd = " ".join([
    "phenix.refine",
    "%s"%pdb_file,
    "%s"%mtz_file,
    "strategy=den",
    # "den.reference_file=%s"%ref_file, # XXX Looks like not used anywhere!
    "den.num_cycles=1",
    "den.optimize=False",
    "output.prefix='den_tmp'",
    "output.write_eff_file=True",
    "output.write_geo_file=False",
    "output.write_def_file=False",
    "output.write_map_coefficients=False",
    "--quiet",
    "--overwrite > den_tmp.log"])
  print(cmd)
  assert not easy_run.call(cmd)
  with open("den_tmp_001.log", 'r') as f:
    lines = f.readlines()
  for line in lines:
    if line.startswith("Start R-work"):
      start_r_work = line[15:21]
      start_r_free = line[32:]
    if line.startswith("Final R-work"):
      final_r_work = line[15:21]
      final_r_free = line[32:]
  f.close()
  assert float(start_r_work) > 0.300
  assert float(start_r_free) > 0.380
  assert float(final_r_work) < 0.09
  assert float(final_r_free) < 0.1

def exercise_01():
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/den_restraints/start2.pdb",
    test=os.path.isfile)
  # XXX Looks like not used anywhere!
  #ref_file = libtbx.env.find_in_repositories(
  #  relative_path="phenix_regression/den_restraints/reference2.pdb",
  #  test=os.path.isfile)
  mtz_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/den_restraints/data2.mtz",
    test=os.path.isfile)
  cmd = " ".join([
    "phenix.refine",
    "%s"%pdb_file,
    "%s"%mtz_file,
    "strategy=den",
    #"den.reference_file=%s"%ref_file, # XXX Looks like not used anywhere!
    "den.num_cycles=1",
    "den.optimize=False",
    "den.annealing_type=cartesian",
    "den.optimize=False",
    "c_beta_restraints=False",
    "output.prefix='den_tmp'",
    "output.write_eff_file=False",
    "output.write_geo_file=False",
    "output.write_def_file=False",
    "output.write_map_coefficients=False",
    "--quiet",
    "--overwrite > den_tmp.log"])
  print(cmd)
  assert not easy_run.call(cmd)
  f = open("den_tmp_001.log", 'r')
  for line in f.readlines():
    if line.startswith("Start R-work"):
      start_r_work = line[15:21]
      start_r_free = line[32:]
    if line.startswith("Final R-work"):
      final_r_work = line[15:21]
      final_r_free = line[32:]
  f.close()
  assert float(start_r_work) > 0.300, float(start_r_work)
  assert float(start_r_free) > 0.380, float(start_r_free)
  assert float(final_r_work) < 0.097, float(final_r_work)
  assert float(final_r_free) < 0.100, float(final_r_free)
  assert not easy_run.call("rm -rf den_tmp*")

def run():
  t0 = time.time()
  exercise_00()
  exercise_01()
  print("Time:%8.2f"%(time.time()-t0))
  print("OK")

if (__name__ == "__main__"):
  run()
