from __future__ import division
from __future__ import print_function
import mmtbx.monomer_library.pdb_interpretation
import iotbx.mtz
from cctbx.array_family import flex
import time
from mmtbx import monomer_library
import mmtbx.refinement.real_space.fit_residues
import iotbx.pdb
from libtbx import group_args
import libtbx.load_env
import os
import scitbx.math
import mmtbx.idealized_aa_residues.rotamer_manager

pdb_str_answer_1="""\n
CRYST1   20.312   17.764   15.391  90.00 109.42  90.00 P 1
ATOM    795  N   GLU A 102      11.676  10.334  10.916  1.00 13.32           N
ATOM    796  CA  GLU A 102      11.279   9.709   9.648  1.00 15.46           C
ATOM    797  C   GLU A 102      11.535  10.654   8.487  1.00 16.48           C
ATOM    798  O   GLU A 102      11.683  11.853   8.683  1.00 14.02           O
ATOM    799  CB  GLU A 102       9.809   9.293   9.674  1.00 13.99           C
ATOM    800  CG  GLU A 102       9.639   7.793   9.915  1.00 17.42           C
ATOM    801  CD  GLU A 102      10.562   6.971   9.014  1.00 15.10           C
ATOM    802  OE1 GLU A 102      10.648   7.279   7.795  1.00 16.69           O
ATOM    803  OE2 GLU A 102      11.204   6.027   9.521  1.00 15.68           O
ATOM    804  N   LYS A 103      11.545  10.128   7.267  1.00 15.66           N
ATOM    805  CA  LYS A 103      11.648  10.974   6.079  1.00 15.27           C
ATOM    806  C   LYS A 103      10.249  11.258   5.535  1.00 16.76           C
ATOM    807  O   LYS A 103       9.333  10.462   5.745  1.00 19.17           O
ATOM    808  CB  LYS A 103      12.505  10.298   5.008  1.00 14.56           C
ATOM    809  CG  LYS A 103      12.317   8.792   4.922  1.00 23.34           C
ATOM    810  CD  LYS A 103      10.847   8.426   4.789  1.00 31.23           C
ATOM    811  CE  LYS A 103      10.243   9.016   3.525  1.00 34.85           C
ATOM    812  NZ  LYS A 103       8.792   8.706   3.407  1.00 36.07           N
ATOM    813  N   PRO A 104      10.069  12.384   4.846  1.00 16.52           N
ATOM    814  CA  PRO A 104      11.138  13.341   4.559  1.00 15.10           C
ATOM    815  C   PRO A 104      11.502  14.098   5.852  1.00 14.34           C
ATOM    816  O   PRO A 104      10.665  14.218   6.726  1.00 14.97           O
ATOM    817  CB  PRO A 104      10.510  14.260   3.512  1.00 15.88           C
ATOM    818  CG  PRO A 104       9.068  14.249   3.856  1.00 16.50           C
ATOM    819  CD  PRO A 104       8.784  12.817   4.276  1.00 15.53           C
TER
ATOM  15595  O3  NAP G   1       1.143   8.168   9.016  1.00 18.43           O
ATOM  15596  NP  NAP G   1       2.356   7.170   8.690  1.00 17.07           P
ATOM  15597  NO1 NAP G   1       2.062   6.498   7.403  1.00 18.29           O
ATOM  15598  NO2 NAP G   1       2.713   6.414   9.910  1.00 15.55           O
ATOM  15599 NO5* NAP G   1       3.510   8.278   8.418  1.00 18.57           O
ATOM  15600 NC5* NAP G   1       4.128   8.987   9.485  1.00 18.15           C
ATOM  15601 NC4* NAP G   1       5.547   9.394   9.086  1.00 16.17           C
ATOM  15602 NC3* NAP G   1       5.572  10.255   7.827  1.00 18.58           C
ATOM  15603 NO3* NAP G   1       6.588  11.239   7.971  1.00 17.67           O
ATOM  15604 NC2* NAP G   1       5.954   9.299   6.709  1.00 17.26           C
ATOM  15605 NO2* NAP G   1       6.724   9.937   5.713  1.00 17.05           O
ATOM  15606 NC1* NAP G   1       6.810   8.298   7.463  1.00 17.84           C
ATOM  15607 NO4* NAP G   1       6.286   8.226   8.767  1.00 20.37           O
ATOM  15608  NN1 NAP G   1       6.744   6.950   6.875  1.00 16.51           N
ATOM  15609  NC6 NAP G   1       5.529   6.333   6.703  1.00 18.80           C
ATOM  15610  NC5 NAP G   1       5.493   5.057   6.147  1.00 18.22           C
ATOM  15611  NC4 NAP G   1       6.691   4.438   5.797  1.00 20.26           C
ATOM  15612  NC3 NAP G   1       7.915   5.065   5.992  1.00 21.48           C
ATOM  15613  NC2 NAP G   1       7.925   6.333   6.537  1.00 19.53           C
ATOM  15614  NC7 NAP G   1       9.194   4.350   5.669  1.00 17.96           C
ATOM  15615  NO7 NAP G   1       9.185   3.102   5.004  1.00 20.13           O
ATOM  15616  NN7 NAP G   1      10.318   4.902   6.094  1.00 20.36           N
TER
END
"""

pdb_str_poor_1="""\n
CRYST1   20.312   17.764   15.391  90.00 109.42  90.00 P 1
ATOM    795  N   GLU A 102      11.676  10.334  10.916  1.00 13.32           N
ATOM    796  CA  GLU A 102      11.279   9.709   9.648  1.00 15.46           C
ATOM    797  C   GLU A 102      11.535  10.654   8.487  1.00 16.48           C
ATOM    798  O   GLU A 102      11.683  11.853   8.683  1.00 14.02           O
ATOM    799  CB  GLU A 102       9.809   9.293   9.674  1.00 13.99           C
ATOM    800  CG  GLU A 102       9.639   7.793   9.915  1.00 17.42           C
ATOM    801  CD  GLU A 102      10.562   6.971   9.014  1.00 15.10           C
ATOM    802  OE1 GLU A 102      10.648   7.279   7.795  1.00 16.69           O
ATOM    803  OE2 GLU A 102      11.204   6.027   9.521  1.00 15.68           O
ATOM    804  N   LYS A 103      11.545  10.128   7.267  1.00 15.66           N
ATOM    805  CA  LYS A 103      11.648  10.974   6.079  1.00 15.27           C
ATOM    806  C   LYS A 103      10.249  11.258   5.535  1.00 16.76           C
ATOM    807  O   LYS A 103       9.333  10.462   5.745  1.00 19.17           O
ATOM    808  CB  LYS A 103      12.505  10.298   5.008  1.00 14.56           C
ATOM    809  CG  LYS A 103      12.317   8.792   4.922  1.00 23.34           C
ATOM    810  CD  LYS A 103      10.847   8.426   4.789  1.00 31.23           C
ATOM    811  CE  LYS A 103      10.243   9.016   3.525  1.00 34.85           C
ATOM    812  NZ  LYS A 103       8.792   8.706   3.407  1.00 36.07           N
ATOM    813  N   PRO A 104      10.069  12.384   4.846  1.00 16.52           N
ATOM    814  CA  PRO A 104      11.138  13.341   4.559  1.00 15.10           C
ATOM    815  C   PRO A 104      11.502  14.098   5.852  1.00 14.34           C
ATOM    816  O   PRO A 104      10.665  14.218   6.726  1.00 14.97           O
ATOM    817  CB  PRO A 104      10.510  14.260   3.512  1.00 15.88           C
ATOM    818  CG  PRO A 104       9.068  14.249   3.856  1.00 16.50           C
ATOM    819  CD  PRO A 104       8.784  12.817   4.276  1.00 15.53           C
TER
ATOM  15595  O3  NAP G   1       1.143   8.168   9.016  1.00 18.43           O
ATOM  15596  NP  NAP G   1       2.356   7.170   8.690  1.00 17.07           P
ATOM  15597  NO1 NAP G   1       2.062   6.498   7.403  1.00 18.29           O
ATOM  15598  NO2 NAP G   1       2.713   6.414   9.910  1.00 15.55           O
ATOM  15599 NO5* NAP G   1       3.510   8.278   8.418  1.00 18.57           O
ATOM  15600 NC5* NAP G   1       4.128   8.987   9.485  1.00 18.15           C
ATOM  15601 NC4* NAP G   1       5.547   9.394   9.086  1.00 16.17           C
ATOM  15602 NC3* NAP G   1       5.572  10.255   7.827  1.00 18.58           C
ATOM  15603 NO3* NAP G   1       6.588  11.239   7.971  1.00 17.67           O
ATOM  15604 NC2* NAP G   1       5.954   9.299   6.709  1.00 17.26           C
ATOM  15605 NO2* NAP G   1       6.724   9.937   5.713  1.00 17.05           O
ATOM  15606 NC1* NAP G   1       6.810   8.298   7.463  1.00 17.84           C
ATOM  15607 NO4* NAP G   1       6.286   8.226   8.767  1.00 20.37           O
ATOM  15608  NN1 NAP G   1       6.744   6.950   6.875  1.00 16.51           N
ATOM  15609  NC6 NAP G   1       5.529   6.333   6.703  1.00 18.80           C
ATOM  15610  NC5 NAP G   1       5.493   5.057   6.147  1.00 18.22           C
ATOM  15611  NC4 NAP G   1       6.691   4.438   5.797  1.00 20.26           C
ATOM  15612  NC3 NAP G   1       7.915   5.065   5.992  1.00 21.48           C
ATOM  15613  NC2 NAP G   1       7.925   6.333   6.537  1.00 19.53           C
ATOM  15614  NC7 NAP G   1       9.194   4.350   5.669  1.00 17.96           C
ATOM  15615  NO7 NAP G   1       9.185   3.102   5.004  1.00 20.13           O
ATOM  15616  NN7 NAP G   1      10.318   4.902   6.094  1.00 20.36           N
TER
END
"""

def exercise(pdb_str_answer, pdb_str_poor, i_inp):
  """
  Test to make sure that valid rotamer outrlier (LYS103) is not moved out of
  good density.
  """
  # answer
  pdb_inp = iotbx.pdb.input(source_info=None, lines=pdb_str_answer)
  pdb_inp.write_pdb_file(file_name = "answer_%s.pdb"%str(i_inp))
  xrs_answer = pdb_inp.construct_hierarchy().extract_xray_structure()
  mtz_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/fit_side_chains/b%s.mtz"%str(i_inp),
    test=os.path.isfile)
  f_map = iotbx.mtz.object(file_name=mtz_file).as_miller_arrays()[0]
  fft_map = f_map.fft_map(resolution_factor=0.25)
  fft_map.apply_sigma_scaling()
  target_map = fft_map.real_map_unpadded()
  # poor
  of = open("poor_%s.pdb"%str(i_inp), "w")
  print(pdb_str_poor, file=of)
  of.close()
  mon_lib_srv = monomer_library.server.server()
  processed_pdb_file = monomer_library.pdb_interpretation.process(
    mon_lib_srv              = mon_lib_srv,
    ener_lib                 = monomer_library.server.ener_lib(),
    raw_records              = flex.std_string(pdb_str_poor.splitlines()),
    strict_conflict_handling = True,
    force_symmetry           = True,
    log                      = None)
  pdb_hierarchy_poor = processed_pdb_file.all_chain_proxies.pdb_hierarchy
  xrs_poor = processed_pdb_file.xray_structure()
  sites_cart_poor = xrs_poor.sites_cart()
  dist = xrs_answer.mean_distance(other = xrs_poor)
  print("start:", dist)
  ####
  grm = mmtbx.restraints.manager(
    geometry=processed_pdb_file.geometry_restraints_manager(show_energies=False),
    normalization = True)
  # load rotamer manager
  rotamer_manager = mmtbx.idealized_aa_residues.rotamer_manager.load(
    rotamers="favored_allowed")
  # pre-compute sin and cos tables
  sin_cos_table = scitbx.math.sin_cos_table(n=10000)
  re = mmtbx.refinement.real_space.side_chain_fit_evaluator(
    pdb_hierarchy      = pdb_hierarchy_poor,
    crystal_symmetry   = xrs_poor.crystal_symmetry(),
    rotamer_evaluator  = rotamer_manager.rotamer_evaluator,
    map_data           = target_map)
  for i in [1]:
    result = mmtbx.refinement.real_space.fit_residues.run(
      pdb_hierarchy     = pdb_hierarchy_poor,
      bselection        = re.sel_outliers_or_poormap(),
      map_data          = target_map,
      crystal_symmetry  = xrs_poor.crystal_symmetry(),
      mon_lib_srv       = mon_lib_srv,
      rotamer_manager   = rotamer_manager,
      sin_cos_table     = sin_cos_table)
    print()
  #
  result.pdb_hierarchy.write_pdb_file(file_name = "refined_%s.pdb"%str(i_inp))
  dist = flex.max(flex.sqrt((xrs_answer.sites_cart() -
    result.pdb_hierarchy.atoms().extract_xyz()).dot()))
  print("final:", dist)
  assert dist < 1.e-6

if(__name__ == "__main__"):
  t0 = time.time()
  inputs = [(pdb_str_answer_1, pdb_str_poor_1)]
  for i, inp in enumerate(inputs):
    exercise(pdb_str_answer=inp[0], pdb_str_poor=inp[1], i_inp=i)
  print("Time: %6.4f"%(time.time()-t0))
