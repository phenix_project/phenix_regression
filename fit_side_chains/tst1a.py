from __future__ import print_function
from scitbx.array_family import flex
import os, time
import iotbx.pdb
import libtbx.load_env
from libtbx import easy_run

def exercise_01():
  def dist(s1,s2):
    return flex.sqrt((s1 - s2).dot())
  pdb_file_name_answer = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/fit_side_chains/fragment3_answer.pdb",
    test=os.path.isfile)
  pdb_file_name_distorted = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/fit_side_chains/fragment3_distorted.pdb",
    test=os.path.isfile)
  hkl_file_name = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/fit_side_chains/fragment3_FOBS.mtz",
    test=os.path.isfile)
  prefix = "exercise_01_fragment3"
  cmd1 = " ".join([
    "phenix.refine",
    "%s"%pdb_file_name_distorted,
    "%s"%hkl_file_name,
    "main.number_of_mac=1",
    "strategy=individual_sites_real_space",
    "output.prefix=%s"%prefix,
    "main.bulk_solvent_and_sc=false",
    'pdb_interpretation.flip_symmetric_amino_acids=None',
    "--overwrite --quiet"
  ])
  cmd2 = " ".join([
    "phenix.refine",
    "%s"%pdb_file_name_distorted,
    "%s"%hkl_file_name,
    "main.number_of_mac=1",
    "strategy=individual_sites_real_space",
    "output.prefix=%s"%prefix,
    "main.bulk_solvent_and_sc=false",
    'pdb_interpretation.flip_symmetric_amino_acids=None',
    "--overwrite --quiet"
  ])
  cmd3 = " ".join([
    "phenix.refine",
    "%s"%pdb_file_name_distorted,
    "%s"%hkl_file_name,
    "main.number_of_mac=1",
    "strategy=individual_sites_real_space+individual_sites",
    "output.prefix=%s"%prefix,
    "main.bulk_solvent_and_sc=false",
    'pdb_interpretation.flip_symmetric_amino_acids=None',
    "--overwrite --quiet"
  ])
  cmd4 = " ".join([
    "phenix.refine",
    "%s"%pdb_file_name_distorted,
    "%s"%hkl_file_name,
    "main.number_of_mac=1",
    "strategy=individual_sites_real_space+individual_sites",
    "output.prefix=%s"%prefix,
    "main.bulk_solvent_and_sc=false",
    'pdb_interpretation.flip_symmetric_amino_acids=None',
    "--overwrite --quiet"
  ])
  assert not easy_run.call(cmd3)
  cntr = 0
  for i_c, cmd in enumerate([cmd1, cmd2, cmd3, cmd4]):
    print(cmd)
    assert not easy_run.call(cmd)
    pdb_inp_answer = iotbx.pdb.input(file_name = pdb_file_name_answer)
    print('pdb_file_name_answer',pdb_file_name_answer)
    sites_answer = pdb_inp_answer.xray_structure_simple().sites_cart()
    print('pdb_file_name_distorted',pdb_file_name_distorted)
    sites_distorted = iotbx.pdb.input(file_name =
      pdb_file_name_distorted).xray_structure_simple().sites_cart()
    print('sites_fixed',"%s_001.pdb"%prefix)
    sites_fixed = iotbx.pdb.input(file_name =
      "%s_001.pdb"%prefix).xray_structure_simple().sites_cart()
    pdb_hierarchy = pdb_inp_answer.construct_hierarchy()
    sel_trp = pdb_hierarchy.atom_selection_cache().selection(string = "resname TRP")
    a1 = flex.max(dist(sites_answer.select(sel_trp), sites_distorted.select(sel_trp)))
    a2 = flex.max(dist(sites_answer.select(sel_trp), sites_fixed.select(sel_trp)))
    assert a1 > 7.3
    a3 = flex.max(dist(sites_answer.select(~sel_trp), sites_distorted.select(~sel_trp)))
    a4 = flex.max(dist(sites_answer.select(~sel_trp), sites_fixed.select(~sel_trp)))
    print(i_c, [a1,a2,a3,a4])
    if  (i_c == 0 or i_c == 1):
      assert a2 < 0.67, a2
      assert a3 < 0.4, a3
      assert a4 < 0.47, a4
    elif(i_c == 2 or i_c == 3):
      assert a2 < 0.3, a2
      assert a3 < 0.4, a3
      assert a4 < 0.3, a4

if (__name__ == "__main__"):
  t0 = time.time()
  exercise_01()
  print("Time: %6.2f" % (time.time()-t0))
