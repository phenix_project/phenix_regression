from __future__ import division
from __future__ import print_function
import libtbx.load_env
import iotbx.mtz
from cctbx.array_family import flex
import time, os
from mmtbx import monomer_library
import mmtbx.refinement.real_space.fit_residues
import iotbx.pdb
from iotbx import reflection_file_reader
import mmtbx.monomer_library.pdb_interpretation
from libtbx.test_utils import approx_equal
import scitbx.math
import mmtbx.idealized_aa_residues.rotamer_manager

pdb_in="""\n
CRYST1   15.119   14.673   18.240  90.00  90.00  90.00 P 1
ATOM    453  N   PRO A  47       8.633   6.370   5.022  1.00 13.79           N
ATOM    454  CA  PRO A  47       7.915   7.571   5.496  1.00 14.61           C
ATOM    455  C   PRO A  47       7.612   7.481   6.994  1.00 15.06           C
ATOM    456  O   PRO A  47       7.289   6.377   7.439  1.00 14.39           O
ATOM    457  CB  PRO A  47       6.639   7.559   4.651  1.00 16.24           C
ATOM    458  CG  PRO A  47       7.089   6.901   3.338  1.00 15.52           C
ATOM    459  CD  PRO A  47       7.990   5.773   3.833  1.00 14.40           C
ATOM    460  N   MSE A  48       7.754   8.528   7.779  1.00 15.13           N
ATOM    461  CA  MSE A  48       7.482   8.456   9.201  1.00 16.17           C
ATOM    462  C   MSE A  48       6.040   8.750   9.517  1.00 15.23           C
ATOM    463  O   MSE A  48       5.417   9.418   8.735  1.00 14.77           O
ATOM    464  CB  MSE A  48       8.165   9.538  10.023  1.00 19.62           C
ATOM    465  CG  MSE A  48       7.944   9.581  11.488  1.00 21.70           C
ATOM    466 SE   MSE A  48       8.776   7.971  12.285  0.70 37.95          Se
ATOM    467  CE  MSE A  48       8.146   8.594  13.981  1.00 28.72           C
ATOM    468  N   LYS A  49       5.519   8.291  10.645  1.00 13.93           N
ATOM    469  CA  LYS A  49       4.167   8.624  11.045  1.00 13.79           C
ATOM    470  C   LYS A  49       4.022  10.138  11.202  1.00 14.66           C
ATOM    471  O   LYS A  49       5.011  10.853  11.351  1.00 15.69           O
ATOM    472  CB  LYS A  49       3.818   7.903  12.334  1.00 13.33           C
ATOM    473  CG  LYS A  49       3.636   6.418  12.111  1.00 14.35           C
ATOM    474  CD  LYS A  49       3.578   5.682  13.426  1.00 16.45           C
ATOM    475  CE  LYS A  49       3.349   4.182  13.198  1.00 18.19           C
ATOM    476  NZ  LYS A  49       3.415   3.440  14.488  1.00 19.97           N
TER      25      LYS A  49
END
"""

pdb_answer = """\n
CRYST1   15.119   14.673   18.240  90.00  90.00  90.00 P 1
ATOM      1  N   PRO A  47       8.633   6.370   5.022  1.00 13.79           N
ATOM      2  CA  PRO A  47       7.915   7.571   5.496  1.00 14.61           C
ATOM      3  C   PRO A  47       7.612   7.481   6.994  1.00 15.06           C
ATOM      4  O   PRO A  47       7.289   6.377   7.439  1.00 14.39           O
ATOM      5  CB  PRO A  47       6.639   7.559   4.651  1.00 16.24           C
ATOM      6  CG  PRO A  47       7.089   6.901   3.338  1.00 15.52           C
ATOM      7  CD  PRO A  47       7.990   5.773   3.833  1.00 14.40           C
ATOM      8  N   MSE A  48       7.754   8.528   7.779  1.00 15.13           N
ATOM      9  CA  MSE A  48       7.482   8.456   9.201  1.00 16.17           C
ATOM     10  C   MSE A  48       6.040   8.750   9.517  1.00 15.23           C
ATOM     11  O   MSE A  48       5.417   9.418   8.735  1.00 14.77           O
ATOM     12  CB  MSE A  48       8.165   9.538  10.023  1.00 19.62           C
ATOM     13  CG  MSE A  48       9.635   9.701   9.926  1.00 21.70           C
ATOM     14 SE   MSE A  48      10.370   9.779  11.762  0.70 37.95          Se
ATOM     15  CE  MSE A  48       8.870  10.874  12.221  1.00 28.72           C
ATOM     16  N   LYS A  49       5.519   8.291  10.645  1.00 13.93           N
ATOM     17  CA  LYS A  49       4.167   8.624  11.045  1.00 13.79           C
ATOM     18  C   LYS A  49       4.022  10.138  11.202  1.00 14.66           C
ATOM     19  O   LYS A  49       5.011  10.853  11.351  1.00 15.69           O
ATOM     20  CB  LYS A  49       3.818   7.903  12.334  1.00 13.33           C
ATOM     21  CG  LYS A  49       3.636   6.418  12.111  1.00 14.35           C
ATOM     22  CD  LYS A  49       3.578   5.682  13.426  1.00 16.45           C
ATOM     23  CE  LYS A  49       3.349   4.182  13.198  1.00 18.19           C
ATOM     24  NZ  LYS A  49       3.415   3.440  14.488  1.00 19.97           N
TER
"""


def exercise(prefix = "tst_fr_1"):
  # target_map
  hkl_file_name = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/fit_side_chains/tst_fit_residues_1.mtz",
    test=os.path.isfile)
  f_map = reflection_file_reader.any_reflection_file(file_name =
    hkl_file_name).as_miller_arrays()[0]
  fft_map = f_map.fft_map(resolution_factor=0.25)
  fft_map.apply_sigma_scaling()
  target_map = fft_map.real_map_unpadded()
  mtz_dataset = f_map.as_mtz_dataset(column_root_label = "f_map")
  mtz_object = mtz_dataset.mtz_object()
  mtz_object.write(file_name = "%s_target.mtz"%prefix)
  # input
  mon_lib_srv = monomer_library.server.server()
  processed_pdb_file = monomer_library.pdb_interpretation.process(
    mon_lib_srv              = mon_lib_srv,
    ener_lib                 = monomer_library.server.ener_lib(),
    raw_records              = flex.std_string(pdb_in.splitlines()),
    strict_conflict_handling = True,
    force_symmetry           = True,
    log                      = None)
  pdb_hierarchy_poor = processed_pdb_file.all_chain_proxies.pdb_hierarchy
  xrs_poor = processed_pdb_file.xray_structure()
  sites_cart_poor = xrs_poor.sites_cart()
  pdb_hierarchy_poor.write_pdb_file(file_name = "%s_in.pdb"%prefix)
  #
  xrs_answer = iotbx.pdb.input(source_info=None,
    lines=pdb_answer).xray_structure_simple()
  ####
  grm = mmtbx.restraints.manager(
    geometry=processed_pdb_file.geometry_restraints_manager(show_energies=False),
    normalization = True)
  # load rotamer manager
  rotamer_manager = mmtbx.idealized_aa_residues.rotamer_manager.load(
    rotamers = "favored_allowed")
  # pre-compute sin and cos tables
  sin_cos_table = scitbx.math.sin_cos_table(n=10000)
  for i in [1,]:
    result = mmtbx.refinement.real_space.fit_residues.run(
      pdb_hierarchy     = pdb_hierarchy_poor,
      map_data          = target_map,
      crystal_symmetry  = xrs_poor.crystal_symmetry(),
      mon_lib_srv       = mon_lib_srv,
      rotamer_manager   = rotamer_manager,
      sin_cos_table     = sin_cos_table)
    print()
  #
  ###
  of = open("answer.pdb","w")
  print(pdb_answer, file=of)
  of.close()
  result.pdb_hierarchy.write_pdb_file(file_name = "%s_refined.pdb"%prefix)
  xrs_refined = iotbx.pdb.input(
    file_name =  "%s_refined.pdb"%prefix).xray_structure_simple()
  d = xrs_answer.distances(other=xrs_refined).min_max_mean().as_tuple()
  assert d[1]<0.68, d
  assert d[2]<0.058, d
  d = xrs_answer.distances(other=xrs_poor).min_max_mean().as_tuple()
  assert approx_equal(d, [0.0, 2.97, 0.32], 0.1)

if(__name__ == "__main__"):
  t0 = time.time()
  exercise()
  print("Time: %6.4f"%(time.time()-t0))
