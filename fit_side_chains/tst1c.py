from __future__ import print_function
from scitbx.array_family import flex
import os, time, math
from scitbx import matrix
from libtbx.test_utils import approx_equal
import iotbx.pdb
import mmtbx.monomer_library.server
import libtbx.load_env
from libtbx import easy_run
from phenix_regression.refinement import get_r_factors
from mmtbx.utils import rotatable_bonds

mon_lib_srv = mmtbx.monomer_library.server.server()

def get_axes_and_atoms(pdb_file_name, remove_clusters_with_all_h=False):
  pdb_inp = iotbx.pdb.input(file_name = pdb_file_name)
  pdb_hierarchy = pdb_inp.construct_hierarchy()
  pdb_atoms = pdb_hierarchy.atoms()
  pdb_atoms.reset_i_seq()
  residue = pdb_hierarchy.only_residue()
  return rotatable_bonds.axes_and_atoms_aa_specific(
    residue = residue, mon_lib_srv = mon_lib_srv,
    remove_clusters_with_all_h = remove_clusters_with_all_h)

def exercise_03():
  pdb_files_dir = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/protein_pdb_files")
  files = os.listdir(pdb_files_dir)
  for i, pdb_file_ in enumerate(files):
    if(pdb_file_[-3:] in ["ent","pdb"]):
      pdb_file = pdb_files_dir + "/" + pdb_file_
      aa = get_axes_and_atoms(pdb_file_name = pdb_file)
      if(pdb_file_.startswith("gly")):
        assert aa is None
      else:
        assert aa is not None, aa

def exercise_06():
  # incomplete residues
  pdb_file_name = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/fit_side_chains/arg_incomplete.pdb",
    test=os.path.isfile)
  aa = get_axes_and_atoms(pdb_file_name = pdb_file_name)
  assert aa == [[(1,4),[5,6]],
                [(4,5),  [6]]]
  #
  pdb_file_name = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/fit_side_chains/phe_incomplete.pdb",
    test=os.path.isfile)
  aa = get_axes_and_atoms(pdb_file_name = pdb_file_name)
  assert aa == [[(1,4),[5,6,7,8,9]],
                [(4,5),  [6,7,8,9]]]
  #


if (__name__ == "__main__"):
  t0 = time.time()
  exercise_03()
  exercise_06()
  print("Time: %6.2f" % (time.time()-t0))
