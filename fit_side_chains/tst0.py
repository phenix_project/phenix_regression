from __future__ import print_function
from scitbx.array_family import flex
import os, time, math
import mmtbx.utils.rotatable_bonds
from scitbx import matrix
from libtbx.test_utils import approx_equal
import iotbx.pdb
import mmtbx.monomer_library.server
import libtbx.load_env
from libtbx import easy_run
from phenix_regression.refinement import get_r_factors


def get_axes_and_atoms(pdb_file_name, remove_clusters_with_all_h=False):
  pdb_inp = iotbx.pdb.input(file_name = pdb_file_name)
  pdb_hierarchy = pdb_inp.construct_hierarchy(sort_atoms=False)
  pdb_atoms = pdb_hierarchy.atoms()
  pdb_atoms.reset_i_seq()
  residue = pdb_hierarchy.only_residue()
  mon_lib_srv = mmtbx.monomer_library.server.server()
  return mmtbx.utils.rotatable_bonds.axes_and_atoms_aa_specific(
    residue = residue, mon_lib_srv = mon_lib_srv,
    remove_clusters_with_all_h = remove_clusters_with_all_h)

def exercise_00():
  #
  pdb_file_name = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/fit_side_chains/arg.pdb",
    test=os.path.isfile)
  aa = get_axes_and_atoms(pdb_file_name = pdb_file_name)
  assert aa == [[(1,4),[5,6,7,8,9,10]],
                [(4,5),  [6,7,8,9,10]],
                [(5,6),    [7,8,9,10]],
                [(6,7),      [8,9,10]]]
  #
  pdb_file_name = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/fit_side_chains/val.pdb",
    test=os.path.isfile)
  aa = get_axes_and_atoms(pdb_file_name = pdb_file_name)
  assert aa == [[(1,4),[5,6]]]
  #
  pdb_file_name = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/fit_side_chains/val_h.pdb",
    test=os.path.isfile)
  aa = get_axes_and_atoms(pdb_file_name = pdb_file_name)
  assert aa == [[(1,4),[5,6,9,10,11,12,13,14,15]],
                [(4,5),      [10,11,12]],
                [(4,6),               [13,14,15]]]
  #
  pdb_file_name = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/fit_side_chains/ile_h.pdb",
    test=os.path.isfile)
  aa = get_axes_and_atoms(pdb_file_name = pdb_file_name)
  assert aa == [[(1,4),[5,6,10,7,11,12,13,14,15,16,17,18]],
                [(4,5),       [7,11,12,         16,17,18]],
                [(4,6),               [13,14,15]],
                [(5, 7),                       [16,17,18]]]
  #
  pdb_file_name = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/fit_side_chains/phe.pdb",
    test=os.path.isfile)
  aa = get_axes_and_atoms(pdb_file_name = pdb_file_name)
  assert aa == [[(1,4),[5,6,7,8,9,10]],
                [(4,5),  [6,7,8,9,10]]]
  #
  pdb_file_name = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/fit_side_chains/arg_h.pdb",
    test=os.path.isfile)
  aa = get_axes_and_atoms(pdb_file_name = pdb_file_name)
  assert aa == [[(1,4),[5,15,16,6,17,18,7,19,20,8,9,10,21,22,23,24,25]],
                [(4,5),        [6,17,18,7,19,20,8,9,10,21,22,23,24,25]],
                [(5,6),                [7,19,20,8,9,10,21,22,23,24,25]],
                [(6,7),                        [8,9,10,21,22,23,24,25]]]
  #
  pdb_file_name = libtbx.env.find_in_repositories(
     relative_path="phenix_regression/fit_side_chains/asp_h.pdb",
     test=os.path.isfile)
  aa = get_axes_and_atoms(pdb_file_name = pdb_file_name)
  assert aa == [[(1,4),[5,9,10,6,7,11]],
                [(4,5),       [6,7,11]],
                [(5,7),           [11]]]
  #
  pdb_file_name = libtbx.env.find_in_repositories(
     relative_path="phenix_regression/fit_side_chains/ile_h_different_order.pdb",
     test=os.path.isfile)
  aa = get_axes_and_atoms(pdb_file_name = pdb_file_name)
  assert aa == [[(1,2),[3,5,10,4,11,12,16,17,18,13,14,15]],
                [(2,3),       [4,11,12,16,17,18]],
                [(3,4),               [16,17,18]],
                [(2,5),                         [13,14,15]]]
  # remve all-H clusters
  pdb_file_name = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/fit_side_chains/val_h.pdb",
    test=os.path.isfile)
  aa = get_axes_and_atoms(pdb_file_name = pdb_file_name,
                          remove_clusters_with_all_h = True)
  assert aa == [[(1,4),[5,6,9,10,11,12,13,14,15]]]
  #
  pdb_file_name = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/fit_side_chains/ile_h.pdb",
    test=os.path.isfile)
  aa = get_axes_and_atoms(pdb_file_name = pdb_file_name,
                          remove_clusters_with_all_h = True)
  assert aa == [[(1,4),[5,6,10,7,11,12,13,14,15,16,17,18]],
                [(4,5),       [7,11,12,         16,17,18]]]
  #
  pdb_file_name = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/fit_side_chains/arg_h.pdb",
    test=os.path.isfile)
  aa = get_axes_and_atoms(pdb_file_name = pdb_file_name,
                          remove_clusters_with_all_h = True)
  assert aa == [[(1,4),[5,15,16,6,17,18,7,19,20,8,9,10,21,22,23,24,25]],
                [(4,5),        [6,17,18,7,19,20,8,9,10,21,22,23,24,25]],
                [(5,6),                [7,19,20,8,9,10,21,22,23,24,25]],
                [(6,7),                        [8,9,10,21,22,23,24,25]]]
  #
  pdb_file_name = libtbx.env.find_in_repositories(
     relative_path="phenix_regression/fit_side_chains/asp_h.pdb",
     test=os.path.isfile)
  aa = get_axes_and_atoms(pdb_file_name = pdb_file_name,
                          remove_clusters_with_all_h = True)
  assert aa == [[(1,4),[5,9,10,6,7,11]],
                [(4,5),       [6,7,11]]]
  #
  pdb_file_name = libtbx.env.find_in_repositories(
     relative_path="phenix_regression/fit_side_chains/MO6.pdb",
     test=os.path.isfile)
  aa = get_axes_and_atoms(pdb_file_name = pdb_file_name,
                          remove_clusters_with_all_h = True)
  assert aa is None

if (__name__ == "__main__"):
  t0 = time.time()
  exercise_00()
  print("Time: %6.2f" % (time.time()-t0))
