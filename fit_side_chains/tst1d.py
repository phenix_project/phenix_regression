from __future__ import print_function
from scitbx.array_family import flex
import os, time
import iotbx.pdb
import libtbx.load_env
from libtbx import easy_run
from phenix_regression.refinement import get_r_factors

def exercise_04():
  def dist(s1,s2):
    return flex.sqrt((s1 - s2).dot())
  pdb_file_name_answer = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/fit_side_chains/fragment1_answer.pdb",
    test=os.path.isfile)
  pdb_file_name_distorted = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/fit_side_chains/fragment1_distorted.pdb",
    test=os.path.isfile)
  hkl_file_name = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/fit_side_chains/fragment1_FOBS.mtz",
    test=os.path.isfile)
  prefix = "exercise_04_fragment1"
  cmd = " ".join([
    "phenix.refine",
    "%s"%pdb_file_name_distorted,
    "%s"%hkl_file_name,
    "main.number_of_mac=1",
    "strategy=none",
    "output.prefix=%s"%prefix,
    "main.bulk_solvent_and_sc=false",
    "main.scattering_table=neutron",
    "--overwrite",
    "--quiet"
  ])
  # XXX PVA: NOT HANDLED BECAUSE H ARE PRESENT. NEEDS TO BE FIXED!
  assert not easy_run.call(cmd)
  pdb_inp_answer = iotbx.pdb.input(file_name = pdb_file_name_answer)
  sites_answer = pdb_inp_answer.xray_structure_simple().sites_cart()
  sites_distorted = iotbx.pdb.input(file_name =
    pdb_file_name_distorted).xray_structure_simple().sites_cart()
  pdb_inp_fixed = iotbx.pdb.input(file_name = "%s_001.pdb"%prefix)
  sites_fixed = pdb_inp_fixed.xray_structure_simple().sites_cart()
  pdb_hierarchy = pdb_inp_answer.construct_hierarchy()
  sel = pdb_hierarchy.atom_selection_cache().selection(string = "pepnames and not (name DG* or name DD* or name DB*)")
  a1 = flex.max(dist(sites_answer.select(sel), sites_distorted.select(sel)))
  a2 = flex.max(dist(sites_answer.select(sel), sites_fixed.select(sel)))
  # XXX PVA: TMP DISABLED assert a1 > 9.0, a1
  # XXX PVA: TMP DISABLED assert a2 < 0.28, a2
  # XXX PVA: TMP DISABLED r_factors = get_r_factors(file_name = "%s_001.pdb"%prefix)
  # XXX PVA: TMP DISABLED assert r_factors.r_work_start > 0.4
  # XXX PVA: TMP DISABLED assert r_factors.r_work_final < 0.085
  assert not easy_run.call("rm -rf %s*"%prefix)


if (__name__ == "__main__"):
  t0 = time.time()
  exercise_04()
  print("Time: %6.2f" % (time.time()-t0))
