from __future__ import print_function
from scitbx.array_family import flex
import os, time
import iotbx.pdb
import libtbx.load_env
from libtbx import easy_run

def exercise_02():
  def dist(s1,s2):
    return flex.sqrt((s1 - s2).dot())
  pdb_file_name_answer = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/fit_side_chains/fragment4_answer.pdb",
    test=os.path.isfile)
  pdb_file_name_distorted = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/fit_side_chains/fragment4_distorted.pdb",
    test=os.path.isfile)
  hkl_file_name = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/fit_side_chains/fragment4_FOBS.mtz",
    test=os.path.isfile)
  prefix = "exercise_02_fragment4"
  cmd1 = " ".join([
    "phenix.refine",
    "%s"%pdb_file_name_distorted,
    "%s"%hkl_file_name,
    "main.number_of_mac=1",
    "strategy=individual_sites_real_space",
    "output.prefix=%s"%prefix,
    "main.bulk_solvent_and_sc=false",
    "--overwrite --quiet"
  ])
  cmd2 = " ".join([
    "phenix.refine",
    "%s"%pdb_file_name_distorted,
    "%s"%hkl_file_name,
    "main.number_of_mac=2",
    "strategy=individual_sites_real_space",
    "output.prefix=%s"%prefix,
    "main.bulk_solvent_and_sc=false",
    "--overwrite --quiet"
  ])
  for cmd in [cmd1, cmd2]:
    assert not easy_run.call(cmd)
    pdb_inp_answer = iotbx.pdb.input(file_name = pdb_file_name_answer)
    pdb_inp_answer.write_pdb_file(file_name="answer.pdb")
    sites_answer = pdb_inp_answer.xray_structure_simple().sites_cart()
    pdb_inp_distorted = iotbx.pdb.input(file_name =
      pdb_file_name_distorted)
    pdb_inp_distorted.write_pdb_file(file_name="distorted.pdb")
    sites_distorted = pdb_inp_distorted.xray_structure_simple().sites_cart()
    sites_fixed = iotbx.pdb.input(file_name =
      "%s_001.pdb"%prefix).xray_structure_simple().sites_cart()
    pdb_hierarchy = pdb_inp_answer.construct_hierarchy()
    sel_trp = pdb_hierarchy.atom_selection_cache().selection(string = "resname TRP")
    a1 = flex.max(dist(sites_answer.select(sel_trp), sites_distorted.select(sel_trp)))
    a2 = flex.max(dist(sites_answer.select(sel_trp), sites_fixed.select(sel_trp)))
    assert a1 > 5.0 , a1
    assert a2 < 0.77, a2
    a3 = flex.max(dist(sites_answer.select(~sel_trp), sites_distorted.select(~sel_trp)))
    a4 = flex.max(dist(sites_answer.select(~sel_trp), sites_fixed.select(~sel_trp)))
    assert a3<0.4 , a3
    assert a4<0.4, a4
    assert not easy_run.call("rm -rf %s*"%prefix)

if (__name__ == "__main__"):
  t0 = time.time()
  exercise_02()
  print("Time: %6.2f" % (time.time()-t0))
