#! /bin/csh -f
set build="`libtbx.show_build_path`"
source "$build/unsetpaths.csh"
source "$build/setpaths.csh"
set verbose
python "`libtbx.show_dist_paths libtbx`/run_tests.py" --valgrind
python "`libtbx.show_dist_paths boost_adaptbx`/run_tests.py" --valgrind
python "`libtbx.show_dist_paths scitbx`/run_tests.py" --valgrind
python "`libtbx.show_dist_paths cctbx`/run_tests.py" --Quick --valgrind
python "`libtbx.show_dist_paths cctbx`/run_examples.py" --Quick --valgrind
python "`libtbx.show_dist_paths iotbx`/run_tests.py" --Quick --valgrind
python "`libtbx.show_dist_paths mmtbx`/run_tests.py" --valgrind
python "`libtbx.show_dist_paths phenix`/run_tests.py" --Quick --valgrind
