from __future__ import print_function
from libtbx import easy_run
import libtbx.load_env
import sys, os
import os.path as op

def run(args):
  assert args in [[], ["--quick"]]
  quick = (len(args) == 1)
  pdb_dir = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/rna_puckers",
    optional=False)
  for node in os.listdir(pdb_dir):
    if (not node.endswith(".pdb")): continue
    #
    start_pdb = op.join(pdb_dir, node)
    cmd = "phenix.pdb_interpretation write_geo_file=True %s" % start_pdb
    print(cmd)
    easy_run.fully_buffered(command=cmd).raise_if_errors()
    #
    cmd = " ".join([
      "phenix.geometry_minimization",
      start_pdb])
    print(cmd)
    easy_run.fully_buffered(command=cmd).raise_if_errors()
    mod_pdb = node[:-4]+"_minimized.pdb"
    min_pdb = node[:-4]+"_geo_min.pdb"
    os.rename(mod_pdb, min_pdb)
    #
    cmd = "phenix.pdb_interpretation write_geo_file=True %s" % min_pdb
    print(cmd)
    easy_run.fully_buffered(command=cmd).raise_if_errors()
    #
    if (quick):
      print("DONE: rna_puckers/geo_min.py")
      return
    #
    print()

if (__name__ == "__main__"):
  run(args=sys.argv[1:])
