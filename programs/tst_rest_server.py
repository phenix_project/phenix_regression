from __future__ import absolute_import, division, print_function
from time import time
import sys

def exercise_1():
  try:
    from iotbx.cli_parser import run_program
    from phenix.programs import predict_chain
    args = ['test_server=True']
    results= run_program(program_class=predict_chain.Program,args=args,
        logger = sys.stdout)
  except Exception as e:
    raise AssertionError("Failed to run rest server")

if (__name__ == "__main__"):
  t0 = time()
  exercise_1()
  print("OK")
