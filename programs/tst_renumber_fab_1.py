from __future__ import absolute_import, division, print_function

from phenix.pdb_tools.renumber_fab import renumber_fab
from time import time
import libtbx.load_env
import os.path
import mmtbx.model
import iotbx.pdb
from iotbx.bioinformatics import fasta_sequence_parse
from libtbx.test_utils import assert_lines_in_text
import requests

fasta_6uce = """\
>6UCE_1|Chain A[auth L]|N123-VRC34_pI3 light chain|Homo sapiens (9606)
DIQLTQSPSFLSASVGDRVTITCRASQGISNELAWYQQKPGKAPNLLIYYASTLQSGVPSRFSASGSGTHFTLTISSLQPEDFATYFCQHMSSYPLTFGGGTKVEIKRTVAAPSVFIFPPSDEQLKSGTASVVCLLNNFYPREAKVQWKVDNALQSGNSQESVTEQDSKDSTYSLSSTLTLSKADYEKHKVYACEVTHQGLSSPVTKSFNRGEC
>6UCE_2|Chain B[auth H]|N123-VRC34_pI3 heavy chain|Homo sapiens (9606)
QEVLVQSGAEVKKPGASVKVSCKAFGYTFTGNPMHWVRQAPGQGLEWMGWINPHSGDTTTAQKFQGRVYMTRDKSINTAYLDVTRLTSDDTAIYYCARDKYYGNEAVGMDVWGQGTTVTVSSASTKGPSVFPLAPSSKSTSGGTAALGCLVKDYFPEPVTVSWNSGALTSGVHTFPAVLQSSGLYSLSSVVTVPSSSLGTQTYICNVNHKPSNTKVDKKVEPKSCDKGLEVLF
>6UCE_3|Chain C|HIV fusion peptide|Human immunodeficiency virus 1 (11676)
AVGIGAVF
"""

def exercise_1():
  model_fname = libtbx.env.find_in_repositories(
      relative_path='phenix_regression/pdb/6uce_sequential.pdb', test=os.path.exists
  )
  model = mmtbx.model.manager(
      model_input=iotbx.pdb.input(model_fname))
  original_ss_annotations = model.get_ss_annotation().as_pdb_str()
  assert_lines_in_text(original_ss_annotations, "HELIX    9 AA9 LYS H  201  ASN H  204  5                                   4")
  assert_lines_in_text(original_ss_annotations, """\
SHEET    1 AA7 6 GLU H  10  LYS H  12  0
SHEET    2 AA7 6 THR H 107  VAL H 111  1  O  THR H 110   N  LYS H  12
SHEET    3 AA7 6 ALA H  88  ASP H  95 -1  N  TYR H  90   O  THR H 107
SHEET    4 AA7 6 MET H  34  GLN H  39 -1  N  HIS H  35   O  ALA H  93
SHEET    5 AA7 6 LEU H  45  ILE H  51 -1  O  MET H  48   N  TRP H  36
SHEET    6 AA7 6 THR H  57  THR H  59 -1  O  THR H  58   N  TRP H  50
""")
  s_objects, non_compliant = fasta_sequence_parse.parse(fasta_6uce)
  working_model = model.deep_copy()
  h = working_model.get_hierarchy()
  assert h.atoms()[2477].parent().parent().resseq == " 107"
  assert h.atoms()[2477].parent().parent().icode == " "
  renumber_fab(working_model, s_objects, verbose=False)
  assert h.atoms()[2477].parent().parent().resseq == " 100"
  assert h.atoms()[2477].parent().parent().icode == "C"
  new_ss_annotation = working_model.get_ss_annotation().as_pdb_str()
  assert_lines_in_text(new_ss_annotation, "HELIX    9 AA9 LYS H  197  ASN H  200  5                                   4")
  assert_lines_in_text(new_ss_annotation, """\
SHEET    1 AA7 6 GLU H  10  LYS H  12  0
SHEET    2 AA7 6 THR H 100C VAL H 102  1  O  THR H 101   N  LYS H  12
SHEET    3 AA7 6 ALA H  84  ASP H  91 -1  N  TYR H  86   O  THR H 100C
SHEET    4 AA7 6 MET H  34  GLN H  39 -1  N  HIS H  35   O  ALA H  89
SHEET    5 AA7 6 LEU H  45  ILE H  51 -1  O  MET H  48   N  TRP H  36
SHEET    6 AA7 6 THR H  56  THR H  58 -1  O  THR H  57   N  TRP H  50
""")

if (__name__ == "__main__"):
  t0 = time()
  exception_occured = False
  try:
    r = requests.get('http://www.bioinf.org.uk/abs/abnum/')
  except Exception:
    print("OK but exception.")
    exception_occured = True
  if not exception_occured and r.ok and len(r.text) > 100:
      exercise_1()
      print("Time: %.2f" % (time() - t0))
      print("OK")
  else:
    print("OK but skipped.")
