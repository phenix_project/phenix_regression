from __future__ import division
from __future__ import print_function
import os
import libtbx.load_env
from libtbx.test_utils import approx_equal

import time

data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="segment_and_split_map_cif",
  test=os.path.isdir)

small_first_part=os.path.join(data_dir,"placed_model_1-20.cif")
small_second_part=os.path.join(data_dir,"placed_model_21-30.cif")
small_pdb=os.path.join(data_dir,"small_model.cif")
small_pdb_two_models=os.path.join(data_dir,"small_model_two_models.cif")
small_start_pdb=os.path.join(data_dir,"start.cif")
small_map_file=os.path.join(data_dir,"small_map.ccp4")

def tst_01():
  print("testing dock_in_map with small model and map")
  args=["%s" %(small_map_file),
        "%s" %(small_pdb),
         "align_moments=False",
         "ssm_search=False",
         "output_files.temp_dir=dom_1",
         "resolution=3",]
  print ("phenix.dock_in_map %s" %(" ".join(args)))
  try:
    from iotbx.cli_parser import run_program
    from phenix.programs import dock_in_map as run
  except Exception as e:
    print("Dock in map not available...skipping")
    return

  results=run_program(program_class=run.Program,args=args)

  final_model=results.final_model
  r,t=results.final_r,results.final_t
  print(tuple(r),tuple(t))

  assert approx_equal ( tuple(r),(0.999992665174783, -0.0008248861216248736, 0.0037347916865552097, 0.0008300606997872123, 0.9999978190421565, -0.0013089614554106027, -0.003733710747678572, 0.0013120509652530298, 0.9999911689384885),eps=0.2)
  assert approx_equal ( tuple(t),   (-0.03519417517693329, 0.0051053107498065685, -0.007964343442760468),eps=0.4)
  print("OK")

if __name__=="__main__":
  t0 = time.time()
  tst_01()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
