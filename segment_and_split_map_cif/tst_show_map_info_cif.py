from __future__ import division
from __future__ import print_function
import os
import libtbx.load_env


import time

data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="segment_and_split_map_cif",
  test=os.path.isdir)

small_map_file=os.path.join(data_dir,"small_map.ccp4")

def tst_01():
  print("testing show_map_info with map that has an offset")
  args=["%s" %(small_map_file),]
  try:
    from iotbx.cli_parser import run_program
    from phenix.programs import show_map_info as run
  except Exception as e:
    print("Show map info not available...skipping")
    return

  results=run_program(program_class=run.Program,args=args)

  print("OK")

if __name__=="__main__":
  t0 = time.time()
  tst_01()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
