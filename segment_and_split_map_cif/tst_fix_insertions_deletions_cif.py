from __future__ import division
from __future__ import print_function
import os
import libtbx.load_env


import time

data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="segment_and_split_map_cif",
  test=os.path.isdir)

deletion_pdb=os.path.join(data_dir,"deletion_box.cif")
deletion_map=os.path.join(data_dir,"deletion_box.ccp4")
deletion_seq=os.path.join(data_dir,"deletion_box.seq")

def tst_01():
  print("testing fix_insertions_deletions with small model and map")
  args=[
          "%s" %(deletion_map),
          "%s" %(deletion_pdb),
          "%s" %(deletion_seq),
          "resolution=3",
          "refine=False",
          "good_enough_cc=0.65",
          "loop_method = split_loop ",
          "try_insertions = True",
          "try_deletions= False",
          "try_as_is = False",
            ]
  from iotbx.cli_parser import run_program
  from phenix.programs import fix_insertions_deletions as run

  results=run_program(program_class=run.Program,args=args)

  print("Final model",\
     results.final_model.get_hierarchy().overall_counts().n_residues)
  print("Final score",\
     results.final_score)
  assert results.final_model.get_hierarchy().overall_counts().n_residues > 20
  assert results.final_score >=40
  print("OK")

if __name__=="__main__":
  t0 = time.time()
  tst_01()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
