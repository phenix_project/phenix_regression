from __future__ import division
from __future__ import print_function
import os
import libtbx.load_env
from libtbx.test_utils import approx_equal



import time

data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="segment_and_split_map_cif",
  test=os.path.isdir)

map_file_1_orig=os.path.join(data_dir,"random_1.ccp4")
map_file_2_orig=os.path.join(data_dir,"random_2.ccp4")
target_map_file_orig=os.path.join(data_dir,"model_A30_trans_100.ccp4")
target_model_file_orig=os.path.join(data_dir,"model_A30_trans_100.cif")
seq_file=os.path.join(data_dir,"model_A30_trans_100.seq")

map_file_1_shifted=os.path.join(data_dir,"random_1_shifted.ccp4")
map_file_2_shifted=os.path.join(data_dir,"random_2_shifted.ccp4")
target_map_file_shifted=os.path.join(data_dir,"model_A30_trans_100_shifted.ccp4")
target_model_file_shifted=os.path.join(data_dir,"model_A30_trans_100_shifted.cif")

def tst_01(map_file_1,map_file_2,target_map_file,seq_file,output_file):

  print("Testing resolve_cryo_em with randomized map.")
  args=[
      "%s" %( map_file_1),
          "%s" %(map_file_2),
          "seq_file=%s" %(seq_file),
         "target_map_file_name=%s" %(target_map_file),
         "resolution=3",
         "soft_mask_at_end=true",
         "write_boxed_map_files=True",
         "verbose=true",
         "output_files.denmod_map_file_name=%s" %output_file]
  print ("phenix.resolve_cryo_em %s" %(" ".join(args)))
  try:
    from iotbx.cli_parser import run_program
    from phenix.programs import resolve_cryo_em as run
  except Exception as e:
    print("resolve_cryo_em not available...skipping")
    return

  from six.moves import cStringIO as StringIO
  f=StringIO()
  results=run_program(program_class=run.Program,args=args,logger=f)
  text=f.getvalue()
  print (text)
  assert approx_equal(float("%.2f" %(results.d_cc_star_half_avg)),3.04,eps=0.1)

  from iotbx.map_model_manager import map_model_manager
  mm2=results.target_map_map_manager
  mm1=results.denmod_map_manager
  mam=map_model_manager(map_manager=mm1,extra_map_manager_list=[mm2],extra_map_manager_id_list=['other'])
  cc=mam.map_map_cc('map_manager','other')
  print("Map-map cc: ",cc)
  return cc

if __name__=="__main__":

  t0 = time.time()
  cc_orig=tst_01(map_file_1_orig,map_file_2_orig,target_map_file_orig,seq_file,
   'tst_01a.ccp4')
  cc_shifted=tst_01(map_file_1_shifted,map_file_2_shifted,target_map_file_shifted,seq_file,'tst_01b.ccp4')
  assert approx_equal(cc_orig,cc_shifted)

  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
