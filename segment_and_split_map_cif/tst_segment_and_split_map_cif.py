from __future__ import division
from __future__ import print_function
import sys,os
import libtbx.load_env
from libtbx.utils import null_out

from cctbx.array_family import flex
import time,sys
import iotbx.pdb

data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="segment_and_split_map_cif",
  test=os.path.isdir)

from cctbx.maptbx.segment_and_split_map import run as segment_and_split_map
map_file=os.path.join(data_dir,"short.ccp4")
symmetry_file=os.path.join(data_dir,"short.ncs_spec")
seq_file=os.path.join(data_dir,"short.seq")

def tst_01():
  print("testing segment_and_split_map with ncs")
  args=["map_file=%s" %(map_file),
         "ncs_file=%s" %(symmetry_file),
         "soft_mask=False","b_blur_hires=None",
          "box_in_auto_sharpen=False","region_weight_method=delta_ratio",
         "get_half_height_width=False",
         "auto_sharpen=False",
         "remove_aniso=False",
         "seq_file=%s" %(seq_file),]
  ncs_group_obj,remainder_ncs_group_obj,tracking_data=segment_and_split_map(
    args)

  print("\nChecking results tst_01")
  # Make sure everything is as expected:
  items_dict={
     'selected_regions': [1],
     'bad_region_list': [],
     'ncs_related_regions': [2, 3, 4, 5, 6, 7],
     'original_id_from_id': {1: 5, 2: 2, 3: 3, 4: 7, 5: 6, 6: 4, 7: 1},
     'edited_volume_list': [1116, 1108, 1107, 1107, 1105, 1102, 1101],
    }
  for item in items_dict.keys():
    print(item,items_dict[item],"Should be:",getattr(ncs_group_obj,item))
  for item in items_dict.keys():
    print("Testing: ",item)
    assert items_dict[item]==getattr(ncs_group_obj,item)

  items_dict={
     'selected_regions': [3],
     'bad_region_list': [],
     'ncs_related_regions': [1, 2, 4, 5, 6, 7],
     'original_id_from_id': {1: 2, 2: 10, 3: 12, 4: 1, 5: 9, 6: 5, 7: 6},
     'edited_volume_list': [329, 329, 328, 326, 325, 322, 321],
   }
  print("\nChecking results tst_01b")
  for item in items_dict.keys():
    print(item,items_dict[item],"Should be:",getattr(remainder_ncs_group_obj,item))
  for item in items_dict.keys():
    print("Testing:",item)
    assert items_dict[item]==getattr(remainder_ncs_group_obj,item)

def tst_02():
  print("testing segment_and_split_map with no ncs")
  args=["map_file=%s" %(map_file),
         "soft_mask=False","b_blur_hires=None",
         "box_in_auto_sharpen=False","region_weight_method=delta_ratio",
         "get_half_height_width=False",
         "auto_sharpen=False",
         "remove_aniso=False",
         "ncs_file=None",
         "seq_file=%s" %(seq_file),]
  ncs_group_obj,remainder_ncs_group_obj,tracking_data=segment_and_split_map(
    args,out=null_out())

  print("\nChecking results tst_02")
  # Make sure everything is as expected:
  items_dict={
     'selected_regions': [1, 2, 3, 4, 5, 6],
     'bad_region_list': [],
     'ncs_related_regions': [],
     'original_id_from_id': {1: 3, 2: 2, 3: 4, 4: 5, 5: 1, 6: 6},
     'edited_volume_list': [2409, 1213, 1212, 1209, 1198, 1188],
    }
  for item in items_dict.keys():
    print(item,items_dict[item],"Should be:",getattr(ncs_group_obj,item))
  for item in items_dict.keys():
    print("Testing",item)
    assert items_dict[item]==getattr(ncs_group_obj,item)

  items_dict={
     'selected_regions': [1, 2, 3, 4, 5, 6, 7],
     'bad_region_list': [],
     'ncs_related_regions': [],
     'original_id_from_id': {1: 13, 2: 1, 3: 7, 4: 16, 5: 2, 6: 14, 7: 8},
     'edited_volume_list': [288, 281, 278, 270, 252, 250, 245],
   }
  print("test_02")
  for item in items_dict.keys():
    print(item,items_dict[item],"Should be: ",getattr(remainder_ncs_group_obj,item))
  for item in items_dict.keys():
    print("Testing ",item)
    assert items_dict[item]==getattr(remainder_ncs_group_obj,item)

def tst_02a():
  print("testing segment_and_split_map with no ncs and get_half_height_width")
  args=["map_file=%s" %(map_file),
         "soft_mask=False","b_blur_hires=None",
         "box_in_auto_sharpen=False","region_weight_method=delta_ratio",
         "auto_sharpen=False",
         "remove_aniso=False",
         "ncs_file=None",
         "seq_file=%s" %(seq_file),]
  ncs_group_obj,remainder_ncs_group_obj,tracking_data=segment_and_split_map(
    args,out=null_out())

  print("\nChecking results tst_02a")
  # Make sure everything is as expected:
  items_dict={
     'selected_regions': [1, 2, 3, 4, 5, 6],
     'bad_region_list': [],
     'ncs_related_regions': [],
     'original_id_from_id': {1: 3, 2: 2, 3: 4, 4: 5, 5: 1, 6: 6},
     'edited_volume_list': [2409, 1213, 1212, 1209, 1198, 1188],
    }
  for item in items_dict.keys():
    print(item,items_dict[item],"Should be:",getattr(ncs_group_obj,item))
  for item in items_dict.keys():
    print("Testing",item)
    assert items_dict[item]==getattr(ncs_group_obj,item)

  items_dict={
     'selected_regions':  [1, 2, 3, 4, 5, 6, 7],
     'bad_region_list': [],
     'ncs_related_regions': [],
     'original_id_from_id': {1: 13, 2: 1, 3: 7, 4: 16, 5: 2, 6: 14, 7: 8},
     'edited_volume_list': [288, 281, 278, 270, 252, 250, 245],
   }
  print("test_02a")
  for item in items_dict.keys():
    print(item,items_dict[item],"Should be: ",getattr(remainder_ncs_group_obj,item))
  for item in items_dict.keys():
    print("Testing ",item)
    assert items_dict[item]==getattr(remainder_ncs_group_obj,item)

def tst_02b():
  print("testing segment_and_split_map with no ncs but with sharpening")
  args=["map_file=%s" %(map_file),
         "resolution=2.8",
         "b_iso=115.23",
         "allow_box_if_b_iso_set=True",
         "k_sharpen=0",
         "max_box_fraction=200",
         "soft_mask=False","b_blur_hires=None",
         "box_in_auto_sharpen=False","region_weight_method=delta_ratio",
         "get_half_height_width=False",
         "remove_aniso=False",
         "discard_if_worse=False",
         "ncs_file=None",
         "seq_file=%s" %(seq_file),]
  ncs_group_obj,remainder_ncs_group_obj,tracking_data=segment_and_split_map(
    args,out=null_out())

  print("\nChecking results tst_02b")
  # Make sure everything is as expected:
  items_dict={
     'selected_regions': [1, 2, 3, 4, 5, 6],
     'bad_region_list': [],
     'ncs_related_regions': [],
     'original_id_from_id': [{1: 7, 2: 3, 3: 37, 4: 1, 5: 19, 6: 47},{1: 7, 2: 39, 3: 3, 4: 1, 5: 19, 6: 49}],
     'edited_volume_list': [[2536, 1285, 1285, 1275, 1269, 1262],[2535, 1287, 1283, 1274, 1267, 1260]],
    }
  for item in items_dict.keys():
    print(item,items_dict[item],"Should be:",getattr(ncs_group_obj,item))
  for item in items_dict.keys():
    print("Testing",item)
    assert items_dict[item]==getattr(ncs_group_obj,item) or getattr(ncs_group_obj,item) in items_dict[item]

  items_dict={
     'selected_regions':  [1, 2, 3, 4, 5, 6, 7],
     'bad_region_list': [],
     'ncs_related_regions': [],
     'original_id_from_id': {1: 10, 2: 5, 3: 9, 4: 2, 5: 6, 6: 12, 7: 1},
     'edited_volume_list': [312, 303, 303, 302, 302, 295, 285],
   }
  print("test_02b remainder")
  for item in items_dict.keys():
    print(item,items_dict[item],"Should be: ",getattr(remainder_ncs_group_obj,item))
  for item in items_dict.keys():
    print("Testing ",item)
    assert items_dict[item]==getattr(remainder_ncs_group_obj,item)

def tst_02c():
  print("testing segment_and_split_map with no ncs but with k_sharpen=100")
  args=["map_file=%s" %(map_file),
         "resolution=2.8",
         "b_iso=115.23",
         "k_sharpen=100",
         "soft_mask=False","b_blur_hires=None",
         "box_in_auto_sharpen=False","region_weight_method=delta_ratio",
         "get_half_height_width=False",
         "remove_aniso=False",
         "discard_if_worse=False",
         "ncs_file=None",
         "seq_file=%s" %(seq_file),]
  ncs_group_obj,remainder_ncs_group_obj,tracking_data=segment_and_split_map(
    args,out=null_out())

  print("\nChecking results tst_02c")
  # Make sure everything is as expected:
  items_dict={
     'selected_regions': [1, 2, 3, 4, 5, 6],
     'bad_region_list': [],
     'ncs_related_regions': [],
     'original_id_from_id': [{1: 7, 2: 3, 3: 37, 4: 1, 5: 19, 6: 47},{1: 7, 2: 39, 3: 3, 4: 1, 5: 19, 6: 49}],
     'edited_volume_list': [[2536, 1285, 1285, 1275, 1269, 1262],[2535, 1287, 1283, 1274, 1267, 1260]],
    }
  for item in items_dict.keys():
    print(item,items_dict[item],"Should be:",getattr(ncs_group_obj,item))
  for item in items_dict.keys():
    print("Testing",item)
    assert items_dict[item]==getattr(ncs_group_obj,item) or getattr(ncs_group_obj,item) in items_dict[item]

  items_dict={
     'selected_regions':  [1, 2, 3, 4, 5, 6, 7],
     'bad_region_list': [],
     'ncs_related_regions': [],
     'original_id_from_id': {1: 10, 2: 5, 3: 9, 4: 2, 5: 6, 6: 12, 7: 1},
     'edited_volume_list': [312, 303, 303, 302, 302, 295, 285],
   }
  print("test_02c remainder")
  for item in items_dict.keys():
    print(item,items_dict[item],"Should be: ",getattr(remainder_ncs_group_obj,item))
  for item in items_dict.keys():
    print("Testing ",item)
    assert items_dict[item]==getattr(remainder_ncs_group_obj,item)

def tst_03(d_min=3, resolution_factor = 0.3, prefix="tst_mask_expand",):


  ncs_info="""
REMARK 350 MOLECULE CAN BE GENERATED BY APPLYING BIOMT TRANSFORMATIONS
REMARK 350   BIOMT1   1  1.000000  0.000000  0.000000        0.00000
REMARK 350   BIOMT2   1  0.000000  1.000000  0.000000        0.00000
REMARK 350   BIOMT3   1  0.000000  0.000000  1.000000        0.00000
REMARK 350   BIOMT1   2  0.623490  0.781831  0.000000        0.00000
REMARK 350   BIOMT2   2 -0.781831  0.623490  0.000000        0.00000
REMARK 350   BIOMT3   2  0.000000  0.000000  1.000000        0.00000
REMARK 350   BIOMT1   3 -0.222521  0.974928  0.000000        0.00000
REMARK 350   BIOMT2   3 -0.974928 -0.222521  0.000000        0.00000
REMARK 350   BIOMT3   3  0.000000  0.000000  1.000000        0.00000
REMARK 350   BIOMT1   4 -0.900969  0.433884  0.000000        0.00000
REMARK 350   BIOMT2   4 -0.433884 -0.900969  0.000000        0.00000
REMARK 350   BIOMT3   4  0.000000  0.000000  1.000000        0.00000
REMARK 350   BIOMT1   5 -0.900969 -0.433884  0.000000        0.00000
REMARK 350   BIOMT2   5  0.433884 -0.900969  0.000000        0.00000
REMARK 350   BIOMT3   5  0.000000  0.000000  1.000000        0.00000
REMARK 350   BIOMT1   6 -0.222521 -0.974928  0.000000        0.00000
REMARK 350   BIOMT2   6  0.974928 -0.222521  0.000000        0.00000
REMARK 350   BIOMT3   6  0.000000  0.000000  1.000000        0.00000
REMARK 350   BIOMT1   7  0.623490 -0.781831  0.000000        0.00000
REMARK 350   BIOMT2   7  0.781831  0.623490  0.000000        0.00000
REMARK 350   BIOMT3   7  0.000000  0.000000  1.000000        0.00000
"""

  cryst1_orthogonal="""
CRYST1   24.085   25.037   23.421  90.00  90.00  90.00 P 1
"""
  cryst1_non_orthogonal="""
CRYST1   24.085   25.037   23.421  30.00  50.00  70.00 P 1
"""
  pdb_str_answer="""\
ATOM      1  N   ASP A  18      19.085  14.927  14.341  1.00 20.00           N
ATOM      2  CA  ASP A  18      18.051  15.952  14.416  1.00 20.00           C
ATOM      3  C   ASP A  18      17.560  16.341  13.025  1.00 20.00           C
ATOM      4  O   ASP A  18      18.362  16.608  12.129  1.00 20.00           O
ATOM      5  CB  ASP A  18      18.572  17.186  15.155  1.00 20.00           C
ATOM      6  CG  ASP A  18      17.527  18.278  15.276  1.00 20.00           C
ATOM      7  OD1 ASP A  18      16.754  18.257  16.256  1.00 20.00           O
ATOM      8  OD2 ASP A  18      17.480  19.158  14.391  1.00 20.00           O
ATOM      9  N   ASN A  19      16.239  16.370  12.861  1.00 20.00           N
ATOM     10  CA  ASN A  19      15.604  16.724  11.593  1.00 20.00           C
ATOM     11  C   ASN A  19      16.078  15.846  10.436  1.00 20.00           C
ATOM     12  O   ASN A  19      16.827  16.297   9.568  1.00 20.00           O
ATOM     13  CB  ASN A  19      15.823  18.206  11.270  1.00 20.00           C
ATOM     14  CG  ASN A  19      14.830  18.739  10.251  1.00 20.00           C
ATOM     15  OD1 ASN A  19      14.313  17.995   9.417  1.00 20.00           O
ATOM     16  ND2 ASN A  19      14.560  20.037  10.315  1.00 20.00           N
ATOM     17  N   TYR A  20      15.637  14.593  10.430  1.00 20.00           N
ATOM     18  CA  TYR A  20      16.015  13.652   9.382  1.00 20.00           C
ATOM     19  C   TYR A  20      14.868  12.702   9.050  1.00 20.00           C
ATOM     20  O   TYR A  20      13.763  12.840   9.573  1.00 20.00           O
ATOM     21  CB  TYR A  20      17.261  12.860   9.791  1.00 20.00           C
ATOM     22  CG  TYR A  20      17.104  12.073  11.074  1.00 20.00           C
ATOM     23  CD1 TYR A  20      16.687  10.748  11.053  1.00 20.00           C
ATOM     24  CD2 TYR A  20      17.378  12.653  12.306  1.00 20.00           C
ATOM     25  CE1 TYR A  20      16.544  10.025  12.223  1.00 20.00           C
ATOM     26  CE2 TYR A  20      17.237  11.938  13.481  1.00 20.00           C
ATOM     27  CZ  TYR A  20      16.820  10.624  13.433  1.00 20.00           C
ATOM     28  OH  TYR A  20      16.679   9.909  14.600  1.00 20.00           O
ATOM     29  N   ARG A  21      15.140  11.738   8.176  1.00 20.00           N
ATOM     30  CA  ARG A  21      14.133  10.764   7.772  1.00 20.00           C
ATOM     31  C   ARG A  21      13.986   9.673   8.827  1.00 20.00           C
ATOM     32  O   ARG A  21      12.921   9.511   9.423  1.00 20.00           O
ATOM     33  CB  ARG A  21      14.493  10.156   6.413  1.00 20.00           C
ATOM     34  CG  ARG A  21      13.399   9.297   5.788  1.00 20.00           C
ATOM     35  CD  ARG A  21      13.605   7.816   6.075  1.00 20.00           C
ATOM     36  NE  ARG A  21      12.559   6.992   5.477  1.00 20.00           N
ATOM     37  CZ  ARG A  21      12.502   5.668   5.576  1.00 20.00           C
ATOM     38  NH1 ARG A  21      13.435   5.011   6.252  1.00 20.00           N
ATOM     39  NH2 ARG A  21      11.512   5.000   5.000  1.00 20.00           N
TER
END
"""

  pdb_str_start="""
ATOM     17  N   TYR A  20      15.637  14.593  10.430  1.00 20.00           N
ATOM     18  CA  TYR A  20      16.015  13.652   9.382  1.00 20.00           C
ATOM     19  C   TYR A  20      14.868  12.702   9.050  1.00 20.00           C
ATOM     20  O   TYR A  20      13.763  12.840   9.573  1.00 20.00           O
ATOM     21  CB  TYR A  20      17.261  12.860   9.791  1.00 20.00           C
ATOM     22  CG  TYR A  20      17.104  12.073  11.074  1.00 20.00           C
ATOM     23  CD1 TYR A  20      16.687  10.748  11.053  1.00 20.00           C
ATOM     24  CD2 TYR A  20      17.378  12.653  12.306  1.00 20.00           C
ATOM     25  CE1 TYR A  20      16.544  10.025  12.223  1.00 20.00           C
ATOM     26  CE2 TYR A  20      17.237  11.938  13.481  1.00 20.00           C
"""

  """
  Test to exercise mask_expand routines
  """
  #
  from mmtbx.ncs.ncs import ncs
  ncs_object=ncs()
  ncs_object.read_ncs(lines=ncs_info.splitlines(),log=sys.stdout)
  ncs_object.display_all()

  pdb_inp = iotbx.pdb.input(source_info=None,
     lines=cryst1_orthogonal+pdb_str_answer)
  start_inp = iotbx.pdb.input(source_info=None, lines=pdb_str_start)
  xrs = pdb_inp.xray_structure_simple()
  f_calc = xrs.structure_factors(d_min = d_min).f_calc()
  fft_map = f_calc.fft_map(resolution_factor=resolution_factor)
  fft_map.apply_sigma_scaling()

  map_data = fft_map.real_map_unpadded()
  starting_sites_cart=start_inp.construct_hierarchy().atoms().extract_xyz()
  unit_cell=xrs.unit_cell()
  every_nth_point=4


  from cctbx.maptbx.segment_and_split_map import get_ncs_sites_cart, \
     get_marked_points_cart,mask_from_sites_and_map,get_ncs_mask,set_radius

  #  make an overall mask around sites

  sites_frac=flex.vec3_double()
  sites_frac.append((0.27,0.54,0.35))
  sites_cart=unit_cell.orthogonalize(sites_frac)
  overall_mask=mask_from_sites_and_map(
      map_data=map_data,unit_cell=unit_cell,
      sites_cart=sites_cart,radius=15.)
  print("Overall mask points marked: %d of %d " %(
      overall_mask.count(True),overall_mask.size()))

  radius=set_radius(unit_cell=unit_cell,map_data=map_data,
   every_nth_point=every_nth_point)

  print("Sampling interval: %s Radius: %7.2f " %(every_nth_point,radius))

  # ----------- verify that this will cover everything
  print("Checking all points in grid can be marked with every_nth_point = %s" %(
    every_nth_point))
  expand_radius=2*radius
  sites_frac=flex.vec3_double()
  sites_frac.append((0.37,0.712,0.519))
  sites_cart=unit_cell.orthogonalize(sites_frac)

  print("starting sites:",sites_cart.size())
  working_mask=mask_from_sites_and_map(
      map_data=map_data,unit_cell=unit_cell,
      sites_cart=sites_cart,radius=radius,overall_mask=overall_mask)
  print("Count of starting marked:",working_mask.count(True))
  count_last=working_mask.count(True)
  while 1:
    sites_cart=get_marked_points_cart(mask_data=working_mask,
     unit_cell=unit_cell,every_nth_point=every_nth_point)
    print("new sites:",sites_cart.size())
    non_expanded=mask_from_sites_and_map(
      map_data=map_data,unit_cell=unit_cell,
      sites_cart=sites_cart,radius=radius,overall_mask=overall_mask)
    print("Count of non-expanded marked:",non_expanded.count(True),non_expanded.count(False))
    working_mask=mask_from_sites_and_map(
      map_data=map_data,unit_cell=unit_cell,
      sites_cart=sites_cart,radius=expand_radius,overall_mask=overall_mask)
    print("Count of marked:",non_expanded.count(True))
    count=non_expanded.count(True)
    if count==count_last: break
    count_last=count
  assert non_expanded.count(False)/overall_mask.size() >=0.95* overall_mask.count(False)/overall_mask.size()
  print("OK\n")

  # ----------- done verify that this will cover everything


  # get starting mask
  starting_mask=mask_from_sites_and_map( # starting au mask
    map_data=map_data,unit_cell=unit_cell,
    sites_cart=starting_sites_cart,radius=radius,overall_mask=overall_mask)

  au_mask,ncs_mask=get_ncs_mask(
    map_data=map_data,unit_cell=unit_cell,ncs_object=ncs_object,
    starting_mask=starting_mask,
    radius=radius,expand_radius=expand_radius,
    overall_mask=overall_mask,
    every_nth_point=every_nth_point)
  print("Points in au: %d   in ncs: %d  both: %d Not marked: %d" %(
     au_mask.count(True),ncs_mask.count(True),
     (au_mask & ncs_mask).count(True),
     au_mask.size()-au_mask.count(True)-ncs_mask.count(True),))

def tst_04():
  print("testing segment_and_split_map with ncs auto")
  args=["map_file=%s" %(map_file),
         "ncs_file=None",
         "symmetry=C7",
         "optimize_center=True",
         "helical_rot_deg=30",
         "helical_trans_z_angstrom=10",
         "two_fold_along_x=True",
         #"ncs_center=17.3437709528 27.3591296417 27.333333333333332",
         "soft_mask=False","b_blur_hires=None",
         "box_in_auto_sharpen=False","region_weight_method=delta_ratio",
         "get_half_height_width=False",
         "auto_sharpen=False",
         "remove_aniso=False",
         "seq_file=%s" %(seq_file),]
  ncs_group_obj,remainder_ncs_group_obj,tracking_data=segment_and_split_map(
    args,out=sys.stdout) #null_out())

  print("\nChecking results tst_04")
  # Make sure everything is as expected:
  items_dict={
     'selected_regions': [1],
     'bad_region_list': [],
     'ncs_related_regions':  [2, 3, 4, 5, 6, 7],
     'original_id_from_id': {1: 5, 2: 2, 3: 3, 4: 7, 5: 6, 6: 4, 7: 1},
     'edited_volume_list':  [1116, 1108, 1107, 1107, 1105, 1102, 1101],
    }
  for item in items_dict.keys():
    print(item,items_dict[item],"Should be:",getattr(ncs_group_obj,item))
  for item in items_dict.keys():
    print("Testing",item)
    assert items_dict[item]==getattr(ncs_group_obj,item)

def tst_05():
  print("testing segment_and_split_map with auto_sharpen")
  args=["map_file=%s" %(map_file),
         "soft_mask=False","b_blur_hires=None",
         "box_in_auto_sharpen=False","region_weight_method=delta_ratio",
         "get_half_height_width=False",
         "auto_sharpen=True",
         "remove_aniso=False",
         "region_weight=40",
         "residual_target=kurtosis",
         "search_b_n=3",
         "resolution=3",
         "solvent_fraction=.90",
         "seq_file=%s" %(seq_file),]
  ncs_group_obj,remainder_ncs_group_obj,tracking_data=segment_and_split_map(
    args,out=null_out())

  print("\nChecking results tst_05")
  # Make sure everything is as expected:
  items_dict={
     'selected_regions': [1, 2, 3, 4, 5, 6],
     'bad_region_list': [],
     'ncs_related_regions': [],
     'original_id_from_id': {1: 3, 2: 2, 3: 4, 4: 5, 5: 6, 6: 1},
     'edited_volume_list':  [2417, 1219, 1219, 1212, 1199, 1196],
    }
  for item in items_dict.keys():
    print(item,items_dict[item],"Should be:",getattr(ncs_group_obj,item))
  for item in items_dict.keys():
    print("Testing",item)
    aa = items_dict[item]==getattr(ncs_group_obj,item)
    if type(getattr(ncs_group_obj,item)) == type ({}):
      assert aa
    else:
      bb = getattr(ncs_group_obj,item) in items_dict[item]
      assert aa or bb

  items_dict={
     'selected_regions': [1, 2, 3, 4, 5, 6, 7],
     'bad_region_list': [],
     'ncs_related_regions': [],
     'original_id_from_id':  {1: 2, 2: 10, 3: 12, 4: 9, 5: 6, 6: 1, 7: 5},
     'edited_volume_list': [341, 333, 331, 330, 329, 328, 325],
   }
  print("test_05")
  for item in items_dict.keys():
    print(item,items_dict[item],"Should be: ",getattr(remainder_ncs_group_obj,item))
  for item in items_dict.keys():
    print("Testing ",item)
    assert items_dict[item]==getattr(remainder_ncs_group_obj,item) or getattr(remainder_ncs_group_obj,item) in items_dict[item]

def tst_05b():
  print("segment_and_split_map with auto_sharpen and box_in_auto_sharpen")
  try:
    from phenix.autosol.map_to_model import iterated_solvent_fraction
  except Exception as e:
    print("Skipping test as iterated_solvent_fraction is not available")
    print("OK")
    return

  args=["map_file=%s" %(map_file),
         "soft_mask=False","b_blur_hires=None",
         "region_weight_method=delta_ratio",
         "get_half_height_width=False",
         "auto_sharpen=True",
         "region_weight=40",
         "box_in_auto_sharpen=True",
         "remove_aniso=False",
         "residual_target=kurtosis",
         "search_b_n=3",
         "resolution=3",
         "solvent_fraction=.90",
         "seq_file=%s" %(seq_file),]
  ncs_group_obj,remainder_ncs_group_obj,tracking_data=segment_and_split_map(
    args,out=null_out())

  print("\nChecking results tst_05b")
  # Make sure everything is as expected:
  items_dict={
     'selected_regions': [1, 2, 3, 4, 5, 6],
     'bad_region_list': [],
     'ncs_related_regions': [],
     'original_id_from_id': {1: 3, 2: 5, 3: 4, 4: 2, 5: 6, 6: 1},
     'edited_volume_list': [2234, 1136, 1126, 1116, 1116, 1106],
    }
  for item in items_dict.keys():
    print(item,items_dict[item],"Should be:",getattr(ncs_group_obj,item))
  for item in items_dict.keys():
    print("Testing",item)
    assert items_dict[item]==getattr(ncs_group_obj,item)

  items_dict={
     'selected_regions': [1, 2, 3, 4, 5, 6, 7],
     'bad_region_list': [],
     'ncs_related_regions': [],
     'original_id_from_id': {1: 2, 2: 14, 3: 1, 4: 12, 5: 6, 6: 11, 7: 7},
     'edited_volume_list':  [328, 319, 318, 318, 312, 310, 275],
   }
  print("test_05b")
  for item in items_dict.keys():
    print(item,items_dict[item],"Should be: ",getattr(remainder_ncs_group_obj,item))
  for item in items_dict.keys():
    print("Testing ",item)
    assert items_dict[item]==getattr(remainder_ncs_group_obj,item)

  print("OK")


def tst_06():
  print("\ntest_06:  sharpening a map\n")
  from cctbx.maptbx.segment_and_split_map import get_map_object
  print("Reading map from %s\n" %(map_file))
  resolution=3
  map,space_group,unit_cell,crystal_symmetry,origin_frac,acc,\
    original_crystal_symmetry,original_unit_cell_grid=\
      get_map_object( map_file)
  map=map.as_double()

  from cctbx.maptbx.segment_and_split_map import get_f_phases_from_map
  map_coeffs,map_coeffs_aa=get_f_phases_from_map(map_data=map,
       crystal_symmetry=crystal_symmetry,
       d_min=resolution,
       return_as_map_coeffs=True)

  from cctbx.maptbx.segment_and_split_map import auto_sharpen_map_or_map_coeffs
  from cctbx.maptbx.segment_and_split_map import get_fft_map,get_b_iso


  print("\n"+80*"-")
  print("\nStandard sharpening\n")
  print("\n"+80*"-")
  new_map_si=auto_sharpen_map_or_map_coeffs(
        adjust_region_weight=False,
        resolution=resolution,
        crystal_symmetry=crystal_symmetry,
        map=map,
        auto_sharpen=True,
        solvent_content=0.775,
        box_in_auto_sharpen=False,
        soft_mask=False,
        b_blur_hires=None,
        auto_sharpen_methods=["b_iso"],
        remove_aniso=False,
        search_b_n=3
        )
  new_map=new_map_si.as_map_data()
  print("\nSharpened map maximum:", new_map.as_1d().as_double().min_max_mean().max)
  f_array,phases=get_f_phases_from_map(map_data=new_map,
       crystal_symmetry=crystal_symmetry,
       d_min=resolution)
  res_cut_array=f_array.resolution_filter(d_min=resolution)
  b_iso=get_b_iso(res_cut_array)
  print("\nEffective B-iso found after sharpening= %7.2f" %(b_iso))
  assert "%7.1f" %(b_iso) == "%7.1f" %(-100.0)

  print("\n"+80*"-")
  print("\nStandard sharpening with remove_aniso\n")
  print("\n"+80*"-")
  new_map_si=auto_sharpen_map_or_map_coeffs(
        adjust_region_weight=False,
        resolution=resolution,
        crystal_symmetry=crystal_symmetry,
        map=map,
        auto_sharpen=True,
        solvent_content=0.775,
        box_in_auto_sharpen=False,
        soft_mask=False,
        b_blur_hires=None,
        auto_sharpen_methods=["b_iso_to_d_cut"],
        remove_aniso=True,
        search_b_n=3
        )
  new_map=new_map_si.as_map_data()
  print("\nSharpened map maximum:", new_map.as_1d().as_double().min_max_mean().max)
  f_array,phases=get_f_phases_from_map(map_data=new_map,
       crystal_symmetry=crystal_symmetry,
       d_min=resolution)
  res_cut_array=f_array.resolution_filter(d_min=resolution)
  b_iso=get_b_iso(res_cut_array)
  print("\nEffective B-iso after sharpening= %7.2f" %(b_iso))
  assert "%7.0f" %(b_iso) == "%7.0f" %(-63)


  print("\n"+80*"-")
  print("\nNow sharpen as map coeffs...\n")
  print("\n"+80*"-")
  new_map_coeffs_si=auto_sharpen_map_or_map_coeffs(
        adjust_region_weight=False,
        resolution=resolution,
        map_coeffs=map_coeffs,
        solvent_content=0.775,
        n_real=new_map.all(),
        auto_sharpen=True,
        box_in_auto_sharpen=False,
        soft_mask=False,
        b_blur_hires=None,
        auto_sharpen_methods=["b_iso"],
        remove_aniso=False,
        search_b_n=3,
       )
  new_map_coeffs=new_map_coeffs_si.as_map_coeffs()
  # and get new map:
  new_map=get_fft_map(map_coeffs=new_map_coeffs,n_real=new_map.all()).real_map_unpadded()
  print("\nSharpened map_coeffs map maximum:", new_map.as_1d().as_double().min_max_mean().max)
  f_array,phases=get_f_phases_from_map(map_data=new_map,
       crystal_symmetry=crystal_symmetry,
       d_min=resolution)
  res_cut_array=f_array.resolution_filter(d_min=resolution)
  b_iso=get_b_iso(res_cut_array)
  print("\nEffective B-iso after sharpening map coeffs= %7.2f" %(b_iso))
  assert "%7.0f" %(0.1*b_iso) == "%7.0f" %(0.1*-100.0)

  print("\n"+80*"-")
  print("\nNow sharpen with box")
  print("\n"+80*"-")
  new_map_si=auto_sharpen_map_or_map_coeffs(
        adjust_region_weight=False,
        resolution=resolution,
        crystal_symmetry=crystal_symmetry,
        map=map,
        region_weight=40,
        auto_sharpen=True,
        solvent_content=0.775,
        box_in_auto_sharpen=True,
        soft_mask=False,
        b_blur_hires=None,
        auto_sharpen_methods=["b_iso"],
        remove_aniso=False,
        search_b_n=3
        )
  new_map=new_map_si.as_map_data()
  print("\nSharpened map using box maximum:", new_map.as_1d().as_double().min_max_mean().max)
  f_array,phases=get_f_phases_from_map(map_data=new_map,
       crystal_symmetry=crystal_symmetry,
       d_min=resolution)
  res_cut_array=f_array.resolution_filter(d_min=resolution)
  b_iso=get_b_iso(res_cut_array)
  print("\nEffective B-iso after box sharpening map coeffs= %7.2f" %(b_iso))
  assert "%7.0f" %(0.01*b_iso) == "%7.0f" %(0.01*109.08)



  print("\n"+80*"-")
  print("\nNow sharpen with box and remove_aniso")
  print("\n"+80*"-")
  new_map_si=auto_sharpen_map_or_map_coeffs(
        adjust_region_weight=False,
        resolution=resolution,
        crystal_symmetry=crystal_symmetry,
        map=map,
        region_weight=40,
        auto_sharpen=True,
        box_in_auto_sharpen=True,
        soft_mask=False,
        b_blur_hires=None,
        auto_sharpen_methods=["b_iso_to_d_cut"],
        remove_aniso=True,
        search_b_n=3
        )
  new_map=new_map_si.as_map_data()
  print("\nSharpened map using box maximum:", new_map.as_1d().as_double().min_max_mean().max)
  f_array,phases=get_f_phases_from_map(map_data=new_map,
       crystal_symmetry=crystal_symmetry,
       d_min=resolution)
  res_cut_array=f_array.resolution_filter(d_min=resolution)
  b_iso=get_b_iso(res_cut_array)
  print("\nEffective B-iso after box sharpening map coeffs= %7.2f" %(b_iso))
  assert "%7.0f" %(0.1*b_iso) == "%7.0f" %(0.1*-54.30)



  print("\n"+80*"-")
  print("\nUsing phenix.auto_sharpen")
  print("\n"+80*"-")
  print("Reading map from %s\n" %(map_file))
  args=["map_file=%s" %(map_file),"resolution=%s" %(resolution),
       "box_in_auto_sharpen=True","soft_mask=False","b_blur_hires=None",
       "remove_aniso=False"]
  from cctbx.maptbx.auto_sharpen import run
  run(args)
  output_file='sharpened_map.ccp4'
  new_map,new_space_group,new_unit_cell,new_crystal_symmetry,new_origin_frac,\
    acc,original_crystal_symmetry,original_unit_cell_grid=\
      get_map_object(output_file)
  new_map=new_map.as_double()
  f_array,phases=get_f_phases_from_map(map_data=new_map,
       crystal_symmetry=new_crystal_symmetry,
       d_min=resolution)
  res_cut_array=f_array.resolution_filter(d_min=resolution)
  b_iso=get_b_iso(res_cut_array)
  print("\nEffective B-iso after auto_sharpen= %7.2f" %(b_iso))
  assert "%7.0f" %(0.01*b_iso) == "%7.0f" %(0.01*16.84)


  print("OK")

def tst_07():
  print("testing segment_and_split_map with no solvent content")
  args=["map_file=%s" %(map_file),
         "soft_mask=False","b_blur_hires=None",
         "resolution=3",
         "box_in_auto_sharpen=False","region_weight_method=delta_ratio",
         "get_half_height_width=False",
         "remove_aniso=False",
         "ncs_file=None",
         "chain_type=RNA",
         ]
  print(" ".join(args))
  ncs_group_obj,remainder_ncs_group_obj,tracking_data=segment_and_split_map(
    args,out=null_out())

  print("\nChecking results tst_07")
  # Make sure everything is as expected:
  items_dict={
     'selected_regions': [1, 2],
     'bad_region_list': [],
     'ncs_related_regions': [],
     'original_id_from_id': {1: 1, 2: 3},
     'edited_volume_list': [8104, 3236],
    }
  for item in items_dict.keys():
    print(item,items_dict[item],"Should be:",getattr(ncs_group_obj,item))
  for item in items_dict.keys():
    print("Testing",item)
    assert items_dict[item]==getattr(ncs_group_obj,item) or getattr(ncs_group_obj,item) in items_dict[item]

  print("OK")

def tst_08():
  print("testing segment_and_split_map with ncs and select_au_box")
  args=["map_file=%s" %(map_file),
         "ncs_file=%s" %(symmetry_file),
         "soft_mask=False","b_blur_hires=None",
          "box_in_auto_sharpen=False","region_weight_method=delta_ratio",
         "get_half_height_width=False",
         "auto_sharpen=False",
          "select_au_box=True",
          "n_ops_to_use_au_box=2",
          "resolution=3",
          "n_au_box=1",
          "random_points=10",
         "remove_aniso=False",
         "seq_file=%s" %(seq_file),]
  ncs_group_obj,remainder_ncs_group_obj,tracking_data=segment_and_split_map(
    args,out=null_out())

  print("\nChecking results tst_08")
  # Make sure everything is as expected:
  items_dict={
     'selected_regions': [1, 2, 3, 4],
     'bad_region_list': [],
     'ncs_related_regions': [],
     'original_id_from_id': {1: 2, 2: 3, 3: 1, 4: 4},
     'edited_volume_list': [529, 291, 122, 85],
    }

  py38_items_dict={
     'selected_regions': [1],
     'bad_region_list': [],
     'ncs_related_regions': [],
     'original_id_from_id': {1: 1},
     'edited_volume_list': [1505],
    }

  for item in items_dict.keys():
    print(item,items_dict[item],"Should be:",getattr(ncs_group_obj,item))
  for item in items_dict.keys():
    print("Testing: ",item)
    assert items_dict[item]==getattr(ncs_group_obj,item) or \
           py38_items_dict[item] == getattr(ncs_group_obj,item)
  print("OK")

if __name__=="__main__":
  t0 = time.time()
  tst_01()
  tst_02()
  tst_02a()
  tst_02b()
  tst_02c()
  tst_03()
  tst_04()
  tst_05()
  tst_05b()
  tst_06()
  tst_07()
  tst_08()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
