from __future__ import division
from __future__ import print_function
import os
import libtbx.load_env
from libtbx.test_utils import approx_equal

import time

data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="segment_and_split_map_cif",
  test=os.path.isdir)

map_file=os.path.join(data_dir,"C4.ccp4")
pdb_file=os.path.join(data_dir,"C4_one.cif")
target_sequence=\
""">C4_one.pdb|Chain=L
AAVHDVFFLIKGVFAFYTYYN
"""
random_sequence_1=\
""">random_sequence 1
GFDYNTASCQELIVAAWQYTSNN
"""
random_sequence_2=\
""">random_sequence 2
HVMNRISRLSFLLITASQRVAAANN
"""

def string_is_similar_enough(s1,s2,minimum_identity=1):
  if len(s1)!=len(s2):
   return False
  sim=0
  sim_n=0
  for ss1,ss2 in zip(s1,s2):
   if ss1==ss2:
     sim+=1
   sim_n+=1
  if sim/max(1,sim_n)>=minimum_identity:
    return True
  else:
    return False

def tst_01(include_pdb_file=False,seq_dir=False,symmetry=None,improper=False,
     best_fitting_sequence=None,
     optimal_sequences=None,
     score_by_residue_groups=False,
     residue_groups=None,
     full_model=None,
     mean_score=None,):

  print("Testing sequence_from_map with map that has C4 symmetry.")
  print("Seq_dir: %s   Symmetry: %s  Improper: %s  Full_model: %s" %(
      seq_dir,symmetry,improper,full_model))
  args=["%s" %(map_file),
         "resolution=3",
         "random_sequences=1",
         "macro_cycles=1",
         "min_ncs_cc=0.5",
         "minimum_length=1",
         "max_segments=1",
         "extract_ss=false",
         "random_seed=17771",
         "molecular_mass=20000",
         "split_at_unknown=False",
         "em_side_density=False",
         "maximum_sequence_length=30",
         ]
  if residue_groups:
    args.append("residue_groups=%s" %(residue_groups))
  if score_by_residue_groups:
    args.append("score_by_residue_groups=True")
  if full_model:
    args.append("build_type=full_model")
    args.append("quick=true")
  else:
    args.append("build_type=helices_strands")

  if include_pdb_file:
    args.append("%s" %(pdb_file))
  if seq_dir:
    args.append("seq_dir=seq_dir")
  if symmetry:
    args.append("symmetry=%s" %(symmetry))
  if improper:
    args.append("improper_symmetry=True")

  if (not os.path.isdir('seq_dir')):
    os.mkdir('seq_dir')
  for x,file_name in zip(
    [target_sequence,random_sequence_1,random_sequence_2],
    ['target_sequence','random_sequence_1','random_sequence_2']) :
      f=open(os.path.join('seq_dir',file_name),'w')
      print(x, file=f)
      f.close()
  try:
    from iotbx.cli_parser import run_program
    from phenix.programs import sequence_from_map as run
  except Exception as e:
    print("Sequence_from_map in map not available...skipping")
    return

  results=run_program(program_class=run.Program,args=args)
  print(results)
  if best_fitting_sequence:
    print(best_fitting_sequence,"should be:",results.best_fitting_sequence)
    assert best_fitting_sequence==results.best_fitting_sequence
  if optimal_sequences:
    print(optimal_sequences,"should be:",results.optimal_sequences)
    assert string_is_similar_enough(
      optimal_sequences[0],results.optimal_sequences[0],minimum_identity=0.2)
  if mean_score is not None:
    print(mean_score)
    assert approx_equal(mean_score,results.mean_score,eps=10)

  print("OK")
if __name__=="__main__":
  t0 = time.time()
  tst_01(include_pdb_file=True,
     best_fitting_sequence=None,
     score_by_residue_groups=True,
     residue_groups="VGASCTI P LDNEQM KRFHYW",
     optimal_sequences=['VPVKVVLKKVXVKVVXVXXLL'],
     mean_score=4)
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
