from __future__ import division
from __future__ import print_function
import os
import libtbx.load_env


import time

data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="segment_and_split_map_cif",
  test=os.path.isdir)

small_pdb=os.path.join(data_dir,"half_strand.cif")
small_map_file=os.path.join(data_dir,"strand.ccp4")

def tst_01():
  print("testing trace_and_build with small model and map")
  args=["%s" %(small_map_file),
        "%s" %(small_pdb),
         "refine_cycles=1",
         "resolution=2.5",]
  from iotbx.cli_parser import run_program
  from phenix.programs import trace_and_build as run

  results=run_program(program_class=run.Program,args=args)

  print("Final model",\
     results.final_score,results.n_residues)
  assert results.final_score > 5

  print("OK")

if __name__=="__main__":
  t0 = time.time()
  tst_01()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
