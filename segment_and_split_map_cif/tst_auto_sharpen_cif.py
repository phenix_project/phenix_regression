from __future__ import division
from __future__ import print_function
import os
import libtbx.load_env
from libtbx.utils import null_out

from cctbx.array_family import flex
import time
import iotbx.pdb

data_dir = libtbx.env.under_dist(
  module_name = "phenix_regression",
  path="segment_and_split_map_cif",
  test = os.path.isdir)

map_file = os.path.join(data_dir, "aniso_small_box.ccp4")
pdb_file = os.path.join(data_dir, "aniso_small_box.cif")


from cctbx.maptbx.segment_and_split_map import get_map_object
print("Reading map from %s\n" %(map_file))
resolution = 3.2
map, space_group, unit_cell, crystal_symmetry, origin_frac, acc, \
    original_crystal_symmetry, original_unit_cell_grid = \
    get_map_object( map_file)
map = map.as_double()

import iotbx.pdb
from scitbx.array_family import flex
pdb_inp = iotbx.pdb.input(source_info = None, lines = flex.split_lines(
        open(pdb_file).read()))
hierarchy = pdb_inp.construct_hierarchy()

xrs = pdb_inp.xray_structure_simple(
       crystal_symmetry = crystal_symmetry)
xrs.scattering_type_registry(table = 'n_gaussian')

import mmtbx.maps.correlation

def get_cc(local_map = None):
  cc_calculator = mmtbx.maps.correlation.from_map_and_xray_structure_or_fmodel(
      xray_structure = xrs,
      map_data      = local_map, # map_as_double,
      d_min         = resolution)

  return cc_calculator.cc(selection = hierarchy.atoms().extract_i_seq(),
             atom_radius = 3.1)

from cctbx.maptbx.segment_and_split_map import auto_sharpen_map_or_map_coeffs

def tst_01():

  print("\n"+80*"-")
  print("Testing auto_sharpen with b_iso and remove_aniso")
  print("\n"+80*"-")
  new_map_si = auto_sharpen_map_or_map_coeffs(
        resolution = resolution,
        crystal_symmetry = crystal_symmetry,
        map = map,
        k_sharpen = 12,
        box_in_auto_sharpen = False,
        soft_mask = False,
        auto_sharpen_methods = ["b_iso_to_d_cut"],
        remove_aniso = True,
        out = null_out())
  new_map = new_map_si.as_map_data().as_double()
  original_map = map.as_double()

  # Get cc of new map and old map with model in hierarchy
  cc_original = get_cc(original_map)
  cc_new = get_cc(new_map)
  text = "CC original %7.2f   After sharpening: %7.2f " %(cc_original, cc_new)
  print(text)
  assert text.replace(" ", "")  ==  "CC original    0.65   After sharpening:    0.69".replace(" ", "")

def tst_02():

  print("\n"+80*"-")
  print("Testing auto_sharpen with b_iso and no remove_aniso")
  print("\n"+80*"-")
  new_map_si = auto_sharpen_map_or_map_coeffs(
        resolution = resolution,
        crystal_symmetry = crystal_symmetry,
        map = map,
        soft_mask = False,
        box_in_auto_sharpen = False,
        auto_sharpen_methods = ["b_iso"],
        remove_aniso = False,
        out = null_out())
  new_map = new_map_si.as_map_data().as_double()
  original_map = map.as_double()

  # Get cc of new map and old map with model in hierarchy
  cc_original = get_cc(original_map)
  cc_new = get_cc(new_map)
  text = "CC original %7.2f   After sharpening: %7.2f " %(cc_original, cc_new)
  print(text)
  assert text.replace(" ", "")  ==  "CC original    0.65   After sharpening:    0.59".replace(" ", "")

def tst_03():

  print("\n"+80*"-")
  print("Testing auto_sharpen with b_iso_to_d_cut and remove_aniso")
  print("\n"+80*"-")
  new_map_si = auto_sharpen_map_or_map_coeffs(
        resolution = resolution,
        crystal_symmetry = crystal_symmetry,
        map = map,
        soft_mask = False,
        box_in_auto_sharpen = False,
        auto_sharpen_methods = ["b_iso_to_d_cut"],
        remove_aniso = True,
        out = null_out())
  new_map = new_map_si.as_map_data().as_double()
  original_map = map.as_double()

  # Get cc of new map and old map with model in hierarchy
  cc_original = get_cc(original_map)
  cc_new = get_cc(new_map)
  text = "CC original %7.2f   After sharpening: %7.2f " %(cc_original, cc_new)
  print(text)
  assert text.replace(" ", "")  ==  "CC original    0.65   After sharpening:    0.67".replace(" ", "")

def tst_04():

  print("\n"+80*"-")
  print("Testing auto_sharpen with b_iso_to_d_cut and no remove_aniso")
  print("\n"+80*"-")
  new_map_si = auto_sharpen_map_or_map_coeffs(
        resolution = resolution,
        crystal_symmetry = crystal_symmetry,
        map = map,
        soft_mask = False,
        box_in_auto_sharpen = False,
        auto_sharpen_methods = ["b_iso_to_d_cut"],
        remove_aniso = False,
        out = null_out())
  new_map = new_map_si.as_map_data().as_double()
  original_map = map.as_double()

  # Get cc of new map and old map with model in hierarchy
  cc_original = get_cc(original_map)
  cc_new = get_cc(new_map)
  text = "CC original %7.2f   After sharpening: %7.2f " %(cc_original, cc_new)
  print(text)
  assert text.replace(" ", "")  ==  "CC original    0.65   After sharpening:    0.64".replace(" ", "")

def tst_05():

  print("\n"+80*"-")
  print("Testing auto_sharpen with model and remove_aniso")
  print("\n"+80*"-")

  pdb_inp = iotbx.pdb.input(source_info = None, lines = flex.split_lines(
        open(pdb_file).read()))
  pdb_hierarchy = pdb_inp.construct_hierarchy()
  new_map_si = auto_sharpen_map_or_map_coeffs(
        resolution = resolution,
        crystal_symmetry = crystal_symmetry,
        map = map,
        pdb_hierarchy = pdb_hierarchy,
        soft_mask = False,
        box_in_auto_sharpen = False,
        remove_aniso = True,
        out = null_out())

  new_map = new_map_si.as_map_data().as_double()
  original_map = map.as_double()

  # Get cc of new map and old map with model in hierarchy
  cc_original = get_cc(original_map)
  cc_new = get_cc(new_map)
  text = "CC original %7.2f   After sharpening: %7.2f " %(cc_original, cc_new)
  print(text)
  assert text.replace(" ", "")  ==  "CC original    0.65   After sharpening:    0.72".replace(" ", "")

def tst_06():

  print("\n"+80*"-")
  print("Testing auto_sharpen with model and no remove_aniso")
  print("\n"+80*"-")
  pdb_inp = iotbx.pdb.input(source_info = None, lines = flex.split_lines(
        open(pdb_file).read()))
  pdb_hierarchy = pdb_inp.construct_hierarchy()
  new_map_si = auto_sharpen_map_or_map_coeffs(
        resolution = resolution,
        crystal_symmetry = crystal_symmetry,
        map = map,
        pdb_hierarchy = pdb_hierarchy,
        soft_mask = False,
        box_in_auto_sharpen = False,
        remove_aniso = False,
        out = null_out())

  new_map = new_map_si.as_map_data().as_double()
  original_map = map.as_double()

  # Get cc of new map and old map with model in hierarchy
  cc_original = get_cc(original_map)
  cc_new = get_cc(new_map)
  text = "CC original %7.2f   After sharpening: %7.2f " %(cc_original, cc_new)
  print(text)
  assert text.replace(" ", "")  ==  "CC original    0.65   After sharpening:    0.70".replace(" ", "")


  print("OK")

def tst_07():

  print("\n"+80*"-")
  print("Testing auto_sharpen with soft_mask and box")
  print("\n"+80*"-")

  new_map_si = auto_sharpen_map_or_map_coeffs(
        resolution = resolution,
        crystal_symmetry = crystal_symmetry,
        map = map,
        box_in_auto_sharpen = True,
        soft_mask = True, region_weight_method = "delta_ratio",
        auto_sharpen_methods = ["b_iso"],
        remove_aniso = True,
        out = null_out()
        )
  new_map = new_map_si.as_map_data().as_double()
  original_map = map.as_double()

  # Get cc of new map and old map with model in hierarchy
  cc_original = get_cc(original_map)
  cc_new = get_cc(new_map)
  text = "CC original %7.2f   After sharpening: %7.2f " %(cc_original, cc_new)
  print(text)
  assert text.replace(" ", "")  ==  "CC original    0.65   After sharpening:    0.57".replace(" ", "")


def tst_08():

  print("\n"+80*"-")
  print("Testing auto_sharpen with soft_mask and box and pdb")
  print("\n"+80*"-")
  pdb_inp = iotbx.pdb.input(source_info = None, lines = flex.split_lines(
        open(pdb_file).read()))
  pdb_hierarchy = pdb_inp.construct_hierarchy()
  new_map_si = auto_sharpen_map_or_map_coeffs(
        resolution = resolution,
        crystal_symmetry = crystal_symmetry,
        map = map,
        box_in_auto_sharpen = True,
        pdb_hierarchy = pdb_hierarchy,
        soft_mask = True, region_weight_method = "delta_ratio",
        auto_sharpen_methods = ["b_iso"],
        remove_aniso = True,
        out = null_out()
        )
  new_map = new_map_si.as_map_data().as_double()
  original_map = map.as_double()

  # Get cc of new map and old map with model in hierarchy
  cc_original = get_cc(original_map)
  cc_new = get_cc(new_map)
  text = "CC original %7.2f   After sharpening: %7.2f " %(cc_original, cc_new)
  print(text)
  assert text.replace(" ", "")  ==  "CC original    0.65   After sharpening:    0.69".replace(" ", "")

def tst_09():

  print("\n"+80*"-")
  print("Testing auto_sharpen with b_iso and remove_aniso and local sharpening")
  print("\n"+80*"-")
  new_map_si = auto_sharpen_map_or_map_coeffs(
        resolution = resolution,
        crystal_symmetry = crystal_symmetry,
        map = map,
        k_sharpen = 12,
        box_in_auto_sharpen = False,
        soft_mask = False,
        auto_sharpen_methods = ["b_iso_to_d_cut"],
        remove_aniso = True,
        overall_before_local = False,
        local_sharpening = True,
        sequence = "AAAAAAAAAAAAAAAAAA",
        out = null_out()

        )
  new_map = new_map_si.as_map_data().as_double()
  original_map = map.as_double()

  # Get cc of new map and old map with model in hierarchy
  cc_original = get_cc(original_map)
  cc_new = get_cc(new_map)
  text = "CC original %7.2f   After sharpening: %7.2f " %(cc_original, cc_new)
  print(text)
  assert text.replace(" ", "")  ==  "CC original    0.65   After sharpening:    0.62".replace(" ", "")


if __name__ == "__main__":
  t0 = time.time()
  tst_01()
  tst_02()
  tst_03()
  tst_04()
  tst_05()
  tst_06()
  tst_07()
  tst_08()
  tst_09()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
