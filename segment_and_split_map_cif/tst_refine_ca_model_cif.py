from __future__ import division
from __future__ import print_function
import os
import libtbx.load_env


import time

data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="segment_and_split_map_cif",
  test=os.path.isdir)

ca_cb_pdb=os.path.join(data_dir,"ca_cb_box.cif")
ca_pdb=os.path.join(data_dir,"ca_box.cif")
small_map_file=os.path.join(data_dir,"ca_cb_box.ccp4")

def tst_01():
  print("testing refine_ca_model with small model and map")
  args=[
          "%s" %(small_map_file),
          "%s" %(ca_cb_pdb),
         "resolution=3",]
  from iotbx.cli_parser import run_program
  from phenix.programs import refine_ca_model as run

  results=run_program(program_class=run.Program,args=args)

  print("Final model",\
     results.final_model.get_hierarchy().overall_counts().n_residues)
  print("Final score",\
     results.final_score)
  assert results.final_model.get_hierarchy().overall_counts().n_residues ==20
  assert results.final_score <= 5
  print("OK")

def tst_02():
  print("testing refine_ca_model with small model and map and no CB atoms")
  args=[
          "%s" %(small_map_file),
          "%s" %(ca_pdb),
         "resolution=3",]
  from iotbx.cli_parser import run_program
  from phenix.programs import refine_ca_model as run

  results=run_program(program_class=run.Program,args=args)

  print("Final model",\
     results.final_model.get_hierarchy().overall_counts().n_residues)
  print("Final score",\
     results.final_score)
  assert results.final_model.get_hierarchy().overall_counts().n_residues ==20
  assert results.final_score <= 5
  print("OK")

if __name__=="__main__":
  t0 = time.time()
  tst_01()
  tst_02()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
