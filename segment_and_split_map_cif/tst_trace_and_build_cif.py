from __future__ import division
from __future__ import print_function
import sys,os
import libtbx.load_env

from phenix.autosol.trace_and_build import utils
from libtbx import group_args

import time

data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="segment_and_split_map_cif",
  test=os.path.isdir)

small_pdb=os.path.join(data_dir,"strand.cif")
small_map_file=os.path.join(data_dir,"strand.ccp4")
second_small_map_file=os.path.join(data_dir,'small_map.ccp4')

def tst_01():
  print("testing trace_and_build with small model and map")
  args=["%s" %(small_map_file),
         "refine_cycles=1",
         "resolution=2.5",]
  from iotbx.cli_parser import run_program
  from phenix.programs import trace_and_build as run
  if (not os.path.isdir("trace_and_build_cif")):
    os.mkdir("trace_and_build_cif")
  os.chdir("trace_and_build_cif")

  results=run_program(program_class=run.Program,args=args)

  n_residues=results.n_residues
  final_score=results.final_score
  print("Final model",\
     n_residues,final_score)
  assert n_residues >= 5
  assert final_score > 2

  print("OK")

class run(utils):
 def __init__(self):
  from iotbx.data_manager import DataManager
  dm = DataManager()
  mm = dm.get_real_map(second_small_map_file)
  mm.shift_origin()
  mm.origin_shift_grid_units = (0,0,0)
  from cctbx.maptbx.box import with_bounds
  box = with_bounds(mm,
       lower_bounds = (0,0,0),
       upper_bounds = (20,15,15),)
  mm = box.map_manager()
  self.map_manager = mm
  self.log= sys.stdout
  self.crystal_symmetry= mm.crystal_symmetry()
  self.params=group_args(
    group_args_type = 'params',
    output_files = group_args(
      group_args_type = 'output_files',
      resampling_ratio_to_resolution = 0.2,
      resample_on_fine_grid=True,
      restore_full_size = False,
    ),
    crystal_info = group_args(
      group_args_type = 'crystal_info',
      resolution = 3,
    ),
    control= group_args(
      group_args_type = 'control',
      verbose= False,
    ),
   )
  self.write_demo_map(mm.map_data(),
      'test.ccp4', origin_cart = (0,0,0),
      unit_cell_grid = mm.unit_cell_grid,
      crystal_symmetry = mm.crystal_symmetry())
  assert mm.map_data().origin() == (0,0,0)
  print(mm.map_data().all())
  assert mm.map_data().all() == (21,16,16)
  new_mm = dm.get_real_map("test.ccp4")
  new_mm.shift_origin()
  print(new_mm.map_data().all())
  assert new_mm.map_data().all() == (26, 20, 20)


def tst_02():
  run()
  print("OK")

if __name__=="__main__":
  t0 = time.time()
  tst_02()
  tst_01()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
