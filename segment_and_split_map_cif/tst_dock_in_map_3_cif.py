from __future__ import division
from __future__ import print_function
import os
import libtbx.load_env
from libtbx.test_utils import approx_equal

import time

data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="segment_and_split_map_cif",
  test=os.path.isdir)

small_first_part=os.path.join(data_dir,"placed_model_1-20.cif")
small_second_part=os.path.join(data_dir,"placed_model_21-30.cif")
small_pdb=os.path.join(data_dir,"small_model.cif")
small_pdb_two_models=os.path.join(data_dir,"small_model_two_models.cif")
small_start_pdb=os.path.join(data_dir,"start.cif")
small_map_file=os.path.join(data_dir,"small_map.ccp4")

def tst_01():
  print("testing dock_in_map with small model and map")
  args=["rigid_body_refinement=False","nproc=1","%s" %(small_map_file),
        "%s" %(small_first_part),
         "align_moments=False",
        "fixed_model=%s" %(small_second_part),
        "skip_fixed_model_in_pdb_out=False",
         "output_files.temp_dir=dom_3",
         "resolution=3",]
  try:
    from iotbx.cli_parser import run_program
    from phenix.programs import dock_in_map as run
  except Exception as e:
    print("Dock in map not available...skipping")
    return
  print("phenix.dock_in_map %s" %(" ".join(args)))
  results=run_program(program_class=run.Program,args=args)

  final_model=results.final_model
  final_cc=results.final_cc
  print("Final CC:",final_cc)
  assert approx_equal(float("%.1f" %(final_cc+0.05)),1.0,eps=0.2)
  assert final_model.get_hierarchy().overall_counts().n_residues > 20
  assert final_model.get_hierarchy().overall_counts().n_residues < 35
  print("OK")

if __name__=="__main__":
  t0 = time.time()
  tst_01()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
