from __future__ import division
from __future__ import print_function
import os
import libtbx.load_env


import time

data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="segment_and_split_map_cif",
  test=os.path.isdir)

target_pdb=os.path.join(data_dir,"model_A30_B30.cif")
rotated_pdb=os.path.join(data_dir,"model_A30_B30_rt.cif")
map_poor_at_b=os.path.join(data_dir,"model_A30_B200.ccp4")
map_poor_at_a=os.path.join(data_dir,"model_A200_B30_rt.ccp4")

map_good_first_half=os.path.join(data_dir,"model_A30_B30_first_half_good.ccp4")
map_good_second_half=os.path.join(data_dir,"model_A30_B30_rt_second_half_good.ccp4")

def tst_01():
  print("testing combine_focused_maps ")
  args=["nproc=1","map_file=%s" %(map_poor_at_b),
        "map_file=%s" %(map_poor_at_a),
        "model_file=%s" %(target_pdb),
         "resolution=3",
         "rigid_body_refinement=False"]
  try:
    from iotbx.cli_parser import run_program
    from phenix.programs import combine_focused_maps as run
  except Exception as e:
    print("Combine focused maps not available...skipping")
    return
  print("phenix.combine_focused_maps %s" %(" ".join(args)))
  results=run_program(program_class=run.Program,args=args)

def tst_02():
  print("testing combine_focused_maps ")
  args=["map_file=%s" %(map_good_first_half),
        "map_file=%s" %(map_good_second_half),
        "model_file=%s" %(target_pdb),
         "resolution=3",]
  try:
    from iotbx.cli_parser import run_program
    from phenix.programs import combine_focused_maps as run
  except Exception as e:
    print("Combine focused maps not available...skipping")
    return

  results=run_program(program_class=run.Program,args=args)

  assert results.final_cc > 0.55 and results.final_cc < 0.75

def tst_03():
  print("testing combine_focused_maps ")
  args=["map_file=%s" %(map_good_first_half),
        "map_file=%s" %(map_good_second_half),
        "model_file=%s" %(target_pdb),
        "local_weight=True",
        "local_residues=2",
         "resolution=3",]
  try:
    from iotbx.cli_parser import run_program
    from phenix.programs import combine_focused_maps as run
  except Exception as e:
    print("Combine focused maps not available...skipping")
    return

  results=run_program(program_class=run.Program,args=args)

  assert results.final_cc > 0.55 and results.final_cc < 0.75

def tst_04():
  print("testing combine_focused_maps with half-maps")
  args=["map_file=%s" %(map_good_first_half),
        "map_file=%s" %(map_good_second_half),
        "model_file=%s" %(target_pdb),
        "half_map_1_file=%s" %(map_good_first_half),
        "half_map_2_file=%s" %(map_good_second_half),
        "half_map_1_file=%s" %(map_good_second_half),
        "half_map_2_file=%s" %(map_good_first_half),
         "resolution=3",]
  try:
    from iotbx.cli_parser import run_program
    from phenix.programs import combine_focused_maps as run
  except Exception as e:
    print("Combine focused maps not available...skipping")
    return

  results=run_program(program_class=run.Program,args=args)

  assert results.final_cc > 0.55 and results.final_cc < 0.75
  assert results.final_cc_1 > 0.55
  assert results.final_cc_2 < 0.75

def tst_05():
  print("testing combine_focused_maps  with scale factors")
  args=["map_file=%s" %(map_good_first_half),
        "map_file=%s" %(map_good_second_half),
        "model_file=%s" %(target_pdb),
        "map_scale=2",
        "map_scale=3",
         "resolution=3",]
  try:
    from iotbx.cli_parser import run_program
    from phenix.programs import combine_focused_maps as run
  except Exception as e:
    print("Combine focused maps not available...skipping")
    return

  results=run_program(program_class=run.Program,args=args)

  assert results.final_cc > 0.55 and results.final_cc < 0.75

if __name__=="__main__":
  t0 = time.time()
  if not os.path.isdir('tst_combine_focused_maps_cif'):
    os.mkdir('tst_combine_focused_maps_cif')
  os.chdir('tst_combine_focused_maps_cif')
  tst_01()
  tst_02()
  tst_03()
  tst_04()
  tst_05()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
