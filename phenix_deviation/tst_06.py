from __future__ import absolute_import, division, print_function
import os
import iotbx.pdb
import mmtbx
from libtbx.utils import null_out
from phenix.programs import deviation
from libtbx.test_utils import approx_equal
from libtbx import easy_run

global_suffix = "_phenix_deviation"

pdb_in="""
CRYST1   18.653   13.767   15.342  90.00  90.00  90.00 P 21 21 21
ATOM      1  N   ARG A  17       5.498   8.753   6.054  1.00 10.00           N
ATOM      2  CA  ARG A  17       6.580   7.612   5.854  1.00 10.00           C
ATOM      3  C   ARG A  17       5.922   6.350   5.590  0.00 10.00           C
ATOM      4  O   ARG A  17       4.974   5.958   6.261  1.00 10.00           O
ATOM      5  CB  ARG A  17       7.549   7.672   6.919  1.00 10.00           C
ATOM      6  CG  ARG A  17       8.782   6.783   6.863  1.00 10.00           C
ATOM      7  CD  ARG A  17       9.603   6.591   8.071  1.00 10.00           C
ATOM      8  NE  ARG A  17      10.874   6.043   7.724  1.00 10.00           N
ATOM      9  CZ  ARG A  17      11.847   5.961   8.684  1.00 10.00           C
ATOM     10  NH1 ARG A  17      11.685   6.757   9.836  1.00 10.00           N
ATOM     11  NH2 ARG A  17      13.012   5.415   8.285  1.00 10.00           N
ATOM     12  HA  ARG A  17       7.131   7.946   5.008  1.00 10.00           H
ATOM     13  HB2 ARG A  17       7.708   8.480   7.196  1.00 10.00           H
ATOM     14  HB3 ARG A  17       6.931   7.258   7.782  1.00 10.00           H
ATOM     15  HG2 ARG A  17       8.419   5.897   6.503  1.00 10.00           H
ATOM     16  HG3 ARG A  17       9.249   7.029   6.039  1.00 10.00           H
ATOM     17  HD2 ARG A  17       9.730   7.476   8.366  1.00 10.00           H
ATOM     18  HD3 ARG A  17       9.032   6.140   8.644  1.00 10.00           H
ATOM     19  HE  ARG A  17      10.855   5.659   7.011  0.00 10.00           H
ATOM     20 HH11 ARG A  17      10.994   7.143   9.922  1.00 10.00           H
ATOM     21 HH12 ARG A  17      12.487   6.607  10.408  1.00 10.00           H
ATOM     22 HH21 ARG A  17      13.126   4.947   7.646  1.00 10.00           H
ATOM     23 HH22 ARG A  17      13.720   5.385   8.815  1.00 10.00           H
TER
END
"""
segid_list = ['299 ', '302 ', '15N ', '299 ', '307 ', '305 ', '284 ', '310 ',
              '315 ', '270 ', '303 ', '198 ', '199 ', '214 ', '201 ', '200 ',
              '207 ', '205 ', '101N', '194 ', '197 ', '196 ', '187 ']

def run(prefix = os.path.basename(__file__).replace(".py","")+global_suffix):
  """
  Validate put peak value in atom segid.
  """
  # Prepare input model
  pdb_inp = iotbx.pdb.input(source_info=None, lines=pdb_in)
  model = mmtbx.model.manager(model_input=pdb_inp, log=null_out())
  model.process(make_restraints=True)
  model.idealize_h_riding()
  with open("%s.pdb"%prefix, "w") as of:
    print (model.model_as_pdb(), file=of)
  #
  cmd = " ".join([
    "phenix.fmodel",
    "%s.pdb"%prefix,
    "type=real",
    "label=Fobs",
    "high_res=0.7",
    "output.file_name=%s.mtz"%prefix
    ])
  assert not easy_run.call(cmd)
  assert not easy_run.call("phenix.deviation %s.pdb %s.mtz nproc=1"%(prefix, prefix))
  refined_atoms = iotbx.pdb.input(file_name="%s_refined.pdb"%prefix).atoms()
#  for refined_atom, seg in zip(refined_atoms, segid_list):
#    assert refined_atom.segid[:2] == seg[:2]

if (__name__ == "__main__"):
  run()
  print ("OK")
