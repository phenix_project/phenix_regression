from __future__ import absolute_import, division, print_function
import os
import iotbx.pdb
from phenix.programs import deviation
from libtbx import easy_run
import mmtbx.model
from libtbx.utils import null_out
import iotbx.cif
from scitbx.array_family import flex

global_suffix = "_phenix_deviation"

ligand_cif="""
#
data_comp_list
loop_
_chem_comp.id
_chem_comp.three_letter_code
_chem_comp.name
_chem_comp.group
_chem_comp.number_atoms_all
_chem_comp.number_atoms_nh
_chem_comp.desc_level
00  00  "Unknown                  " ligand 18 13 .
#
data_comp_00
#
loop_
_chem_comp_atom.comp_id
_chem_comp_atom.atom_id
_chem_comp_atom.type_symbol
_chem_comp_atom.type_energy
_chem_comp_atom.charge
_chem_comp_atom.partial_charge
_chem_comp_atom.x
_chem_comp_atom.y
_chem_comp_atom.z
00         C1      C   C      0 .          0.0000    0.0000    0.0000
00         C2      C   CH2    0 .          1.5267    0.0000    0.0000
00         C3      C   CT     0 .          2.1069    0.0000    1.4119
00         C4      C   CH2    0 .          3.6050    0.2909    1.3476
00         C5      C   C      0 .          4.2096    0.5580    2.7250
00         C6      C   C      0 .          1.8783   -1.3674    2.0519
00         O1      O   O      0 .         -0.6301   -1.0128    0.4033
00         O2      O   OC    -1 .         -0.6290    1.0129   -0.4030
00         O3      O   O      0 .          4.0403   -0.2657    3.6616
00         O4      O   OC    -1 .          4.8819    1.6036    2.9228
00         O5      O   O      0 .          1.1638   -1.4687    3.0834
00         O6      O   OC    -1 .          2.4033   -2.3941    1.5459
00         OX      O   OH1    0 .          1.4712    0.9862    2.1784
00         H22     H   HCH2   0 .          1.8773   -0.8814   -0.5232
00         H23     H   HCH2   0 .          1.8773    0.8814   -0.5232
00         H41     H   HCH2   0 .          3.7661    1.1595    0.7205
00         H42     H   HCH2   0 .          4.1079   -0.5597    0.9038
00         HX1     H   HOH1   0 .          1.6406    1.8342    1.8026
#
loop_
_chem_comp_bond.comp_id
_chem_comp_bond.atom_id_1
_chem_comp_bond.atom_id_2
_chem_comp_bond.type
_chem_comp_bond.value_dist
_chem_comp_bond.value_dist_esd
_chem_comp_bond.value_dist_neutron
00   C1      C2     single        1.527 0.020     1.527
00   C1      O1     deloc         1.259 0.020     1.259
00   C1      O2     deloc         1.259 0.020     1.259
00   C2      C3     single        1.527 0.020     1.527
00   C3      C4     single        1.527 0.020     1.527
00   C3      C6     single        1.527 0.020     1.527
00   C3      OX     single        1.401 0.020     1.401
00   C4      C5     single        1.527 0.020     1.527
00   C5      O3     deloc         1.259 0.020     1.259
00   C5      O4     deloc         1.259 0.020     1.259
00   C6      O5     deloc         1.259 0.020     1.259
00   C6      O6     deloc         1.259 0.020     1.259
00   H22     C2     single        0.970 0.020     1.083
00   H23     C2     single        0.970 0.020     1.083
00   H41     C4     single        0.970 0.020     1.083
00   H42     C4     single        0.970 0.020     1.083
00   HX1     OX     single        0.850 0.020     0.943
#
loop_
_chem_comp_angle.comp_id
_chem_comp_angle.atom_id_1
_chem_comp_angle.atom_id_2
_chem_comp_angle.atom_id_3
_chem_comp_angle.value_angle
_chem_comp_angle.value_angle_esd
00   O2      C1      O1           120.00 3.000
00   O2      C1      C2           120.00 3.000
00   O1      C1      C2           120.00 3.000
00   H23     C2      H22          108.92 3.000
00   H23     C2      C3           108.90 3.000
00   H22     C2      C3           108.90 3.000
00   H23     C2      C1           108.89 3.000
00   H22     C2      C1           108.89 3.000
00   C3      C2      C1           112.29 3.000
00   OX      C3      C6           109.47 3.000
00   OX      C3      C4           109.47 3.000
00   C6      C3      C4           109.47 3.000
00   OX      C3      C2           109.47 3.000
00   C6      C3      C2           109.47 3.000
00   C4      C3      C2           109.47 3.000
00   H42     C4      H41          108.92 3.000
00   H42     C4      C5           108.89 3.000
00   H41     C4      C5           108.89 3.000
00   H42     C4      C3           108.90 3.000
00   H41     C4      C3           108.90 3.000
00   C5      C4      C3           112.29 3.000
00   O4      C5      O3           120.00 3.000
00   O4      C5      C4           120.00 3.000
00   O3      C5      C4           120.00 3.000
00   O6      C6      O5           120.00 3.000
00   O6      C6      C3           120.00 3.000
00   O5      C6      C3           120.00 3.000
00   HX1     OX      C3           109.48 3.000
#
loop_
_chem_comp_tor.comp_id
_chem_comp_tor.id
_chem_comp_tor.atom_id_1
_chem_comp_tor.atom_id_2
_chem_comp_tor.atom_id_3
_chem_comp_tor.atom_id_4
_chem_comp_tor.value_angle
_chem_comp_tor.value_angle_esd
_chem_comp_tor.period
00  Var_01        C4      C3      C2      C1          -168.34  30.0 3
00  Var_02        C6      C3      C2      C1            71.60  30.0 3
00  Var_03        OX      C3      C2      C1           -48.28  30.0 3
00  Var_04        C5      C4      C3      C2           169.62  30.0 3
00  Var_05        O5      C6      C3      C2          -117.45  30.0 3
00  Var_06        O6      C6      C3      C2            62.44  30.0 3
00  Var_07        O1      C1      C2      C3           -68.29  30.0 2
00  Var_08        O2      C1      C2      C3           111.70  30.0 2
00  Var_09        O3      C5      C4      C3            52.95  30.0 3
00  Var_10        O4      C5      C4      C3          -127.23  30.0 3
00  Var_11        O5      C6      C3      C4           122.54  30.0 3
00  Var_12        O6      C6      C3      C4           -57.57  30.0 3
00  Var_13        C6      C3      C4      C5           -70.49  30.0 1
00  Var_14        OX      C3      C4      C5            49.57  30.0 1
00  Var_15        OX      C3      C6      O5             2.45  30.0 2
00  Var_16        OX      C3      C6      O6          -177.66  30.0 2
00  Var_17        H41     C4      C3      C2            48.88  30.0 3
00  Var_18        H42     C4      C3      C2           -69.64  30.0 3
00  Var_19        H22     C2      C3      C4            70.97  30.0 1
00  Var_20        H23     C2      C3      C4           -47.65  30.0 1
00  Var_21        H22     C2      C3      C6           -49.09  30.0 1
00  Var_22        H23     C2      C3      C6          -167.71  30.0 1
00  Var_23        H41     C4      C3      C6           168.77  30.0 3
00  Var_24        H42     C4      C3      C6            50.25  30.0 3
00  Var_25        H22     C2      C1      O1            52.41  30.0 3
00  Var_26        H23     C2      C1      O1           171.02  30.0 3
00  Var_27        H22     C2      C1      O2          -127.61  30.0 3
00  Var_28        H23     C2      C1      O2            -8.99  30.0 3
00  Var_29        H41     C4      C5      O3           173.69  30.0 2
00  Var_30        H42     C4      C5      O3           -67.80  30.0 2
00  Var_31        H41     C4      C5      O4            -6.48  30.0 2
00  Var_32        H42     C4      C5      O4           112.03  30.0 2
00  Var_33        H22     C2      C3      OX          -168.97  30.0 1
00  Var_34        H23     C2      C3      OX            72.41  30.0 1
00  Var_35        H41     C4      C3      OX           -71.17  30.0 3
00  Var_36        H42     C4      C3      OX           170.31  30.0 3
#
loop_
_chem_comp_plane_atom.comp_id
_chem_comp_plane_atom.plane_id
_chem_comp_plane_atom.atom_id
_chem_comp_plane_atom.dist_esd
00  plan-1  C1     0.020
00  plan-1  C2     0.020
00  plan-1  O1     0.020
00  plan-1  O2     0.020
00  plan-2  C4     0.020
00  plan-2  C5     0.020
00  plan-2  O3     0.020
00  plan-2  O4     0.020
00  plan-3  C3     0.020
00  plan-3  C6     0.020
00  plan-3  O5     0.020
00  plan-3  O6     0.020
"""

pdb_in = """
CRYST1   14.611   14.481   14.720  90.00  90.00  90.00 P 21 21 21
SCALE1      0.068442  0.000000  0.000000        0.00000
SCALE2      0.000000  0.069056  0.000000        0.00000
SCALE3      0.000000  0.000000  0.067935        0.00000
HETATM    1  C1  00  A 450       9.179   7.816   8.858  1.00  3.55           C
HETATM    2  C2  00  A 450       8.949   7.405   7.405  1.00  4.50           C
HETATM    3  C3  00  A 450       7.471   7.267   7.047  1.00  4.83           C
HETATM    4  C4  00  A 450       7.337   6.659   5.652  1.00  5.20           C
HETATM    5  C5  00  A 450       5.902   6.254   5.326  1.00  5.80           C
HETATM    6  C6  00  A 450       6.812   8.644   7.065  1.00  4.77           C
HETATM    7  O1  00  A 450       8.854   8.969   9.245  1.00  4.07           O
HETATM    8  O2  00  A 450       9.693   7.001   9.668  1.00  3.60           O
HETATM    9  O3  00  A 450       5.532   6.158   4.127  1.00  5.95           O
HETATM   10  O4  00  A 450       5.088   6.017   6.257  1.00  7.53           O
HETATM   11  O5  00  A 450       5.884   8.888   7.881  1.00  5.33           O
HETATM   12  O6  00  A 450       7.196   9.537   6.265  1.00  5.42           O
HETATM   13  OX  00  A 450       6.840   6.435   7.981  1.00  5.58           O
END
"""

pdb_answer = """
CRYST1   14.611   14.481   14.720  90.00  90.00  90.00 P 21 21 21
HETATM    1  C1  00  A 450       9.179   7.816   8.858  1.00  3.55           C
HETATM    2  C2  00  A 450       8.949   7.405   7.405  1.00  4.50           C
HETATM    3  C3  00  A 450       7.471   7.267   7.047  1.00  4.83           C
HETATM    4  C4  00  A 450       7.337   6.659   5.652  1.00  5.20           C
HETATM    5  C5  00  A 450       5.902   6.254   5.326  1.00  5.80           C
HETATM    6  C6  00  A 450       6.812   8.644   7.065  1.00  4.77           C
HETATM    7  O1  00  A 450       8.854   8.969   9.245  1.00  4.07           O
HETATM    8  O2  00  A 450       9.693   7.001   9.668  1.00  3.60           O
HETATM    9  O3  00  A 450       5.532   6.158   4.127  1.00  5.95           O
HETATM   10  O4  00  A 450       5.088   6.017   6.257  1.00  7.53           O
HETATM   11  O5  00  A 450       5.884   8.888   7.881  1.00  5.33           O
HETATM   12  O6  00  A 450       7.196   9.537   6.265  1.00  5.42           O
HETATM   13  OX  00  A 450       6.840   6.435   7.981  1.00  5.58           O
HETATM   14  H22 00  A 450       9.387   6.553   7.250  1.00  4.50           H
HETATM   15  H23 00  A 450       9.348   8.074   6.826  1.00  4.50           H
HETATM   16  H41 00  A 450       7.635   7.309   4.997  1.00  5.20           H
HETATM   17  H42 00  A 450       7.904   5.874   5.597  1.00  5.20           H
HETATM   18  HX1 00  A 450       7.424   6.084   8.488  1.00  5.58           H
END
"""
def get_r(f1,f2):
  x1 = iotbx.pdb.input(file_name=f1).xray_structure_simple()
  x2 = iotbx.pdb.input(file_name=f2).xray_structure_simple()
  fo1 = x1.structure_factors(d_min=0.7).f_calc()
  fo2 = fo1.structure_factors_from_scatterers(xray_structure=x2).f_calc()
  fo1 = flex.abs(fo1.data())
  fo2 = flex.abs(fo2.data())
  return flex.sum(flex.abs(fo1-fo2))/flex.sum(flex.abs(fo1+fo2)) * 2

def run(prefix = os.path.basename(__file__).replace(".py","")+global_suffix):
  """
  Handling unknown ligands
  """
  with open("%s.pdb"%prefix, "w") as of:
    print (pdb_in, file=of)
  #
  pdb_inp = iotbx.pdb.input(source_info=None, lines=pdb_answer)
  cif_object = iotbx.cif.reader(input_string = ligand_cif).model()
  cif_objects = [('bla.cif', cif_object)]
  model = mmtbx.model.manager(
    model_input       = pdb_inp,
    restraint_objects = cif_objects,
    log               = null_out())
  model.process(make_restraints=True)
  model.idealize_h_riding()
  with open("%s_answer.pdb"%prefix, "w") as of:
    print (model.model_as_pdb(), file=of)
  #
  with open("%s.cif"%prefix, "w") as of:
    print (ligand_cif, file=of)
  cmd = " ".join([
    "phenix.fmodel",
    "%s_answer.pdb"%prefix,
    "type=real",
    "label=Fobs",
    "high_res=0.7",
    "output.file_name=%s.mtz"%prefix,
    "> %s.log"%prefix
    ])
  assert not easy_run.call(cmd)
  print('\n\t%s\n' % cmd)
  cmd = " ".join([
    "phenix.deviation",
    "%s.pdb %s.mtz %s.cif"%(prefix,prefix,prefix),
    "nproc=1",
    ])
  print('\n\t%s\n' % cmd)
  assert not easy_run.call(cmd)
  atoms_1 = iotbx.pdb.input(file_name="%s_refined.pdb"%prefix).atoms()
  atoms_2 = iotbx.pdb.input(source_info=None, lines=pdb_answer).atoms()
  assert atoms_1.size() == atoms_2.size()
  for a1, a2 in zip(atoms_1, atoms_2):
    assert a1.name == a2.name
    d = a1.distance(a2)
    #assert d < 0.03, [d, a1.name, a2.name] # Symmetric molecule, atoms can flip randomly
    assert a1.segid.strip()[-1]=="Y"
    assert a2.segid.strip()    ==""
  r = get_r("%s_refined.pdb"%prefix, "%s_answer.pdb"%prefix)
  print (r)

if (__name__ == "__main__"):
  run()
  print ("OK")
