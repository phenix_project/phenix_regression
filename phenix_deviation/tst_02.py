from __future__ import absolute_import, division, print_function
import os
import iotbx.pdb
from phenix.programs import deviation
from libtbx import easy_run
from scitbx.array_family import flex
import mmtbx.model
from libtbx.utils import null_out

global_suffix = "_phenix_deviation"

pdb_answer = """
CRYST1   18.947   20.235   17.510  90.00  90.00  90.00 P 21 21 21
ATOM      1  N   MET L   4      13.273  11.731   8.169  1.00  1.00           N
ATOM      2  CA  MET L   4      11.872  11.727   8.572  1.00  1.00           C
ATOM      3  C   MET L   4      11.369  10.300   8.758  1.00  1.00           C
ATOM      4  O   MET L   4      10.726   9.737   7.873  1.00  1.00           O
ATOM      5  CB  MET L   4      11.014  12.455   7.535  1.00  1.00           C
ATOM      6  CG  MET L   4      11.185  13.965   7.542  1.00  1.00           C
ATOM      7  SD  MET L   4      10.302  14.761   8.897  1.00  1.00           S
ATOM      8  CE  MET L   4       8.643  14.830   8.226  1.00  1.00           C
ATOM      9  HA  MET L   4      11.787  12.201   9.414  1.00  1.00           H
ATOM     10  HB2 MET L   4      11.253  12.134   6.652  1.00  1.00           H
ATOM     11  HB3 MET L   4      10.080  12.263   7.713  1.00  1.00           H
ATOM     12  HG2 MET L   4      12.128  14.176   7.633  1.00  1.00           H
ATOM     13  HG3 MET L   4      10.844  14.326   6.709  1.00  1.00           H
ATOM     14  HE1 MET L   4       8.057  15.247   8.877  1.00  1.00           H
ATOM     15  HE2 MET L   4       8.655  15.351   7.408  1.00  1.00           H
ATOM     16  HE3 MET L   4       8.341  13.927   8.039  1.00  1.00           H
ATOM     17  N   THR L   5      11.669   9.718   9.917  1.00  1.00           N
ATOM     18  CA  THR L   5      11.243   8.355  10.211  1.00  1.00           C
ATOM     19  C   THR L   5       9.739   8.325  10.455  1.00  1.00           C
ATOM     20  O   THR L   5       9.240   8.967  11.385  1.00  1.00           O
ATOM     21  CB  THR L   5      11.993   7.818  11.428  1.00  1.00           C
ATOM     22  OG1 THR L   5      13.348   8.282  11.403  0.20  1.00           O
ATOM     23  CG2 THR L   5      11.980   6.295  11.434  1.00  1.00           C
ATOM     24  H   THR L   5      12.117  10.092  10.549  1.00  1.00           H
ATOM     25  HA  THR L   5      11.442   7.785   9.452  1.00  1.00           H
ATOM     26  HB  THR L   5      11.560   8.131  12.237  1.00  1.00           H
ATOM     27  HG1 THR L   5      13.389   9.030  11.021  1.00  1.00           H
ATOM     28 HG21 THR L   5      11.544   5.971  12.237  1.00  1.00           H
ATOM     29 HG22 THR L   5      11.499   5.965  10.659  1.00  1.00           H
ATOM     30 HG23 THR L   5      12.888   5.956  11.409  1.00  1.00           H
ATOM     31  N   GLN L   6       9.020   7.581   9.621  1.00  1.00           N
ATOM     32  CA  GLN L   6       7.572   7.470   9.749  1.00  1.00           C
ATOM     33  C   GLN L   6       7.199   6.610  10.952  1.00  1.00           C
ATOM     34  O   GLN L   6       7.450   5.405  10.967  1.00  1.00           O
ATOM     35  CB  GLN L   6       6.965   6.881   8.474  1.00  1.00           C
ATOM     36  CG  GLN L   6       7.020   7.814   7.275  1.00  1.00           C
ATOM     37  CD  GLN L   6       6.570   7.143   5.993  1.00  1.00           C
ATOM     38  OE1 GLN L   6       7.323   7.061   5.023  1.00  1.00           O
ATOM     39  NE2 GLN L   6       5.334   6.657   5.982  1.00  1.00           N
ATOM     40  H   GLN L   6       9.349   7.128   8.969  1.00  1.00           H
ATOM     41  HA  GLN L   6       7.195   8.354   9.882  1.00  1.00           H
ATOM     42  HB2 GLN L   6       7.449   6.073   8.243  1.00  1.00           H
ATOM     43  HB3 GLN L   6       6.033   6.669   8.641  1.00  1.00           H
ATOM     44  HG2 GLN L   6       6.438   8.573   7.438  1.00  1.00           H
ATOM     45  HG3 GLN L   6       7.934   8.116   7.151  1.00  1.00           H
ATOM     46 HE21 GLN L   6       4.836   6.732   6.679  1.00  1.00           H
ATOM     47 HE22 GLN L   6       5.031   6.267   5.278  1.00  1.00           H
TER
END
"""

pdb_in = """
CRYST1   18.947   20.235   17.510  90.00  90.00  90.00 P 21 21 21    0
ATOM      1  N   MET L   4      13.273  11.731   8.169  1.00  1.00           N
ATOM      2  CA  MET L   4      11.872  11.727   8.572  1.00  1.00           C
ATOM      3  C   MET L   4      11.369  10.300   8.758  1.00  1.00           C
ATOM      4  O   MET L   4      10.726   9.737   7.873  1.00  1.00           O
ATOM      5  CB  MET L   4      11.014  12.455   7.535  1.00  1.00           C
ATOM      6  CG  MET L   4      11.185  13.965   7.542  1.00  1.00           C
ATOM      7  SD  MET L   4      10.302  14.761   8.897  1.00  1.00           S
ATOM      8  CE  MET L   4       8.643  14.830   8.226  1.00  1.00           C
ATOM     17  N   THR L   5      11.669   9.718   9.917  1.00  1.00           N
ATOM     18  CA  THR L   5      11.243   8.355  10.211  1.00  1.00           C
ATOM     19  C   THR L   5       9.739   8.325  10.455  1.00  1.00           C
ATOM     20  O   THR L   5       9.240   8.967  11.385  1.00  1.00           O
ATOM     21  CB  THR L   5      11.993   7.818  11.428  1.00  1.00           C
ATOM     22  OG1 THR L   5      13.348   8.282  11.403  0.20  1.00           O
ATOM     23  CG2 THR L   5      11.980   6.295  11.434  1.00  1.00           C
ATOM     31  N   GLN L   6       9.020   7.581   9.621  1.00  1.00           N
ATOM     32  CA  GLN L   6       7.572   7.470   9.749  1.00  1.00           C
ATOM     33  C   GLN L   6       7.199   6.610  10.952  1.00  1.00           C
ATOM     34  O   GLN L   6       7.450   5.405  10.967  1.00  1.00           O
ATOM     35  CB  GLN L   6       6.965   6.881   8.474  1.00  1.00           C
ATOM     36  CG  GLN L   6       7.020   7.814   7.275  1.00  1.00           C
ATOM     37  CD  GLN L   6       6.570   7.143   5.993  1.00  1.00           C
ATOM     38  OE1 GLN L   6       7.323   7.061   5.023  1.00  1.00           O
ATOM     39  NE2 GLN L   6       5.334   6.657   5.982  1.00  1.00           N
TER
END
"""

def get_r(f1,f2):
  x1 = iotbx.pdb.input(file_name=f1).xray_structure_simple()
  x2 = iotbx.pdb.input(file_name=f2).xray_structure_simple()
  fo1 = x1.structure_factors(d_min=2.).f_calc()
  fo2 = fo1.structure_factors_from_scatterers(xray_structure=x2).f_calc()
  fo1 = flex.abs(fo1.data())
  fo2 = flex.abs(fo2.data())
  print (flex.sum(flex.abs(fo1-fo2))/flex.sum(flex.abs(fo1+fo2)) * 2)
  return flex.sum(flex.abs(fo1-fo2))/flex.sum(flex.abs(fo1+fo2)) * 2

def run(prefix = os.path.basename(__file__).replace(".py","")+global_suffix):
  """
  THR treatement (rotatable H exercise)
  """
  pdb_inp = iotbx.pdb.input(source_info=None, lines=pdb_answer)
  model = mmtbx.model.manager(model_input=pdb_inp, log=null_out())
  model.process(make_restraints=True)
  model.idealize_h_riding()
  with open("%s_answer.pdb"%prefix, "w") as of:
    print (model.model_as_pdb(), file=of)
  with open("%s.pdb"%prefix, "w") as of:
    print (pdb_in, file=of)
  cmd = " ".join([
    "phenix.fmodel",
    "%s_answer.pdb"%prefix,
    "type=real",
    "label=Fobs",
    "high_res=0.7",
    "output.file_name=%s.mtz"%prefix
    ])
  assert not easy_run.call(cmd)
  assert not easy_run.call("phenix.deviation %s.pdb %s.mtz nproc=1"%(prefix, prefix))
  atoms_1 = iotbx.pdb.input(file_name="%s_refined.pdb"%prefix).atoms()
  atoms_2 = iotbx.pdb.input(file_name="%s_answer.pdb"%prefix).atoms()
  assert atoms_1.size() == atoms_2.size()
  moving = ["HG1","HG21","HG22","HG23"]
  for a1, a2 in zip(atoms_1, atoms_2):
    assert a1.name == a2.name
    d = a1.distance(a2)
    #print d
    #assert d < 0.05, d # This may fail due to symmetric flip of CH3 group
    assert a1.segid.strip()[-1] == "Y"
  r=get_r("%s_refined.pdb"%prefix, "%s_answer.pdb"%prefix)
#  assert r < 0.02 # best seen 0.0145


if (__name__ == "__main__"):
  run()
  print ("OK")
