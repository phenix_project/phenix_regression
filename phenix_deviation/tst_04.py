from __future__ import absolute_import, division, print_function
import os
import iotbx.pdb
from phenix.programs import deviation
from libtbx import easy_run

global_suffix = "_phenix_deviation"

pdb_str="""
CRYST1   18.653   13.767   15.342  90.00  90.00  90.00 P 21 21 21    0
ATOM      1  N   ARG A  17       5.498   8.753   6.054  1.00 15.00           N
ATOM      2  CA  ARG A  17       6.580   7.612   5.854  1.00 15.00           C
ATOM      3  C   ARG A  17       5.922   6.350   5.590  1.00 15.00           C
ATOM      4  O   ARG A  17       4.974   5.958   6.261  1.00 15.00           O
ATOM      5  CB  ARG A  17       7.549   7.672   6.919  1.00 15.00           C
ATOM      6  CG  ARG A  17       8.782   6.783   6.863  1.00 15.00           C
ATOM      7  CD  ARG A  17       9.603   6.591   8.071  1.00 15.00           C
ATOM      8  NE  ARG A  17      10.874   6.043   7.724  1.00 15.00           N
ATOM      9  CZ  ARG A  17      11.847   5.961   8.684  1.00 15.00           C
ATOM     10  NH1 ARG A  17      11.685   6.757   9.836  1.00 15.00           N
ATOM     11  NH2 ARG A  17      13.012   5.415   8.285  1.00 15.00           N
ATOM      0  HA  ARG A  17       7.121   7.740   5.059  1.00 15.00           H   new
ATOM      0  HB2 ARG A  17       7.856   8.590   6.981  1.00 15.00           H   new
ATOM      0  HB3 ARG A  17       7.084   7.472   7.746  1.00 15.00           H   new
ATOM      0  HG2 ARG A  17       8.493   5.906   6.568  1.00 15.00           H   new
ATOM      0  HG3 ARG A  17       9.363   7.138   6.172  1.00 15.00           H   new
ATOM      0  HD2 ARG A  17       9.723   7.439   8.526  1.00 15.00           H   new
ATOM      0  HD3 ARG A  17       9.146   5.999   8.689  1.00 15.00           H   new
ATOM      0  HE  ARG A  17      11.027   5.768   6.924  1.00 15.00           H   new
ATOM      0 HH11 ARG A  17      10.996   7.266   9.913  1.00 15.00           H   new
ATOM      0 HH12 ARG A  17      12.275   6.733  10.461  1.00 15.00           H   new
ATOM      0 HH21 ARG A  17      13.105   5.146   7.474  1.00 15.00           H   new
ATOM      0 HH22 ARG A  17      13.663   5.336   8.842  1.00 15.00           H   new
TER
END
"""

def run(prefix = os.path.basename(__file__).replace(".py","")+global_suffix):
  """
  Atoms are not expected to move if starting with exact model
  """
  with open("%s.pdb"%prefix, "w") as of:
    print (pdb_str, file=of)
  cmd = " ".join([
    "phenix.fmodel",
    "%s.pdb"%prefix,
    "type=real",
    "label=Fobs",
    "high_res=0.7",
    "output.file_name=%s.mtz"%prefix,
    "> %s.fmodel.log"%prefix
    ])
  assert not easy_run.call(cmd)
  #
  cmd = " ".join([
    "phenix.deviation",
    "%s.pdb"%prefix,
    "%s.mtz"%prefix,
    "nproc=1",
    ">%s.log"%prefix
  ])
  assert not easy_run.call(cmd)
  # check results
  atoms_1 = iotbx.pdb.input(file_name="%s_refined.pdb"%prefix).atoms()
  atoms_2 = iotbx.pdb.input(source_info=None, lines=pdb_str).atoms()
  assert atoms_1.size() == atoms_2.size()
  for a1, a2 in zip(atoms_1, atoms_2):
    assert a1.name == a2.name
    #print ( a1.distance(a2))
    assert a1.distance(a2) < 0.03, 'distance between %s and %s > 0.015 %0.3f' % (
      a1.quote(),
      a2.quote(),
      a1.distance(a2),
      )
    assert a1.segid.strip()[-1] == "Y"
    assert a2.segid.strip() == ""

if (__name__ == "__main__"):
  run()
  print ("OK")
