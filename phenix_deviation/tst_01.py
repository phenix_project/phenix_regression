from __future__ import absolute_import, division, print_function
import os
import iotbx.pdb
from phenix.programs import deviation
from libtbx import easy_run
import mmtbx.model
from libtbx.utils import null_out

global_suffix="_phenix_deviation"

pdb_x = """
CRYST1   13.207   13.125   17.346  90.00  90.00  90.00 P 21 21 21
ATOM      1  N   HIS L  93       5.000   7.080   7.230  1.00  5.00           N
ATOM      2  CA  HIS L  93       6.447   7.107   7.053  1.00  5.00           C
ATOM      3  C   HIS L  93       6.824   6.727   5.625  1.00  5.00           C
ATOM      4  O   HIS L  93       6.166   5.895   5.000  1.00  5.00           O
ATOM      5  CB  HIS L  93       7.126   6.162   8.046  1.00  5.00           C
ATOM      6  CG  HIS L  93       6.776   6.435   9.476  1.00  5.00           C
ATOM      7  ND1 HIS L  93       7.369   7.440  10.209  1.00  5.00           N
ATOM      8  CD2 HIS L  93       5.893   5.833  10.308  1.00  5.00           C
ATOM      9  CE1 HIS L  93       6.867   7.446  11.430  1.00  5.00           C
ATOM     10  NE2 HIS L  93       5.969   6.481  11.517  1.00  5.00           N
ATOM     11  HA  HIS L  93       6.771   8.006   7.222  1.00  5.00           H
ATOM     12  HB2 HIS L  93       6.859   5.252   7.844  1.00  5.00           H
ATOM     13  HB3 HIS L  93       8.088   6.250   7.954  1.00  5.00           H
ATOM     14  HD1 HIS L  93       7.972   7.979   9.916  1.00  5.00           H
ATOM     15  HD2 HIS L  93       5.339   5.116  10.100  1.00  5.00           H
ATOM     16  HE1 HIS L  93       7.105   8.031  12.114  1.00  5.00           H
ATOM     17  HE2 HIS L  93       5.507   6.290  12.217  1.00  5.00           H
END
"""

pdb_in = """
CRYST1   13.207   13.125   17.346  90.00  90.00  90.00 P 21 21 21
ATOM      1  N   HIS L  93       5.000   7.080   7.230  1.00  5.00           N
ATOM      2  CA  HIS L  93       6.447   7.107   7.053  1.00  5.00           C
ATOM      3  C   HIS L  93       6.824   6.727   5.625  1.00  5.00           C
ATOM      4  O   HIS L  93       6.166   5.895   5.000  1.00  5.00           O
ATOM      5  CB  HIS L  93       7.126   6.162   8.046  1.00  5.00           C
ATOM      6  CG  HIS L  93       6.776   6.435   9.476  1.00  5.00           C
ATOM      7  ND1 HIS L  93       7.369   7.440  10.209  1.00  5.00           N
ATOM      8  CD2 HIS L  93       5.893   5.833  10.308  1.00  5.00           C
ATOM      9  CE1 HIS L  93       6.867   7.446  11.430  1.00  5.00           C
ATOM     10  NE2 HIS L  93       5.969   6.481  11.517  1.00  5.00           N
END
"""

def run(prefix = os.path.basename(__file__).replace(".py","")+global_suffix):
  """
  HIS treatement (get all possible H)
  """
  # Prepare input model
  pdb_inp = iotbx.pdb.input(source_info=None, lines=pdb_x)
  model = mmtbx.model.manager(model_input=pdb_inp, log=null_out())
  model.process(make_restraints=True)
  model.idealize_h_riding()
  with open("%s_answer.pdb"%prefix, "w") as of:
    print (model.model_as_pdb(), file=of)
  #
  with open("%s.pdb"%prefix, "w") as of:
    print (pdb_in, file=of)
  cmd = " ".join([
    "phenix.fmodel",
    "%s_answer.pdb"%prefix,
    "type=real",
    "label=Fobs",
    "high_res=0.7",
    "output.file_name=%s.mtz"%prefix,
    "> %s.log"%prefix
    ])
  assert not easy_run.call(cmd)
  cmd = " ".join([
    "phenix.deviation",
    "%s.pdb %s.mtz"%(prefix,prefix),
    "nproc=1",
    ">%s.log"%prefix
    ])
  assert not easy_run.call(cmd)
  atoms_1 = iotbx.pdb.input(file_name="%s_refined.pdb"%prefix).atoms()
  atoms_2 = iotbx.pdb.input(file_name="%s_answer.pdb"%prefix).atoms()
  assert atoms_1.size() == atoms_2.size()
  for a1, a2 in zip(atoms_1, atoms_2):
    assert a1.name == a2.name
    d = a1.distance(a2)
    #print (d, a1.name)
    assert d < 0.0025, d
    assert a1.segid.strip()[-1] == "Y"

if (__name__ == "__main__"):
  run()
  print ("OK")
