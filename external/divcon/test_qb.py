from __future__ import print_function
import os, sys
import StringIO

from libtbx import easy_run
import libtbx.load_env

phenix_path = libtbx.env.dist_path("phenix_regression")

def run():
  print("Testing QuantumBio's DivCon")
  if not os.environ.get("QBHOME", None):
    print("skipping QB test")
    return
  pdb = os.path.join(phenix_path, "external", "divcon", "3FVA.pdb")
  mtz = os.path.join(phenix_path, "external", "divcon", "3FVA.mtz")
  cmd = "phenix.refine %s %s" % (pdb, mtz)
  cmd += ' qblib=True qblib_method=pm6'
  cmd += ' qblib_region_selection="chain W and resname HOH and resid 7"'
  cmd += ' qblib_region_radius=3.0 qblib_buffer_radius=3.0 qblib_np=4'
  cmd += ' qblib_mem=2gb final_geo=True'
  print(cmd)

  ero = easy_run.fully_buffered(command=cmd)
  out = StringIO.StringIO()
  ero.show_stdout(out=out)
  outl = ""
  for line in out.getvalue().split("\n"):
    if not line: continue
    if line.find("POST-REFINED LOCAL STRAIN ENERGY")>-1:
      outl += "%s\n" % line
  #
  print(outl)
  assert outl, "phenix.refine qblib=True call failed."

  print("OK")

if __name__=="__main__":
  run()#sys.argv[1])
  
