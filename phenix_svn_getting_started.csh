#! /bin/csh -f

if ($#argv != 0) then
  set cci_user="$1"
else
  set cci_user="$USER"
endif

echo "cci.lbl.gov user name: $cci_user"

set verbose

rm -f {boost,scons,annlib}_hot.tar.gz
curl http://cci.lbl.gov/hot/boost_hot.tar.gz -o boost_hot.tar.gz
curl http://cci.lbl.gov/hot/scons_hot.tar.gz -o scons_hot.tar.gz
curl http://cci.lbl.gov/hot/annlib_hot.tar.gz -o annlib_hot.tar.gz
tar zxf boost_hot.tar.gz
tar zxf scons_hot.tar.gz
tar zxf annlib_hot.tar.gz

svn co https://svn.code.sf.net/p/cctbx/code/trunk cctbx_project

set from_cci=( \
  Plex \
  PyQuante \
  annlib_adaptbx \
  ccp4io \
  ccp4io_adaptbx \
  cxi_xdr_xes \
  gui_resources \
  chem_data \
  docutils \
  elbow \
  ksdssp \
  labelit \
  labelit_regression \
  lapack_fem \
  muscle \
  opt_resources \
  pex \
  phenix \
  phenix_html \
  phenix_regression \
  pulchra \
  solve_resolve \
  reel \
  tntbx)
foreach module ($from_cci)
  svn co svn+ssh://"$cci_user"@cci.lbl.gov/$module/trunk $module
end

svn co svn://svn.code.sf.net/p/cbflib/code-0/trunk/CBFlib_bleeding_edge cbflib

svn co https://quiddity.biochem.duke.edu/svn/reduce/trunk reduce
svn co https://quiddity.biochem.duke.edu/svn/probe/trunk probe
svn co https://quiddity.biochem.duke.edu/svn/phenix/king
svn co https://quiddity.biochem.duke.edu/svn/suitename

ln -s phenix_regression/allsvn .

exit 0
