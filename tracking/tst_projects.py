from __future__ import print_function

import phenix.tracking
import libtbx.load_env
from libtbx.test_utils import *
from libtbx.utils import Sorry
from libtbx import easy_run
import libtbx.utils
from six.moves import cStringIO as StringIO
import sys, os, time, math

sys.excepthook = libtbx.utils.__prev_excepthook

class fake_result (object) :
  def __init__ (self, job_id, dirname) :
    self.job_id = job_id
    self.dirname = dirname

  def finish_job (self) :
    j = self.job_id
    r_work = 0.2 + (0.01 * (j % 7)) + (0.001 * (j % 16)) + (0.0001 * j)
    r_free = r_work + 0.03 + (0.001 * (j % 27))
    rms_bonds = 0.01 + (0.01 * math.sqrt(j % 13))
    rms_angles = 1.5 + (0.1 * math.sqrt(j % 21))
    stats = [
      ("R-work", r_work),
      ("R-free", r_free),
      ("RMS(bonds)", rms_bonds),
      ("RMS(angles)", rms_angles)]
    output_files = [
      ("refine_%d.pdb" % j, "Refined model"),
      ("refine_%d_map_coeffs.mtz" % j, "Map coefficients for Coot"),
      ("refine_%d.log" % j, "phenix.refine log file"),]
    return output_files, stats

def exercise_1 (details=False) :
  if os.path.isdir("fake_home_1") :
    assert not easy_run.call("/bin/rm -rf fake_home_1")
  base_dir = os.getcwd()
  os.mkdir("fake_home_1")
  home_dir = os.path.join(base_dir, "fake_home_1")
  os.mkdir("fake_home_1/.phenix")
  os.mkdir("fake_home_1/1yjp")
  os.mkdir("fake_home_1/nsf-d2-ligand")
  db_file = os.path.join("fake_home_1", ".phenix", "project_db.phil")
  db_file = os.path.abspath(db_file)
  db = phenix.tracking.start_tracking(home_dir)
  #db = phenix.tracking.project_db.project_db(file_name=db_file)
  db.add_project("1yjp", os.path.abspath("fake_home_1/1yjp"))
  for id in ["nsf d2", "d2", "", "1yjp"] :
    try :
      db.add_project(id, os.path.abspath("fake_home_1/nsf-d2-ligand"))
    except Sorry :
      pass
    else :
      print(id)
      raise Exception_expected
  db.add_project("nsf-d2-ligand", os.path.abspath("fake_home_1/nsf-d2-ligand"))
  p = db.get_project_object("nsf-d2-ligand")
  t1 = time.time()
  n_jobs = 100
  for i in range(n_jobs) :
    job_id = p.get_next_job_id()
    p.start_job(
      app_id="Refine",
      program_name="phenix.refine",
      program_args=["/home/nat/refine_%d.eff" % job_id, "--overwrite"],
      config_file="/home/nat/refine_%d.eff" % job_id,
      title="Refinement of hen egg lysozyme",
      directory="/home/nat/Refine_%d" % job_id)
    if ((i % 20) == 0) :
      p.abort_job(job_id)
    elif ((i % 10) == 0) :
      p.job_error(job_id)
    else :
      p.finish_job(
        job_id=job_id,
        result=fake_result(job_id, "/home/nat/Refine_%d" % job_id))
  job_id = p.get_next_job_id()
  assert (job_id == 101)
  p.start_job(
    app_id="Xtriage",
    program_name="phenix.xtriage",
    program_args=["/home/nat/xtriage_%d.eff" % job_id],
    config_file="/home/nat/xtriage_%d.eff" % job_id,
    title="Xtriage")
  p.finish_job(job_id=job_id)
  t2 = time.time()
  p = db.load_project("nsf-d2-ligand", force_reload=True)
  job = p.get_job(100)
  assert (job.status == "complete")
  t3 = time.time()
  if details :
    print("time to create %d jobs: %5.1fms" % (n_jobs, (t2 - t1) * 1000))
    print("time to reload %d jobs: %5.1fms" % (n_jobs, (t3 - t2) * 1000))
  assert (p.get_job_count() == 101)
  job_id = p.get_job_id_from_config_file("/home/nat/refine_45.eff")
  assert (job_id == 45)
  jdata = p.get_job_data_source(hide_failed=True,
    flagged_only=False,
    app_name=None)
  assert (jdata.GetItemCount() == 91)
  p.flag_job(99)
  jdata.refresh(flagged_only=True)
  assert (jdata.GetItemCount() == 1)
  assert (jdata.GetItemText(0, 0) == "99")
  assert (jdata.GetItemText(0, 1) == "phenix.refine")
  jdata.refresh(app_name="phenix.xtriage")
  assert (jdata.GetItemCount() == 1)
  for i in range(n_jobs) :
    job_id = i+1
    if ((i % 5) == 0) :
      p.delete_job(job_id, delete_params=True)
  jdata.refresh()
  assert (jdata.GetItemCount() == 81)
  assert (p.get_job_count() == 101)
  assert (p.get_next_job_id() == 102)
  assert (p.get_program_list() == ['phenix.refine', 'phenix.xtriage'])
  assert (p.get_app_list() == ["Refine", "Xtriage"])
  assert ("%.3f" %(float(p.get_job_statistic(100, "R-free"))) == "0.283")
  assert (p.get_best_r_free() == "0.2445")
  j = p.get_job_info(57)
  assert (j.get_output_files() ==
    [('Refined model', 'refine_57.pdb'),
     ('Map coefficients for Coot', 'refine_57_map_coeffs.mtz'),
     ('phenix.refine log file', 'refine_57.log')])
  j.add_comments("this is job number 57")
  j.add_citation("phenix.refine")
  #---------------------------------------------------------------------
  # MOVING
  os.mkdir("fake_home_new")
  new_dir = os.path.join(os.getcwd(), "fake_home_new")
  p.move_project(new_dir)
  #---------------------------------------------------------------------
  new_dir = os.path.abspath("fake_home_new/nsf-d2-ligand/new_data")
  os.mkdir(new_dir)
  db.add_project("nsf2", new_dir)
  assert os.path.isdir(os.path.join(new_dir, ".phenix", "project_data"))
  assert (not os.path.isfile(
          os.path.join(new_dir, ".phenix", "job_history.phil")))
  assert (db.get_directory_project_id(new_dir) == "nsf2")
  assert (db.get_current_project_id() is None)
  #del db
  phenix.tracking.clear_database()
  old_cwd = os.getcwd()
  os.chdir(new_dir)
  db = phenix.tracking.start_tracking(home_dir)
  assert (db.get_current_project_id() == "nsf2")
  os.chdir(old_cwd)
  phenix.tracking.clear_database()
  db = phenix.tracking.start_tracking(home_dir)
  #db = phenix.tracking.project_db.project_db(file_name=db_file)
  assert (db.get_current_project_id() == "nsf2")
  assert os.path.isdir(os.path.join(new_dir, ".phenix"))
  s = db.get_project_summaries(sortby="ID")
  #print [ p.project_id for p in s ]
  assert ([ p.project_id for p in s ] == ['1yjp', 'nsf-d2-ligand', 'nsf2'])
  s = db.get_project_summaries(sortby="date", sort_in_place=False)
  # FIXME
  #print [ p.project_id for p in s ]
  #assert ([ p.project_id for p in s ] == ['nsf2', 'nsf-d2-ligand', '1yjp'])
  db.rename_project("nsf2", "nsf-d2-ligand_new")
  p = db.load_project("nsf-d2-ligand_new")
  assert (p.get_id() == "nsf-d2-ligand_new")
  assert (len(db.get_project_summaries()) == 3)
  ok = db.delete_project("nsf-d2-ligand_new", remove_data=True)
  time.sleep(1)
  assert (not ok) or (not os.path.isdir(os.path.join(new_dir, ".phenix")))

  assert (db.get_current_project_id() is not None) #== "1yjp") # FIXME
  s = db.get_project_summaries(sortby="ID")
  assert ([ p.project_id for p in s ] == ['1yjp', 'nsf-d2-ligand'])
  try :
    db.delete_project("nsf2")
  except Sorry:
    pass
  else :
    raise Exception_expected
  return
  # CCP4i database import
  ccp4_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/tracking/directories.def",
    test=os.path.isfile)
  class fake_callback (object) :
    def __init__ (self) :
      self.data = None
      self.n = 0
    def __call__ (self, data) :
      print(data)
      self.data = data
      self.n += 1
  cb1 = fake_callback()
  cb2 = fake_callback()
  _stdout = sys.stdout
  sys.stdout = StringIO()
  db.sync_with_ccp4(callback_warn=cb1,
    callback_added=cb2,
    ccp4_db=ccp4_file,
    verify_paths=False)
  sys.stdout = _stdout
  assert (cb1.n == 1)
  assert (cb2.data == ['beta-blip', 'gene-5-mad', 'p9-sad', 'rnase-s'])
  s = db.get_project_summaries(sortby="ID")
  assert ([ p.project_id for p in s ] ==
    ['1yjp','beta-blip','gene-5-mad','nsf-d2-ligand','p9-sad','rnase-s'])
  phenix.tracking.clear_database()

def exercise (details=False) :
  exercise_1(details)

if __name__ == "__main__" :
  # remove send2trash check after dependency updates
  try:
    import send2trash
  except ImportError:
    print('send2trash is not available, skipping test')
    sys.exit()
  exercise("--details" in sys.argv)
  print("OK")
