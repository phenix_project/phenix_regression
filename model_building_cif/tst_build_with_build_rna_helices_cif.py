from __future__ import division
from __future__ import print_function
import sys,os
import libtbx.load_env
from libtbx.test_utils import approx_equal


from iotbx.map_model_manager import map_model_manager as MapModelManager
from iotbx.data_manager import DataManager
from phenix.model_building import local_model_building

import time,sys

data_dir = libtbx.env.under_dist(
  module_name='phenix_regression',
  path='model_building_cif',
  test=os.path.isdir)

map_file=os.path.join(data_dir,'build_rna_helices_box.ccp4')
seq_file=os.path.join(data_dir,'seq_rna.dat')

def tst_01(map_file,  seq_file):

  print('Testing chain building with build_rna_helices and small map .')

  # Set up a data_manager to read and write files
  dm=DataManager()

  # Read in a cut_out map and build into the map

  # Read in the map and model
  map_manager=dm.get_real_map(map_file)

  # Get  sequence of full molecule
  sequence=dm.get_sequence(seq_file)[0].sequence

  # make a map-model manager, but skip the model this time
  mmm=MapModelManager(map_manager=map_manager, model=None)

  mmm.set_resolution(3)
  mmm.set_experiment_type('cryo_em')


  # Ready with small map and no model

  # Set up local model building
  build=local_model_building(
     map_model_manager=mmm, # map_model manager


     nproc= 1,
     normalize=True,
    )

  # set any defaults
  build.set_log(sys.stdout)
  build.set_defaults(
     scattering_table='electron',  # for a cryo-em map
     thoroughness='quick',
     debug=True,
     build_methods=['build_rna_helices'],
     random_seed = 785877,
     )

  # run
  build_model=build.build(
     target_number_of_residues_to_build = 35,
     refine_cycles=0,
     sequence=sequence,
     chain_type='RNA',
     cc_min=0.2,
     return_model = True,
    )

  # set the model
  build.set_model(build_model)

  # write out the model
  output_file = dm.write_model_file(
     build.model(),'build_model.cif',overwrite=True)

  dm=DataManager()
  read_in_fitted_model=dm.get_model(output_file)
  read_in_map_manager=dm.get_real_map(map_file)

  mam=MapModelManager(model=read_in_fitted_model,map_manager=read_in_map_manager)
  cc=mam.map_model_cc(resolution=3)
  assert approx_equal(cc,0.35,eps=0.10)


if __name__=='__main__':

  t0 = time.time()
  tst_01(map_file,  seq_file)

  print('Time: %6.4f'%(time.time()-t0))
  print('OK')
