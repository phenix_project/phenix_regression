from __future__ import division
from __future__ import print_function
import sys,os
import libtbx.load_env
from libtbx.test_utils import approx_equal


from iotbx.map_model_manager import map_model_manager as MapModelManager

from phenix.model_building import local_model_building

import time,sys

data_dir = libtbx.env.under_dist(
  module_name='phenix_regression',
  path='model_building_cif',
  test=os.path.isdir)

model_file=os.path.join(data_dir,'structure_search_target.cif')

def tst_01( model_file, ):

  print('Testing segment replacement with structure_search.')

  working_directory = 'cif_segment_replacement_01'
  if os.path.isdir(working_directory):
    print ('please remove %s before running this test' %(working_directory))
    assert not os.path.isdir(working_directory)
  os.mkdir(working_directory)
  os.chdir(working_directory)
  print ('Working in %s' %(os.getcwd()))


  # Make a data file
  mmm=MapModelManager()
  mmm.generate_map(file_name=model_file, d_min=3)
  mmm.set_resolution(3)
  mmm.set_experiment_type('cryo_em')


  sequence=mmm.model().as_sequence(as_string = True)


  # Ready with small map and model that is inside the map region

  # Set up local model building
  build=local_model_building(
     map_model_manager=mmm, # map_model manager
     nproc= 1,
    )
  build.set_log(sys.stdout)
  # set any defaults
  build.set_defaults(
     thoroughness='quick',
     debug=False,
     )

  # run replace_segment with sequence
  replace_segment_model=build.replace_segment(
     replace_segment_methods=['structure_search'],
     chain_id='AXLONG',
     last_resno_before_replace=21,
     first_resno_after_replace=35,
     refine_cycles=1,
    )

  # insert the loop
  build.insert_fitted_loop()
  mam=build.as_map_model_manager()
  mam.write_model('built_model.cif')
  mam.write_map('built_model.ccp4')

  cc=mam.map_model_cc(resolution=3)
  assert approx_equal(cc,0.92,eps=0.05)


if __name__=='__main__':

  t0 = time.time()
  tst_01( model_file, )

  print('Time: %6.4f'%(time.time()-t0))
  print('OK')
