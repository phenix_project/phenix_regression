from __future__ import division
from __future__ import print_function
import os
import libtbx.load_env
from libtbx.test_utils import approx_equal


from iotbx.map_model_manager import map_model_manager as MapModelManager
from iotbx.data_manager import DataManager


import time

data_dir = libtbx.env.under_dist(
  module_name='phenix_regression',
  path='model_building_cif',
  test=os.path.isdir)

map_file=os.path.join(data_dir,'build_rna_helices_box.ccp4')
model_file=os.path.join(data_dir,'build_rna_helices_box.cif')

def tst_01(map_file, model_file,):

  print('Testing RNA morphing with small map .')

  # Set up a data_manager to read and write files
  dm=DataManager()

  # Read in a cut_out map and build into the map

  # Read in the map and model
  map_manager=dm.get_real_map(map_file)
  model=dm.get_model(model_file)


  # make a map-model manager

  mmm=MapModelManager(map_manager=map_manager, model=model)

  mmm.set_resolution(3)
  mmm.set_experiment_type('cryo_em')

  starting_coords_chain_U = model.select(
      model.selection('chain UXLONG')).get_sites_cart()
  starting_coords_chain_V = model.select(
      model.selection('chain VXLONG')).get_sites_cart()

  # Set up model building
  build=mmm.model_building(
     nproc= 1,
    )

  build.set_defaults(debug=True)


  build.morph(default_selection_method='all')

  final_coords_chain_U = model.select(
      model.selection('chain UXLONG')).get_sites_cart()
  final_coords_chain_V = model.select(
      model.selection('chain VXLONG')).get_sites_cart()

  rms_U = final_coords_chain_U.rms_difference(starting_coords_chain_U)
  rms_V = final_coords_chain_V.rms_difference(starting_coords_chain_V)
  assert approx_equal( (rms_U, rms_V), (1.22220289003,0.755265223376), eps=0.1)



if __name__=='__main__':
  t0 = time.time()
  tst_01(map_file, model_file, )
  print('Time: %6.4f'%(time.time()-t0))
  print('OK')

