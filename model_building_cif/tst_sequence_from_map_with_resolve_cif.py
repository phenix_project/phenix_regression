from __future__ import division
from __future__ import print_function
import os
import libtbx.load_env
from libtbx.test_utils import approx_equal


from iotbx.map_model_manager import map_model_manager as MapModelManager
from iotbx.data_manager import DataManager
from phenix.model_building import local_model_building

import time

data_dir = libtbx.env.under_dist(
  module_name='phenix_regression',
  path='model_building_cif',
  test=os.path.isdir)

map_file=os.path.join(data_dir,'short_model_box.ccp4')
model_file=os.path.join(data_dir,'short_model_main_chain_box.cif')
seq_file=os.path.join(data_dir,'seq.dat')

def tst_01(map_file, model_file, seq_file):

  print('Testing sequence assignment with small map .')

  # Set up a data_manager to read and write files
  dm=DataManager()

  # Read in a cut_out map and a model and guess sequence assignment

  # Read in the map and model
  map_manager=dm.get_real_map(map_file)
  model=dm.get_model(model_file)

  # Get  sequence of full molecule
  sequence=dm.get_sequence(seq_file)[0].sequence

  # make a map-model manager
  mmm=MapModelManager(map_manager=map_manager, model=model)

  mmm.set_resolution(3)
  mmm.set_experiment_type('cryo_em')

  print ('Starting map-model cc:',mmm.map_model_cc(resolution=3))

  # Ready with small map and model that is inside the map region

  # Set up local model building
  build=local_model_building(
     map_model_manager=mmm, # map_model manager


    )

  # set any defaults
  build.set_defaults(
     scattering_table='electron',  # for a cryo-em map
     thoroughness='quick',  # makes no difference in this case
     debug=True,
     )

  # run sequence_from_map with sequence
  assign_sequence_model=build.sequence_from_map(
     sequence=sequence,
     return_model = True,
    )

  # write out the model
  dm.write_model_file(assign_sequence_model,'assign_sequence_model.cif',overwrite=True)

  dm=DataManager()
  read_in_fitted_model=dm.get_model('assign_sequence_model.cif')
  read_in_map_manager=dm.get_real_map(map_file)

  mam=MapModelManager(model=read_in_fitted_model,map_manager=read_in_map_manager)
  cc=mam.map_model_cc(resolution=3)
  assert approx_equal(cc,0.83,eps=0.25)

if __name__=='__main__':

  t0 = time.time()
  tst_01(map_file, model_file, seq_file)

  print('Time: %6.4f'%(time.time()-t0))
  print('OK')
