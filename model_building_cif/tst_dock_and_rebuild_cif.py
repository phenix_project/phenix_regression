from __future__ import division
from __future__ import print_function
import os
import libtbx.load_env




from iotbx.data_manager import DataManager


import time

data_dir = libtbx.env.under_dist(
  module_name='phenix_regression',
  path='model_building_cif',
  test=os.path.isdir)

map_file=os.path.join(data_dir,'dummy_af.ccp4')
model_file=os.path.join(data_dir,'dummy_af.cif')

def tst_01(map_file, model_file):

  print('Testing dock_and_rebuild with small map and dummy alphafold model.')

  # Set up a data_manager to read and write files
  dm=DataManager()

  # Read in a cut_out map and a model and refit a loop

  try:
    from iotbx.cli_parser import run_program
    from phenix.programs import dock_and_rebuild as run
  except Exception as e:
    print('dock_and_rebuild not available...skipping')
    return

  args = 'acceptable_docking_cc=0.05 minimum_docking_cc=0.05 keep_fraction_of_best=0.9 loop_refine_cycles=0 low_res_if_multiple_solutions=100 run_iterative_resolution_refine=False run_trace_loops_through_density=False run_iterative_morph=False refine_only=True rebuild_strategy=Quick control.nproc=1 ssm_search=False model=%s full_map=%s crystal_info.resolution=3 search.refine_cycles=0 build.refine_cycles=0 rigid_body_refinement=False rigid_body_refine_cycles=0 run_trace_loops_through_density=False  run_iterative_resolution_refine=False run_extend=False' %(model_file, map_file)
  args = args.split()
  print('phenix.dock_and_rebuild %s' %(' '.join(args)))

  dr = run_program(program_class=run.Program,args=args)



if __name__=='__main__':

  t0 = time.time()
  tst_01(map_file, model_file)

  print('Time: %6.4f'%(time.time()-t0))
  print('OK')
