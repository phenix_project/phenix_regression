from __future__ import division
from __future__ import print_function
import sys,os
import libtbx.load_env
from libtbx.test_utils import approx_equal



from iotbx.data_manager import DataManager
from phenix.model_building import local_model_building

import time,sys

data_dir = libtbx.env.under_dist(
  module_name='phenix_regression',
  path='model_building_cif',
  test=os.path.isdir)

target_model_file = os.path.join(data_dir,'first_half_modified.cif')
model_file = os.path.join(data_dir,'first_half_bad.cif')

def tst_01( model_file, ):

  print('Testing segment replacement with fix_insertions_deletions.')

  working_directory = 'cif_segment_replacement_03'
  if os.path.isdir(working_directory):
    print ('please remove %s before running this test' %(working_directory))
    assert not os.path.isdir(working_directory)
  os.mkdir(working_directory)
  os.chdir(working_directory)
  print ('Working in %s' %(os.getcwd()))


  # Make a data file
  from iotbx.map_model_manager import map_model_manager
  from iotbx.data_manager import DataManager
  dm = DataManager()

  target_model = dm.get_model(target_model_file)
  mmm = map_model_manager(model = target_model)
  mmm.generate_map(model = mmm.model(), d_min = 3)


  sequence=mmm.model().as_sequence(as_string = True)

  model = dm.get_model(model_file)
  mmm.add_model_by_id(model_id = 'model', model = model)
  mmm.set_experiment_type('cryo_em')
  print (mmm)


  # Ready with small map and model that is inside the map region

  # Set up local model building
  mmm.write_model('model.cif')
  mmm.write_map('map.ccp4')
  build=local_model_building(
     map_model_manager=mmm, # map_model manager
     nproc=1,
    )

  # set any defaults
  build.set_log(sys.stdout)
  build.set_defaults(
     thoroughness='quick',
     debug=True,
     sequence = sequence,
     )

  # run replace_segment with sequence
  replace_segment_model=build.replace_segment(
     chain_id='AXLONG',
     last_resno_before_replace=3,
     first_resno_after_replace=99,
     refine_cycles=0,
     fix_insertions_deletions = True,
     fix_insertions_deletions_type  = 'split_with_sequence',
    )

  # insert the loop
  build.insert_fitted_loop()
  mam=build.as_map_model_manager()
  mam.write_model('built_model.cif')
  mam.write_map('built_model.ccp4')

  cc=mam.map_model_cc(resolution=3)
  assert approx_equal(cc,0.85,eps=0.15)


if __name__=='__main__':

  t0 = time.time()
  tst_01( model_file, )

  print('Time: %6.4f'%(time.time()-t0))
  print('OK')
