from __future__ import division
from __future__ import print_function
import sys,os
import libtbx.load_env
from libtbx.test_utils import approx_equal


from iotbx.map_model_manager import map_model_manager as MapModelManager
from iotbx.data_manager import DataManager

import time,sys

data_dir = libtbx.env.under_dist(
  module_name='phenix_regression',
  path='model_building_cif',
  test=os.path.isdir)

map_file=os.path.join(data_dir,'trace_density.ccp4')
model_file=os.path.join(data_dir,'trace_density.cif')

def tst_01(map_file, model_file, ):

  print('Testing trace_density with small map and model.')

  # Read in the map and model
  dm=DataManager()
  map_manager=dm.get_real_map(map_file)
  model=dm.get_model(model_file)

  # make a map-model manager and remove the model outside the map
  mmm=MapModelManager(map_manager=map_manager, model=model)

  mmm.set_resolution(3)
  mmm.set_experiment_type('cryo_em')

  # Ready with small map and model that is inside the map region and a ligand to fit
  log = sys.stdout
  from phenix.model_building.build_tools import split_model_into_segments
  model = mmm.model().apply_selection_string('name ca')
  model_list = split_model_into_segments(model)
  for m in model_list: print(m)
  print('Connecting %s models C -> N' %(len(model_list)), file = log)
  print('working with ',mmm.map_manager().map_data().all())
  map_data = mmm.map_manager().map_data()

  m1,m2  = model_list[:2]
  sc1 = m1.get_sites_cart()[-1]
  sc2 = m2.get_sites_cart()[0]
  mmm.set_log(sys.stdout)
  info = mmm.model_building(
    normalize=False).trace_between_sites_through_density(
      site_cart_1 = sc1,
      site_cart_2 = sc2,
      sampling_ratio = 1,
     avoid_these_sites = m1.get_sites_cart()[:-1])
  assert info.path_length is not None
  assert approx_equal(info.path_length,73,eps=1)

  info = mmm.model_building(
    normalize=False).trace_between_sites_through_density(
      site_cart_1 = sc1,
      site_cart_2 = sc2,
      sampling_ratio = 2,
     avoid_these_sites = m1.get_sites_cart()[:-1])
  assert approx_equal(info.path_length,73,eps=1)


  info = mmm.model_building(
    normalize=False).trace_between_sites_through_density(
      site_cart_1 = sc1,
      site_cart_2 = sc2,
      sampling_ratio = 1.5,
     avoid_these_sites = m1.get_sites_cart()[:-1])
  assert approx_equal(info.path_length,74,eps=1)


if __name__=='__main__':

  t0 = time.time()
  tst_01(map_file, model_file)

  print('Time: %6.4f'%(time.time()-t0))
  print('OK')
