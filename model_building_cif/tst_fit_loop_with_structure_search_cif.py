from __future__ import division
from __future__ import print_function
import os
import libtbx.load_env
from libtbx.test_utils import approx_equal


from iotbx.map_model_manager import map_model_manager as MapModelManager
from iotbx.data_manager import DataManager
from phenix.model_building import local_model_building

import time

data_dir = libtbx.env.under_dist(
  module_name='phenix_regression',
  path='model_building_cif',
  test=os.path.isdir)

model_file=os.path.join(data_dir,'structure_search_target.cif')

def tst_01( model_file, ):

  print('Testing loop fitting with structure_search.')

  if (not os.path.isdir("structure_search_cif")):
    os.mkdir("structure_search_cif")
  os.chdir("structure_search_cif")

  # Make a data file
  mmm=MapModelManager()
  mmm.generate_map(file_name=model_file, d_min=3)
  mmm.set_resolution(3)
  mmm.set_experiment_type('cryo_em')

  sequence=mmm.model().as_sequence(as_string = True)

  full_model=mmm.model().deep_copy()
  gapped_model=mmm.model()
  gapped_model=gapped_model.select(gapped_model.selection(
     '(not resseq 22:34)'))
  mmm.add_model_by_id(model_id='model',model=gapped_model)



  # Ready with small map and model that is inside the map region

  # Set up local model building
  build=local_model_building(
     map_model_manager=mmm, # map_model manager
     nproc= 1,
    )

  # set any defaults
  build.set_defaults(
     thoroughness='quick',
     debug=True,
     fit_loop_methods=['structure_search'],
     )

  # run fit_loop with sequence
  replace_segment_model=build.fit_loop(
     chain_id='AXLONG',
     last_resno_before_loop=21,
     first_resno_after_loop=35,
     refine_cycles=1,
    )

  dm=DataManager()
  dm.write_model_file(build.model(),'working_model.cif',overwrite=True)
  dm.write_model_file(replace_segment_model,'replace_segment_model.cif',overwrite=True)

  # insert the loop
  build.insert_fitted_loop()
  mam=build.as_map_model_manager()
  mam.write_model('built_model.cif')
  mam.write_map('built_model.ccp4')

  cc=mam.map_model_cc(resolution=3)
  assert approx_equal(cc,0.92,eps=0.05)


if __name__=='__main__':

  t0 = time.time()
  tst_01( model_file, )

  print('Time: %6.4f'%(time.time()-t0))
  print('OK')
