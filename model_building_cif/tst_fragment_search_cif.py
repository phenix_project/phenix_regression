from __future__ import division, print_function
import sys, os, time

import libtbx.load_env
from libtbx.test_utils import approx_equal

data_dir = libtbx.env.under_dist(
  module_name='phenix_regression',
  path='model_building_cif',
  test=os.path.isdir)

model_file=os.path.join(data_dir,'short_1-28_short.cif')

def tst_01(log = sys.stdout):


  working_directory = 'cif_fragment_search_01'
  if os.path.isdir(working_directory):
    print ('please remove %s before running this test' %(working_directory))
    assert not os.path.isdir(working_directory)
  os.mkdir(working_directory)
  os.chdir(working_directory)
  print ('Working in %s' %(os.getcwd()))

  try:
    from iotbx.cli_parser import run_program
    from phenix.programs import fragment_search as run
  except Exception as e:
    print('fragment_search not available...skipping')
    return

  args = 'model=%s n_res=28 write_files=False nproc=1' %(model_file)
  args = args.split()

  model_list_info = run_program(program_class=run.Program,args=args)
  model_list = model_list_info.model_list
  new_model = model_list[0]
  rmsd = new_model.info().rmsd
  print('RMSD: %.2f' %(rmsd))
  assert approx_equal(rmsd,2.0,eps = 0.5)

if __name__ == '__main__':

  t0 = time.time()
  tst_01()

  print('Time: %6.4f'%(time.time()-t0))
  print('OK')
