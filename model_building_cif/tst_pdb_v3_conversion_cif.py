from __future__ import division, print_function
import sys, os, time

import libtbx.load_env




data_dir = libtbx.env.under_dist(
  module_name='phenix_regression',
  path='model_building_cif',
  test=os.path.isdir)

data_file=os.path.join(data_dir,'resolve_work.mtz')
text_dat_file=os.path.join(data_dir,'text.dat')
short_cif_file=os.path.join(data_dir,'short.cif')


def tst_01():
  # demo run with any mtz file
  import sys
  # get miller arrays from mtz file so we can pass to resolve
  print('Running resolve in memory...')
  input_text=open(text_dat_file).read()

  from iotbx import reflection_file_reader
  reflection_file=reflection_file_reader.any_reflection_file(data_file)
  mtz_content=reflection_file.file_content()
  miller_arrays=reflection_file.as_miller_arrays(merge_equivalents=True)

  from solve_resolve.resolve_python import refl_db
  array_dict=refl_db.assign_miller_arrays( miller_arrays=miller_arrays)


  import iotbx.pdb
  lines = list(open(short_cif_file).read().splitlines())
  pdb_inp=iotbx.pdb.input(lines=lines,source_info='strings')

  from solve_resolve.resolve_python.resolve_in_memory import run
  result_obj=run(array_dict=array_dict,pdb_inp_or_ph=pdb_inp,
    out=sys.stdout,
     input_text=input_text)
  cmn=result_obj.results
  assert cmn.atom_db.pdb_out_as_string.find('ZBA') > -1

  print('DONE')

if  (__name__ == '__main__'):
  t0 = time.time()
  tst_01()

  print('Time: %6.4f'%(time.time()-t0))
  print('OK')
