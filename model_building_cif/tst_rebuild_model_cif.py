from __future__ import division
from __future__ import print_function
import os , time
import libtbx.load_env






data_dir = libtbx.env.under_dist(
  module_name='phenix_regression',
  path=os.path.join('model_building_cif'),
  test=os.path.isdir)

def tst_01(data_dir):

  try:
    from iotbx.cli_parser import run_program
    from phenix.programs import rebuild_model as run
  except Exception as e:
    print('rebuild_model not available...skipping')
    return

  args = 'base_methods=refine optimization_sequence=base model=%s full_map=%s seq_file=%s quick=True nproc=1 refine_cycles=0 ' %(
      os.path.join(data_dir,'box_mtm.cif'),
      os.path.join(data_dir,'box.ccp4'),
      os.path.join(data_dir,'box.seq'))
  args = args.split()

  new_model = run_program(program_class=run.Program,args=args)

if __name__=='__main__':

  t0 = time.time()
  tst_01(data_dir)

  print('Time: %6.4f'%(time.time()-t0))
  print('OK')
