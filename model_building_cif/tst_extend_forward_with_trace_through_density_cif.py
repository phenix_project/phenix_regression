from __future__ import division, print_function


import sys, time, os


import libtbx.load_env
data_dir = libtbx.env.under_dist(
  module_name='phenix_regression',
  path='model_building_cif',
  test=os.path.isdir)

map_file=os.path.join(data_dir,'c_term_box.ccp4')
model_file = os.path.join(data_dir,'n_term_stub.cif')


def tst_01():

  # get a target model and an insertion model
  from iotbx.data_manager import DataManager
  dm = DataManager()
  dm.set_overwrite(True)

  mmm = dm.get_map_model_manager(map_files=map_file,
     model_file=model_file,
     )

  mmm.set_log(sys.stdout)
  mmm_sav = mmm.deep_copy()
  sequence='''CLWDLQNKAERQNDILV'''
  build = mmm.model_building()
  build.set_defaults(sequence=sequence, debug=True)

  extended = build.extend_forward(extend_methods=['trace_through_density'],
    refine_cycles = 0, target_number_of_residues_to_build=16)


  sequence_found = extended.as_sequence(as_string = True)
  print(sequence_found)
  assert sequence_found.find('LWDLQNKAERQND') > -1
  print(extended.model_as_pdb())

if __name__ == '__main__':

  t0 = time.time()
  tst_01()

  print('Time: %6.4f'%(time.time()-t0))
  print('OK')
