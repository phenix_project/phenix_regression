from __future__ import division, print_function
import sys, os, time

import libtbx.load_env

from iotbx.data_manager import DataManager




data_dir = libtbx.env.under_dist(
  module_name='phenix_regression',
  path='model_building_cif',
  test=os.path.isdir)

model_file=os.path.join(data_dir,'ssm_model.cif')
other_model_file=os.path.join(data_dir,'ssm_other_model.cif')
model_file_1=os.path.join(data_dir,'short_model_main_chain_gap_box.cif')
other_model_file_1=os.path.join(data_dir,'short_model_main_chain_box.cif')

def tst_01(log = sys.stdout):
  # Run using model_building object

  dm = DataManager()
  m1 = dm.get_model(model_file_1)
  m1.add_crystal_symmetry_if_necessary()
  build = m1.as_map_model_manager().model_building()
  build.set_log(log)
  build.set_defaults(debug=True)

  other_m1 = dm.get_model(other_model_file_1)
  other_m1.add_crystal_symmetry_if_necessary()

  # Test lsq superpose
  superposed_model = build.lsq_superpose(
    fixed_model = m1,
    moving_model = other_m1,
    morph = False,
    trim = False,
    )
  assert superposed_model is not None
  print('Superposed model (lsq):', superposed_model.info())

  m = dm.get_model(model_file)
  m.add_crystal_symmetry_if_necessary()
  build = m.as_map_model_manager().model_building()
  build.set_log(log)
  build.set_defaults(debug=True)
  other_m = dm.get_model(other_model_file)
  other_m.add_crystal_symmetry_if_necessary()

  # One where it will superpose perfectly:
  superposed_model = build.lsq_superpose(
    fixed_model = m,
    moving_model = m,
    )
  assert superposed_model is not None
  print('Superposed model (exact,lsq):', superposed_model.info())

  # Test general superpose:
  superposed_model = build.superpose_and_morph(
    fixed_model = m1,
    moving_model = other_m1,
    )
  assert superposed_model is not None
  print('General superposed model:', superposed_model.info())

if __name__ == '__main__':

  t0 = time.time()
  tst_01()

  print('Time: %6.4f'%(time.time()-t0))
  print('OK')
