from __future__ import division
from __future__ import print_function
import os
import libtbx.load_env




from iotbx.data_manager import DataManager


import time

data_dir = libtbx.env.under_dist(
  module_name='phenix_regression',
  path='model_building_cif',
  test=os.path.isdir)

map_file=os.path.join(data_dir,'model_A30_B30.ccp4')
scaffold_file=os.path.join(data_dir,'model_A30_B30.cif')
model_file_1=os.path.join(data_dir,'model_A30.cif')
model_file_2=os.path.join(data_dir,'model_B30.cif')

def tst_01():

  print('Testing dock_and_rebuild with scaffold model .')

  # Set up a data_manager to read and write files
  dm=DataManager()

  # Read in a cut_out map and a model and refit a loop

  try:
    from iotbx.cli_parser import run_program
    from phenix.programs import dock_and_rebuild as run
  except Exception as e:
    print('dock_and_rebuild not available...skipping')
    return

  args = 'rebuild_strategy=Quick control.nproc=4 scaffold_model=%s full_map=%s crystal_info.resolution=3 predicted_model=%s predicted_model=%s stop_after_morph=true maximum_rmsd=100' %(scaffold_file,
    map_file, model_file_1, model_file_2)

  args = args.split()
  print('phenix.dock_and_rebuild %s' %(' '.join(args)))

  dr = run_program(program_class=run.Program,args=args)
  assert dr.rebuilt_model.overall_counts().n_residues == 14



if __name__=='__main__':

  t0 = time.time()
  tst_01()

  print('Time: %6.4f'%(time.time()-t0))
  print('OK')
