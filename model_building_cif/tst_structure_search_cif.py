from __future__ import division, print_function
import sys , time
import os


def tst_01():
  from iotbx.map_model_manager import map_model_manager as MapModelManager
  mmm = MapModelManager()
  mmm.generate_map()
  build = mmm.model_building()
  build.set_defaults(nproc=1)
  build.set_log(sys.stdout)
  build.set_defaults(debug=True)
  here = os.getcwd()
  dd = 'tst_structure_search_cif_1'
  if not os.path.isdir(dd): os.mkdir(dd)
  os.chdir(dd)
  model_info = build.structure_search(sequence_list = ['XAREFSLEKTRNIGIMAHIDAGKTTTTERILYYTGRIHXXXXXXXXXXXXXXXXXXXXXXXXXTSAATTAAWEGHRVNIIDTPGHVDFTVEVERSLRVLDGAVTVLDAQSGVEPQTETVWRQATTYGVPRIVFVNKMDKLGANFEYSVSTLHDRLQANAAPIQLPIGAEDEFEAIIDLVEMKCFKYTNDLGTEIEEIEIPEDHLDRAEEARASLIEAVAETSDELMEKYLGDEEISVSELKEAIRQATTNVEFYPVLCGTAFKNKGVQLMLDAVIDYLPSPLDVKPIIGHRASNPEEEVIAKADDSAEFAALAFKVMTDPYVGKLTFFRVYSGTMTSGSYVKNSTKGKRERVGRLLQMHANSRQEIDTVYSGDIAAAVGLKDTGTGDTLCGEKNDIILESMEFPEPVIHLSVEPKSKADQDKMTQALVKLQEEDPTFHAHTXXXTGQVIIGGMGELHLDILVDRMKKEFNVECNVGAPMVSYRETFKSSAQVQGKFSRQSGGRGQYGDVHIEFTPNETGAGFEFENAIVGGVVPREYIPSVEAGLKDAMENGVLAGYPLIDVKAKLYDGSYHDVDSSEMAFKIAASLALKEAAKKCDPVILEPMMKVTIEMPEEYMGDIMGDVTSRRGRVDGMEPRGNAQVVNAYVPLSEMFGYATSLRSNTQGRGTYTMYFDHYAEVPKSIAEDIIKKNKGE'],
    number_of_models = 1)
  assert len(model_info.model_list) == 1
  os.chdir(here)

def tst_02():
  from iotbx.map_model_manager import map_model_manager as MapModelManager
  mmm = MapModelManager()
  mmm.generate_map()
  build = mmm.model_building()
  build.set_defaults(nproc=1)
  here = os.getcwd()
  dd = 'tst_structure_search_cif_2'
  if not os.path.isdir(dd): os.mkdir(dd)
  os.chdir(dd)
  model_info = build.structure_search(pdb_id = '2xex',
    number_of_models = 1)
  assert len(model_info.model_list) == 1
  os.chdir(here)

if __name__ == '__main__':

  t0 = time.time()
  time.sleep(0.1)
  tst_01()
  tst_02()

  print('Time: %6.4f'%(time.time()-t0))
  print('OK')

