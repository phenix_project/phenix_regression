from __future__ import division, print_function
import sys, os, time

import libtbx.load_env

from iotbx.data_manager import DataManager




data_dir = libtbx.env.under_dist(
  module_name='phenix_regression',
  path='model_building_cif',
  test=os.path.isdir)

model_file_1=os.path.join(data_dir,'short_model_main_chain_gap_box.cif')
other_model_file_1=os.path.join(data_dir,'short_model_main_chain_box.cif')

model_file=os.path.join(data_dir,'ssm_model.cif')
other_model_file=os.path.join(data_dir,'ssm_other_model.cif')

def tst_01(log = sys.stdout):

  dm = DataManager()
  m = dm.get_model(model_file)
  m.add_crystal_symmetry_if_necessary()
  build = m.as_map_model_manager().model_building()
  build.set_log(log)

  # Test ssm search against db
  info = build.ssm_search(
    model = m,
    pdb_include_list = ['2DY1'],
    max_models = 1,
    quick = True)
  assert info is not None
  for model in info.model_list:
    print('Model:', model.info(),model)

def tst_02(log = sys.stdout):
  # Test superposition with ssm

  dm = DataManager()
  m = dm.get_model(model_file)
  m.add_crystal_symmetry_if_necessary()
  build = m.as_map_model_manager().model_building()
  build.set_log(log)

  other_m = dm.get_model(other_model_file)
  other_m.add_crystal_symmetry_if_necessary()

  # Test ssm superpose
  superposed_model = build.ssm_superpose(
    fixed_model = m,
    moving_model = other_m,
    )
  assert superposed_model is not None
  print('Superposed model:', superposed_model.info())


  # Test reverse ssm superpose
  superposed_model = build.ssm_superpose(
    fixed_model = m,
    moving_model = other_m,
    include_swapped_match = True,
    )
  assert superposed_model is not None
  print('Superposed model:', superposed_model.info())

  # Test reverse
  superposed_model = build.ssm_superpose(
    fixed_model = m,
    moving_model = other_m,
    allow_reverse_connectivity = True,
    )
  assert superposed_model is not None
  print('Superposed model:', superposed_model.info())

if __name__ == '__main__':

  t0 = time.time()
  # ZZZ tst_01()
  tst_02()

  print('Time: %6.4f'%(time.time()-t0))
  print('OK')
