from __future__ import division
from __future__ import print_function
import sys,os,time
import libtbx.load_env






data_dir = libtbx.env.under_dist(
  module_name='phenix_regression',
  path=os.path.join('model_building_cif','reassemble_model'),
  test=os.path.isdir)

def tst_01(data_dir):
  from phenix.model_building.reassemble_model import exercise_00
  exercise_00(data_dir = data_dir, log = sys.stdout)
if __name__=='__main__':

  t0 = time.time()
  tst_01(data_dir)

  print('Time: %6.4f'%(time.time()-t0))
  print('OK')
