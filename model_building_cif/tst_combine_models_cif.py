from __future__ import division
from __future__ import print_function
import os
import libtbx.load_env



from iotbx.map_model_manager import map_model_manager as MapModelManager
from iotbx.data_manager import DataManager
from phenix.model_building import local_model_building

import time

data_dir = libtbx.env.under_dist(
  module_name='phenix_regression',
  path='model_building_cif',
  test=os.path.isdir)

map_file=os.path.join(data_dir,'short_model_box.ccp4')
model_file_1=os.path.join(data_dir,'short_model_box_1.cif')
model_file_2=os.path.join(data_dir,'short_model_box_2.cif')

def tst_01(map_file, model_file_1, model_file_2):

  print('Testing combine models ')

  # Set up a data_manager to read and write files
  dm=DataManager()

# Set up local model building
  map_manager=dm.get_real_map(map_file)
  model=dm.get_model(model_file_1)
  model_2=dm.get_model(model_file_2)
  mmm=MapModelManager(map_manager=map_manager, model=model)

  mmm.set_resolution(3)
  mmm.set_experiment_type('cryo_em')

  build=local_model_building(
     map_model_manager=mmm, # map_model manager

     nproc= 1,
    )

  # set any defaults
  build.set_defaults(
     scattering_table='electron',  # for a cryo-em map
     thoroughness='quick',
     debug=True,
     )

  # Empty run
  m1=build.model().apply_selection_string('name zz')
  m2=model_2.apply_selection_string('name zz')
  new_model = build.combine_models(model_list=[m1,m2], merge_type='non_overlapping')
  assert new_model.overall_counts().n_residues == 0

  # Standard run
  new_model = build.combine_models(model_list=[build.model(),model_2])
  assert (build.model().get_hierarchy().overall_counts().n_residues,
          model_2.get_hierarchy().overall_counts().n_residues,
          new_model.get_hierarchy().overall_counts().n_residues) == (10, 17, 20)

  # non_overlapping and  keep_all_of_first_model=True
  new_model = build.combine_models(model_list=[build.model(),model_2],
     keep_all_of_first_model=True,merge_type = 'non_overlapping')
  assert (build.model().get_hierarchy().overall_counts().n_residues,
          model_2.get_hierarchy().overall_counts().n_residues,
          new_model.get_hierarchy().overall_counts().n_residues) == (10, 17, 17)


if __name__=='__main__':

  t0 = time.time()
  tst_01(map_file, model_file_1, model_file_2)

  print('Time: %6.4f'%(time.time()-t0))
  print('OK')
