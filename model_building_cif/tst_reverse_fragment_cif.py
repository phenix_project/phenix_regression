from __future__ import division, print_function
import sys, os, time

import libtbx.load_env
from iotbx.data_manager import DataManager

data_dir = libtbx.env.under_dist(
  module_name='phenix_regression',
  path='model_building_cif',
  test=os.path.isdir)

model_file=os.path.join(data_dir,'short_model_box.cif')

def tst_01(log = sys.stdout):


  working_directory = 'cif_reverse_fragment_01'
  if os.path.isdir(working_directory):
    print ('please remove %s before running this test' %(working_directory))
    assert not os.path.isdir(working_directory)
  os.mkdir(working_directory)
  os.chdir(working_directory)
  print ('Working in %s' %(os.getcwd()))

  # Make a data file
  from iotbx.data_manager import DataManager
  dm = DataManager()
  model = dm.get_model(model_file)

  try:
    from iotbx.cli_parser import run_program
    from phenix.programs import reverse_fragment as run
  except Exception as e:
    print('reverse_fragment not available...skipping')
    return

  args = 'model=%s refine_cycles=0 ca_only=True' %(model_file)
  args = args.split()

  reverse_model = run_program(program_class=run.Program,args=args)
  assert reverse_model.get_hierarchy().overall_counts().n_residues == \
    model.get_hierarchy().overall_counts().n_residues


  args = 'model=%s refine_cycles=0 ca_only=False' %(model_file)
  args = args.split()

  reverse_model = run_program(program_class=run.Program,args=args)
  assert reverse_model.get_hierarchy().overall_counts().n_residues == \
    model.get_hierarchy().overall_counts().n_residues

if __name__ == '__main__':

  t0 = time.time()
  tst_01()

  print('Time: %6.4f'%(time.time()-t0))
  print('OK')
