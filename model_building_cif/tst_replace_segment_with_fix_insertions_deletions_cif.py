from __future__ import division
from __future__ import print_function
import os
import libtbx.load_env
from libtbx.test_utils import approx_equal



from iotbx.data_manager import DataManager
from phenix.model_building import local_model_building

import time

data_dir = libtbx.env.under_dist(
  module_name='phenix_regression',
  path='model_building_cif',
  test=os.path.isdir)

model_file=os.path.join(data_dir,'box_mtm.cif')
target_file = os.path.join(data_dir,'box.cif')
map_file = os.path.join(data_dir,'box.ccp4')

def tst_01( model_file, ):

  print('Testing segment replacement with fix_insertions_deletions.')

  working_directory = 'cif_segment_replacement_02'
  if os.path.isdir(working_directory):
    print ('please remove %s before running this test' %(working_directory))
    assert not os.path.isdir(working_directory)
  os.mkdir(working_directory)
  os.chdir(working_directory)
  print ('Working in %s' %(os.getcwd()))


  # Make a data file
  from iotbx.data_manager import DataManager
  dm = DataManager()
  mmm=dm.get_map_model_manager(model_file=model_file,map_files=map_file)
  target_model=dm.get_model(target_file)
  mmm.add_model_by_id(model=target_model,model_id='target_model')

  mmm.set_resolution(3)
  mmm.set_experiment_type('cryo_em')


  sequence=mmm.model().as_sequence(as_string = True)


  # Ready with small map and model that is inside the map region

  # Set up local model building
  mmm.write_model('model.cif')
  mmm.write_map('map.ccp4')
  build=local_model_building(
     map_model_manager=mmm, # map_model manager
     nproc= 1,
    )

  # set any defaults
  build.set_defaults(
     thoroughness='quick',
     debug=True,
     )

  # run replace_segment with sequence
  replace_segment_model=build.replace_segment(
     chain_id='UXLONG',
     last_resno_before_replace=196,
     first_resno_after_replace=206,
     refine_cycles=1,
     fix_insertions_deletions = True,
     fix_insertions_deletions_type  = 'rebuild_loops',
    )

  # insert the loop
  build.insert_fitted_loop()
  mam=build.as_map_model_manager()
  mam.write_model('built_model.cif')
  mam.write_map('built_model.ccp4')

  cc=mam.map_model_cc(resolution=3)
  assert approx_equal(cc,0.75,eps=0.15)


if __name__=='__main__':

  t0 = time.time()
  tst_01( model_file, )

  print('Time: %6.4f'%(time.time()-t0))
  print('OK')
