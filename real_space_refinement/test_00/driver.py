from __future__ import division
from scitbx.array_family import flex
from libtbx import group_args
from libtbx.utils import null_out
import mmtbx
from mmtbx.refinement.real_space import fit_small_molecule
import libtbx.load_env
import os

def write_ccp4_map(unit_cell, space_group, file_name, map_data):
  from iotbx import ccp4_map
  ccp4_map.write_ccp4_map(
      file_name=file_name,
      unit_cell=unit_cell,
      space_group=space_group,
      #gridding_first=(0,0,0),# This causes a bug (map gets shifted)
      #gridding_last=n_real,  # This causes a bug (map gets shifted)
      map_data=map_data,
      labels=flex.std_string([""]))


def get_pdb_inputs(folder_name, file_name):
  pdb_file_name=get_fn(folder_name=folder_name, file_name=file_name)
  ppf = mmtbx.utils.process_pdb_file_srv(log=null_out()).process_pdb_files(
    pdb_file_names=[pdb_file_name])[0]
  xrs = ppf.xray_structure(show_summary = False)
  restraints_manager = mmtbx.restraints.manager(
    geometry      = ppf.geometry_restraints_manager(show_energies = False),
    normalization = True)
  return group_args(
    ph  = ppf.all_chain_proxies.pdb_hierarchy,
    grm = restraints_manager,
    xrs = xrs)

def get_fn(folder_name, file_name):
  return libtbx.env.find_in_repositories(
    relative_path="phenix_regression/real_space_refinement/test_00/%s/%s"%(
      folder_name,file_name),
    test=os.path.isfile)

def exercise(folder_name, target):
  # Read good and poor models
  p_answer = get_pdb_inputs(folder_name=folder_name, file_name="answer.pdb")
  p_poor   = get_pdb_inputs(folder_name=folder_name, file_name="poor.pdb")
  p_answer.ph.write_pdb_file(file_name="answer.pdb",
    crystal_symmetry=p_answer.xrs.crystal_symmetry())
  p_poor.ph.write_pdb_file(file_name="poor.pdb",
    crystal_symmetry=p_poor.xrs.crystal_symmetry())
  cs = p_poor.xrs.crystal_symmetry()
  # Read three maps
  # Assume all maps are sigma-scaled: this is critical. Otherwise scale yourself.
  from iotbx import ccp4_map
  fn1=get_fn(folder_name=folder_name, file_name= "fofc_box.ccp4")
  fn2=get_fn(folder_name=folder_name, file_name="2fofc_box.ccp4")
  fn3=get_fn(folder_name=folder_name, file_name=  "fem_box.ccp4")
  m1 = ccp4_map.map_reader(file_name=fn1).data.as_double()
  m2 = ccp4_map.map_reader(file_name=fn2).data.as_double()
  m3 = ccp4_map.map_reader(file_name=fn3).data.as_double()
  # map for refinement
  m = fit_small_molecule.prepare_maps(fofc=m1, two_fofc=m2, fem=m3)
  write_ccp4_map(unit_cell=cs.unit_cell(), space_group=cs.space_group(),
    file_name="target_map.ccp4", map_data=m)
  # now going into refinement
  xrs_refined = fit_small_molecule.macro_cycle(
    xray_structure      = p_poor.xrs,
    target_map          = m,
    geometry_restraints = p_poor.grm.geometry,
    log                 = None)
  p_poor.ph.adopt_xray_structure(xrs_refined)
  p_poor.ph.write_pdb_file(file_name="refined.pdb")
  result = flex.mean(xrs_refined.distances(p_answer.xrs))
  assert result < target, result
