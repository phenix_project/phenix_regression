from __future__ import division
from __future__ import print_function
from libtbx.utils import user_plus_sys_time
from phenix_regression.real_space_refinement.test_00 import driver


if(__name__ == "__main__"):
  timer = user_plus_sys_time()
  driver.exercise(folder_name="test_data_00", target=0.15)
  print("Time: %6.2f" % timer.elapsed())
