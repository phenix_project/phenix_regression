from __future__ import division
from __future__ import print_function
import cctbx.maptbx.minimization
from cctbx import sgtbx
from cctbx import adptbx
from cctbx.array_family import flex
from cctbx.development import random_structure
import random, time
import sys, os
import iotbx.pdb
import libtbx.load_env
import mmtbx.utils
import mmtbx.model

def run():
  if (1):
    random.seed(0)
    flex.set_random_seed(0)
  def compute_map(xray_structure, d_min=1.5, resolution_factor=1./4):
    fc = xray_structure.structure_factors(d_min = d_min).f_calc()
    fft_map = fc.fft_map(resolution_factor=resolution_factor)
    fft_map.apply_sigma_scaling()
    result = fft_map.real_map_unpadded()
    return result, fc, fft_map
  pdbfn = libtbx.env.find_in_repositories(
        relative_path="phenix_regression/pdb/1akg.pdb", test=os.path.isfile)
  model = mmtbx.model.manager(
      model_input = iotbx.pdb.input(file_name = pdbfn))
  model.process(make_restraints=True)
  model.setup_scattering_dictionaries(scattering_table="wk1995")
  xrs = model.get_xray_structure()
  map_target,tmp,tmp = compute_map(xray_structure = xrs)
  xrs_sh = xrs.deep_copy_scatterers()
  xrs_sh.shake_sites_in_place(mean_distance=0.5)
  start_error = flex.mean(xrs.distances(other = xrs_sh))
  print("Start:", start_error)
  assert (start_error >= 0.49999999) # XXX why doesn't >= 0.5 work?
  map_current, miller_array, crystal_gridding = compute_map(
    xray_structure = xrs_sh)
  #for step in steps1+steps2:
  geometry_restraints_manager = None
  if 1:
    geometry_restraints_manager = model.restraints_manager
  for step in [miller_array.d_min()/4]*5:
    if(1):
      minimized = cctbx.maptbx.minimization.run(
        xray_structure              = xrs_sh,
        miller_array                = miller_array,
        crystal_gridding            = crystal_gridding,
        map_target                  = map_target,
        max_iterations              = 500,
        min_iterations              = 25,
        step                        = step,
        geometry_restraints_manager = geometry_restraints_manager,
        target_type                 = "diffmap")
      xrs_sh = minimized.xray_structure
      final_error = flex.mean(xrs.distances(other = minimized.xray_structure))
    print("Final:", final_error)
  assert final_error < 0.005, "Final error : %s" % final_error # lazy fix, was 0.04, before 0.0008
  print("OK")

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("Time: %8.3f"%(time.time()-t0))
