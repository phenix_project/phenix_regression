from __future__ import print_function
from cctbx.array_family import flex
from libtbx.test_utils import approx_equal
import random, sys, math, os, time
from libtbx import easy_run
import libtbx.load_env

examples = {
"1a": [0.2264, 0.2423],
"1b": [0.1767, 0.1851],
"1c": [0.1485, 0.1820],
"1d": [0.1378, 0.1738],
"1e": [0.1272, 0.1599],
"2a": [0.2127, 0.2738],
"2b": [0.2394, 0.2840],
"2c": [0.2127, 0.2557],
"2d": [0.2055, 0.2513],
"3a": [0.2060, 0.2732],
"3b": [0.2184, 0.2520],
"3c": [0.1993, 0.2393],
"4a": [0.2937, 0.3531],
"4b": [0.2364, 0.2915],
"5a": [0.1647, 0.2024],
"5b": [0.1825, 0.1833],
"6a": [0.2479, 0.2753],
"6b": [0.1687, 0.2093],
}
keys = examples.keys()
values = examples.values()

def check_directory_content():
  counter = 0
  for item in os.listdir("refinement_examples/"):
    if(item.startswith("example_")):
      counter += 1
      assert item[8:] in keys
  assert counter == len(keys)

def make_chunks(args):
  assert len(args) in [0,1]
  slices = []
  if(len(args)==0):
    slices.append([0, len(keys)])
    n_cpu = 1
  else:
    n_cpu = int(args[0])
    assert n_cpu > 0
    if(n_cpu > len(keys)):
      n_cpu = len(keys)
    first = len(keys) / n_cpu
    second = len(keys) % n_cpu
    j = 0
    slices = []
    for i in range(n_cpu):
      slices.append([j,j+first])
      j = j+first
    if(second > 0):
      slices[len(slices)-1][1] += second
  print("Jobs will be run on %d CPU distributed as:"%n_cpu)
  for slc in slices:
    print("  slice:", slc)
  return slices

def copy_examples_dir_and_clean():
  examples_dir = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement_examples")
  if(examples_dir is None):
    print("ERROR: directory phenix_regression/refinement_examples is not found.")
  easy_run.call("rm -rf refinement_examples")
  easy_run.call("cp -a %s . ; rm -rf refinement_examples/.svn "%examples_dir)
  for key in keys:
    easy_run.call("rm -rf refinement_examples/example_%s/.svn"%key)

def run(args):
  copy_examples_dir_and_clean()
  check_directory_content()
  chunks = make_chunks(args)
  run_examples = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/examples.py", test=os.path.isfile)
  if(run_examples is None):
    print("ERROR: file phenix_regression/examples.py is not found.")
  print("The following examples are submitted to run:")
  if (len(chunks) < 2):
    bg = ""
  else:
    bg = " &"
  for chunk in chunks:
    start, end =  chunk[0], chunk[1]
    print(keys[start:end])
    command = "phenix.python %s %s %s%s" % (
      run_examples, str(start),str(end), bg)
    print(command)
    easy_run.call(command=command)

if(__name__ == "__main__"):
  run(sys.argv[1:])
