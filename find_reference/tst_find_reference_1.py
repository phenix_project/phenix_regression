from __future__ import absolute_import, division, print_function

from libtbx.test_utils import assert_lines_in_file, show_diff, approx_equal
from libtbx import easy_run
import libtbx.load_env
from libtbx.utils import format_cpu_times

import requests
import os, sys


def exercise_1():
  """Running as command-line
  """

  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/find_reference/1ucs_modified.pdb",
    test=os.path.isfile)
  cmd = "phenix.find_reference %s prefix=ex1" % pdb_file
  print(cmd)
  assert not easy_run.call(cmd)
  assert os.path.isfile('reference_for_chain_A_1UCS.pdb')
  assert_lines_in_file(file_name='ex1_000.eff', lines="""\
refinement.reference_model {
  search_options {
    chain_similarity_threshold = 0.01
  }
  enabled=True
  file=reference_for_chain_A_1UCS.pdb

  reference_group {
    reference = chain A
    selection = chain A
    file_name = reference_for_chain_A_1UCS.pdb
  }
}""")

def exercise_2():
  """Running as Program
  """
  from iotbx.cli_parser import run_program
  from phenix.programs.find_reference import Program as find_reference_program
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/find_reference/1ucs_modified.pdb",
    test=os.path.isfile)
  result = run_program(program_class=find_reference_program,
                       args=[pdb_file, "prefix=ex2"])
  # [(chain id of target model, chain id of reference model, model object, reference model pdb id)]
  print('result', result)
  target_cid, ref_cid, ref_model, ref_info, ref_calc_info = result[0]
  assert target_cid == 'A'
  assert ref_cid == 'A'
  assert ref_info.get_pdb_id() == '1UCS'
  # print('ref_calc_info', ref_calc_info)
  # ref_calc_info
  assert approx_equal(ref_calc_info, {
      'sort_value_1': 0.35,
      'seq_identity': 1.0,
      'total_residues': 64,
      'xyz_pbs': 0.0,
      'tor_pbs': 8.526512829121202e-14,
      'xyz_rmsd': 2.4945616659621994e-15,
      'tor_rmsd': 2.2580645269294792,
      'sort_value_2': 0.35})
  # No overall coverage here, no object, unlike exercise_3.
  # assert approx_equal(fr.get_overall_coverage(), 1.)

  assert os.path.isfile('reference_for_chain_A_1UCS.pdb')
  assert_lines_in_file(file_name='ex2_000.eff', lines="""\
refinement.reference_model {
  search_options {
    chain_similarity_threshold = 0.01
  }
  enabled=True
  file=reference_for_chain_A_1UCS.pdb

  reference_group {
    reference = chain A
    selection = chain A
    file_name = reference_for_chain_A_1UCS.pdb
  }
}""")

def exercise_3():
  """Running as a function
  """
  from phenix.automation.find_reference import find_reference
  import iotbx.pdb
  import mmtbx.model
  pdb_file_name = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/find_reference/1ucs_modified.pdb",
    test=os.path.isfile)
  target_model = mmtbx.model.manager(model_input = iotbx.pdb.input(pdb_file_name))
  # running with defaults:
  fr = find_reference(
      target_model=target_model,
      identity_cutoff=0.9,
      include_xray=True,
      include_csm=True,
      use_padded_sequence=False,
      logger=sys.stdout
  )
  fr.run()
  result = fr.get_results()
  # [(chain id of target model, chain id of reference model, model object, reference model pdb id)]
  # print('result in test', result)
  target_cid, ref_cid, ref_model, ref_info, ref_calc_info = result[0]
  assert target_cid == 'A'
  assert ref_cid == 'A'
  assert ref_info.get_pdb_id() == '1UCS'
  # print('ref_calc_info', ref_calc_info)
  # ref_calc_info
  assert approx_equal(ref_calc_info, {
      'sort_value_1': 0.35,
      'seq_identity': 1.0,
      'total_residues': 64,
      'xyz_pbs': 0.0,
      'tor_pbs': 8.526512829121202e-14,
      'xyz_rmsd': 2.4945616659621994e-15,
      'tor_rmsd': 2.2580645269294792,
      'sort_value_2': 0.35})
  assert approx_equal(fr.get_overall_coverage(), 1.)

  eff_file = fr.get_reference_model_param_file_content()
  print (eff_file)
  assert not show_diff(eff_file, """\
refinement.reference_model {
  search_options {
    chain_similarity_threshold = 0.01
  }
  enabled=True
  file=A_1UCS.pdb

  reference_group {
    reference = chain A
    selection = chain A
    file_name = A_1UCS.pdb
  }
}
""")

if (__name__ == "__main__"):
  if sys.version_info.major >= 3:
    exception_occured = False
    try:
      r = requests.get('https://search.rcsb.org/')
    except Exception:
      print("OK but exception.")
      exception_occured = True
    if not exception_occured and r.ok and len(r.text) > 100:
      exercise_1()
      exercise_2()
      exercise_3()
      print("OK")
    else:
      print("OK but skipped.")
  print(format_cpu_times())
