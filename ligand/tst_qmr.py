import os, sys
from mmtbx.regression import model_1yjp, model_1aL1
from libtbx import easy_run
from mmtbx.geometry_restraints.quantum_interface import get_qm_restraints_scope

def get_qmr_phil(swaps=None):
  phil_scope = get_qm_restraints_scope()
  if swaps is None: swaps=[]
  rc=''
  for line in phil_scope.splitlines():
    if line.strip().startswith('.'): continue
    if line.find('qm_restraints')==0: line=line.replace('qm_restraints',
                                                        'qi.qm_restraints')
    line = line.replace('read_output_to_skip_opt_if_available = False',
                        'read_output_to_skip_opt_if_available = True')
    for s1,s2 in swaps:
      if line.find(s1)>-1: line=line.replace(s1,s2)
    rc+='%s\n' % line
  return rc

def _setup(lines, index, swaps):
  f=open('tst_qmr_%s.pdb' % index, 'w')
  f.write(lines)
  del f
  qmr_phil = get_qmr_phil(swaps)
  qmr_phil = qmr_phil.replace('*mopac test', 'mopac *test')
  default_phil = get_qmr_phil(None)
  for line1, line2 in zip(qmr_phil.splitlines(), default_phil.splitlines()):
    if line1!=line2:
      print('>',line1)
  # print(qmr_phil)
  f=open('tst_qmr_%s.phil' % index, 'w')
  f.write(qmr_phil)
  del f
  cmd = 'mmtbx.quantum_interface %s run_qmr=1 %s' % ('tst_qmr_%s.pdb' % index,
                                                     'tst_qmr_%s.phil' %index,
                                                     )
  return cmd

def get_h_model(lines):
  import hashlib
  result = hashlib.md5(lines.encode())
  id_str = result.hexdigest()
  hf = 'tst_qmr_%s_hydrogenate.pdb' % id_str
  if os.path.exists(hf):
    f=open(hf, 'r')
    lines=f.read()
    del f
    return lines
  f=open('tst_qmr_%s.pdb' % id_str, 'w')
  f.write(lines)
  del f
  cmd = 'mmtbx.hydrogenate %s' % 'tst_qmr_%s.pdb' % id_str
  easy_run.go(cmd)
  f=open(hf, 'r')
  lines=f.read()
  del f
  return lines

def empty_model(model_1yjp_h, swaps=None, adding=0):
  if swaps is None: swaps = [['  selection = None', '  selection = resid 1']]
  cmd = _setup(model_1yjp_h, 2+adding, swaps)
  print(cmd)
  rc = easy_run.go(cmd)
  lines = '\n'.join(rc.stdout_lines)
  assert lines.find('results in empty model')>-1

def long_h_bond(model_1yjp_h, swaps=None, adding=0):
  if swaps is None: swaps = [['  selection = None', '  selection = resid 1']]
  cmd = _setup(model_1yjp_h, 2+adding, swaps)
  print(cmd)
  rc = easy_run.go(cmd)
  lines = '\n'.join(rc.stdout_lines)
  assert lines.find('The QM optimisation has caused a X-H covalent bond to exceed 1.5 Angstrom.')>-1

def runs_fine(model_1yjp_h, swaps=None, adding=0):
  if swaps is None: swaps = [['  selection = None', '  selection = resid 2']]
  cmd = _setup(model_1yjp_h, 3+adding, swaps)
  print(cmd)
  rc = easy_run.go(cmd)
  lines = '\n'.join(rc.stdout_lines)
  assert lines.find('QM energies')>-1

def main(only_i=None):
  try: only_i=int(only_i)
  except: only_i=None
  print('tst_qmr',only_i)

  model_1yjp_h = get_h_model(model_1yjp)
  outl = ''
  for line in model_1aL1.splitlines():
    if line.find('ACE')>-1: continue
    outl += '%s\n' % line
  model_1aL1_h = get_h_model(outl)

if __name__ == '__main__':
  main(*tuple(sys.argv[1:]))
