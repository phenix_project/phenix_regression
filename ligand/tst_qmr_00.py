import os, sys
from mmtbx.regression import model_1yjp, model_1aL1
from libtbx import easy_run
from mmtbx.geometry_restraints.quantum_interface import get_qm_restraints_scope

from tst_qmr import get_h_model, _setup, long_h_bond, empty_model

def stop_on_no_hydrogens():
  cmd = _setup(model_1yjp, 1, [['  selection = None', '  selection = resid 1']])
  print(cmd)
  rc = easy_run.go(cmd)
  lines = '\n'.join(rc.stdout_lines)
  assert lines.find('Sorry: Model must have Hydrogen atoms')>-1


def run_energy_1(model_1yjp_h):
  # calculate = *in_situ_opt starting_energy final_energy starting_strain final_strain starting_bound final_bound starting_higher_single_point final_higher_single_point
  cmd = _setup(model_1yjp_h,
               4,
               [['  selection = None', '  selection = resid 2'],
                [' starting_energy ', ' *starting_energy ']
               ])
  print(cmd)
  rc = easy_run.go(cmd)
  lines = '\n'.join(rc.stdout_lines)
  assert lines.find('QM energies')>-1, '%s' % lines
  assert lines.find(' energy     ')>-1

def run_energy_2(model_1yjp_h):
  cmd = _setup(model_1yjp_h,
               5,
               [['  selection = None', '  selection = resid 2'],
                [' starting_strain ', ' *starting_strain ']
               ])
  print(cmd)
  rc = easy_run.go(cmd)
  lines = '\n'.join(rc.stdout_lines)
  assert lines.find('QM energies')>-1
  assert lines.find(' strain     ')>-1

def run_energy_3(model_1yjp_h):
  cmd = _setup(model_1yjp_h,
               6,
               [['  selection = None', '  selection = resid 2'],
                [' starting_bound ', ' *starting_bound ']
               ])
  print(cmd)
  rc = easy_run.go(cmd)
  lines = '\n'.join(rc.stdout_lines)
  assert lines.find('QM energies')>-1
  assert lines.find(' bound     ')>-1

def run_update_1(model_1yjp_h):
  cmd = _setup(model_1yjp_h,
               7,
               [['  selection = None', '  selection = resid 2'],
                ['  include_inter_residue_restraints = False',
                 '  include_inter_residue_restraints = True']
               ])
  print(cmd)
  rc = easy_run.go(cmd)
  lines = '\n'.join(rc.stdout_lines)
  assert lines.find('QM energies')>-1
  # assert lines.find(' bound     ')>-1

def main(only_i=None):
  try: only_i=int(only_i)
  except: only_i=None
  print('tst_qmr',only_i)

  model_1yjp_h = get_h_model(model_1yjp)
  outl = ''
  for line in model_1aL1.splitlines():
    if line.find('ACE')>-1: continue
    outl += '%s\n' % line
  model_1aL1_h = get_h_model(outl)

  tests = [
    [stop_on_no_hydrogens, (), {}],
    # [long_h_bond, (model_1yjp_h,), {}],
    [run_energy_1, (model_1yjp_h,), {}],
    [run_energy_2, (model_1yjp_h,), {}],
    [run_energy_3, (model_1yjp_h,), {}],
    [run_update_1, (model_1yjp_h,), {}],
    ]

  for i, (func, args, kwds) in enumerate(tests):
    if only_i is not None and only_i!=i+1: continue
    func(*args, **kwds)

if __name__ == '__main__':
  main(*tuple(sys.argv[1:]))
