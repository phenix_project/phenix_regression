import os, sys
from mmtbx.regression import model_1yjp, model_1aL1
from libtbx import easy_run
from mmtbx.geometry_restraints.quantum_interface import get_qm_restraints_scope

from tst_qmr import get_h_model, runs_fine

def main(only_i=None):
  try: only_i=int(only_i)
  except: only_i=None
  print('tst_qmr',only_i)

  model_1yjp_h = get_h_model(model_1yjp)
  outl = ''
  for line in model_1aL1.splitlines():
    if line.find('ACE')>-1: continue
    outl += '%s\n' % line
  model_1aL1_h = get_h_model(outl)

  tests = []
  for i, swap in enumerate([
                           [['  write_files = *restraints pdb_core pdb_buffer pdb_final_core pdb_final_buffer',
                             '  write_files = *restraints pdb_core pdb_buffer pdb_final_core *pdb_final_buffer']],
                           [['  write_files = *restraints pdb_core pdb_buffer pdb_final_core pdb_final_buffer',
                             '  write_files = *restraints *pdb_core *pdb_buffer *pdb_final_core *pdb_final_buffer']],
                           [['  write_files = *restraints pdb_core pdb_buffer pdb_final_core pdb_final_buffer',
                             '  write_files = restraints pdb_core pdb_buffer pdb_final_core *pdb_final_buffer']],
    ]):
    swap.append(['  selection = None', '  selection = resid 2'])
    tests.append([runs_fine,
                 (model_1yjp_h,),
                 {'adding': 300+i,
                  'swaps' : swap}]
                 )
  for i, (func, args, kwds) in enumerate(tests):
    if only_i is not None and only_i!=i+1: continue
    print(i,only_i,func, kwds)
    func(*args, **kwds)

if __name__ == '__main__':
  main(*tuple(sys.argv[1:]))
