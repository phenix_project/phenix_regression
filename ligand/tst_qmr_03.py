import os, sys
from mmtbx.regression import model_1yjp, model_1aL1
from libtbx import easy_run
from mmtbx.geometry_restraints.quantum_interface import get_qm_restraints_scope

from tst_qmr import get_h_model, runs_fine

def main(only_i=None):

  model_1yjp_h = get_h_model(model_1yjp)
  outl = ''
  for line in model_1aL1.splitlines():
    if line.find('ACE')>-1: continue
    outl += '%s\n' % line
  model_1aL1_h = get_h_model(outl)

  pf = 'tst_qmr_03.pdb'
  f=open(pf, 'w')
  f.write(model_1yjp_h)
  del f
  cmd = 'mmtbx.quantum_interface %s iterate_NQH=ASN write_qmr=1 format=qi qi.selection="chain A and resid 3 and resname ASN"' % pf
  print(cmd)
  rc = easy_run.go(cmd)
  # print('\n'.join(rc.stdout_lines))

  pf = 'tst_qmr_03_A_3_ASN.phil'
  f=open(pf, 'r')
  lines=f.read()
  del f
  lines=lines.replace('*mopac test', 'mopac *test')
  f=open(pf, 'w')
  f.write(lines)
  del f

  cmd = 'mmtbx.quantum_interface tst_qmr_03.pdb run_qmr=True iterate_NQH=ASN tst_qmr_03_A_3_ASN.phil'
  print(cmd)
  rc = easy_run.go(cmd)
  assert '\n'.join(rc.stdout_lines).find(' flipped ')>-1, 'ERROR\n%s' % '\n'.join(rc.stdout_lines)

if __name__ == '__main__':
  main(*tuple(sys.argv[1:]))
