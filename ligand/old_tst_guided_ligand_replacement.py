from __future__ import print_function
import os, sys
import pickle

from libtbx import easy_run

from elbow.command_line import builder

def run():
  pdb_directory = os.environ["PDB_MIRROR_PDB"]
  mtz_directory = os.path.join(os.environ["MTZ_MIRROR"],
                               "mtz_files",
                               )
  print('PDB directory',pdb_directory)
  print('MTZ directory',mtz_directory)
  args = [
    ["3a37", "3a36", "AMP"],
    ["3a37", "3a36", "ADP"],
    ["3a37", "3a36", "ATP"],
    ]
  for guide_pdb_code, current_pdb_code, ligand_code in args:
    print('-'*80)
    print("guide PDB code")
    print(guide_pdb_code)
    guide_pdb_filename = os.path.join(pdb_directory,
                                      guide_pdb_code[1:3],
                                      "pdb%s.ent.gz" % guide_pdb_code,
                                      )
    print(guide_pdb_filename)
    if not os.path.exists(guide_pdb_filename):
      print('file not found',guide_pdb_filename)
      continue
    print(guide_pdb_code)
    current_pdb_filename = os.path.join(pdb_directory,
                                        current_pdb_code[1:3],
                                        "pdb%s.ent.gz" % current_pdb_code,
                                        )
    print(current_pdb_filename)
    if not os.path.exists(current_pdb_filename):
      print('file not found',current_pdb_filename)
      continue
    current_mtz_filename = os.path.join(mtz_directory,
                                        "%s.mtz" % current_pdb_code,
                                        )
    print(current_mtz_filename)
    if not os.path.exists(current_mtz_filename):
      print('file not found',current_mtz_filename)
      current_mtz_filename = "pdb%s_ent_gz_fake.mtz" % current_pdb_code
      if not os.path.exists(current_mtz_filename):
        cmd = "iotbx.pdb.as_xray_structure --fake-f-obs-and-r-free-flags-d-min=2.5 %s" % current_pdb_filename
        easy_run.call(cmd)
    print(current_mtz_filename)

    # generate map coeffs
    map_coeffs_filename = "pdb%s.ent_refine_001_map_coeffs.mtz" % current_pdb_code
    if not os.path.exists(map_coeffs_filename):
      cmd = "phenix.refine %s %s refinement.main.number_of_macro_cycles=0" % (
        current_pdb_filename,
        current_mtz_filename,
        )
      print(cmd)
      easy_run.call(cmd)

    if not os.path.exists(map_coeffs_filename):
      print('file not found',map_coeffs_filename)
      continue

    # generate ligand input
    ligand_filename = "%s.pickle" % ligand_code
    if os.path.exists(ligand_filename):
      f = open(ligand_filename, "rb")
      molecule = pickle.load(f)
      f.close()
    else:
      molecule = builder.run(chemical_component=ligand_code,
                             quiet=True,
                             output=ligand_code,
                             )

    # run guided_ligand_replacement
    for ligand_filename in [
      "%s.pickle" % ligand_code,
      "%s.cif" % ligand_code,
      "%s.pdb" % ligand_code,
      ]:
      print('using',ligand_filename)
      cmd = """phenix.guided_ligand_replacement guide_pdb_file_name=%s \
                                                protein_pdb_file_name=%s \
                                                ligand_input_file_name=%s \
                                                map_coeffs_file_name=%s
      """ % (guide_pdb_filename,
             current_pdb_filename,
             ligand_filename,
             map_coeffs_filename,
             )
      print("RUNNING "*10)
      print(cmd)
      easy_run.call(cmd)
  print("OK")

if __name__=="__main__":
  run()
