from __future__ import print_function
import os, sys
from six.moves import cStringIO as StringIO
import libtbx.load_env
import time

from libtbx import easy_run

repository_dir = os.path.dirname(libtbx.env.dist_path("phenix_regression"))
ligand_dir = os.path.join(repository_dir,
                          "phenix_regression",
                          "ligand",
                          )

def run():
  t0=time.time()
  cmd = 'phenix.guided_ligand_replacement'
  cmd += ' guide_pdb_file_name=%s' % os.path.join(ligand_dir, '3l8x.pdb')
  cmd += ' ligand_cif_file_name=%s' % os.path.join(ligand_dir, '38P.cif')
  cmd += ' protein_pdb_file_name=%s' % os.path.join(ligand_dir,
                                                    '3mvl.pdb_modified.pdb',
                                                    )
  cmd += ' ligand_selection_in_guide_model="resname N4D"'
  print(cmd)
  ero = easy_run.fully_buffered(command=cmd)
  err = StringIO()
  ero.show_stdout(out=err)

  rmsd = 0
  found = 0
  for line in err.getvalue().split("\n"):
    if line.find("Superpose RMSD")>-1:
      tmp = line.split()
      assert float(tmp[5])<1
      rmsd +=1
    if line.find("Found")>-1:
      tmp = line.split()
      assert int(tmp[1])==17
      found +=1
  assert rmsd==1, rmsd
  assert found==1, found
  print('time %0.1f' % (time.time()-t0))
  print("OK")

if __name__=="__main__":
  run()
