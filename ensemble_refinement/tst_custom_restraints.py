
from __future__ import division
from __future__ import print_function
from libtbx import easy_run
from libtbx.test_utils import assert_lines_in_text

def exercise_ensemble_refinement_extra_phil () :
  pdb_in = """
HETATM 4154 MG    MG A 341       1.384  19.340  11.968  1.00 67.64          MG
HETATM 4155 MG    MG A 342       3.277  24.927   8.421  1.00 50.39          MG
HETATM 4158  PG  AGS A 340       0.280  21.925   8.908  1.00 71.92           P
HETATM 4159  S1G AGS A 340       0.063  22.372   6.949  1.00 73.73           S
HETATM 4160  O2G AGS A 340       1.848  21.862   9.226  1.00 69.56           O
HETATM 4161  O3G AGS A 340      -0.394  20.577   9.175  1.00 71.04           O
HETATM 4162  PB  AGS A 340      -0.181  23.047  11.491  1.00 66.45           P
HETATM 4163  O1B AGS A 340      -1.491  23.286  12.095  1.00 67.78           O
HETATM 4164  O2B AGS A 340       0.580  21.738  11.841  1.00 65.98           O
HETATM 4165  O3B AGS A 340      -0.268  23.073   9.888  1.00 68.73           O
HETATM 4166  PA  AGS A 340       2.209  24.398  12.120  1.00 56.53           P
HETATM 4167  O1A AGS A 340       2.494  23.844  13.561  1.00 56.34           O
HETATM 4168  O2A AGS A 340       3.071  23.628  11.115  1.00 56.96           O
HETATM 4169  O3A AGS A 340       0.728  24.297  11.692  1.00 62.02           O
HETATM 4170  O5' AGS A 340       2.410  25.997  12.136  1.00 54.18           O
HETATM 4171  C5' AGS A 340       1.534  26.899  11.359  1.00 51.58           C
HETATM 4172  C4' AGS A 340       2.177  28.275  11.386  1.00 48.20           C
HETATM 4173  O4' AGS A 340       2.053  28.722  12.737  1.00 47.27           O
HETATM 4174  C3' AGS A 340       3.669  28.243  11.175  1.00 48.70           C
HETATM 4175  O3' AGS A 340       4.108  27.899   9.817  1.00 47.57           O
HETATM 4176  C2' AGS A 340       3.974  29.568  11.886  1.00 47.76           C
HETATM 4177  O2' AGS A 340       3.674  30.760  11.137  1.00 49.68           O
HETATM 4178  C1' AGS A 340       3.110  29.492  13.096  1.00 46.98           C
HETATM 4179  N9  AGS A 340       3.749  28.890  14.289  1.00 46.39           N
HETATM 4180  C8  AGS A 340       3.769  27.614  14.756  1.00 46.18           C
HETATM 4181  N7  AGS A 340       4.448  27.473  15.895  1.00 45.28           N
HETATM 4182  C5  AGS A 340       4.887  28.750  16.164  1.00 45.77           C
HETATM 4183  C6  AGS A 340       5.661  29.287  17.235  1.00 46.11           C
HETATM 4184  N6  AGS A 340       6.130  28.675  18.212  1.00 46.60           N
HETATM 4185  N1  AGS A 340       5.875  30.642  17.118  1.00 45.68           N
HETATM 4186  C2  AGS A 340       5.409  31.437  16.089  1.00 45.91           C
HETATM 4187  N3  AGS A 340       4.688  30.971  15.086  1.00 46.57           N
HETATM 4188  C4  AGS A 340       4.462  29.625  15.179  1.00 45.96           C
END"""
  ens_ref_phil = """
ensemble_refinement.geometry_restraints.edits {
  bond {
    action = *add
    atom_selection_1 = name MG   and chain A and resname MG and resseq  341
    atom_selection_2 = name  O2B and chain A and resname AGS and resseq  340
    distance_ideal = 2.090000
    sigma = 0.250
  }
}
"""
  open("tst_ens_ref_phil.pdb", "w").write(pdb_in)
  open("tst_ens_ref_phil.edits", "w").write(ens_ref_phil)
  args = [
    "phenix.fmodel",
    "tst_ens_ref_phil.pdb",
    "high_resolution=3.0",
    "generate_fake_p1_symmetry=True",
    "type=real",
    "label=F",
    "r_free_flags_fraction=0.1",
    "output.file_name=tst_ens_ref_phil.mtz",
  ]
  print(" ".join(args))
  result = easy_run.fully_buffered(" ".join(args)).raise_if_errors()
  assert result.return_code == 0
  args = [
    "phenix.ensemble_refinement",
    "tst_ens_ref_phil.pdb",
    "tst_ens_ref_phil.mtz",
    "extra_restraints_file=tst_ens_ref_phil.edits",
    "--dry_run",
  ]
  print(" ".join(args))
  result = easy_run.fully_buffered(" ".join(args))
  assert result.return_code == 0, "\n".join(result.stdout_lines)
  assert ("Processing custom geometry restraints in file:" in
          result.stdout_lines), "\n".join(result.stdout_lines)
  assert ("""\
        atom_selection_1 = name MG and chain A and resname MG and resseq 341""" in
    result.stdout_lines)
  assert ("""extra_restraints_file = None""" in result.stdout_lines)
  assert result.stdout_lines[-1].startswith("Unit cell volume")
  assert_lines_in_text("\n".join(result.stdout_lines), """\
  Custom bonds:
    bond:
      atom 1: "HETATM    1 MG    MG A 341 .*.    MG  "
      atom 2: "HETATM   22  O2B AGS A 340 .*.     O  "
      symmetry operation: x,y,z
      distance_model:   2.532
      distance_ideal:   2.090
  """)
  print("OK")

if (__name__ == "__main__") :
  exercise_ensemble_refinement_extra_phil()
