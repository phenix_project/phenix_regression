
from __future__ import division
from __future__ import print_function
from cctbx.array_family import flex
from libtbx import smart_open
from libtbx import easy_run
import libtbx.load_env
import random, time, os

random.seed(0)
flex.set_random_seed(0)

def exercise():
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/ensemble_refinement/3PZZ.pdb",
    test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/ensemble_refinement/3pzz-sf.mtz", \
    test=os.path.isfile)
  if (os.path.isfile("ens_ref_mlhl.pdb.gz")) :
    os.remove("ens_ref_mlhl.pdb.gz")
  args = [
    "phenix.reciprocal_space_arrays",
    "\"%s\"" % pdb,
    "\"%s\"" % hkl,
    "output_file_name=3pss_phases.mtz",
  ]
  rc = easy_run.fully_buffered(" ".join(args)).raise_if_errors().return_code
  assert (rc == 0)
  assert os.path.isfile("3pss_phases.mtz")
  args = [ "phenix.ensemble_refinement",
    "\"%s\"" % pdb,
    "3pss_phases.mtz",
    "xray_data.labels='FOBS,SIGFOBS'",
    "experimental_phases.labels='HLmodelA,HLmodelB,HLmodelC,HLmodelD'",
    "use_experimental_phases=True",
    "tx=0.1",
    "ptls=0.95",
    "acquisition_block_n_tx=1",
    "equilibrium_n_tx=1",
    "number_of_acquisition_periods=2",
    "output_file_prefix=ens_ref_mlhl",
    "harmonic_restraints.selections='chain a and resseq 5'",
  ]
  result = easy_run.fully_buffered(" ".join(args)).raise_if_errors()
  assert (result.return_code == 0), "\n".join(result.stdout_lines)
  assert "Using MLHL target with experimental phases" in result.stdout_lines
  pdb_in = smart_open.for_reading("ens_ref_mlhl.pdb.gz")
  lines = pdb_in.readlines()
  cntr = 0
  n_models = 0
  # FIXME this is going to break every time the author list is updated, if
  # not sooner
  for byte_or_text_line in lines:
    try:
      byte_or_text_line.startswith("a")
      line = byte_or_text_line
    except Exception as e: # python 3.8
      line = byte_or_text_line.decode("utf-8")
    if(line.startswith("REMARK   3 TIME-AVERAGED ENSEMBLE REFINEMENT.")):
      cntr += 1
    if(line.startswith("REMARK   3   PROGRAM     : PHENIX (phenix.ensemble_refinement")):
      cntr += 1
    if(line.startswith("REMARK   3   AUTHORS     : Adams,Afonine,Bunkoczi")):
      cntr += 1
    if(line.startswith("MODEL   ")):
      n_models += 1
  assert cntr == 3
  assert n_models > 1

if (__name__ == "__main__"):
  t0 = time.time()
  exercise()
  print("Total time: %8.3f"%(time.time() - t0))
  print("OK.")
