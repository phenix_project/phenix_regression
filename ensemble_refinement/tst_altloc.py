
# This test tests running with alternative conformations

from __future__ import division
from __future__ import print_function
from mmtbx.refinement import ensemble_refinement
from mmtbx.programs import fmodel
from iotbx.cli_parser import run_program
from libtbx.utils import null_out
import libtbx.load_env
import os

def exercise () :
  pdb_file = libtbx.env.find_in_repositories(
    relative_path=\
      "phenix_regression/pdb/1akg.pdb",
    test=os.path.isfile)

  mtz_file = "tst_ensemble_altloc.mtz"
  args = [
    pdb_file,
    "high_resolution=3.0",
    "type=real",
    "label=F",
    "r_free_flags_fraction=0.1",
    "random_seed=12345",
    "output.file_name=%s" % mtz_file,
  ]
  print('args',args)
  run_program(program_class=fmodel.Program, args=args, logger=null_out())
  args = [
    pdb_file,
    mtz_file,
    "acquisition_block_n_tx=1",
    "equilibrium_n_tx=1",
    "number_of_acquisition_periods=2",
  ]
  print('args',args)
  ensemble_refinement.run(args=args, out=null_out(), replace_stderr=False)

if (__name__ == "__main__") :
  exercise()
  print("OK")
