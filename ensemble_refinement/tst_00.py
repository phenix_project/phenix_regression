from __future__ import print_function
from cctbx.array_family import flex
from libtbx import easy_run
import libtbx.load_env
import random, time, os

random.seed(0)
flex.set_random_seed(0)

def exercise():
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/ensemble_refinement/3PZZ.pdb",
    test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/ensemble_refinement/3pzz-sf.mtz", \
    test=os.path.isfile)
  par = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/ensemble_refinement/params", \
    test=os.path.isfile)
  if (os.path.isfile("ens_ref.pdb")) :
    os.remove("ens_ref.pdb")
  cmd = "phenix.ensemble_refinement %s %s %s > ens_ref_tmp.log"%(pdb,hkl,par)
  print(cmd)
  assert not easy_run.call(cmd)
  assert not easy_run.call("gunzip ens_ref.pdb.gz")
  lines = open("ens_ref.pdb","r").readlines()
  cntr = 0
  n_models = 0
  # FIXME this is going to break every time the author list is updated, if
  # not sooner
  for line in lines:
    if(line.startswith("REMARK   3 TIME-AVERAGED ENSEMBLE REFINEMENT.")):
      cntr += 1
    if(line.startswith("REMARK   3   PROGRAM     : PHENIX (phenix.ensemble_refinement")):
      cntr += 1
    if(line.startswith("REMARK   3   AUTHORS     : Adams,Afonine,Bunkoczi")):
      cntr += 1
    if(line.startswith("MODEL   ")):
      n_models += 1
  assert cntr == 3
  assert n_models > 1

if (__name__ == "__main__"):
  t0 = time.time()
  exercise()
  print("Total time: %8.3f"%(time.time() - t0))
  print("OK.")
