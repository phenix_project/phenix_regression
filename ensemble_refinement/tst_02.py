from __future__ import print_function
from cctbx.array_family import flex
from libtbx import easy_run
import libtbx.load_env
import random, time, os
from libtbx.test_utils import approx_equal

random.seed(0)
flex.set_random_seed(0)

'''

  RMSD (mean delta per restraint)
    bond      : 0.0539085
    angle     : 5.15056
    chirality : 0.306137
    planarity : 0.0238453
    dihedral  : 14.9216
  RMSD (mean RMSD per structure)
    bond      : 0.0581719
    angle     : 5.40359
    chirality : 0.31689
    planarity : 0.0244358
    dihedral  : 20.7086

'''
results = {
  'bond' : [0.0539085, 0.0581719],
  'angle': [5.15056, 5.40359],
}
eps = {
  'bond' : 0.01,
  'angle': 1,
  }

def exercise():
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/ensemble_refinement/3sgo_redo.pdb",
    test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/ensemble_refinement/3sgo_redo.mtz", \
    test=os.path.isfile)
  cmd = 'phenix.AmberPrep %s' % pdb
  assert not easy_run.call(cmd)
  log_filename = 'log-0.8-5.0-0.01_amber.log'
  cmd =  'phenix.ensemble_refinement %s %s' % ('4phenix_%s' % os.path.basename(pdb),
                                               hkl)
  # tx = 0.1
  # ptls = 0.95
  cmd += ' import_tls_pdb=None den_restraints=True den.weight=30'
  cmd += ' ptls=0.8 tx=0.01 number_of_acquisition_periods=2 equilibrium_n_tx=1'
  cmd += ' acquisition_block_n_tx=1 wxray_coupled_tbath_offset=5.0'
  cmd += ' use_amber=True amber.topology_file_name=4amber_3sgo_redo.prmtop'
  cmd += ' amber.coordinate_file_name=4amber_3sgo_redo.rst7'
  cmd += ' amber.order_file_name=4amber_3sgo_redo.order'
  cmd += ' >& %s' % log_filename
  print(cmd)
  assert not easy_run.call(cmd)
  lines = open(log_filename).read()
  tmp = lines.split('============================ Ensemble RMSD summary')
  for line in tmp[1].splitlines():
    print(line)
    for internal in ['bond', 'angle']:
      if line.find(internal)>-1:
        value = float(line.split()[-1])
        for rc in results[internal]:
          if approx_equal(rc, value, eps.get(internal, 1e-6), out=None): break
        else:
          assert 0

if (__name__ == "__main__"):
  t0 = time.time()
  exercise()
  print("Total time: %8.3f"%(time.time() - t0))
  print("OK.")
