from __future__ import print_function
from cctbx.array_family import flex
from libtbx import easy_run
import libtbx.load_env
import random, time, os
from libtbx.test_utils import approx_equal

random.seed(0)
flex.set_random_seed(0)

'''

  RMSD (mean delta per restraint)
    bond      : 0.0064916
    angle     : 0.815689
    chirality : 0.0603485
    planarity : 0.00330531
    dihedral  : 14.494
  RMSD (mean RMSD per structure)
    bond      : 0.00725965
    angle     : 0.918666
    chirality : 0.0623703
    planarity : 0.00352571
    dihedral  : 20.8461
'''
results = {
  'bond' : [0.00711566, 0.00793102],
  'angle': [0.787835, 0.914261],
}
eps = {
  'bond' : 0.001,
  'angle': 0.001,
  }

def exercise():
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/ensemble_refinement/3sgo_redo.pdb",
    test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/ensemble_refinement/3sgo_redo.mtz", \
    test=os.path.isfile)
  log_filename = 'log-0.8-5.0-0.01.log'
  cmd =  'phenix.ensemble_refinement const_shrink_donor_acceptor=0.6 %s %s' % (pdb,hkl)
  cmd += ' import_tls_pdb=None den_restraints=True den.weight=30'
  cmd += ' ptls=0.8 tx=0.01 number_of_acquisition_periods=3'
  cmd += ' acquisition_block_n_tx=10 wxray_coupled_tbath_offset=5.0'
  cmd += ' >& %s' % log_filename
  assert not easy_run.call(cmd)
  f=open(log_filename, 'rb')
  lines = f.readlines()
  lines = [l.decode("utf-8") for l in lines]
  lines = "".join(lines)
  del f
  tmp = lines.split('============================ Ensemble RMSD summary')
  for line in tmp[1].splitlines():
    for internal in ['bond', 'angle']:
      if internal in line:
        value = float(line.split()[-1])
        for rc in results[internal]:
          if approx_equal(rc, value, eps.get(internal, 1e-6), out=None): break
        else:
          assert 0

if (__name__ == "__main__"):
  t0 = time.time()
  exercise()
  print("Total time: %8.3f"%(time.time() - t0))
  print("OK.")
