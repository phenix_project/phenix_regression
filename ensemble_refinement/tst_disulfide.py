
# This test uses a single CYS residue with a disulfide bond across the
# symmetry axis.

from __future__ import division
from __future__ import print_function
from mmtbx.refinement import ensemble_refinement
from mmtbx.programs import fmodel
from iotbx.cli_parser import run_program
from libtbx.utils import null_out

def exercise () :
  pdb_str = """\
CRYST1   10.000    6.000   10.000  90.00  90.00  90.00 P 1 2 1
SCALE1      0.100000  0.000000  0.000000        0.00000
SCALE2      0.000000  0.166667  0.000000        0.00000
SCALE3      0.000000  0.000000  0.100000        0.00000
HETATM    1  N   CYS A   1       7.233   1.797   0.643  1.00 12.00      A    N
HETATM    2  CA  CYS A   1       6.900   2.248   1.988  1.00 10.00      A    C
HETATM    3  C   CYS A   1       7.851   1.641   3.015  1.00 12.00      A    C
HETATM    4  O   CYS A   1       8.086   2.213   4.079  1.00 16.00      A    O
HETATM    5  CB  CYS A   1       5.454   1.884   2.331  1.00 18.00      A    C
HETATM    6  SG  CYS A   1       4.931   2.389   3.987  1.00 20.00      A    S
HETATM    7  OXT CYS A   1       8.409   0.565   2.802  1.00 16.00      A    O1-
HETATM    8  H   CYS A   1       6.564   1.786   0.102  1.00 20.00      A    H
HETATM    9  H2  CYS A   1       8.152   1.692   0.386  1.00 20.00      A    H
HETATM   10  HA  CYS A   1       6.987   3.213   2.030  1.00 20.00      A    H
HETATM   11  HB2 CYS A   1       5.353   0.922   2.271  1.00 20.00      A    H
HETATM   12  HB3 CYS A   1       4.865   2.316   1.692  1.00 20.00      A    H
TER"""
  pdb_file = "tst_ensemble_disulfide.pdb"
  mtz_file = "tst_ensemble_disulfide.mtz"
  open(pdb_file, "w").write(pdb_str)
  args = [
    pdb_file,
    "high_resolution=3.0",
    "type=real",
    "label=F",
    "r_free_flags_fraction=0.1",
    "random_seed=12345",
    "output.file_name=%s" % mtz_file,
  ]
  run_program(program_class=fmodel.Program, args=args, logger=null_out())
  args = [
    pdb_file,
    mtz_file,
    "acquisition_block_n_tx=1",
    "equilibrium_n_tx=1",
    "number_of_acquisition_periods=2",
  ]
  ensemble_refinement.run(args=args, out=null_out(), replace_stderr=False)

if (__name__ == "__main__") :
  exercise()
  print("OK")
