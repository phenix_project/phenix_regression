
from __future__ import division
from __future__ import print_function
from cctbx.array_family import flex
from libtbx import smart_open
from libtbx import easy_run
import libtbx.load_env
import random, time, os
import sys

random.seed(0)
flex.set_random_seed(0)

def exercise(nproc=1):
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/ensemble_refinement/3PZZ.pdb",
    test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/ensemble_refinement/3pzz-sf.mtz", \
    test=os.path.isfile)
  args = [
    pdb,
    hkl,
    "nproc=%s" % str(nproc),
    "tx=0.1",
    "ptls=1.0,0.9,0.8,0.6",
    "acquisition_block_n_tx=1",
    "equilibrium_n_tx=1",
    "number_of_acquisition_periods=2",
    "output_file_prefix=ens_ref_par",
    "harmonic_restraints.selections='chain a and resseq 5'",
  ]
  if (os.path.isfile("ens_ref_par.pdb")) :
    os.remove("ens_ref_par.pdb")
  cmd = "phenix.ensemble_refinement %s > ens_ref_par_tmp.log" % " ".join(args)
  assert (easy_run.call(cmd) == 0)
  lines = smart_open.for_reading("ens_ref_par.pdb.gz").readlines()
  cntr = 0
  n_models = 0
  # FIXME this is going to break every time the author list is updated, if
  # not sooner
  for line in lines:
    if(line.startswith("REMARK   3 TIME-AVERAGED ENSEMBLE REFINEMENT.")):
      cntr += 1
    if(line.startswith("REMARK   3   PROGRAM     : PHENIX (phenix.ensemble_refinement")):
      cntr += 1
    if(line.startswith("REMARK   3   AUTHORS     : Adams,Afonine,Burnley,Chen")):
      cntr += 1
    if(line.startswith("REMARK   3               : Gopal,Gros,Grosse-Kunstleve,")):
      cntr += 1
    if(line.startswith("MODEL   ")):
      n_models += 1
  assert cntr == 4
  assert n_models > 1

if (__name__ == "__main__"):
  t0 = time.time()
  nproc = 1
  for arg in sys.argv[1:] :
    if (arg.startswith("--nproc=") or arg.startswith("nproc=")) :
      nproc = arg.split("=")[1]
  exercise(nproc=nproc)
  print("Total time: %8.3f"%(time.time() - t0))
  print("OK.")
