from __future__ import print_function
from libtbx import easy_run
from elbow.programs import charges

from iotbx.cli_parser import run_program

pdbs = [
  '''CRYST1   33.622   34.533   43.071  90.00  90.00  90.00 P 21 21 21
SCALE1      0.029742  0.000000  0.000000        0.00000
SCALE2      0.000000  0.028958  0.000000        0.00000
SCALE3      0.000000  0.000000  0.023217        0.00000
ATOM      1  N   CYS A   5      13.740   7.801   1.135  1.00  2.51           N
ATOM      2  CA  CYS A   5      12.400   8.307   0.886  1.00  2.23           C
ATOM      3  C   CYS A   5      12.525   9.412  -0.157  1.00  2.46           C
ATOM      4  O   CYS A   5      13.134  10.458   0.099  1.00  2.79           O
ATOM      5  CB  CYS A   5      11.813   8.880   2.168  1.00  2.81           C
ATOM      6  SG  CYS A   5      10.150   9.570   1.843  1.00  2.67           S
ATOM         OXT CYS A   5      12.017   9.269  -1.269  1.00  2.46           O
ATOM         H1  CYS A   5      13.980   7.934   2.138  0.00  2.51           H
ATOM         H2  CYS A   5      13.777   6.788   0.901  0.00  2.51           H
ATOM         H3  CYS A   5      14.421   8.319   0.544  0.00  2.51           H
ATOM      7  HA  CYS A   5      11.730   7.520   0.541  0.00  2.23           H
ATOM      8  HB2 CYS A   5      11.725   8.091   2.915  0.00  2.81           H
ATOM      9  HB3 CYS A   5      12.456   9.677   2.541  0.00  2.81           H
ATOM     10  N   CYS A   8      11.107  13.019   1.219  1.00  3.02           N
ATOM     11  CA  CYS A   8      11.655  13.847   2.283  1.00  3.00           C
ATOM     12  C   CYS A   8      13.137  13.626   2.568  1.00  2.87           C
ATOM     13  O   CYS A   8      13.713  14.389   3.349  1.00  3.31           O
ATOM     14  CB  CYS A   8      10.859  13.681   3.587  1.00  3.30           C
ATOM     15  SG  CYS A   8      11.225  12.126   4.471  1.00  2.86           S
ATOM         OXT CYS A   8      13.737  12.700   2.023  1.00  2.87           O
ATOM         H1  CYS A   8      10.338  12.430   1.599  0.00  3.02           H
ATOM         H2  CYS A   8      10.736  13.628   0.462  0.00  3.02           H
ATOM         H3  CYS A   8      11.855  12.407   0.836  0.00  3.02           H
ATOM     16  HA  CYS A   8      11.563  14.873   1.928  0.00  3.00           H
ATOM     17  HB2 CYS A   8      11.100  14.509   4.254  0.00  3.30           H
ATOM     18  HB3 CYS A   8       9.795  13.689   3.352  0.00  3.30           H
ATOM     19  N   CYS A  38       7.129   6.012   5.828  1.00  3.25           N
ATOM     20  CA  CYS A  38       8.241   6.849   6.230  1.00  2.30           C
ATOM     21  C   CYS A  38       7.912   7.491   7.578  1.00  2.42           C
ATOM     22  O   CYS A  38       6.860   8.137   7.706  1.00  2.72           O
ATOM     23  CB  CYS A  38       8.435   7.950   5.191  1.00  2.33           C
ATOM     24  SG  CYS A  38       9.799   9.064   5.628  1.00  2.51           S
ATOM         OXT CYS A  38       8.688   7.365   8.525  1.00  2.42           O
ATOM         H1  CYS A  38       6.750   6.351   4.921  0.00  3.25           H
ATOM         H2  CYS A  38       7.455   5.030   5.722  0.00  3.25           H
ATOM         H3  CYS A  38       6.384   6.054   6.552  0.00  3.25           H
ATOM     25  HA  CYS A  38       9.159   6.267   6.311  0.00  2.30           H
ATOM     26  HB2 CYS A  38       8.661   7.496   4.226  0.00  2.33           H
ATOM     27  HB3 CYS A  38       7.521   8.540   5.120  0.00  2.33           H
ATOM     28  N   CYS A  41       7.724  11.698   6.903  1.00  3.33           N
ATOM     29  CA  CYS A  41       6.716  12.358   6.087  1.00  2.90           C
ATOM     30  C   CYS A  41       5.456  11.531   5.857  1.00  2.68           C
ATOM     31  O   CYS A  41       4.488  12.061   5.299  1.00  3.05           O
ATOM     32  CB  CYS A  41       7.300  12.794   4.736  1.00  3.39           C
ATOM     33  SG  CYS A  41       7.585  11.410   3.579  1.00  2.71           S
ATOM         OXT CYS A  41       5.415  10.357   6.225  1.00  2.68           O
ATOM         H1  CYS A  41       8.603  11.598   6.355  0.00  3.33           H
ATOM         H2  CYS A  41       7.908  12.267   7.754  0.00  3.33           H
ATOM         H3  CYS A  41       7.381  10.757   7.183  0.00  3.33           H
ATOM     34  HA  CYS A  41       6.403  13.241   6.645  0.00  2.90           H
ATOM     35  HB2 CYS A  41       6.606  13.488   4.261  0.00  3.39           H
ATOM     36  HB3 CYS A  41       8.257  13.285   4.910  0.00  3.39           H
TER
HETATM   37 FE    FE A 101       9.689  10.568   3.866  1.00  2.39          Fe
''',
]
pdbs.append(pdbs[0])
pdbs.append(pdbs[0].replace(
  'HETATM   37 FE    FE A 101       9.689  10.568   3.866  1.00  2.39          Fe',
  'HETATM   37 FE    FE A 101       9.689  10.568   3.866  1.00  2.39          Fe3+'))
pdbs.append(pdbs[0].replace(
  'HETATM   37 FE    FE A 101       9.689  10.568   3.866  1.00  2.39          Fe',
  'HETATM   37 FE    FE A 101       9.689  10.568   3.866  1.00  2.39          Fe3+'))

phils = ['''
inputs {
  specific_atom_charges
  {
    atom_selection = resname FE
    charge = 2
  }
}
''',
]

def main():
  with open('iron.phil', 'w') as f:
    f.write(phils[0])
  #
  ptr = 0
  filename = 'tst_electrons_02_%02d.pdb' % ptr
  with open(filename, 'w') as f:
    f.write(pdbs[ptr])
  #
  args = [filename]
  try: results=run_program(program_class=charges.Program, args=args)
  except Exception as e: results = 'Error'
  assert results=='Error'
  #
  for ptr in range(1,4):
    filename = 'tst_electrons_%02d.pdb' % ptr
    with open(filename, 'w') as f:
      f.write(pdbs[ptr])
    #
    if ptr==1:
      args = [filename, 'iron.phil']
      tc=-2
    elif ptr==2:
      args = [filename, 'iron.phil']
      tc=-2
    elif ptr==3:
      args = [filename]
      tc=-1
    #
    results=run_program(program_class=charges.Program, args=args)
    assert results.total_charge==tc
    #
    args += ['link_metals=True']
    results=run_program(program_class=charges.Program, args=args)
    print(results)
    assert results.total_charge==tc

if __name__ == '__main__':
  main()
