from __future__ import print_function
from libtbx import easy_run
from elbow.programs import charges

from iotbx.cli_parser import run_program

pdbs = [
  '''CRYST1   45.022   96.047  103.889  90.00  90.00  90.00 P 21 21 21    0
ATOM      1  N   CYS A 249     -21.102  34.926  38.471  1.00 14.94           N
ATOM      2  CA  CYS A 249     -20.557  34.523  37.177  1.00 15.84           C
ATOM      3  C   CYS A 249     -21.592  33.613  36.520  1.00 17.71           C
ATOM      4  O   CYS A 249     -22.196  32.772  37.192  1.00 17.96           O
ATOM      5  CB  CYS A 249     -19.246  33.752  37.369  1.00 13.37           C
ATOM      6  SG  CYS A 249     -18.532  33.062  35.853  1.00 16.65           S
ATOM      0  HA  CYS A 249     -20.372  35.301  36.628  1.00 15.84           H   new
ATOM      0  HB2 CYS A 249     -18.595  34.345  37.776  1.00 13.37           H   new
ATOM      0  HB3 CYS A 249     -19.401  33.028  37.996  1.00 13.37           H   new
ATOM      7  N   CYS A 252     -19.827  29.795  36.725  1.00 19.22           N
ATOM      8  CA  CYS A 252     -19.517  29.132  37.992  1.00 21.00           C
ATOM      9  C   CYS A 252     -20.216  29.696  39.234  1.00 19.26           C
ATOM     10  O   CYS A 252     -20.178  29.082  40.307  1.00 17.05           O
ATOM     11  CB  CYS A 252     -18.005  29.128  38.207  1.00 25.53           C
ATOM     12  SG  CYS A 252     -17.474  28.157  39.617  1.00 43.08           S
ATOM      0  HA  CYS A 252     -19.871  28.234  37.898  1.00 21.00           H   new
ATOM      0  HB2 CYS A 252     -17.575  28.784  37.408  1.00 25.53           H   new
ATOM      0  HB3 CYS A 252     -17.701  30.042  38.322  1.00 25.53           H   new
ATOM      0  HG  CYS A 252     -18.394  28.015  40.375  1.00 43.08           H   new
ATOM     13  N   CYS A 269     -13.556  37.140  36.550  1.00 15.31           N
ATOM     14  CA  CYS A 269     -14.020  36.103  37.483  1.00 15.56           C
ATOM     15  C   CYS A 269     -12.764  35.444  38.049  1.00 18.19           C
ATOM     16  O   CYS A 269     -11.979  34.847  37.307  1.00 18.29           O
ATOM     17  CB  CYS A 269     -14.857  35.030  36.783  1.00 14.60           C
ATOM     18  SG  CYS A 269     -15.227  33.619  37.904  1.00 16.23           S
ATOM      0  HA  CYS A 269     -14.579  36.507  38.165  1.00 15.56           H   new
ATOM      0  HB2 CYS A 269     -15.687  35.421  36.469  1.00 14.60           H   new
ATOM      0  HB3 CYS A 269     -14.382  34.707  36.001  1.00 14.60           H   new
ATOM     19  N   CYS A 272     -12.806  31.333  37.125  1.00 18.59           N
ATOM     20  CA  CYS A 272     -12.708  30.925  35.723  1.00 19.63           C
ATOM     21  C   CYS A 272     -11.437  31.420  35.032  1.00 20.51           C
ATOM     22  O   CYS A 272     -10.978  30.817  34.058  1.00 21.36           O
ATOM     23  CB  CYS A 272     -13.923  31.420  34.922  1.00 18.23           C
ATOM     24  SG  CYS A 272     -15.502  30.581  35.270  1.00 20.55           S
ATOM      0  HA  CYS A 272     -12.681  29.956  35.739  1.00 19.63           H   new
ATOM      0  HB2 CYS A 272     -14.036  32.368  35.092  1.00 18.23           H   new
ATOM      0  HB3 CYS A 272     -13.727  31.323  33.977  1.00 18.23           H   new
TER
HETATM   25 ZN    ZN A 300     -16.593  31.972  36.863  1.00 16.50          Zn
END
''',
  # "complete" protonation
  '''CRYST1   45.022   96.047  103.889  90.00  90.00  90.00 P 21 21 21
ATOM      1  N   CYS A 249      23.920  34.926  38.471  1.00 14.94           N
ATOM      2  CA  CYS A 249      24.465  34.523  37.177  1.00 15.84           C
ATOM      3  C   CYS A 249      23.430  33.613  36.520  1.00 17.71           C
ATOM      4  O   CYS A 249      22.826  32.772  37.192  1.00 17.96           O
ATOM      5  CB  CYS A 249      25.776  33.752  37.369  1.00 13.37           C
ATOM      6  SG  CYS A 249      26.490  33.062  35.853  1.00 16.65           S
ATOM         OXT CYS A 249      23.195  33.717  35.316  1.00 17.71           O
ATOM         H1  CYS A 249      24.558  34.610  39.229  0.00 14.94           H
ATOM         H2  CYS A 249      23.831  35.962  38.502  0.00 14.94           H
ATOM         H3  CYS A 249      22.984  34.493  38.603  0.00 14.94           H
ATOM      7  HA  CYS A 249      24.684  35.386  36.548  0.00 15.84           H
ATOM      8  HB2 CYS A 249      26.514  34.429  37.799  0.00 13.37           H
ATOM      9  HB3 CYS A 249      25.591  32.921  38.050  0.00 13.37           H
ATOM     10  N   CYS A 252      25.195  29.795  36.725  1.00 19.22           N
ATOM     11  CA  CYS A 252      25.505  29.132  37.992  1.00 21.00           C
ATOM     12  C   CYS A 252      24.806  29.696  39.234  1.00 19.26           C
ATOM     13  O   CYS A 252      24.844  29.082  40.307  1.00 17.05           O
ATOM     14  CB  CYS A 252      27.017  29.128  38.207  1.00 25.53           C
ATOM     15  SG  CYS A 252      27.548  28.157  39.617  1.00 43.08           S
ATOM         OXT CYS A 252      24.202  30.766  39.167  1.00 19.26           O
ATOM         H1  CYS A 252      26.069  30.178  36.311  0.00 19.22           H
ATOM         H2  CYS A 252      24.773  29.108  36.068  0.00 19.22           H
ATOM         H3  CYS A 252      24.523  30.570  36.895  0.00 19.22           H
ATOM     16  HA  CYS A 252      25.120  28.116  37.905  0.00 21.00           H
ATOM     17  HB2 CYS A 252      27.495  28.714  37.319  0.00 25.53           H
ATOM     18  HB3 CYS A 252      27.350  30.154  38.366  0.00 25.53           H
ATOM     19  HG  CYS A 252      26.545  28.009  40.431  0.00 43.08           H
ATOM     20  N   CYS A 269      31.466  37.140  36.550  1.00 15.31           N
ATOM     21  CA  CYS A 269      31.002  36.103  37.483  1.00 15.56           C
ATOM     22  C   CYS A 269      32.258  35.444  38.049  1.00 18.19           C
ATOM     23  O   CYS A 269      33.043  34.847  37.307  1.00 18.29           O
ATOM     24  CB  CYS A 269      30.165  35.030  36.783  1.00 14.60           C
ATOM     25  SG  CYS A 269      29.795  33.619  37.904  1.00 16.23           S
ATOM         OXT CYS A 269      32.495  35.505  39.255  1.00 18.19           O
ATOM         H1  CYS A 269      31.099  36.939  35.598  0.00 15.31           H
ATOM         H2  CYS A 269      31.121  38.069  36.865  0.00 15.31           H
ATOM         H3  CYS A 269      32.506  37.146  36.527  0.00 15.31           H
ATOM     26  HA  CYS A 269      30.367  36.539  38.254  0.00 15.56           H
ATOM     27  HB2 CYS A 269      29.221  35.466  36.458  0.00 14.60           H
ATOM     28  HB3 CYS A 269      30.716  34.649  35.923  0.00 14.60           H
ATOM     29  N   CYS A 272      32.216  31.333  37.125  1.00 18.59           N
ATOM     30  CA  CYS A 272      32.314  30.925  35.723  1.00 19.63           C
ATOM     31  C   CYS A 272      33.585  31.420  35.032  1.00 20.51           C
ATOM     32  O   CYS A 272      34.044  30.817  34.058  1.00 21.36           O
ATOM     33  CB  CYS A 272      31.099  31.420  34.922  1.00 18.23           C
ATOM     34  SG  CYS A 272      29.520  30.581  35.270  1.00 20.55           S
ATOM         OXT CYS A 272      34.159  32.427  35.447  1.00 20.51           O
ATOM         H1  CYS A 272      31.359  31.906  37.260  0.00 18.59           H
ATOM         H2  CYS A 272      32.167  30.488  37.729  0.00 18.59           H
ATOM         H3  CYS A 272      33.053  31.894  37.382  0.00 18.59           H
ATOM     35  HA  CYS A 272      32.340  29.835  35.719  0.00 19.63           H
ATOM     36  HB2 CYS A 272      30.956  32.479  35.139  0.00 18.23           H
ATOM     37  HB3 CYS A 272      31.311  31.282  33.862  0.00 18.23           H
TER
HETATM   38 ZN    ZN A 300      28.429  31.972  36.863  1.00 16.50          Zn
''',
  # "capping" example
  '''CRYST1   45.022   96.047  103.889  90.00  90.00  90.00 P 21 21 21
ATOM      1  N   CYS A 249      23.920  34.926  38.471  1.00 14.94           N
ATOM      2  CA  CYS A 249      24.465  34.523  37.177  1.00 15.84           C
ATOM      3  C   CYS A 249      23.430  33.613  36.520  1.00 17.71           C
ATOM      4  O   CYS A 249      22.826  32.772  37.192  1.00 17.96           O
ATOM      5  CB  CYS A 249      25.776  33.752  37.369  1.00 13.37           C
ATOM      6  SG  CYS A 249      26.490  33.062  35.853  1.00 16.65           S
ATOM         H1  CYS A 249      24.472  34.653  39.128  0.00 14.94           H
ATOM         H2  CYS A 249      23.843  35.822  38.498  0.00 14.94           H
ATOM      7  HA  CYS A 249      24.684  35.386  36.548  0.00 15.84           H
ATOM      8  HB2 CYS A 249      26.514  34.429  37.799  0.00 13.37           H
ATOM      9  HB3 CYS A 249      25.591  32.921  38.050  0.00 13.37           H
ATOM         HC  CYS A 249      23.228  33.684  35.543  0.00 17.71           H
ATOM     10  N   CYS A 252      25.195  29.795  36.725  1.00 19.22           N
ATOM     11  CA  CYS A 252      25.505  29.132  37.992  1.00 21.00           C
ATOM     12  C   CYS A 252      24.806  29.696  39.234  1.00 19.26           C
ATOM     13  O   CYS A 252      24.844  29.082  40.307  1.00 17.05           O
ATOM     14  CB  CYS A 252      27.017  29.128  38.207  1.00 25.53           C
ATOM     15  SG  CYS A 252      27.548  28.157  39.617  1.00 43.08           S
ATOM         H1  CYS A 252      25.951  30.127  36.367  0.00 19.22           H
ATOM         H2  CYS A 252      24.830  29.201  36.156  0.00 19.22           H
ATOM     16  HA  CYS A 252      25.120  28.116  37.905  0.00 21.00           H
ATOM     17  HB2 CYS A 252      27.495  28.714  37.319  0.00 25.53           H
ATOM     18  HB3 CYS A 252      27.350  30.154  38.366  0.00 25.53           H
ATOM     19  HG  CYS A 252      26.545  28.009  40.431  0.00 43.08           H
ATOM         HC  CYS A 252      24.311  30.564  39.197  0.00 19.26           H
ATOM     20  N   CYS A 269      31.466  37.140  36.550  1.00 15.31           N
ATOM     21  CA  CYS A 269      31.002  36.103  37.483  1.00 15.56           C
ATOM     22  C   CYS A 269      32.258  35.444  38.049  1.00 18.19           C
ATOM     23  O   CYS A 269      33.043  34.847  37.307  1.00 18.29           O
ATOM     24  CB  CYS A 269      30.165  35.030  36.783  1.00 14.60           C
ATOM     25  SG  CYS A 269      29.795  33.619  37.904  1.00 16.23           S
ATOM         H1  CYS A 269      31.149  36.966  35.726  0.00 15.31           H
ATOM         H2  CYS A 269      31.168  37.944  36.822  0.00 15.31           H
ATOM     26  HA  CYS A 269      30.367  36.539  38.254  0.00 15.56           H
ATOM     27  HB2 CYS A 269      29.221  35.466  36.458  0.00 14.60           H
ATOM     28  HB3 CYS A 269      30.716  34.649  35.923  0.00 14.60           H
ATOM         HC  CYS A 269      32.465  35.485  39.026  0.00 18.19           H
ATOM     29  N   CYS A 272      32.216  31.333  37.125  1.00 18.59           N
ATOM     30  CA  CYS A 272      32.314  30.925  35.723  1.00 19.63           C
ATOM     31  C   CYS A 272      33.585  31.420  35.032  1.00 20.51           C
ATOM     32  O   CYS A 272      34.044  30.817  34.058  1.00 21.36           O
ATOM     33  CB  CYS A 272      31.099  31.420  34.922  1.00 18.23           C
ATOM     34  SG  CYS A 272      29.520  30.581  35.270  1.00 20.55           S
ATOM         H1  CYS A 272      31.474  31.829  37.242  0.00 18.59           H
ATOM         H2  CYS A 272      32.173  30.602  37.648  0.00 18.59           H
ATOM     35  HA  CYS A 272      32.340  29.835  35.719  0.00 19.63           H
ATOM     36  HB2 CYS A 272      30.956  32.479  35.139  0.00 18.23           H
ATOM     37  HB3 CYS A 272      31.311  31.282  33.862  0.00 18.23           H
ATOM         HC  CYS A 272      34.063  32.236  35.356  0.00 20.51           H
TER
HETATM   38 ZN    ZN A 300      28.429  31.972  36.863  1.00 16.50          Zn
''',
  # "capping" @ end
  '''CRYST1   45.022   96.047  103.889  90.00  90.00  90.00 P 21 21 21
ATOM      1  N   CYS A 249      23.920  34.926  38.471  1.00 14.94           N
ATOM      2  CA  CYS A 249      24.465  34.523  37.177  1.00 15.84           C
ATOM      3  C   CYS A 249      23.430  33.613  36.520  1.00 17.71           C
ATOM      4  O   CYS A 249      22.826  32.772  37.192  1.00 17.96           O
ATOM      5  CB  CYS A 249      25.776  33.752  37.369  1.00 13.37           C
ATOM      6  SG  CYS A 249      26.490  33.062  35.853  1.00 16.65           S
ATOM         H1  CYS A 249      24.472  34.653  39.128  0.00 14.94           H
ATOM      7  HA  CYS A 249      24.684  35.386  36.548  0.00 15.84           H
ATOM      8  HB2 CYS A 249      26.514  34.429  37.799  0.00 13.37           H
ATOM      9  HB3 CYS A 249      25.591  32.921  38.050  0.00 13.37           H
ATOM     10  N   CYS A 252      25.195  29.795  36.725  1.00 19.22           N
ATOM     11  CA  CYS A 252      25.505  29.132  37.992  1.00 21.00           C
ATOM     12  C   CYS A 252      24.806  29.696  39.234  1.00 19.26           C
ATOM     13  O   CYS A 252      24.844  29.082  40.307  1.00 17.05           O
ATOM     14  CB  CYS A 252      27.017  29.128  38.207  1.00 25.53           C
ATOM     15  SG  CYS A 252      27.548  28.157  39.617  1.00 43.08           S
ATOM         H1  CYS A 252      25.951  30.127  36.367  0.00 19.22           H
ATOM     16  HA  CYS A 252      25.120  28.116  37.905  0.00 21.00           H
ATOM     17  HB2 CYS A 252      27.495  28.714  37.319  0.00 25.53           H
ATOM     18  HB3 CYS A 252      27.350  30.154  38.366  0.00 25.53           H
ATOM     19  HG  CYS A 252      26.545  28.009  40.431  0.00 43.08           H
ATOM     20  N   CYS A 269      31.466  37.140  36.550  1.00 15.31           N
ATOM     21  CA  CYS A 269      31.002  36.103  37.483  1.00 15.56           C
ATOM     22  C   CYS A 269      32.258  35.444  38.049  1.00 18.19           C
ATOM     23  O   CYS A 269      33.043  34.847  37.307  1.00 18.29           O
ATOM     24  CB  CYS A 269      30.165  35.030  36.783  1.00 14.60           C
ATOM     25  SG  CYS A 269      29.795  33.619  37.904  1.00 16.23           S
ATOM         H1  CYS A 269      31.149  36.966  35.726  0.00 15.31           H
ATOM     26  HA  CYS A 269      30.367  36.539  38.254  0.00 15.56           H
ATOM     27  HB2 CYS A 269      29.221  35.466  36.458  0.00 14.60           H
ATOM     28  HB3 CYS A 269      30.716  34.649  35.923  0.00 14.60           H
ATOM     29  N   CYS A 272      32.216  31.333  37.125  1.00 18.59           N
ATOM     30  CA  CYS A 272      32.314  30.925  35.723  1.00 19.63           C
ATOM     31  C   CYS A 272      33.585  31.420  35.032  1.00 20.51           C
ATOM     32  O   CYS A 272      34.044  30.817  34.058  1.00 21.36           O
ATOM     33  CB  CYS A 272      31.099  31.420  34.922  1.00 18.23           C
ATOM     34  SG  CYS A 272      29.520  30.581  35.270  1.00 20.55           S
ATOM         H1  CYS A 272      31.474  31.829  37.242  0.00 18.59           H
ATOM     35  HA  CYS A 272      32.340  29.835  35.719  0.00 19.63           H
ATOM     36  HB2 CYS A 272      30.956  32.479  35.139  0.00 18.23           H
ATOM     37  HB3 CYS A 272      31.311  31.282  33.862  0.00 18.23           H
TER
HETATM   38 ZN    ZN A 300      28.429  31.972  36.863  1.00 16.50          Zn
ATOM         H2  CYS A 249      23.843  35.822  38.498  0.00 14.94           H
ATOM         HC  CYS A 249      23.239  33.697  35.542  0.00 17.71           H
ATOM         H2  CYS A 252      24.830  29.201  36.156  0.00 19.22           H
ATOM         HC  CYS A 252      24.315  30.566  39.180  0.00 19.26           H
ATOM         H2  CYS A 269      31.168  37.944  36.822  0.00 15.31           H
ATOM         HC  CYS A 269      32.450  35.494  39.029  0.00 18.19           H
ATOM         H2  CYS A 272      32.173  30.602  37.648  0.00 18.59           H
ATOM         HC  CYS A 272      34.051  32.238  35.369  0.00 20.51           H
''',
]

def main():
  ptr = 0
  filename = 'tst_electrons_01_%02d.pdb' % ptr
  with open(filename, 'w') as f:
    f.write(pdbs[ptr])
  #
  args = [filename]
  try: results=run_program(program_class=charges.Program, args=args)
  except Exception as e:
    # print('"%s" %s' % (str(e), type(e)))
    assert str(e).strip()=='Element has strange number of electrons  N  : 2'
  #
  for ptr in range(1,4):
    filename = 'tst_electrons_01_%02d.pdb' % ptr
    with open(filename, 'w') as f:
      f.write(pdbs[ptr])
    #
    args = [filename]
    results=run_program(program_class=charges.Program, args=args)
    assert results.total_charge==-1
    #
    args = [filename, 'link_metals=True']
    results=run_program(program_class=charges.Program, args=args)
    print(results)
    assert results.total_charge==-1

if __name__ == '__main__':
  main()
