import os, sys
from libtbx import easy_run
from phenix_regression.refinement.model_cif_output.tst_cif_dict_compliance import pdb_1al1
from mmtbx.geometry_restraints.quantum_restraints_manager import select_and_reindex
from mmtbx.geometry_restraints.quantum_restraints_manager import add_additional_hydrogen_atoms_to_model
from mmtbx.geometry_restraints.quantum_restraints_manager import update_restraints

from iotbx.data_manager import DataManager

single_residue_model_atoms = [
  None, # [' C  ', ' O  ', ' CH3'],
  [' N  ', ' CA ', ' C  ', ' O  ', ' CB ', ' CG ', ' CD ', ' OE1', ' OE2', ' H1 ', ' H2 ', ' H3 ', ' OXT'],
  [' N  ', ' CA ', ' C  ', ' O  ', ' CB ', ' CG ', ' CD1', ' CD2', ' H1 ', ' H2 ', ' H3 ', ' OXT'],
  None,
  None,
  None,
  None,
  None,
  None,
  None,
  None,
  [' N  ', ' CA ', ' C  ', ' O  ', ' CB ', ' H1 ', ' H2 ', ' H3 ', ' OXT'],
  [' N  ', ' CA ', ' C  ', ' O  ', ' OXT', ' H1 ', ' H2 ', ' H3 '],
  [' O1 ', ' O2 ', ' O3 ', ' O4 ', ' S  '],
  ]
single_residue_model_capping_atoms = [
  None,
  [' N  ', ' CA ', ' C  ', ' O  ', ' CB ', ' CG ', ' CD ', ' OE1', ' OE2', ' H1 ', ' H2 ', ' HC '],
  [' N  ', ' CA ', ' C  ', ' O  ', ' CB ', ' CG ', ' CD1', ' CD2', ' H1 ', ' H2 ', ' HC '],
  None,
  None,
  None,
  None,
  None,
  None,
  None,
  None,
  [' N  ', ' CA ', ' C  ', ' O  ', ' CB ', ' H1 ', ' H2 ', ' HC '],
  [' N  ', ' CA ', ' C  ', ' O  ', ' OXT', ' H1 ', ' H2 '],
  [' O1 ', ' O2 ', ' O3 ', ' O4 ', ' S  '],
  ]

test_finder = [single_residue_model_atoms, single_residue_model_capping_atoms]

def test_tester(long_list, short_list, zero_subset=False):
  if long_list is None: return True
  if zero_subset:
    d = set(short_list).intersection(set(long_list))
    assert not d, 'intersection %s %s %s not zero' % (d, short_list, long_list)
  else:
    d = set(short_list).difference(set(long_list))
    assert not d, 'difference %s %s %s not zero' % (d, short_list, long_list)

def exercise_00(model):
  for resid in range(14):
    if resid in [3,4,5,6,7,8,9,10]: continue
    for use_capping_hydrogens in range(2):
      selection_string = 'resid %s' % resid
      print('_'*80)
      print('selection_string', selection_string, 'use_capping_hydrogens', use_capping_hydrogens)
      single_residue_model = select_and_reindex(
        model,
        selection_string)
      add_additional_hydrogen_atoms_to_model( single_residue_model,
                                              use_capping_hydrogens=use_capping_hydrogens,
                                              retain_original_hydrogens=False,
                                              )
      atom_names = []
      f=open('add_hydrogen_atoms_to_model_%02d_%d.pdb' % (resid, use_capping_hydrogens), 'w')
      for atom in single_residue_model.get_atoms():
        atom_names.append(atom.name)
        # print(atom.format_atom_record())
        f.write('%s\n' % atom.format_atom_record())
      del f
      tester = test_finder[use_capping_hydrogens]
      if tester[resid] is None: continue
      test_tester(atom_names, tester[resid])

def exercise_01(model):
  for resid in range(14):
    for use_capping_hydrogens in range(2):
      selection_string = 'residues_within(3, resid %s)' % resid
      print('_'*80)
      print(selection_string, use_capping_hydrogens)
      single_residue_model = select_and_reindex(
        model,
        selection_string)
      single_residue_model.get_hierarchy().show()
      add_additional_hydrogen_atoms_to_model( single_residue_model,
                                              use_capping_hydrogens=use_capping_hydrogens,
                                              )
      atom_names = []
      for atom in single_residue_model.get_atoms():
        atom_names.append(atom.name)
      print(atom_names)
      assert 0

def get_model_for_testing(file_name):
  dm = DataManager()
  dm.process_model_file(file_name)
  model = dm.get_model(file_name)
  model.process(make_restraints=True)
  return model

def main():
  print('test QM restraints - add terminal atoms')
  file_name = 'tst_add_hydrogens_to_fragments_1aL1.pdb'
  f=open(file_name, 'w')
  f.write(pdb_1al1)
  del f
  new = file_name.replace('.pdb', '_h.pdb')
  cmd = 'phenix.reduce %s > %s' % (file_name, new)
  rc = easy_run.go(cmd)
  assert rc.return_code==0
  file_name=new
  #
  model = get_model_for_testing(file_name)
  #
  exercise_00(model)
  # exercise_01(model)

if __name__ == '__main__':
  main()
