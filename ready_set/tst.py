from __future__ import print_function
import os, sys
from libtbx import easy_run
import libtbx.load_env

try:
  phenix_regression = libtbx.env.dist_path("phenix_regression")
except:
  phenix_regression = None

def run(only_i=2):
  try: only_i=int(only_i)
  except: only_i=None
  if phenix_regression is None:
    print("no phenix_regression")
    return
  filenames = os.listdir(os.path.join(phenix_regression, "ready_set"))
  for i, filename in enumerate(sorted(filenames)):
    print(i,filename)
    if only_i is not None and i+1!=only_i: continue
    if not filename.endswith(".pdb"): continue
    if filename in [
      "asp.pdb", # HD2 is not in monomer lib
      '5bu.pdb', # 5BU has an ? in the xyz in the Comp. Chem.
      ]: continue
    filename = os.path.join(phenix_regression, "ready_set", filename)
    print("""
%s
  Testing ReadySet on %s
%s
""" % ('-'*80,
       filename,
       '-'*80,
       ))
    preamble = os.path.splitext(os.path.basename(filename))[0]
    cmd = "phenix.ready_set %s" % filename
    print(cmd)
    rc = easy_run.fully_buffered(cmd) #.raise_if_errors()
    for line in rc.stdout_lines:
      print(line)
      if line.find("eLBOW has stopped")>-1:
        assert 0

    cmd = "phenix.geometry_minimization %s.updated.pdb" % (preamble)
    if os.path.exists("%s.ligands.cif" % preamble):
      cmd += " %s.ligands.cif" % (preamble)

    print(cmd)
    rc = easy_run.fully_buffered(cmd).raise_if_errors()
    for line in rc.stdout_lines:
      print(line)
      if line.find("Detailed timing")>-1:
        break
    else:
      assert 0

if __name__=="__main__":
  run(*tuple(sys.argv[1:]))

