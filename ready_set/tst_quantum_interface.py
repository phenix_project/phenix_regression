import os, sys
from libtbx import easy_run

from iotbx.data_manager import DataManager

from mmtbx.geometry_restraints.quantum_interface import is_quantum_interface_active

def introduction(model_filename, qi_phil_filename=None, restraints_filename=None):
  print('Quantum Interface Checker')
  dm = DataManager(datatypes=['model', 'phil', 'restraint'])
  if restraints_filename:
    dm.process_restraint_file(restraints_filename)
  if qi_phil_filename:
    dm.process_phil_file(qi_phil_filename)
  dm.process_model_file(model_filename)
  model = dm.get_model(model_filename)
  model.process(make_restraints=True)

  params = model.get_current_pdb_interpretation_params()

  print('Checking QI is active')
  if is_quantum_interface_active(params, verbose=True):
    assert 0
  else:
    print(dir(params))

def exercise_00():
  from phenix_regression import ligand_pipeline
  prefix="tst_quantum_interface_00"
  ligand_pipeline.make_inputs(prefix=prefix, use_chained_ligand=True)
  params = '''
refinement.qi.qm_restraints
{
  selection = resname ACT
  cleanup = Auto
  buffer = 3.
  write_files = *pdb_core *pdb_buffer
  package
  {
    program = *mopac
    charge = 0
    multiplicity = Auto
    method = Auto
    basis_set = Auto
  }
}
  '''
  f=open('%s.phil' % prefix, 'w')
  f.write(params)
  del f
  cmd = 'phenix.reduce %(prefix)s_in.pdb > %(prefix)s_in_h.pdb' % locals()
  print('\n ~> %s\n' % cmd)
  rc = easy_run.go(cmd)
  assert rc.return_code==0
  cmd = 'phenix.refine %(prefix)s_in.pdb %(prefix)s_in.mtz' % locals()
  cmd += ' overwrite=True'
  cmd += ' refinement.main.number_of_macro_cycles=1'
  cmd += ' %(prefix)s.phil output.serial=2' % locals()
  print('\n ~> %s\n' % cmd)
  os.environ['PHENIX_QM_TEST']='True'
  rc = easy_run.go(cmd)
  assert rc.return_code != 0, rc.return_code
  cmd = 'phenix.refine %(prefix)s_in_h.pdb %(prefix)s_in.mtz' % locals()
  cmd += ' overwrite=True'
  cmd += ' refinement.main.number_of_macro_cycles=1'
  cmd += ' %(prefix)s.phil output.serial=3' % locals()
  print('\n ~> %s\n' % cmd)
  rc = easy_run.go(cmd)
  # rc.show_stdout()
  assert rc.return_code == 0, rc.return_code

def main():
  exercise_00()

if __name__ == '__main__':
  main(*tuple(sys.argv[1:]))
