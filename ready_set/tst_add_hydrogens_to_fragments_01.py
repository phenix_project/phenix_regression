import os, sys
from mmtbx.geometry_restraints.quantum_restraints_manager import add_additional_hydrogen_atoms_to_model

from tst_add_hydrogens_to_fragments import get_model_for_testing, test_tester

pdb_str = '''
CRYST1   69.873   71.696  120.736  90.00  90.00  90.00 P 21 21 21
SCALE1      0.014312  0.000000  0.000000        0.00000
SCALE2      0.000000  0.013948  0.000000        0.00000
SCALE3      0.000000  0.000000  0.008283        0.00000
ATOM      1  N   CYS W   0      20.663  29.193 -38.814  1.00 11.26           N
ATOM      2  CA  CYS W   0      21.799  29.059 -37.916  1.00 10.56           C
ATOM      3  C   CYS W   0      23.068  28.775 -38.699  1.00 11.18           C
ATOM      4  O   CYS W   0      23.053  28.093 -39.724  1.00 10.81           O
ATOM      5  CB  CYS W   0      21.598  27.851 -37.004  1.00  9.35           C
ATOM      6  SG  CYS W   0      20.153  27.941 -35.989  1.00 14.74           S
ATOM      7  N   LYS W   2      26.671  27.556 -37.018  1.00 13.10           N
ATOM      8  CA  LYS W   2      27.479  27.245 -35.835  1.00 15.84           C
ATOM      9  C   LYS W   2      28.394  26.064 -36.128  1.00 14.04           C
ATOM     10  O   LYS W   2      27.924  25.027 -36.678  1.00 14.05           O
ATOM     11  CB  LYS W   2      26.565  26.937 -34.640  1.00 11.96           C
ATOM     12  CG  LYS W   2      25.577  28.094 -34.319  1.00 12.87           C
ATOM     13  CD  LYS W   2      24.634  27.641 -33.213  1.00 17.44           C
ATOM     14  CE  LYS W   2      23.617  28.719 -32.809  1.00 13.32           C
ATOM     15  NZ  LYS W   2      22.583  28.813 -33.871  1.00 25.21           N
TER
HETATM   16  C1  CHX W 101      20.190  28.826 -32.556  1.00 27.31           C
HETATM   17  C2  CHX W 101      18.722  28.775 -32.978  1.00 31.27           C
HETATM   18  C3  CHX W 101      18.316  29.955 -33.845  1.00 40.64           C
HETATM   19  C4  CHX W 101      19.302  30.261 -34.967  1.00 24.46           C
HETATM   20  C5  CHX W 101      20.557  29.381 -35.046  1.00 15.31           C
HETATM   21  C6  CHX W 101      21.170  28.958 -33.715  1.00 27.48           C
END
'''

def main():
  preamble = os.path.basename(sys.argv[0]).replace('.py','')
  print('test QM restraints - add terminal atoms %s' % preamble)
  file_name = '%s.pdb' % preamble
  f=open(file_name, 'w')
  f.write(pdb_str)
  del f
  #
  model = get_model_for_testing(file_name)
  #
  use_capping_hydrogens=True
  add_additional_hydrogen_atoms_to_model( model,
                                          use_capping_hydrogens=use_capping_hydrogens,
                                          retain_original_hydrogens=False,
                                        )
  atom_names=[]
  for atom in model.get_atoms():
    atom_names.append(atom.name)
  test_tester(atom_names, [' HG '], zero_subset=True)

if __name__ == '__main__':
  main()
