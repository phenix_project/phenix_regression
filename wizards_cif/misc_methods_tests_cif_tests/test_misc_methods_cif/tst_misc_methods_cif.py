from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os,sys
  print ("Test script for test_misc_methods_cif")
  print ("")
  if (sys.platform == "win32") :
     print ("Windows installation...skipping this test...OK")
     return
  print(""
  )
  log_file=os.path.join(os.getcwd(),"test_misc_methods_cif.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  from phenix.command_line.get_cc_mtz_pdb  import get_cc_mtz_pdb
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes
  args=split_except_quotes('''map_coeffs.mtz partial.cif''')
  get_cc_mtz_pdb(args=args,out=log)


  from phenix.command_line.find_all_ligands  import find_all_ligands
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes
  args=split_except_quotes('''data=perfect.mtz model=partial.cif ligand_list=side.cif resolution=3.0 n_group_search=1 ligand_cc_min=0.5 quick=True number_of_ligands=1 temp_dir=tmpdir debug=True compare_file=side.cif input_labels="FP PHIC FOM"''')
  find_all_ligands(args=args,out=log)


  from phenix.command_line.get_cc_mtz_mtz  import get_cc_mtz_mtz
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes
  args=split_except_quotes('''map_coeffs.mtz map_coeffs_1.mtz''')
  get_cc_mtz_mtz(args=args,out=log)


  from phenix.command_line.map_to_object  import map_to_object
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes
  args=split_except_quotes('''fixed_pdb=coords.cif moving_pdb=ha_perfect.cif output_pdb=out.pdb debug=True''')
  map_to_object(args=args,out=log)

  cmds='''from phenix.autosol.utils import remove_zero_phase_columns
remove_zero_phase_columns(file_name='missing.mtz',output_file_name='out.mtz')
remove_zero_phase_columns(file_name='hl.mtz',output_file_name='out.mtz')
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print (open('phenix.python.log').read(), file = log)

  log.close()


  print ()
  print ('Done with test...running checks on output')
  print ()
  lines=open(log_file).readlines()
  from phenix_regression.wizards_cif.check_results import check_results
  check_results(test='test_misc_methods_cif',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print ()
  print ('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards_cif.run_wizard_test import set_up_wizard_test
    args=['misc_methods_tests_cif_tests','test_misc_methods_cif']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
