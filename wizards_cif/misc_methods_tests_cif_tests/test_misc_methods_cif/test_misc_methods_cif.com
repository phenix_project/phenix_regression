#!/bin/csh  -f
#$ -cwd
#
setenv LIBTBX_FULL_TESTING

echo "METHOD get_cc_mtz_pdb"
phenix.get_cc_mtz_pdb map_coeffs.mtz partial.cif 


echo "METHOD find_all_ligands"

phenix.find_all_ligands data=perfect.mtz model=partial.cif ligand_list=side.cif resolution=3.0 n_group_search=1 ligand_cc_min=0.5 quick=True number_of_ligands=1 temp_dir=tmpdir  debug=True compare_file=side.cif input_labels="FP PHIC FOM" 

phenix.get_cc_mtz_mtz  map_coeffs.mtz map_coeffs_1.mtz 


echo "METHOD map_to_object"
phenix.map_to_object fixed_pdb=coords.cif moving_pdb=ha_perfect.cif output_pdb=out.pdb  debug=True 

echo "METHOD remove_zero_phase_columns"
phenix.python<<EOD
from phenix.autosol.utils import remove_zero_phase_columns
remove_zero_phase_columns(file_name='missing.mtz',output_file_name='out.mtz')
remove_zero_phase_columns(file_name='hl.mtz',output_file_name='out.mtz')
EOD

echo "Done with misc_methods"
echo "OK"


