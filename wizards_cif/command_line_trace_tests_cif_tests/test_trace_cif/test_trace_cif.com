#!/bin/csh -ef
#$ -cwd
setenv LIBTBX_FULL_TESTING

phenix.python<<EOD
from solve_resolve.resolve_python.run_local_trace import run_demo
args=" overall_best_denmod_map_coeffs.mtz full.cif short.cif verbose".split()
run_demo(args)
EOD

if ( $status )then
  echo "FAILED"
  exit 1
endif



