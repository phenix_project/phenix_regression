from __future__ import print_function

done_list=[   # just use tst_xxx.py as is for these now (already edited)
   ['command_line_map_to_model_tests','test_segment'] ,
   ['command_line_map_to_model_tests','test_map_to_model_quick'],
   ['command_line_build_tests','test_build_rna_helices'],
   ['command_line_tests','test_replace_side_chains'],
   ['command_line_resolve_memory_tests','test_density_modification'],
   ['command_line_resolve_memory_tests','test_ncs_average'],
   ['ncs_in_phenix_refine_tests','test_ncs_in_phenix_refine'],
   ['command_line_tests','test_combine_models'],
   ['command_line_rosetta_quick_tests','test_autobuild_double'],
   ['command_line_loops_tests','test_fit_loops_offset'],
  ]

import os
for test_group,test in done_list:
  file_name=os.path.join(test_group,test,"tst_%s.py" %(test[5:]))
  old_text=open(file_name).read()
  f=open(file_name,'w')
  for line in old_text.splitlines():
    if line.startswith("if __name__=="):
      break
    line=line.rstrip()
    print(line, file=f)

  # now finish off with new text

  print("""

if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import run_wizard_test
    args=['%s','%s']
    run_wizard_test(args)

""" %(test_group,test), file=f)
  f.close()
