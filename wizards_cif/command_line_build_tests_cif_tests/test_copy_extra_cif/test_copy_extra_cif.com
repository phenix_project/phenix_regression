#!/bin/csh -ef
#$ -cwd
phenix.python <<EOD
from phenix.autosol.copy_extra import copy_extra_to_pdb
copy_extra_to_pdb(pdb1="resolve.cif",pdb2="se.cif",pdb3="out.cif",
        copy_coords=True)
EOD
if ( $status )then
  echo "FAILED"
  exit 1
endif
diff expected.cif out.cif


