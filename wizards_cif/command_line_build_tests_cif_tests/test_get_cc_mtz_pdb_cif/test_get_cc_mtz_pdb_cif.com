#!/bin/csh -ef
#$ -cwd


phenix.get_cc_mtz_pdb veryshort.cif veryshort.ccp4
if ( $status )then
  echo "FAILED"
  exit 1
endif
cat cc.log
phenix.map_model_cc veryshort.ccp4 veryshort_offset.cif resolution=3


phenix.get_cc_mtz_pdb bad.cif map.mtz 
if ( $status )then
  echo "FAILED"
  exit 1
endif
cat cc.log

phenix.get_cc_mtz_pdb bad.cif map.mtz fix_xyz=True
if ( $status )then
  echo "FAILED"
  exit 1
endif
cat cc.log

phenix.get_cc_mtz_pdb bad.cif map.mtz any_offset=true
if ( $status )then
  echo "FAILED"
  exit 1
endif
cat cc.log

phenix.get_cc_mtz_pdb bad.cif map.mtz atom_selection="resid 45:52"
if ( $status )then
  echo "FAILED"
  exit 1
endif
cat cc.log

phenix.get_cc_mtz_pdb bad.cif map.mtz atom_selection=all
if ( $status )then
  echo "FAILED"
  exit 1
endif
cat cc.log

phenix.get_cc_mtz_pdb single.cif map.mtz fix_xyz=true
if ( $status )then
  echo "FAILED"
  exit 1
endif
cat cc.log




