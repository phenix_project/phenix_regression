from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print ("Test script for test_get_cc_mtz_pdb_cif")
  print ("")
  log_file=os.path.join(os.getcwd(),"test_get_cc_mtz_pdb_cif.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  from phenix.command_line.get_cc_mtz_pdb  import get_cc_mtz_pdb
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes
  args=split_except_quotes('''veryshort.cif veryshort.ccp4''')
  get_cc_mtz_pdb(args=args,out=log)
  print ( open('cc.log').read(), file = log)


  from phenix.programs  import map_model_cc as run

  from iotbx.cli_parser import run_program
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes
  args=split_except_quotes('''veryshort.ccp4 veryshort_offset.cif resolution=3''')
  run_program(program_class=run.Program,args=args,logger=log)


  from phenix.command_line.get_cc_mtz_pdb  import get_cc_mtz_pdb
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes
  args=split_except_quotes('''bad.cif map.mtz''')
  get_cc_mtz_pdb(args=args,out=log)
  print ( open('cc.log').read(), file = log)


  from phenix.command_line.get_cc_mtz_pdb  import get_cc_mtz_pdb
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes
  args=split_except_quotes('''bad.cif map.mtz fix_xyz=True''')
  get_cc_mtz_pdb(args=args,out=log)
  print ( open('cc.log').read(), file = log)


  from phenix.command_line.get_cc_mtz_pdb  import get_cc_mtz_pdb
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes
  args=split_except_quotes('''bad.cif map.mtz any_offset=true''')
  get_cc_mtz_pdb(args=args,out=log)
  print ( open('cc.log').read(), file = log)


  from phenix.command_line.get_cc_mtz_pdb  import get_cc_mtz_pdb
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes
  args=split_except_quotes('''bad.cif map.mtz atom_selection="resid 45:52"''')
  get_cc_mtz_pdb(args=args,out=log)
  print ( open('cc.log').read(), file = log)


  from phenix.command_line.get_cc_mtz_pdb  import get_cc_mtz_pdb
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes
  args=split_except_quotes('''bad.cif map.mtz atom_selection=all''')
  get_cc_mtz_pdb(args=args,out=log)
  print ( open('cc.log').read(), file = log)


  from phenix.command_line.get_cc_mtz_pdb  import get_cc_mtz_pdb
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes
  args=split_except_quotes('''single.cif map.mtz fix_xyz=true''')
  get_cc_mtz_pdb(args=args,out=log)
  print ( open('cc.log').read(), file = log)

  log.close()


  print ()
  print ('Done with test...running checks on output')
  print ()
  lines=open(log_file).readlines()
  from phenix_regression.wizards_cif.check_results import check_results
  check_results(test='test_get_cc_mtz_pdb_cif',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print ()
  print ('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards_cif.run_wizard_test import set_up_wizard_test
    args=['command_line_build_tests_cif_tests','test_get_cc_mtz_pdb_cif']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
