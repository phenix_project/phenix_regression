from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print ("Test script for test_map_correlations_get_cc_mtz_pdb_cif")
  print ("")

  os.environ['PHENIX_OVERWRITE_ALL'] = '1'

  log = sys.stdout
  from iotbx.cli_parser import run_program
  from phenix.programs import map_correlations

  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes
  args=split_except_quotes('''veryshort.cif veryshort.ccp4 translational_offsets="Allowed_by_symmetry" prefix="veryshort" ''')
  run_program(map_correlations.Program, args = args, logger = log)

  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes
  args=split_except_quotes('''translational_offsets="Allowed_by_symmetry" veryshort.ccp4 veryshort_offset.cif resolution=3''')
  run_program(map_correlations.Program,args=args,logger=log)


  args=split_except_quotes('''translational_offsets="Allowed_by_symmetry" bad.cif map.mtz''')
  run_program(map_correlations.Program, args = args, logger = log)


  args=split_except_quotes('''bad.cif map.mtz ''')
  run_program(map_correlations.Program, args = args, logger = log)


  args=split_except_quotes('''translational_offsets="Any" bad.cif map.mtz ''')
  run_program(map_correlations.Program, args = args, logger = log)


  args=split_except_quotes('''translational_offsets="Allowed_by_symmetry" single.cif map.mtz ''')
  run_program(map_correlations.Program, args = args, logger = log)


  print ()
  print ('Done with test...running checks on output')
  print ()
  print ('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards_cif.run_wizard_test import set_up_wizard_test
    args=['command_line_build_tests_cif_tests','test_map_correlations_get_cc_mtz_pdb_cif']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
