from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os,sys
  print ("Test script for test_build_one_model_cif")
  print ("")
  log_file=os.path.join(os.getcwd(),"test_build_one_model_cif.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  local_log_file='test_build_one_model_cif.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.build_one_model  import build_one_model
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''quick=True sequence_autosol.dat macro_cycles=1 all.cif cycle_2.mtz labin_map_coeffs="FP=FWT PHIB=PHWT " free_in=working_data_file.mtz debug=true''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  build_one_model(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)


  for file_name in ['temp_dir/refine_build.cif']:
    for line in open(file_name).readlines():
      if line.lower().find("cc")>-1:
        print (line, file = log)

  log.close()


  print ()
  print ('Done with test...running checks on output')
  print ()
  lines=open(log_file).readlines()
  from phenix_regression.wizards_cif.check_results import check_results
  check_results(test='test_build_one_model_cif',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print ()
  print ('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards_cif.run_wizard_test import set_up_wizard_test
    args=['command_line_build_tests_cif_tests','test_build_one_model_cif']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
