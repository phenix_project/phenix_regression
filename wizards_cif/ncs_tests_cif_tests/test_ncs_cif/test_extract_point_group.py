from __future__ import print_function

from mmtbx.ncs.ncs import ncs

file_name='mixed_ncs.dat'
ncs_object=ncs()
ncs_object.read_ncs(file_name,source_info=file_name)
ncs_object.display_all()
print("original symmetry is point-group: ",ncs_object.is_point_group_symmetry())
new_ncs_object=ncs_object.deep_copy(extract_point_group_symmetry=True)
new_ncs_object.display_all()
print("extracted symmetry is point-group:", new_ncs_object.is_point_group_symmetry())
if new_ncs_object.is_point_group_symmetry():
  print("PG symmetry 1")
  print("OK")
else:
  print("Error PG symmetry not found")

