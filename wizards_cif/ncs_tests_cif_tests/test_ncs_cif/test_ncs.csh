#!/bin/csh  -f
#$ -cwd
#
setenv LIBTBX_FULL_TESTING
set phenix_dist="`libtbx.show_dist_paths phenix`"
set phenix_regression="`libtbx.find_in_repositories phenix_regression`"
# avoid copying wizards/CVS directory
find "$phenix_regression"/wizards -maxdepth 1 -type f -exec cp {} . \;
# special case for mtz:
echo "unpacking MM-all.tgz"
tar xzf MM-all.tgz
#
#
if ($#argv == 0) then
  set find_ncs="simple_ncs_from_pdb"
else
  if ($argv[1] == "--help") then
    echo "test_ncs. Use: test_ncs.csh [simple_ncs_from_pdb | find_ncs]"
    exit 0
  endif
  set find_ncs="$argv[1]"
endif

echo " "
echo "----------------------------------------------------- "
echo "find_ncs :  $find_ncs"
if (! -f "$phenix_dist"/phenix/command_line/${find_ncs}.py)then
 echo "Sorry, the command $find_ncs is not known"
 exit 1
endif
if ( ${find_ncs} == "simple_ncs_from_pdb") then
 set args = "pdb_in = anb.pdb write_spec_files=True"
else
 set args = "MM-all.pdb MM-all.mtz "
endif
#
#
if (-f anb_simple_ncs_from_pdb.ncs) rm -f anb_simple_ncs_from_pdb.ncs
if (-f anb_simple_resolve_from_pdb.resolve) rm -f anb_simple_resolve_from_pdb.resolve
#
if ( ${find_ncs} == "simple_ncs_from_pdb") then
 phenix.python "$phenix_dist"/phenix/command_line/${find_ncs}.py $args >test_ncs.log
else
 phenix.python "$phenix_dist"/phenix/command_line/${find_ncs}.py $args labin="FP=FP PHIB=PHIC ">test_ncs.log
endif
@ status_sav = $status
#
if ( ${find_ncs} != "simple_ncs_from_pdb")then
  echo "Differences in test_ncs.log from standard: \
      `diff test_ncs.log test_ncs_find_ncs.standard `"
  goto finish
endif

echo "Differences in test_ncs.log from standard: \
      `diff test_ncs.log test_ncs.standard `"
#

if ( -f anb_simple_ncs_from_pdb.resolve) then
 echo "Differences in simple_ncs_from_pdb.resolve from standard: \
      `diff anb_simple_ncs_from_pdb.resolve find_ncs_resolve.standard `"
else
 echo "Error: no simple_ncs_from_pdb.resolve produced"
 exit 1
endif

#
finish:
if ( $status_sav == 0 )then
  goto finished
else if ( $status_sav == 1 )then
    echo "*****************************************************"
    echo "Error: ${find_ncs} test failed to run with commands $args"
    echo "*****************************************************"
    exit 1
  goto finished
endif
end
finished:
echo "Done with ${find_ncs} $args"
echo "OK"
echo "------------------------------------------------------------"
echo " "
exit 0
