

import os

original_text="""     run_wizard_test(args)"""
original_text1= """    run_wizard_test(args)"""
new_text = """
     print("Setting up test")
     set_up_wizard_test(args)
     print("Running test")
     run(args=[])
"""
new_text1 = """
    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
"""


def run(args):
  path, t_full = os.path.split(args[0])
  t = "_".join(t_full.split("_")[:-1])
  assert os.path.split(t)[-1].startswith("test_")
  t_tst = "_".join(['tst']+t.split("_")[1:])


  for ext in ['.py','_cif.py']:
    for prefix in [t_tst]:
      fn = os.path.join(path,t_full,"%s%s" %(prefix, ext))
      print(fn)
      if not os.path.isfile(fn): continue
      text = open(fn).read()
      text = text.replace(original_text,new_text)
      text = text.replace(original_text1,new_text1)
      text = text.replace("import run_wizard_test","import set_up_wizard_test")
      f = open(fn,'w')
      print(text, file = f)
      f.close()

if __name__=="__main__":
  import sys
  run(sys.argv[1:])
