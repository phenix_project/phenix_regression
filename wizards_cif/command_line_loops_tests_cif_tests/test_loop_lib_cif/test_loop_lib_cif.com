#!/bin/csh -ef
#$ -cwd
mkdir run_test_loop_lib_cif
cp gene-5.cif run_test_loop_lib_cif/gene-5.cif
cp gene-5.pdb.mtz run_test_loop_lib_cif/gene-5.pdb.mtz
cp gap_gvp.cif run_test_loop_lib_cif/gap_gvp.cif
cd run_test_loop_lib_cif

echo "Be sure to run in empty directory"
echo "Running unit tests on clustering.py"
phenix.python<<EOD
from phenix.loop_lib.clustering import clustering
c=clustering()
c.tst()
EOD

echo "Running unit tests on pattern_match.py"
phenix.python <<EOD
from phenix.loop_lib.pattern_match import pattern_match
pm=pattern_match(database_dir="loop_lib")
pm.tst(verbose=False)
EOD

echo ""
echo "Setting up clusters with bin width 100 rmsd 1.5 loop_length 9"
phenix.python<<EOD
from phenix.loop_lib.pdb_mining import run
run("--file_name=gene-5.cif --set_up_cluster_from_file --bin_width=100 --loop_length=9 --rmsd=1.5 ".split())
EOD

echo ""
echo "Getting hash for these clusters"
phenix.python<<EOD
from phenix.loop_lib.pdb_mining import run
run("--file_name=gene-5.cif --set_up_cluster_from_file --bin_width=100 --loop_length=9 --rmsd=1.5 --existing_database=clusters_0.pkl.gz".split())
EOD

echo ""
echo "Selecting hash for all distance bins:"
cat database_0.log | sort -n -k12  > train.list
phenix.python <<EOD
from phenix.loop_lib.pattern_match import pattern_match
pm=pattern_match(database_dir="loop_lib")
pm.setup(file_name="train.list")
EOD


echo ""
echo "Reading/writing clusters"
phenix.python<<EOD
from phenix.loop_lib.clustering import clustering
c=clustering()
c.read_write_clusters(" clusters_0.pkl.gz split_clusters_3.pkl.gz ".split())
EOD

echo "Reading/writing clusters with database"
phenix.python<<EOD
from phenix.loop_lib.clustering import clustering
c=clustering()
c.read_write_clusters_database("clusters_0.pkl.gz test.pkl.gz".split())
EOD

echo ""
echo "Dumping segments to dump.pdb"
phenix.python<<EOD
from phenix.loop_lib.clustering import clustering
c=clustering()
c.read_dump_segments("dump.pdb".split())
EOD

echo ""
echo "Showing summary of clusters in loop_lib/clusters_0.pkl.gz"
phenix.python<<EOD
from phenix.loop_lib.clustering import clustering
c=clustering()
c.show_clusters()
EOD


echo ""
echo "Creating 2 sets of clusters and merging them"
phenix.python<<EOD
from phenix.loop_lib.clustering import clustering
c=clustering()
c.run_set_up_cluster(file_name="gvp.pkl.gz")
EOD

phenix.python<<EOD
from phenix.loop_lib.clustering import clustering
c=clustering()
c.run_set_up_cluster(file_name="aep.pkl.gz")
EOD

phenix.python<<EOD
from phenix.loop_lib.clustering import clustering
c=clustering()
c.merge_clusters(
       cluster_file="gvp.pkl.gz",
       cluster_file_merge_list=["aep.pkl.gz"])
EOD

echo ""
echo "..and merging via a list"
echo "aep.pkl.gz" > merge.list
phenix.python<<EOD
from phenix.loop_lib.clustering import clustering
c=clustering()
c.merge_clusters(
       cluster_file="gvp.pkl.gz",
       cluster_file_merge_list=open("merge.list").readlines())
EOD

echo ""
echo "..and merging, keeping only new ones:"
phenix.python<<EOD
from phenix.loop_lib.clustering import clustering
c=clustering()
c.merge_clusters(
       cluster_file="gvp.pkl.gz",
       cluster_file_merge_list=open("merge.list").readlines(),
       new_clusters_only=True)
EOD


echo ""
echo "..and merging, keeping only new ones, assuming unique:"
phenix.python<<EOD
from phenix.loop_lib.clustering import clustering
c=clustering()
c.merge_clusters(
       cluster_file="gvp.pkl.gz",
       cluster_file_merge_list=open("merge.list").readlines(),
       new_clusters_only=True,assume_unique=True)
EOD

echo ""
echo "Applying loops to gap_gvp.cif:"

phenix.python<<EOD
from phenix.loop_lib.clustering import clustering
cluster_file="loop_lib/clusters_0.pkl.gz"
database_dir=os.path.split(cluster_file)[0]
c=clustering(database_dir=database_dir)
print "Database dir will be: ",database_dir
c.apply_clusters(
        mtz_file="gene-5.pdb.mtz",ends_as_pdb="gap_gvp.cif",
        cluster_file=cluster_file,database_dir=database_dir)
EOD


if ( $status )then
  echo "FAILED"
  exit 1
endif
echo "ENDING:"


