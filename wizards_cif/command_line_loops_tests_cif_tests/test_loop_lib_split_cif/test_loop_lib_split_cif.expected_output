

#                       fit_loops
#
# Fit loops in a PDB file with gaps

# Type phenix.doc for help

usage: phenix.fit_loops protein.pdb mtzfile.mtz chain_id=A start=23 end=27 [labin='FP=FP PHIB=PHIM FOM=FOMM']

Values of all params:
fit_loops {
  input_files {
    pdb_in = "split.pdb"
    mtz_in = "random.mtz"
    map_coeff_labels = None
    labin = ""
    map_in = None
    seq_file = "sequence.dat"
    seq_prob_file = None
  }
  output_files {
    pdb_out = "connect.pdb"
    log = None
    params_out = "fit_loops_params.eff"
  }
  fitting {
    min_acceptable_prob = None
    refine_loops = True
    chain_id = None
    start = None
    end = None
    insert_or_delete_residues = None
    remove_loops = False
    skip_trim = True
    score_min = 1
    min_dist = 3
    min_log_prob = -5
    skip_seq_prob = False
    save_acceptable_loops = False
    n_random_loop = 200
    connect_all_segments = True
    sequential_only = False
    dist_max = 15
    ignore_sequence_register = False
    all_assigned = True
    sequence_offset = None
    loop_cc_min = 0.2
    aggressive = False
    target_insert = None
    time_per_residue = None
    loop_lib = False
    standard_loops = True
    trace_loops = False
    refine_trace_loops = True
    density_of_points = None
    a_cut_min = None
    max_density_of_points = None
    cutout_model_radius = None
    max_cutout_model_radius = 20
    padding = 1
    cut_out_density = True
    max_span = 30
    max_c_ca_dist = None
    max_overlap_rmsd = 2
    max_overlap = None
    min_overlap = None
  }
  crystal_info {
    resolution = 0
    chain_type = *PROTEIN DNA RNA
    final_solvent_content = None
  }
  directories {
    temp_dir = "temp_dir"
    create_temp_dir_if_missing = True
    output_dir = None
    gui_output_dir = None
  }
  control {
    verbose = False
    quick = False
    raise_sorry = False
    debug = True
    dry_run = False
    coarse_grid = False
    i_ran_seed = None
    resolve_command_list = None
    write_run_directory_to_file = None
    pickled_arg_dict = None
    nproc = 1
    max_wait_time = 1
    wait_between_submit_time = 1
    background = None
    run_command = "sh "
  }
  non_user_params {
    print_citations = True
  }
  gui {
    result_file = None
    job_title = None
  }
}

Guessed map labels as:  FP,SIGFP,PHIM,FOMM

Resolution set to    3.00 A
Fit_loops: Fit a missing loop in a model.
Model from:  split.pdb

Reading in sequence probability info and matching to input PDB

List of segments read from input PDB file:
Fixed gap length of  1 0.0  for  0 -> 1
Final model with loops is in  /net/anaconda/raid1/terwill/misc/b54test_38/command_line_loops_tests/test_loop_lib_split_cif/connect.pdb
Current residues built : 86   placed: 86

Citations for fit_loops:

Liebschner D, Afonine PV, Baker ML, Bunkóczi G, Chen VB, Croll TI, Hintze B,
Hung LW, Jain S, McCoy AJ, Moriarty NW, Oeffner RD, Poon BK, Prisant MG, Read
RJ, Richardson JS, Richardson DC, Sammito MD, Sobolev OV, Stockwell DH,
Terwilliger TC, Urzhumtsev AG, Videau LL, Williams CJ, Adams PD. (2019)
Macromolecular structure determination using X-rays, neutrons and electrons:
recent developments in Phenix. Acta Cryst. D75:861-877.

Terwilliger TC, Grosse-Kunstleve RW, Afonine PV, Moriarty NW, Zwart PH, Hung
L-W, Read RJ, Adams PD. (2008) Iterative model building, structure refinement
and density modification with the PHENIX AutoBuild wizard. Acta Cryst.
D64:61-69.


