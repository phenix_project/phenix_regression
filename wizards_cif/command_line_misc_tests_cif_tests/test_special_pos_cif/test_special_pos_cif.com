#!/bin/csh -ef
#$ -cwd

echo "Running autosol with sites on special positions"

phenix.autosol seq_file= BYDVCon5.fas unit_cell="167.923 53.764 122.964 90 116.539 90" space_group="C 1 2 1" data=merge_8.sca labels="I(+) SIGI(+) I(-) SIGI(-)" atom_type=cs lambda=1.7 sites=10 sites_file=bad.cif phase_only=true skip_xtriage=true 
echo "Should be no lines with PDB="



