#!/bin/csh -ef
#$ -cwd
mkdir test\\xtal\\nat\\sp\ ace\\here\\b\\t\\r\\z\\ending/
cp perfect.mtz test\\xtal\\nat\\sp\ ace\\here\\b\\t\\r\\z\\ending/perfect.mtz
cp side.cif    test\\xtal\\nat\\sp\ ace\\here\\b\\t\\r\\z\\ending/side.cif 
cp partial.cif test\\xtal\\nat\\sp\ ace\\here\\b\\t\\r\\z\\ending/partial.cif
cd test\\xtal\\nat\\sp\ ace\\here\\b\\t\\r\\z\\ending/
phenix.ligandfit data=perfect.mtz model=partial.cif ligand=side.cif resolution=3.0 n_group_search=1 ligand_cc_min=0.5 remove_path_word_list=dummy input_labels="FP PHIC FOM"
if ( $status )then
  echo "FAILED"
  exit 1
endif



