#!/bin/csh -ef
#$ -cwd
echo "" > tmp.dat

echo "ha_inv.cif coords.cif"
phenix.autosol peak.sca seq.dat build=false quick=true have_hand=true resolution=4 skip_xtriage=true remove_aniso=false thorough_denmod=false check_only=true debug=true partpdb_file=coords.cif sites_file=ha_inv.cif 

echo "ha_inv.cif coords_inv.cif"
phenix.autosol peak.sca seq.dat build=false quick=true have_hand=true resolution=4 skip_xtriage=true remove_aniso=false thorough_denmod=false check_only=true debug=true partpdb_file=coords_inv.cif sites_file=ha_inv.cif

echo "ha_inv.cif None"
phenix.autosol peak.sca seq.dat build=false quick=true have_hand=true resolution=4 skip_xtriage=true remove_aniso=false thorough_denmod=false check_only=true debug=true partpdb_file=None sites_file=ha_inv.cif 


echo "ha.cif coords.cif"
phenix.autosol peak.sca seq.dat build=false quick=true have_hand=true resolution=4 skip_xtriage=true remove_aniso=false thorough_denmod=false check_only=true debug=true partpdb_file=coords.cif sites_file=ha.cif 

echo "ha.cif coords_inv.cif"
phenix.autosol peak.sca seq.dat build=false quick=true have_hand=true resolution=4 skip_xtriage=true remove_aniso=false thorough_denmod=false check_only=true debug=true partpdb_file=coords_inv.cif sites_file=ha.cif

echo "ha.cif None"
phenix.autosol peak.sca seq.dat build=false quick=true have_hand=true resolution=4 skip_xtriage=true remove_aniso=false thorough_denmod=false check_only=true debug=true partpdb_file=None sites_file=ha.cif 


echo "None coords.cif"
phenix.autosol peak.sca seq.dat build=false quick=true have_hand=true resolution=4 skip_xtriage=true remove_aniso=false thorough_denmod=false check_only=true debug=true partpdb_file=coords.cif sites_file=None 

echo "None coords_inv.cif"
phenix.autosol peak.sca seq.dat build=false quick=true have_hand=true resolution=4 skip_xtriage=true remove_aniso=false thorough_denmod=false check_only=true debug=true partpdb_file=coords_inv.cif sites_file=None

echo "None None"
phenix.autosol peak.sca seq.dat build=false quick=true have_hand=true resolution=4 skip_xtriage=true remove_aniso=false thorough_denmod=false check_only=true debug=true partpdb_file=None sites_file=None 



