from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os,sys
  print ("Test script for test_two_fofc_denmod_in_rebuild_cif")
  print ("")
  log_file=os.path.join(os.getcwd(),"test_two_fofc_denmod_in_rebuild_cif.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  local_log_file='test_two_fofc_denmod_in_rebuild_cif.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''two_fofc_denmod_in_rebuild=True native.sca coords.cif rebuild_in_place=False number_of_parallel_models=1 maps_only=True refine=false''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)



  from phenix.command_line.get_cc_mtz_mtz  import get_cc_mtz_mtz
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes
  args=split_except_quotes('''two_fofc_denmod_map.mtz AutoBuild_run_1_/two_fofc_denmod_map.mtz''')
  get_cc_mtz_mtz(args=args,out=log)

  log.close()


  print ()
  print ('Done with test...running checks on output')
  print ()
  lines=open(log_file).readlines()
  from phenix_regression.wizards_cif.check_results import check_results
  check_results(test='test_two_fofc_denmod_in_rebuild_cif',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print ()
  print ('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards_cif.run_wizard_test import set_up_wizard_test
    args=['command_line_misc_tests_cif_tests','test_two_fofc_denmod_in_rebuild_cif']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
