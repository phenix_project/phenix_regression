#!/bin/csh -ef
#$ -cwd
cp model1.pdb model.pdb
phenix.resolve<<EOD
verbose
fix_rad_max
hklin random_1.mtz
labin FP=FWT PHIB=PHWT
no_build
ha_file NONE
evaluate_model
b_overall 0
fill_ratio 1.0
fill
res_fill 0.0
verbose
model model.pdb
use_wang
no_ha
EOD
if ( $status )then
  echo "FAILED"
  exit 1
endif
cp model3.pdb model.pdb
phenix.resolve<<EOD
verbose
fix_rad_max
hklin random_1.mtz
labin FP=FWT PHIB=PHWT
no_build
ha_file NONE
evaluate_model
b_overall 0
fill_ratio 1.0
fill
res_fill 0.0
verbose
model model.pdb
use_wang
no_ha
EOD
if ( $status )then
  echo "FAILED"
  exit 1
endif
echo "Model1 -Model 3 log files: Should be nearly the same"
echo "except at boundaries of residue 94 (residues 93-94-95)"

echo "testing  split_conformers=true"

phenix.get_cc_mtz_pdb short.mtz short.cif fix_xyz=true split_conformers=False > no_split.log
cat cc.log|grep EFO
phenix.get_cc_mtz_pdb short.mtz short.cif fix_xyz=true split_conformers=true> split.log
cat cc.log|grep EFO

echo "testing get_c_mtz_mtz"
phenix.get_cc_mtz_mtz short1.mtz short2.mtz
echo "testing get_c_mtz_mtz with maps"
phenix.get_cc_mtz_mtz short1.ccp4 short2.ccp4



