#!/bin/csh -ef
#$ -cwd
phenix.autobuild data=native.sca input_pdb_file=coords.cif number_of_parallel_models=1 refine=False rebuild_in_place=True map_file=work.mtz
if ( $status )then
  echo "FAILED"
  exit 1
endif

