#!/bin/csh -fv    
phenix.autosol fom_for_extreme_dm=0.01 direct_methods_only=true ha_iteration=False model_ha_iteration=False sites_file=ha.cif mask_cycles=1 minor_cycles=1 debug=true have_hand=true skip_xtriage=true remove_aniso=false lyso_as_dano.sca 2 se resolution=3 build=false 
phenix.autosol fom_for_extreme_dm=0.01 direct_methods_only=true ha_iteration=False model_ha_iteration=False sites_file=ha.cif mask_cycles=1 minor_cycles=1 debug=true have_hand=true skip_xtriage=true remove_aniso=false lyso_as_dano.mtz 2 se resolution=3 build=false   
phenix.autosol fom_for_extreme_dm=0.01 direct_methods_only=true ha_iteration=False model_ha_iteration=False sites_file=ha.cif mask_cycles=1 minor_cycles=1 debug=true have_hand=true skip_xtriage=true remove_aniso=false lyso_as_dano_imean.mtz 2 se resolution=3 build=false  
phenix.autosol fom_for_extreme_dm=0.01 direct_methods_only=true ha_iteration=False model_ha_iteration=False sites_file=ha.cif mask_cycles=1 minor_cycles=1 debug=true have_hand=true skip_xtriage=true remove_aniso=false lyso2001_scala1.mtz 2 se resolution=3 build=false  
phenix.autosol fom_for_extreme_dm=0.01 direct_methods_only=true ha_iteration=False model_ha_iteration=False sites_file=ha.cif mask_cycles=1 minor_cycles=1 debug=true have_hand=true skip_xtriage=true remove_aniso=false lyso2001_scala1.mtz 2 se resolution=3 build=false labels="F_CuKa SIGF_CuKa DANO_CuKa SIGDANO_CuKa ISYM_CuKa"  
phenix.autosol fom_for_extreme_dm=0.01 direct_methods_only=true ha_iteration=False model_ha_iteration=False sites_file=ha.cif mask_cycles=1 minor_cycles=1 debug=true have_hand=true skip_xtriage=true remove_aniso=false lyso2001_scala1.mtz 2 se resolution=3 build=false labels="F_CuKa SIGF_CuKa DANO_CuKa SIGDANO_CuKa ISYM_CuKa" refinement_labels="F_CuKa SIGF_CuKa FreeR_flag"  
phenix.autosol fom_for_extreme_dm=0.01 direct_methods_only=true ha_iteration=False model_ha_iteration=False sites_file=ha.cif mask_cycles=1 minor_cycles=1 debug=true have_hand=true skip_xtriage=true remove_aniso=false lyso2001_scala1.mtz 2 se resolution=3 build=false labels="F_CuKa SIGF_CuKa DANO_CuKa SIGDANO_CuKa ISYM_CuKa" refinement_labels="F_CuKa(+) SIGF_CuKa(+) F_CuKa(-) SIGF_CuKa(-)"  
phenix.autosol fom_for_extreme_dm=0.01 sites_file=ha.cif direct_methods_only=true ha_iteration=False model_ha_iteration=False lyso2001_scala1.mtz 2 se resolution=3 build=false labels="F_CuKa(+) SIGF_CuKa(+) F_CuKa(-) SIGF_CuKa(-)" refinement_labels="F_CuKa(+) SIGF_CuKa(+) F_CuKa(-) SIGF_CuKa(-)"  
phenix.autobuild  n_cycle_rebuild_max=0 number_of_parallel_models=1 coords.cif lyso_as_dano.sca   
phenix.autobuild  n_cycle_rebuild_max=0 number_of_parallel_models=1 coords.cif lyso_as_dano.sca   
phenix.autobuild  n_cycle_rebuild_max=0 number_of_parallel_models=1 coords.cif lyso_as_dano_imean.mtz  
phenix.autobuild  n_cycle_rebuild_max=0 number_of_parallel_models=1 coords.cif lyso2001_scala1.mtz  
phenix.autobuild  n_cycle_rebuild_max=0 number_of_parallel_models=1 coords.cif lyso2001_scala1.mtz input_labels="F_CuKa SIGF_CuKa DANO_CuKa SIGDANO_CuKa ISYM_CuKa"  
phenix.autobuild  n_cycle_rebuild_max=0 number_of_parallel_models=1 coords.cif lyso2001_scala1.mtz input_labels="F_CuKa SIGF_CuKa DANO_CuKa SIGDANO_CuKa ISYM_CuKa" input_refinement_labels="F_CuKa SIGF_CuKa FreeR_flag"



