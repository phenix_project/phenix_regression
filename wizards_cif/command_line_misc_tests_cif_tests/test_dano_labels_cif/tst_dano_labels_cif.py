from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os,sys
  print ("Test script for test_dano_labels_cif")
  print ("")
  log_file=os.path.join(os.getcwd(),"test_dano_labels_cif.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  local_log_file='test_dano_labels_cif.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autosol  import run_autosol
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''fom_for_extreme_dm=0.01 direct_methods_only=true ha_iteration=False model_ha_iteration=False sites_file=ha.cif mask_cycles=1 minor_cycles=1 debug=true have_hand=true skip_xtriage=true remove_aniso=false lyso_as_dano.sca 2 se resolution=3 build=false''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autosol(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)



  local_log_file='test_dano_labels_cif.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autosol  import run_autosol
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''fom_for_extreme_dm=0.01 direct_methods_only=true ha_iteration=False model_ha_iteration=False sites_file=ha.cif mask_cycles=1 minor_cycles=1 debug=true have_hand=true skip_xtriage=true remove_aniso=false lyso_as_dano.mtz 2 se resolution=3 build=false''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autosol(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)



  local_log_file='test_dano_labels_cif.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autosol  import run_autosol
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''fom_for_extreme_dm=0.01 direct_methods_only=true ha_iteration=False model_ha_iteration=False sites_file=ha.cif mask_cycles=1 minor_cycles=1 debug=true have_hand=true skip_xtriage=true remove_aniso=false lyso_as_dano_imean.mtz 2 se resolution=3 build=false''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autosol(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)



  local_log_file='test_dano_labels_cif.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autosol  import run_autosol
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''fom_for_extreme_dm=0.01 direct_methods_only=true ha_iteration=False model_ha_iteration=False sites_file=ha.cif mask_cycles=1 minor_cycles=1 debug=true have_hand=true skip_xtriage=true remove_aniso=false lyso2001_scala1.mtz 2 se resolution=3 build=false''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autosol(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)



  local_log_file='test_dano_labels_cif.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autosol  import run_autosol
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''fom_for_extreme_dm=0.01 direct_methods_only=true ha_iteration=False model_ha_iteration=False sites_file=ha.cif mask_cycles=1 minor_cycles=1 debug=true have_hand=true skip_xtriage=true remove_aniso=false lyso2001_scala1.mtz 2 se resolution=3 build=false labels="F_CuKa SIGF_CuKa DANO_CuKa SIGDANO_CuKa ISYM_CuKa"''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autosol(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)



  local_log_file='test_dano_labels_cif.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autosol  import run_autosol
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''fom_for_extreme_dm=0.01 direct_methods_only=true ha_iteration=False model_ha_iteration=False sites_file=ha.cif mask_cycles=1 minor_cycles=1 debug=true have_hand=true skip_xtriage=true remove_aniso=false lyso2001_scala1.mtz 2 se resolution=3 build=false labels="F_CuKa SIGF_CuKa DANO_CuKa SIGDANO_CuKa ISYM_CuKa" refinement_labels="F_CuKa SIGF_CuKa FreeR_flag"''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autosol(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)



  local_log_file='test_dano_labels_cif.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autosol  import run_autosol
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''fom_for_extreme_dm=0.01 direct_methods_only=true ha_iteration=False model_ha_iteration=False sites_file=ha.cif mask_cycles=1 minor_cycles=1 debug=true have_hand=true skip_xtriage=true remove_aniso=false lyso2001_scala1.mtz 2 se resolution=3 build=false labels="F_CuKa SIGF_CuKa DANO_CuKa SIGDANO_CuKa ISYM_CuKa" refinement_labels="F_CuKa(+) SIGF_CuKa(+) F_CuKa(-) SIGF_CuKa(-)"''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autosol(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)



  local_log_file='test_dano_labels_cif.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autosol  import run_autosol
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''fom_for_extreme_dm=0.01 sites_file=ha.cif direct_methods_only=true ha_iteration=False model_ha_iteration=False lyso2001_scala1.mtz 2 se resolution=3 build=false labels="F_CuKa(+) SIGF_CuKa(+) F_CuKa(-) SIGF_CuKa(-)" refinement_labels="F_CuKa(+) SIGF_CuKa(+) F_CuKa(-) SIGF_CuKa(-)"''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autosol(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)



  local_log_file='test_dano_labels_cif.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''n_cycle_rebuild_max=0 number_of_parallel_models=1 coords.cif lyso_as_dano.sca''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)



  local_log_file='test_dano_labels_cif.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''n_cycle_rebuild_max=0 number_of_parallel_models=1 coords.cif lyso_as_dano.sca''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)



  local_log_file='test_dano_labels_cif.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''n_cycle_rebuild_max=0 number_of_parallel_models=1 coords.cif lyso_as_dano_imean.mtz''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)



  local_log_file='test_dano_labels_cif.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''n_cycle_rebuild_max=0 number_of_parallel_models=1 coords.cif lyso2001_scala1.mtz''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)



  local_log_file='test_dano_labels_cif.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''n_cycle_rebuild_max=0 number_of_parallel_models=1 coords.cif lyso2001_scala1.mtz input_labels="F_CuKa SIGF_CuKa DANO_CuKa SIGDANO_CuKa ISYM_CuKa"''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)



  local_log_file='test_dano_labels_cif.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''n_cycle_rebuild_max=0 number_of_parallel_models=1 coords.cif lyso2001_scala1.mtz input_labels="F_CuKa SIGF_CuKa DANO_CuKa SIGDANO_CuKa ISYM_CuKa" input_refinement_labels="F_CuKa SIGF_CuKa FreeR_flag"''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)


  log.close()


  print ()
  print ('Done with test...running checks on output')
  print ()
  lines=open(log_file).readlines()
  from phenix_regression.wizards_cif.check_results import check_results
  check_results(test='test_dano_labels_cif',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print ()
  print ('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards_cif.run_wizard_test import set_up_wizard_test
    args=['command_line_misc_tests_cif_tests','test_dano_labels_cif']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
