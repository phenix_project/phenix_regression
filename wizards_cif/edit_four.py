

import os

def run(args):
  path, t_full = os.path.split(args[0])
  t = "_".join(t_full.split("_")[:-1])
  assert os.path.split(t)[-1].startswith("test_")
  t_tst = "_".join(['tst']+t.split("_")[1:])


  for ext in ['.expected_output','.com','.ignored_output','.py']:
    for prefix in [t, t_tst]:
      fn = os.path.join(path,t_full,"%s%s" %(prefix, ext))
      if not os.path.isfile(fn): continue
      print(fn)
      new_fn = os.path.join(path,t_full,"%s_cif%s" %(prefix, ext))
      text = open(fn).read()
      text = text.replace(t,"%s_cif" %(t))
      f = open(new_fn,'w')
      print(text, file = f)
      f.close()
      os.rename(fn,"%s.old" %(fn))

if __name__=="__main__":
  import sys
  run(sys.argv[1:])
