from __future__ import print_function
# demo run density modification with any mtz file and start file and ncs
import sys
args=sys.argv[1:]
assert len(args) >= 1, "phenix.python build_model.py hklin pdb_in 'FWT,PHWT' "

from iotbx import reflection_file_reader
reflection_file=reflection_file_reader.any_reflection_file(args[0])
miller_arrays=reflection_file.as_miller_arrays(merge_equivalents=True)
from solve_resolve.resolve_python import refl_db
labels='FWT,PHWT'
if len(args)>2:
  labels=args[2]
  print("Labels used: %s" %(labels))
array_dict=refl_db.assign_miller_arrays( miller_arrays=miller_arrays,
  target_label_dict={'map_coeffs':labels})
map_coeffs=array_dict.get('map_coeffs')

import iotbx.pdb
pdb_inp=iotbx.pdb.input(args[1])
atoms_with_labels=pdb_inp.construct_hierarchy().atoms_with_labels()

from solve_resolve.resolve_python import resolve_in_memory
input_text="""
morph MEMORY
rad_morph 6
morph_main ! only include main-chain + cb in morphing
morph_as_group !  morph everything in one go
morph_max_dis 20 !  max shift in morphing
"""
result_obj=resolve_in_memory.run(
        map_coeffs=map_coeffs, # map coefficients
        input_text=input_text, # any model-building commands
        model_pdb_inp_atoms_with_labels=atoms_with_labels,
        )
cmn=result_obj.results
f=open('model.pdb','w')
print(cmn.atom_db.pdb_out_as_string, file=f)
f.close()
