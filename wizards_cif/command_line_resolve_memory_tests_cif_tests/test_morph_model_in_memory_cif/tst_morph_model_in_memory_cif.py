from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print ("Test script for test_morph_model_in_memory_cif")
  print ("")
  log_file=os.path.join(os.getcwd(),"test_morph_model_in_memory_cif.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.python morph.py map_coeffs.mtz coords.cif')
  for line in result.stdout_lines:
    print (line, file = log)


  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.python morph.py m2_box.mtz m2_box.cif "BoxMap,PHIBoxMap"')
  for line in result.stdout_lines:
    print (line, file = log)


  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.python morph_group.py map_coeffs.mtz offset.cif')
  for line in result.stdout_lines:
    print (line, file = log)



  log.close()


  print ()
  print ('Done with test...running checks on output')
  print ()
  lines=open(log_file).readlines()
  from phenix_regression.wizards_cif.check_results import check_results
  check_results(test='test_morph_model_in_memory_cif',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print ()
  print ('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards_cif.run_wizard_test import set_up_wizard_test
    args=['command_line_resolve_memory_tests_cif_tests','test_morph_model_in_memory_cif']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
