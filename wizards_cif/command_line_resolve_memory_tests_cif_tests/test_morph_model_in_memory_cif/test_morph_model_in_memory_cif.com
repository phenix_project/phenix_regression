#!/bin/csh -ef
#$ -cwd
echo "standard morph"
phenix.python morph.py map_coeffs.mtz coords.cif
if ( $status )then
  echo "FAILED"
  exit 1
endif
phenix.superpose_pdbs model.pdb coords_offset.cif

echo "morph in box"
phenix.python morph.py m2_box.mtz m2_box.cif "BoxMap,PHIBoxMap"
if ( $status )then
  echo "FAILED"
  exit 1
endif
phenix.superpose_pdbs model.pdb m2_box_offset.cif

echo "morph as group "
phenix.python morph_group.py map_coeffs.mtz offset.cif 
if ( $status )then
  echo "FAILED"
  exit 1
endif
phenix.superpose_pdbs model.pdb coords.cif




