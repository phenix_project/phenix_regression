from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print ("Test script for test_resolve_denmod_pdb_memory_cif")
  print ("")
  log_file=os.path.join(os.getcwd(),"test_resolve_denmod_pdb_memory_cif.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  cmds='''import sys
args="exptl_fobs_phases_freeR_flags.mtz pdb_file=start.cif solvent_content=0.6 resolve_commands_file=inputs.dat mask_cycles=1 minor_cycles=1 resolution=3.0".split()
from solve_resolve.resolve_python.density_modify_in_memory import get_files_and_run
get_files_and_run(args)
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print (open('phenix.python.log').read(), file = log)

  cmds='''import sys
args="exptl_fobs_phases_freeR_flags.mtz pdb_file=start.cif solvent_content=0.6 resolve_commands_file=inputs.dat mask_cycles=1 minor_cycles=1 resolution=3.0 mask_from_pdb=mask.cif".split()
from solve_resolve.resolve_python.density_modify_in_memory import get_files_and_run
get_files_and_run(args)
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print (open('phenix.python.log').read(), file = log)

  log.close()


  print ()
  print ('Done with test...running checks on output')
  print ()
  lines=open(log_file).readlines()
  from phenix_regression.wizards_cif.check_results import check_results
  check_results(test='test_resolve_denmod_pdb_memory_cif',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print ()
  print ('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards_cif.run_wizard_test import set_up_wizard_test
    args=['command_line_resolve_memory_tests_cif_tests','test_resolve_denmod_pdb_memory_cif']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
