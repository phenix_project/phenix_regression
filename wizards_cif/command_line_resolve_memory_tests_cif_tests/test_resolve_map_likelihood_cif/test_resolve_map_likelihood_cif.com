#!/bin/csh -ef
#$ -cwd

echo ""
echo "RUNNING test_resolve_map_likelihood_cif"
#!/bin/csh -f
echo "demo of map_likelihood"
echo "random_xx.mtz has randomized phases with <cos(delta_phi)>=xx"
echo ""
echo "Running map_likelihood with base of random_6.mtz and test of random_0.mtz"

phenix.python<<EOD
from solve_resolve.resolve_python.map_likelihood import run
args=" random_6.mtz random_0.mtz".split()
run(args)
EOD

if ( $status ) then
  echo "FAILED"
  exit 1
endif
echo "Running map_likelihood with base of random_6.mtz and test of random_999.mtz"
phenix.python<<EOD
from solve_resolve.resolve_python.map_likelihood import run
args="random_6.mtz random_999.mtz ha.cif".split()
run(args)
EOD
if ( $status ) then
  echo "FAILED"
  exit 1
endif


