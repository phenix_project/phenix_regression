#!/bin/csh -ef
#$ -cwd

echo ""
echo "RUNNING test_ncs_average_cif"
phenix.ncs_average ncs_map_coeffs.mtz ncs.cif
if ( $status ) then
  echo "FAILED"
  exit 1
endif
phenix.ncs_average ncs_map_coeffs.mtz ncs.ncs_spec
if ( $status ) then
  echo "FAILED"
  exit 1
endif
phenix.get_cc_mtz_mtz  ncs_map_coeffs.mtz ncs_average.mtz|grep offset


