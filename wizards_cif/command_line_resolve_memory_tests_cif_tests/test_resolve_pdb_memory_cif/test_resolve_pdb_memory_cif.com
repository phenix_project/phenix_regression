#!/bin/csh -ef
#$ -cwd


echo "RUNNING test_command_line_resolve_pdb_memory"

phenix.python<<EOD
from solve_resolve.resolve_python.density_modify_in_memory import get_files_and_run
args="phaser_1.mtz resolve_commands_file=pdb_not_in_memory.dat output_mtz=denmod.mtz solvent_content=0.64 pdb_in=coords_h.cif".split()
get_files_and_run(args)
EOD

if ( $status ) then
  echo "FAILED"
  exit 1
endif
phenix.python<<EOD
from solve_resolve.resolve_python.density_modify_in_memory import get_files_and_run
args="phaser_1.mtz pdb_file=coords_h.cif resolve_commands_file=pdb_in_memory.dat output_mtz=denmod.mtz denmod_with_model=False solvent_content=0.64 pdb_in=coords_h.cif".split()
get_files_and_run(args)
EOD

if ( $status ) then
  echo "FAILED"
  exit 1
endif
echo "DONE WITH TEST"


