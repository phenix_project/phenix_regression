#!/bin/csh -f


echo "RUNNING test_command_line_resolve_denmod_memory"
echo "For summary correlation, search for 'Starting correlation:'"
echo ""

echo "Testing density_modification.py"
echo "Testing std denmod "
phenix.density_modification phaser_1.mtz mask_cycles=1 minor_cycles=1 resolution=3 output_mtz=denmod_std.mtz solvent_content=0.64 mask_type=histograms|tee std.log 
if ( $status ) then
  echo "FAILED"
  exit 1
endif
phenix.get_cc_mtz_mtz denmod_std.mtz denmod_std_expected.mtz|grep 'after offsetting maps'


echo "Testing std denmod using HL coeffs to regenerate phib fom"
phenix.density_modification fom_labels=SKIP phib_labels=SKIP phaser_1.mtz mask_cycles=1 minor_cycles=1 resolution=3 output_mtz=denmod_std_hl.mtz solvent_content=0.64 mask_type=histograms|tee std_hl.log 
if ( $status ) then
  echo "FAILED"
  exit 1
endif
phenix.get_cc_mtz_mtz denmod_std_hl.mtz denmod_std_expected.mtz|grep 'after offsetting maps'


echo "Testing std denmod no remove_aniso"
phenix.density_modification phaser_1.mtz mask_cycles=1 minor_cycles=1 resolution=3 output_mtz=denmod_no_aniso.mtz solvent_content=0.64 remove_aniso=False mask_type=histograms resolve_commands_file=resolve_commands.dat|tee std_no_aniso.log 
if ( $status ) then
  echo "FAILED"
  exit 1
endif
phenix.get_cc_mtz_mtz denmod_no_aniso.mtz denmod_no_aniso_expected.mtz|grep 'after offsetting maps'


echo "Testing std denmod ha_file "
phenix.density_modification phaser_1.mtz mask_cycles=1 minor_cycles=1 resolution=3 output_mtz=denmod_ha.mtz solvent_content=0.64 remove_aniso=True ha_file=ha.cif rad_mask=3 mask_type=histograms|tee std_ha.log 
if ( $status ) then
  echo "FAILED"
  exit 1
endif
phenix.get_cc_mtz_mtz denmod_ha.mtz denmod_ha_expected.mtz|grep 'after offsetting maps'



echo "Testing std denmod model_file "
phenix.density_modification phaser_1.mtz mask_cycles=1 minor_cycles=1 resolution=3 output_mtz=denmod_model.mtz solvent_content=0.64 remove_aniso=True pdb_file=coords.cif rad_mask=3 mask_type=histograms|tee std_model.log 
if ( $status ) then
  echo "FAILED"
  exit 1
endif
phenix.get_cc_mtz_mtz denmod_model.mtz denmod_model_expected.mtz|grep 'after offsetting maps'



echo "Testing std denmod model mask"
phenix.density_modification phaser_1.mtz mask_cycles=1 minor_cycles=1 resolution=3 output_mtz=denmod_model_mask.mtz solvent_content=0.64 remove_aniso=True rad_mask=2 mask_type=histograms mask_from_pdb=mask.cif|tee std_model_mask.log 
if ( $status ) then
  echo "FAILED"
  exit 1
endif
phenix.get_cc_mtz_mtz denmod_model_mask.mtz denmod_model_mask_expected.mtz|grep 'after offsetting maps'



echo "Testing std denmod model mask with model"
phenix.density_modification phaser_1.mtz mask_cycles=1 minor_cycles=1 resolution=3 output_mtz=denmod_model_mask_model.mtz solvent_content=0.64 remove_aniso=True rad_mask=2 mask_type=histograms mask_from_pdb=mask.cif pdb_file=coords.cif|tee std_model_mask_model.log 
if ( $status ) then
  echo "FAILED"
  exit 1
endif
phenix.get_cc_mtz_mtz denmod_model_mask_model.mtz denmod_model_mask_model_expected.mtz|grep 'after offsetting maps'



echo "Testing ncs"

phenix.density_modification ncs.mtz map_coeffs_file=phaser_1.mtz seq_file=seq.dat mask_cycles=1 minor_cycles=1 resolution=3 output_mtz=denmod_ncs.mtz solvent_content=0.50 remove_aniso=True mask_type=histograms ncs_file=ncs.ncs_spec pdb_file=ncs_one_chain_plus.cif|tee std_ncs.log 
if ( $status ) then
  echo "FAILED"
  exit 1
endif
phenix.get_cc_mtz_mtz denmod_ncs.mtz denmod_ncs_expected.mtz|grep 'after offsetting maps'



echo "Testing ncs_mask"

phenix.density_modification ncs.mtz map_coeffs_file=phaser_1.mtz seq_file=seq.dat mask_cycles=1 minor_cycles=1 resolution=3 output_mtz=denmod_ncs_mask.mtz solvent_content=0.50 remove_aniso=True mask_type=histograms ncs_file=ncs.ncs_spec pdb_file=ncs_one_chain_plus.cif ncs_domain_pdb=ncs_mask.cif|tee std_ncs_mask.log 
if ( $status ) then
  echo "FAILED"
  exit 1
endif
phenix.get_cc_mtz_mtz denmod_ncs_mask.mtz denmod_ncs_mask_expected.mtz|grep 'after offsetting maps'




