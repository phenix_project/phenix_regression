from __future__ import print_function
#
# script to create python scripts to run wizard tests based on the existing
# csh scripts

# TO USE ON ONE WIZARD TEST, USE SOMETHING LIKE:
#  phenix.python create_py.py test_name=test_cutout

# TO ADD A NEW TEST:
# copy a directory like command_line_map_to_model_tests/test_map_to_model_quick/
# and edit the file names and contents
# THEN IN THIS FILE (create_py.py) add lines for where any new commands are
#  located (like phenix.map_to_model)
#  'map_to_model':'run',
#  'map_to_model':'out',
#    'map_to_model':'phenix.command_line',
# NOTE: New programs (including now map_to_model) use phenix.programs instead

import os,sys

skip=["update_wizard_tests.py"]

def select_names(name_list,text=None):
  new_list=[]
  for n in name_list:
    if n.find(text)>-1 and not n in skip:
      new_list.append(n)
  return new_list

def get_tests(wizard_dir=None,test_group=None):
  tests=os.listdir(os.path.join(wizard_dir,test_group))
  test_dirs=[]
  for t in tests:
    if t==".svn": continue
    if os.path.isdir(os.path.join(wizard_dir,test_group,t)):
      test_dirs.append(t)
  return test_dirs

def is_nothing(line):
  if not line: return True
  if line.startswith("#"): return True
  if line.startswith("if ") and line.find("$status")>-1 and line.find("goto")>-1:
     return True
  if line.strip().startswith('set mmtbx'): return True
  if line.strip().startswith('set solve_resolve'): return True
  if line.strip().startswith('setenv PATH'): return True
  if line.strip().startswith('set phenix'): return True
  if line.strip().startswith('set solve'): return True
  if line.strip().startswith('set phenix_regression'): return True
  if line.strip().startswith('setenv SOLVETMPDIR /var/tmp'): return True
  if line.strip().startswith('setenv LIBTBX_FULL_TESTING'): return True
  if line.strip().startswith('set phenix_dist="`libtbx.show_dist_paths phenix`"'): return True
  if line.strip().startswith('set phenix_regression="`libtbx.find_in_repositories phenix_regression`"'): return True

  return False

def join_lines(text):
  new_lines=[]
  saved_line=""
  for line in text.splitlines():
    line=line.rstrip()
    if line.endswith("\\"):
      line=line[:-1]
      if saved_line:
        saved_line+=line
      else:
        saved_line=line
    else:
      if saved_line:
        new_lines.append(saved_line+line)
        saved_line=""
      else:
        new_lines.append(line)
  if saved_line:
      new_lines.append(saved_line)
  return "\n".join(new_lines)

def is_command(line):
  for x in ["phenix","solve_resolve","cp ","cat ","head ","if ",
    "switch","foreach","end","endsw","diff","goto","exit","exit","echo",
    "set ","setenv","else","bad:","finished:","breaksw","finish:","case",
    "@ ","mkdir","cd","grep","./","wc","ls ","skip:","run2:","run3:"]:
    if line.lstrip().startswith(x):
      return True

def is_eod(line):
  if line.find("EOD") > -1: return True
  return False

def is_command_with_eod(line):
  if not is_command(line): return False
  if line.find("EOD") > -1: return True
  return False

def remove_diff(lines,test):
  files_to_remove_diff="""
    test_ampl_int_autobuild.com

  """.split()
  test_name="%s.com" %(test)

  if not test_name in files_to_remove_diff: return lines
  # this is one to work on
  new_lines=[]
  for line in lines:
    if line.strip().startswith("diff ") and not \
      line.startswith("if ( $status == 1) goto"):
      pass
    else:
      new_lines.append(line)
  return new_lines

def remove_after_goto(lines):
  new_lines=[]
  for line in lines:
    if line.startswith("goto "):
      return new_lines
    else:
      new_lines.append(line)
  return new_lines

def remove_help_lines(lines):

  # remove lines like these:
  """if ($#argv > 0) then
  if ($argv[1] == "--help") then
    echo "test autobuild with intensities and amplitudes in MTZ file"
    exit 0
    endif
  endif
  """

  end_line=None
  start_line=None
  have_help=None
  endif_count=0

  for i in range(len(lines)):
    line=lines[i].strip()
    if line.startswith("if"):
      if line.find("argv ")>-1:
        assert start_line is None
        start_line=i
      elif line.find("help")>-1:
        assert start_line is not None
        assert have_help is None
        have_help=True
    elif line.startswith("endif"):
      if start_line is not None:
        endif_count+=1
        if endif_count==2:
          end_line=i
          break

  if start_line is not None:
    lines=lines[:start_line]+lines[end_line+1:]

  return lines

def remove_passed(i=None,line_groups=None):
  new_line_groups=[]
  for x in line_groups:
    if i<=x[1]:
       new_line_groups.append(x)
  return new_line_groups

def next_lines(i=None,lines=None,
      next_line_groups=None,
      next_if_groups=None,
      next_foreach_groups=None):

     # see if any group starts here
     next_set_of_lines=[]
     line_type=None

     # get rid of any groups we already passed (duplicate types)
     next_line_groups=remove_passed(i=i,line_groups=next_line_groups)
     next_if_groups=remove_passed(i=i,line_groups=next_if_groups)
     next_foreach_groups=remove_passed(i=i,line_groups=next_foreach_groups)

     if next_if_groups and next_if_groups[0][0]==i:
       line_type='if_group'
       for j in range(next_if_groups[0][0],next_if_groups[0][1]+1):
         next_set_of_lines.append(lines[j])
       i=next_if_groups[0][1]+1
       next_if_groups=next_if_groups[1:]
     elif next_line_groups and next_line_groups[0][0]==i:
       for j in range(next_line_groups[0][0],next_line_groups[0][1]+1):
         next_set_of_lines.append(lines[j])
       i=next_line_groups[0][1]+1
       next_line_groups=next_line_groups[1:]
       if len(next_set_of_lines)>1:
         line_type='line_group'
       else:
         line_type='line'

     elif next_foreach_groups and next_foreach_groups[0][0]==i:
       line_type='foreach_group'
       for j in range(next_foreach_groups[0][0],next_foreach_groups[0][1]+1):
         next_set_of_lines.append(lines[j])
       i=next_foreach_groups[0][1]+1
       next_foreach_groups=next_foreach_groups[1:]

     else:  # just a line
       line_type='line'
       next_set_of_lines.append(lines[i])
       i+=1

     return i,line_type,next_set_of_lines,next_line_groups,next_if_groups,\
        next_foreach_groups

def unroll_foreach(lines):
  line_groups,lines=get_line_groups(lines)
  # find foreach blocks and unroll them, replacing characters with values

  foreach_groups=[]
  foreach_start=None
  for j in range(len(lines)):
    line=lines[j]
    if line.startswith("foreach"):
      assert foreach_start is None
      foreach_start=j
    elif line.startswith("endif"):
      pass
    elif line.startswith("end"):
      assert foreach_start is not None
      foreach_groups.append([foreach_start,j])
      foreach_start=None
  assert foreach_start is None
  if foreach_groups:
    # unroll the foreach lines
    assert len(foreach_groups)<2
    start_line=foreach_groups[0][0]
    end_line=foreach_groups[0][1]
    new_lines=lines[:start_line]
    spl=lines[start_line].replace("(","").replace(")","").split()[1:]
    #     foreach x ( a b c d e ) ->  [x,a,b,d,e]
    key=spl[0]
    values=spl[1:]
    #print ("KEY:",key,"VALUES: ",values)
    replacement_lines=lines[start_line+1:end_line] # leave off first last
    for value in values:
      kw_dict={key:value.replace("'","").replace('"',''),
         }
      new_lines.append("print ('Running with %s', file = log)" %(value))
      for line in replacement_lines:
        new_lines.append(replace_kw(line,kw_dict))
  else:
    new_lines=lines
  return new_lines

def get_line_groups(lines):
  verbose=False
  i=-1
  line_groups=[]
  command_with_eod_start=None
  command_start=None
  new_lines=[]
  skip_next=False
  for ii in range(len(lines)):
    if skip_next:
     skip_next=False
     continue
    line=lines[ii].strip()
    if is_nothing(line):
      continue
    if line.startswith("diff -b") and ii<len(lines)-1 and lines[ii+1].strip().find('compare_files.py')>-1:
      skip_next=True
      continue
    if command_with_eod_start is not None: # only allow kw or EOD
      new_lines.append(line)
      i+=1
      if is_eod(line):
        line_groups.append([command_with_eod_start,i])
        command_with_eod_start=None
      else:
        pass # add in more lines
    elif is_command_with_eod(line):
      new_lines.append(line)
      i+=1
      assert command_with_eod_start is None
      command_with_eod_start=i
    elif is_command(line):
      new_lines.append(line)
      i+=1
      line_groups.append([i,i])
    else:
      if verbose:
        print("SKIPPED LINE: %s"  %(line.strip()))
  assert command_with_eod_start is None

  return line_groups,new_lines

def set_up_test(wizard_dir=None,test_directory=None,
      test_group=None,test=None,out=sys.stdout):
  com_file=os.path.join(test_directory,"%s.com" %(test))
  print("Working in %s with file %s" %(test_directory,com_file), file=out)
  assert os.path.isfile(com_file)
  text=open(com_file).read()
  text=join_lines(text)

  # find things line phenix.find_ncs a b c d |grep xx > tmp.dat
  # or phenix.solve<<EOD | grep xx > tmp.dat ....EOD
  verbose=False
  lines=text.splitlines()
  lines=remove_help_lines(lines)
  lines=remove_after_goto(lines)
  lines=break_up_compound_lines(lines)
  lines=remove_diff(lines,test)
  lines=unroll_foreach(lines)
  line_groups,new_lines=get_line_groups(lines)

  lines=new_lines
  assert new_lines
  # identify sets of line_groups that are all part of a single if else endif
  #  or switch case endsw

  if_groups=[]
  switch_groups=[]
  foreach_groups=[] # not used any more
  if_start=None
  switch_start=None
  for i in range(len(lines)):
    line=lines[i].strip()
    if line.startswith("if") and line.endswith("then"):
      assert if_start is None
      if_start=i
    elif line.replace(" ","").startswith("endif"):
      if if_start is not None:
        if_groups.append([if_start,i])
        if_start=None
    elif line.startswith("else"):
      pass # part of the if statement

  assert if_start is None
  assert switch_start is None

  return lines,line_groups,if_groups,switch_groups,foreach_groups

def contents_of_quotes(line):
  if line.count('"')==2:
    if line.count("'")==2:
      if line.find("'")<line.find('"'):
        quote="'"
      else:
        quote='"'
    else:
      quote='"'
  elif line.count('"')==2:
    quote="'"
  spl=line.split(quote)
  return spl[1]

def echo_to_print(line):
  contents=contents_of_quotes(line)
  return 'print ("%s")' %(contents)

def remove_after_bar(xx,remove_after_redirect=True):
  # remove everything after | if present
  if xx.find("|")>-1:
    xx=xx.split("|")[0]
  if remove_after_redirect and xx.find(">")>-1:
    xx=xx.split(">")[0]
  return xx

def lines_from_echo_with_embed(line):
  contents=contents_of_quotes(line)
  new_lines=[]
  spl=contents.split("`")
  spl=spl[1:]
  for x in spl:
    if x.find(";")>-1:
      local_spl=x.split(";")
    else:
      local_spl=[x]
    for xx in local_spl:
      new_lines.append(remove_after_bar(xx))
  return new_lines

def replace_kw(line,kw_dict):
  found=False
  for x in kw_dict.keys():
    if line.find("$%s" %(x))>-1 or line.find("${%s}" %(x))>-1:
      line=line.replace("$%s" %(x),x).replace("${%s}" %(x),x).replace(x,kw_dict[x])
      found=True
  if found:
    return replace_kw(line,kw_dict)
  else:
    return line

def break_up_compound_lines(lines):
  new_lines=[]
  kw_dict={}
  from copy import deepcopy
  for i in range(len(lines)):
    line=lines[i]
    orig_line=deepcopy(line)
    line=replace_kw(line,kw_dict) # replace any kw if present

    if line.startswith("echo"):
      if line.find("`")==-1: # just a print command
        new_lines.append(echo_to_print(line))
      else: #this has an embedded command or commands
        new_lines+=lines_from_echo_with_embed(line)

    else:
      line=remove_after_bar(line)
      if line.startswith("set"):
        if line.startswith("setenv"):
          spl=line.strip().split()
          value=" ".join(spl[2:]).replace("'","").replace('"','')
          kw_dict[spl[1]]=value
        else:
          spl=line.strip().split("=")
          key=spl[0].split()[1]
          value="=".join(spl[1:]).replace("'","").replace('"','')
          kw_dict[key]=value
        continue # skip the line
      else:
        new_lines.append(line)
  return new_lines

def grep_line(line):
  # look for grep -i xx file_name
  target=None
  file_name_list=[]
  lower_upper=None
  started_quotes=None
  quoted_list=None
  for x in line.split():
    if x.startswith('"') or x.startswith("'"):
      assert started_quotes is None
      started_quotes=True
      quoted_list=[]
    if started_quotes:
      quoted_list.append(x)
      if x.endswith('"') or x.startswith("'"):
        started_quotes=None
        x=" ".join(quoted_list)[1:-1]
        quoted_list=None
      else:
        continue # don't do the rest of this if we are accumulating quotes

    if x=='grep':
      pass
    elif x=="-i":
      lower_upper=True
    elif not target:
      target=x
    else:
      file_name_list.append(x)

  text="""
for file_name in %s:
  for line in open(file_name).readlines():
    if line.lower().find("%s")>-1:
      print (line, file = log)
""" %(file_name_list,target.lower())

  new_lines=text.splitlines()
  return new_lines

def python_line(line):
  full_command=" ".join(line.strip().split(">")[0].split())
  cmd=full_command.split()[0]

  # hard-wire fix
  if full_command=="""phenix.mr_rosetta set_real_space_optimize=True map_coeffs=chainI.mtz search_models=short.pdb seq_file=seq.dat labin_map_coeffs='FP=FWT PHIB=PHWT' rosetta_rebuild.rosetta_models=1 relax_top_models.nstruct=1 rosetta_fixed_seed=771001""":
    full_command="""phenix.mr_rosetta set_real_space_optimize=True map_coeffs=chainI.mtz search_models=short.pdb seq_file=seq.dat labin_map_coeffs="FP=FWT PHIB=PHWT" rosetta_rebuild.rosetta_models=1 relax_top_models.nstruct=1 rosetta_fixed_seed=771001"""

  text="""
from libtbx import easy_run
result=easy_run.fully_buffered(command='%s')
for line in result.stdout_lines:
  print (line, file = log)
""" %(full_command)

  return text.splitlines()

cmd_dict={
  'remove_poor_fragments':'run',  #  autosol etc...
  'autobuild':'run_autobuild',  #  autosol etc...
  'autosol':'run_autosol',  #  autosol etc...
  'automr':'run_automr',  #  automr etc...
  'ligandfit':'run_ligandfit',  #  ligandfit etc...
  'superpose_pdbs':'run',
  'simple_ncs_from_pdb':'run',   # run(args) instead of simple_ncs_from_pdb(args)
  'segment_and_split_map':'run',
  'map_model_cc':'run',
  'map_to_model':'run',
  'map_to_structure_factors':'run',
  'map_box':'run',
  'reflection_file_converter':'run',
  'anomalous_signal':'run',
  'average_map_coeffs':'run',
  'sad_data_from_pdb':'run',
  'fit_loops':'run',
  'hyss':'run',
  'fmodel':'run',
  'ramalyze':'run',
  'list':'run',
  'density_modification':'get_files_and_run',
  'pdbtools':'run',
  'emma':'run',
  'get_patterson_skew':'run',
  'scale_and_merge':'run',
  'iterative_ss_refine':'run',
   'get_struct_fact_from_md':'run',
   }
use_out_dict={
  'pdbtools':'out',
  'map_to_structure_factors':'log',
  'map_box':'log',
  'map_model_cc':'log',
  'get_cc_mtz_pdb':'out',
  'get_cc_mtz_mtz':'out',
  'parallel_autobuild':'out',
  'segment_and_split_map':'out',
  'map_to_model':'out',
  'remove_poor_fragments':'log',
  'anomalous_signal':'out',
  'average_map_coeffs':'out',
  'sad_data_from_pdb':'out',
  'morph_model':'out',
  'hyss':'out',
  'list':'out',
  'density_modification':'out',
  'build_rna_helices':'out',
  'scale_and_merge':'out',
  'replace_side_chains':'out',
  'get_patterson_skew':'out',
  'assign_sequence':'out',
  'ncs_average':'out',
  'map_to_object':'out',
  'find_all_ligands':'out',
  'find_ncs':'out',
  'multi_crystal_average':'out',
  'fit_loops':'out',
  'combine_models':'out',
  'iterative_ss_refine':'out',
  'ramalyze':'out',
  'superpose_pdbs':'log',
 }
command_line_dict={
     'remove_poor_fragments':'phenix.autosol',
     'segment_and_split_map':'cctbx.maptbx',
     'map_model_cc':'phenix.programs',
     'fmodel':'mmtbx.command_line',
     'map_to_structure_factors':'mmtbx.command_line',
     'map_box':'mmtbx.command_line',
     'density_modification':'solve_resolve.resolve_python.density_modify_in_memory',
     'emma':'iotbx.command_line',
     'ramalyze':'mmtbx.programs',
     'pdbtools':'mmtbx.programs',
     'reflection_file_converter':'iotbx',
     'map_to_model':'phenix.programs',
     'fit_loops':'phenix.programs',
     'iterative_ss_refine':'phenix.autosol',
     'get_struct_fact_from_md':'phenix.utilities',
}
local_command_dict={
    'density_modification':''}

def swap_double_if_nec(args):
  text=" ".join(args)
  if text.startswith("'") or text.endswith("'"):
      new_args=[]
      for arg in args:
        new_args.append(arg.replace("'","|").replace('"',"'").replace("|",'"'))
      return new_args
  else:
    return args

def auto_something_line(line,test):
  #args=line.replace(" =","=").replace("= ","=").replace("'",'"').split()[1:]
  args=line.replace(" =","=").replace("= ","=").split()[1:]
  args=swap_double_if_nec(args)
  cmd=line.split()[0].split(".")[1]
  full_cmd=cmd_dict.get(cmd,cmd)

  command_line_location=command_line_dict.get(cmd,'phenix.command_line')
  ending=local_command_dict.get(cmd,".%s" %(cmd))

  if cmd in use_out_dict.keys():
    if command_line_location.endswith('.programs'):
      ending=local_command_dict.get(cmd,"%s" %(cmd))
      text="""

from %s  import %s as run

from iotbx.cli_parser import run_program
from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes
args=split_except_quotes('''%s''')
run_program(program_class=run.Program,args=args,logger=log)
"""  %(command_line_location,ending," ".join(args))

    else: # usual
      text="""

from %s%s  import %s
from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes
args=split_except_quotes('''%s''')
%s(args=args,%s=log)
"""  %(command_line_location,ending,full_cmd," ".join(args),full_cmd,use_out_dict[cmd])

  else:

    text="""

local_log_file='%s.log'
local_log=open(local_log_file,'w')
from %s%s  import %s
from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

args=split_except_quotes('''%s''')
sys_stdout_sav=sys.stdout
sys.stdout=local_log
%s(args=args)
sys.stdout=sys_stdout_sav
local_log.close()
print (open(local_log_file).read(), file = log)

"""  %(test,command_line_location,ending,full_cmd," ".join(args),full_cmd)



  lines=text.splitlines()
  return lines

def mkdir_line(line):
  spl=line.split()
  target=" ".join(spl[1:]) # allow a space
  target=target.replace("\ ace"," ace") # hardwire for one test
  text="""
os.mkdir('%s')
""" %(target)
  return text.splitlines()

def dump_line(line):
   return python_line(line)

def cd_line(line):
  spl=line.split()
  target=" ".join(spl[1:]) # allow a space
  target=target.replace("\ ace"," ace") # hardwire for one test
  text="""
os.chdir('%s')
""" %(target)
  return text.splitlines()

def cat_line(line):
  spl=line.split()
  assert len(spl)==2
  return ["print ( open('%s').read(), file = log)" %(spl[1])]

def cp_line(line):
  spl=line.split()
  target=" ".join(spl[2:]) # allow a space in place to copy
  target=target.replace("\ ace"," ace") # hardwire for one test
  text="""
import shutil
shutil.copyfile('%s','%s')
""" %(spl[1],target)
  return text.splitlines()

def ls_line(line):
  spl=line.split()
  assert len(spl)==3
  dir_name=spl[2]
  dir_parts=dir_name.split(os.path.sep)
  text="""
directory=os.path.sep.join(%s)
for x in os.listdir(directory):
     print (x)
"""  %(str(dir_parts))
  return text.splitlines()

def wc_line(line):
  spl=line.split()
  assert len(spl)==2
  text="""
input_text=open('%s').read()
chars=len(input_text)
lines=len(input_text.splitlines())
print (chars,lines, file = log)
""" %(spl[1])
  return text.splitlines()
def head_line(line):
  spl=line.split()
  assert len(spl)==3
  text="""
for line in open('%s').readlines()[:%s]:
  print (line, file = log)
""" %(spl[2],spl[1].replace("-",""))
  return text.splitlines()

def diff_line(line):
  # just print out the file
  spl=line.split()
  assert len(spl) in [3,4]
  text="""
import difflib
expected=open('%s').readlines()
found=open('%s').readlines()
for line in difflib.ndiff(expected,found):
  print (line, file = log)
""" %(spl[-2],spl[-1])
  return text.splitlines()

def replace_line(lines,test):
  new_lines=[]
  for line in lines:
    if not line.replace(" ",""): continue
    if line.startswith("print "):
      if  line.replace('"','').replace("'",""
         ).replace("print","").replace(" ",""):  # print line
        new_lines.append(line)
      else:
        pass # ignore empty print
    elif line.startswith("grep"):  # grep line
      new_lines+=grep_line(line)
    elif line.startswith("phenix.python"):
      new_lines+=python_line(line)
    elif line.startswith("phenix.mr_rosetta"):
      new_lines+=python_line(line)
    elif line.startswith("diff "):
      new_lines+=diff_line(line)
    elif line.startswith("head "):
      new_lines+=head_line(line)
    elif line.startswith("wc "):
      new_lines+=wc_line(line)
    elif line.startswith("cp "):
      new_lines+=cp_line(line)
    elif line.startswith("ls -1 "):
      new_lines+=ls_line(line)
    elif line.startswith("cat "):
      new_lines+=cat_line(line)
    elif line.startswith("mkdir "):
      new_lines+=mkdir_line(line)
    elif line.startswith("cd "):
      new_lines+=cd_line(line)
    elif line.startswith("phenix.mtz.dump") or line.startswith("phenix.get_cc_ano"):
      new_lines+=dump_line(line)
    elif line.split()[0].startswith("phenix."):
      new_lines+=auto_something_line(line,test)
    else:
      print("SKIPPED:",line)

  return new_lines

def replace_line_group(lines):
  new_lines=[]
  assert len(lines)>=3
  # looks like:
  #  phenix.solve<<EOD
  #   ...commmands
  #  EOD
  #  we want to run solve with these commands and put output in log_file

  if lines[0].strip().startswith("phenix.") or \
       lines[0].strip().startswith("solve_resolve"):
    cmd=lines[0].strip().replace("<<"," <<").split()[0]
    logitfile='%s.log' %(cmd)
    text="""
cmds='''%s
'''
from phenix.autosol.run_program import run_program
run_program('%s', cmds,'%s',None,None,None)
print (open('%s').read(), file = log)
""" %("\n".join(lines[1:-1]),cmd,logitfile,logitfile)

    return text.splitlines()

def is_status_check(lines):
  if len(lines)<3: return False
  if lines[0].replace(" ","")=="if($status)then" and \
     lines[-1].replace(" ","")=="endif":
    return True

def replace_if_group(lines):
  # special case: skip status checks
  if is_status_check(lines):
    return []

  text="".join(lines).replace(" ","")

  new_lines=[]
  for line in lines:
    pass # new_lines.append(" IF GROUP: "+line)
  return new_lines

def replace_foreach_group(lines):

  new_lines=[]
  for line in lines:
    new_lines.append(" FOREACH GROUP: "+line)
  return new_lines

def starts_with_triple_quote(line):

  if line.startswith("\"\"\"") or line.startswith("\'\'\'"):
    return True
  spl=line.split("=")
  if len(spl)>1 and starts_with_triple_quote(spl[1]):
    return True
  return False

def make_new_script(lines,line_groups,if_groups,switch_groups,
     foreach_groups,test=None,test_directory=None,test_group=None):

  print("\nWorking on script...\n")

  # HEADER LINES



  # Special case to skip Rosetta tests on windows platforms
  if test_group in ['command_line_rosetta_quick_tests']:
    skip_these_tests_text="""
if (not "PHENIX_ROSETTA_PATH" in os.environ) :
   print "No Rosetta installation...skipping this test...OK"
   return
"""

  elif test in ['test_parallel_autobuild','test_find_all_ligands',
    'test_long_file_names','test_backslash','test_misc_methods','test_help',
     'test_rna']:
    skip_these_tests_text=""")
if (sys.platform == "win32") :
   print ("Windows installation...skipping this test...OK")
   return
print(""
"""
  else:
    skip_these_tests_text=""


  text="""

# Auto-generated script prepared by create_py.py
#
import os,sys
print ("Test script for %s")
print (""%s)
log_file=os.path.join(os.getcwd(),"%s.output")  # always use log_file where needed
log=open(log_file,'w')  # the word log in the output script is going to be this

""" %(test,skip_these_tests_text,test)

  #BODY LINES

  new_lines=text.splitlines()

  i=-1
  from copy import deepcopy
  next_line_groups=deepcopy(line_groups)
  next_if_groups=deepcopy(if_groups)
  next_foreach_groups=deepcopy(foreach_groups)

  i=0
  while i<len(lines):
    i,line_type,next_set_of_lines,\
      next_line_groups,next_if_groups,next_foreach_groups=next_lines(
          i=i,lines=lines,
          next_line_groups=next_line_groups,
          next_if_groups=next_if_groups,
          next_foreach_groups=next_foreach_groups)
    if line_type == 'line_group':
      next_set_of_lines=replace_line_group(next_set_of_lines)
    elif line_type=='line':
      next_set_of_lines=replace_line(next_set_of_lines,test)
    elif line_type=='if_group':
      next_set_of_lines=replace_if_group(next_set_of_lines)
    elif line_type=='foreach_group':
      next_set_of_lines=replace_foreach_group(next_set_of_lines)

    else:
      print("Unknown line type")
      assert 1==0
    new_lines+=next_set_of_lines

  #FOOTER LINES
  text="""
log.close()
  """

  new_lines+=text.splitlines()

  # now make a script that allows choices...

  test_file=os.path.join(test_directory,"%s.py" %("tst_%s" %(test[5:])))
  f=open(test_file,'w')

  # Header lines for script
  print("from __future__ import division", file=f)
  print("from __future__ import print_function", file=f)
  print("""def run(args,
   write_expected_file=False,
   update_ignore_file=False):
""", file=f)
  # Body lines for script (content of run() method, indented)
  started_triple_quote=False
  first=False
  for line in new_lines:
    if starts_with_triple_quote(line):
      if started_triple_quote:
         started_triple_quote=False
      else:
         started_triple_quote=True
         first=True
    if started_triple_quote and not first:
      print("%s" %(line.rstrip()), file=f)  # no indent
    else:
      print("  %s" %(line.rstrip()), file=f)
    first=False

  # Now run check_results on this test

  print("""
  print ()
  print ('Done with test...running checks on output')
  print ()
  lines=open(log_file).readlines()
  from phenix_regression.wizards_cif.check_results import check_results
  check_results(test='%s',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print ()
  print ('OK')
""" %(test), file=f)
  # Footer lines for script

  print("""

if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards_cif.run_wizard_test import run_wizard_test
    args=['%s','%s']
    run_wizard_test(args)

""" %(test_group,test), file=f)

  f.close()

  print("New script written to %s" %(test_file))


done_list=[   # just use tst_xxx.py as is for these now (already edited)
   ['ncs_in_phenix_refine_tests','test_ncs_in_phenix_refine'],
  ]

def run(args,out=sys.stdout):

  new_args=[]
  target_test_name=None
  for arg in args:
    if len(arg.split("="))==2 and arg.split("=")[0]=='test_name':
      target_test_name=arg.split("=")[1]
    else:
      new_args.append(arg)
  args=new_args
  if not args:
    args=[os.getcwd()]

  wizard_dir=args[0]
  print("Working in %s" %(wizard_dir), file=out)
  test_groups=select_names(os.listdir(wizard_dir),text="_tests")
  test_dict={}
  for tg in test_groups:
    print(tg, file=out)
    test_dict[tg]=get_tests(wizard_dir=wizard_dir,test_group=tg)

  print("\nWorking on individual tests:", file=out)
  for tg in test_groups:
    print("\n%s" %(tg), file=out)
    for t in test_dict[tg]:
      if target_test_name and t != target_test_name: continue
      if [tg,t] in done_list: continue
      test_directory=os.path.join(wizard_dir,tg,t)
      lines,line_groups,if_groups,switch_groups,foreach_groups=set_up_test(
        test_directory=test_directory,test_group=tg,test=t,out=out)
      # lines are edited lines
      # line_groups are [start,end] of groups that make a solve<<EOD etc
      # if_groups,switch_groups,foreach_groups are start,end of line_groups that
      #   are part of an if...endif etc.
      new_lines=make_new_script(
        lines,line_groups,if_groups,switch_groups,foreach_groups,test=t,
         test_group=tg,
         test_directory=test_directory)


if __name__=="__main__":
  run(sys.argv[1:])
