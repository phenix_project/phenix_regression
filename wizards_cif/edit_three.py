from __future__ import division
import os,sys

def run(args):
  ''' Work in the path (directory) specified by args[0]. Base file name is last
  part of directory. test_xxxx/.  Use wizards/... as reference information and
   write to wizards_cif/...
  Identify all the .cif files that contain a model (or heavy atoms)
   FOR NOW: only do chains, not ligands
  Edit the cif and write to .cif.
   Replace chain names with
   long chain names, and replace residue names in hetero records with long
   residue names.
  '''
  # ID the directory to work with
  path, file_name = os.path.split(args[0])
  path_ref = os.path.join("../wizards",path)
  print("Working on %s in %s (info from %s)" %(file_name, path, path_ref))

  # Get the model and ligand files
  model_files, ligand_files = get_model_and_ligand_files(path_ref, path)
  print("Model files: %s\nLigand files: %s" %(
   " ".join(model_files)," ".join(ligand_files)))
  return
  # Identify all the model and ligand files referred to in the .com file
  from iotbx.data_manager import DataManager
  dm = DataManager()
  dm.set_overwrite(True)
  com_text = open(os.path.join(path_ref, file_name)).read()
  found_something = False
  placeholder = "XYZAB"
  for m in model_files:
    m_dot = "%s." %(m)
    com_text = com_text.replace(m_dot,placeholder)
    new_m = "%s%s" %(os.path.splitext(m)[0],".cif")
    if com_text.find(m) > -1:
      print("Model: %s" %(m))
      old_file = os.path.join(path,m)
      if os.path.isfile(old_file):
        os.remove(old_file)
      com_text = com_text.replace(m,new_m)
      mm = dm.get_model(os.path.join(path_ref,m))
      # Can modify model here
      dm.write_model_file(mm, os.path.join(path,new_m), format = 'cif')
      found_something = True
    com_text = com_text.replace(placeholder,m_dot)
  for m in ligand_files:
    m_dot = "%s." %(m)
    com_text = com_text.replace(m_dot,placeholder)
    new_m = "%s%s" %(os.path.splitext(m)[0],".cif")
    if com_text.find(m) > -1:
      print("Ligand: %s" %(m))
      old_file = os.path.join(path,m)
      if os.path.isfile(old_file):
        os.remove(old_file)
      com_text = com_text.replace(m,new_m)
      mm = dm.get_model(os.path.join(path_ref,m))
      # Can modify model here
      dm.write_model_file(mm, os.path.join(path,new_m), format = 'cif')
      found_something = True
    com_text = com_text.replace(placeholder,m_dot)

  """ SKIP
  # Write edited command file
  f = open(os.path.join(path,file_name),'w')
  print(com_text, file = f)
  f.close()

  if found_something:
    f = open(os.path.join(path,'MODIFIED'),'w')
    print("MODIFIED", file = f)
    f.close()
  """
  files = os.listdir(path)
  print("New files: %s" %(" ".join(files)))

def get_model_and_ligand_files(path_ref, path):
  files = os.listdir(path_ref)
  model_files = []
  ligand_files = []
  from iotbx.pdb.utils import get_pdb_input,get_pdb_hierarchy
  for f in files:
    p,ext = os.path.splitext(f)
    if ext != '.pdb': continue
    pdb_inp = get_pdb_input(file_name=os.path.join(path_ref,f))
    pdb_hierarchy = get_pdb_hierarchy(file_name=os.path.join(path_ref,f))
    print("ZZA",pdb_hierarchy.as_pdb_string())
    print(f,pdb_hierarchy.overall_counts().n_residues)
    if not pdb_hierarchy.overall_counts().n_residues: continue

    # Now edit this hierarchy
    for model in pdb_hierarchy.models():
      for chain in model.chains():
        if len(chain.id) < 3:
           chain.id = "%sXLONG" %(chain.id.strip())
        for residue_group in chain.residue_groups():
          for atom_group in residue_group.atom_groups():
            for atom in atom_group.atoms():
              if atom.hetero and len(chain.id) >3:
                chain.id = str(chain.id).replace("XLONG","") # XXX JUST FOR NOW
              if atom.hetero and len(atom_group.resname) <5:
                pass # JUST FOR NOW atom_group.resname = "%sXL" %(atom_group.resname)
    info = pdb_hierarchy.pdb_or_mmcif_string_info(
       target_filename = os.path.join(path,f),
       target_format = 'mmcif',
       crystal_symmetry = pdb_inp.crystal_symmetry(),
       write_file = True)
    assert os.path.join(path,f).replace(".pdb",".cif") == info.file_name
    if p.startswith("ha_") or p.find("side")>-1:
      ligand_files.append(f)
    else:
      model_files.append(f)
  return model_files, ligand_files



if __name__=="__main__":
  run(sys.argv[1:])
