from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os,sys
  print ("Test script for test_missing_data_cif")
  print ("")
  log_file=os.path.join(os.getcwd(),"test_missing_data_cif.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  local_log_file='test_missing_data_cif.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''remove_aniso=False coords.cif fobs_std.mtz n_cycle_rebuild_max=0 nbatch=1 skip_xtriage=true''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)


  cmds='''import time
time.sleep(1.)
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print (open('phenix.python.log').read(), file = log)


  local_log_file='test_missing_data_cif.log'
  local_log=open(local_log_file,'w')
  from iotbx.reflection_file_converter  import run
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''--label=Free --cns=run1_free.dump AutoBuild_run_1_/exptl_fobs_phases_freeR_flags.mtz''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)



  local_log_file='test_missing_data_cif.log'
  local_log=open(local_log_file,'w')
  from iotbx.reflection_file_converter  import run
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''--label=FP --cns=run1_fp.dump AutoBuild_run_1_/exptl_fobs_phases_freeR_flags.mtz''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)



  local_log_file='test_missing_data_cif.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''remove_aniso=False coords.cif fobs_std_edit.mtz n_cycle_rebuild_max=0 nbatch=1 skip_xtriage=true''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)


  cmds='''import time
time.sleep(1.)
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print (open('phenix.python.log').read(), file = log)


  local_log_file='test_missing_data_cif.log'
  local_log=open(local_log_file,'w')
  from iotbx.reflection_file_converter  import run
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''--label=Free --cns=run2_free.dump AutoBuild_run_2_/exptl_fobs_phases_freeR_flags.mtz''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)



  local_log_file='test_missing_data_cif.log'
  local_log=open(local_log_file,'w')
  from iotbx.reflection_file_converter  import run
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''--label=FP --cns=run2_fp.dump AutoBuild_run_2_/exptl_fobs_phases_freeR_flags.mtz''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)


  log.close()


  print ()
  print ('Done with test...running checks on output')
  print ()
  lines=open(log_file).readlines()
  from phenix_regression.wizards_cif.check_results import check_results
  check_results(test='test_missing_data_cif',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print ()
  print ('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards_cif.run_wizard_test import set_up_wizard_test
    args=['missing_data_tests_cif_tests','test_missing_data_cif']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
