#!/bin/csh  -f
#$ -cwd
#
setenv LIBTBX_FULL_TESTING
#
if ($#argv > 0) then
  if ($argv[1] == "--help") then
    echo "test multiple-model generation"
    exit 0
  endif
endif

echo " "
echo "----------------------------------------------------- "
echo "test_mult_cif: multiple-model generation"
#
#
set logname = "mult_results.list"
phenix.autobuild super_quick=True data=perfect.mtz input_labels='FP SIGFP' map_file=perfect.mtz input_map_labels='FP PHIC FOM' seq_file=seq.dat resolution=3.0 solvent_fraction=.6 model=coords.cif rebuild_in_place=True n_cycle_rebuild_min=1 n_cycle_rebuild_max=1  multiple_models=True multiple_models_group_number=1 multiple_models_number=1 start_chains_list=92> mult.log


