from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os,sys
  print ("Test script for test_omit_lig_cif")
  print ("")
  log_file=os.path.join(os.getcwd(),"test_omit_lig_cif.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  local_log_file='test_omit_lig_cif.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes(''' build_outside=False connect=False extensive_build=False fit_loops=False insert_helices=False n_cycle_build_max=1 n_cycle_rebuild_max=1 n_try_rebuild=1 ncycle_refine=1 remove_aniso=false bad_model.cif data=noligand.mtz input_labels="FP SIGFP None None None None None None FreeR_flag" maps_only=true composite_omit_type=simple_omit refine=True omit_box_pdb_list=random.cif super_quick=True resolution=4''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)



  from phenix.command_line.get_cc_mtz_pdb  import get_cc_mtz_pdb
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes
  args=split_except_quotes('''AutoBuild_run_1_/OMIT/resolve_composite_map.mtz p.cif''')
  get_cc_mtz_pdb(args=args,out=log)


  local_log_file='test_omit_lig_cif.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes(''' build_outside=False connect=False extensive_build=False fit_loops=False insert_helices=False n_cycle_build_max=1 n_cycle_rebuild_max=1 n_try_rebuild=1 ncycle_refine=1  remove_aniso=false bad_model.cif data=noligand.mtz input_labels="FP SIGFP None None None None None None FreeR_flag" maps_only=true composite_omit_type=simple_omit refine=True omit_box_pdb_list=ligand.cif super_quick=True resolution=4''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)



  from phenix.command_line.get_cc_mtz_pdb  import get_cc_mtz_pdb
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes
  args=split_except_quotes('''AutoBuild_run_1_/OMIT/resolve_composite_map.mtz p.cif''')
  get_cc_mtz_pdb(args=args,out=log)

  log.close()


  print ()
  print ('Done with test...running checks on output')
  print ()
  lines=open(log_file).readlines()
  from phenix_regression.wizards_cif.check_results import check_results
  check_results(test='test_omit_lig_cif',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print ()
  print ('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards_cif.run_wizard_test import set_up_wizard_test
    args=['omit_lig_tests_cif_tests','test_omit_lig_cif']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
