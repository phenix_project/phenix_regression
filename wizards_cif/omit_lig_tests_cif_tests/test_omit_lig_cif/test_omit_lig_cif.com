#!/bin/csh  -f
#$ -cwd
#
setenv LIBTBX_FULL_TESTING
#

phenix.autobuild  build_outside=False connect=False extensive_build=False fit_loops=False insert_helices=False n_cycle_build_max=1 n_cycle_rebuild_max=1 n_try_rebuild=1 ncycle_refine=0  super_quick=True remove_aniso=false bad_model.cif data=noligand.mtz input_labels ="FP SIGFP None None None None None None FreeR_flag" maps_only=true composite_omit_type=simple_omit refine=true omit_box_pdb_list=random.cif resolution=2.4
phenix.get_cc_mtz_pdb AutoBuild_run_1_/OMIT/resolve_composite_map.mtz p.cif
phenix.autobuild  build_outside=False connect=False extensive_build=False fit_loops=False insert_helices=False n_cycle_build_max=1 n_cycle_rebuild_max=1 n_try_rebuild=1 ncycle_refine=0  super_quick=True remove_aniso=false bad_model.cif data=noligand.mtz input_labels ="FP SIGFP None None None None None None FreeR_flag" maps_only=true composite_omit_type=simple_omit refine=True omit_box_pdb_list=ligand.cif resolution=2.4
phenix.get_cc_mtz_pdb AutoBuild_run_1_/OMIT/resolve_composite_map.mtz p.cif


