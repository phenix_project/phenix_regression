from __future__ import print_function
import sys
def get_lines(file_name):
  dd=[]
  for line in open(file_name).readlines():
    if not line: continue
    if not line.startswith("ATOM"): continue
    line=line.strip()[13:]
    dd.append(line)
  return dd
dd1=get_lines(sys.argv[1])
dd2=get_lines(sys.argv[2])
for line in dd1:
  if not line in dd2:
    print("Missing line: ",line)
  assert line in dd2
for line in dd2:
  if not line in dd1:
    print("Missing line: ",line)
  assert line in dd1
