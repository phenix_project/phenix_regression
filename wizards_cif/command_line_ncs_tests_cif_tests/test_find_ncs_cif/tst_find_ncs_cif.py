from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os,sys
  print ("Test script for test_find_ncs_cif")
  print ("")
  log_file=os.path.join(os.getcwd(),"test_find_ncs_cif.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  from phenix.command_line.find_ncs  import find_ncs
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes
  args=split_except_quotes('''helix_trans_along_z=1 helix_theta=20''')
  find_ncs(args=args,out=log)


  from phenix.command_line.find_ncs  import find_ncs
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes
  args=split_except_quotes('''coords_tet.cif coords_tet.mtz resolution=3''')
  find_ncs(args=args,out=log)


  from phenix.command_line.find_ncs  import find_ncs
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes
  args=split_except_quotes('''coords_tet.cif coords_tet.ccp4 resolution=3''')
  find_ncs(args=args,out=log)


  from phenix.command_line.find_ncs  import find_ncs
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes
  args=split_except_quotes('''ncs_in=short.ncs_spec coordinate_offset="10 10 10"''')
  find_ncs(args=args,out=log)


  local_log_file='test_find_ncs_cif.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.simple_ncs_from_pdb  import run
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''veryshort_ncs.cif write_ncs_domain_pdb=true write_spec_files=True''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)

  print ( open('veryshort_ncs_simple_ncs_from_pdb.ncs_spec').read(), file = log)

  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.python compare.py group_1.pdb group_1_expected.cif')
  for line in result.stdout_lines:
    print (line, file = log)

  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.python compare.py group_4.pdb group_4_expected.cif')
  for line in result.stdout_lines:
    print (line, file = log)

  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.python test.py')
  for line in result.stdout_lines:
    print (line, file = log)


  from phenix.command_line.find_ncs  import find_ncs
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes
  args=split_except_quotes('''helical.ncs_spec helix_extend_range=10''')
  find_ncs(args=args,out=log)

  log.close()


  print ()
  print ('Done with test...running checks on output')
  print ()
  lines=open(log_file).readlines()
  from phenix_regression.wizards_cif.check_results import check_results
  check_results(test='test_find_ncs_cif',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print ()
  print ('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards_cif.run_wizard_test import set_up_wizard_test
    args=['command_line_ncs_tests_cif_tests','test_find_ncs_cif']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
