#!/bin/csh -ef
#$ -cwd

phenix.apply_ncs ab.ncs_spec b.cif debug=True> test_apply_ncs_cif_current.log
if ( $status )then
  echo "FAILED"
  exit 1
endif
phenix.superpose_pdbs apply_ncs.cif ab.cif >> test_apply_ncs_cif_current.log
if ( $status )then
  echo "FAILED"
  exit 1
endif



