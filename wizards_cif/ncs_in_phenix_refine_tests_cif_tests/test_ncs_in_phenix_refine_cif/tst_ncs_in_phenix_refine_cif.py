from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py. EDITED FILE
  #

  print("Test script for test_ncs_in_phenix_refine_cif")
  print("")
  log_file="test_ncs_in_phenix_refine_cif.output"  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  from phenix.autosol.run_refine import run_refine_program

  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('mlt.mtz mlt_offset.cif main.number_of_macro_cycles=0 refinement.pdb_interpretation.ncs_search.enabled=True refinement.ncs.excessive_distance_limit=None --overwrite')
  run_refine_program(args)

  log.close()


  lines=open(log_file).readlines()
  from phenix_regression.wizards_cif.check_results import check_results
  check_results(test='test_ncs_in_phenix_refine_cif',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards_cif.run_wizard_test import set_up_wizard_test
    args=['ncs_in_phenix_refine_tests_cif_tests','test_ncs_in_phenix_refine_cif']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
