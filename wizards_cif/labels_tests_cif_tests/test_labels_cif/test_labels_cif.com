#!/bin/csh  -f
#$ -cwd
#
setenv LIBTBX_FULL_TESTING
#
if ($#argv > 0) then
  if ($argv[1] == "--help") then
    echo "test labels for autobuild"
    exit 0
  endif
endif

echo " "
echo "----------------------------------------------------- "
echo "test_labels_cif: test labels for autobuild"
echo "run1 :  run autobuild 0 cycles but get map refinement and hires files"
#
phenix.autobuild super_quick=True a2u_short.cif data=a2u-data.mtz \
   input_map_file=a2u-sigmaa.mtz hires_file=a2u.sca \
   refinement_file=a2u-data.mtz debug=True  \
   number_of_parallel_models=1 n_cycle_rebuild_max=0 \
   input_map_labels=FWT,PHIC 
#
phenix.autobuild super_quick=True show_facts 
####
echo "run2 :  specify labels for hires and refinement this time..."
#
phenix.autobuild super_quick=True a2u_short.cif data=a2u-data.mtz \
   input_map_file=a2u-sigmaa.mtz hires_file=a2u.sca \
   refinement_file=a2u-data.mtz debug=True  \
   number_of_parallel_models=1 n_cycle_rebuild_max=0 \
   input_map_labels=FWT,PHIC \
   input_hires_labels=I,SIGI \
   input_refinement_labels='F-obs SIGF-obs R-free-flags'
#
phenix.autobuild super_quick=True show_facts 
echo "run3 :  specify different labels this time..."
#
phenix.autobuild super_quick=True a2u_short.cif data=a2u-data.mtz \
   input_map_file=a2u-sigmaa.mtz hires_file=a2u.sca \
   refinement_file=a2u-data.mtz debug=True  \
   number_of_parallel_models=1 n_cycle_rebuild_max=0 \
   input_map_labels=FWT,PHIC,FOM \
   input_hires_labels=I,SIGI \
   input_refinement_labels='F-obs SIGF-obs' 
#
phenix.autobuild super_quick=True show_facts 


