from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  os.environ['PHENIX_OVERWRITE_ALL'] = '1'
  print ("Test script for test_assign_sequence_1_cif")
  print ("")
  log_file=os.path.join(os.getcwd(),"test_assign_sequence_1_cif.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  from iotbx.cli_parser import run_program
  from phenix.programs import assign_sequence as run

  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes
  args=split_except_quotes('''cycle_2.mtz model_1.cif seq_file=sequence_autosol.dat labin="FP=FP SIGFP=SIGFP PHIB=PHIM FOM=FOMM" debug=true''')
  run_program(program_class=run.Program, args = args, logger=log)

  log.close()


  print ()
  print ('Done with test...running checks on output')
  print ()
  lines=open(log_file).readlines()
  from phenix_regression.wizards_cif.check_results import check_results
  check_results(test='test_assign_sequence_1_cif',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print ()
  print ('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards_cif.run_wizard_test import set_up_wizard_test
    args=['command_line_tests_cif_tests','test_assign_sequence_1_cif']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
