from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print ("Test script for test_replace_side_chains_5_cif")
  print ("")
  log_file=os.path.join(os.getcwd(),"test_replace_side_chains_5_cif.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  from phenix.command_line.replace_side_chains  import replace_side_chains
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes
  args=split_except_quotes('''use_any_side=False sequence_autosol.dat arr_1.cif cycle_2.mtz debug=true remove_clashes=True''')
  replace_side_chains(args=args,out=log)

  input_text=open('replace_side_chains.cif').read()
  chars=len(input_text)
  lines=len(input_text.splitlines())
  print (chars,lines, file = log)

  log.close()


  print ()
  print ('Done with test...running checks on output')
  print ()
  lines=open(log_file).readlines()
  from phenix_regression.wizards_cif.check_results import check_results
  check_results(test='test_replace_side_chains_5_cif',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print ()
  print ('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards_cif.run_wizard_test import set_up_wizard_test
    args=['command_line_tests_cif_tests','test_replace_side_chains_5_cif']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
