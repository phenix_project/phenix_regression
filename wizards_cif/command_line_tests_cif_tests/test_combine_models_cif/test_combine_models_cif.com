#!/bin/csh -ef
#$ -cwd

phenix.combine_models working.ccp4 merge_by_segment_correlation=True merge_remainder=False pdb_out=out.pdb working.cif ncs_file=working.ncs dump_remainder=true verbose=true in_cell_only=False resolution=2.85
if ( $status )then
  echo "FAILED"
  exit 1
endif
phenix.get_cc_mtz_pdb target.mtz out.cif

phenix.combine_models working_box.ccp4 merge_by_segment_correlation=True merge_remainder=False pdb_out=out.pdb working_box.cif ncs_file=working_box.ncs_spec dump_remainder=true verbose=true in_cell_only=False resolution=2.85
if ( $status )then
  echo "FAILED"
  exit 1
endif
phenix.get_cc_mtz_pdb target.mtz out.cif


phenix.combine_models working.ccp4 merge_by_segment_correlation=True merge_remainder=False pdb_out=out.pdb working_ncs.cif ncs_file=working.ncs dump_remainder=true verbose=true in_cell_only=False resolution=2.85
if ( $status )then
  echo "FAILED"
  exit 1
endif
phenix.get_cc_mtz_pdb target.mtz out.cif

phenix.combine_models working_box.ccp4 merge_by_segment_correlation=True merge_remainder=False pdb_out=out.pdb working_box_ncs.cif ncs_file=working_box.ncs_spec dump_remainder=true verbose=true in_cell_only=False resolution=2.85
if ( $status )then
  echo "FAILED"
  exit 1
endif

phenix.combine_models working.ccp4 merge_by_segment_correlation=False merge_remainder=False pdb_out=out.pdb working_ncs.cif ncs_file=working.ncs dump_remainder=true verbose=true in_cell_only=False resolution=2.85
if ( $status )then
  echo "FAILED"
  exit 1
endif
phenix.get_cc_mtz_pdb target.mtz out.cif

phenix.combine_models working_box.ccp4 merge_by_segment_correlation=False merge_remainder=False pdb_out=out.pdb working_box_ncs.cif ncs_file=working_box.ncs_spec dump_remainder=true verbose=true in_cell_only=False resolution=2.85
if ( $status )then
  echo "FAILED"
  exit 1
endif


rm -f combine_models.cif
phenix.combine_models working.mtz merge_by_segment_correlation=True working.cif 
if ( $status )then
  echo "FAILED"
  exit 1
endif
if (! -f combine_models.cif)then
  echo "FAILED"
  exit 1
endif
wc combine_models.cif

phenix.combine_models combine_models.eff
if ( $status )then
  echo "FAILED"
  exit 1
endif

wc combine_models.cif

phenix.combine_models combine_rna_protein.eff
if ( $status )then
  echo "FAILED"
  exit 1
endif
wc combine_models.cif

phenix.combine_models working.mtz merge_by_segment_correlation=True merge_remainder=False pdb_out=out.pdb working.cif ncs_file=working.ncs dump_remainder=true verbose=true in_cell_only=False
if ( $status )then
  echo "FAILED"
  exit 1
endif
phenix.get_cc_mtz_pdb target.mtz out.cif

phenix.combine_models working.mtz merge_by_segment_correlation=True merge_remainder=False pdb_out=out.pdb working.cif ncs_file=working.ncs dump_remainder=true verbose=true in_cell_only=True
if ( $status )then
  echo "FAILED"
  exit 1
endif
phenix.get_cc_mtz_pdb target.mtz out.cif


