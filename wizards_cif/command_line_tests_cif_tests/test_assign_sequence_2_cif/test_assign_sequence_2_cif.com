#!/bin/csh -ef
#$ -cwd
# now test with bad sequence file
phenix.assign_sequence cycle_2.mtz model_1.cif seq_file=bad_seq.dat labin="FP=FP SIGFP=SIGFP PHIB=PHIM FOM=FOMM" debug=true
if ( $status )then
  echo "FAILED"
  exit 1
endif
echo "OK"



