#!/bin/csh -f
echo "Running autosol with sad data and build=false (output in autosol.log)"

phenix.autosol seq_file="hewl.seq" unit_cell="78.239 78.239 37.281 90 90 90" space_group="P 43 21 2" data="lyso2001.sca"  atom_type="S" lambda=1.5418 input_partpdb_file="MR.1.pdb" partpdb_rms=1 build=False > autosol.log

echo "Running autobuild maps_only with phaser_1.mtz FP, HLcoeffs and FWT/PHIFWT"
echo " (output in autobuild.log)"

phenix.autobuild maps_only=True data=AutoSol_run_1_/phaser_1.mtz input_map_file=AutoSol_run_1_/phaser_1.mtz input_map_labels="FWT PHIFWT" seq_file=hewl.seq map_file_fom=0.45 ha_file=AutoSol_run_1_/ha_1.pdb_formatted.pdb > autobuild.log

echo "Getting correlation of autosol density modified map (AutoSol_run_1_/resolve_1.mtz)"
echo " and maps_only density-modified map (AutoBuild_run_1_/cycle_best_1.mtz)"
echo "Result should be near 1.0"
phenix.get_cc_mtz_mtz AutoSol_run_1_/resolve_1.mtz AutoBuild_run_1_/cycle_best_1.mtz|grep offsetting

echo "All done"
