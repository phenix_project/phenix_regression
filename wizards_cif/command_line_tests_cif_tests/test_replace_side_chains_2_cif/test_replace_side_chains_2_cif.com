#!/bin/csh -f
#$ -cwd


echo "Testing sequence_from_density..."
phenix.replace_side_chains sequence_from_density=True sequence_autosol.dat arr_1.cif cycle_2.mtz debug=true remove_clashes=False pdb_out=sequence_from_density.pdb
head -100 sequence_from_density.cif


