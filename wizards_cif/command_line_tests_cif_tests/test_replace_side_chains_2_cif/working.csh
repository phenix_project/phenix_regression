#!/bin/csh -f
phenix.combine_models working.mtz merge_by_segment_correlation=True merge_remainder=False pdb_out=out.pdb working.pdb ncs_file=working.ncs dump_remainder=true verbose=true remainder_in_cell_only=False
phenix.get_cc_mtz_pdb target.mtz remainder.pdb | grep Correl
