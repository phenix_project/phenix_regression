#!/bin/csh -ef
#$ -cwd

phenix.morph_model run.eff

phenix.morph_model coords.cif native.sca cycles=1 debug=true refinement_params=refine.eff



