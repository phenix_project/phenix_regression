#!/bin/csh -f
#$ -cwd

echo "Testing reassign_sequence..."

phenix.replace_side_chains reassign_sequence=True sequence_autosol.dat arr_1.cif cycle_2.mtz debug=true remove_clashes=False pdb_out=reassign.cif 
cat reassign.cif



