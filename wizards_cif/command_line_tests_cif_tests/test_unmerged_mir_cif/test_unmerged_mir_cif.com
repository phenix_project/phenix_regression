#!/bin/csh -ef
#$ -cwd

phenix.autosol trace_chain=False chain_type=PROTEIN expt_type=sir native.data=unmerged.mtz deriv.data=unmerged.mtz space_group=p622 debug=True resolution=7 sites_file=ha_perfect.cif build=False have_hand=True ncs_copies=1 residues=200 solvent_fraction=0.50 quick=true remove_aniso=False 

phenix.autosol trace_chain=False chain_type=PROTEIN expt_type=sir native.data=unmerged.mtz deriv.data=unmerged.mtz space_group=p622 debug=True resolution=7 sites_file=ha_perfect.cif build=False have_hand=True ncs_copies=1 residues=200 solvent_fraction=0.50 quick=true remove_aniso=True

phenix.autosol trace_chain=False chain_type=PROTEIN expt_type=sad data=unmerged.mtz space_group=p622 debug=True resolution=7 sites_file=ha_perfect.cif build=False have_hand=True ncs_copies=1 residues=200 solvent_fraction=0.50 quick=true remove_aniso=False 

phenix.autosol trace_chain=False chain_type=PROTEIN expt_type=sad data=unmerged.mtz space_group=p622 debug=True resolution=7 sites_file=ha_perfect.cif build=False have_hand=True ncs_copies=1 residues=200 solvent_fraction=0.50 quick=true remove_aniso=True


