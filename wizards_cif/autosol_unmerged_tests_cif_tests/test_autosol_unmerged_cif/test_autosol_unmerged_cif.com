#!/bin/csh  -f
#$ -cwd
#
setenv LIBTBX_FULL_TESTING
#
if ($#argv > 0) then
  if ($argv[1] == "--help") then
    echo "test autosol_unmerged maps "
    exit 0
  endif
endif

echo " "
echo "----------------------------------------------------- "
echo "test_autosol_unmerged_cif: test autosol_unmerged maps"
#
#
echo "testing autosol unmerged with p9 sca data"
phenix.autosol quick=True ha_iteration=false model_ha_iteration=false add_classic_denmod=False chain_type=PROTEIN p9_se_w2.sca debug=True resolution=3.5 sites_file= p9_ha.cif build=false have_hand=True remove_aniso=False test_remove_aniso=False unit_cell="113.949 113.949  32.474 90.000  90.000  90.00" space_group=I4 residues=200 solvent_fraction=None ncs_copies=1

echo "testing autosol unmerged with remove_aniso=True and mtz data"
phenix.autosol quick=True ha_iteration=false model_ha_iteration=false add_classic_denmod=False  chain_type=PROTEIN unmerged.mtz space_group=p622 debug=True resolution=3.5 sites_file= ha_perfect_2.cif build=false have_hand=True remove_aniso=True test_remove_aniso=False ncs_copies=1 residues=200 solvent_fraction=None 

echo "testing autosol unmerged with remove_aniso=False and mtz data"
phenix.autosol quick=True ha_iteration=false model_ha_iteration=false add_classic_denmod=False  chain_type=PROTEIN unmerged.mtz space_group=p622 debug=True resolution=3.5 sites_file= ha_perfect_2.cif build=false have_hand=True remove_aniso=False test_remove_aniso=False ncs_copies=1 residues=200 solvent_fraction=None 


