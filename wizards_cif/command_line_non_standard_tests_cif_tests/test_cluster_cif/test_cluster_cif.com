#!/bin/csh -ef
#$ -cwd
echo "Clusters as cluster_pdb_file"
phenix.autosol fom_for_extreme_dm=0.01 ncycle_refine=1 resolution=5 coords.pdb.mtz seq.dat build=false quick=true mask_cycles=1 minor_cycles=1 lambda=.9792 skip_xtriage=True remove_aniso=false cluster_pdb_file=ha.cif sites_file=ha_1.cif have_hand=True atom_type=XX ha_iteration=False add_classic_denmod=False > run_cluster.log
tail -500 run_cluster.log
if ( $status )then
  echo "FAILED"
  exit 1
endif



