from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os,sys
  print ("Test script for test_change_sg_cif")
  print ("")
  log_file=os.path.join(os.getcwd(),"test_change_sg_cif.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  local_log_file='test_change_sg_cif.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.automr  import run_automr
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''rebuild_after_mr=False coords=coords.cif data=native.mtz RMS=0.85 seq_file=seq.dat copies=1 start_chains_list=92 n_cycle_rebuild_max=1 nbatch=1 input_refinement_file=refine.mtz use_all_plausible_sg=True include_input_model=False rebuild_in_place=True use_tncs=False''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_automr(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)


  cmds='''from phenix.autosol.run_program import run_program
temp_dir='AutoBuild_run_1_'
for x in ['aniso_data_PHX.mtz','cycle_best_2.mtz','cycle_best_refine_map_coeffs_2.mtz','exptl_fobs_phases_freeR_flags.mtz','exptl_phases_and_amplitudes_for_density_modification_aniso.mtz','overall_best_denmod_map_coeffs.mtz','overall_best_final_refine_001.mtz','overall_best_refine_data.mtz','overall_best_refine_map_coeffs.mtz','refinement_PHX.mtz','working_best_denmod_map_coeffs.mtz','working_best_refine_map_coeffs.mtz']:
xx=os.path.join(temp_dir,x)
cmds="%s" %(xx)
run_program('phenix.mtz.dump', cmds,None,None,None,None)
for xx in ['native.mtz','refine.mtz']:
cmds="%s" %(xx)
run_program('phenix.mtz.dump', cmds,None,None,None,None)
for x in ['MR.1_ed.pdb','cycle_best_2.pdb','edited_pdb.pdb','overall_best.pdb','overall_best_final_refine_001.pdb','overall_best_placed.pdb','working_best.pdb','working_best_placed.pdb']:
for line in open(os.path.join(temp_dir,x)).readlines():
if line.startwith('CRYST1'): print line
for xx in ['coords.cif']:
for line in open(xx).readlines():
if line.startwith('CRYST1'): print line
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print (open('phenix.python.log').read(), file = log)

  log.close()


  print ()
  print ('Done with test...running checks on output')
  print ()
  lines=open(log_file).readlines()
  from phenix_regression.wizards_cif.check_results import check_results
  check_results(test='test_change_sg_cif',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print ()
  print ('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards_cif.run_wizard_test import set_up_wizard_test
    args=['command_line_non_standard_tests_cif_tests','test_change_sg_cif']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
