#!/bin/csh -ef
phenix.autosol resolution=2.49 lam1_p2122.sca seq.dat 2 se build=false space_group=p2122 quick=True remove_aniso=False skip_xtriage=true ha_sites_file=sites.cif 
phenix.autobuild quick=True generate_hl_if_missing=true resolve.mtz coords.cif seq.dat number_of_parallel_models=1 quick=True  resolution=3.0


