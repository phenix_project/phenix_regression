#!/bin/csh -ef
#$ -cwd
echo ""
echo "Se with partial occ and part_map_coeffs"
phenix.autosol coords.pdb.mtz seq.dat part_map_coeffs_file= no_ha.pdb.mtz partpdb_rms=1.5 build=false quick=true mask_cycles=1 minor_cycles=1 atom_type=SE lambda=.9792 skip_xtriage=True remove_aniso=false input_part_map_coeffs_labels=FC,PHIFC phaser_sites_then_phase=False 



