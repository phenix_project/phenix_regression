from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os,sys
  print ("Test script for test_cluster_1_cif")
  from phenix.command_line.autosol  import run_autosol
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''fom_for_extreme_dm=0.01 ncycle_refine=1 resolution=5 coords.pdb.mtz seq.dat build=false quick=true mask_cycles=1 minor_cycles=1 lambda=.9792 skip_xtriage=True remove_aniso=false atom_type=TX sites_file=ha_1.cif have_hand=True ha_iteration=False add_classic_denmod=False build=False phase_only=True''')
  run_autosol(args=args)

if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards_cif.run_wizard_test import set_up_wizard_test
    args=['command_line_non_standard_tests_cif_tests','test_cluster_1_cif']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
