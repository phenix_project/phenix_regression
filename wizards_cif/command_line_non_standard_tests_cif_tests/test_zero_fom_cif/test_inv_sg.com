#!/bin/csh -ef
#$ -cwd
phenix.autosol sites=2 lam1_p31.sca quick=true seq.dat remove_aniso=False skip_xtriage=true debug=true clean_up=False >  tmp.dat
if ( $status )then
  echo "FAILED"
  exit 1
endif
cat tmp.dat|grep -v "R/Rfree   "|grep -vi placed|grep -vi built |grep -vi xyz|grep -vi "CC :"|grep -v "Overall model-map correlation :"|grep -v "(R/Rfree ="|grep -v "CC:"|grep -v "Overall model-map correlation:"
cd AutoSol_run_1_/TEMP0
foreach file (*.mtz)
 echo " `phenix.mtz.dump $file |grep 'Space group symbol from file'` $file"
end
grep CRYST1 *.pdb

