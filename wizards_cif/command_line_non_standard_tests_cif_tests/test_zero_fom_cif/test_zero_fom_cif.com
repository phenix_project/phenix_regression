#!/bin/csh -ef
#$ -cwd
phenix.autobuild exptl_fobs_phases_freeR_flags.mtz coords.cif maps_only=true refine=false skip_xtriage=true |tee test.log
if ( $status )then
  echo "FAILED"
  exit 1
endif
phenix.python<<EOD
text=open('test.log').read()
if text.find("FP=FP SIGFP=SIGFP PHIB=PHIM FreeR_flag=FreeR_flag")>-1:
  import sys
  print "FAILED TEST (PHIM READ IN THOUGH ALL 0 or -90)"
  sys.exit(1)
EOD



