from __future__ import division
import os,sys

def run(args):
  ''' Work in the path (directory) specified by args[0]. Base file name is last
  part of directory. test_xxxx/.  Use wizards/... as reference information and
   write to wizards_cif/...
  Identify all the PDB files (or ha files or side.pdb files) that
   are referred to test_xxxx.com and that exist.
  Edit the .com file to replace all these names with .cif
  Edit the pdb files to be cif and write to .cif.  Later,
   Replace chain names with
   long chain names, and replace residue names in hetero records with long
   residue names.  Replace
  '''
  # ID the directory to work with
  path, file_name = os.path.split(args[0])
  path_ref = os.path.join("../wizards",path)
  print("Working on %s in %s (info from %s)" %(file_name, path, path_ref))

  # Get the model and ligand files
  model_files, ligand_files = get_model_and_ligand_files(path_ref)
  print("Model files: %s\nLigand files: %s" %(
   " ".join(model_files)," ".join(ligand_files)))

  # Identify all the model and ligand files referred to in the .com file
  from iotbx.data_manager import DataManager
  dm = DataManager()
  dm.set_overwrite(True)
  com_text = open(os.path.join(path_ref, file_name)).read()
  found_something = False
  placeholder = "XYZAB"
  for m in model_files:
    m_dot = "%s." %(m)
    com_text = com_text.replace(m_dot,placeholder)
    new_m = "%s%s" %(os.path.splitext(m)[0],".cif")
    if com_text.find(m) > -1:
      print("Model: %s" %(m))
      old_file = os.path.join(path,m)
      if os.path.isfile(old_file):
        os.remove(old_file)
      com_text = com_text.replace(m,new_m)
      mm = dm.get_model(os.path.join(path_ref,m))
      # Can modify model here
      dm.write_model_file(mm, os.path.join(path,new_m), format = 'cif')
      found_something = True
    com_text = com_text.replace(placeholder,m_dot)
  for m in ligand_files:
    m_dot = "%s." %(m)
    com_text = com_text.replace(m_dot,placeholder)
    new_m = "%s%s" %(os.path.splitext(m)[0],".cif")
    if com_text.find(m) > -1:
      print("Ligand: %s" %(m))
      old_file = os.path.join(path,m)
      if os.path.isfile(old_file):
        os.remove(old_file)
      com_text = com_text.replace(m,new_m)
      mm = dm.get_model(os.path.join(path_ref,m))
      # Can modify model here
      dm.write_model_file(mm, os.path.join(path,new_m), format = 'cif')
      found_something = True
    com_text = com_text.replace(placeholder,m_dot)

  # Write edited command file
  f = open(os.path.join(path,file_name),'w')
  print(com_text, file = f)
  f.close()

  if found_something:
    f = open(os.path.join(path,'MODIFIED'),'w')
    print("MODIFIED", file = f)
    f.close()
  files = os.listdir(path)
  print("New files: %s" %(" ".join(files)))

def get_model_and_ligand_files(path_ref):
  files = os.listdir(path_ref)
  model_files = []
  ligand_files = []
  for f in files:
    p,ext = os.path.splitext(f)
    if ext != '.pdb': continue
    if p.startswith("ha_") or p.find("side")>-1:
      ligand_files.append(f)
    else:
      model_files.append(f)
  return model_files, ligand_files



if __name__=="__main__":
  run(sys.argv[1:])
