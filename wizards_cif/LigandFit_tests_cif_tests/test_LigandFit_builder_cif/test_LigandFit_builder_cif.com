#!/bin/csh -f
setenv LIBTBX_FULL_TESTING
phenix.ligandfit data=perfect.mtz model=partial.cif \
   ligand=smiles.dat resolution=3.0 n_group_search=1 ligand_cc_min=0.5 \
   input_labels="FP PHIC FOM"


