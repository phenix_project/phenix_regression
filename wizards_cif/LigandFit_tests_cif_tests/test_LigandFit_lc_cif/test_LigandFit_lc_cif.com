#!/bin/csh -f
phenix.ligandfit quick=True data=perfect.mtz model=test_ncs_lc.cif \
   ligand=side.cif resolution=3.0 n_group_search=1 ligand_cc_min=0.5 \
   ligand_near_chain="aXLONG" ligand_near_res=16 input_labels="FP PHIC FOM"
 


