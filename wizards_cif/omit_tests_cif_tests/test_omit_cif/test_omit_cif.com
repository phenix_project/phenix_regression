#!/bin/csh  -f
#$ -cwd
#
setenv LIBTBX_FULL_TESTING

phenix.autobuild super_quick=True data=perfect.mtz input_labels='FP SIGFP' seq_file=seq.dat resolution=3.0 solvent_fraction=.6 model=coords.cif rebuild_in_place=True n_cycle_rebuild_min=1 n_cycle_rebuild_max=1  composite_omit_type=iterative_build_omit number_of_parallel_models=1 input_lig_file_list=ha.cif start_chains_list=92 omit_box_start=3 omit_box_end=3


