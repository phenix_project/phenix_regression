from __future__ import print_function

# amplitude_vs_resolution.py
# read in map coefficients or amplitudes and plot mean amplitude vs resolution
#

import sys,os
from libtbx.utils import Sorry
from cctbx.array_family import flex


def get_b_iso(miller_array):
  from mmtbx.scaling import absolute_scaling
  aniso_scale_and_b=absolute_scaling.ml_aniso_absolute_scaling(
      miller_array=miller_array, n_residues=200, n_bases=0)
  try: b_cart=aniso_scale_and_b.b_cart
  except: b_cart=[0,0,0]
  b_aniso_mean=0.
  for k in [0,1,2]:
    b_aniso_mean+=b_cart[k]
  return b_aniso_mean/3.0

def get_array(file_name=None,labels=None):

  print("Reading from %s" %(file_name))
  from iotbx import reflection_file_reader
  reflection_file = reflection_file_reader.any_reflection_file(
       file_name=file_name)
  array_to_use=None
  if labels:
    for array in reflection_file.as_miller_arrays():
      if ",".join(array.info().labels)==labels:
        array_to_use=array
        break
  else:
    for array in reflection_file.as_miller_arrays():
      if array.is_complex_array() or array.is_xray_amplitude_array() or\
          array.is_xray_intensity_array():
        array_to_use=array
        break
  if not array_to_use:
    text=""
    for array in reflection_file.as_miller_arrays():
      text+=" %s " %(",".join(array.info().labels))

    raise Sorry("Cannot identify array to use...possibilities: %s" %(text))

  print("Using the array %s" %(",".join(array_to_use.info().labels)))
  return array_to_use

def run_apply_b_iso(amplitudes,b_iso=None):
    b_iso_orig=get_b_iso(amplitudes)
    if b_iso is None: b_iso=b_iso_orig

    b_sharpen=b_iso_orig-b_iso # positive means sharpen
    b_cart_aniso_removed=[ b_sharpen, b_sharpen, b_sharpen, 0, 0, 0]
    from mmtbx.scaling import absolute_scaling
    from cctbx import adptbx
    u_star_aniso_removed=adptbx.u_cart_as_u_star(
      amplitudes.unit_cell(), adptbx.b_as_u( b_cart_aniso_removed  ) )
    amplitudes_sharpened=absolute_scaling.anisotropic_correction(
      amplitudes,0.0,u_star_aniso_removed,must_be_greater_than=-0.0001)
    return b_iso,amplitudes_sharpened

def run(args):
  if not args or 'help' in args or '--help' in args:
    print("\namplitude_vs_resolution.py")
    print("Read in map coefficients or amplitudes and plot mean amplitude "+\
       "vs resolution")
    print("\nphenix.python amplitude_vs_resolution.py my_mtz.mtz\n")
    return

  new_args=[]
  file_list=[]
  apply_b_iso=None
  for arg in args:
    if os.path.isfile(arg) and arg.endswith(".mtz"):
      file_list.append(arg)
    elif arg=='apply_b_iso':
      apply_b_iso=True
    else:
      new_args.append(arg)
  args=new_args
  n_bins=20
  labels=None

  if len(args)>0:
    labels=args[0]
  if len(args)>1:
    n_bins=int(args[1])

  array_list=[]
  mean_f_list=[]
  bin_d_min_list=[]
  d_min=None
  d_max=None
  b_iso=None
  for file_name in file_list:
    array_list.append(get_array(file_name=file_name,labels=labels))
    array=array_list[-1]
    if array.is_complex_array():
      amplitudes=array.as_amplitude_array()
    elif array.is_xray_amplitude_array():
      amplitudes=array
    elif array.is_xray_intensity_array():
      amplitudes=array.as_x_ray_amplitude_array()
    else:
      raise Sorry("Type of array must be complex, amplitudes or intensities")

    # optionally sharpen and remove aniso
    print("B-iso for %s: %7.2f" %(file_name,get_b_iso(amplitudes)))
    if apply_b_iso:
      b_iso,amplitudes=run_apply_b_iso(amplitudes,b_iso=b_iso)
      print("New B-iso for %s: %7.2f" %(file_name,get_b_iso(amplitudes)))

    # Get mean amplitude vs resolution
    if not d_max and not d_min:
       (d_max,d_min)=array.d_max_min()
    ma=amplitudes
    ma.setup_binner(n_bins=n_bins,d_max=d_max,d_min=d_min)
    binner=ma.binner()
    mean_f=[]
    bin_d_min=[]
    for bin in list(binner.range_used())+[None]:
      if bin is None:
        sele=flex.bool(ma.indices().size(), True)
        bin_d_min.append(bin_d_min[-1])
      else:
        bin_d_min.append(binner.bin_d_min(bin))
        sele=ma.binner().selection(bin)
      mean_f.append(ma.select(sele).data().min_max_mean().mean)
    mean_f_list.append(mean_f)
    bin_d_min_list.append(bin_d_min)

  for f in file_list:
    print(" %20s " %(f), end=' ')
  print()
  print(" Dmin  ", end=' ')
  for f in file_list:
    print("  meanF ", end=' ')
  print()

  f_dict={}
  d_min_dict={}
  n_bin=None
  for f,bin_d_min,mean_f in zip(file_list,bin_d_min_list,mean_f_list):
    for i in range(n_bins):
      if not i in f_dict.keys():
        f_dict[i]={}
        d_min_dict[i]={}
      f_dict[i][f]=mean_f[i]
      d_min_dict[i][f]=bin_d_min[i]

  for i in range(n_bins):
    f=file_list[0]
    print("%6.2f " %(d_min_dict[i][f]), end=' ')
    for f in file_list:
      import math
      x=math.log10(f_dict[i][f])
      print(" %8.2f" %(x), end=' ')
    print()




if __name__=="__main__":
  run(sys.argv[1:])
