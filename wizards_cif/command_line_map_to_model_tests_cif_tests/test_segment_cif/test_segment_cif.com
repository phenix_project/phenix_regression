#!/bin/csh -ef
#$ -cwd

phenix.segment_and_split_map  solvent_content=0.856 b_blur_hires=None soft_mask=False get_half_height_width=False region_weight=20 discard_if_worse=False auto_sharpen=false veryshort.ccp4 veryshort.seq chain_type=RNA resolution=3.8 b_iso=-100 k_sharpen=0 d_min_ratio=0.6 

phenix.map_to_structure_factors segmented_maps/shifted_sharpened_map.ccp4 d_min=2.4 

phenix.python amplitude_vs_resolution.py map_to_structure_factors.mtz 


phenix.segment_and_split_map  solvent_content=0.856  b_blur_hires=None soft_mask=False get_half_height_width=False region_weight=20 discard_if_worse=False auto_sharpen=false veryshort.ccp4 veryshort.seq chain_type=RNA resolution=3.8 b_iso=-100 k_sharpen=1000 d_min_ratio=0.6 

phenix.map_to_structure_factors segmented_maps/shifted_sharpened_map.ccp4 d_min=2.4

phenix.python amplitude_vs_resolution.py map_to_structure_factors.mtz



phenix.segment_and_split_map  b_blur_hires=None soft_mask=False get_half_height_width=False region_weight=20 discard_if_worse=False auto_sharpen=false helix.ccp4 helix.seq helix.ncs_spec resolution=2.8

phenix.segment_and_split_map  b_blur_hires=None soft_mask=False get_half_height_width=False region_weight=20 discard_if_worse=False auto_sharpen=false veryshort.ccp4 veryshort.seq chain_type=RNA resolution=2.8

phenix.segment_and_split_map  b_blur_hires=None soft_mask=False get_half_height_width=False region_weight=20 discard_if_worse=False auto_sharpen=false model_map.ccp4 resolution=2.8 model_map.seq resolution=2.8

phenix.map_box model_map.ccp4 density_select=True resolution=2.8 keep_origin=False

phenix.map_box model_map.ccp4 density_select=True model_map.cif resolution=2.8 keep_origin=False


