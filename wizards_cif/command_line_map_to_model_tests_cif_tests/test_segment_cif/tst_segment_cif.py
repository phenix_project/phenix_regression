from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print ("Test script for test_segment_cif")
  print ("")
  log_file=os.path.join(os.getcwd(),"test_segment_cif.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  from cctbx.maptbx.segment_and_split_map  import run
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes
  args=split_except_quotes('''solvent_content=0.856 b_blur_hires=None soft_mask=False get_half_height_width=False region_weight=20 discard_if_worse=False auto_sharpen=false veryshort.ccp4 veryshort.seq chain_type=RNA resolution=3.8 b_iso=-100 k_sharpen=0 d_min_ratio=0.6''')
  run(args=args,out=log)


  from mmtbx.command_line.map_to_structure_factors  import run
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes
  args=split_except_quotes('''segmented_maps/shifted_sharpened_map.ccp4 d_min=2.4''')
  run(args=args,log=log)

  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.python amplitude_vs_resolution.py map_to_structure_factors.mtz')
  for line in result.stdout_lines:
    print (line, file = log)


  from cctbx.maptbx.segment_and_split_map  import run
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes
  args=split_except_quotes('''solvent_content=0.856 b_blur_hires=None soft_mask=False get_half_height_width=False region_weight=20 discard_if_worse=False auto_sharpen=false veryshort.ccp4 veryshort.seq chain_type=RNA resolution=3.8 b_iso=-100 k_sharpen=1000 d_min_ratio=0.6''')
  run(args=args,out=log)


  from mmtbx.command_line.map_to_structure_factors  import run
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes
  args=split_except_quotes('''segmented_maps/shifted_sharpened_map.ccp4 d_min=2.4''')
  run(args=args,log=log)

  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.python amplitude_vs_resolution.py map_to_structure_factors.mtz')
  for line in result.stdout_lines:
    print (line, file = log)


  from cctbx.maptbx.segment_and_split_map  import run
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes
  args=split_except_quotes('''b_blur_hires=None soft_mask=False get_half_height_width=False region_weight=20 discard_if_worse=False auto_sharpen=false helix.ccp4 helix.seq helix.ncs_spec resolution=2.8''')
  run(args=args,out=log)


  from cctbx.maptbx.segment_and_split_map  import run
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes
  args=split_except_quotes('''b_blur_hires=None soft_mask=False get_half_height_width=False region_weight=20 discard_if_worse=False auto_sharpen=false veryshort.ccp4 veryshort.seq chain_type=RNA resolution=2.8''')
  run(args=args,out=log)


  from cctbx.maptbx.segment_and_split_map  import run
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes
  args=split_except_quotes('''b_blur_hires=None soft_mask=False get_half_height_width=False region_weight=20 discard_if_worse=False auto_sharpen=false model_map.ccp4 resolution=2.8 model_map.seq resolution=2.8''')
  run(args=args,out=log)


  from mmtbx.command_line.map_box  import run
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes
  args=split_except_quotes('''model_map.ccp4 density_select=True resolution=2.8 keep_origin=False''')
  run(args=args,log=log)


  from mmtbx.command_line.map_box  import run
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes
  args=split_except_quotes('''model_map.ccp4 density_select=True model_map.cif resolution=2.8 keep_origin=False''')
  run(args=args,log=log)

  log.close()


  print ()
  print ('Done with test...running checks on output')
  print ()
  lines=open(log_file).readlines()
  from phenix_regression.wizards_cif.check_results import check_results
  check_results(test='test_segment_cif',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print ()
  print ('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards_cif.run_wizard_test import set_up_wizard_test
    args=['command_line_map_to_model_tests_cif_tests','test_segment_cif']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
