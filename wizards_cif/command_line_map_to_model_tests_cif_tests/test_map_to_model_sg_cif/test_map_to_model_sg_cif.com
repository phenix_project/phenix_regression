#!/bin/csh -ef
#$ -cwd

echo "Running map_to_model on ccp4 map with crystal symmetry=true"
phenix.map_to_model asymmetric_map=True include_trace_and_build=False get_half_height_width=False assign_sequence=false auto_sharpen=false resolution=3 seq.dat segment=true number_of_macro_cycles=1 include_helices_strands_only=false build_in_regions=false partial_model=start.cif partial_model_type=standard_PROTEIN refine=false fit_loops_before_assign_sequence=false extend_with_resolve=false map.ccp4 use_sg_symmetry=true is_crystal=true
if ( $status )then
  echo "FAILED"
  exit 1
endif

echo "Running map_to_model on ccp4 map with crystal symmetry=false"
phenix.map_to_model asymmetric_map=True include_trace_and_build=False get_half_height_width=False assign_sequence=false auto_sharpen=false resolution=3 seq.dat segment=true number_of_macro_cycles=1 include_helices_strands_only=false build_in_regions=false partial_model=start.cif partial_model_type=standard_PROTEIN refine=false fit_loops_before_assign_sequence=false extend_with_resolve=false map.ccp4 use_sg_symmetry=false is_crystal=false
if ( $status )then
  echo "FAILED"
  exit 1
endif

echo "Running map_to_model on ccp4 mtz with crystal symmetry=true"
phenix.map_to_model asymmetric_map=True include_trace_and_build=False get_half_height_width=False assign_sequence=false auto_sharpen=false resolution=3 seq.dat segment=true number_of_macro_cycles=1 include_helices_strands_only=false build_in_regions=false partial_model=start.cif partial_model_type=standard_PROTEIN refine=false fit_loops_before_assign_sequence=false extend_with_resolve=false map_coeffs_file=map.mtz use_sg_symmetry=true is_crystal=true
if ( $status )then
  echo "FAILED"
  exit 1
endif

echo "Running map_to_model on ccp4 mtz with crystal symmetry=false"
phenix.map_to_model asymmetric_map=True  include_trace_and_build=False get_half_height_width=False assign_sequence=false auto_sharpen=false resolution=3 seq.dat segment=true number_of_macro_cycles=1 include_helices_strands_only=false build_in_regions=false partial_model=start.cif partial_model_type=standard_PROTEIN refine=false fit_loops_before_assign_sequence=false extend_with_resolve=false map_coeffs_file=map.mtz use_sg_symmetry=false is_crystal=false
if ( $status )then
  echo "FAILED"
  exit 1
endif


