from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print ("Test script for test_map_to_model_sg_cif")
  print ("")
  log_file=os.path.join(os.getcwd(),"test_map_to_model_sg_cif.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  from phenix.programs  import map_to_model as run

  from iotbx.cli_parser import run_program
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes
  args=split_except_quotes('''asymmetric_map=True include_trace_and_build=False get_half_height_width=False assign_sequence=false auto_sharpen=false resolution=3 seq.dat segment=true number_of_macro_cycles=1 include_helices_strands_only=false build_in_regions=false partial_model=start.cif partial_model_type=standard_PROTEIN refine=false fit_loops_before_assign_sequence=false extend_with_resolve=false map.ccp4 use_sg_symmetry=true is_crystal=true''')
  run_program(program_class=run.Program,args=args,logger=log)


  from phenix.programs  import map_to_model as run

  from iotbx.cli_parser import run_program
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes
  args=split_except_quotes('''asymmetric_map=True include_trace_and_build=False get_half_height_width=False assign_sequence=false auto_sharpen=false resolution=3 seq.dat segment=true number_of_macro_cycles=1 include_helices_strands_only=false build_in_regions=false partial_model=start.cif partial_model_type=standard_PROTEIN refine=false fit_loops_before_assign_sequence=false extend_with_resolve=false map.ccp4 use_sg_symmetry=false is_crystal=false''')
  run_program(program_class=run.Program,args=args,logger=log)


  from phenix.programs  import map_to_model as run

  from iotbx.cli_parser import run_program
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes
  args=split_except_quotes('''asymmetric_map=True include_trace_and_build=False get_half_height_width=False assign_sequence=false auto_sharpen=false resolution=3 seq.dat segment=true number_of_macro_cycles=1 include_helices_strands_only=false build_in_regions=false partial_model=start.cif partial_model_type=standard_PROTEIN refine=false fit_loops_before_assign_sequence=false extend_with_resolve=false map_coeffs_file=map.mtz use_sg_symmetry=true is_crystal=true''')
  run_program(program_class=run.Program,args=args,logger=log)


  from phenix.programs  import map_to_model as run

  from iotbx.cli_parser import run_program
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes
  args=split_except_quotes('''asymmetric_map=True include_trace_and_build=False get_half_height_width=False assign_sequence=false auto_sharpen=false resolution=3 seq.dat segment=true number_of_macro_cycles=1 include_helices_strands_only=false build_in_regions=false partial_model=start.cif partial_model_type=standard_PROTEIN refine=false fit_loops_before_assign_sequence=false extend_with_resolve=false map_coeffs_file=map.mtz use_sg_symmetry=false is_crystal=false''')
  run_program(program_class=run.Program,args=args,logger=log)

  log.close()


  print ()
  print ('Done with test...running checks on output')
  print ()
  lines=open(log_file).readlines()
  from phenix_regression.wizards_cif.check_results import check_results
  check_results(test='test_map_to_model_sg_cif',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print ()
  print ('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards_cif.run_wizard_test import set_up_wizard_test
    args=['command_line_map_to_model_tests_cif_tests','test_map_to_model_sg_cif']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
