from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os,sys
  print ("Test script for test_AutoSol_phaser_mr_cif")
  print ("")
  log_file=os.path.join(os.getcwd(),"test_AutoSol_phaser_mr_cif.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  local_log_file='test_AutoSol_phaser_mr_cif.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autosol  import run_autosol
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''w1.sca seq_file=seq.dat quick=True ncs_copies=1 phasing_method=PHASER atom_type=Au build_type=RESOLVE model_building.helices_strands_only=True f_prime=-8. f_double_prime=10. input_partpdb_file=coords.cif quick=True helices_strands_start=False refine=False''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autosol(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)


  log.close()


  print ()
  print ('Done with test...running checks on output')
  print ()
  lines=open(log_file).readlines()
  from phenix_regression.wizards_cif.check_results import check_results
  check_results(test='test_AutoSol_phaser_mr_cif',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print ()
  print ('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards_cif.run_wizard_test import set_up_wizard_test
    args=['AutoSol_tests_cif_tests','test_AutoSol_phaser_mr_cif']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
