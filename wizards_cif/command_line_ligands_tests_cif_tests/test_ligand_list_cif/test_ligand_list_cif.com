#!/bin/csh -ef
#$ -cwd
phenix.ligandfit data=map_coeffs.mtz quick=true nproc=1 ligand=sdf_file_list.dat partial_model=protein.cif fixed_ligand=true
if ( $status )then
  echo "FAILED"
  exit 1
endif


