#!/bin/csh -f
phenix.find_all_ligands data=perfect.mtz model=partial.pdb resolution=3.0 n_group_search=1 ligand_cc_min=0.5 input_labels="FP PHIC FOM" quick=true ligand_list="side.pdb sideb.pdb" nproc=2
