#!/bin/csh -f
phenix.autobuild perfect.mtz input_labels="FP SIGFP" seq.dat resolution=3.0 solvent_fraction=.6 model=coords.cif rebuild_in_place=True n_cycle_rebuild_min=1 n_cycle_rebuild_max=1 omit_box_pdb_list=coords.cif omit_res_start_list="93 95" omit_res_end_list="93 95" maps_only=True composite_omit_type=simple_omit number_of_parallel_models=1 start_chains_list=92


