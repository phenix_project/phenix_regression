from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os,sys
  print ("Test script for test_AutoBuild_mult_cif")
  print ("")
  log_file=os.path.join(os.getcwd(),"test_AutoBuild_mult_cif.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  local_log_file='test_AutoBuild_mult_cif.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''perfect.mtz input_labels="FP SIGFP" map_file=perfect.mtz input_map_labels="FP PHIC FOM" seq.dat super_quick=True resolution=5.0 solvent_fraction=.6 model=coords.cif rebuild_in_place=True n_cycle_rebuild_min=1 n_cycle_rebuild_max=1 multiple_models=True multiple_models_number=1 multiple_models_group_number=1 highest_resno=200''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)


  log.close()


  print ()
  print ('Done with test...running checks on output')
  print ()
  lines=open(log_file).readlines()
  from phenix_regression.wizards_cif.check_results import check_results
  check_results(test='test_AutoBuild_mult_cif',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print ()
  print ('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards_cif.run_wizard_test import set_up_wizard_test
    args=['AutoBuild_tests_cif_tests','test_AutoBuild_mult_cif']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
