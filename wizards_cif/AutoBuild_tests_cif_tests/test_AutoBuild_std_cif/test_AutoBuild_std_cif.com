#!/bin/csh -f
setenv LIBTBX_FULL_TESTING
phenix.autobuild super_quick=True perfect.mtz input_labels="FP SIGFP PHIC FOM" map_file=perfect.mtz seq.dat resolution=3.0 solvent_fraction=.6 model=coords.cif rebuild_in_place=True n_cycle_rebuild_min=1 n_cycle_rebuild_max=1 input_map_labels='FP PHIC FOM'  number_of_parallel_models=1 start_chains_list=92


