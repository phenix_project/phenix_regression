#!/bin/csh -f
setenv LIBTBX_FULL_TESTING

echo "Running rebuild_in_place with truncate_missing_side_chains=False"

phenix.autobuild data.mtz coords.cif number_of_parallel_models=1 refine=false quick=true find_ncs=false resolve_command_list="'side_avg_min 0.5'"
if ( $status ) then
 echo "FAILED to run autobuild rebuild_in_place "
 exit 1
endif

echo "Should not have TRUNCATE in cycle_best.log:"
grep TRUNCATE AutoBuild_run_1_/TEMP0/cycle_best.log|wc
if ( $status ) then
 echo "OK ... no TRUNCATE present"
else
 echo "FAIL ... TRUNCATE present in cycle_best.log with rebuild_in_place"
 exit 1
endif

echo ""
echo "Running rebuild_in_place with truncate_missing_side_chains=True"

phenix.autobuild data.mtz coords.cif number_of_parallel_models=1 refine=false quick=true find_ncs=false truncate_missing_side_chains=True resolve_command_list="'side_avg_min 0.5'"
if ( $status ) then
  echo "FAILED to run autobuild rebuild_in_place "
  exit 1
endif

echo "Should have TRUNCATE in cycle_best.log:"
grep TRUNCATE AutoBuild_run_2_/TEMP0/cycle_best.log|wc
if ( $status ) then
 echo "FAILED... no TRUNCATE present"
 exit 1
else
 echo "OK ... TRUNCATE present in cycle_best.log with rebuild_in_place and truncate_missing_side_chains=True"
endif

echo "OK"



