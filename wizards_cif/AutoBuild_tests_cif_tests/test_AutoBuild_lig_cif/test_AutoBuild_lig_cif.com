#!/bin/csh -f
phenix.autobuild super_quick=True data=perfect_rna.mtz seq_file=rna.seq model=coords_rna_gap.cif ncycle_refine=0 rebuild_in_place=False resolution=3.0 number_of_models=0 build_outside=False n_cycle_build=1 n_cycle_rebuild_max=1 input_map_file=perfect_rna.mtz skip_combine_extend=True refine=false trace_as_lig=True input_map_labels='FP PHIC FOM' number_of_parallel_models=1 input_labels="FP SIGFP PHIC FOM"



