#!/bin/csh  -f
#$ -cwd
#
setenv LIBTBX_FULL_TESTING
#
if ($#argv == 0) then
else
  if ($argv[1] == "--help") then
    echo "test_short_seq_cif. Use: test_short_seq_cif.csh "
    exit 0
  endif
endif

echo " "
echo "----------------------------------------------------- "
echo " test_short_seq_cif "
#
phenix.autobuild input_pdb_file=hipip.cif min_seq_identity_percent=50.0 resolution=0.0 input_data_file=hipip_2.sca space_group="P 21 21 21" input_seq_file=hipip_shortseq.dat rebuild_in_place=true rebuild_res_start_list=1 rebuild_res_end_list=1 number_of_parallel_models=1 rebuild_chain_list=AXLONG refine=False maps_only=true skip_xtriage=true n_cycle_rebuild_max=0|grep -i deleted 
echo "Done with short_seq"
echo "OK"
echo "------------------------------------------------------------"
echo " "
exit 0


