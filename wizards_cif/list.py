from __future__ import division
from __future__ import print_function
import sys, os
from libtbx.utils import Sorry

skip=[] # ['resolve_tests','resolve_pattern_tests','solve_tests']
def related(args,key):
  r=False
  for arg in args:
    if key.find(arg)>-1:
      r=True
  return r

def is_unique(test_name,all_test_dict):
  # return true if test_name only appears in one element of all_test_dict
  count=0
  for key in all_test_dict.keys():
     g,t=all_test_dict[key]
     if t==test_name: count+=1
  if count==0:
    return None
  elif count==1:
    return True
  else:
    return False

def run(args):
  import libtbx.load_env
  phenix_regression = libtbx.env.find_in_repositories("phenix_regression")

  dd=os.path.join(phenix_regression,'wizards')
  all=os.listdir(dd)
  dirs=[]
  for x in all:
    xx=os.path.join(phenix_regression,'wizards',x)
    if os.path.isdir(xx):
      if x.find('tests')>-1 and (not x in skip):
         dirs.append(x)

  all_test_dict={}
  dir_list=[]
  for group_name in dirs:
    xx=os.path.join(phenix_regression,'wizards',group_name)
    for test_name in os.listdir(xx):
      if os.path.isdir(os.path.join(xx,test_name)):
        if test_name.startswith("."): continue
        root_name=test_name[5:]  #  .replace("test_","")
        tst_file_name="tst_%s.py" %(root_name)
        full_tst_file_path=os.path.join(xx,test_name,tst_file_name)
        if not os.path.isfile(full_tst_file_path):
          raise Sorry("Missing tst file: %s" %(full_tst_file_path))
        key="%s_%s" %(group_name,test_name)
        all_test_dict[key]=[group_name,test_name]
  keys=all_test_dict.keys()
  keys.sort()
  for key in keys:
    if (args and (related(args,key) or related(args,all_test_dict[key][0])
       or related(args,all_test_dict[key][1]))) \
       or (not args):
      [group_name,test_name]=all_test_dict[key]
      if is_unique(test_name,all_test_dict):
        print("phenix_regression.wizards.run_wizard_test %s" %(test_name))
      else:
        print("phenix_regression.wizards.run_wizard_test %s %s" %(group_name,test_name))

if (__name__ == "__main__"):
  print("\nphenix_regression.wizards.list "+\
     "[chars_to_find]")
  print("\nFind a wizard regression test. Available tests:\n")
  run(args=sys.argv[1:])
