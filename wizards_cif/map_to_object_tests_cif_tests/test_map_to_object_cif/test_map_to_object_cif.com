#!/bin/csh  -f
#$ -cwd
#
setenv LIBTBX_FULL_TESTING
#
if ($#argv == 0) then
else
  if ($argv[1] == "--help") then
    echo "test_map_to_object_cif. Use: test_map_to_object_cif.csh "
    exit 0
  endif
endif

echo " "
echo "----------------------------------------------------- "
echo " test_map_to_object_cif "
#
phenix.map_to_object fixed_pdb=coords.cif moving_pdb=ha_perfect.cif  >map_to_object.log
@ status_sav = $status
#
#
if ( $status_sav == 0 )then
  goto finished
else if ( $status_sav == 1 )then
    echo "*****************************************************"
    echo "Error: test failed to run map_to_object"
    echo "*****************************************************"
    exit 1
  goto finished
endif
finished:
echo "Done with map_to_object"
echo "OK"
echo "------------------------------------------------------------"
echo " "
exit 0


