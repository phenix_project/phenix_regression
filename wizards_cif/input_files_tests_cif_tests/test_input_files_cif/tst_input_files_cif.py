from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os,sys
  print ("Test script for test_input_files_cif")
  print ("")
  log_file=os.path.join(os.getcwd(),"test_input_files_cif.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  local_log_file='test_input_files_cif.log'
  local_log=open(local_log_file,'w')
  from iotbx.reflection_file_converter  import run
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''w1_c_freer.mtz --label=TEST --cns=w1_c_freer.dump''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)

  print ( open('w1_c_freer.dump.cns').read(), file = log)

  os.mkdir('test_STD')

  import shutil
  shutil.copyfile('map_coeffs.mtz','test_STD/map_coeffs.mtz')

  import shutil
  shutil.copyfile('w1_c.sca','test_STD/w1_c.sca')

  import shutil
  shutil.copyfile('w1.sca','test_STD/w1.sca')

  import shutil
  shutil.copyfile('w1_c_freer.mtz','test_STD/w1_c_freer.mtz')

  import shutil
  shutil.copyfile('coords.cif','test_STD/coords.cif')

  os.chdir('test_STD')


  local_log_file='test_input_files_cif.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''remove_aniso=False coords.cif w1_c_freer.mtz n_cycle_rebuild_max=0 nbatch=1 i_ran_seed=2557 input_refinement_file=w1_c.sca input_map_file=map_coeffs.mtz input_labels='FP SIGFP ' debug=true''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)



  local_log_file='test_input_files_cif.log'
  local_log=open(local_log_file,'w')
  from iotbx.reflection_file_converter  import run
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''--label=Free --cns=run1.dump AutoBuild_run_1_/exptl_fobs_phases_freeR_flags.mtz''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)


  cmds='''import time
time.sleep(2.)
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print (open('phenix.python.log').read(), file = log)


  local_log_file='test_input_files_cif.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''remove_aniso=false coords.cif w1_c_freer.mtz n_cycle_rebuild_max=0 nbatch=1 i_ran_seed=7121 input_map_file=map_coeffs.mtz input_labels='FP SIGFP ' debug=true''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)



  local_log_file='test_input_files_cif.log'
  local_log=open(local_log_file,'w')
  from iotbx.reflection_file_converter  import run
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''--label=Free --cns=run2.dump AutoBuild_run_2_/exptl_fobs_phases_freeR_flags.mtz''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)



  local_log_file='test_input_files_cif.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''remove_aniso=false coords.cif w1_c_freer.mtz n_cycle_rebuild_max=0 nbatch=1 i_ran_seed=7121 input_refinement_file=w1_c.sca input_map_file=map_coeffs.mtz debug=true''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)


  cmds='''import time
time.sleep(2.)
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print (open('phenix.python.log').read(), file = log)


  local_log_file='test_input_files_cif.log'
  local_log=open(local_log_file,'w')
  from iotbx.reflection_file_converter  import run
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''--label=Free --cns=run3.dump AutoBuild_run_3_/exptl_fobs_phases_freeR_flags.mtz''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)


  cmds='''import time
time.sleep(2.)
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print (open('phenix.python.log').read(), file = log)


  local_log_file='test_input_files_cif.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''remove_aniso=false coords.cif hires_file=w1_c_freer.mtz n_cycle_rebuild_max=0 data=w1_c.sca nbatch=1 i_ran_seed=7121 input_refinement_file=w1_c.sca input_map_file=map_coeffs.mtz debug=true''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)



  local_log_file='test_input_files_cif.log'
  local_log=open(local_log_file,'w')
  from iotbx.reflection_file_converter  import run
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''--label=Free --cns=run4.dump AutoBuild_run_4_/exptl_fobs_phases_freeR_flags.mtz''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)


  cmds='''import time
time.sleep(2.)
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print (open('phenix.python.log').read(), file = log)
  print ( open('run1.dump.cns').read(), file = log)
  print ( open('run2.dump.cns').read(), file = log)
  print ( open('run3.dump.cns').read(), file = log)
  print ( open('run4.dump.cns').read(), file = log)

  os.chdir('..')

  os.mkdir('test_REF')

  import shutil
  shutil.copyfile('map_coeffs.mtz','test_REF/map_coeffs.mtz')

  import shutil
  shutil.copyfile('w1_c.sca','test_REF/w1_c.sca')

  import shutil
  shutil.copyfile('w1.sca','test_REF/w1.sca')

  import shutil
  shutil.copyfile('w1_c_freer.mtz','test_REF/w1_c_freer.mtz')

  import shutil
  shutil.copyfile('coords.cif','test_REF/coords.cif')

  os.chdir('test_REF')


  local_log_file='test_input_files_cif.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''remove_aniso=False coords.cif w1_c_freer.mtz n_cycle_rebuild_max=0 nbatch=1 i_ran_seed=2557 input_refinement_file=w1_c.sca input_map_file=map_coeffs.mtz input_labels='FP SIGFP ' debug=true''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)



  local_log_file='test_input_files_cif.log'
  local_log=open(local_log_file,'w')
  from iotbx.reflection_file_converter  import run
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''--label=Free --cns=run1.dump AutoBuild_run_1_/exptl_fobs_phases_freeR_flags.mtz''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)


  cmds='''import time
time.sleep(2.)
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print (open('phenix.python.log').read(), file = log)


  local_log_file='test_input_files_cif.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''remove_aniso=false coords.cif w1_c_freer.mtz n_cycle_rebuild_max=0 nbatch=1 i_ran_seed=7121 input_map_file=map_coeffs.mtz input_labels='FP SIGFP ' debug=true''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)



  local_log_file='test_input_files_cif.log'
  local_log=open(local_log_file,'w')
  from iotbx.reflection_file_converter  import run
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''--label=Free --cns=run2.dump AutoBuild_run_2_/exptl_fobs_phases_freeR_flags.mtz''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)



  local_log_file='test_input_files_cif.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''remove_aniso=false coords.cif w1_c_freer.mtz n_cycle_rebuild_max=0 nbatch=1 i_ran_seed=7121 input_refinement_file=w1_c.sca input_map_file=map_coeffs.mtz debug=true''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)


  cmds='''import time
time.sleep(2.)
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print (open('phenix.python.log').read(), file = log)


  local_log_file='test_input_files_cif.log'
  local_log=open(local_log_file,'w')
  from iotbx.reflection_file_converter  import run
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''--label=Free --cns=run3.dump AutoBuild_run_3_/exptl_fobs_phases_freeR_flags.mtz''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)


  cmds='''import time
time.sleep(2.)
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print (open('phenix.python.log').read(), file = log)


  local_log_file='test_input_files_cif.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''remove_aniso=false coords.cif hires_file=w1_c_freer.mtz n_cycle_rebuild_max=0 data=w1_c.sca nbatch=1 i_ran_seed=7121 input_refinement_file=w1_c.sca input_map_file=map_coeffs.mtz debug=true''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)



  local_log_file='test_input_files_cif.log'
  local_log=open(local_log_file,'w')
  from iotbx.reflection_file_converter  import run
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''--label=Free --cns=run4.dump AutoBuild_run_4_/exptl_fobs_phases_freeR_flags.mtz''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)


  cmds='''import time
time.sleep(2.)
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print (open('phenix.python.log').read(), file = log)
  print ( open('run1.dump.cns').read(), file = log)
  print ( open('run2.dump.cns').read(), file = log)
  print ( open('run3.dump.cns').read(), file = log)
  print ( open('run4.dump.cns').read(), file = log)

  os.chdir('..')

  os.mkdir('test_MAP_REF')

  import shutil
  shutil.copyfile('map_coeffs.mtz','test_MAP_REF/map_coeffs.mtz')

  import shutil
  shutil.copyfile('w1_c.sca','test_MAP_REF/w1_c.sca')

  import shutil
  shutil.copyfile('w1.sca','test_MAP_REF/w1.sca')

  import shutil
  shutil.copyfile('w1_c_freer.mtz','test_MAP_REF/w1_c_freer.mtz')

  import shutil
  shutil.copyfile('coords.cif','test_MAP_REF/coords.cif')

  os.chdir('test_MAP_REF')


  local_log_file='test_input_files_cif.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''remove_aniso=False coords.cif w1_c_freer.mtz n_cycle_rebuild_max=0 nbatch=1 i_ran_seed=2557 input_refinement_file=w1_c.sca input_map_file=map_coeffs.mtz input_labels='FP SIGFP ' debug=true''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)



  local_log_file='test_input_files_cif.log'
  local_log=open(local_log_file,'w')
  from iotbx.reflection_file_converter  import run
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''--label=Free --cns=run1.dump AutoBuild_run_1_/exptl_fobs_phases_freeR_flags.mtz''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)


  cmds='''import time
time.sleep(2.)
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print (open('phenix.python.log').read(), file = log)


  local_log_file='test_input_files_cif.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''remove_aniso=false coords.cif w1_c_freer.mtz n_cycle_rebuild_max=0 nbatch=1 i_ran_seed=7121 input_map_file=map_coeffs.mtz input_labels='FP SIGFP ' debug=true''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)



  local_log_file='test_input_files_cif.log'
  local_log=open(local_log_file,'w')
  from iotbx.reflection_file_converter  import run
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''--label=Free --cns=run2.dump AutoBuild_run_2_/exptl_fobs_phases_freeR_flags.mtz''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)



  local_log_file='test_input_files_cif.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''remove_aniso=false coords.cif w1_c_freer.mtz n_cycle_rebuild_max=0 nbatch=1 i_ran_seed=7121 input_refinement_file=w1_c.sca input_map_file=map_coeffs.mtz debug=true''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)


  cmds='''import time
time.sleep(2.)
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print (open('phenix.python.log').read(), file = log)


  local_log_file='test_input_files_cif.log'
  local_log=open(local_log_file,'w')
  from iotbx.reflection_file_converter  import run
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''--label=Free --cns=run3.dump AutoBuild_run_3_/exptl_fobs_phases_freeR_flags.mtz''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)


  cmds='''import time
time.sleep(2.)
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print (open('phenix.python.log').read(), file = log)


  local_log_file='test_input_files_cif.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''remove_aniso=false coords.cif hires_file=w1_c_freer.mtz n_cycle_rebuild_max=0 data=w1_c.sca nbatch=1 i_ran_seed=7121 input_refinement_file=w1_c.sca input_map_file=map_coeffs.mtz debug=true''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)



  local_log_file='test_input_files_cif.log'
  local_log=open(local_log_file,'w')
  from iotbx.reflection_file_converter  import run
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''--label=Free --cns=run4.dump AutoBuild_run_4_/exptl_fobs_phases_freeR_flags.mtz''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)


  cmds='''import time
time.sleep(2.)
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print (open('phenix.python.log').read(), file = log)
  print ( open('run1.dump.cns').read(), file = log)
  print ( open('run2.dump.cns').read(), file = log)
  print ( open('run3.dump.cns').read(), file = log)
  print ( open('run4.dump.cns').read(), file = log)

  os.chdir('..')

  os.mkdir('test_MAP')

  import shutil
  shutil.copyfile('map_coeffs.mtz','test_MAP/map_coeffs.mtz')

  import shutil
  shutil.copyfile('w1_c.sca','test_MAP/w1_c.sca')

  import shutil
  shutil.copyfile('w1.sca','test_MAP/w1.sca')

  import shutil
  shutil.copyfile('w1_c_freer.mtz','test_MAP/w1_c_freer.mtz')

  import shutil
  shutil.copyfile('coords.cif','test_MAP/coords.cif')

  os.chdir('test_MAP')


  local_log_file='test_input_files_cif.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''remove_aniso=False coords.cif w1_c_freer.mtz n_cycle_rebuild_max=0 nbatch=1 i_ran_seed=2557 input_refinement_file=w1_c.sca input_map_file=map_coeffs.mtz input_labels='FP SIGFP ' debug=true''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)



  local_log_file='test_input_files_cif.log'
  local_log=open(local_log_file,'w')
  from iotbx.reflection_file_converter  import run
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''--label=Free --cns=run1.dump AutoBuild_run_1_/exptl_fobs_phases_freeR_flags.mtz''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)


  cmds='''import time
time.sleep(2.)
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print (open('phenix.python.log').read(), file = log)


  local_log_file='test_input_files_cif.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''remove_aniso=false coords.cif w1_c_freer.mtz n_cycle_rebuild_max=0 nbatch=1 i_ran_seed=7121 input_map_file=map_coeffs.mtz input_labels='FP SIGFP ' debug=true''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)



  local_log_file='test_input_files_cif.log'
  local_log=open(local_log_file,'w')
  from iotbx.reflection_file_converter  import run
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''--label=Free --cns=run2.dump AutoBuild_run_2_/exptl_fobs_phases_freeR_flags.mtz''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)



  local_log_file='test_input_files_cif.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''remove_aniso=false coords.cif w1_c_freer.mtz n_cycle_rebuild_max=0 nbatch=1 i_ran_seed=7121 input_refinement_file=w1_c.sca input_map_file=map_coeffs.mtz debug=true''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)


  cmds='''import time
time.sleep(2.)
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print (open('phenix.python.log').read(), file = log)


  local_log_file='test_input_files_cif.log'
  local_log=open(local_log_file,'w')
  from iotbx.reflection_file_converter  import run
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''--label=Free --cns=run3.dump AutoBuild_run_3_/exptl_fobs_phases_freeR_flags.mtz''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)


  cmds='''import time
time.sleep(2.)
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print (open('phenix.python.log').read(), file = log)


  local_log_file='test_input_files_cif.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''remove_aniso=false coords.cif hires_file=w1_c_freer.mtz n_cycle_rebuild_max=0 data=w1_c.sca nbatch=1 i_ran_seed=7121 input_refinement_file=w1_c.sca input_map_file=map_coeffs.mtz debug=true''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)



  local_log_file='test_input_files_cif.log'
  local_log=open(local_log_file,'w')
  from iotbx.reflection_file_converter  import run
  from phenix_regression.wizards_cif.run_wizard_test import split_except_quotes

  args=split_except_quotes('''--label=Free --cns=run4.dump AutoBuild_run_4_/exptl_fobs_phases_freeR_flags.mtz''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)


  cmds='''import time
time.sleep(2.)
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print (open('phenix.python.log').read(), file = log)
  print ( open('run1.dump.cns').read(), file = log)
  print ( open('run2.dump.cns').read(), file = log)
  print ( open('run3.dump.cns').read(), file = log)
  print ( open('run4.dump.cns').read(), file = log)

  os.chdir('..')

  log.close()


  print ()
  print ('Done with test...running checks on output')
  print ()
  lines=open(log_file).readlines()
  from phenix_regression.wizards_cif.check_results import check_results
  check_results(test='test_input_files_cif',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print ()
  print ('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards_cif.run_wizard_test import set_up_wizard_test
    args=['input_files_tests_cif_tests','test_input_files_cif']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
