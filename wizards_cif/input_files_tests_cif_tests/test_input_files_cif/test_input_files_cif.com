#!/bin/csh -f
echo "Checking multiple input files..."
#
#ls -lt   map_coeffs.mtz  w1_c.sca  w1.sca w1_c_freer.mtz coords.cif
#
echo "w1_c_freer.mtz : starting freeR flags from seed=2557"
phenix.reflection_file_converter w1_c_freer.mtz --label=TEST --cns=w1_c_freer.dump > /dev/null
cat w1_c_freer.dump.cns 

foreach tt ( "STD" "REF" "MAP_REF" "MAP")
echo "TYPE: $tt"
if ( $tt == "STD") then
set ref = " verbose=True "
set map = " verbose=True "
else if ( $tt == "REF") then
set ref = "input_refinement_file=w1_c.sca"
set map = " verbose=True "
else if ( $tt == "MAP") then
set ref = " verbose=True "
set map = "input_map_file=map_coeffs.mtz  "
else if ( $tt == "MAP_REF") then
set ref = "input_refinement_file=w1_c.sca"
set map = "input_map_file=map_coeffs.mtz  "
endif
#

mkdir test_$tt
cp map_coeffs.mtz test_$tt/map_coeffs.mtz
cp w1_c.sca test_$tt/w1_c.sca 
cp w1.sca test_$tt/w1.sca
cp w1_c_freer.mtz test_$tt/w1_c_freer.mtz
cp coords.cif test_$tt/coords.cif
cd test_$tt

echo "run1 : generate new freer flags with same seed=2557:"
phenix.autobuild remove_aniso=False \
    coords.cif w1_c_freer.mtz n_cycle_rebuild_max=0 \
   nbatch=1 i_ran_seed=2557  \
    $ref $map  input_labels='FP SIGFP ' debug=true> run1.log

#
phenix.reflection_file_converter --label=Free \
 --cns=run1.dump AutoBuild_run_1_/exptl_fobs_phases_freeR_flags.mtz > /dev/null
####
phenix.python<<EOD
import time
time.sleep(2.)
EOD
#
echo "run2 : generate new freer flags with new seed=7121:"
phenix.autobuild remove_aniso=false \
   coords.cif w1_c_freer.mtz n_cycle_rebuild_max=0  \
   nbatch=1 i_ran_seed=7121 \
    $map  input_labels='FP SIGFP '  debug=true> run2.log
#
phenix.reflection_file_converter --label=Free \
 --cns=run2.dump AutoBuild_run_2_/exptl_fobs_phases_freeR_flags.mtz  > /dev/null
####
#
echo "run3 : take w1_c_freer flags by default, different seed=7121:"
phenix.autobuild remove_aniso=false \
   coords.cif w1_c_freer.mtz n_cycle_rebuild_max=0 \
   nbatch=1 i_ran_seed=7121  \
    $ref $map  \
    debug=true > run3.log
#
phenix.python<<EOD
import time
time.sleep(2.)
EOD
phenix.reflection_file_converter --label=Free \
--cns=run3.dump AutoBuild_run_3_/exptl_fobs_phases_freeR_flags.mtz  > /dev/null
echo "run4 : take w1_c_freer flags from hires file unless ref supplied, different seed=7121"
phenix.python<<EOD
import time
time.sleep(2.)
EOD
phenix.autobuild remove_aniso=false \
   coords.cif hires_file=w1_c_freer.mtz \
      n_cycle_rebuild_max=0 data=w1_c.sca nbatch=1 i_ran_seed=7121  \
    $ref  $map  debug=true > run4.log
phenix.reflection_file_converter --label=Free --cns=run4.dump AutoBuild_run_4_/exptl_fobs_phases_freeR_flags.mtz  > /dev/null
#
phenix.python<<EOD
import time
time.sleep(2.)
EOD

cat run1.dump.cns 
cat run2.dump.cns 
cat run3.dump.cns 
cat run4.dump.cns 

cd ..
#
end
exit 0


