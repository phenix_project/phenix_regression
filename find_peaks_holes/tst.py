from __future__ import division
from __future__ import print_function
import libtbx.load_env
import os, time
from libtbx import easy_run

def exercise_00():
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/den_restraints/start2.pdb",
    test=os.path.isfile)
  mtz_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/den_restraints/data2.mtz",
    test=os.path.isfile)
  cmd = " ".join([
    "phenix.find_peaks_holes",
    "%s"%pdb_file,
    "%s"%mtz_file,
    " > find_peaks_holes.log"])
  print(cmd)
  assert not easy_run.call(cmd)

if (__name__ == "__main__"):
  t0 = time.time()
  exercise_00()
  print("Time:%8.2f"%(time.time()-t0))
  print("OK")

