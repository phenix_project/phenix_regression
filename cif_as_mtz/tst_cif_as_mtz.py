from __future__ import print_function
import os, time
from cctbx.array_family import flex
from libtbx import easy_run
from libtbx.test_utils import is_below_limit
import libtbx.load_env
from libtbx.test_utils import approx_equal, Exception_expected
from libtbx.test_utils import open_tmp_file
from libtbx.utils import remove_files
import random
from iotbx import reflection_file_reader

random.seed(0)
flex.set_random_seed(0)

def file_finder(pdb_file = None, hkl_file = None):
  if(pdb_file is not None):
    pdb_file = libtbx.env.find_in_repositories(
      relative_path="phenix_regression/cif_as_mtz/%s"%pdb_file, \
      test=os.path.isfile)
  if(hkl_file is not None):
    hkl_file = libtbx.env.find_in_repositories(
      relative_path="phenix_regression/cif_as_mtz/%s"%hkl_file, \
      test=os.path.isfile)
  if([pdb_file, hkl_file].count(None) > 0):
    if(pdb_file is not None): return pdb_file
    elif(hkl_file is not None): return hkl_file
  else: return pdb_file, hkl_file

def template_1(pdb_file, hkl_file, string, limit, eps=0.005, info_low_eps=0.005,
               merge=False,remove_systematic_absences=False):
  pdb_file, hkl_file = file_finder(pdb_file = pdb_file, hkl_file = hkl_file)
  cmd = "phenix.cif_as_mtz %s --symmetry=%s --show_log" % (hkl_file, pdb_file)
  if merge: cmd += " --merge "
  if(remove_systematic_absences): cmd += " --remove_systematic_absences "
  print(cmd)
  r = easy_run.go(cmd).stdout_lines

def compare_two_mtz(file_1, file_2):
  from iotbx import reflection_file_reader
  fobs = []
  for file_name in [file_1, file_2]:
    assert file_name is not None
    reflection_file = reflection_file_reader.any_reflection_file(file_name)
    reflection_file_as_miller_arrays  = \
      reflection_file.as_miller_arrays(merge_equivalents=True)
    for miller_array in reflection_file_as_miller_arrays:
      label = miller_array.info().labels[0]
      if(label == 'FOBS'):
        fobs.append(miller_array)
  assert len(fobs) == 2
  return fobs[0].data().size() == fobs[1].data().size() and \
         approx_equal(fobs[0].data(), fobs[1].data())

def exercise_00():
  hkl_file = file_finder(hkl_file = "r1jxmsf.ent")
  hkl_file_w1c1 = file_finder(hkl_file = "r1jxmsf_wi1_ci1.ent")
  hkl_file_w2c2 = file_finder(hkl_file = "r1jxmsf_wi2_ci2.ent")
  hkl_file_w3c2 = file_finder(hkl_file = "r1jxmsf_wi3_ci2.ent")
  symmetry = "--unit_cell=%s --space_group=%s"%(
    "59.724,59.724,209.956,90,90,90", '"P 41 21 2"')
  #
  cmd = "phenix.cif_as_mtz %s %s"%(hkl_file, symmetry)
  r = easy_run.go(cmd).stdout_lines
  # XXX this test is not correct
  #assert  'Miller indices not unique under symmetry: %s (22 redundant indices out of 302)'%hkl_file in r
  remove_files(pattern="r1jxmsf*")
  #
  wc = [(1,1),
        #(None,1),
        (1,2),
        (2,1),(2,2),
        (3,1),(3,2)]
  for w, c in wc:
    remove_files(pattern="r1jxmsf*")
    ow = ""
    oc = ""
    if(w is not None): ow = "--wavelength_id=%s"%str(w)
    if(c is not None): oc = "--crystal_id=%s"%str(c)
    cmd = "phenix.cif_as_mtz %s %s %s %s"%(hkl_file, symmetry, ow, oc)
    assert not easy_run.call(cmd)
    if(w in (1,None) and c in (1,None)):
      cmd = "phenix.cif_as_mtz %s %s"%(hkl_file_w1c1, symmetry)
      assert not easy_run.call(cmd)
      assert compare_two_mtz(file_1="r1jxmsf_wi1_ci1.mtz", file_2="r1jxmsf.mtz")
      cmd = "phenix.cif_as_mtz %s %s %s %s"%(hkl_file_w1c1, symmetry, ow, oc)
      assert not easy_run.call(cmd)
      assert compare_two_mtz(file_1="r1jxmsf_wi1_ci1.mtz", file_2="r1jxmsf.mtz")
    if(w in (2,None) and c in (2,2)):
      cmd = "phenix.cif_as_mtz %s %s"%(hkl_file_w2c2, symmetry)
      assert not easy_run.call(cmd)
      assert compare_two_mtz(file_1="r1jxmsf_wi2_ci2.mtz", file_2="r1jxmsf.mtz")
      cmd = "phenix.cif_as_mtz %s %s %s %s"%(hkl_file_w2c2, symmetry, ow, oc)
      assert not easy_run.call(cmd)
      assert compare_two_mtz(file_1="r1jxmsf_wi2_ci2.mtz", file_2="r1jxmsf.mtz")
    if(w in (3,None) and c in (3,2)):
      cmd = "phenix.cif_as_mtz %s %s"%(hkl_file_w3c2, symmetry)
      assert not easy_run.call(cmd)
      assert compare_two_mtz(file_1="r1jxmsf_wi3_ci2.mtz", file_2="r1jxmsf.mtz")
      cmd = "phenix.cif_as_mtz %s %s %s %s"%(hkl_file_w3c2, symmetry, ow, oc)
      easy_run.call(cmd)
      assert compare_two_mtz(file_1="r1jxmsf_wi3_ci2.mtz", file_2="r1jxmsf.mtz")
  #
  remove_files(pattern="r1jxmsf*")
  cmd = "phenix.cif_as_mtz %s %s --crystal_id=2 --wavelength_id=2"%(hkl_file, symmetry)
  easy_run.call(cmd)
  cmd = "phenix.cif_as_mtz %s %s"%(hkl_file_w1c1, symmetry)
  easy_run.call(cmd)
  assert not compare_two_mtz(file_1="r1jxmsf_wi1_ci1.mtz", file_2="r1jxmsf.mtz")
  #
  remove_files(pattern="r1jxmsf*")
  cmd = "phenix.cif_as_mtz %s %s --crystal_id=1 --wavelength_id=1"%(hkl_file, symmetry)
  easy_run.call(cmd)
  cmd = "phenix.cif_as_mtz %s %s"%(hkl_file_w3c2, symmetry)
  easy_run.call(cmd)
  assert not compare_two_mtz(file_1="r1jxmsf_wi3_ci2.mtz", file_2="r1jxmsf.mtz")

def run(eps = 0.5, info_low_eps = 0.5):
  # label - intensities, columns - intensities , neutron data
  template_1(pdb_file = "pdb1wqz.ent",
             hkl_file = "r1wqzsf_1.ent",
             string   = 'Answer:      N FFORCE  False',
             limit    = 0.2153)
  # label - amplitudes, columns - intensities , neutron data
  template_1(pdb_file = "pdb1wqz.ent",
             hkl_file = "r1wqzsf_2.ent",
             string   = 'Answer:      N FFORCE  False',
             limit    = 0.2153)
  # label - amplitudes, columns - amplitudes , neutron data
  template_1(pdb_file = "pdb1wqz.ent",
             hkl_file = "r1wqzsf_3.ent",
             string   = 'Answer: FOBS_N',
             limit    = None)
  # label - intensities, columns - amplitudes , neutron data
  template_1(pdb_file = "pdb1wqz.ent",
             hkl_file = "r1wqzsf_4.ent",
             string   = 'Answer: IOBS_N',
             limit    = None)
  # label - intensities, columns - intensities , xray data
  template_1(pdb_file = "pdb1r68.ent",
             hkl_file = "r1r68sf_1.ent",
             string   = 'Answer:      X FFORCE  False',
             limit    = 0.1610,
             remove_systematic_absences=True)
  # label - amplitudes, columns - intensities , xray data
  template_1(pdb_file = "pdb1r68.ent",
             hkl_file = "r1r68sf_2.ent",
             string   = 'Answer:      X FFORCE  False',
             limit    = 0.1610,
             remove_systematic_absences=True)
  # label - amplitudes, columns - amplitudes , xray data
  template_1(pdb_file = "pdb2olx.ent",
             hkl_file = "r2olxsf_1.ent",
             string   = 'Answer:      X      F  False',
             limit    = 0.1991,
             merge    = True)
  # label - intensities, columns - amplitudes , xray data
  template_1(pdb_file = "pdb2olx.ent",
             hkl_file = "r2olxsf_2.ent",
             string   = 'Answer:      X      F  False',
             limit    = 0.1991)
  # check outliers
  template_1(pdb_file = "pdb2olx.ent",
             hkl_file = "r2olxsf_8.ent",
             string   = 'Answer:      X      F  False',
             limit    = 0.2066,
             merge    = True)
  # check after-end
  template_1(pdb_file = "pdb2olx.ent",
             hkl_file = "r2olxsf_10.ent",
             string   = 'Answer:      X      F  False',
             limit    = 0.2066,
             merge    = True)
  # check no sigma and status
  template_1(pdb_file = "pdb2olx.ent",
             hkl_file = "r2olxsf_11.ent",
             string   = 'Answer:      X      F  False',
             limit    = 0.1991,
             merge    = True)
  # reflections removed by MTZ machinery
  template_1(pdb_file = "pdb1gx2.ent",
             hkl_file = "r1gx2sf.ent",
             string   = 'Answer:      X      F  False',
             limit    = 0.2357)
  # reflections removed by MTZ machinery, .gz input files
  template_1(pdb_file = "pdb1gx2.ent.gz",
             hkl_file = "r1gx2sf.ent.gz",
             string   = 'Answer:      X      F  False',
             limit    = 0.2357)
  # exercise wavelength_id and crystal_id options
  exercise_00()

def exercise_03():
  hkl_file = file_finder(hkl_file = "r1fcpsf_cut.ent.gz")
  pdb_file = file_finder(hkl_file = "pdb1fcp_symm_only.ent.gz")
  cmd = "phenix.cif_as_mtz %s --symmetry=%s"%(hkl_file, pdb_file)
  easy_run.call(cmd)
  fobs = []
  reflection_file = reflection_file_reader.any_reflection_file(
    file_name = "r1fcpsf_cut.mtz")
  reflection_file_as_miller_arrays  = \
    reflection_file.as_miller_arrays(merge_equivalents=True)
  for miller_array in reflection_file_as_miller_arrays:
    label = miller_array.info().labels[0]
    if(label == 'IOBS'):
      fobs.append(miller_array)
  f_obs = fobs[0]
  assert f_obs.indices().size() == 6

def exercise_04() :
  if (os.path.isfile("2h34-sf.mtz")) :
    os.remove("2h34-sf.mtz")
  pdb_file, hkl_file = file_finder("2h34.pdb", "2h34-sf.cif")
  assert (not None in [pdb_file, hkl_file])
  cmd = "phenix.cif_as_mtz %s --symmetry=%s" % (hkl_file, pdb_file)
  cmd_out = easy_run.fully_buffered(cmd)
  assert (not os.path.isfile("2h34-sf.mtz"))
  assert ('Add --merge to command arguments to force merging data.' in
          "\n".join(cmd_out.stderr_lines))
  cmd2 = cmd + " --merge"
  cmd2_out = easy_run.fully_buffered(cmd2)
  assert ('Warning: merging non-unique data' in cmd2_out.stdout_lines)
  assert (os.path.isfile("2h34-sf.mtz"))

def exercise_05():
  mtz_name = "3adr-sf.mtz"
  if (os.path.isfile(mtz_name)):
    os.remove(mtz_name)
  pdb_file, hkl_file = file_finder("3ADR.pdb", "3adr-sf.cif")
  assert [pdb_file, hkl_file].count(None) == 0
  cmd = "phenix.cif_as_mtz %s --symmetry=%s" % (hkl_file, pdb_file)
  cmd_out = easy_run.fully_buffered(cmd)
  assert (os.path.isfile(mtz_name))
  mas = reflection_file_reader.any_reflection_file(
    file_name=mtz_name).as_miller_arrays()
  hl_coeffs_in_mtz = False
  anomalous_array_in_mtz = False
  for ma in mas:
    if isinstance(ma.data(), flex.hendrickson_lattman):
      hl_coeffs_in_mtz = True
    elif (ma.info().labels ==
          ['I(+)', 'SIGI(+)', 'I(-)', 'SIGI(-)']):
      anomalous_array_in_mtz = True
  assert hl_coeffs_in_mtz
  assert anomalous_array_in_mtz

def exercise_06():
  pdb_file, hkl_file = file_finder("m1.pdb", "m1.cif")
  assert [pdb_file, hkl_file].count(None) == 0
  cmd = "phenix.cif_as_mtz %s --symmetry=%s" % (hkl_file, pdb_file)
  easy_run.call(cmd)
  assert (os.path.isfile("m1.mtz"))

def exercise_07():
  pdb_file, hkl_file = file_finder("101m_cut.pdb", "101m-sf_cut.cif")
  assert [pdb_file, hkl_file].count(None) == 0
  ofn = "101m_cut.mtz"
  cmd = " ".join([
    "phenix.cif_as_mtz",
    "%s"%hkl_file,
    "--merge",
    "--remove_systematic_absences",
    "--map_to_asu",
    "--symmetry=%s"%pdb_file,
    "--symmetry=%s"%pdb_file,
    '--output_file_name=\"%s\"' %ofn,
    "--ignore_bad_sigmas"])
  easy_run.call(cmd)
  mas = reflection_file_reader.any_reflection_file(
    file_name=ofn).as_miller_arrays()
  cntr = 0
  for ma in mas:
    if(ma.info().labels == ['FOBS']):
      assert ma.data().size() == 49
      assert ma.sigmas() is None
      cntr += 1
  assert cntr == 1

def exercise_08():
  cif_text = """\
data_xxxx
loop_
_refln.wavelength_id
_refln.crystal_id
_refln.index_h
_refln.index_k
_refln.index_l
_refln.intensity_meas
_refln.intensity_sigma
1 1 1 1  1  1.0 5
1 1 1 1 -1  2.0 6
1 1 1 2  2  3.0 6
1 1 1 2 -2  4.0 7
"""

  cif_symmetry_text = """
_cell.length_a  8.945
_cell.length_b  9.540
_cell.length_c  12.116
_cell.angle_alpha  90.00
_cell.angle_beta  90.00
_cell.angle_gamma  90.00

_symmetry.space_group_name_H-M 'P 21 21 21'
"""

  pdb_symmetry_text = """\
CRYST1    8.945    9.540   12.116  90.00  90.00  90.00 P 21 21 21
SCALE1      0.111789  0.000000  0.000000        0.00000
SCALE2      0.000000  0.104827  0.000000        0.00000
SCALE3      0.000000  0.000000  0.082539        0.00000
  """
  pdb_file = open_tmp_file(suffix=".pdb")
  pdb_file.write(pdb_symmetry_text)
  pdb_file.close()


  def run_cif_as_mtz(cmd, hkl_file):
    result = easy_run.fully_buffered(cmd).raise_if_errors()
    mas = reflection_file_reader.any_reflection_file(
      file_name=os.path.splitext(hkl_file.name)[0]+".mtz").as_miller_arrays()
    assert len(mas) == 1
    assert mas[0].size() == 4

  # Test case where symmetry is in file, no symmetry passed via cmd line
  hkl_file = open_tmp_file(suffix=".cif")
  hkl_file.write("\n".join((cif_text, cif_symmetry_text)))
  hkl_file.close()
  cmd = " ".join([
    "phenix.cif_as_mtz",
    "%s"%hkl_file.name,
  ])
  run_cif_as_mtz(cmd, hkl_file)

  # Test case where cif file contains no symmetry, ensure it is passed along from pdb
  hkl_file = open_tmp_file(suffix=".cif")
  hkl_file.write(cif_text)
  hkl_file.close()
  cmd = " ".join([
    "phenix.cif_as_mtz",
    "%s"%hkl_file.name,
    "--symmetry=%s"%pdb_file.name
  ])
  run_cif_as_mtz(cmd, hkl_file)

  # Test case where no symmetry is given at all, check correct error message is raised
  hkl_file = open_tmp_file(suffix=".cif")
  hkl_file.write(cif_text)
  hkl_file.close()
  cmd = " ".join([
    "phenix.cif_as_mtz",
    "%s"%hkl_file.name,
  ])
  result = easy_run.fully_buffered(cmd)
  assert not os.path.exists(os.path.splitext(hkl_file.name)[0]+".mtz")
  assert "\n".join(result.stderr_lines) \
         == "Sorry: Crystal symmetry is not defined. Please use the --symmetry option."

def exercise_09():
  cif_text = """data_r1lnisf
_cell.length_a      64.200
_cell.length_b      77.800
_cell.length_c      38.280
_cell.angle_alpha   90.000
_cell.angle_beta    90.000
_cell.angle_gamma   90.000
#
_symmetry.space_group_name_H-M   'P 21 21 21'
#
loop_
_refln.crystal_id
_refln.wavelength_id
_refln.scale_group_code
_refln.index_h
_refln.index_k
_refln.index_l
_refln.status
_refln.intensity_meas
_refln.intensity_sigma
1 1 1    0    0    8  o     91935     4674
1 1 1    0    0   10  o      1530       84
1 1 1    0    0   12  o     41390     1650
1 1 1    0    0   14  o      5968      159
1 1 1    0    0   16  o      1095       61
1 1 1    0    0   18  o      7888      324
1 1 1    0    0   20  o      1265       91
1 1 1    0    0   22  o     13070      753
1 1 1    0    0   24  o      1734      104
1 1 1    0    0   26  o        58       48
1 1 1    0    0   28  o       137       51
1 1 1    0    0   30  o      1764      169
1 1 1    0    0   32  o        58       54
1 1 1    0    0   34  o       -10       52
1 1 1    0    0   38  o       -17       47
"""
  hkl_file = open_tmp_file(suffix=".cif")
  hkl_file.write(cif_text)
  hkl_file.close()
  cmd = " ".join([
    "phenix.cif_as_mtz",
    "%s"%hkl_file.name,
  ])
  result = easy_run.fully_buffered(cmd).raise_if_errors()
  mtz_name = os.path.splitext(hkl_file.name)[0]+".mtz"
  assert os.path.isfile(mtz_name)
  mas = reflection_file_reader.any_reflection_file(
    file_name=mtz_name).as_miller_arrays()
  assert len(mas) == 2
  assert (sorted(mas[0].info().labels + mas[1].info().labels) == sorted(['R-free-flags', 'IOBS', 'SIGIOBS']))
  for ma in mas:
    assert ma.size() == 15

# XFEL data CIF provided as supplemental material for Acta D article
def exercise_10 () :
  cif_in = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/cif_as_mtz/barends.cif",
    test=os.path.isfile)
  assert os.path.isfile(cif_in)
  cmd = "phenix.cif_as_mtz \"%s\"" % cif_in
  result = easy_run.fully_buffered(cmd).raise_if_errors()
  assert (result.return_code == 0)
  mas = reflection_file_reader.any_reflection_file(
    file_name="barends.mtz").as_miller_arrays()
  labels = [ ma.info().label_string() for ma in mas ]
  # verify the original column order from the cif file is retained as to avoid quirk in
  # mtz module that will merge two adjacent amplitude columns into one anomalous data column
  assert (sorted(labels) == sorted(['R-free-flags', 'FOBS,SIGFOBS','PHIC','DANO,SIGDANO']))

# 2wfw (has phase_meas and fom arrays)
def exercise_11 () :
  cif_in = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/cif_as_mtz/2wfw-sf.cif",
    test=os.path.isfile)
  assert os.path.isfile(cif_in)
  cmd = "phenix.cif_as_mtz \"%s\"" % cif_in
  result = easy_run.fully_buffered(cmd).raise_if_errors()
  assert (result.return_code == 0)
  mas = reflection_file_reader.any_reflection_file(
    file_name="2wfw-sf.mtz").as_miller_arrays()
  labels = [ ma.info().label_string() for ma in mas ]
  assert (sorted(labels) == sorted(['FC,PHIFC', 'FOBS,SIGFOBS', 'FOM', 'PHIM', 'R-free-flags']))

# 4fs6 (has isolated DANO,SIGDANO)
def exercise_12 () :
  cif_in = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/cif_as_mtz/4fs6-sf.cif",
    test=os.path.isfile)
  assert os.path.isfile(cif_in)
  cmd = "phenix.cif_as_mtz --merge \"%s\"" % cif_in
  result = easy_run.fully_buffered(cmd).raise_if_errors()
  assert (result.return_code == 0)
  mas = reflection_file_reader.any_reflection_file(
    file_name="4fs6-sf.mtz").as_miller_arrays()
  dano = None
  for ma in mas :
    if (ma.info().label_string() == "DANO,SIGDANO") :
      dano = ma
      break
  assert (dano is not None)
  assert dano.observation_type() is None

def exercise_13():
  cif_in = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/cif_as_mtz/4hae-sf_truncated.cif",
    test=os.path.isfile)
  assert os.path.isfile(cif_in)
  cmd = "phenix.cif_as_mtz \"%s\"" % cif_in
  result = easy_run.fully_buffered(cmd).raise_if_errors()
  assert (result.return_code == 0)
  mas = reflection_file_reader.any_reflection_file(
    file_name="4hae-sf_truncated.mtz").as_miller_arrays()
  d1 = mas[0].data()
  assert approx_equal(mas[1].data(), [458.5, 34.7, 39.7], 0.1)


def exercise_14():
  cif_text = """# snippet from structure factor file with PDB code 6CXO
data_r6cxosf
#
_audit.revision_id     1_0
_audit.creation_date   2018-09-05
_audit.update_record   "Initial release"
#
_cell.entry_id      6cxo
_cell.length_a      52.588
_cell.length_b      149.213
_cell.length_c      165.827
_cell.angle_alpha   90.000
_cell.angle_beta    90.000
_cell.angle_gamma   90.000
#
_diffrn_radiation_wavelength.id           1
_diffrn_radiation_wavelength.wavelength   1.2036
#
_entry.id   6cxo
#
_exptl_crystal.id   1
#
_reflns_scale.group_code   1
#
_symmetry.entry_id               6cxo
_symmetry.space_group_name_H-M   "P 21 2 21"
_symmetry.Int_Tables_number      2018
#
loop_
_refln.crystal_id
_refln.wavelength_id
_refln.scale_group_code
_refln.index_h
_refln.index_k
_refln.index_l
_refln.status
_refln.pdbx_r_free_flag
_refln.F_meas_au
_refln.F_meas_sigma_au
_refln.F_calc_au
_refln.phase_calc
_refln.pdbx_HL_A_iso
_refln.pdbx_HL_B_iso
_refln.pdbx_HL_C_iso
_refln.pdbx_HL_D_iso
_refln.pdbx_FWT
_refln.pdbx_PHWT
_refln.pdbx_DELFWT
_refln.pdbx_DELPHWT
_refln.fom
1 1 1 0  0  6  o 10 2637.91 60.30 80.15   180.00 -0.30   0.00    0.00 0.00 975.82  180.00 895.68  180.00 0.29
1 1 1 0  0  8  o 12 976.72  17.78 1455.08 180.00 -3.75   0.00    0.00 0.00 120.17  0.00   1575.25 0.00   1.00
1 1 1 0  0  10 o 17 473.14  11.64 474.88  0.00   0.84    0.00    0.00 0.00 33.10   180.00 507.98  180.00 0.69
1 1 1 0  0  18 f 0  2590.99 42.02 1898.47 180.00 -32.04  0.00    0.00 0.00 1546.52 180.00 351.95  0.00   1.00
1 1 1 0  1  4  o 16 1300.19 15.72 28.41   180.00 -0.06   0.00    0.00 0.00 71.53   180.00 43.12   180.00 0.06
1 1 1 0  1  5  o 11 106.11  7.63  367.11  90.00  0.00    0.04    0.00 0.00 361.40  270.00 728.52  270.00 0.04
1 1 1 0  1  6  o 14 946.07  10.00 504.84  180.00 -1.32   0.00    0.00 0.00 620.26  180.00 115.42  180.00 0.87
1 1 1 0  1  7  o 3  341.27  6.02  37.42   90.00  0.00    0.03    0.00 0.00 25.01   270.00 62.43   270.00 0.03
1 1 1 0  1  8  o 14 312.56  8.38  366.82  180.00 -0.60   0.00    0.00 0.00 138.54  0.00   505.37  0.00   0.53
1 1 1 0  1  9  o 14 762.82  10.59 194.08  90.00  0.00    0.62    0.00 0.00 381.28  90.00  187.20  90.00  0.55
1 1 1 0  1  10 o 4  1956.12 22.59 1047.15 180.00 -15.10  0.00    0.00 0.00 1618.79 180.00 571.63  180.00 1.00
1 1 1 0  1  11 o 18 384.90  7.69  160.75  270.00 -0.00   -0.44   0.00 0.00 54.16   270.00 106.59  90.00  0.41
1 1 1 0  1  12 o 4  640.34  9.55  59.40   0.00   0.30    0.00    0.00 0.00 191.03  0.00   131.62  0.00   0.29
1 1 1 0  1  13 o 4  56.14   9.21  53.87   90.00  0.00    0.02    0.00 0.00 52.30   270.00 106.18  270.00 0.02
1 1 1 0  1  14 o 7  515.54  8.59  138.24  0.00   0.53    0.00    0.00 0.00 197.41  0.00   59.17   0.00   0.48
1 1 1 0  1  15 o 3  305.27  7.30  0.69    270.00 -0.00   -0.00   0.00 0.00 0.26    270.00 0.43    90.00  0.00
1 1 1 0  1  16 o 4  794.52  10.90 477.61  180.00 -4.16   0.00    0.00 0.00 586.25  180.00 108.64  180.00 1.00
1 1 1 0  1  17 o 8  533.50  8.74  423.11  90.00  0.00    3.06    0.00 0.00 285.89  90.00  137.22  270.00 1.00
1 1 1 0  1  18 o 19 1287.12 15.65 613.36  180.00 -10.31  0.00    0.00 0.00 1098.06 180.00 484.70  180.00 1.00
  """
  hkl_file = open_tmp_file(suffix=".cif")
  hkl_file.write(cif_text)
  hkl_file.close()
  cmd = " ".join([ "phenix.cif_as_mtz", "%s"%hkl_file.name ])
  result = easy_run.fully_buffered(cmd).raise_if_errors()
  assert (result.return_code == 0)
  mtz_name = os.path.splitext(hkl_file.name)[0]+".mtz"
  assert os.path.isfile(mtz_name)
  mas = reflection_file_reader.any_reflection_file(
    file_name=mtz_name).as_miller_arrays()
  labels = [ ma.info().label_string() for ma in mas ]
  assert (sorted(labels) == sorted(['FC,PHIFC', 'FOBS,SIGFOBS', 'FOM', 'HLA,HLB,HLC,HLD',
                              'R-free-flags', 'FWT,PHWT', 'DELFWT,PHDELWT', 'R-free-flags-1']))




if(__name__ == "__main__"):
  t0 = time.time()
  run()
  exercise_04()
  exercise_05()
  exercise_06()
  exercise_07()
  exercise_08()
  exercise_09()
  exercise_10()
  exercise_11()
  exercise_12()
  exercise_13()
  exercise_14()
  print("OK, phenix.cif_as_mtz, time: %-10.2f"%(time.time() - t0))
