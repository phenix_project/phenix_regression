from __future__ import division
from __future__ import print_function
from libtbx import easy_run
import libtbx.load_env
import iotbx.mtz
import os

def exercise_01():
  from iotbx import file_reader
  cif_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/cif_as_mtz/4dfx-sf.cif",
    test=os.path.isfile)
  result = easy_run.fully_buffered(
    "phenix.cif_as_mtz %s --extend_flags" % cif_file).raise_if_errors()
  assert ("R-free-flags: missing 1 reflections" in result.stdout_lines)
  mtz_in = iotbx.mtz.object("4dfx-sf.mtz")
  miller_arrays = mtz_in.as_miller_arrays()
  r_free_flags = miller_arrays[-1]
  assert (r_free_flags.data().size() == 99612)

def exercise_02():
  from libtbx import easy_run
  import libtbx.load_env
  import os
  from iotbx import file_reader
  cif_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/cif_as_mtz/2uwa-sf_part.cif",
    test=os.path.isfile)
  cmd = " ".join([
    "phenix.cif_as_mtz",
    "--merge",
    "--remove_systematic_absences",
    "--map_to_asu",
    "--ignore_bad_sigmas",
    "--extend_flags",
    "%s"%cif_file])
  print(cmd)
  result = easy_run.fully_buffered(cmd)
  #assert "WARNING: array of R-free flags (R-free-flags) is empty." in \
  #  result.stdout_lines
  mtz_in = iotbx.mtz.object("2uwa-sf_part.mtz")
  miller_arrays = mtz_in.as_miller_arrays()
  cntr = 0
  for ma in miller_arrays: # 341
    if(ma.info().labels == ['FOBS', 'SIGFOBS']):
      assert ma.data().size()==334
      cntr+=1
  assert cntr==1

if (__name__ == "__main__") :
  exercise_01()
  exercise_02()
  print("OK")
