
from __future__ import division
from __future__ import print_function
from libtbx.utils import null_out
from six.moves import cStringIO as StringIO
from libtbx.test_utils import show_diff
from mmtbx.command_line import cif_as_mtz
import iotbx.mtz
import sys

def exercise () :
  cif_raw = """\
data_r4j4qsf
#
_cell.entry_id      4j4q
_cell.length_a      241.2650
_cell.length_b      241.2650
_cell.length_c      109.5610
_cell.angle_alpha   90.0000
_cell.angle_beta    90.0000
_cell.angle_gamma   120.0000
#
_diffrn_radiation_wavelength.id           1
_diffrn_radiation_wavelength.wavelength   0.91840
#
_entry.id   4j4q
#
_exptl_crystal.id   1
#
_reflns_scale.group_code   1
#
_symmetry.entry_id               4j4q
_symmetry.space_group_name_H-M   'H 3 2'
_symmetry.Int_Tables_number      155
#
loop_
_refln.crystal_id
_refln.wavelength_id
_refln.scale_group_code
_refln.index_h
_refln.index_k
_refln.index_l
_refln.status
_refln.F_meas_au
_refln.F_meas_sigma_au
1 1 1    0    0    6 o   7930.4  109.8
1 1 1    0    0    9 o   2552.9   35.4
1 1 1    0    0   12 o    368.4   15.5
1 1 1    0    0   15 o   3249.1   45.1
1 1 1    0    0   18 o   2038.3   28.7
1 1 1    0    0   21 o   2404.1   33.7
1 1 1    0    0   24 o    253.7   53.9
1 1 1    0    0   27 f   2792.5   39.7
1 1 1    0    0   30 o    670.3   37.1
1 1 1    0    0   33 o    669.7   47.5
1 1 1    0    0   36 o    420.2   97.0
1 1 1    0    0   39 o    285.1  112.4
1 1 1    1    0  -41 h    288.8   90.8
1 1 1    1    0  -38 o    276.8   87.1
1 1 1    1    0  -35 o    337.8   64.4
1 1 1    1    0  -32 o   1159.7   17.0
1 1 1    1    0  -29 o    509.0   24.2
1 1 1    1    0  -26 o    686.4   15.2
1 1 1    1    0  -23 f   1489.2   13.1
1 1 1    1    0  -20 o   2293.6   18.6
1 1 1    1    0  -17 o   3205.8   25.7
"""
  cif_file = "tst_cif_as_mtz_phil.cif"
  mtz_file = "tst_cif_as_mtz_phil.mtz"
  open(cif_file, "w").write(cif_raw)
  args = [ "cif_file=%s" % cif_file, "space_group=H32",
    "unit_cell=241.265,241.265,109.56,90,90,120", ]
  _stderr = sys.stderr
  sys.stderr = StringIO()
  cif_as_mtz.run2(args=args, log=null_out())
  err = sys.stderr.getvalue()
  sys.stderr = _stderr
  mtz_in = iotbx.mtz.object(mtz_file)
  ma = mtz_in.as_miller_arrays()
  if ma[0].info().label_string() =="FreeR_flag":
    flags = ma[0]
    fobs =  ma[1]
  else:
    flags = ma[1]
    fobs =  ma[0]
  assert len(flags.indices()) == len(fobs.indices()) - 1
  args.append("extend_flags=True")
  cif_as_mtz.run2(args=args, log=null_out())
  mtz_in = iotbx.mtz.object(mtz_file)
  ma = mtz_in.as_miller_arrays()
  if ma[0].info().label_string() =="FreeR_flag":
    flags = ma[0]
    fobs =  ma[1]
  else:
    flags = ma[1]
    fobs =  ma[0]
  assert len(flags.indices()) == len(fobs.indices())

if (__name__ == "__main__") :
  exercise()
  print("OK")
