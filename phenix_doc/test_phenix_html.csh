#! /bin/csh -f
echo "Testing phenix html generation"
phenix_html.rebuild_docs | tee test.log
tail -1 test.log| libtbx.assert_stdin '  copying images'
if ($status == 0) then
  rm -f test.log
  echo "OK"
endif
