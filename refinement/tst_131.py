from __future__ import print_function
from six.moves import cStringIO as StringIO
from libtbx import easy_run
import libtbx.load_env
import libtbx.path
import time, sys, os
import iotbx.pdb
#from phenix_regression.refinement import get_r_factors
from libtbx.test_utils import approx_equal, show_diff
import iotbx.cif
import mmtbx.f_model
from iotbx import reflection_file_reader
import mmtbx.utils
from scitbx.array_family import flex
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel

pdb_str="""
CRYST1   46.586   53.307  113.275  90.00 101.00  90.00 I 1 2 1
SCALE1      0.021466  0.000000  0.004173        0.00000
SCALE2      0.000000  0.018759  0.000000        0.00000
SCALE3      0.000000  0.000000  0.008993        0.00000
HETATM    1  C  AACE C   1     -19.921   5.052  17.046  0.54 49.09           C
HETATM    2  O  AACE C   1     -21.054   4.858  17.310  0.54 53.76           O
HETATM    3  CH3AACE C   1     -19.078   5.959  17.934  0.54 57.79           C
HETATM    4  C  BACE C   1     -19.970   5.083  17.170  0.46 49.18           C
HETATM    5  O  BACE C   1     -21.128   4.972  17.374  0.46 54.28           O
HETATM    6  CH3BACE C   1     -19.110   5.930  18.100  0.46 58.49           C
ATOM      7  N  ALEU C   2     -19.314   4.439  15.874  0.97 45.46           N
ATOM      8  CA ALEU C   2     -20.069   3.571  14.996  0.97 40.66           C
ATOM      9  C  ALEU C   2     -19.386   2.183  14.977  0.97 35.20           C
ATOM     10  O  ALEU C   2     -18.296   2.095  14.520  0.97 31.86           O
ATOM     11  CB ALEU C   2     -20.128   4.132  13.569  0.97 52.79           C
ATOM     12  CG ALEU C   2     -21.564   4.510  13.110  0.97 45.24           C
ATOM     13  CD1ALEU C   2     -22.589   3.417  13.440  0.97 51.63           C
ATOM     14  CD2ALEU C   2     -21.967   5.837  13.786  0.97 48.18           C
ATOM     15  N  BLEU C   2     -19.343   4.434  16.035  0.03 45.49           N
ATOM     16  CA BLEU C   2     -20.082   3.613  15.102  0.03 40.96           C
ATOM     17  C  BLEU C   2     -19.385   2.241  15.018  0.03 35.58           C
ATOM     18  O  BLEU C   2     -18.314   2.180  14.527  0.03 32.47           O
ATOM     19  CB BLEU C   2     -20.133   4.273  13.724  0.03 50.41           C
ATOM     20  CG BLEU C   2     -21.441   3.877  12.990  0.03 47.17           C
ATOM     21  CD1BLEU C   2     -22.611   4.627  13.651  0.03 46.33           C
ATOM     22  CD2BLEU C   2     -21.312   4.280  11.514  0.03 49.74           C
ATOM     23  N  ALEU C   3     -20.057   0.990  15.501  0.97 31.40           N
ATOM     24  CA ALEU C   3     -19.388  -0.309  15.464  0.97 35.22           C
ATOM     25  C  ALEU C   3     -19.335  -0.790  14.016  0.97 41.65           C
ATOM     26  O  ALEU C   3     -20.291  -0.664  13.328  0.97 39.26           O
ATOM     27  CB ALEU C   3     -20.154  -1.313  16.330  0.97 29.29           C
ATOM     28  CG ALEU C   3     -20.378  -0.732  17.759  0.97 33.66           C
ATOM     29  CD1ALEU C   3     -20.910  -1.836  18.695  0.97 38.80           C
ATOM     30  CD2ALEU C   3     -19.053  -0.193  18.336  0.97 33.85           C
ATOM     31  N  BLEU C   3     -20.044   1.038  15.540  0.03 32.05           N
ATOM     32  CA BLEU C   3     -19.399  -0.265  15.468  0.03 35.32           C
ATOM     33  C  BLEU C   3     -19.379  -0.731  14.018  0.03 41.14           C
ATOM     34  O  BLEU C   3     -20.357  -0.607  13.359  0.03 39.58           O
ATOM     35  CB BLEU C   3     -20.176  -1.272  16.320  0.03 29.67           C
ATOM     36  CG BLEU C   3     -20.380  -0.720  17.760  0.03 33.53           C
ATOM     37  CD1BLEU C   3     -20.902  -1.825  18.699  0.03 38.31           C
ATOM     38  CD2BLEU C   3     -19.043  -0.200  18.329  0.03 33.71           C
HETATM   39  N  AAR7 C   4     -18.107  -1.405  13.483  0.26 32.48           N
HETATM   40  CA AAR7 C   4     -18.050  -1.859  12.089  0.26 31.14           C
HETATM   41  C  AAR7 C   4     -17.654  -3.302  11.955  0.26 37.66           C
HETATM   42  O  AAR7 C   4     -18.642  -4.084  12.589  0.26 68.99           O
HETATM   43  CB AAR7 C   4     -17.107  -0.932  11.291  0.26 39.11           C
HETATM   44  CG AAR7 C   4     -17.603   0.541  11.352  0.26 45.43           C
HETATM   45  CD AAR7 C   4     -18.597   0.860  10.187  0.26 53.37           C
HETATM   46  NE AAR7 C   4     -19.981   0.749  10.666  0.26 66.13           N
HETATM   47  CZ AAR7 C   4     -21.097   1.261   9.845  0.26 80.68           C
HETATM   48  NH1AAR7 C   4     -20.827   1.886   8.549  0.26 86.13           N
HETATM   49  NH2AAR7 C   4     -22.278   1.162  10.258  0.26 78.06           N
HETATM   50  OXTAAR7 C   4     -17.651  -3.643  10.585  0.26 45.88           O
HETATM   51  N  BAR7 C   4     -18.153  -1.330  13.474  0.74 32.17           N
HETATM   52  CA BAR7 C   4     -18.075  -1.791  12.085  0.74 30.27           C
HETATM   53  C  BAR7 C   4     -17.663  -3.229  11.949  0.74 37.53           C
HETATM   54  O  BAR7 C   4     -18.671  -4.021  12.533  0.74 69.67           O
HETATM   55  CB BAR7 C   4     -17.121  -0.855  11.306  0.74 38.84           C
HETATM   56  CG BAR7 C   4     -17.580   0.628  11.388  0.74 45.43           C
HETATM   57  CD BAR7 C   4     -18.592   0.959  10.244  0.74 53.30           C
HETATM   58  NE BAR7 C   4     -19.962   0.673  10.694  0.74 66.88           N
HETATM   59  CZ BAR7 C   4     -21.109   1.178   9.915  0.74 81.70           C
HETATM   60  NH1BAR7 C   4     -20.871   1.956   8.699  0.74 88.03           N
HETATM   61  NH2BAR7 C   4     -22.283   0.940  10.292  0.74 79.58           N
HETATM   62  OXTBAR7 C   4     -17.630  -3.532  10.575  0.74 45.98           O
TER
END
"""

works = '''
CRYST1   46.586   53.307  113.275  90.00 101.00  90.00 I 1 2 1
SCALE1      0.021466  0.000000  0.004173        0.00000
SCALE2      0.000000  0.018759  0.000000        0.00000
SCALE3      0.000000  0.000000  0.008993        0.00000
HETATM    1  C   ACE C   1     -19.921   5.052  17.046  0.54 49.09           C
HETATM    2  O   ACE C   1     -21.054   4.858  17.310  0.54 53.76           O
HETATM    3  CH3 ACE C   1     -19.078   5.959  17.934  0.54 57.79           C
ATOM      7  N   LEU C   2     -19.314   4.439  15.874  0.97 45.46           N
ATOM      8  CA  LEU C   2     -20.069   3.571  14.996  0.97 40.66           C
ATOM      9  C   LEU C   2     -19.386   2.183  14.977  0.97 35.20           C
ATOM     10  O   LEU C   2     -18.296   2.095  14.520  0.97 31.86           O
ATOM     11  CB  LEU C   2     -20.128   4.132  13.569  0.97 52.79           C
ATOM     12  CG  LEU C   2     -21.564   4.510  13.110  0.97 45.24           C
ATOM     13  CD1 LEU C   2     -22.589   3.417  13.440  0.97 51.63           C
ATOM     14  CD2 LEU C   2     -21.967   5.837  13.786  0.97 48.18           C
ATOM     23  N   LEU C   3     -20.057   0.990  15.501  0.97 31.40           N
ATOM     24  CA  LEU C   3     -19.388  -0.309  15.464  0.97 35.22           C
ATOM     25  C   LEU C   3     -19.335  -0.790  14.016  0.97 41.65           C
ATOM     26  O   LEU C   3     -20.291  -0.664  13.328  0.97 39.26           O
ATOM     27  CB  LEU C   3     -20.154  -1.313  16.330  0.97 29.29           C
ATOM     28  CG  LEU C   3     -20.378  -0.732  17.759  0.97 33.66           C
ATOM     29  CD1 LEU C   3     -20.910  -1.836  18.695  0.97 38.80           C
ATOM     30  CD2 LEU C   3     -19.053  -0.193  18.336  0.97 33.85           C
HETATM   39  N   AR7 C   4     -18.107  -1.405  13.483  0.26 32.48           N
HETATM   40  CA  AR7 C   4     -18.050  -1.859  12.089  0.26 31.14           C
HETATM   41  C   AR7 C   4     -17.654  -3.302  11.955  0.26 37.66           C
HETATM   42  O   AR7 C   4     -18.642  -4.084  12.589  0.26 68.99           O
HETATM   43  CB  AR7 C   4     -17.107  -0.932  11.291  0.26 39.11           C
HETATM   44  CG  AR7 C   4     -17.603   0.541  11.352  0.26 45.43           C
HETATM   45  CD  AR7 C   4     -18.597   0.860  10.187  0.26 53.37           C
HETATM   46  NE  AR7 C   4     -19.981   0.749  10.666  0.26 66.13           N
HETATM   47  CZ  AR7 C   4     -21.097   1.261   9.845  0.26 80.68           C
HETATM   48  NH1 AR7 C   4     -20.827   1.886   8.549  0.26 86.13           N
HETATM   49  NH2 AR7 C   4     -22.278   1.162  10.258  0.26 78.06           N
HETATM   50  OXT AR7 C   4     -17.651  -3.643  10.585  0.26 45.88           O
TER
END
'''

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Probe failure with "Segmentation fault".
  """
  root_name = prefix
  pdb="%s.pdb"%prefix
  with open(pdb,"w") as fo:
    fo.write(pdb_str)
  cmd = 'phenix.probe %s' % pdb
  rc = easy_run.go(cmd)
  print('\n'.join(rc.stdout_lines))
  print(rc.stderr_lines)
  assert len(rc.stdout_lines)>1, 'probe fails on alt loc model'

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK. Time: %8.3f"%(time.time()-t0))
