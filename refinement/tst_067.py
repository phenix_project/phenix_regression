from __future__ import division
from __future__ import print_function
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel
import libtbx.load_env
import os
from cctbx.array_family import flex
from mmtbx import utils
import iotbx.pdb
from libtbx.test_utils import approx_equal, not_approx_equal

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Occupancy of H in refinement.
  """
  pdb_in = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/phe_altloc_ab.pdb",
    test=os.path.isfile)
  args = [
    pdb_in,
    'high_resolution=1.5',
    'label=FOBS',
    'type=real',
    'r_free=0.1']
  r = run_fmodel(args = args, prefix = prefix)
  args = [pdb_in, r.mtz]
  r = run_phenix_refine(args = args, prefix=prefix)
  occ = iotbx.pdb.input(file_name = r.pdb).xray_structure_simple().\
    scatterers().extract_occupancies()
  sel_occ_A = utils.get_atom_selection(pdb_file_name = r.pdb,
    selection_string = "altloc A")
  sel_occ_B = utils.get_atom_selection(pdb_file_name = r.pdb,
    selection_string = "altloc B")
  occ_A = occ.select(sel_occ_A)
  occ_B = occ.select(sel_occ_B)
  assert not_approx_equal(occ_A, occ_B)
  assert approx_equal(flex.min(occ_A-occ_A), 0)
  assert approx_equal(flex.min(occ_B-occ_B), 0)
  assert approx_equal( (occ_A+occ_B).min_max_mean().as_tuple(), (1.,1.,1.) )

if (__name__ == "__main__"):
  run()
  print("OK")
