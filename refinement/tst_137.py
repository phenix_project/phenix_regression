from __future__ import print_function
from libtbx.test_utils import run_command
from libtbx.utils import search_for
import libtbx.load_env
import time
import sys, os
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel
from libtbx.test_utils import assert_lines_in_file

pdb_str = """
CRYST1   32.095   63.561   36.911  90.00  90.00  90.00 P 21 21 21
ATOM  74850  N   ASP G  20      73.398  -3.845  13.424  1.00184.24      G    N
ATOM  74851  CA  ASP G  20      72.045  -3.502  12.997  1.00153.33      G    C
ATOM  74852  CB  ASP G  20      72.001  -3.179  11.504  1.00231.15      G    C
ATOM  74853  CG  ASP G  20      72.561  -1.804  11.189  1.00149.54      G    C
ATOM  74854  OD1 ASP G  20      73.051  -1.132  12.121  1.00188.93      G    O
ATOM  74855  OD2 ASP G  20      72.514  -1.396  10.010  1.00177.19      G    O
ATOM  74856  C   ASP G  20      71.130  -4.670  13.332  1.00184.59      G    C
ATOM  74857  O   ASP G  20      69.905  -4.556  13.303  1.00155.95      G    O
ATOM  74862  N   SER G  21      71.756  -5.794  13.658  1.00114.38      G    N
ATOM  74863  CA  SER G  21      71.058  -6.986  14.108  1.00 99.15      G    C
ATOM  74864  CB  SER G  21      70.342  -7.659  12.940  1.00198.11      G    C
ATOM  74865  OG  SER G  21      69.802  -8.910  13.328  1.00275.75      G    O
ATOM  74866  C   SER G  21      72.092  -7.929  14.698  1.00183.14      G    C
ATOM  74867  O   SER G  21      73.062  -8.274  14.034  1.00160.19      G    O
ATOM  75050  N   GLY G  33      75.286  -8.720  12.669  1.00233.54      G    N
ATOM  75051  CA  GLY G  33      75.048  -8.417  11.273  1.00133.05      G    C
ATOM  75052  C   GLY G  33      75.203  -6.945  10.946  1.00197.10      G    C
ATOM  75053  O   GLY G  33      74.716  -6.059  11.671  1.00312.04      G    O
END
"""

slack_geo_edits = """
refinement.geometry_restraints.edits {
  bond {
    action = *add delete change
    atom_selection_1 = "chain G and resname ASP and resid 20 and name N"
    atom_selection_2 = "chain G and resname GLY and resid 33 and name O"
    symmetry_operation = None
    distance_ideal = 2.8
    sigma = 0.1
    slack = 0.3
  }
  bond {
    action = *add delete change
    atom_selection_1 = "chain G and resname GLY and resid 33 and name N"
    atom_selection_2 = "chain G and resname SER and resid 21 and name O"
    symmetry_operation = None
    distance_ideal = 2.8
    sigma = 0.1
    slack = 0.4
  }
}
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Test bond slack
  """
  with open("%s.pdb"%prefix, "w") as fo:
    fo.write(pdb_str)
  with open("%s.edits"%prefix, "w") as fo:
    fo.write(slack_geo_edits)
  #
  args = ["%s.pdb"%prefix, "high_res=5 type=real r_free=0.1"]
  r = run_fmodel(args = args, prefix = prefix)
  #
  args = ["%s.pdb"%prefix, r.mtz, "%s.edits"%prefix]
  r = run_phenix_refine(args = args, prefix = prefix)
  #
  lines = search_for(
    pattern="    Total number of added/changed bonds: 2",
    mode="==",
    file_name=r.log)
  assert len(lines) == 1, 'lines %s' % lines
  lines = search_for(
    pattern="  slack  ",
    mode="find",
    file_name=r.geo)
  assert len(lines) == 2
  txt = "\n".join(open(r.geo).read().splitlines())
  assert txt.find("""\
bond pdb=" N   ASP G  20 " segid="G   "
     pdb=" O   GLY G  33 " segid="G   "
  ideal  model  slack  delta    sigma   weight residual
  2.800  3.116  0.300 -0.316 1.00e-01 1.00e+02 2.69e-02
""") > 0
  assert txt.find("""\
bond pdb=" O   SER G  21 " segid="G   "
     pdb=" N   GLY G  33 " segid="G   "
  ideal  model  slack  delta    sigma   weight residual
  2.800  2.647  0.400  0.153 1.00e-01 1.00e+02 0.00e+00
""") > 0

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK", "time: %.2f"%(time.time()-t0))

