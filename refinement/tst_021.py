from __future__ import division
from __future__ import print_function
from libtbx.test_utils import assert_lines_in_file
import libtbx.load_env
import os.path
from phenix_regression.refinement import run_phenix_refine

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Exercise harmonic reference restraints.
  """
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/1yjp_h.pdb",
    test=os.path.isfile)
  mtz_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/1yjp.mtz",
    test=os.path.isfile)
  args = [
    pdb_file,
    mtz_file,
    "main.simulated_annealing=True",
    "reference_coordinate_restraints.enabled=True",
    "reference_coordinate_restraints.selection=\"not (name H)\"",
    "reference_coordinate_restraints.sigma=0.02",
    "xray_data.high_resolution=3.0",
    "main.number_of_macro_cycles=1"]
  r = run_phenix_refine(args = args, prefix = prefix)
  assert_lines_in_file(file_name=r.log,
    lines="Number of reference coordinate restraints generated: 108")
  import iotbx.pdb
  pdb_start = iotbx.pdb.input(file_name=pdb_file)
  pdb_end = iotbx.pdb.input(r.pdb)
  sites_start = pdb_start.construct_hierarchy().atoms().extract_xyz()
  sites_end = pdb_end.construct_hierarchy().atoms().extract_xyz()
  rmsd = sites_start.rms_difference(sites_end)
  assert (rmsd < 0.1), rmsd

if (__name__ == "__main__") :
  run()
  print("OK")
