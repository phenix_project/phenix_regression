from __future__ import division
from __future__ import print_function
from phenix_regression.refinement import run_phenix_refine
import os
import libtbx.load_env

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Handling anomalous scatterers (group anomalous refinement).
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/l.pdb", test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/l.mtz",
    test=os.path.isfile)
  params = """\
data_manager { 
  fmodel {
    xray_data {
      high_resolution=3
      low_resolution=6
    }
  }
}
refinement {
  main {
    number_of_macro_cycles=2
    max_number_of_iterations=2
    bulk_solvent_and_scale = False
  }
  electron_density_maps {
    map_coefficients {
      map_type = mFo-DFc
      format = *mtz *phs
      mtz_label_amplitudes = FOFCWT
      mtz_label_phases = PHFOFCWT
    }
    map {
      map_type = 2mFo-DFc
      region = selection *cell
      atom_selection_buffer = 3
      #sharpening = True
    }
  }
}
refinement.refine.strategy = *group_anomalous
refinement.refine.anomalous_scatterers {
  group {
    selection = name AU
    f_prime = -2
    f_double_prime = 3
    refine = *f_prime *f_double_prime
  }
  group {
    selection = name SG or name SD
    f_prime = -1
    f_double_prime = 2
    refine = f_prime *f_double_prime
  }
}
  """
  phil = "%s.eff"%prefix
  open(phil, "w").write(params)
  args = [hkl, pdb, phil]
  r = run_phenix_refine(args = args, prefix = prefix)
  assert os.path.isfile("%s_001_mFo-DFc.phs"%prefix)

if (__name__ == "__main__"):
  run()
  print("OK")
