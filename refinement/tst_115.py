from __future__ import print_function
from phenix_regression.refinement import get_r_factors
import libtbx.load_env
from libtbx.test_utils import approx_equal, not_approx_equal, run_command
import sys, os, time, random
from mmtbx import utils
from cctbx.array_family import flex
import iotbx.pdb
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel
import os

if (1):
  random.seed(0)
  flex.set_random_seed(0)

pdb_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="pdb",
  test=os.path.isdir)

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Occupancies.
  """
  pdb_file = os.path.join(pdb_dir, "phe_h.pdb")
  args = [
    pdb_file,
    'high_resolution=1.7',
    'algorithm=direct',
    'label=FOBS',
    'type=real']
  r0 = run_fmodel(args = args, prefix = prefix)
  pdb_in = os.path.join(pdb_dir, "phe_h_bad.pdb")
  for option in ['', 'hydrogens.refine=individual']:
    args = [
      'strategy=occupancies',
      pdb_in,
      'main.target=ls',
      'main.bulk_solvent_and_=false',
      'ls_target_names.target_name=ls_wunit_kunit',
      r0.mtz,
      'main.number_of_macro_cycles=30',
      'output.prefix=occ_exercise_08',
      'structure_factors_and_gradients_accuracy.algorithm=direct',
      'xray_data.r_free_flags.ignore_r_free_flags=true',
      'xray_data.r_free_flags.generate=True',
      "xray_data.outliers_rejection=false",
      "neutron_data.outliers_rejection=false",
      'optimize_scattering_contribution=false',
      'correct_hydrogens=False']
    if  option:
      args.append('%s'%option)
    r = run_phenix_refine(args = args, prefix = prefix)
    assert r.r_work_start > 0.1
    assert r.r_free_start > 0.1
    if(len(option) == 0):
      r.r_work_final > 0.02
      r.r_free_final > 0.02
    else:
      assert approx_equal(r.r_work_final, 0.0)
      assert approx_equal(r.r_free_final, 0.0)

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK", "time: %.2f"%(time.time()-t0))
