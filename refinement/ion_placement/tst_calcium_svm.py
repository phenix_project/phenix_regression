
from __future__ import division
from __future__ import print_function
from libtbx import easy_run
import os
import warnings
import iotbx.pdb

def exercise (prefix="ion_placement_tst_calcium_svm") :
  from mmtbx.regression import make_fake_anomalous_data
  mtz_file, pdb_file = make_fake_anomalous_data.generate_calcium_inputs(
      file_base=prefix)
  args = [pdb_file, mtz_file, "wavelength=1.12", "main.nproc=1", "use_svm=True",
    "place_ions=CA", "main.number_of_macro_cycles=1", "--overwrite",]
  cmd = "phenix.refine %s" % " ".join(args)
  print(cmd)
  result = easy_run.fully_buffered(cmd)
  # assert (result.return_code == 0), "\n".join(result.stdout_lines)
  # assert ("""  pdb=" O   HOH S   1 " becomes CA+2""" in result.stdout_lines)
  # assert ("""    setting f'=0.335, f''=0.7239""" in result.stdout_lines)
  assert os.path.isfile("ion_placement_tst_calcium_svm_hoh_refine_001.pdb")
  pdb_out = iotbx.pdb.input("ion_placement_tst_calcium_svm_hoh_refine_001.pdb")
  n_ca = 0
  for atom in pdb_out.construct_hierarchy().atoms() :
    if (atom.element.upper() == "CA") :
      n_ca += 1
  assert (n_ca == 1)
  print("OK")

if (__name__ == "__main__") :
  try :
    from libsvm import svm
    from libsvm import svmutil
  except ImportError :
    warnings.warn("libsvm not available, skipping this test")
  else :
    exercise()
