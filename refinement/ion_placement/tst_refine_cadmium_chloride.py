
from __future__ import division
from __future__ import print_function
from libtbx.utils import null_out
from libtbx import group_args
from libtbx import easy_run
import iotbx.pdb
import time

def exercise () :
  from phenix.refinement import command_line
  from mmtbx.regression import make_fake_anomalous_data
  import mmtbx.ions.utils
  file_base = "cd_cl_frag"
  pdb_file = make_fake_anomalous_data.write_pdb_input_cd_cl(file_base=file_base)
  mtz_file = make_fake_anomalous_data.generate_mtz_file(
    file_base="cd_cl_frag",
    d_min=1.0,
    anomalous_scatterers=[
      group_args(selection="element CD", fp=-0.29, fdp=2.676),
      group_args(selection="element CL", fp=0.256, fdp=0.5),
    ])
  pdb_in = iotbx.pdb.input(pdb_file)
  hierarchy = pdb_in.construct_hierarchy()
  hierarchy, n = mmtbx.ions.utils.anonymize_ions(hierarchy, log=null_out())
  hierarchy.write_pdb_file("%s_start.pdb" % file_base,
    crystal_symmetry=pdb_in.crystal_symmetry())
  args = [
    "%s.mtz" % file_base,
    "%s_start.pdb" % file_base,
    "refine.strategy=individual_sites+individual_adp",
    "place_ions=CD,CL",
    "wavelength=1.116",
    "max_anom_level=5",
    "--developer",
    "--overwrite",
  ]
  result = easy_run.fully_buffered(
    "phenix.refine %s" % " ".join(args)).raise_if_errors()
  assert result.return_code == 0, "\n".join(result.stdout_lines)
  pdb_in = iotbx.pdb.input("cd_cl_frag_start_refine_001.pdb")
  hierarchy = pdb_in.construct_hierarchy()
  n_cd = n_cl = 0
  for atom in hierarchy.atoms() :
    if (atom.element.upper() == "CD") :
      n_cd += 1
    elif (atom.element.upper() == "CL") :
      n_cl += 1
  # FIXME very unpredictable due to map artifacts
  assert (n_cd == 1) #n_cl == 1)
  print("OK")

if (__name__ == "__main__") :
  print("WARNING: TEST TOO SLOW. MAKE IT RUN UNDER 300s AND ENABLE BACK.")
  if 0: #XXX FIXME disabled
    t0 = time.time()
    exercise()
    print("Time: %6.2f"%(time.time()-t0))
    print("OK")
