
from __future__ import division
from __future__ import print_function
from libtbx import easy_run

def exercise (prefix="ion_placement_refine_calcium_simple") :
  #
  # UNSTABLE!!! 7x
  # Failing in make_fake_anomalous_data. Maybe race conditions?
  #
  from mmtbx.regression import make_fake_anomalous_data
  from iotbx import pdb
  mtz_file, pdb_file = make_fake_anomalous_data.generate_calcium_inputs(
      file_base=prefix)
  args = [pdb_file, mtz_file, "wavelength=1.12", "refinement.main.nproc=1",
    "place_ions=Auto", "main.number_of_macro_cycles=1", "--overwrite",
    "use_svm=False"]
  cmd = "phenix.refine %s" % " ".join(args)
  print(cmd)
  # assert not easy_run.call(cmd)
  result = easy_run.fully_buffered(cmd).raise_if_errors()
  assert (result.return_code == 0), "\n".join(result.stdout_lines)
  assert ("""  pdb=" O   HOH S   1 " segid="CA  " becomes CA+2""" in result.stdout_lines)
  assert ("""    setting f'=0.335, f''=0.7239""" in result.stdout_lines)
  pdb_out = pdb.input("ca_frag_hoh_refine_001.pdb")
  n_ca = 0
  for atom in pdb_out.construct_hierarchy().atoms() :
    if (atom.element.upper() == "CA") :
      n_ca += 1
  assert (n_ca == 1)
  print("OK")

if (__name__ == "__main__") :
  # The test is unstable, run couple times before debugging
  exercise()
