from __future__ import division
from __future__ import print_function
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel
import os
import iotbx.pdb

pdb_str="""
CRYST1   11.867   12.861    8.990  90.00  90.00  90.00 P 1
ATOM      1  CA  TYR A   1       7.810   9.507   3.648  1.00 25.00           C
ATOM     12  N   TYR A   1       7.348  10.861   3.366  1.00 25.00           N
TER
ATOM     13  CA  GLY B   2       5.404   5.540   3.119  1.00 25.00           C
ATOM     20  N   GLY B   2       5.495   5.355   1.683  1.00 25.00           N
TER
ATOM     21  CA  PHE C   4       3.130   8.183   6.990  1.00 25.00           C
ATOM     30  N   PHE C   4       4.186   7.758   6.079  1.00 25.00           N
TER
ATOM     32  CA  LEU D   5       1.981   7.489   1.963  1.00 25.00           C
ATOM     38  N   LEU D   5       2.399   7.359   3.353  1.00 25.00           N
TER
ATOM     41  O   HOH E   1      10.346   1.776   3.872  1.00 25.00           O
ATOM     42  O   HOH E   2       9.686   4.492   3.890  1.00 25.00           O
ATOM     43  O   HOH E   3       7.499   2.963   4.068  1.00 25.00           O
TER
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Refined individual XYZ
  """
  pdb_in = "%s.pdb"%prefix
  open(pdb_in, "w").write(pdb_str)
  #
  args = [
    pdb_in,
    "type=real",
    "r_free=0.05",
    "high_res=3"]
  r0 = run_fmodel(args = args, prefix = prefix)
  #
  for sel_refine in ["None", "chain A or chain B or chain C"]:
    for sel_modify in ["None", "chain A or chain B or chain C"]:
      args = [
        pdb_in,
        r0.mtz,
        "const_shrink_donor_acceptor=0.6",
        "refinement.refine.strategy=individual_sites",
        "refinement.refine.sites.individual=%s"%sel_refine,
        "refinement.main.fake_f_obs=true",
        "refinement.modify_start_model.modify.sites.shake=0.5",
        "refinement.main.bulk_solvent_and_scale=false",
        "refinement.modify_start_model.modify.selection=%s"%sel_modify,
        "refinement.main.scattering_table=it1992",
        "refinement.structure_factors_and_gradients_accuracy.cos_sin_table=false",
        "refinement.structure_factors_and_gradients_accuracy.algorithm=direct",
        "refinement.fake_f_obs.structure_factors_accuracy.cos_sin_table=false",
        "refinement.fake_f_obs.structure_factors_accuracy.algorithm=direct",
        "refinement.main.number_of_macro_cycles=5",
        "refinement.main.target=ls",
        "refinement.main.random_seed=1312425"]
      r = run_phenix_refine(args = args, prefix = prefix)
      #
      assert r.r_work_start > 0.1
      if(sel_refine == sel_modify or sel_refine=="None"):
        assert r.r_work_final < 0.001
      else:
        assert r.r_work_final > 0.1

if (__name__ == "__main__"):
  run()
  print("OK")
