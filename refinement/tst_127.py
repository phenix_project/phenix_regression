from __future__ import print_function
from six.moves import cStringIO as StringIO
from libtbx import easy_run
import libtbx.load_env
import libtbx.path
import time, sys, os
import iotbx.pdb
#from phenix_regression.refinement import get_r_factors
from libtbx.test_utils import approx_equal, show_diff
import iotbx.cif
import mmtbx.f_model
from iotbx import reflection_file_reader
import mmtbx.utils
from scitbx.array_family import flex
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel

pdb_str="""
CRYST1   15.000   15.000   15.000  90.00  90.00  90.00 P 1
SCALE1      0.066667  0.000000  0.000000        0.00000
SCALE2      0.000000  0.066667  0.000000        0.00000
SCALE3      0.000000  0.000000  0.066667        0.00000
ATOM      1  CB  PHE H   1       7.767   5.853   7.671  1.00 15.00           C
ANISOU    1  CB  PHE H   1     1900   1900   1900      0      0      0       C
ATOM      2  CG  PHE H   1       6.935   5.032   8.622  1.00 15.00           C
ANISOU    2  CG  PHE H   1     1900   1900   1900      0      0      0       C
ATOM      3  CD1 PHE H   1       5.918   4.176   8.140  1.00 15.00           C
ANISOU    3  CD1 PHE H   1     1900   1900   1900      0      0      0       C
ATOM      4  CD2 PHE H   1       7.161   5.107  10.012  1.00 15.00           C
ANISOU    4  CD2 PHE H   1     1900   1900   1900      0      0      0       C
ATOM      5  CE1 PHE H   1       5.126   3.395   9.038  1.00 15.00           C
ANISOU    5  CE1 PHE H   1     1900   1900   1900      0      0      0       C
ATOM      6  CE2 PHE H   1       6.382   4.336  10.930  1.00 15.00           C
ANISOU    6  CE2 PHE H   1     1900   1900   1900      0      0      0       C
ATOM      7  CZ  PHE H   1       5.360   3.476  10.439  1.00 15.00           C
ANISOU    7  CZ  PHE H   1     1900   1900   1900      0      0      0       C
ATOM      8  C   PHE H   1       7.956   7.811   6.133  1.00 15.00           C
ANISOU    8  C   PHE H   1     1900   1900   1900      0      0      0       C
ATOM      9  O   PHE H   1       8.506   7.237   5.169  1.00 15.00           O
ANISOU    9  O   PHE H   1     1900   1900   1900      0      0      0       O
ATOM     10  OXT PHE H   1       8.143   9.010   6.428  1.00 15.00           O
ANISOU   10  OXT PHE H   1     1900   1900   1900      0      0      0       O
ATOM     11  H1  PHE H   1       6.253   5.840   5.439  1.00 15.00           H
ANISOU   11  H1  PHE H   1     1900   1900   1900      0      0      0       H
ATOM     12  H2  PHE H   1       5.364   7.253   5.745  1.00 15.00           H
ANISOU   12  H2  PHE H   1     1900   1900   1900      0      0      0       H
ATOM     13  N   PHE H   1       5.875   6.461   6.183  1.00 15.00           N
ANISOU   13  N   PHE H   1     1900   1900   1900      0      0      0       N
ATOM     14  H3  PHE H   1       5.216   5.927   6.782  1.00 15.00           H
ANISOU   14  H3  PHE H   1     1900   1900   1900      0      0      0       H
ATOM     15  CA  PHE H   1       7.000   7.000   7.000  1.00 15.00           C
ANISOU   15  CA  PHE H   1     1900   1900   1900      0      0      0       C
TER
END
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  CIF output.
  """
  root_name = prefix
  pdb="%s.pdb"%prefix
  with open(pdb,"w") as fo:
    fo.write(pdb_str)
  args = [
    pdb,
    "high_res=2.0",
    "r_free=0.1",
    "type=real",
    'label=F-obs',
    "random_seed=2679941"]
  r0 = run_fmodel(args = args, prefix = prefix)
  hkl = r0.mtz
  args = [
    pdb,
    r0.mtz,
    "modify_start_model.modify.sites.shake=0.3",
    "wavelength=0.9792",
    "strategy=none",
    "main.bulk_solve=False",
    "xray_data.outliers_rejection=false",
    "neutron_data.outliers_rejection=false",
    'optimize_scattering_contribution=false',
    "main.number_of_mac=0",
    "write_model_cif_file=true",
    "write_reflection_cif_file=true",
    "main.random_seed=2679941"]
  r = run_phenix_refine(args = args, prefix = prefix)
  assert approx_equal(r.r_work_final, 0.2, 0.03)
  #
  cif_input = iotbx.pdb.input(file_name=r.cif)
  wl = cif_input.extract_wavelength()
  assert approx_equal(wl, 0.9792, eps=0.00001)
  hierarchy_cif= cif_input.construct_hierarchy()
  pdb_input = iotbx.pdb.input(file_name=r.pdb)
  hierarchy_pdb = pdb_input.construct_hierarchy()
  s1 = StringIO()
  s2 = StringIO()
  hierarchy_cif.show(out=s1)
  hierarchy_pdb.show(out=s2)
  assert not show_diff(s1.getvalue(), s2.getvalue())
  cif_model = iotbx.cif.reader(file_path=r.cif).model()
  expected_keys = ('_exptl.method',
                   '_refine.ls_R_factor_R_work',
                   '_refine_ls_shell.R_factor_R_free',
                   '_refine.pdbx_solvent_vdw_probe_radii',
                   '_refine.pdbx_overall_phase_error',
                   '_refine_ls_restr.number',
                   '_reflns.B_iso_Wilson_estimate',
                   '_diffrn_source.pdbx_wavelength',
                   '_software.name',
                   '_software.contact_author',
                   '_citation.id',
                   '_citation.journal_volume',
                   '_citation_author.name',
                   '_citation_author.identifier_ORCID',)
  for key in expected_keys:
    assert key in cif_model[prefix], "key: %s is missing" % key
  assert cif_model[prefix]["_exptl.method"] == "X-RAY DIFFRACTION"
  assert 'Phenix' in list(cif_model[prefix]['_software.name'])
  assert list(cif_model[prefix]['_citation.id']) \
         == ['1'], list(cif_model[prefix]['_citation.id'])
  cif_miller_array = open(r.cif_ref,"r").read()
  cif_model = iotbx.cif.reader(input_string=cif_miller_array).model()
  ma_builder = iotbx.cif.builders.miller_array_builder(cif_model[prefix])
  f_obs        = ma_builder.arrays()['_refln.F_meas_au']
  f_calc       = ma_builder.arrays()['_refln.pdbx_F_calc_with_solvent']
  r_free_flags = ma_builder.arrays()['_refln.status']
  r_free_flags = r_free_flags.customized_copy(data=(r_free_flags.data() == "f"))
  fmodel = mmtbx.f_model.manager(
    f_obs        = f_obs,
    r_free_flags = r_free_flags,
    f_calc       = f_calc,
    f_mask       = f_calc.array(data=f_calc.data()*0))
  cif_block = cif_model.blocks[prefix]
  assert '_space_group.name_H-M_alt' in cif_block
  assert '_cell.angle_alpha' in cif_block
  assert '_refln.index_h' in cif_block
  assert approx_equal(r.r_work_final, fmodel.r_work(), 0.001)
  assert approx_equal(r.r_free_final, fmodel.r_free(), 0.001)
  expected_keys = ('_refln.F_meas_au',
                   '_refln.pdbx_r_free_flag',
                   '_refln.pdbx_FWT',
                   '_refln.pdbx_DELFWT',
                   '_refln.pdbx_F_calc_with_solvent',
                   '_refln.status',
                   )
  for key in expected_keys:
    assert key in ma_builder.arrays()
  # XXX this doesn't really belong here, but it didn't seem worth writing a
  # separate test since we're already checking the wavelength above
  mtz_file = r.mtz
  from iotbx import mtz
  mtz_in = mtz.object(mtz_file)
  f_obs = mtz_in.as_miller_arrays()[0]
  assert approx_equal(f_obs.info().wavelength, 0.9792, eps=0.00001)

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK. Time: %8.3f"%(time.time()-t0))
