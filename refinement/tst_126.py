from __future__ import print_function
import libtbx.load_env
import os, time
import iotbx.pdb
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel

pdb_str_1 = """
CRYST1   20.000   20.000   20.000  90.00 110.00 120.00 P 1
ATOM   1134  N   TYR A  76      16.015 -11.128   3.159  1.00 15.00           N  
ATOM   1135  CA  TYR A  76      17.177 -11.794   3.754  1.00 15.00           C  
ATOM   1136  C   TYR A  76      17.419 -13.154   3.146  1.00 15.00           C  
ATOM   1137  O   TYR A  76      18.584 -13.522   2.980  1.00 15.00           O  
ATOM   1138  CB  TYR A  76      16.883 -11.927   5.252  1.00 15.00           C  
ATOM   1139  CG  TYR A  76      17.988 -12.428   6.143  1.00 15.00           C  
ATOM   1140  CD1 TYR A  76      19.336 -12.335   5.857  1.00 15.00           C  
ATOM   1141  CD2 TYR A  76      17.620 -13.002   7.363  1.00 15.00           C  
ATOM   1142  CE1 TYR A  76      20.305 -12.803   6.723  1.00 15.00           C  
ATOM   1143  CE2 TYR A  76      18.537 -13.454   8.259  1.00 15.00           C  
ATOM   1144  CZ  TYR A  76      19.881 -13.356   7.932  1.00 15.00           C  
ATOM   1145  OH  TYR A  76      20.757 -13.824   8.842  1.00 15.00           O  
ATOM   1146  D  ATYR A  76      15.266 -10.831   3.719  0.63 15.00           D  
ATOM   1147  H  BTYR A  76      15.266 -10.831   3.719  0.37 15.00           H  
ATOM   1148  HA  TYR A  76      18.034 -11.192   3.602  1.00 15.00           H  
ATOM   1149  HB2 TYR A  76      16.626 -10.947   5.640  1.00 15.00           H  
ATOM   1150  HB3 TYR A  76      16.047 -12.590   5.377  1.00 15.00           H  
ATOM   1151  HD1 TYR A  76      19.634 -11.872   4.938  1.00 15.00           H  
ATOM   1152  HD2 TYR A  76      16.568 -13.074   7.611  1.00 15.00           H  
ATOM   1153  HE1 TYR A  76      21.370 -12.724   6.485  1.00 15.00           H  
ATOM   1154  HE2 TYR A  76      18.226 -13.899   9.190  1.00 15.00           H 
ATOM   1254  HH ATYR A  76      20.259 -14.176   9.635  0.30 15.00           H
ATOM   1256  DH BTYR A  76      20.259 -14.176   9.635  0.70 15.00           D
END
"""

pdb_str_2 = """
CRYST1   20.000   20.000   20.000  90.00 110.00 120.00 P 1
ATOM   1134  N   TYR A  76      16.015 -11.128   3.159  1.00 15.00           N  
ATOM   1135  CA  TYR A  76      17.177 -11.794   3.754  1.00 15.00           C  
ATOM   1136  C   TYR A  76      17.419 -13.154   3.146  1.00 15.00           C  
ATOM   1137  O   TYR A  76      18.584 -13.522   2.980  1.00 15.00           O  
ATOM   1138  CB  TYR A  76      16.883 -11.927   5.252  1.00 15.00           C  
ATOM   1139  CG  TYR A  76      17.988 -12.428   6.143  1.00 15.00           C  
ATOM   1140  CD1 TYR A  76      19.336 -12.335   5.857  1.00 15.00           C  
ATOM   1141  CD2 TYR A  76      17.620 -13.002   7.363  1.00 15.00           C  
ATOM   1142  CE1 TYR A  76      20.305 -12.803   6.723  1.00 15.00           C  
ATOM   1143  CE2 TYR A  76      18.537 -13.454   8.259  1.00 15.00           C  
ATOM   1144  CZ  TYR A  76      19.881 -13.356   7.932  1.00 15.00           C  
ATOM   1145  OH  TYR A  76      20.757 -13.824   8.842  1.00 15.00           O  
ATOM   1146  D  ATYR A  76      15.266 -10.831   3.719  0.63 15.00           D  
ATOM   1147  H  BTYR A  76      15.266 -10.831   3.719  0.37 15.00           H  
ATOM   1148  HA  TYR A  76      18.034 -11.192   3.602  1.00 15.00           H  
ATOM   1149  HB2 TYR A  76      16.626 -10.947   5.640  1.00 15.00           H  
ATOM   1150  HB3 TYR A  76      16.047 -12.590   5.377  1.00 15.00           H  
ATOM   1151  HD1 TYR A  76      19.634 -11.872   4.938  1.00 15.00           H  
ATOM   1152  HD2 TYR A  76      16.568 -13.074   7.611  1.00 15.00           H  
ATOM   1153  HE1 TYR A  76      21.370 -12.724   6.485  1.00 15.00           H  
ATOM   1154  HE2 TYR A  76      18.226 -13.899   9.190  1.00 15.00           H 
ATOM   1254  HH ATYR A  76      21.616 -13.926   8.441  0.30 15.00           H
ATOM   1256  DH BTYR A  76      21.616 -13.926   8.441  0.70 15.00           D
END                                                                             
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  rsfit_of_hd_neutron
  """
  with open("%s_1.pdb"%prefix,"w") as fo:
    fo.write(pdb_str_1)
  with open("%s_2.pdb"%prefix,"w") as fo:
    fo.write(pdb_str_2)
  #
  args = [
    "%s_1.pdb"%prefix,
    "type=real",
    "label=f-obs",
    "r_free=0.05",
    "high_res=1.0",
    "scattering_table=neutron"]
  r0 = run_fmodel(args = args, prefix = prefix)
  #
  cntr = 0
  for i, pdb_file in enumerate(["%s_1.pdb"%prefix, "%s_2.pdb"%prefix]):
    args = [
      pdb_file,
      r0.mtz,
      "main.scattering_table=neutron",
      "main.bulk_solvent=false",
      "strategy=individual_sites",
      "main.max_number_of_iterations=0",
      "main.number_of_macro=1",
      "xray_data.high_res=%s"%str(1.0)]
    r = run_phenix_refine(args = args, prefix = prefix)
    if(i==0):
      assert r.r_work_start < 0.02, r.r_work_start
      assert r.r_work_final < 0.02, r.r_work_final
      cntr += 1
    else:
      assert r.r_work_start > 0.1  , r.r_work_start
      assert r.r_work_final < 0.021, r.r_work_final
      cntr += 1
  assert cntr == 2

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK", "time: %.2f"%(time.time()-t0))
