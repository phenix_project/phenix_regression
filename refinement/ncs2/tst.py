from __future__ import print_function
from libtbx import easy_run
import libtbx.load_env
import libtbx.path
import time, os

def exercise():
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/ncs2/m.pdb",test=os.path.isfile)
  prefix = "tst_ncs2"
  cmd1 = " ".join([
    "phenix.fmodel",
    "%s"%pdb,
    "high_res=15",
    "type=real",
    "label=f-obs",
    "r_free=0.1",
    "output.file_name=%s.mtz"%prefix,
    "> tmp.log"
  ])
  cmd2 = " ".join([
    "phenix.refine",
    "%s"%pdb,
    "%s.mtz"%prefix,
    "main.number_of_macro=1",
    "ncs_search.enabled=true",
    "ncs.type=torsion",
    "output.prefix=%s"%prefix,
    "main.bulk_sol=false",
    "--quiet",
    "--overwrite",
    "> tmp.log"
  ])
  for cmd in [cmd1,cmd2]:
    assert not easy_run.call(cmd)

if (__name__ == "__main__"):
  t0 = time.time()
  exercise()
  print("OK. Time: %8.3f"%(time.time()-t0))
