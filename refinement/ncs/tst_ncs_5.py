from __future__ import print_function
from libtbx import easy_run
import time
from libtbx.test_utils import assert_lines_in_file, show_diff

def exercise_1(prefix="tst_ncs_5"):
  """
  Neutron structure, ncs and reference model restraints.
  """
  model_pdb_str = """\
CRYST1   26.984   30.094   21.606  90.00  90.00  90.00 P 1
ATOM      1  N   HIS A 245       5.673  19.831   9.910  1.00 50.00           N
ATOM      2  CA  HIS A 245       6.528  18.661   9.725  1.00 50.00           C
ATOM      3  C   HIS A 245       7.762  18.960   8.848  1.00 50.00           C
ATOM      4  O   HIS A 245       8.894  18.897   9.339  1.00 50.00           O
ATOM      5  CB  HIS A 245       5.701  17.479   9.185  1.00 50.00           C
ATOM      6  HA  HIS A 245       6.903  18.363  10.704  1.00 50.00           H
ATOM      7  N   TRP A 246       7.534  19.310   7.576  1.00 50.00           N
ATOM      8  CA  TRP A 246       8.609  19.471   6.570  1.00 50.00           C
ATOM      9  C   TRP A 246       9.023  20.920   6.291  1.00 50.00           C
ATOM     10  O   TRP A 246       9.414  21.256   5.161  1.00 50.00           O
ATOM     11  CB  TRP A 246       8.234  18.780   5.252  1.00 50.00           C
ATOM     12  HA  TRP A 246       9.495  18.962   6.950  1.00 50.00           H
ATOM     13  H  ATRP A 246       6.603  19.493   7.201  0.50 50.00           H
ATOM     14  D  BTRP A 246       6.603  19.493   7.201  0.50 50.00           D
ATOM     15  N   GLU A 247       8.905  21.762   7.321  1.00 50.00           N
ATOM     16  CA  GLU A 247       9.495  23.108   7.351  1.00 50.00           C
ATOM     17  C   GLU A 247      10.488  23.138   8.503  1.00 50.00           C
ATOM     18  O   GLU A 247      11.440  23.925   8.509  1.00 50.00           O
ATOM     19  CB  GLU A 247       8.432  24.195   7.557  1.00 50.00           C
ATOM     20  HA  GLU A 247      10.026  23.303   6.420  1.00 50.00           H
ATOM     22  D  BGLU A 247       8.378  21.537   8.164  0.50 50.00           D
ATOM     23  N   ALA A 248      10.238  22.266   9.475  1.00 50.00           N
ATOM     24  CA  ALA A 248      11.106  22.077  10.617  1.00 50.00           C
ATOM     25  C   ALA A 248      12.222  21.079  10.295  1.00 50.00           C
ATOM     26  O   ALA A 248      13.352  21.219  10.767  1.00 50.00           O
ATOM     27  CB  ALA A 248      10.286  21.601  11.798  1.00 50.00           C
ATOM     28  HA  ALA A 248      11.565  23.030  10.881  1.00 50.00           H
ATOM     29  HB1 ALA A 248      10.946  21.461  12.654  1.00 50.00           H
ATOM     30  HB2 ALA A 248       9.530  22.351  12.030  1.00 50.00           H
ATOM     31  HB3 ALA A 248       9.807  20.657  11.539  1.00 50.00           H
ATOM     32  H  AALA A 248       9.417  21.665   9.508  0.50 50.00           H
ATOM     33  D  BALA A 248       9.417  21.665   9.508  0.50 50.00           D
ATOM     34  N   GLU A 249      11.895  20.069   9.495  1.00 50.00           N
ATOM     35  CA  GLU A 249      12.886  19.112   9.007  1.00 50.00           C
ATOM     36  C   GLU A 249      13.759  19.712   7.883  1.00 50.00           C
ATOM     37  O   GLU A 249      14.977  19.481   7.835  1.00 50.00           O
ATOM     38  CB  GLU A 249      12.185  17.839   8.531  1.00 50.00           C
ATOM     39  HA  GLU A 249      13.546  18.840   9.831  1.00 50.00           H
ATOM     40  H  AGLU A 249      10.948  19.885   9.165  0.50 50.00           H
ATOM     41  D  BGLU A 249      10.948  19.885   9.165  0.50 50.00           D
ATOM     42  N   ASP A 250      13.133  20.487   6.994  1.00 50.00           N
ATOM     43  CA  ASP A 250      13.831  21.165   5.895  1.00 50.00           C
ATOM     44  C   ASP A 250      14.896  22.148   6.402  1.00 50.00           C
ATOM     45  O   ASP A 250      15.722  22.634   5.630  1.00 50.00           O
ATOM     46  CB  ASP A 250      12.823  21.891   5.000  1.00 50.00           C
ATOM     47  HA  ASP A 250      14.336  20.415   5.286  1.00 50.00           H
ATOM     48  H  AASP A 250      12.129  20.667   7.008  0.50 50.00           H
ATOM     49  D  BASP A 250      12.129  20.667   7.008  0.50 50.00           D
ATOM     50  N   ALA A 251      14.866  22.421   7.704  1.00 50.00           N
ATOM     51  CA  ALA A 251      15.790  23.343   8.347  1.00 50.00           C
ATOM     52  C   ALA A 251      16.774  22.631   9.285  1.00 50.00           C
ATOM     53  O   ALA A 251      17.931  23.043   9.408  1.00 50.00           O
ATOM     54  CB  ALA A 251      15.013  24.412   9.100  1.00 50.00           C
ATOM     55  HA  ALA A 251      16.376  23.845   7.577  1.00 50.00           H
ATOM     56  HB1 ALA A 251      15.718  25.094   9.575  1.00 50.00           H
ATOM     57  HB2 ALA A 251      14.386  24.957   8.395  1.00 50.00           H
ATOM     58  HB3 ALA A 251      14.392  23.932   9.856  1.00 50.00           H
ATOM     59  H  AALA A 251      14.192  22.021   8.356  0.50 50.00           H
ATOM     60  D  BALA A 251      14.192  22.021   8.356  0.50 50.00           D
ATOM     61  N   LYS A 252      16.309  21.572   9.949  1.00 50.00           N
ATOM     62  CA  LYS A 252      17.168  20.752  10.807  1.00 50.00           C
ATOM     63  C   LYS A 252      18.109  19.919   9.954  1.00 50.00           C
ATOM     64  O   LYS A 252      19.142  19.476  10.446  1.00 50.00           O
ATOM     65  CB  LYS A 252      16.345  19.839  11.724  1.00 50.00           C
ATOM     66  HA  LYS A 252      17.772  21.407  11.436  1.00 50.00           H
ATOM     67  H  ALYS A 252      15.340  21.255   9.914  0.50 50.00           H
ATOM     68  D  BLYS A 252      15.340  21.255   9.914  0.50 50.00           D
ATOM     69  N   ARG A 253      17.738  19.715   8.685  1.00 50.00           N
ATOM     70  CA  ARG A 253      18.561  18.994   7.700  1.00 50.00           C
ATOM     71  C   ARG A 253      19.293  19.928   6.742  1.00 50.00           C
ATOM     72  O   ARG A 253      20.117  19.477   5.941  1.00 50.00           O
ATOM     73  CB  ARG A 253      17.720  18.009   6.894  1.00 50.00           C
ATOM     74  HA  ARG A 253      19.316  18.418   8.236  1.00 50.00           H
ATOM     75  H  AARG A 253      16.853  20.041   8.298  0.50 50.00           H
ATOM     76  D  BARG A 253      16.853  20.041   8.298  0.50 50.00           D
ATOM     77  N   ILE A 254      18.971  21.219   6.815  1.00 50.00           N
ATOM     78  CA  ILE A 254      19.762  22.258   6.157  1.00 50.00           C
ATOM     79  C   ILE A 254      20.949  22.629   7.047  1.00 50.00           C
ATOM     80  O   ILE A 254      21.984  23.093   6.569  1.00 50.00           O
ATOM     81  CB  ILE A 254      18.923  23.518   5.862  1.00 50.00           C
ATOM     82  HA  ILE A 254      20.148  21.874   5.213  1.00 50.00           H
ATOM     83  H  AILE A 254      18.164  21.580   7.323  0.50 50.00           H
ATOM     84  D  BILE A 254      18.164  21.580   7.323  0.50 50.00           D
TER
ATOM    519  N   HIS B   1       5.000   7.552   9.415  1.00 20.00           N
ATOM    520  CA  HIS B   1       5.999   7.750   8.377  1.00 20.00           C
ATOM    521  C   HIS B   1       7.157   8.664   8.820  1.00 20.00           C
ATOM    522  O   HIS B   1       8.302   8.202   8.917  1.00 20.00           O
ATOM    523  CB  HIS B   1       5.315   8.263   7.100  1.00 20.00           C
ATOM    524  HA  HIS B   1       6.434   6.779   8.138  1.00 20.00           H
ATOM    525  N   TRP B   2       6.845   9.937   9.108  1.00 20.00           N
ATOM    526  CA  TRP B   2       7.853  10.991   9.386  1.00 20.00           C
ATOM    527  C   TRP B   2       8.071  11.299  10.868  1.00 20.00           C
ATOM    528  O   TRP B   2       8.355  12.443  11.244  1.00 20.00           O
ATOM    529  CB  TRP B   2       7.516  12.294   8.639  1.00 20.00           C
ATOM    530  HA  TRP B   2       8.809  10.643   8.995  1.00 20.00           H
ATOM    531  H  ATRP B   2       5.886  10.280   9.158  0.50 20.00           H
ATOM    532  D  BTRP B   2       5.886  10.280   9.158  0.50 20.00           D
ATOM    533  N   GLU B   3       7.910  10.276  11.699  1.00 20.00           N
ATOM    534  CA  GLU B   3       8.310  10.321  13.101  1.00 20.00           C
ATOM    535  C   GLU B   3       9.344   9.227  13.282  1.00 20.00           C
ATOM    536  O   GLU B   3      10.197   9.304  14.170  1.00 20.00           O
ATOM    537  CB  GLU B   3       7.115  10.078  14.034  1.00 20.00           C
ATOM    538  HA  GLU B   3       8.761  11.285  13.337  1.00 20.00           H
ATOM    539  H  AGLU B   3       7.474   9.397  11.425  0.50 20.00           H
ATOM    540  D  BGLU B   3       7.474   9.397  11.425  0.50 20.00           D
ATOM    541  N   ALA B   4       9.243   8.212  12.421  1.00 20.00           N
ATOM    542  CA  ALA B   4      10.152   7.070  12.398  1.00 20.00           C
ATOM    543  C   ALA B   4      11.377   7.393  11.552  1.00 20.00           C
ATOM    544  O   ALA B   4      12.495   6.939  11.843  1.00 20.00           O
ATOM    545  CB  ALA B   4       9.436   5.840  11.856  1.00 20.00           C
ATOM    546  HA  ALA B   4      10.484   6.853  13.413  1.00 20.00           H
ATOM    547  HB1 ALA B   4      10.131   5.000  11.847  1.00 20.00           H
ATOM    548  HB2 ALA B   4       8.586   5.613  12.499  1.00 20.00           H
ATOM    549  HB3 ALA B   4       9.091   6.048  10.843  1.00 20.00           H
ATOM    550  H  AALA B   4       8.519   8.135  11.709  0.50 20.00           H
ATOM    551  D  BALA B   4       8.519   8.135  11.709  0.50 20.00           D
ATOM    552  N   GLU B   5      11.154   8.177  10.501  1.00 20.00           N
ATOM    553  CA  GLU B   5      12.236   8.656   9.665  1.00 20.00           C
ATOM    554  C   GLU B   5      12.962   9.809  10.359  1.00 20.00           C
ATOM    555  O   GLU B   5      14.192   9.913  10.287  1.00 20.00           O
ATOM    556  CB  GLU B   5      11.695   9.090   8.303  1.00 20.00           C
ATOM    557  HA  GLU B   5      12.951   7.849   9.506  1.00 20.00           H
ATOM    558  H  AGLU B   5      10.231   8.495  10.207  0.50 20.00           H
ATOM    559  D  BGLU B   5      10.231   8.495  10.207  0.50 20.00           D
ATOM    560  N   ASP B   6      12.201  10.656  11.052  1.00 20.00           N
ATOM    561  CA  ASP B   6      12.780  11.797  11.751  1.00 20.00           C
ATOM    562  C   ASP B   6      13.734  11.366  12.850  1.00 20.00           C
ATOM    563  O   ASP B   6      14.490  12.174  13.376  1.00 20.00           O
ATOM    564  CB  ASP B   6      11.696  12.706  12.321  1.00 20.00           C
ATOM    565  CG  ASP B   6      12.077  14.186  12.227  1.00 20.00           C
ATOM    566  OD1 ASP B   6      13.294  14.515  12.235  1.00 20.00           O
ATOM    567  OD2 ASP B   6      11.151  15.026  12.123  1.00 20.00           O
ATOM    568  HA  ASP B   6      13.354  12.383  11.033  1.00 20.00           H
ATOM    569  HB2 ASP B   6      10.773  12.557  11.760  1.00 20.00           H
ATOM    570  HB3 ASP B   6      11.540  12.462  13.372  1.00 20.00           H
ATOM    571  H  AASP B   6      11.188  10.578  11.146  0.50 20.00           H
ATOM    572  D  BASP B   6      11.188  10.578  11.146  0.50 20.00           D
ATOM    573  N   ALA B   7      13.697  10.078  13.167  1.00 20.00           N
ATOM    574  CA  ALA B   7      14.518   9.494  14.219  1.00 20.00           C
ATOM    575  C   ALA B   7      15.609   8.571  13.671  1.00 20.00           C
ATOM    576  O   ALA B   7      16.701   8.494  14.235  1.00 20.00           O
ATOM    577  CB  ALA B   7      13.635   8.751  15.209  1.00 20.00           C
ATOM    578  HA  ALA B   7      15.013  10.299  14.762  1.00 20.00           H
ATOM    579  HB1 ALA B   7      14.262   8.320  15.989  1.00 20.00           H
ATOM    580  HB2 ALA B   7      12.926   9.453  15.647  1.00 20.00           H
ATOM    581  HB3 ALA B   7      13.100   7.961  14.683  1.00 20.00           H
ATOM    582  H  AALA B   7      13.090   9.394  12.718  0.50 20.00           H
ATOM    583  D  BALA B   7      13.090   9.394  12.718  0.50 20.00           D
ATOM    584  N   LYS B   8      15.303   7.866  12.584  1.00 20.00           N
ATOM    585  CA  LYS B   8      16.289   7.029  11.903  1.00 20.00           C
ATOM    586  C   LYS B   8      17.319   7.905  11.180  1.00 20.00           C
ATOM    587  O   LYS B   8      18.428   7.455  10.865  1.00 20.00           O
ATOM    588  CB  LYS B   8      15.603   6.067  10.925  1.00 20.00           C
ATOM    589  HA  LYS B   8      16.819   6.432  12.645  1.00 20.00           H
ATOM    590  H  ALYS B   8      14.380   7.853  12.150  0.50 20.00           H
ATOM    591  D  BLYS B   8      14.380   7.853  12.150  0.50 20.00           D
ATOM    592  N   ARG B   9      16.937   9.158  10.933  1.00 20.00           N
ATOM    593  CA  ARG B   9      17.800  10.143  10.286  1.00 20.00           C
ATOM    594  C   ARG B   9      18.363  11.139  11.281  1.00 20.00           C
ATOM    595  O   ARG B   9      19.208  11.965  10.940  1.00 20.00           O
ATOM    596  CB  ARG B   9      17.034  10.896   9.206  1.00 20.00           C
ATOM    597  HA  ARG B   9      18.636   9.629   9.812  1.00 20.00           H
ATOM    598  H  AARG B   9      16.017   9.529  11.171  0.50 20.00           H
ATOM    599  D  BARG B   9      16.017   9.529  11.171  0.50 20.00           D
ATOM    600  N   ILE B  10      17.880  11.066  12.511  1.00 20.00           N
ATOM    601  CA  ILE B  10      18.511  11.788  13.593  1.00 20.00           C
ATOM    602  C   ILE B  10      19.673  10.950  14.140  1.00 20.00           C
ATOM    603  O   ILE B  10      20.625  11.486  14.724  1.00 20.00           O
ATOM    604  CB  ILE B  10      17.473  12.141  14.677  1.00 20.00           C
ATOM    605  CG1 ILE B  10      17.428  13.661  14.894  1.00 20.00           C
ATOM    606  CG2 ILE B  10      17.646  11.285  15.934  1.00 20.00           C
ATOM    607  CD1 ILE B  10      16.857  14.431  13.710  1.00 20.00           C
ATOM    608  HA  ILE B  10      18.926  12.719  13.207  1.00 20.00           H
ATOM    609  HB  ILE B  10      16.506  11.865  14.266  1.00 20.00           H
ATOM    610 HG12 ILE B  10      16.805  13.874  15.762  1.00 20.00           H
ATOM    611 HG13 ILE B  10      18.442  14.021  15.069  1.00 20.00           H
ATOM    612 HG21 ILE B  10      16.810  11.478  16.606  1.00 20.00           H
ATOM    613 HG22 ILE B  10      17.657  10.232  15.653  1.00 20.00           H
ATOM    614 HG23 ILE B  10      18.581  11.552  16.426  1.00 20.00           H
ATOM    615 HD11 ILE B  10      16.932  15.499  13.914  1.00 20.00           H
ATOM    616 HD12 ILE B  10      17.425  14.184  12.813  1.00 20.00           H
ATOM    617 HD13 ILE B  10      15.812  14.152  13.576  1.00 20.00           H
ATOM    618  H  AILE B  10      17.061  10.524  12.788  0.50 20.00           H
ATOM    619  D  BILE B  10      17.061  10.524  12.788  0.50 20.00           D
TER
END
"""
  ref_pdb_str = """\
CRYST1   26.537   17.143   17.624  90.00  90.00  90.00 P 1
ATOM      1  N   HIS A 245       5.362   7.439   9.718  1.00 50.00           N
ATOM      2  CA  HIS A 245       6.008   6.138   9.723  1.00 50.00           C
ATOM      3  C   HIS A 245       7.031   6.106   8.617  1.00 50.00           C
ATOM      4  O   HIS A 245       8.222   5.856   8.847  1.00 50.00           O
ATOM      5  CB  HIS A 245       5.000   5.011   9.556  1.00 50.00           C
ATOM      0  HA  HIS A 245       6.467   6.033  10.579  1.00 50.00           H
ATOM      6  N   TRP A 246       6.534   6.331   7.392  1.00 50.00           N
ATOM      7  CA  TRP A 246       7.405   6.627   6.270  1.00 50.00           C
ATOM      8  C   TRP A 246       8.215   7.880   6.510  1.00 50.00           C
ATOM      9  O   TRP A 246       9.319   8.021   5.943  1.00 50.00           O
ATOM     10  CB  TRP A 246       6.584   6.742   5.000  1.00 50.00           C
ATOM      0  H   TRP A 246       5.687   6.333   7.209  1.00 50.00           H
ATOM      0  HA  TRP A 246       8.051   5.906   6.200  1.00 50.00           H
ATOM     11  N   GLU A 247       7.753   8.751   7.426  1.00 50.00           N
ATOM     12  CA  GLU A 247       8.379  10.048   7.636  1.00 50.00           C
ATOM     13  C   GLU A 247       9.554  10.033   8.603  1.00 50.00           C
ATOM     14  O   GLU A 247      10.602  10.630   8.290  1.00 50.00           O
ATOM     15  CB  GLU A 247       7.322  11.065   8.120  1.00 50.00           C
ATOM      0  H   GLU A 247       7.063   8.594   7.919  1.00 50.00           H
ATOM      0  HA  GLU A 247       8.666  10.366   6.768  1.00 50.00           H
ATOM     16  N   ALA A 248       9.457   9.336   9.745  1.00 50.00           N
ATOM     17  CA  ALA A 248      10.609   9.304  10.650  1.00 50.00           C
ATOM     18  C   ALA A 248      11.652   8.265  10.243  1.00 50.00           C
ATOM     19  O   ALA A 248      12.799   8.358  10.686  1.00 50.00           O
ATOM     20  CB  ALA A 248      10.176   9.022  12.082  1.00 50.00           C
ATOM      0  H   ALA A 248       8.769   8.875   9.997  1.00 50.00           H
ATOM      0  HA  ALA A 248      11.044  10.174  10.630  1.00 50.00           H
ATOM      0  HB1 ALA A 248      10.985   8.981  12.624  1.00 50.00           H
ATOM      0  HB2 ALA A 248       9.621   9.745  12.414  1.00 50.00           H
ATOM      0  HB3 ALA A 248       9.727   8.162  12.092  1.00 50.00           H
ATOM     21  N   GLU A 249      11.261   7.283   9.448  1.00 50.00           N
ATOM     22  CA  GLU A 249      12.210   6.284   8.974  1.00 50.00           C
ATOM     23  C   GLU A 249      13.015   6.834   7.791  1.00 50.00           C
ATOM     24  O   GLU A 249      14.159   6.415   7.560  1.00 50.00           O
ATOM     25  CB  GLU A 249      11.458   5.000   8.584  1.00 50.00           C
ATOM      0  H   GLU A 249      10.460   7.144   9.167  1.00 50.00           H
ATOM      0  HA  GLU A 249      12.830   6.082   9.691  1.00 50.00           H
ATOM     26  N   ASP A 250      12.398   7.740   7.025  1.00 50.00           N
ATOM     27  CA  ASP A 250      13.106   8.423   5.953  1.00 50.00           C
ATOM     28  C   ASP A 250      14.189   9.334   6.474  1.00 50.00           C
ATOM     29  O   ASP A 250      15.162   9.607   5.760  1.00 50.00           O
ATOM     30  CB  ASP A 250      12.123   9.234   5.097  1.00 50.00           C
ATOM      0  H   ASP A 250      11.568   7.967   7.102  1.00 50.00           H
ATOM      0  HA  ASP A 250      13.519   7.768   5.370  1.00 50.00           H
ATOM     31  N   ALA A 251      14.055   9.806   7.714  1.00 50.00           N
ATOM     32  CA  ALA A 251      15.087  10.586   8.395  1.00 50.00           C
ATOM     33  C   ALA A 251      16.081   9.740   9.169  1.00 50.00           C
ATOM     34  O   ALA A 251      17.268  10.074   9.192  1.00 50.00           O
ATOM     35  CB  ALA A 251      14.420  11.567   9.370  1.00 50.00           C
ATOM      0  H   ALA A 251      13.347   9.684   8.178  1.00 50.00           H
ATOM      0  HA  ALA A 251      15.578  11.092   7.730  1.00 50.00           H
ATOM      0  HB1 ALA A 251      15.099  12.143   9.727  1.00 50.00           H
ATOM      0  HB2 ALA A 251      13.782  12.113   8.884  1.00 50.00           H
ATOM      0  HB3 ALA A 251      13.978  11.080  10.083  1.00 50.00           H
ATOM     36  N   LYS A 252      15.623   8.674   9.816  1.00 50.00           N
ATOM     37  CA  LYS A 252      16.527   7.792  10.542  1.00 50.00           C
ATOM     38  C   LYS A 252      17.491   7.093   9.604  1.00 50.00           C
ATOM     39  O   LYS A 252      18.674   6.985   9.924  1.00 50.00           O
ATOM     40  CB  LYS A 252      15.752   6.756  11.329  1.00 50.00           C
ATOM      0  H   LYS A 252      14.796   8.445   9.839  1.00 50.00           H
ATOM      0  HA  LYS A 252      17.050   8.299  11.188  1.00 50.00           H
ATOM     41  N   ARG A 253      16.983   6.582   8.479  1.00 50.00           N
ATOM     42  CA  ARG A 253      17.802   5.996   7.444  1.00 50.00           C
ATOM     43  C   ARG A 253      18.855   6.965   6.910  1.00 50.00           C
ATOM     44  O   ARG A 253      19.988   6.532   6.656  1.00 50.00           O
ATOM     45  CB  ARG A 253      16.914   5.505   6.284  1.00 50.00           C
ATOM      0  H   ARG A 253      16.138   6.527   8.308  1.00 50.00           H
ATOM      0  HA  ARG A 253      18.272   5.233   7.802  1.00 50.00           H
ATOM     46  N   ILE A 254      18.501   8.233   6.692  1.00 50.00           N
ATOM     47  CA  ILE A 254      19.422   9.191   6.076  1.00 50.00           C
ATOM     48  C   ILE A 254      20.515   9.626   7.037  1.00 50.00           C
ATOM     49  O   ILE A 254      21.537  10.145   6.588  1.00 50.00           O
ATOM     50  CB  ILE A 254      18.682  10.450   5.567  1.00 50.00           C
ATOM      0  H   ILE A 254      17.739   8.569   6.905  1.00 50.00           H
ATOM      0  HA  ILE A 254      19.833   8.769   5.300  1.00 50.00           H
TER
END
"""

  f = open("%s_model.pdb" % prefix, "w")
  f.write(model_pdb_str)
  f.close()
  f = open("%s_ref.pdb" % prefix, "w")
  f.write(ref_pdb_str)
  f.close()
  cmd = " ".join([
    "phenix.fmodel",
    "%s_model.pdb"%prefix,
    "high_res=2",
    "type=real",
    "label=f-obs",
    "r_free=0.1",
    "output.file_name=%s.mtz"%prefix,
    "> tmp.log"
  ])
  print(cmd)
  assert not easy_run.call(cmd)

  # No shutoff for NCS

  cmd = " ".join([
    "phenix.refine",
    "%s_model.pdb" % prefix,
    "%s.mtz" % prefix,
    "main.number_of_macro_cycles=0",
    "output.prefix=%s_1" % prefix,
    "output.write_eff_file=False",
    "output.write_geo_file=False",
    "output.write_def_file=True",
    "output.write_map_coefficients=False",
    "reference_model.enabled=True",
    "reference_model.file=%s_ref.pdb" % prefix,
    "reference_model.auto_shutoff_for_ncs=False",
    "ncs_search.enabled=true",
    "--overwrite",
    "> %s_1.log" % prefix
  ])
  print(cmd)
  assert not easy_run.call(cmd)
  # check that log in file is the same as log on the screen
  scr = open("%s_1.log" % prefix,'r')
  scr_lines = scr.read()
  scr.close()
  log=open("%s_1_001.log" % prefix, 'r')
  log_lines=log.read()
  log.close()
  assert not show_diff(log_lines, scr_lines)
  assert_lines_in_file(file_name="%s_1_001.log" % prefix,
      lines="""
Number of NCS groups: 1
refinement.pdb_interpretation.ncs_group {
  reference = chain 'A'
  selection = (chain 'B' and (name N or name CA or name C or name O or name \\
    CB ))
}
""")
  assert_lines_in_file(file_name="%s_1_001.log" % prefix,
      lines="""
Initializing torsion NCS restraints...
Number of torsion NCS restraints: 54""")
  assert_lines_in_file(file_name="%s_1_001.log" % prefix,
      lines="""
Total # of matched residue pairs: 20
Total # of reference model restraints: 54""")

  assert_lines_in_file(file_name="%s_1_002.def" % prefix,
      lines="""     ncs_group {
      reference = "chain A"
      selection = "(chain B and (name N or name CA or name C or name O or name CB ))"
    }
    """)
  assert_lines_in_file(file_name="%s_1_001.log" % prefix,
      lines="""
=========================== phenix.refine: finished ===========================
      """)

  # shutoff for NCS
  print("OK")

  cmd = " ".join([
    "phenix.refine",
    "%s_model.pdb" % prefix,
    "%s.mtz" % prefix,
    "main.number_of_macro_cycles=0",
    "output.prefix=%s_2" % prefix,
    "output.write_eff_file=False",
    "output.write_geo_file=False",
    "output.write_def_file=True",
    "output.write_map_coefficients=False",
    "reference_model.enabled=True",
    "reference_model.file=%s_ref.pdb" % prefix,
    "reference_model.auto_shutoff_for_ncs=True",
    "ncs_search.enabled=true",
    "--overwrite",
    "> %s_2.log" % prefix
  ])
  print(cmd)
  assert not easy_run.call(cmd)
  # check that log in file is the same as log on the screen
  scr = open("%s_2.log" % prefix,'r')
  scr_lines = scr.read()
  scr.close()
  log=open("%s_2_001.log" % prefix, 'r')
  log_lines=log.read()
  log.close()
  assert not show_diff(log_lines, scr_lines)
  assert_lines_in_file(file_name="%s_2_001.log" % prefix,
      lines="""
Number of NCS groups: 1
refinement.pdb_interpretation.ncs_group {
  reference = chain 'A'
  selection = (chain 'B' and (name N or name CA or name C or name O or name \\
    CB))
}
""")
  assert_lines_in_file(file_name="%s_2_001.log" % prefix,
      lines="""
Initializing torsion NCS restraints...
Number of torsion NCS restraints: 54""")
  assert_lines_in_file(file_name="%s_2_001.log" % prefix,
      lines="""
Total # of matched residue pairs: 20
Total # of reference model restraints: 54""")

  assert_lines_in_file(file_name="%s_2_001.log" % prefix,
      lines="""
**Removed reference restraints that overlap with torsion NCS restraints**

Updated Reference Model Restraints:
--------------------------------------------------------
Reference Model Matching Summary:

reference file: tst_ncs_5_ref.pdb

Model:              Reference:
HIS A 245  <=====>  HIS A 245
TRP A 246  <=====>  TRP A 246
GLU A 247  <=====>  GLU A 247
ALA A 248  <=====>  ALA A 248
GLU A 249  <=====>  GLU A 249
ASP A 250  <=====>  ASP A 250
ALA A 251  <=====>  ALA A 251
LYS A 252  <=====>  LYS A 252
ARG A 253  <=====>  ARG A 253
ILE A 254  <=====>  ILE A 254
HIS B   1  <=====>  HIS A 245
TRP B   2  <=====>  TRP A 246
GLU B   3  <=====>  GLU A 247
ALA B   4  <=====>  ALA A 248
GLU B   5  <=====>  GLU A 249
ASP B   6  <=====>  ASP A 250
ALA B   7  <=====>  ALA A 251
LYS B   8  <=====>  LYS A 252
ARG B   9  <=====>  ARG A 253
ILE B  10  <=====>  ILE A 254

Total # of matched residue pairs: 20
Total # of reference model restraints: 0
""")

  assert_lines_in_file(file_name="%s_2_002.def" % prefix,
      lines="""    ncs_group {
      reference = "chain A"
      selection = "(chain B and (name N or name CA or name C or name O or name CB ))"
    }""")
  assert_lines_in_file(file_name="%s_2_001.log" % prefix,
      lines="""
=========================== phenix.refine: finished ===========================
      """)
  print("OK")


if (__name__ == "__main__"):
  t0 = time.time()
  exercise_1()
  print("OK. Time: %8.3f"%(time.time()-t0))
