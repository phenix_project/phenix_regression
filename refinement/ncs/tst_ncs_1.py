from __future__ import print_function
from libtbx import easy_run
import time

def exercise_1(prefix="tst_ncs_1"):
  """
  Excessive distances in NCS. Checking number of excessive atoms and
  ability to provide Sorry message.
  """
  pdb_str = """\
CRYST1   53.068   53.068  136.114  90.00  90.00 120.00 H 3 2
SCALE1      0.018844  0.010879  0.000000        0.00000
SCALE2      0.000000  0.021759  0.000000        0.00000
SCALE3      0.000000  0.000000  0.007347        0.00000
ATOM    155  N   GLU A  20      23.416 -22.891 -10.849  1.00120.88           N
ATOM    156  CA  GLU A  20      22.594 -23.844 -11.604  1.00130.41           C
ATOM    157  CB  GLU A  20      21.404 -24.349 -10.799  1.00134.99           C
ATOM    158  CG  GLU A  20      21.770 -25.449  -9.819  1.00142.52           C
ATOM    159  CD  GLU A  20      20.608 -25.833  -8.939  1.00144.58           C
ATOM    160  OE1 GLU A  20      19.527 -25.189  -9.043  1.00152.83           O
ATOM    161  OE2 GLU A  20      20.783 -26.786  -8.147  1.00132.97           O
ATOM    162  C   GLU A  20      22.079 -23.147 -12.802  1.00123.98           C
ATOM    163  O   GLU A  20      22.173 -23.672 -13.901  1.00128.34           O
ATOM    164  N   PHE A  21      21.538 -21.956 -12.553  1.00117.14           N
ATOM    165  CA  PHE A  21      21.065 -21.062 -13.598  1.00114.84           C
ATOM    166  CB  PHE A  21      20.646 -19.725 -13.003  1.00110.48           C
ATOM    167  CG  PHE A  21      20.438 -18.673 -14.022  1.00111.69           C
ATOM    168  CD1 PHE A  21      19.284 -18.658 -14.767  1.00116.39           C
ATOM    169  CE1 PHE A  21      19.077 -17.684 -15.727  1.00112.86           C
ATOM    170  CZ  PHE A  21      20.033 -16.720 -15.961  1.00108.71           C
ATOM    171  CE2 PHE A  21      21.202 -16.737 -15.229  1.00118.50           C
ATOM    172  CD2 PHE A  21      21.401 -17.714 -14.266  1.00118.18           C
ATOM    173  C   PHE A  21      22.114 -20.844 -14.675  1.00107.65           C
ATOM    174  O   PHE A  21      21.834 -20.998 -15.866  1.00114.44           O
ATOM    175  N   LEU A  22      23.317 -20.500 -14.245  1.00104.22           N
ATOM    176  CA  LEU A  22      24.413 -20.274 -15.182  1.00117.54           C
ATOM    177  CB  LEU A  22      25.683 -19.856 -14.437  1.00126.18           C
ATOM    178  CG  LEU A  22      25.671 -18.552 -13.637  1.00124.63           C
ATOM    179  CD1 LEU A  22      26.949 -18.390 -12.814  1.00123.80           C
ATOM    180  CD2 LEU A  22      25.454 -17.381 -14.573  1.00120.91           C
ATOM    181  C   LEU A  22      24.735 -21.507 -16.028  1.00115.10           C
ATOM    182  O   LEU A  22      25.030 -21.404 -17.229  1.00124.03           O
ATOM    183  N   ARG A  23      24.680 -22.675 -15.402  1.00111.47           N
ATOM    184  CA  ARG A  23      24.918 -23.900 -16.144  1.00121.97           C
ATOM    185  CB  ARG A  23      24.770 -25.128 -15.245  1.00130.80           C
ATOM    186  CG  ARG A  23      25.065 -26.463 -15.940  1.00145.37           C
ATOM    187  CD  ARG A  23      24.636 -27.666 -15.100  1.00155.63           C
ATOM    188  NE  ARG A  23      23.239 -27.522 -14.682  1.00169.45           N
ATOM    189  CZ  ARG A  23      22.171 -27.733 -15.459  1.00187.05           C
ATOM    190  NH1 ARG A  23      22.301 -28.159 -16.729  1.00192.12           N
ATOM    191  NH2 ARG A  23      20.950 -27.518 -14.948  1.00191.42           N
ATOM    192  C   ARG A  23      23.905 -23.914 -17.282  1.00132.04           C
ATOM    193  O   ARG A  23      24.277 -24.040 -18.474  1.00129.03           O
ATOM    194  N   GLU A  24      22.639 -23.692 -16.910  1.00144.46           N
ATOM    195  CA  GLU A  24      21.520 -23.687 -17.867  1.00152.37           C
ATOM    196  CB  GLU A  24      20.165 -23.487 -17.162  1.00168.33           C
ATOM    197  CG  GLU A  24      19.759 -24.601 -16.211  1.00196.43           C
ATOM    198  CD  GLU A  24      18.579 -24.243 -15.310  1.00232.19           C
ATOM    199  OE1 GLU A  24      17.905 -23.200 -15.541  1.00216.43           O
ATOM    200  OE2 GLU A  24      18.317 -25.030 -14.365  1.00283.53           O
ATOM    201  C   GLU A  24      21.644 -22.612 -18.936  1.00143.05           C
ATOM    202  O   GLU A  24      21.199 -22.807 -20.092  1.00141.20           O
ATOM    203  N   LEU A  25      22.175 -21.458 -18.544  1.00130.76           N
ATOM    204  CA  LEU A  25      22.253 -20.371 -19.488  1.00122.15           C
ATOM    205  CB  LEU A  25      22.702 -19.084 -18.815  1.00122.12           C
ATOM    206  CG  LEU A  25      22.604 -17.826 -19.688  1.00134.83           C
ATOM    207  CD1 LEU A  25      21.197 -17.590 -20.230  1.00149.27           C
ATOM    208  CD2 LEU A  25      23.061 -16.600 -18.907  1.00139.30           C
ATOM    209  C   LEU A  25      23.233 -20.821 -20.538  1.00126.34           C
ATOM    210  O   LEU A  25      22.923 -20.844 -21.737  1.00125.70           O
ATOM    211  N   ILE A  26      24.380 -21.273 -20.042  1.00143.74           N
ATOM    212  CA  ILE A  26      25.509 -21.680 -20.868  1.00161.46           C
ATOM    213  CB  ILE A  26      26.668 -22.244 -20.001  1.00166.23           C
ATOM    214  CG1 ILE A  26      27.355 -21.101 -19.243  1.00159.86           C
ATOM    215  CD1 ILE A  26      28.203 -21.540 -18.074  1.00156.44           C
ATOM    216  CG2 ILE A  26      27.720 -22.946 -20.855  1.00176.50           C
ATOM    217  C   ILE A  26      25.143 -22.657 -21.985  1.00163.72           C
ATOM    218  O   ILE A  26      25.654 -22.527 -23.103  1.00180.82           O
ATOM    219  N   GLU A  27      24.263 -23.612 -21.689  1.00159.20           N
ATOM    220  CA  GLU A  27      23.815 -24.606 -22.677  1.00160.61           C
ATOM    221  CB  GLU A  27      23.154 -25.799 -21.937  1.00174.39           C
ATOM    222  CG  GLU A  27      24.098 -26.541 -20.950  1.00185.43           C
ATOM    223  CD  GLU A  27      23.409 -27.477 -19.934  1.00192.95           C
ATOM    224  OE1 GLU A  27      22.177 -27.659 -20.016  1.00207.52           O
ATOM    225  OE2 GLU A  27      24.099 -28.046 -19.038  1.00178.50           O
ATOM    226  C   GLU A  27      22.898 -23.934 -23.750  1.00163.72           C
ATOM    227  O   GLU A  27      21.686 -24.171 -23.788  1.00162.53           O
ATOM    228  N   ALA A  28      23.523 -23.096 -24.601  1.00162.40           N
ATOM    229  CA  ALA A  28      22.877 -22.298 -25.677  1.00152.86           C
ATOM    230  CB  ALA A  28      22.384 -20.958 -25.141  1.00144.78           C
ATOM    231  C   ALA A  28      23.864 -22.047 -26.831  1.00145.06           C
ATOM    232  O   ALA A  28      23.639 -22.452 -27.978  1.00142.26           O
TER     233      ALA A  28
ATOM    374  N   GLU B  20      13.774 -11.046  -1.283  1.00130.89           N
ATOM    375  CA  GLU B  20      13.124 -11.735  -0.152  1.00138.33           C
ATOM    376  CB  GLU B  20      12.800 -13.193  -0.550  1.00148.17           C
ATOM    377  CG  GLU B  20      12.111 -14.070   0.465  1.00153.15           C
ATOM    378  CD  GLU B  20      10.717 -13.609   0.727  1.00160.39           C
ATOM    379  OE1 GLU B  20      10.159 -12.832  -0.079  1.00151.42           O
ATOM    380  OE2 GLU B  20      10.170 -14.079   1.737  1.00170.30           O
ATOM    381  C   GLU B  20      14.060 -11.697   1.027  1.00138.54           C
ATOM    382  O   GLU B  20      13.728 -11.199   2.095  1.00130.47           O
ATOM    383  N   PHE B  21      15.263 -12.197   0.790  1.00144.81           N
ATOM    384  CA  PHE B  21      16.259 -12.279   1.816  1.00135.67           C
ATOM    385  CB  PHE B  21      17.499 -13.002   1.320  1.00125.41           C
ATOM    386  CG  PHE B  21      18.508 -13.212   2.381  1.00122.02           C
ATOM    387  CD1 PHE B  21      18.185 -13.961   3.499  1.00129.64           C
ATOM    388  CE1 PHE B  21      19.101 -14.177   4.508  1.00127.24           C
ATOM    389  CZ  PHE B  21      20.359 -13.624   4.411  1.00131.04           C
ATOM    390  CE2 PHE B  21      20.688 -12.856   3.300  1.00129.78           C
ATOM    391  CD2 PHE B  21      19.759 -12.648   2.295  1.00123.17           C
ATOM    392  C   PHE B  21      16.657 -10.915   2.289  1.00131.98           C
ATOM    393  O   PHE B  21      16.778 -10.694   3.486  1.00128.42           O
ATOM    394  N   LEU B  22      16.859  -9.992   1.364  1.00139.28           N
ATOM    395  CA  LEU B  22      17.296  -8.673   1.755  1.00163.35           C
ATOM    396  CB  LEU B  22      17.447  -7.793   0.526  1.00192.62           C
ATOM    397  CG  LEU B  22      17.948  -6.375   0.733  1.00232.81           C
ATOM    398  CD1 LEU B  22      19.218  -6.322   1.574  1.00230.74           C
ATOM    399  CD2 LEU B  22      18.181  -5.759  -0.642  1.00253.04           C
ATOM    400  C   LEU B  22      16.339  -8.053   2.770  1.00165.39           C
ATOM    401  O   LEU B  22      16.777  -7.557   3.817  1.00177.01           O
ATOM    402  N   ARG B  23      15.047  -8.113   2.465  1.00168.66           N
ATOM    403  CA  ARG B  23      14.028  -7.562   3.357  1.00179.84           C
ATOM    404  CB  ARG B  23      12.741  -7.255   2.580  1.00188.82           C
ATOM    405  CG  ARG B  23      11.956  -8.473   2.120  1.00189.97           C
ATOM    406  CD  ARG B  23      10.929  -8.099   1.060  1.00184.17           C
ATOM    407  NE  ARG B  23      10.220  -9.285   0.565  1.00189.35           N
ATOM    408  CZ  ARG B  23       9.812  -9.495  -0.691  1.00185.89           C
ATOM    409  NH1 ARG B  23      10.089  -8.639  -1.676  1.00187.90           N
ATOM    410  NH2 ARG B  23       9.176 -10.632  -0.973  1.00187.23           N
ATOM    411  C   ARG B  23      13.741  -8.463   4.558  1.00169.52           C
ATOM    412  O   ARG B  23      13.244  -7.992   5.584  1.00181.00           O
ATOM    413  N   GLU B  24      14.034  -9.754   4.417  1.00154.74           N
ATOM    414  CA  GLU B  24      13.926 -10.716   5.529  1.00160.90           C
ATOM    415  CB  GLU B  24      14.531 -12.039   5.111  1.00165.11           C
ATOM    416  CG  GLU B  24      14.181 -13.168   6.028  1.00164.65           C
ATOM    417  CD  GLU B  24      12.958 -13.883   5.489  1.00163.28           C
ATOM    418  OE1 GLU B  24      11.993 -13.144   5.255  1.00181.13           O
ATOM    419  OE2 GLU B  24      12.947 -15.130   5.270  1.00153.57           O
ATOM    420  C   GLU B  24      14.752 -10.192   6.701  1.00167.96           C
ATOM    421  O   GLU B  24      14.324 -10.103   7.887  1.00180.22           O
ATOM    422  N   LEU B  25      15.949  -9.762   6.322  1.00175.28           N
ATOM    423  CA  LEU B  25      16.954  -9.298   7.272  1.00181.30           C
ATOM    424  CB  LEU B  25      18.323  -9.247   6.604  1.00186.58           C
ATOM    425  CG  LEU B  25      18.969 -10.561   6.200  1.00187.91           C
ATOM    426  CD1 LEU B  25      20.319 -10.220   5.596  1.00189.19           C
ATOM    427  CD2 LEU B  25      19.131 -11.536   7.358  1.00187.91           C
ATOM    428  C   LEU B  25      16.696  -7.933   7.897  1.00182.57           C
ATOM    429  O   LEU B  25      17.591  -7.381   8.544  1.00182.48           O
ATOM    430  N   ILE B  26      15.504  -7.379   7.710  1.00185.12           N
ATOM    431  CA  ILE B  26      15.141  -6.149   8.390  1.00192.20           C
ATOM    432  CB  ILE B  26      13.772  -5.635   7.894  1.00214.11           C
ATOM    433  CG1 ILE B  26      13.611  -4.134   8.188  1.00223.10           C
ATOM    434  CD1 ILE B  26      14.594  -3.246   7.450  1.00219.80           C
ATOM    435  CG2 ILE B  26      12.632  -6.490   8.458  1.00218.88           C
ATOM    436  C   ILE B  26      15.152  -6.332   9.918  1.00185.82           C
ATOM    437  O   ILE B  26      15.350  -5.356  10.662  1.00170.09           O
ATOM    438  N   GLU B  27      14.946  -7.582  10.363  1.00179.12           N
ATOM    439  CA  GLU B  27      15.184  -7.982  11.763  1.00180.79           C
ATOM    440  CB  GLU B  27      15.495  -9.503  11.864  1.00173.99           C
ATOM    441  CG  GLU B  27      15.483 -10.002  13.337  1.00181.18           C
ATOM    442  CD  GLU B  27      16.660 -10.900  13.773  1.00185.72           C
ATOM    443  OE1 GLU B  27      17.007 -11.807  13.006  1.00215.69           O
ATOM    444  OE2 GLU B  27      17.241 -10.679  14.873  1.00166.15           O
ATOM    445  C   GLU B  27      16.312  -7.187  12.487  1.00189.82           C
ATOM    446  O   GLU B  27      17.506  -7.409  12.243  1.00172.95           O
ATOM    447  N   ALA B  28      15.934  -6.303  13.407  1.00206.74           N
ATOM    448  CA  ALA B  28      16.932  -5.581  14.180  1.00203.89           C
ATOM    449  CB  ALA B  28      17.420  -4.393  13.376  1.00206.47           C
ATOM    450  C   ALA B  28      16.403  -5.166  15.549  1.00181.56           C
ATOM    451  O   ALA B  28      16.204  -6.030  16.396  1.00141.67           O
END
  """

  f = open("%s_model.pdb" % prefix, "w")
  f.write(pdb_str)
  f.close()
  f = open("%s_ncs.ncs" % prefix, "w")
  f.write("""
refinement.pdb_interpretation.ncs_group {
  reference = chain A and resseq 20:28
  selection = chain B and resseq 20:28
}
""")
  f.close()

  cmd = " ".join([
    "phenix.fmodel",
    "%s_model.pdb"%prefix,
    "high_res=15",
    "type=real",
    "label=f-obs",
    "r_free=0.1",
    "output.file_name=%s.mtz"%prefix,
    "> tmp.log"
  ])
  print(cmd)
  assert not easy_run.call(cmd)

  cmd = " ".join([
    "phenix.refine",
    "%s_model.pdb" % prefix,
    "%s.mtz" % prefix,
    "%s_ncs.ncs" % prefix,
    "output.prefix=%s_1" % prefix,
    "ncs_search.enabled=true",
    "ncs.type=cartesian",
    "--overwrite",
    "> %s_1.log" % prefix
  ])
  print(cmd)
  assert easy_run.call(cmd) # meant to fail
  log_out = open("%s_1_001.log" % prefix, "r")
  log_lines = log_out.readlines()
  log_out.close()
  for l in ["Sorry: Excessive distances to NCS averages:\n",
      "  The number of distances exceeding this limit is: 36\n"]:
    assert l in log_lines, "'%s' is not found in log file." % l

  # Now applying suggested parameter modification and make sure phenix.refine
  # finishes
  cmd = " ".join([
    "phenix.refine",
    "%s_model.pdb" % prefix,
    "%s.mtz" % prefix,
    "%s_ncs.ncs" % prefix,
    "strategy=individual_sites",
    "main.number_of_macro_cycles=1",
    "refinement.ncs.excessive_distance_limit=None",
    "output.prefix=%s_2" % prefix,
    "ncs_search.enabled=true",
    "ncs.type=cartesian",
    "--overwrite",
    "> %s_2.log" % prefix
  ])
  print(cmd)
  assert not easy_run.call(cmd)
  log_out = open("%s_2_001.log" % prefix, "r")
  log_lines = log_out.readlines()
  log_out.close()
  for l in ["=========================== phenix.refine: finished ===========================\n",
      ]:
    assert l in log_lines, "'%s' is not found in log file." % l

if (__name__ == "__main__"):
  t0 = time.time()
  exercise_1()
  print("OK. Time: %8.3f"%(time.time()-t0))
