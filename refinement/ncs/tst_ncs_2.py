from __future__ import print_function
import time
from libtbx import easy_run
import iotbx.pdb

pdb_str = """\n
CRYST1   21.598   12.560   17.485  90.00  90.00  90.00 P 1
ATOM      1  N   ALA A   1      18.331   9.040   8.557  1.00 10.00           N
ATOM      2  CA  ALA A   1      17.483   8.005   9.137  1.00 10.00           C
ATOM      3  C   ALA A   1      17.670   6.677   8.412  1.00 10.00           C
ATOM      4  O   ALA A   1      17.468   6.586   7.201  1.00 10.00           O
ATOM      5  CB  ALA A   1      16.024   8.432   9.098  1.00 10.00           C
ATOM      1  N   ALA A   2      18.058   5.645   9.163  1.00 10.00           N
ATOM      2  CA  ALA A   2      18.268   4.326   8.581  1.00 10.00           C
ATOM      3  C   ALA A   2      16.968   3.571   8.346  1.00 10.00           C
ATOM      4  O   ALA A   2      16.925   2.698   7.473  1.00 10.00           O
ATOM      5  CB  ALA A   2      19.190   3.495   9.477  1.00 10.00           C
ATOM      1  N   ALA A   3      15.912   3.883   9.100  1.00 10.00           N
ATOM      2  CA  ALA A   3      14.640   3.194   8.921  1.00 10.00           C
ATOM      3  C   ALA A   3      13.840   3.759   7.754  1.00 10.00           C
ATOM      4  O   ALA A   3      13.069   3.026   7.125  1.00 10.00           O
ATOM      5  CB  ALA A   3      13.815   3.268  10.206  1.00 10.00           C
ATOM      1  N   ALA A   4      14.006   5.048   7.451  1.00 10.00           N
ATOM      2  CA  ALA A   4      13.272   5.653   6.347  1.00 10.00           C
ATOM      3  C   ALA A   4      13.947   5.406   5.004  1.00 10.00           C
ATOM      4  O   ALA A   4      13.273   5.407   3.968  1.00 10.00           O
ATOM      5  CB  ALA A   4      13.109   7.156   6.582  1.00 10.00           C
ATOM      1  N   ALA A   5      15.259   5.194   4.999  1.00 10.00           N
ATOM      2  CA  ALA A   5      15.990   4.948   3.762  1.00 10.00           C
ATOM      3  C   ALA A   5      16.023   3.459   3.435  1.00 10.00           C
ATOM      4  O   ALA A   5      16.377   3.066   2.323  1.00 10.00           O
ATOM      5  CB  ALA A   5      17.403   5.501   3.862  1.00 10.00           C
TER
ATOM      1  N   ALA B   1       7.307   9.827  13.934  1.00 50.00           N
ATOM      2  CA  ALA B   1       7.548   8.402  14.129  1.00 50.00           C
ATOM      3  C   ALA B   1       7.794   7.703  12.797  1.00 50.00           C
ATOM      4  O   ALA B   1       7.278   8.122  11.760  1.00 50.00           O
ATOM      5  CB  ALA B   1       6.377   7.761  14.857  1.00 50.00           C
ATOM      1  N   ALA B   2       8.588   6.631  12.831  1.00 50.00           N
ATOM      2  CA  ALA B   2       8.895   5.882  11.620  1.00 50.00           C
ATOM      3  C   ALA B   2       7.783   4.920  11.224  1.00 50.00           C
ATOM      4  O   ALA B   2       7.712   4.526  10.054  1.00 50.00           O
ATOM      5  CB  ALA B   2      10.204   5.109  11.796  1.00 50.00           C
ATOM      1  N   ALA B   3       6.918   4.535  12.163  1.00 50.00           N
ATOM      2  CA  ALA B   3       5.832   3.616  11.843  1.00 50.00           C
ATOM      3  C   ALA B   3       4.627   4.335  11.252  1.00 50.00           C
ATOM      4  O   ALA B   3       3.941   3.776  10.389  1.00 50.00           O
ATOM      5  CB  ALA B   3       5.417   2.837  13.092  1.00 50.00           C
ATOM      1  N   ALA B   4       4.354   5.563  11.696  1.00 50.00           N
ATOM      2  CA  ALA B   4       3.214   6.306  11.171  1.00 50.00           C
ATOM      3  C   ALA B   4       3.548   6.986   9.849  1.00 50.00           C
ATOM      4  O   ALA B   4       2.746   6.948   8.909  1.00 50.00           O
ATOM      5  CB  ALA B   4       2.740   7.336  12.196  1.00 50.00           C
ATOM      1  N   ALA B   5       4.719   7.609   9.758  1.00 50.00           N
ATOM      2  CA  ALA B   5       5.131   8.290   8.536  1.00 50.00           C
ATOM      3  C   ALA B   5       5.906   7.347   7.622  1.00 50.00           C
ATOM      4  O   ALA B   5       5.709   7.345   6.406  1.00 50.00           O
ATOM      5  CB  ALA B   5       5.969   9.515   8.869  1.00 50.00           C
TER
END
"""

def run(prefix="exercise_tst_ncs_2"):
  """
  Basic NCS restrained refinement in presence of twinning: no check results,
  just make sure it runs
  """
  pdb_in = "%s.pdb"%prefix
  fo = open(pdb_in, "w")
  print(pdb_str, file=fo)
  fo.close()
  cmd = " ".join([
    "phenix.fmodel",
    "%s"%pdb_in,
    "high_res=5",
    "type=real",
    "label=f-obs",
    "r_free=0.1",
    "twin_fraction=0.3",
    "twin_law='%s'"%"h,-k,l-h",
    "output.file_name=%s.mtz"%prefix,
    "> zlog1"])
  print(cmd)
  assert not easy_run.call(cmd)
  #
  cmd = " ".join([
    "phenix.refine",
    "%s.{pdb,mtz}"%prefix,
    "refinement.ncs.excessive_distance_limit=None",
    "ncs_search.enabled=true",
    "main.number_of_macro_cycles=2",
    "ncs.type=cartesian",
    "main.bulk_sol=False",
    "xray_data.twin_law='%s'"%"h,-k,l-h",
    "output.prefix=%s"%prefix,
    "main.target=ls",
    "ncs.type=constraints",
    "--overwrite",
    "--quiet",
    "> zlog2",
  ])
  print(cmd)
  assert not easy_run.call(cmd)

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK time =%8.3f"%(time.time() - t0))
