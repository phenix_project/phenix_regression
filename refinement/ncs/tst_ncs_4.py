from __future__ import print_function
from libtbx import easy_run
import time
from libtbx.test_utils import assert_lines_in_file

def exercise_1(prefix="tst_ncs_4"):
  """
  One atom has altloc != ''. Nevertheless, this is still one conformer and
  no alternative conformations. Cartesian NCS should be OK with it.
  """
  pdb_str = """\
CRYST1  272.680  272.680  272.680  90.00  90.00  90.00 F 2 3        96
ORIGX1      1.000000  0.000000  0.000000        0.00000
ORIGX2      0.000000  1.000000  0.000000        0.00000
ORIGX3      0.000000  0.000000  1.000000        0.00000
SCALE1      0.003667  0.000000  0.000000        0.00000
SCALE2      0.000000  0.003667  0.000000        0.00000
SCALE3      0.000000  0.000000  0.003667        0.00000
ATOM      8  N   LYS A   6      -6.299 -35.136 -26.797  1.00346.92           N
ATOM      9  CA  LYS A   6      -5.853 -36.460 -27.290  1.00342.42           C
ATOM     10  C   LYS A   6      -4.360 -36.521 -27.643  1.00352.04           C
ATOM     11  O   LYS A   6      -3.590 -37.208 -26.966  1.00352.01           O
ATOM     12  CB  LYS A   6      -6.696 -36.909 -28.503  1.00331.55           C
ATOM     13  CG  LYS A   6      -8.147 -37.258 -28.188  1.00316.11           C
ATOM     14  CD  LYS A   6      -8.818 -38.053 -29.310  1.00301.59           C
ATOM     15  CE  LYS A   6     -10.293 -38.320 -29.028  1.00289.30           C
ATOM     16  NZ  LYS A   6     -10.518 -39.382 -28.008  1.00277.90           N
ATOM     17  N   LEU A   7      -3.960 -35.813 -28.701  1.00359.07           N
ATOM     18  CA ALEU A   7      -2.543 -35.701 -29.071  0.50352.98           C
ATOM     19  C   LEU A   7      -1.964 -34.379 -28.552  1.00307.51           C
ATOM     20  O   LEU A   7      -2.502 -33.305 -28.832  1.00295.11           O
ATOM     21  CB  LEU A   7      -2.337 -35.825 -30.589  1.00393.68           C
ATOM     22  CG  LEU A   7      -2.092 -37.246 -31.147  1.00436.57           C
ATOM     23  CD1 LEU A   7      -3.388 -38.030 -31.314  1.00445.19           C
ATOM     24  CD2 LEU A   7      -1.342 -37.242 -32.482  1.00452.49           C
ATOM     25  N   PRO A   8      -0.853 -34.454 -27.801  1.00269.40           N
ATOM     26  CA  PRO A   8      -0.233 -33.325 -27.108  1.00253.11           C
ATOM     27  C   PRO A   8       0.677 -32.480 -28.000  1.00237.32           C
ATOM     28  O   PRO A   8       1.787 -32.132 -27.603  1.00240.47           O
ATOM     29  CB  PRO A   8       0.594 -34.021 -26.032  1.00255.21           C
ATOM     30  CG  PRO A   8       1.059 -35.267 -26.717  1.00257.51           C
ATOM     31  CD  PRO A   8      -0.124 -35.708 -27.538  1.00262.23           C
ATOM   1986  N   LYS B   6      26.818-101.173   6.312  1.00348.94           N
ATOM   1987  CA  LYS B   6      27.312 -99.853   5.861  1.00342.22           C
ATOM   1988  C   LYS B   6      27.664 -99.799   4.367  1.00355.30           C
ATOM   1989  O   LYS B   6      26.989 -99.113   3.596  1.00359.02           O
ATOM   1990  CB  LYS B   6      28.525 -99.400   6.702  1.00330.18           C
ATOM   1991  CG  LYS B   6      28.207 -99.045   8.151  1.00316.88           C
ATOM   1992  CD  LYS B   6      29.327 -98.251   8.824  1.00307.62           C
ATOM   1993  CE  LYS B   6      29.041 -97.980  10.297  1.00298.31           C
ATOM   1994  NZ  LYS B   6      28.020 -96.918  10.518  1.00288.26           N
ATOM   1995  N   LEU B   7      28.721-100.511   3.970  1.00364.96           N
ATOM   1996  CA ALEU B   7      29.089-100.632   2.553  0.50360.39           C
ATOM   1997  C   LEU B   7      28.564-101.955   1.980  1.00318.84           C
ATOM   1998  O   LEU B   7      28.843-103.026   2.524  1.00312.63           O
ATOM   1999  CB  LEU B   7      30.608-100.510   2.345  1.00393.29           C
ATOM   2000  CG  LEU B   7      31.169 -99.092   2.094  1.00429.20           C
ATOM   2001  CD1 LEU B   7      31.339 -98.302   3.386  1.00439.09           C
ATOM   2002  CD2 LEU B   7      32.503 -99.102   1.343  1.00451.08           C
ATOM   2003  N   PRO B   8      27.811-101.884   0.870  1.00278.08           N
ATOM   2004  CA  PRO B   8      27.114-103.013   0.256  1.00254.15           C
ATOM   2005  C   PRO B   8      28.003-103.862  -0.650  1.00232.31           C
ATOM   2006  O   PRO B   8      27.605-104.212  -1.758  1.00230.50           O
ATOM   2007  CB  PRO B   8      26.039-102.316  -0.573  1.00259.17           C
ATOM   2008  CG  PRO B   8      26.728-101.073  -1.044  1.00266.60           C
ATOM   2009  CD  PRO B   8      27.553-100.631   0.136  1.00273.44           C
END
  """

  f = open("%s_model.pdb" % prefix, "w")
  f.write(pdb_str)
  f.close()
  cmd = " ".join([
    "phenix.fmodel",
    "%s_model.pdb"%prefix,
    "high_res=15",
    "type=real",
    "label=f-obs",
    "r_free=0.1",
    "output.file_name=%s.mtz"%prefix,
    "> tmp.log"
  ])
  print(cmd)
  assert not easy_run.call(cmd)

  for excl in ["water or element H or element D",
    "water or element H or element D or resid 6"]:
    cmd = " ".join([
      "phenix.refine",
      "%s_model.pdb" % prefix,
      "%s.mtz" % prefix,
      "main.number_of_macro_cycles=0",
      "output.prefix=%s_1" % prefix,
      "output.write_eff_file=False",
      "output.write_geo_file=False",
      "output.write_def_file=True",
      "output.write_map_coefficients=False",
      "ncs_search.enabled=true",
      "ncs.type=cartesian",
      'ncs_search.exclude_selection="%s"' % excl,
      "--overwrite",
      "> %s_1.log" % prefix
    ])
    print(cmd)
    assert not easy_run.call(cmd)
    assert_lines_in_file(file_name="%s_1_001.log" % prefix,
        lines="""
  Number of NCS groups: 1

  refinement.pdb_interpretation.ncs_group {
    reference = chain 'A'
    selection = chain 'B'
  }
        """)
    assert_lines_in_file(file_name="%s_1_001.log" % prefix,
        lines="exclude_selection = %s" % excl)
    assert_lines_in_file(file_name="%s_1_002.def" % prefix,
        lines="""ncs_search {
                   enabled = True
                   exclude_selection = %s""" % excl)
    assert_lines_in_file(file_name="%s_1_001.log" % prefix,
        lines="""
  =========================== phenix.refine: finished ===========================
        """)

if (__name__ == "__main__"):
  t0 = time.time()
  exercise_1()
  print("OK. Time: %8.3f"%(time.time()-t0))
