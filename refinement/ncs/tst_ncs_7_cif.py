from __future__ import print_function
from libtbx import easy_run
import time
from libtbx.test_utils import assert_lines_in_file, show_diff, approx_equal
import libtbx.load_env
import os


def exercise_1(prefix="tst_ncs_7_cif"):
  pdb_fname = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/2bfu_mm.pdb",
    test=os.path.isfile)

  cmd = " ".join([
    "phenix.fmodel",
    "%s" % pdb_fname,
    "high_res=10",
    "type=real",
    "label=f-obs",
    "r_free=0.1",
    "output.file_name=%s.mtz" % prefix,
    "> tmp.log"
  ])
  print(cmd)
  assert not easy_run.call(cmd)


  cmd = " ".join([
    "phenix.refine",
    "%s" % pdb_fname,
    "%s.mtz" % prefix,
    "ncs.type=cartesian",
    "ncs_search.enabled=True",
    "main.number_of_mac=0",
    "overwrite=true",
    "refinement.pdb_interpretation.clash_guard.nonbonded_distance_threshold=None",
    "output.prefix=%s_1" % prefix,
    "output.write_eff_file=False",
    "output.write_geo_file=False",
    "output.write_map_coefficients=False",
    "> %s.screen" % prefix
  ])
  print(cmd)
  assert not easy_run.call(cmd)
  # check that log in file is the same as log on the screen
  scr = open("%s.screen" % prefix,'r')
  scr_lines = scr.read()
  scr.close()
  log=open("%s_1_001.log" % prefix, 'r')
  log_lines=log.read()
  log.close()
  assert not show_diff(log_lines, scr_lines)

  of_cif = "%s_1_001.cif" % prefix
  assert_lines_in_file(file_name=of_cif, lines="""\
      loop_
        _struct_ncs_ens.id
        _struct_ncs_ens.details
         ens_1 ?
         ens_2 ?
  """)

  # Note here we use chain ids and residue ids for Phenix selection syntax
  assert_lines_in_file(file_name=of_cif, lines="""\
      loop_
        _struct_ncs_dom.pdbx_ens_id
        _struct_ncs_dom.id
        _struct_ncs_dom.details
         ens_1 d_1 'chain "C"'
         ens_1 d_2 '(chain "E" and (resid 1 through 9 or resid 11 through 20))'
         ens_1 d_3 '(chain "G" and (resid 1 through 9 or resid 11 through 20))'
         ens_1 d_4 '(chain "I" and (resid 1 through 9 or resid 11 through 20))'
         ens_2 d_1 'chain "D"'
         ens_2 d_2 '(chain "F" and (resid 1 through 9 or (resid 10 and (name N or name CA or name C or name O )) or resid 11 through 20))'
         ens_2 d_3 '(chain "H" and (resid 1 through 9 or (resid 10 and (name N or name CA or name C or name O )) or resid 11 through 20))'
         ens_2 d_4 '(chain "J" and (resid 1 through 9 or (resid 10 and (name N or name CA or name C or name O )) or resid 11 through 20))'
  """)

  assert_lines_in_file(file_name=of_cif, lines="""\
      loop_
        _struct_ncs_ens_gen.ens_id
        _struct_ncs_ens_gen.dom_id_1
        _struct_ncs_ens_gen.dom_id_2
        _struct_ncs_ens_gen.oper_id
         ens_1 d_2 d_1 op_1
         ens_1 d_3 d_1 op_2
         ens_1 d_4 d_1 op_3
         ens_2 d_2 d_1 op_4
         ens_2 d_3 d_1 op_5
         ens_2 d_4 d_1 op_6
  """)

  # And here label_asym_id and label_seq_id are used (they are generated independently)
  assert_lines_in_file(file_name=of_cif, lines="""\
      loop_
        _struct_ncs_dom_lim.pdbx_ens_id
        _struct_ncs_dom_lim.dom_id
        _struct_ncs_dom_lim.pdbx_component_id
        _struct_ncs_dom_lim.beg_label_alt_id
        _struct_ncs_dom_lim.beg_label_asym_id
        _struct_ncs_dom_lim.beg_label_comp_id
        _struct_ncs_dom_lim.beg_label_seq_id
        _struct_ncs_dom_lim.end_label_alt_id
        _struct_ncs_dom_lim.end_label_asym_id
        _struct_ncs_dom_lim.end_label_comp_id
        _struct_ncs_dom_lim.end_label_seq_id
         ens_1 d_1 1 . A MET 1 . A LEU 19
         ens_1 d_2 1 . C MET 1 . C SER 9
         ens_1 d_2 2 . C ASP 11 . C LEU 20
         ens_1 d_3 1 . E MET 1 . E SER 9
         ens_1 d_3 2 . E ASP 11 . E LEU 20
         ens_1 d_4 1 . G MET 1 . G SER 9
         ens_1 d_4 2 . G ASP 11 . G LEU 20
         ens_2 d_1 1 . B GLY 1 . B PRO 20
         ens_2 d_2 1 . D GLY 1 . D PRO 20
         ens_2 d_3 1 . F GLY 1 . F PRO 20
         ens_2 d_4 1 . H GLY 1 . H PRO 20
  """)

  assert_lines_in_file(file_name=of_cif, lines="""\
      loop_
        _struct_ncs_oper.id
        _struct_ncs_oper.code
        _struct_ncs_oper.matrix[1][1]
        _struct_ncs_oper.matrix[1][2]
        _struct_ncs_oper.matrix[1][3]
        _struct_ncs_oper.matrix[2][1]
        _struct_ncs_oper.matrix[2][2]
        _struct_ncs_oper.matrix[2][3]
        _struct_ncs_oper.matrix[3][1]
        _struct_ncs_oper.matrix[3][2]
        _struct_ncs_oper.matrix[3][3]
        _struct_ncs_oper.vector[1]
        _struct_ncs_oper.vector[2]
        _struct_ncs_oper.vector[3]
        _struct_ncs_oper.details
  """)
  #        op_1 given 0.309017663186 -0.809017520605 0.499998735191 0.809015098162 0.500001102147 0.309020175388 -0.500002654777 0.309013833356 0.809016561024 7.54349608911e-05 -0.000203656443603 0.000312599634938 ?
  #        op_2 given -0.809016805907 -0.500000608203 0.309016503698 0.499999345685 -0.309015581879 0.809017938287 -0.30901854649 0.809017158009 0.499998775971 6.99431583513e-05 -0.000183284989571 5.19715715654e-05 ?
  #        op_3 given 0.309018546487 -0.809017158015 -0.499998775963 0.809016805905 0.500000608199 -0.309016503707 0.499999345689 -0.30901558187 0.809017938288 -5.19713815663e-05 -6.99422712316e-05 -0.000183285955327 ?
  #        op_4 given 0.309017950623 -0.8090201539 0.499994296744 0.809020625505 0.499996168392 0.309013687562 -0.499993533658 0.309014922256 0.80902178223 0.000801679734159 0.000570701844254 -0.000488087102013 ?
  #        op_5 given -0.809015151208 -0.499999242081 0.309023046121 0.500005038936 -0.30901556914 0.809014424511 -0.309013666614 0.809018007184 0.500000417898 -0.000773920684663 5.39183587733e-05 -0.000205323092885 ?
  #        op_6 given 0.309013666614 -0.809018007184 -0.500000417898 0.809015151208 0.499999242081 -0.309023046121 0.500005038936 -0.30901556914 0.809014424511 0.0002053230928 0.000773920684937 5.39183588018e-05 ?
  # """)

  assert_lines_in_file(file_name=of_cif, lines="""\
      loop_
        _refine_ls_restr_ncs.pdbx_ordinal
        _refine_ls_restr_ncs.pdbx_ens_id
        _refine_ls_restr_ncs.dom_id
        _refine_ls_restr_ncs.pdbx_refine_id
        _refine_ls_restr_ncs.pdbx_asym_id
        _refine_ls_restr_ncs.pdbx_type
        _refine_ls_restr_ncs.weight_position
        _refine_ls_restr_ncs.weight_B_iso
        _refine_ls_restr_ncs.rms_dev_position
        _refine_ls_restr_ncs.rms_dev_B_iso
        _refine_ls_restr_ncs.ncs_model_details
  """)
  #        1 ens_1 d_2 'X-RAY DIFFRACTION' 'A' 'cartesian NCS' ? ? 0.000601419464996 ? ?
  #        2 ens_1 d_3 'X-RAY DIFFRACTION' 'A' 'cartesian NCS' ? ? 0.000603313465248 ? ?
  #        3 ens_1 d_4 'X-RAY DIFFRACTION' 'A' 'cartesian NCS' ? ? 0.000603313465248 ? ?
  #        4 ens_2 d_2 'X-RAY DIFFRACTION' 'B' 'cartesian NCS' ? ? 0.000612794355943 ? ?
  #        5 ens_2 d_3 'X-RAY DIFFRACTION' 'B' 'cartesian NCS' ? ? 0.000607809357093 ? ?
  #        6 ens_2 d_4 'X-RAY DIFFRACTION' 'B' 'cartesian NCS' ? ? 0.000607809357093 ? ?

  checks = 0
  with open(of_cif, 'r') as f:
    of_cif_lines = f.readlines()
    for l in of_cif_lines:
      if l.strip().startswith("1 ens_1 d_2 'X-RAY DIFFRACTION' 'A' 'cartesian NCS'"):
        sp = l.split()
        assert approx_equal(float(sp[10]), 0.000601419464996)
        checks += 1
      if l.strip().startswith("5 ens_2 d_3 'X-RAY DIFFRACTION' 'B' 'cartesian NCS'"):
        sp = l.split()
        assert approx_equal(float(sp[10]), 0.000607809357093, 1e-4)
        checks += 1
      if l.strip().startswith("op_1 given"):
        answer = [0.309017663186, -0.809017520605, 0.499998735191, 0.809015098162,
            0.500001102147, 0.309020175388, -0.500002654777, 0.309013833356,
            0.809016561024, 7.54349608911e-05, -0.000203656443603, 0.000312599634938]
        sp = l.split()
        res = [float(x) for x in sp[2:-1]]
        assert approx_equal(res, answer)
        checks += 1
  assert checks == 3, checks


if (__name__ == "__main__"):
  t0 = time.time()
  exercise_1()
  print("OK. Time: %8.3f"%(time.time()-t0))
