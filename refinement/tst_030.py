from __future__ import division
from __future__ import print_function
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel
import os
import libtbx.load_env
from libtbx.test_utils import approx_equal

pdb_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="pdb",
  test=os.path.isdir)

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Mask optimization.
  """
  r_solv = 0.4
  pairs = [("pdb1akg_very_well_phenix_refined_001.pdb",1.3),
          ("pdb1akg_very_well_phenix_refined_001_noH.pdb",0.2)]
  for pdb_file_name_, r_shrink in pairs:
    pdb_file_name = "%s/%s"%(pdb_dir, pdb_file_name_)
    args = [
      pdb_file_name,
      'label=f-obs',
      'type=real',
      'high_res=1.5',
      'r_free=0.1',
      'solvent_radius=%s'%str(r_solv),
      'shrink_truncation_radius=%s'%str(r_shrink),
      'k_sol=0.5 b_sol=30.0']
    r = run_fmodel(args = args, prefix = prefix)
    args = [
      pdb_file_name,
      r.mtz,
      'optimize_mask=True',
      'strategy=none main.number_of_macro=3',
      'optimize_scattering_contribution=false',
      'bulk_solvent_and_scale.anisotropic_scaling=false',
      'xray_data.r_free_flags.ignore_r_free_flags=true',
      'bulk_solvent_and_scale.mode=slow']
    r = run_phenix_refine(args = args, prefix = prefix)
    assert r.r_work_start > 0.01
    assert approx_equal(r.r_work_final, 0.0)
    assert approx_equal(r.r_free_final, 0.0)

if (__name__ == "__main__"):
  run()
  print("OK")
