from __future__ import division
from __future__ import print_function
from iotbx import pdb
import os.path
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel

def write_pdb(file_name):
  open(file_name, "w").write("""\
ATOM      1  N   TYR A   7       8.292   1.817   6.147  1.00 14.70           N
ANISOU    1  N   TYR A   7     1862   1862   1862      0      0     -0       N
ATOM      2  CA  TYR A   7       9.159   2.144   7.299  1.00 15.18           C
ANISOU    2  CA  TYR A   7     1923   1923   1923      0      0     -0       C
ATOM      3  C   TYR A   7      10.603   2.331   6.885  1.00 15.91           C
ANISOU    3  C   TYR A   7     2015   2015   2015      0      0     -0       C
ATOM      4  O   TYR A   7      11.041   1.811   5.855  1.00 15.76           O
ANISOU    4  O   TYR A   7     1996   1996   1996      0      0     -0       O
ATOM      5  CB  TYR A   7       9.061   1.065   8.369  1.00 15.35           C
ANISOU    5  CB  TYR A   7     1944   1944   1944      0      0     -0       C
ATOM      6  CG  TYR A   7       7.665   0.929   8.902  1.00 14.45           C
ANISOU    6  CG  TYR A   7     1830   1830   1830      0      0     -0       C
ATOM      7  CD1 TYR A   7       6.771   0.021   8.327  1.00 15.68           C
ANISOU    7  CD1 TYR A   7     1986   1986   1986      0      0     -0       C
ATOM      8  CD2 TYR A   7       7.210   1.756   9.920  1.00 14.80           C
ANISOU    8  CD2 TYR A   7     1874   1874   1874      0      0     -0       C
ATOM      9  CE1 TYR A   7       5.480  -0.094   8.796  1.00 13.46           C
ANISOU    9  CE1 TYR A   7     1705   1705   1705      0      0     -0       C
ATOM     10  CE2 TYR A   7       5.904   1.649  10.416  1.00 14.33           C
ANISOU   10  CE2 TYR A   7     1815   1815   1815      0      0     -0       C
ATOM     11  CZ  TYR A   7       5.047   0.729   9.831  1.00 15.09           C
ANISOU   11  CZ  TYR A   7     1911   1911   1911      0      0     -0       C
ATOM     12  OH  TYR A   7       3.766   0.589  10.291  1.00 14.39           O
ANISOU   12  OH  TYR A   7     1823   1823   1823      0      0     -0       O
ATOM     13  OXT TYR A   7      11.358   2.999   7.612  1.00 17.49           O
ANISOU   13  OXT TYR A   7     2215   2215   2215      0      0      0       O
ATOM     14  H   TYR A   7       8.075   0.845   6.052  1.00 14.70           H
ANISOU   14  H   TYR A   7     1862   1862   1862      0      0     -0       H
ATOM     15  HA  TYR A   7       8.800   3.098   7.713  1.00 15.18           H
ANISOU   15  HA  TYR A   7     1923   1923   1923      0      0     -0       H
ATOM     16 1HB  TYR A   7       9.388   0.102   7.949  1.00 15.35           H
ANISOU   16 1HB  TYR A   7     1944   1944   1944      0      0     -0       H
ATOM     17 2HB  TYR A   7       9.747   1.305   9.195  1.00 15.35           H
ANISOU   17 2HB  TYR A   7     1944   1944   1944      0      0     -0       H
ATOM     18  HD1 TYR A   7       7.102  -0.610   7.489  1.00 15.68           H
ANISOU   18  HD1 TYR A   7     1986   1986   1986      0      0     -0       H
ATOM     19  HD2 TYR A   7       7.888   2.510  10.346  1.00 14.80           H
ANISOU   19  HD2 TYR A   7     1874   1874   1874      0      0     -0       H
ATOM     20  HE1 TYR A   7       4.795  -0.833   8.354  1.00 13.46           H
ANISOU   20  HE1 TYR A   7     1705   1705   1705      0      0     -0       H
ATOM     21  HE2 TYR A   7       5.566   2.281  11.251  1.00 14.33           H
ANISOU   21  HE2 TYR A   7     1815   1815   1815      0      0     -0       H
ATOM     22  HH  TYR A   7       3.603   1.243  11.030  1.00 14.39           H
ANISOU   22  HH  TYR A   7     1823   1823   1823      0      0     -0       H
TER
END
""")

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Exercise handling of H with anisotropic ADP: xray vs neutron data.
  """
  cntr = 0
  for scattering_table in ["n_gaussian","neutron"]:
    pdb_in = "%s.pdb"%prefix
    write_pdb(file_name = pdb_in)
    args = [
      pdb_in,
      "high_resolution=1.0",
      "r_free_flags_fraction=0.1",
      "type=real",
      "scattering_table=%s"%scattering_table,
      "generate_fake_p1_symmetry=True"]
    r = run_fmodel(args = args, prefix = prefix)
    args = [
      r.mtz, pdb_in,
      "hydrogens.refine=individual",
      "main.number_of_macro_cycles=1",
      "main.scattering_table=%s"%scattering_table]
    r = run_phenix_refine(args = args, prefix = prefix)
    #
    pdb_in = pdb.input(r.pdb)
    xrs = pdb_in.xray_structure_simple()
    hd_sel = xrs.hd_selection()
    aniso_sel = xrs.use_u_aniso()
    iso_sel = xrs.use_u_iso()
    if(scattering_table=="n_gaussian"):
      assert (hd_sel & aniso_sel).count(True) == 0
      assert hd_sel.all_eq(iso_sel)
      cntr+=1
    else:
      assert aniso_sel.select(hd_sel).all_eq(True)
      cntr+=1
  assert cntr == 2

if (__name__ == "__main__") :
  run()
  print("OK")
