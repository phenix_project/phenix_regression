from __future__ import division
from __future__ import print_function
import libtbx.load_env
import libtbx.path
import time, os
from libtbx.test_utils import assert_lines_in_file, approx_equal
from iotbx import pdb
from phenix_regression.refinement import run_phenix_refine

input_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/ncs/ncs_solvent.pdb",
    test=os.path.isfile)
hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/ncs/ncs_solvent.mtz",
    test=os.path.isfile)

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Torsion NCS with solvent.
  """
  f = open("%s_ncs.eff"%prefix, "w")
  f.write("""
refinement.pdb_interpretation.ncs_group {
  reference = chain A
  selection = chain B
}""")
  f.close()
  args = [
    input_file,
    hkl,
    "main.number_of_macro_cycles=2",
    "main.bulk_solv=true",
    "ncs_search.enabled=true",
    "main.ordered_solvent=True",
    "ordered_solvent.mode=every_macro_cycle",
    "ncs.type=torsion",
    "strategy=individual_sites",
    "output.write_model_cif_file=True",
    "%s_ncs.eff" % prefix]
  r = run_phenix_refine(args = args, prefix = prefix)
  assert r.r_work_start >0.24,  "expected %f>0.24" %  r.r_work_start
  assert r.r_work_final <0.258, "expected %f<0.258" % r.r_work_final
  assert r.r_free_start >0.30,  "expected %f>0.31" %  r.r_free_start
  assert r.r_free_final <0.303,  "expected %f<0.303" %  r.r_free_final

  f = open(r.pdb, 'r')
  final_pdb = f.readlines()
  f.close()
  assert final_pdb.count("REMARK   3  TORSION NCS DETAILS.\n") == 1, ""+\
      "Expecting REMARK 3 with torsion NCS details."
  hoh_count = 0
  for line in final_pdb:
    if line.startswith("HETATM") and line.find("HOH S"):
      hoh_count += 1
  assert hoh_count == 4, "Expecting 4 waters in final pdb, got %d." % hoh_count

  of_cif=r.cif

  assert_lines_in_file(of_cif, """\
      loop_
        _struct_ncs_ens.id
        _struct_ncs_ens.details
         ens_1 ?

      loop_
        _struct_ncs_dom.pdbx_ens_id
        _struct_ncs_dom.id
        _struct_ncs_dom.details
         ens_1 d_1 '(chain "A" and resid 1 through 6)'
         ens_1 d_2 'chain "B"'

      loop_
        _struct_ncs_ens_gen.ens_id
        _struct_ncs_ens_gen.dom_id_1
        _struct_ncs_ens_gen.dom_id_2
        _struct_ncs_ens_gen.oper_id
         ens_1 d_2 d_1 op_1

      loop_
        _struct_ncs_dom_lim.pdbx_ens_id
        _struct_ncs_dom_lim.dom_id
        _struct_ncs_dom_lim.pdbx_component_id
        _struct_ncs_dom_lim.beg_label_alt_id
        _struct_ncs_dom_lim.beg_label_asym_id
        _struct_ncs_dom_lim.beg_label_comp_id
        _struct_ncs_dom_lim.beg_label_seq_id
        _struct_ncs_dom_lim.end_label_alt_id
        _struct_ncs_dom_lim.end_label_asym_id
        _struct_ncs_dom_lim.end_label_comp_id
        _struct_ncs_dom_lim.end_label_seq_id
         ens_1 d_1 1 . A LYS 1 . A ALA 6
         ens_1 d_2 1 . C LYS 1 . C ALA 6

      loop_
        _struct_ncs_oper.id
        _struct_ncs_oper.code
        _struct_ncs_oper.matrix[1][1]
        _struct_ncs_oper.matrix[1][2]
        _struct_ncs_oper.matrix[1][3]
        _struct_ncs_oper.matrix[2][1]
        _struct_ncs_oper.matrix[2][2]
        _struct_ncs_oper.matrix[2][3]
        _struct_ncs_oper.matrix[3][1]
        _struct_ncs_oper.matrix[3][2]
        _struct_ncs_oper.matrix[3][3]
        _struct_ncs_oper.vector[1]
        _struct_ncs_oper.vector[2]
        _struct_ncs_oper.vector[3]
        _struct_ncs_oper.details
  """)
         # op_1 given -0.997730801391 -0.0584435779303 -0.0334304674455 -0.0597916876617 0.997374366563 0.0408573984666 0.030954838741 0.0427635489827 -0.998605566196 -1.50720558745 -5.05910759567 10.4414332325 ?
  assert_lines_in_file(of_cif, """\
      loop_
        _refine_ls_restr_ncs.pdbx_ordinal
        _refine_ls_restr_ncs.pdbx_ens_id
        _refine_ls_restr_ncs.dom_id
        _refine_ls_restr_ncs.pdbx_refine_id
        _refine_ls_restr_ncs.pdbx_asym_id
        _refine_ls_restr_ncs.pdbx_type
        _refine_ls_restr_ncs.weight_position
        _refine_ls_restr_ncs.weight_B_iso
        _refine_ls_restr_ncs.rms_dev_position
        _refine_ls_restr_ncs.rms_dev_B_iso
        _refine_ls_restr_ncs.ncs_model_details
  """)
  #        1 ens_1 d_2 'X-RAY DIFFRACTION' 'A' 'Torsion NCS' ? ? 1.75362501302 ? ?
  # """)

  checks = 0
  with open(of_cif, 'r') as f:
    of_cif_lines = f.readlines()
    for l in of_cif_lines:
      if l.strip().startswith("1 ens_1 d_2 'X-RAY DIFFRACTION' 'A' 'Torsion NCS'"):
        sp = l.split()
        assert approx_equal(float(sp[10]), 1.83193523069, 1e-2), 'sp[10] %s not equal %s' % (
          sp[10], 1.81027368453)
        checks += 1
      if l.strip().startswith("op_1 given"):
        answer = [-0.997352193346, -0.064396530228, -0.0337888934666,
          -0.0663468407752, 0.995979979657, 0.0601828616926, 0.0297774939553,
          0.0622652954457, -0.997615323578, -1.51233914781, -5.18791815313,
          10.4612245772]
        sp = l.split()
        res = [float(x) for x in sp[2:-1]]
        assert approx_equal(res, answer, 1e-2)
        checks += 1
  assert checks == 2


if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK. Time: %8.3f"%(time.time()-t0))
