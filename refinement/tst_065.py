from __future__ import division
from __future__ import print_function
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel
import os
from iotbx import pdb
from cctbx.array_family import flex
import mmtbx.model
import iotbx.pdb
from libtbx.utils import null_out

pdb_str="""
CRYST1   32.540   19.386   21.825  90.00  90.00  90.00 P 1
SCALE1      0.030731  0.000000  0.000000        0.00000
SCALE2      0.000000  0.051584  0.000000        0.00000
SCALE3      0.000000  0.000000  0.045819        0.00000
ATOM      1  N   LYS A   1      25.007  11.941   9.405  1.00 12.85           N
ATOM      2  CA  LYS A   1      23.875  11.316   9.376  1.00 11.37           C
ATOM      3  C   LYS A   1      22.778  12.144   9.355  1.00 11.88           C
ATOM      4  O   LYS A   1      22.768  13.079   8.977  1.00 11.02           O
ATOM      5  CB  LYS A   1      23.956  11.315  10.942  1.00 15.61           C
ATOM      6  CG  LYS A   1      24.229  12.388  12.052  1.00 23.32           C
ATOM      7  CD  LYS A   1      25.396  12.649  12.464  1.00 22.74           C
ATOM      8  CE  LYS A   1      25.051  14.393  13.315  1.00 22.96           C
ATOM      9  NZ  LYS A   1      26.493  14.541  13.631  1.00 18.89           N
ATOM     10  H1  LYS A   1      25.906  12.046   9.928  1.00 12.85           H
ATOM     11  H2  LYS A   1      25.175  11.306   8.638  1.00 12.85           H
ATOM     12  H3  LYS A   1      24.986  12.653   9.085  1.00 12.85           H
ATOM     13  HA  LYS A   1      23.896  10.424   9.226  1.00 11.37           H
ATOM     14  HB2 LYS A   1      23.446  10.273  11.644  1.00 15.61           H
ATOM     15  HB3 LYS A   1      25.120  10.577  11.678  1.00 15.61           H
ATOM     16  HG2 LYS A   1      23.458  12.899  11.155  1.00 23.32           H
ATOM     17  HG3 LYS A   1      23.359  12.248  12.676  1.00 23.32           H
ATOM     18  HD2 LYS A   1      25.467  12.185  13.076  1.00 22.74           H
ATOM     19  HD3 LYS A   1      25.771  12.672  11.739  1.00 22.74           H
ATOM     20  HE2 LYS A   1      24.930  14.935  12.308  1.00 22.96           H
ATOM     21  HE3 LYS A   1      24.301  14.391  13.581  1.00 22.96           H
ATOM     22  HZ1 LYS A   1      26.495  15.003  14.094  1.00 18.89           H
ATOM     23  HZ2 LYS A   1      26.746  13.988  14.782  1.00 18.89           H
ATOM     24  HZ3 LYS A   1      26.921  14.505  13.409  1.00 18.89           H
ATOM     25  N   LEU A   2      21.579  11.060   9.393  1.00  8.33           N
ATOM     26  CA  LEU A   2      20.248  11.745   9.282  1.00  8.69           C
ATOM     27  C   LEU A   2      18.890  10.960  10.171  1.00  9.37           C
ATOM     28  O   LEU A   2      19.375   9.893  10.201  1.00  8.16           O
ATOM     29  CB  LEU A   2      19.883  11.653   7.770  1.00  9.53           C
ATOM     30  CG  LEU A   2      18.256  12.063   6.970  1.00 16.06           C
ATOM     31  CD1 LEU A   2      16.974  11.323   7.614  1.00 15.86           C
ATOM     32  CD2 LEU A   2      17.924  13.417   6.986  1.00 17.67           C
ATOM     33  H   LEU A   2      21.634  10.608   9.573  1.00  8.33           H
ATOM     34  HA  LEU A   2      20.020  12.906   9.503  1.00  8.69           H
ATOM     35  HB2 LEU A   2      20.471  11.634   7.287  1.00  9.53           H
ATOM     36  HB3 LEU A   2      19.673  10.630   7.405  1.00  9.53           H
ATOM     37  HG  LEU A   2      18.653  11.880   6.102  1.00 16.06           H
ATOM     38 HD11 LEU A   2      16.220  11.500   7.015  1.00 15.86           H
ATOM     39 HD12 LEU A   2      17.421  10.252   7.270  1.00 15.86           H
ATOM     40 HD13 LEU A   2      17.258  10.829   8.756  1.00 15.86           H
ATOM     41 HD21 LEU A   2      17.249  13.435   6.767  1.00 17.67           H
ATOM     42 HD22 LEU A   2      18.369  13.650   8.452  1.00 17.67           H
ATOM     43 HD23 LEU A   2      18.869  13.794   6.863  1.00 17.67           H
ATOM     44  N   VAL A   3      18.233  11.625  11.016  1.00  6.29           N
ATOM     45  CA  VAL A   3      17.163  11.363  11.664  1.00  6.45           C
ATOM     46  C   VAL A   3      16.163  11.982  11.312  1.00 11.86           C
ATOM     47  O   VAL A   3      15.983  13.390  10.929  1.00 10.67           O
ATOM     48  CB  VAL A   3      17.314  11.796  12.872  1.00 10.59           C
ATOM     49  CG1 VAL A   3      16.168  11.156  13.949  1.00 10.71           C
ATOM     50  CG2 VAL A   3      18.339  11.818  13.988  1.00 10.64           C
ATOM     51  H   VAL A   3      18.088  12.819  10.739  1.00  6.29           H
ATOM     52  HA  VAL A   3      16.980  10.475  11.845  1.00  6.45           H
ATOM     53  HB  VAL A   3      16.949  12.757  13.166  1.00 10.59           H
ATOM     54 HG11 VAL A   3      16.358  11.624  14.876  1.00 10.71           H
ATOM     55 HG12 VAL A   3      15.064  11.636  13.936  1.00 10.71           H
ATOM     56 HG13 VAL A   3      16.272  10.352  13.852  1.00 10.71           H
ATOM     57 HG21 VAL A   3      18.622  11.960  14.423  1.00 10.64           H
ATOM     58 HG22 VAL A   3      18.812  10.757  13.816  1.00 10.64           H
ATOM     59 HG23 VAL A   3      19.611  12.030  13.396  1.00 10.64           H
ATOM     60  N   PHE A   4      15.056  11.308  10.596  1.00  8.41           N
ATOM     61  CA  PHE A   4      13.332  11.786  10.463  1.00  8.51           C
ATOM     62  C   PHE A   4      12.483  10.831  11.371  1.00 11.98           C
ATOM     63  O   PHE A   4      12.279   9.761  11.656  1.00 10.68           O
ATOM     64  CB  PHE A   4      13.177  11.218   9.150  1.00  9.14           C
ATOM     65  CG  PHE A   4      11.858  11.434   8.261  1.00  9.69           C
ATOM     66  CD1 PHE A   4      11.854  12.613   7.463  1.00 10.70           C
ATOM     67  CD2 PHE A   4      11.060  10.593   8.408  1.00 12.10           C
ATOM     68  CE1 PHE A   4      10.397  12.522   6.983  1.00 11.47           C
ATOM     69  CE2 PHE A   4       9.868  10.592   8.168  1.00 15.42           C
ATOM     70  CZ  PHE A   4       9.638  11.558   6.852  1.00 13.27           C
ATOM     71  H   PHE A   4      15.181  10.183  10.949  1.00  8.41           H
ATOM     72  HA  PHE A   4      13.434  12.335  10.196  1.00  8.51           H
ATOM     73  HB2 PHE A   4      14.220  11.778   8.218  1.00  9.14           H
ATOM     74  HB3 PHE A   4      13.918  10.170   8.812  1.00  9.14           H
ATOM     75  HD1 PHE A   4      12.818  13.227   7.284  1.00 10.70           H
ATOM     76  HD2 PHE A   4      11.400   9.907   9.192  1.00 12.10           H
ATOM     77  HE1 PHE A   4      10.626  13.512   6.265  1.00 11.47           H
ATOM     78  HE2 PHE A   4       9.151  10.098   7.990  1.00 15.42           H
ATOM     79  HZ  PHE A   4       8.543  11.851   6.512  1.00 13.27           H
ATOM     80  N   PHE A   5      11.209  12.184  11.349  1.00  8.96           N
ATOM     81  CA  PHE A   5      10.159  11.588  12.520  1.00  9.67           C
ATOM     82  C   PHE A   5       9.199  12.285  11.738  1.00 13.65           C
ATOM     83  O   PHE A   5       9.061  13.533  11.297  1.00 10.13           O
ATOM     84  CB  PHE A   5      10.727  11.839  13.992  1.00 11.19           C
ATOM     85  CG  PHE A   5       9.415  11.346  14.182  1.00 12.79           C
ATOM     86  CD1 PHE A   5       8.725  10.383  14.930  1.00 16.12           C
ATOM     87  CD2 PHE A   5       8.119  12.178  14.604  1.00 15.48           C
ATOM     88  CE1 PHE A   5       7.698   9.739  15.624  1.00 17.28           C
ATOM     89  CE2 PHE A   5       7.298  12.352  15.420  1.00 18.56           C
ATOM     90  CZ  PHE A   5       6.646  10.839  15.911  1.00 15.89           C
ATOM     91  H   PHE A   5      11.466  12.703  11.317  1.00  8.96           H
ATOM     92  HA  PHE A   5      10.151  10.577  12.046  1.00  9.67           H
ATOM     93  HB2 PHE A   5      11.221  10.988  13.862  1.00 11.19           H
ATOM     94  HB3 PHE A   5      10.762  12.674  13.884  1.00 11.19           H
ATOM     95  HD1 PHE A   5       9.055   9.435  14.479  1.00 16.12           H
ATOM     96  HD2 PHE A   5       8.407  13.549  14.478  1.00 15.48           H
ATOM     97  HE1 PHE A   5       7.185   8.853  15.670  1.00 17.28           H
ATOM     98  HE2 PHE A   5       6.506  12.777  15.630  1.00 18.56           H
ATOM     99  HZ  PHE A   5       5.786  10.968  15.995  1.00 15.89           H
ATOM    100  N   ALA A   6       8.165  11.171  11.384  1.00 12.21           N
ATOM    101  CA  ALA A   6       7.148  11.796  10.893  1.00 15.49           C
ATOM    102  C   ALA A   6       5.758  10.830  11.601  1.00 31.54           C
ATOM    103  O   ALA A   6       5.665   9.760  11.371  1.00 33.30           O
ATOM    104  CB  ALA A   6       6.829  11.770   9.354  1.00 16.16           C
ATOM    105  OXT ALA A   6       5.151  11.567  11.990  1.00 51.39           O
ATOM    106  H   ALA A   6       8.332  10.343  11.155  1.00 12.21           H
ATOM    107  HA  ALA A   6       6.444  12.488  10.773  1.00 15.49           H
ATOM    108  HB1 ALA A   6       5.610  11.667   8.769  1.00 16.16           H
ATOM    109  HB2 ALA A   6       7.438  11.815   9.050  1.00 16.16           H
ATOM    110  HB3 ALA A   6       6.636  10.241   8.722  1.00 16.16           H
TER
HETATM  111  O   HOH A   7      27.404  12.468  15.613  0.01300.18           O
HETATM  112  O   HOH A   8      27.636  10.486  10.404  0.01300.07           O
HETATM  113  O   HOH A   9      27.113   9.044  12.981  1.00 10.60           O
HETATM  114  O   HOH A  10      26.937  13.298  16.691  0.01300.60           O
HETATM  115  O   HOH A  11      27.584   8.293   9.207  0.01300.60           O
TER
ATOM    116  N   LYS B   1       6.059   6.776  11.247  1.00 12.06           N
ATOM    117  CA  LYS B   1       7.533   6.705  10.619  1.00 12.29           C
ATOM    118  C   LYS B   1       8.741   7.135  11.243  1.00 12.37           C
ATOM    119  O   LYS B   1       8.965   8.639  11.554  1.00  7.97           O
ATOM    120  CB  LYS B   1       7.481   7.391   9.518  1.00 15.40           C
ATOM    121  CG  LYS B   1       8.291   6.174   8.083  1.00 27.98           C
ATOM    122  CD  LYS B   1       8.171   6.078   6.766  1.00 36.65           C
ATOM    123  CE  LYS B   1       8.213   7.819   5.975  1.00 51.92           C
ATOM    124  NZ  LYS B   1       6.868   8.663   5.875  1.00 65.39           N
ATOM    125  H1  LYS B   1       5.390   6.886  11.234  1.00 12.06           H
ATOM    126  H2  LYS B   1       6.266   6.835  12.592  1.00 12.06           H
ATOM    127  H3  LYS B   1       6.158   7.820  11.501  1.00 12.06           H
ATOM    128  HA  LYS B   1       7.759   5.718  10.646  1.00 12.29           H
ATOM    129  HB2 LYS B   1       6.495   6.647   8.850  1.00 15.40           H
ATOM    130  HB3 LYS B   1       7.643   8.230   9.146  1.00 15.40           H
ATOM    131  HG2 LYS B   1       8.994   6.608   8.523  1.00 27.98           H
ATOM    132  HG3 LYS B   1       8.649   5.336   8.563  1.00 27.98           H
ATOM    133  HD2 LYS B   1       8.673   5.699   6.399  1.00 36.65           H
ATOM    134  HD3 LYS B   1       6.760   6.170   6.665  1.00 36.65           H
ATOM    135  HE2 LYS B   1       8.919   8.325   6.843  1.00 51.92           H
ATOM    136  HE3 LYS B   1       8.542   7.816   4.997  1.00 51.92           H
ATOM    137  HZ1 LYS B   1       7.138   9.144   5.353  1.00 65.39           H
ATOM    138  HZ2 LYS B   1       5.956   7.821   5.874  1.00 65.39           H
ATOM    139  HZ3 LYS B   1       6.346   8.555   6.892  1.00 65.39           H
ATOM    140  N   LEU B   2       9.607   6.359  11.382  1.00  8.53           N
ATOM    141  CA  LEU B   2      11.441   6.881  12.266  1.00  8.19           C
ATOM    142  C   LEU B   2      12.097   6.033  11.267  1.00  9.32           C
ATOM    143  O   LEU B   2      12.191   4.917  11.218  1.00  8.36           O
ATOM    144  CB  LEU B   2      11.377   6.337  13.764  1.00  9.24           C
ATOM    145  CG  LEU B   2      12.123   6.773  14.967  1.00 14.56           C
ATOM    146  CD1 LEU B   2      13.427   6.207  14.378  1.00 16.64           C
ATOM    147  CD2 LEU B   2      12.408   8.641  14.281  1.00 13.86           C
ATOM    148  H   LEU B   2       9.546   5.295  11.688  1.00  8.53           H
ATOM    149  HA  LEU B   2      10.951   7.704  11.993  1.00  8.19           H
ATOM    150  HB2 LEU B   2      10.252   6.709  13.789  1.00  9.24           H
ATOM    151  HB3 LEU B   2      11.569   5.296  13.481  1.00  9.24           H
ATOM    152  HG  LEU B   2      12.035   6.836  15.751  1.00 14.56           H
ATOM    153 HD11 LEU B   2      13.985   6.349  15.013  1.00 16.64           H
ATOM    154 HD12 LEU B   2      13.551   5.200  14.730  1.00 16.64           H
ATOM    155 HD13 LEU B   2      13.666   6.024  13.646  1.00 16.64           H
ATOM    156 HD21 LEU B   2      13.245   8.680  15.342  1.00 13.86           H
ATOM    157 HD22 LEU B   2      13.041   8.691  13.538  1.00 13.86           H
ATOM    158 HD23 LEU B   2      11.817   8.618  15.010  1.00 13.86           H
ATOM    159  N   VAL B   3      12.861   7.358  10.763  1.00  5.98           N
ATOM    160  CA  VAL B   3      14.443   6.761  10.044  1.00  4.31           C
ATOM    161  C   VAL B   3      15.528   7.414  10.735  1.00  8.02           C
ATOM    162  O   VAL B   3      15.598   8.255  10.374  1.00  7.46           O
ATOM    163  CB  VAL B   3      14.304   7.042   8.341  1.00  6.99           C
ATOM    164  CG1 VAL B   3      15.152   6.121   7.388  1.00  6.50           C
ATOM    165  CG2 VAL B   3      12.668   6.647   7.899  1.00  7.17           C
ATOM    166  H   VAL B   3      13.032   7.820  11.043  1.00  5.98           H
ATOM    167  HA  VAL B   3      14.000   5.859  10.003  1.00  4.31           H
ATOM    168  HB  VAL B   3      14.302   7.714   8.520  1.00  6.99           H
ATOM    169 HG11 VAL B   3      14.813   6.063   6.431  1.00  6.50           H
ATOM    170 HG12 VAL B   3      16.336   6.603   8.208  1.00  6.50           H
ATOM    171 HG13 VAL B   3      15.237   4.871   7.945  1.00  6.50           H
ATOM    172 HG21 VAL B   3      12.376   6.776   7.256  1.00  7.17           H
ATOM    173 HG22 VAL B   3      12.834   5.220   8.029  1.00  7.17           H
ATOM    174 HG23 VAL B   3      12.357   7.019   8.285  1.00  7.17           H
ATOM    175  N   PHE B   4      16.488   6.744  10.467  1.00  4.71           N
ATOM    176  CA  PHE B   4      18.102   7.189  11.359  1.00  5.68           C
ATOM    177  C   PHE B   4      18.837   6.382  10.238  1.00  9.59           C
ATOM    178  O   PHE B   4      19.122   4.977  10.223  1.00  9.76           O
ATOM    179  CB  PHE B   4      18.250   6.974  12.471  1.00  7.35           C
ATOM    180  CG  PHE B   4      19.482   6.992  13.096  1.00 10.11           C
ATOM    181  CD1 PHE B   4      20.437   7.907  13.191  1.00 14.40           C
ATOM    182  CD2 PHE B   4      20.112   5.308  13.652  1.00 13.08           C
ATOM    183  CE1 PHE B   4      21.401   7.886  13.300  1.00 16.51           C
ATOM    184  CE2 PHE B   4      21.372   5.460  13.691  1.00 17.07           C
ATOM    185  CZ  PHE B   4      22.219   6.734  13.808  1.00 16.28           C
ATOM    186  H   PHE B   4      16.307   5.930  10.786  1.00  4.71           H
ATOM    187  HA  PHE B   4      18.110   7.867  10.882  1.00  5.68           H
ATOM    188  HB2 PHE B   4      17.851   7.702  12.820  1.00  7.35           H
ATOM    189  HB3 PHE B   4      17.635   5.707  13.063  1.00  7.35           H
ATOM    190  HD1 PHE B   4      20.103   8.652  12.988  1.00 14.40           H
ATOM    191  HD2 PHE B   4      19.690   4.609  13.557  1.00 13.08           H
ATOM    192  HE1 PHE B   4      22.139   8.766  13.599  1.00 16.51           H
ATOM    193  HE2 PHE B   4      21.869   4.896  13.919  1.00 17.07           H
ATOM    194  HZ  PHE B   4      22.916   6.777  13.868  1.00 16.28           H
ATOM    195  N   PHE B   5      19.937   7.142  10.030  1.00  7.84           N
ATOM    196  CA  PHE B   5      21.120   6.440   9.313  1.00  6.46           C
ATOM    197  C   PHE B   5      22.606   7.336   8.977  1.00 10.69           C
ATOM    198  O   PHE B   5      22.311   8.533   9.648  1.00  8.77           O
ATOM    199  CB  PHE B   5      20.722   5.741   7.454  1.00  8.36           C
ATOM    200  CG  PHE B   5      20.179   7.257   7.000  1.00  8.12           C
ATOM    201  CD1 PHE B   5      21.581   7.939   6.107  1.00 10.77           C
ATOM    202  CD2 PHE B   5      19.316   6.959   6.645  1.00  8.53           C
ATOM    203  CE1 PHE B   5      20.553   8.780   5.521  1.00 11.77           C
ATOM    204  CE2 PHE B   5      18.511   8.062   5.376  1.00 11.24           C
ATOM    205  CZ  PHE B   5      19.625   8.985   4.920  1.00 10.01           C
ATOM    206  H   PHE B   5      19.861   8.000   9.965  1.00  7.84           H
ATOM    207  HA  PHE B   5      21.065   5.542   9.466  1.00  6.46           H
ATOM    208  HB2 PHE B   5      21.684   5.478   7.488  1.00  8.36           H
ATOM    209  HB3 PHE B   5      20.424   5.075   7.508  1.00  8.36           H
ATOM    210  HD1 PHE B   5      22.050   8.025   6.522  1.00 10.77           H
ATOM    211  HD2 PHE B   5      18.656   6.709   6.700  1.00  8.53           H
ATOM    212  HE1 PHE B   5      21.371   9.770   4.850  1.00 11.77           H
ATOM    213  HE2 PHE B   5      17.801   8.139   5.574  1.00 11.24           H
ATOM    214  HZ  PHE B   5      19.340   9.782   4.280  1.00 10.01           H
ATOM    215  N   ALA B   6      23.702   6.381   9.170  1.00 10.09           N
ATOM    216  CA  ALA B   6      25.140   7.488   9.124  1.00 12.61           C
ATOM    217  C   ALA B   6      26.059   5.978   8.632  1.00 36.73           C
ATOM    218  O   ALA B   6      25.847   5.199   8.713  1.00 40.97           O
ATOM    219  CB  ALA B   6      25.425   7.392  10.811  1.00 13.92           C
ATOM    220  OXT ALA B   6      26.909   6.835   8.092  1.00 60.32           O
ATOM    221  H   ALA B   6      23.723   5.658   9.026  1.00 10.09           H
ATOM    222  HA  ALA B   6      25.039   8.306   8.536  1.00 12.61           H
ATOM    223  HB1 ALA B   6      26.015   8.125  10.591  1.00 13.92           H
ATOM    224  HB2 ALA B   6      24.698   8.343  11.122  1.00 13.92           H
ATOM    225  HB3 ALA B   6      25.172   6.765  11.051  1.00 13.92           H
TER
HETATM  226  O   HOH B   7      27.639   9.170   8.109  1.00 10.70           O
TER
END
"""

def get_diffs(pdb_file_name):
  pdb_inp = iotbx.pdb.input(file_name = pdb_file_name)
  model = mmtbx.model.manager(
    model_input = pdb_inp,
    log         = null_out())
  model.process(make_restraints=True)
  diffs = flex.double()
  for it in model.xh_connectivity_table():
    diffs.append(abs(it[3]-it[4]))
  return diffs.min_max_mean()

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  This test runs 2 refinements with riding H: one standard, another with
  ordered_solvent=True. The inputs are such that water in between chains
  get removed by ordered solvent procedure. In this case (at the time of
  test implementation), the riding_h_manager in model object becomes None
  and no riding hydrogens machinery works.
  This is shown by comparing final .geo files from two refinement. In particular,
  bonds formed with HB3 atoms (10 occurances).
  By definition deltas of bond distances x-H = 0 when using riding H.
  In the first case this is true, in the second case it is not.
  When the bug is fixed, test should work as it is, without modifications.
  The root of the bug is that ordered solvent is being executed after
  riding_h_manager is reinstantiated in the beginning of macro cycle.
  Ways to solve:
  - implement and use proper select() function of riding_h_manager when doing
    select() of the mmtbx.model
  - reinstantiate riding_h_manager in select() of mmtbx.model
  - rearrange macro-cycle
  """
  # Input model
  pdb_in = "%s.pdb"%prefix
  open(pdb_in, "w").write(pdb_str)
  # Compute Fobs
  args = [
    pdb_in,
    "type=real",
    "label=F-obs",
    "r_free=0.1",
    "high_res=2.7",
    "random_seed=2217384",
    "> %s_fmodel.log"%prefix]
  r0 = run_fmodel(args = args, prefix = prefix)
  # Run refinement
  for solvent_option in [True, False]:
    args = [
      pdb_in,
      r0.mtz,
      "main.ordered_solvent=%s"%str(solvent_option),
      "main.number_of_mac=1",
      "wc=0.001"]
    r = run_phenix_refine(args = args, prefix=prefix+"_%s"%str(solvent_option))
  #
  diffs1 = get_diffs("%s_False_001.pdb"%prefix)
  diffs2 = get_diffs("%s_True_001.pdb"%prefix)
  print(diffs1.as_tuple())
  print(diffs2.as_tuple())
  assert diffs1.max <0.01 , diffs1.max
  assert diffs1.mean<0.001, diffs1.mean
  assert diffs2.max <0.01 , diffs2.max
  assert diffs2.mean<0.001, diffs2.mean

if (__name__ == "__main__"):
  run()
  print("OK")
