from __future__ import division
from __future__ import print_function
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel
import libtbx.load_env
import os
from mmtbx import utils
import iotbx.pdb

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Occupancy of H in refinement.
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/ile_2conf_h.pdb",
    test=os.path.isfile)
  args = [pdb,"high_res=2 r_fre=0.1 type=real label=f-obs"]
  r = run_fmodel(args = args, prefix = prefix)
  args = [pdb, r.mtz]
  r = run_phenix_refine(args = args, prefix=prefix)
  #
  sel_a = utils.get_atom_selection(pdb_file_name = r.pdb,
    selection_string = "altloc A")
  sel_b = utils.get_atom_selection(pdb_file_name = r.pdb,
    selection_string = "altloc B")
  sel_1 = utils.get_atom_selection(pdb_file_name = r.pdb,
    selection_string = "not (altloc A or altloc B) and not element H")
  sel_2 = utils.get_atom_selection(pdb_file_name = r.pdb,
    selection_string = "name H and element H")
  assert sel_a.count(True) == 15
  assert sel_a.count(True) == sel_b.count(True)
  occupancies = iotbx.pdb.input(file_name = r.pdb
    ).xray_structure_simple().scatterers().extract_occupancies()
  occ_a = occupancies.select(sel_a)
  occ_b = occupancies.select(sel_b)
  assert occ_a.all_eq(occ_a[0])
  assert occ_b.all_eq(occ_b[0])
  assert (occ_a+occ_b).all_eq(1.0)
  assert sel_1.count(True) == 3
  occ_1 = occupancies.select(sel_1)
  assert occ_1.all_eq(1.0)
  occ_2 = occupancies.select(sel_2)
  assert occ_2.all_eq(1.0)

if (__name__ == "__main__"):
  run()
  print("OK")
