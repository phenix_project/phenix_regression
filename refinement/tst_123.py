from __future__ import division
from __future__ import print_function
from libtbx import easy_run
import libtbx.load_env
import libtbx.path
import time, os
from phenix_regression.refinement import get_r_factors
from mmtbx.validation.rotalyze import rotalyze
from iotbx import pdb
from libtbx.test_utils import assert_lines_in_file
from phenix_regression.refinement import run_phenix_refine

input_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/ncs/3ow9FH_start.pdb",
    test=os.path.isfile)
hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/ncs/3ow9FH.mtz",
    test=os.path.isfile)

ncs_string = """\
refinement.pdb_interpretation.ncs_group {
  reference        = (chain A and resid 1:6)
  selection        = (chain B and resid 1:6)
}
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Torsion NCS
  """
  #prescreen rotamers
  model_hash = {}
  model_score = {}
  pdb_io = pdb.input(file_name= input_file)
  pdb_hierarchy = pdb_io.construct_hierarchy()
  pdb_hierarchy.reset_i_seq_if_necessary()
  r = rotalyze(pdb_hierarchy=pdb_hierarchy)
  for rot in r.results:
    model_hash[rot.id_str()] = rot.rotamer_name
    model_score[rot.id_str()] = rot.score
  assert model_hash[' A   3  VAL'] == 'OUTLIER'
  assert model_hash[' A   2  LEU'] == 'tm'

  ncs = open("ncs.eff", "w")
  ncs.write(ncs_string)
  ncs.close()

  args = [
    input_file,
    hkl,
    "main.number_of_macro=1",
    "main.bulk_solv=true",
    "ncs_search.enabled=true",
    "ncs.type=torsion",
    "strategy=individual_sites",
    "min_number_of_test_set_reflections_for_max_likelihood_target=10",
    "write_geo_file=True",
    "write_def_file=True",
    "ncs.eff",
    "torsion.fix_outliers=true",
    "c_beta_restraints=False",
    "write_final_geo=True"]
  r0 = run_phenix_refine(args = args, prefix = prefix)
  ofn=r0.pdb
  assert r0.r_work_start>0.16, r0.r_work_start
  assert r0.r_work_final<0.04, r0.r_work_final
  assert r0.r_free_start>0.18, r0.r_free_start
  assert r0.r_free_final<0.05, r0.r_free_final

  model_hash = {}
  model_score = {}
  pdb_io = pdb.input(file_name= ofn)
  pdb_hierarchy = pdb_io.construct_hierarchy()
  pdb_hierarchy.reset_i_seq_if_necessary()
  r = rotalyze(pdb_hierarchy=pdb_hierarchy)
  for rot in r.results:
    model_hash[rot.id_str()] = rot.rotamer_name
    model_score[rot.id_str()] = rot.score
  assert model_hash[' A   2  LEU'] == model_hash[' B   2  LEU']
  assert model_hash[' A   3  VAL'] == model_hash[' B   3  VAL']
  assert model_hash[' A   5  PHE'] == 't80'
  assert model_hash[' B   5  PHE'] == 'p90'

  log     = open(r0.log, 'r').readlines()
  fin_pdb = open(r0.pdb, 'r').readlines()
  beg_geo = open(r0.geo, 'r').readlines()
  fin_geo = open(r0.geo_fin, 'r').readlines()
  for l in fin_geo:
    if l.startswith("NCS torsion angle"):
      n = int(l.split()[-1])
      assert n == 42, "Expected 42, got %d" % n # We don't want proxies with H atoms
  for l in beg_geo:
    if l.startswith("NCS torsion angle"):
      n = int(l.split()[-1])
      assert n == 39, "Expected 39, got %d" % n # We don't want proxies with H atoms
  for l in fin_pdb:
    if l.startswith("REMARK   3   NUMBER OF NCS GROUPS"):
      n = int(l.split()[-1])
      assert n == 1, "Expected 1 NCS group, got %d" % n
  assert_lines_in_file(file_name="%s_002.def" % prefix, lines="""
      ncs_group {
        reference = "chain A"
        selection = "chain B"
      }    """)
  assert_lines_in_file(file_name="%s_001.log" % prefix, lines="""
      Number of NCS groups: 1
      refinement.pdb_interpretation.ncs_group {
        reference = chain 'A'
        selection = chain 'B'
      }    """)

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK. Time: %8.3f"%(time.time()-t0))
