from __future__ import division
from __future__ import print_function
import os.path
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel
from libtbx import easy_run

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Test for the case when .geo file should be outputted (requested by
  parameters), but model contains atoms sitting on top of each other
  (the las two waters) which triggers clash_guard exception (Sorry)
  and phenix.refine stops without outputting geo.
  (this is fixed and test ensures this).
  Same for log in command-line.
  """
  pdb_raw = """\
CRYST1   21.937    4.866   23.477  90.00 107.08  90.00 P 1 21 1      2
ATOM     47  N   TYR A   7       8.292   1.817   6.147  1.00 14.70           N
ATOM     48  CA  TYR A   7       9.159   2.144   7.299  1.00 15.18           C
ATOM     49  C   TYR A   7      10.603   2.331   6.885  1.00 15.91           C
ATOM     50  O   TYR A   7      11.041   1.811   5.855  1.00 15.76           O
ATOM     51  CB  TYR A   7       9.061   1.065   8.369  1.00 15.35           C
ATOM     52  CG  TYR A   7       7.665   0.929   8.902  1.00 14.45           C
ATOM     53  CD1 TYR A   7       6.771   0.021   8.327  1.00 15.68           C
ATOM     54  CD2 TYR A   7       7.210   1.756   9.920  1.00 14.80           C
ATOM     55  CE1 TYR A   7       5.480  -0.094   8.796  1.00 13.46           C
ATOM     56  CE2 TYR A   7       5.904   1.649  10.416  1.00 14.33           C
ATOM     57  CZ  TYR A   7       5.047   0.729   9.831  1.00 15.09           C
ATOM     58  OH  TYR A   7       3.766   0.589  10.291  1.00 14.39           O
ATOM     59  OXT TYR A   7      11.358   2.999   7.612  1.00 17.49           O
TER      60      TYR A   7
HETATM   63  O   HOH A  10     -11.286   1.756  -1.468  1.00 17.08           O
HETATM   64  O   HOH A  11      11.808   4.179   9.970  1.00 23.99           O
HETATM   65  O   HOH A  12      13.605   1.327   9.198  1.00 26.17           O
HETATM   67  O   HOH A  14      -1.500   0.682  10.967  1.00 43.49           O
HETATM   69  O   HOH A  16      -1.500   0.682  10.967  1.00 43.49           O
TER
END
"""
  pdb_file = "%s.pdb" % prefix
  open(pdb_file, "w").write(pdb_raw)
  args = [
    pdb_file,
    "high_resolution=3.0",
    "type=real",
    "label=F",
    "r_free_flags_fraction=0.1"]
  r1 = run_fmodel(args = args, prefix = prefix)
  # No file required by parameter
  args = [pdb_file, r1.mtz, "output.write_geo_file=False"]
  r = run_phenix_refine(args = args, prefix = prefix+"_1", sorry_expected=True)
  assert not os.path.isfile("%s_1_001.geo" % prefix)
  # File required by parameter
  args = [pdb_file, r1.mtz, "output.write_geo_file=True"]
  r = run_phenix_refine(args = args, prefix = prefix+"_2", sorry_expected=True)
  assert os.path.isfile("%s_2_001.geo" % prefix)

if (__name__ == "__main__") :
  run()
  print("OK")
