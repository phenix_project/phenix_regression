from libtbx import easy_run
import libtbx.load_env
import os, time
from iotbx import reflection_file_reader

def exercise_00():
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/map_to_asu/model.pdb",
    test=os.path.isfile)
  hkl_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/map_to_asu/data.mtz",
    test=os.path.isfile)
  #
  miller_arrays = reflection_file_reader.any_reflection_file(file_name =
    hkl_file).as_miller_arrays()
  cntr = 0
  for ma in miller_arrays:
    if(ma.info().labels == ['I-obs', 'SIGI-obs'] or 
       ma.info().labels == ['R-free-flags']):
      assert not ma.is_in_asu()
      cntr += 1
  assert cntr == 2
  #
  cmd = " ".join([
    "phenix.refine %s %s"%(pdb_file, hkl_file),
    "strategy=none",
    "output.prefix=tst_refinement_map_to_asu",
    "main.bulk_sol=True main.number_of_mac=2",
    "--quiet",
    "--overwrite"
  ])
  assert (easy_run.call(cmd) == 0)
  #
  miller_arrays = reflection_file_reader.any_reflection_file(file_name =
    "tst_refinement_map_to_asu_001.mtz").as_miller_arrays()
  cntr = 0
  for ma in miller_arrays:
    assert ma.is_in_asu()
    cntr += 1
  assert cntr == 7

if (__name__ == "__main__"):
  exercise_00()
  
