from __future__ import division
from __future__ import print_function
from phenix_regression.refinement import run_phenix_refine
import libtbx.load_env
import os

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Exercise iso/aniso def selections.
  """
  pdb = libtbx.env.find_in_repositories(
        relative_path="phenix_regression/pdb/phe_adp_refinement.pdb", test=os.path.isfile)
  pdb_i = libtbx.env.find_in_repositories(
        relative_path="phenix_regression/pdb/phe_i.pdb", test=os.path.isfile)
  pdb_a = libtbx.env.find_in_repositories(
        relative_path="phenix_regression/pdb/phe_a.pdb", test=os.path.isfile)
  pdb_d = libtbx.env.find_in_repositories(
        relative_path="phenix_regression/pdb/phe_d.pdb", test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
        relative_path="phenix_regression/reflection_files/phe_1.6.mtz", \
        test=os.path.isfile)
  #
  args_a = [hkl,pdb,"main.number_of_macro_cycles=0",
           "adp.individual.anisotropic=all"]
  args_i = [hkl,pdb,"main.number_of_macro_cycles=0",
            "adp.individual.isotropic=all"]
  args_d = [hkl,pdb,"main.number_of_macro_cycles=0"]
  #
  for p in [[args_a,pdb_a], [args_i,pdb_i], [args_d,pdb_d]]:
    r = run_phenix_refine(args = p[0], prefix = prefix)
    for line1, line2 in zip(open(r.pdb,"r").read().splitlines(),
                            open(p[1],"r").read().splitlines()):
      if(line1.startswith("ATOM") or line1.startswith("ANISOU")):
         assert line1 == line2

if (__name__ == "__main__"):
  run()
  print("OK")
