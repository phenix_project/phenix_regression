from __future__ import division
from phenix_regression.refinement import run_phenix_refine
import libtbx.load_env
import os
import iotbx.pdb
from libtbx.test_utils import approx_equal
from scitbx.array_family import flex

params_str = """\
refinement.main.scattering_table=wk1995
refinement.refine.strategy=rigid_body
refinement.main.fake_f_obs=true
refinement.fake_f_obs.fmodel.k_sol=0.37
refinement.fake_f_obs.fmodel.b_sol=49.0
refinement.fake_f_obs.fmodel.b_cart= 5 7 -12 0 0 0
data_manager.fmodel.xray_data.high_resolution=1.5
refinement.modify_start_model.modify.sites.translate=1.0 1.5 2.0
refinement.modify_start_model.modify.sites.rotate=5 6 8
refinement.bulk_solvent_and_scale.mode=slow
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Rigid-body refinement, MZ protocol.
  """
  pdb = libtbx.env.find_in_repositories(
   relative_path="phenix_regression/pdb/lys_rigid_body.pdb",
   test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/l.mtz", \
    test=os.path.isfile)
  #
  par_in = "%s.params"%prefix
  open(par_in, "w").write(params_str)
  args = [pdb, hkl, par_in, "k_sol_b_sol_grid_search=false",
          "main.nqh_flips=False","rigid_body.target=ls_wunit_k1"]
  r = run_phenix_refine(args = args, prefix=prefix)
  #
  r.check_start_r(r_work=0.56, r_free=0.56, eps=0.005)
  r.check_final_r(r_work=0.009, r_free=0.009, eps=0.005, info_low_eps=0.01)
  assert approx_equal(r.bond_start, r.bond_final)
  assert approx_equal(r.angle_start, r.angle_final)
  a1 = iotbx.pdb.input(file_name=  pdb).construct_hierarchy().atoms()
  a2 = iotbx.pdb.input(file_name=r.pdb).construct_hierarchy().atoms()
  dist = flex.mean(flex.sqrt((a1.extract_xyz() - a2.extract_xyz()).dot()))
  assert dist < 0.006, 'dist %s is greater than 0.006' % dist
  assert approx_equal(flex.mean(flex.abs(a1.extract_b()-a2.extract_b())), 0)

if (__name__ == "__main__"):
  run()
