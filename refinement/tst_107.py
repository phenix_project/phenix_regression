from __future__ import print_function
import time
from libtbx.test_utils import approx_equal
from libtbx import easy_run
import iotbx.pdb
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel
import os

pdb_str_answer = """
CRYST1   11.936   10.320    9.363  90.00  90.00  90.00 P 1
HETATM    1  C1  GLC   205       6.490   4.741   3.542  0.90  3.05           C
HETATM    2  C2  GLC   205       7.822   3.953   3.427  1.00  2.85           C
HETATM    3  C3  GLC   205       8.546   3.870   4.789  1.00  2.56           C
HETATM    4  C4  GLC   205       8.724   5.263   5.432  1.00  2.87           C
HETATM    5  C5  GLC   205       7.365   6.016   5.506  1.00  3.04           C
HETATM    6  C6  GLC   205       7.498   7.476   5.972  1.00  2.70           C
HETATM    7  O1  GLC   205       5.520   3.972   4.305  1.00  3.70           O
HETATM    8  O2  GLC   205       7.582   2.633   2.942  1.00  3.54           O
HETATM    9  O3  GLC   205       9.821   3.274   4.590  1.00  3.58           O
HETATM   10  O4  GLC   205       9.269   5.074   6.733  1.00  2.86           O
HETATM   11  O5  GLC   205       6.722   6.027   4.192  1.00  3.13           O
HETATM   12  O6  GLC   205       8.125   8.267   4.965  1.00  3.49           O
HETATM   13  HO6 GLC   205       8.335   7.704   4.191  0.56  3.49           H
HETATM   14  HO4 GLC   205       9.396   4.116   6.898  0.30  2.86           H
HETATM   15  HO3 GLC   205       9.936   3.059   3.641  0.80  3.58           H
HETATM   16  HO2 GLC   205       6.622   2.512   2.786  1.00  3.54           H
HETATM   17  H62 GLC   205       6.509   7.880   6.188  0.20  2.70           H
HETATM   18  H61 GLC   205       8.083   7.512   6.891  0.60  2.70           H
HETATM   19  H5  GLC   205       6.719   5.489   6.208  0.40  3.04           H
HETATM   20  H4  GLC   205       9.425   5.834   4.824  0.70  2.87           H
HETATM   21  H3  GLC   205       7.952   3.244   5.455  0.80  2.56           H
HETATM   22  H2  GLC   205       8.475   4.482   2.733  0.10  2.85           H
HETATM   23  H1  GLC   205       6.109   4.918   2.536  0.50  3.05           H
HETATM   24  C1  FRU   206       3.687   2.938   3.096  1.00  4.83           C
HETATM   25  C2  FRU   206       4.096   4.112   4.011  1.00  4.63           C
HETATM   26  C3  FRU   206       3.319   4.150   5.348  1.00  4.43           C
HETATM   27  C4  FRU   206       3.271   5.643   5.649  1.00  4.60           C
HETATM   28  C5  FRU   206       3.074   6.242   4.252  1.00  4.36           C
HETATM   29  C6  FRU   206       3.606   7.679   4.108  1.00  4.59           C
HETATM   30  O1  FRU   206       2.340   3.061   2.629  1.00  5.25           O
HETATM   31  O3  FRU   206       3.962   3.445   6.410  1.00  4.53           O
HETATM   32  O4  FRU   206       2.188   5.942   6.522  1.00  5.61           O
HETATM   33  O5  FRU   206       3.769   5.360   3.348  1.00  4.20           O
HETATM   34  O6  FRU   206       3.345   8.192   2.802  1.00  4.83           O
HETATM   35  H11 FRU   206       4.362   2.897   2.238  0.20  4.83           H
HETATM   36  H12 FRU   206       3.793   2.000   3.645  0.40  4.83           H
HETATM   37  H3  FRU   206       2.298   3.774   5.192  0.30  4.43           H
HETATM   38  H4  FRU   206       4.230   5.974   6.071  0.90  4.60           H
HETATM   39  H5  FRU   206       2.000   6.241   4.019  0.70  4.36           H
HETATM   40  H61 FRU   206       4.682   7.690   4.295  0.50  4.59           H
HETATM   41  H62 FRU   206       3.129   8.320   4.853  1.00  4.59           H
HETATM   42  HO1 FRU   206       2.151   2.351   2.000  0.50  5.25           H
HETATM   43  HO3 FRU   206       3.914   2.494   6.241  0.60  4.53           H
HETATM   44  HO4 FRU   206       2.315   5.482   7.363  0.90  5.61           H
TER
END
"""

pdb_str_poor = """
CRYST1   11.936   10.320    9.363  90.00  90.00  90.00 P 1
HETATM    1  C1  GLC   205       6.490   4.741   3.542  0.90  3.05           C
HETATM    2  C2  GLC   205       7.822   3.953   3.427  1.00  2.85           C
HETATM    3  C3  GLC   205       8.546   3.870   4.789  1.00  2.56           C
HETATM    4  C4  GLC   205       8.724   5.263   5.432  1.00  2.87           C
HETATM    5  C5  GLC   205       7.365   6.016   5.506  1.00  3.04           C
HETATM    6  C6  GLC   205       7.498   7.476   5.972  1.00  2.70           C
HETATM    7  O1  GLC   205       5.520   3.972   4.305  1.00  3.70           O
HETATM    8  O2  GLC   205       7.582   2.633   2.942  1.00  3.54           O
HETATM    9  O3  GLC   205       9.821   3.274   4.590  1.00  3.58           O
HETATM   10  O4  GLC   205       9.269   5.074   6.733  1.00  2.86           O
HETATM   11  O5  GLC   205       6.722   6.027   4.192  1.00  3.13           O
HETATM   12  O6  GLC   205       8.125   8.267   4.965  1.00  3.49           O
HETATM   13  HO6 GLC   205       8.335   7.704   4.191  1.00  3.49           H
HETATM   14  HO4 GLC   205       9.396   4.116   6.898  1.00  2.86           H
HETATM   15  HO3 GLC   205       9.936   3.059   3.641  1.00  3.58           H
HETATM   16  HO2 GLC   205       6.622   2.512   2.786  1.00  3.54           H
HETATM   17  H62 GLC   205       6.509   7.880   6.188  1.00  2.70           H
HETATM   18  H61 GLC   205       8.083   7.512   6.891  1.00  2.70           H
HETATM   19  H5  GLC   205       6.719   5.489   6.208  1.00  3.04           H
HETATM   20  H4  GLC   205       9.425   5.834   4.824  1.00  2.87           H
HETATM   21  H3  GLC   205       7.952   3.244   5.455  1.00  2.56           H
HETATM   22  H2  GLC   205       8.475   4.482   2.733  1.00  2.85           H
HETATM   23  H1  GLC   205       6.109   4.918   2.536  1.00  3.05           H
HETATM   24  C1  FRU   206       3.687   2.938   3.096  1.00  4.83           C
HETATM   25  C2  FRU   206       4.096   4.112   4.011  1.00  4.63           C
HETATM   26  C3  FRU   206       3.319   4.150   5.348  1.00  4.43           C
HETATM   27  C4  FRU   206       3.271   5.643   5.649  1.00  4.60           C
HETATM   28  C5  FRU   206       3.074   6.242   4.252  1.00  4.36           C
HETATM   29  C6  FRU   206       3.606   7.679   4.108  1.00  4.59           C
HETATM   30  O1  FRU   206       2.340   3.061   2.629  1.00  5.25           O
HETATM   31  O3  FRU   206       3.962   3.445   6.410  1.00  4.53           O
HETATM   32  O4  FRU   206       2.188   5.942   6.522  1.00  5.61           O
HETATM   33  O5  FRU   206       3.769   5.360   3.348  1.00  4.20           O
HETATM   34  O6  FRU   206       3.345   8.192   2.802  1.00  4.83           O
HETATM   35  H11 FRU   206       4.362   2.897   2.238  1.00  4.83           H
HETATM   36  H12 FRU   206       3.793   2.000   3.645  1.00  4.83           H
HETATM   37  H3  FRU   206       2.298   3.774   5.192  1.00  4.43           H
HETATM   38  H4  FRU   206       4.230   5.974   6.071  1.00  4.60           H
HETATM   39  H5  FRU   206       2.000   6.241   4.019  1.00  4.36           H
HETATM   40  H61 FRU   206       4.682   7.690   4.295  1.00  4.59           H
HETATM   41  H62 FRU   206       3.129   8.320   4.853  1.00  4.59           H
HETATM   42  HO1 FRU   206       2.151   2.351   2.000  1.00  5.25           H
HETATM   43  HO3 FRU   206       3.914   2.494   6.241  1.00  4.53           H
HETATM   44  HO4 FRU   206       2.315   5.482   7.363  1.00  5.61           H
TER
END
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Occupancies.
  Make sure individual occupancies of H atoms are refined is requested.
  """
  pdb_file_answer = "%s_answer.pdb"%prefix
  of = open(pdb_file_answer, "w")
  print(pdb_str_answer, file=of)
  of.close()
  xrs_answer = iotbx.pdb.input(source_info=None,
    lines=pdb_str_answer).construct_hierarchy().extract_xray_structure()
  #
  pdb_file_poor = "%s_poor.pdb"%prefix
  of = open(pdb_file_poor, "w")
  print(pdb_str_poor, file=of)
  of.close()
  #
  args = [
    "%s"%pdb_file_answer,
    "high_res=1.0",
    "type=real",
    "r_free=0.1",
    "label=f-obs"]
  r0 = run_fmodel(args = args, prefix = prefix)
  #
  args = [
    pdb_file_poor,
    r0.mtz,
    "main.number_of_mac=5",
    "strategy=occupancies",
    "main.bulk_sol=false","main.target=ls",
    "ls_target_names.target_name=ls_wunit_kunit",
    "refine.occupancies.individual='%s'"%"element H",
    "hydrogens.refine=individual"]
  r = run_phenix_refine(args = args, prefix = prefix)
  #
  assert r.r_work_start > 0.03 , r.r_work_start
  assert r.r_work_final < 0.001, r.r_work_final
  #
  xrs_refined = iotbx.pdb.input(file_name=r.pdb).\
      construct_hierarchy().extract_xray_structure()
  o_answer  =  xrs_answer.scatterers().extract_occupancies()
  o_refined = xrs_refined.scatterers().extract_occupancies()
  assert approx_equal(o_answer, o_refined)

if(__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK")
  print("Time: %8.3f"%(time.time()-t0))
