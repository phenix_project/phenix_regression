from __future__ import division
from __future__ import print_function
from mmtbx.refinement import tst_occupancy_selections
from phenix_regression.refinement import run_phenix_refine
import iotbx.pdb
import os
from libtbx import easy_run

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Refined correlated occupancy.
  """
  assert not easy_run.call("rm -rf %s*.mtz"%prefix) # Line below will crash if MTZ present.
  tst_occupancy_selections.prepare_correlated_occupancy_inputs(
    prefix=prefix,
    create_mtz=True)
  pdb_file = "%s_start.pdb"%prefix
  mtz_file = "%s.mtz"%prefix
  args = [
    pdb_file,
    mtz_file,
    "refine.strategy=occupancies+individual_adp",
    "output.prefix=constrain3d",
    "constrain_correlated_3d_groups=True"]
  r = run_phenix_refine(args = args, prefix=prefix)
  #
  hierarchy = iotbx.pdb.input(r.pdb).construct_hierarchy()
  atoms = hierarchy.atoms()
  occ = atoms.extract_occ()
  sel_cache = hierarchy.atom_selection_cache()
  selA = sel_cache.selection("altloc A")
  selB = sel_cache.selection("altloc B")
  occA = occ.select(selA)
  occB = occ.select(selB)
  assert occA.all_eq(occA[0])
  assert occB.all_eq(occB[0])
  assert (occA[0] + occB[0] == 1.0)

if (__name__ == "__main__") :
  run()
  print("OK")
