from __future__ import print_function
import time, os, math
import iotbx.pdb
from scitbx.array_family import flex
import libtbx.load_env
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel
import mmtbx.model
from libtbx.test_utils import approx_equal

pdb_str = """
CRYST1   33.005   14.702   21.532  90.00  90.00  90.00 P 1
ATOM      1  N   GLY A   1       5.020   9.702  11.875  1.00 10.00           N
ATOM      4  O   GLY A   1       6.519   7.552  11.335  1.00 10.00           O
ATOM      7  C   ASN A   2       8.895   7.554   9.468  1.00 10.00           C
"""

scat = """O 0.0974 0.2921 0.6910 0.6990 0.2039 0.2067 1.3815 4.6943 12.7105 32.4726
N 0.1022 0.3219 0.7982 0.8197 0.1715 0.2451 1.7481 6.1925 17.3894 48.1431
C 0.0893 0.2563 0.7570 1.0487 0.3575 0.2465 1.7100 6.4094 18.6113 50.2523"""


def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Exercise external form factors: custom_scattering_factors.
  """
  #
  pdb = "%s.pdb"%prefix
  with open(pdb, "w") as fo:
    fo.write(pdb_str)
  #
  ffs = "%s.scat"%prefix
  with open(ffs, "w") as fo:
    fo.write(scat)
  #
  for st in ["n_gaussian", "electron", "neutron"]:
    args = [
      pdb,
      "high_resolution=1.5",
      "type=real r_free=0.1",
      "scattering_table=%s"%st]
    r = run_fmodel(args = args, prefix = prefix)
    #
    args = [
      pdb,
      r.mtz,
      "strategy=None",
      "main.number_of_mac=0",
      "main.bulk_sol=false",
      "custom_scattering_factors=%s.scat"%prefix,
      ]
    r = run_phenix_refine(args = args, prefix = prefix)
    #
    if st=="electron":
      assert approx_equal(r.r_work_start, r.r_work_final, 1.e-4)
      assert approx_equal(r.r_work_start, 0, 1.e-4)
    elif st=="n_gaussian":
      assert r.r_work_start > 0.05
    elif st=="neutron":
      assert r.r_work_start > 0.2
    else: assert 0

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
