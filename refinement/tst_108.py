from __future__ import print_function
from phenix_regression.refinement import get_r_factors
import libtbx.load_env
from libtbx.test_utils import approx_equal, not_approx_equal, run_command
import sys, os, time, random
from mmtbx import utils
from cctbx.array_family import flex
import iotbx.pdb
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel
import os

if (1):
  random.seed(0)
  flex.set_random_seed(0)

pdb_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="pdb",
  test=os.path.isdir)

common_params = [
  'main.target=ls',
  'main.bulk_solvent_and_=false',
  'optimize_scattering_contribution=false',
  'ls_target_names.target_name=ls_wunit_kunit',
  'structure_factors_and_gradients_accuracy.algorithm=direct',
  'xray_data.r_free_flags.generate=True',
  'xray_data.r_free_flags.ignore_r_free_flags=true',
  "xray_data.outliers_rejection=false",
  "neutron_data.outliers_rejection=false"]

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Occupancies.
  """
  pdb_file = os.path.join(pdb_dir, "1234.pdb")
  mtz_file = prefix+'.mtz'
  counter = 0
  counter1=0
  def assert_sum_is_one(file, sel_str, scat):
    sel = utils.get_atom_selection(pdb_file_name = file,
      selection_string = sel_str)
    assert approx_equal(flex.sum(scat.extract_occupancies().select(sel)), 1.0)
  for refine_sel in ["all", None, "resid 1"]:
    for shake_xyz in [None, 0.3]:
      args = [
        pdb_file,
        'high_resolution=1.5',
        'algorithm=direct',
        'label=FOBS',
        'type=real']
      r0 = run_fmodel(args = args, prefix = prefix)

      pdb_file_to_refine = "%s_shifted.pdb"%prefix
      pdbtools_cmd = " ".join([
        'phenix.pdbtools',
        '%s'%pdb_file,
        'random_seed=7112384',
        'occupancies.randomize=True',
        'sites.shake=%s'%str(shake_xyz),
        "suffix=none",
        'output.prefix=%s'%pdb_file_to_refine.replace(".pdb",""),
        '> tmp_occ_ind_1.log'])
      run_command(
        command=pdbtools_cmd,
        stdout_file_name="tmp_occ_ind_1.log",
        result_file_names=[pdb_file_to_refine],
        verbose=False)

      args = [
        'strategy=occupancies',
        'refine.occupancies.individual="%s"'%refine_sel,
        pdb_file_to_refine,
        r0.mtz,
        'main.number_of_macro_cycles=5'] + common_params
      r = run_phenix_refine(args = args, prefix = prefix)

      # check results
      scat1 = iotbx.pdb.input(file_name =
        pdb_file).xray_structure_simple().scatterers()
      scat2 = iotbx.pdb.input(file_name =
        r.pdb).xray_structure_simple().scatterers()
      assert r.r_work_start > 0.1
      assert r.r_free_start > 0.1
      if(shake_xyz is None):
        counter += 1
        counter1 += 1
        assert approx_equal(r.r_work_final, 0)
        assert approx_equal(r.r_free_final, 0)
        assert approx_equal(scat1.extract_occupancies(),
                            scat2.extract_occupancies())
      else:
        counter += 1
        if(refine_sel in [None, ]):
          counter1 += 1
          for sel_str in ["resid 1", "resid 2", "resid 3"]:
            assert_sum_is_one(
              file = r.pdb, sel_str = sel_str, scat = scat2)
        if(refine_sel == "resid 1"):
          counter1 += 1
          for sel_str in ["resid 2", "resid 3"]:
            assert_sum_is_one(
              file = r.pdb, sel_str = sel_str, scat = scat2)
  assert counter == 6
  assert counter1 == 5

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK", "time: %.2f"%(time.time()-t0))
