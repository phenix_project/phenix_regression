from __future__ import division
from __future__ import print_function
from phenix_regression.refinement import run_phenix_refine
import libtbx.load_env
import os
from libtbx.test_utils import assert_lines_in_file

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Twinning.
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/%s.pdb"%prefix,
    test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/%s.mtz"%prefix,
    test=os.path.isfile)
  args = [
    pdb,
    hkl,
    "main.number_of_mac=1",
    "xray_data.twin_law='%s'"%"h,-k,-l",
    "strategy=individual_adp"]
  r = run_phenix_refine(args = args, prefix = prefix)
  assert r.r_work_final < 0.250
  assert r.r_free_final < 0.265
  assert_lines_in_file(
    file_name=r.pdb,
    lines="REMARK   3  REFINEMENT TARGET : TWIN_LSQ_F")

if (__name__ == "__main__") :
  run()
  print("OK")
