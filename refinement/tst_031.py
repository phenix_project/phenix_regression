from __future__ import division
from __future__ import print_function
import libtbx.load_env
import os
from phenix_regression.refinement import run_phenix_refine
from libtbx.test_utils import assert_lines_in_file

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Exercise duplicate rigid-body selections.
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/enk.pdb", test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/enk_1.5.hkl",
    test=os.path.isfile)
  for sel in [["chain A","chain a"],
              ["chain A and resid 1:2","chain A and resid 2:5"]]:
    args = [
      hkl,
      pdb,
      'main.number_of_macro_cycles=1',
      'main.bulk_s=false',
      'strategy=rigid_body',
      'refine.sites.rigid_body="%s"'%sel[0],
      'refine.sites.rigid_body="%s"'%sel[1]]
    r = run_phenix_refine(args = args, prefix = prefix, sorry_expected=True)
    assert_lines_in_file(
      file_name=r.log,
      lines="Sorry: One or more overlapping selections for refinement.refine.sites.rigid_body:")

if (__name__ == "__main__"):
  run()
  print("OK")
