from __future__ import division
from __future__ import print_function
from libtbx import easy_run
from libtbx.utils import null_out
import libtbx.load_env
import time
import os
from iotbx import cif
from iotbx.cif import validation

pdb_1al1 = """
CRYST1   62.350   62.350   62.350  90.00  90.00  90.00 I 41 3 2     48
ORIGX1      1.000000  0.000000  0.000000        0.00000
ORIGX2      0.000000  1.000000  0.000000        0.00000
ORIGX3      0.000000  0.000000  1.000000        0.00000
SCALE1      0.016038  0.000000  0.000000        0.00000
SCALE2      0.000000  0.016038  0.000000        0.00000
SCALE3      0.000000  0.000000  0.016038        0.00000
HETATM    1  C   ACE A   0      31.227  38.585  11.521  1.00 25.00           C
HETATM    2  O   ACE A   0      30.433  37.878  10.859  1.00 25.00           O
HETATM    3  CH3 ACE A   0      30.894  39.978  11.951  1.00 25.00           C
ATOM      4  N   GLU A   1      32.153  37.943  12.252  1.00 25.00           N
ATOM      5  CA  GLU A   1      32.594  36.639  11.811  1.00 25.00           C
ATOM      6  C   GLU A   1      32.002  35.428  12.514  1.00 25.00           C
ATOM      7  O   GLU A   1      32.521  34.279  12.454  1.00 25.00           O
ATOM      8  CB  GLU A   1      34.093  36.609  11.812  1.00 25.00           C
ATOM      9  CG  GLU A   1      34.609  36.464  13.285  1.00 25.00           C
ATOM     10  CD  GLU A   1      36.125  36.460  13.044  1.00 25.00           C
ATOM     11  OE1 GLU A   1      36.442  35.745  12.084  1.00 25.00           O
ATOM     12  OE2 GLU A   1      36.684  37.260  13.809  1.00 25.00           O
ATOM     13  N   LEU A   2      30.895  35.627  13.188  1.00 25.00           N
ATOM     14  CA  LEU A   2      30.067  34.612  13.814  1.00 25.00           C
ATOM     15  C   LEU A   2      29.025  34.315  12.695  1.00 25.00           C
ATOM     16  O   LEU A   2      28.665  33.168  12.395  1.00 25.00           O
ATOM     17  CB  LEU A   2      29.515  35.034  15.156  1.00 25.00           C
ATOM     18  CG  LEU A   2      28.434  34.122  15.752  1.00 25.00           C
ATOM     19  CD1 LEU A   2      29.008  33.110  16.720  1.00 25.00           C
ATOM     20  CD2 LEU A   2      27.350  34.963  16.399  1.00 25.00           C
ATOM     21  N   LEU A   3      28.628  35.417  12.027  1.00 25.00           N
ATOM     22  CA  LEU A   3      27.686  35.251  10.912  1.00 25.00           C
ATOM     23  C   LEU A   3      28.396  34.389   9.850  1.00 25.00           C
ATOM     24  O   LEU A   3      27.770  33.565   9.191  1.00 25.00           O
ATOM     25  CB  LEU A   3      27.084  36.547  10.374  1.00 25.00           C
ATOM     26  CG  LEU A   3      26.064  36.325   9.246  1.00 25.00           C
ATOM     27  CD1 LEU A   3      24.784  35.715   9.770  1.00 25.00           C
ATOM     28  CD2 LEU A   3      25.777  37.594   8.497  1.00 25.00           C
ATOM     29  N   LYS A   4      29.674  34.603   9.703  1.00 25.00           N
ATOM     30  CA  LYS A   4      30.512  33.899   8.748  1.00 25.00           C
ATOM     31  C   LYS A   4      30.578  32.417   9.098  1.00 25.00           C
ATOM     32  O   LYS A   4      30.261  31.531   8.280  1.00 25.00           O
ATOM     33  CB  LYS A   4      31.924  34.471   8.710  1.00 25.00           C
ATOM     34  CG  LYS A   4      32.539  34.431   7.317  1.00 25.00           C
ATOM     35  CD  LYS A   4      34.046  34.511   7.426  1.00 25.00           C
ATOM     36  CE  LYS A   4      34.729  33.782   6.289  1.00 25.00           C
ATOM     37  NZ  LYS A   4      34.710  34.603   5.059  1.00 25.00           N
ATOM     38  N   LYS A   5      30.993  32.153  10.327  1.00 25.00           N
ATOM     39  CA  LYS A   5      31.104  30.730  10.715  1.00 25.00           C
ATOM     40  C   LYS A   5      29.734  30.111  10.595  1.00 25.00           C
ATOM     41  O   LYS A   5      29.602  29.133   9.833  1.00 25.00           O
ATOM     42  CB  LYS A   5      31.793  30.525  12.023  1.00 25.00           C
ATOM     43  CG  LYS A   5      33.284  30.906  12.043  1.00 25.00           C
ATOM     44  N   LEU A   6      28.753  30.676  11.269  1.00 25.00           N
ATOM     45  CA  LEU A   6      27.375  30.124  11.257  1.00 25.00           C
ATOM     46  C   LEU A   6      26.938  29.849   9.827  1.00 25.00           C
ATOM     47  O   LEU A   6      26.490  28.746   9.489  1.00 25.00           O
ATOM     48  CB  LEU A   6      26.459  31.026  12.062  1.00 25.00           C
ATOM     49  CG  LEU A   6      24.966  30.925  11.913  1.00 25.00           C
ATOM     50  CD1 LEU A   6      24.329  29.894  12.837  1.00 25.00           C
ATOM     51  CD2 LEU A   6      24.339  32.286  12.234  1.00 25.00           C
ATOM     52  N   LEU A   7      27.110  30.857   9.002  1.00 25.00           N
ATOM     53  CA  LEU A   7      26.760  30.849   7.586  1.00 25.00           C
ATOM     54  C   LEU A   7      27.408  29.683   6.868  1.00 25.00           C
ATOM     55  O   LEU A   7      26.743  29.154   5.950  1.00 25.00           O
ATOM     56  CB  LEU A   7      27.003  32.249   6.971  1.00 25.00           C
ATOM     57  CG  LEU A   7      25.878  33.239   6.752  1.00 25.00           C
ATOM     58  CD1 LEU A   7      26.241  34.705   6.949  1.00 25.00           C
ATOM     59  CD2 LEU A   7      25.397  33.158   5.297  1.00 25.00           C
ATOM     60  N   GLU A   8      28.603  29.243   7.208  1.00 25.00           N
ATOM     61  CA  GLU A   8      29.260  28.146   6.496  1.00 25.00           C
ATOM     62  C   GLU A   8      28.759  26.751   6.831  1.00 25.00           C
ATOM     63  O   GLU A   8      28.974  25.822   6.023  1.00 25.00           O
ATOM     64  CB  GLU A   8      30.742  28.009   6.785  1.00 25.00           C
ATOM     65  CG  GLU A   8      31.432  29.271   7.271  1.00 25.00           C
ATOM     66  CD  GLU A   8      32.846  28.933   7.703  1.00 25.00           C
ATOM     67  OE1 GLU A   8      33.727  28.886   6.851  1.00 25.00           O
ATOM     68  OE2 GLU A   8      32.945  28.685   8.934  1.00 25.00           O
ATOM     69  N   GLU A   9      28.216  26.687   8.024  1.00 25.00           N
ATOM     70  CA  GLU A   9      27.676  25.450   8.602  1.00 25.00           C
ATOM     71  C   GLU A   9      26.228  25.197   8.197  1.00 25.00           C
ATOM     72  O   GLU A   9      25.741  24.063   8.299  1.00 25.00           O
ATOM     73  CB  GLU A   9      27.655  25.571  10.131  1.00 25.00           C
ATOM     74  CG  GLU A   9      28.212  24.450  10.999  1.00 25.00           C
ATOM     75  CD  GLU A   9      29.424  24.893  11.771  1.00 25.00           C
ATOM     76  OE1 GLU A   9      29.571  24.773  12.967  1.00 25.00           O
ATOM     77  OE2 GLU A   9      30.236  25.448  10.993  1.00 25.00           O
ATOM     78  N   LEU A  10      25.547  26.243   7.776  1.00 25.00           N
ATOM     79  CA  LEU A  10      24.138  26.200   7.405  1.00 25.00           C
ATOM     80  C   LEU A  10      23.858  25.477   6.104  1.00 25.00           C
ATOM     81  O   LEU A  10      24.653  25.474   5.162  1.00 25.00           O
ATOM     82  CB  LEU A  10      23.611  27.641   7.410  1.00 25.00           C
ATOM     83  CG  LEU A  10      22.355  27.821   8.248  1.00 25.00           C
ATOM     84  CD1 LEU A  10      22.565  27.116   9.582  1.00 25.00           C
ATOM     85  CD2 LEU A  10      22.151  29.317   8.369  1.00 25.00           C
ATOM     86  N   LYS A  11      22.674  24.898   6.074  1.00 25.00           N
ATOM     87  CA  LYS A  11      22.163  24.138   4.939  1.00 25.00           C
ATOM     88  C   LYS A  11      21.851  25.032   3.731  1.00 25.00           C
ATOM     89  O   LYS A  11      21.403  26.185   3.840  1.00 25.00           O
ATOM     90  CB  LYS A  11      20.933  23.323   5.327  1.00 25.00           C
ATOM     91  N   GLY A  12      22.109  24.411   2.573  1.00 25.00           N
ATOM     92  CA  GLY A  12      21.938  24.917   1.234  1.00 25.00           C
ATOM     93  C   GLY A  12      21.882  26.448   1.133  1.00 25.00           C
ATOM     94  O   GLY A  12      22.838  26.994   0.516  1.00 25.00           O
ATOM     95  OXT GLY A  12      20.888  27.022   1.650  1.00 25.00           O
TER      96      GLY A  12
HETATM   97  S   SO4 A  13      31.477  38.950  15.821  0.50 25.00           S
HETATM   98  O1  SO4 A  13      31.243  38.502  17.238  0.50 25.00           O
HETATM   99  O2  SO4 A  13      30.616  40.133  15.527  0.50 25.00           O
HETATM  100  O3  SO4 A  13      31.158  37.816  14.905  0.50 25.00           O
HETATM  101  O4  SO4 A  13      32.916  39.343  15.640  0.50 25.00           O
"""

known_unfixeable_errors = [
"Invalid enumeration value for _software.language: 'Python/C++' not in ('Ada', 'assembler', 'Awk', 'Basic', 'C++', 'C/C++', 'C', 'csh', 'Fortran', 'Fortran_77', 'Fortran 77', 'Fortran 90', 'Java', 'Java & Fortran', 'ksh', 'Pascal', 'Perl', 'Python', 'sh', 'Tcl', 'Other')",
# these only work properly when sequence present
"Invalid loop: missing parent for loop containing '_atom_site.label_seq_id': '_entity_poly_seq.num' required but not present in data block",
"Invalid loop: missing parent for loop containing '_atom_site.label_entity_id': '_entity.id' required but not present in data block",
"Invalid loop: missing parent for loop containing '_atom_site.label_atom_id': '_chem_comp_atom.atom_id' required but not present in data block",
]

known_unfixeable_warnings = [
"Unknown data item: '_chem_comp_atom.type_energy' not present in dictionary",
"Unknown data item: '_chem_comp_tree.comp_id' not present in dictionary",
"Unknown data item: '_chem_comp_tree.atom_id' not present in dictionary",
"Unknown data item: '_chem_comp_tree.atom_back' not present in dictionary",
"Unknown data item: '_chem_comp_tree.atom_forward' not present in dictionary",
"Unknown data item: '_chem_comp_tree.connect_type' not present in dictionary",
"Unknown data item: '_chem_comp_bond.type' not present in dictionary",
"Unknown data item: '_chem_comp.group' not present in dictionary",
"Unknown data item: '_chem_comp.desc_level' not present in dictionary",
"Unknown data item: '_chem_comp_atom.x' not present in dictionary",
"Unknown data item: '_chem_comp_atom.y' not present in dictionary",
"Unknown data item: '_chem_comp_atom.z' not present in dictionary",
"Unknown data item: '_chem_comp_bond.value_dist_neutron' not present in dictionary",
"Unknown data item: '_chem_comp_tor.value_angle' not present in dictionary",
"Unknown data item: '_chem_comp_tor.value_angle_esd' not present in dictionary",
"Unknown data item: '_chem_comp_tor.period' not present in dictionary",
]

def exercise(prefix="tst_cif_dict_compliance"):
  dict_path = libtbx.env.find_in_repositories(
    relative_path='iotbx/cif/dictionaries/mmcif_pdbx_v50.dic.gz', test=os.path.isfile)
  pdb = "%s.pdb" % prefix
  with open(pdb, 'w') as f:
    f.write(pdb_1al1)
  cmd = " ".join([
      "phenix.fmodel",
      "%s"%pdb,
      "high_res=4.0",
      "r_free=0.1",
      "type=real",
      'label=F-obs',
      "output.file_name=%s.mtz" % prefix,
      "random_seed=2679941",
      "> %s.zlog" % prefix])
  print(cmd)
  assert not easy_run.call(cmd)

  cmd = " ".join([
      "phenix.refine",
      "%s" % pdb,
      "%s.mtz" % prefix,
      "wavelength=0.9792",
      "strategy=none",
      "main.bulk_solve=False",
      "xray_data.outliers_rejection=false",
      "neutron_data.outliers_rejection=false",
      'optimize_scattering_contribution=false',
      "main.number_of_mac=0",
      "write_model_cif_file=true",
      "output.prefix=%s" % prefix,
      "main.random_seed=2679941",
      "--overwrite --quiet"])
  print(cmd)
  assert not easy_run.call(cmd)
  cif_fn = "%s_001.cif" % prefix
  assert os.path.isfile(cif_fn)
  cif_dic = validation.smart_load_dictionary(name=dict_path)
  cm = cif.reader(file_path=cif_fn, strict=True).model()
  eh = cm.validate(cif_dic, show_warnings=True) #, out=null_out())
  errors = cm.get_errors()
  n_new_errors = 0
  n_new_warnings = 0
  # print errors
  for cif_block_name, error_handler in errors.items():
    print("E r r o r s:")
    for code, list_of_validation_errors in error_handler.errors.items():
      for ve in list_of_validation_errors:
        if ve.__str__() not in known_unfixeable_errors:
          n_new_errors += 1
          print(ve.__str__())
    print("W a r n i n g s:")
    for code, list_of_validation_warnings in error_handler.warnings.items():
      for vw in list_of_validation_warnings:
        if vw.__str__() not in known_unfixeable_warnings:
          n_new_warnings += 1
          print(vw.__str__())
  print("n_new_er-rors", n_new_errors)
  print("n_new_war-nings", n_new_warnings)
  assert n_new_errors+n_new_warnings == 0, "Errors: %d\nWarnings: %d\n" % (
      n_new_errors, n_new_warnings)+"They are unintended mmCIF dictionary violations."


if (__name__ == "__main__"):
  t0 = time.time()
  exercise()
  print("OK. Time: %8.3f"%(time.time()-t0))
