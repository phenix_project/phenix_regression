from __future__ import print_function
from libtbx import easy_run
import libtbx.load_env
import libtbx.path
import time, os
import iotbx.pdb
import iotbx.cif

def exercise(prefix="model_cif_output_tst_2"):
  root_path = "phenix_regression/pdb/"
  root_name = "3bdn" # XXX crash if switch to "phe" instead of "phe_ia"
  pdb = libtbx.env.find_in_repositories(
    relative_path=root_path+"%s.pdb"%root_name, test=os.path.isfile)
  cmd = " ".join([
    "phenix.fmodel",
    "%s"%pdb,
    "high_res=4.0",
    "r_free=0.1",
    "type=real",
    'label=F-obs',
    "output.file_name=%s.mtz" % prefix,
    "random_seed=2679941",
    "> %s.zlog" % prefix])
  print(cmd)
  assert not easy_run.call(cmd)

  cmd = " ".join([
    "phenix.refine",
    "%s" % pdb,
    "%s.mtz" % prefix,
    "wavelength=0.9792",
    "strategy=none",
    "main.bulk_solve=False",
    "xray_data.outliers_rejection=false",
    "neutron_data.outliers_rejection=false",
    'optimize_scattering_contribution=false',
    "main.number_of_mac=0",
    "write_model_cif_file=true",
    "output.prefix=%s" % prefix,
    "write_reflection_cif_file=true",
    "main.random_seed=2679941",
    "--overwrite --quiet"])
  print(cmd)
  assert not easy_run.call(cmd)
  assert os.path.isfile("%s_001.cif" % prefix)
  cif_f = open("%s_001.cif" % prefix, 'r')
  cif_l = cif_f.readlines()
  cif_f.close()

  for l in ["_cell.angle_alpha                 90.000\n",
      "  _software.name\n",
      "  _citation.title\n",
      "  _citation_author.name\n",
      "  _refine_ls_restr.number\n",
      "  _struct_conf.pdbx_PDB_helix_id\n",
      "  _struct_sheet.id\n",
      "  _struct_sheet_range.id\n",
      "  _atom_site.label_atom_id\n",
      ]:
    assert l in cif_l, "%s not in cif file!" % l

if (__name__ == "__main__"):
  t0 = time.time()
  exercise()
  print("OK. Time: %8.3f"%(time.time()-t0))
