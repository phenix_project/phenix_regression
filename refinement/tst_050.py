from __future__ import division
from __future__ import print_function
from phenix_regression.refinement import run_phenix_refine
import iotbx.pdb
import libtbx.load_env
import os
from libtbx.test_utils import approx_equal

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Test refinement parallel.
  """
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/enk.pdb",
    test=os.path.isfile)
  mtz_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/enk.mtz",
    test=os.path.isfile)
  base = [
    pdb_file,
    mtz_file,
    "const_shrink_donor_acceptor=0.6",
    "main.number_of_macro_cycles=2",
    "xray_data.high_resolution=2.5",
    "optimize_xyz_weight=True",
    "optimize_adp_weight=True",
    "bonds_rmsd=0.014",
    "angles_rmsd=1.6"]
  opt_args = [
    [""],
    ["main.nproc=2"],
  ]
  rs = []
  for i, args_i in enumerate(opt_args):
    args = base
    if args_i != [""]:
      args += args_i
    r = run_phenix_refine(args = args, prefix = "%s_%d"%(prefix,i))
    rs.append(r)
  #
  h1 = iotbx.pdb.input(rs[0].pdb).construct_hierarchy()
  h2 = iotbx.pdb.input(rs[1].pdb).construct_hierarchy()
  assert h1.is_similar_hierarchy(h2)
  assert approx_equal(rs[0].r_work_start, rs[1].r_work_start)
  assert approx_equal(rs[0].r_free_start, rs[1].r_free_start)
  assert approx_equal(rs[0].r_work_final, rs[1].r_work_final, 1e-2)
  assert approx_equal(rs[0].r_free_final, rs[1].r_free_final, 1e-2)

if (__name__ == "__main__") :
  run()
  print("OK")
