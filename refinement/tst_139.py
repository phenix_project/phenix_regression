from __future__ import print_function
from libtbx import easy_run
import libtbx.load_env
import os, time
from iotbx import reflection_file_reader
import iotbx.pdb
from scitbx.array_family import flex
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel

pdb_str = """
CRYST1   26.851   32.174   25.146  90.00  90.00  90.00 P 1
ATOM      1  N   GLY B   1      10.491   7.299  10.131  1.00  7.85           N
ATOM      2  CA  GLY B   1      11.259   8.382   9.484  1.00  6.79           C
ATOM      3  C   GLY B   1      11.687   9.407  10.508  1.00  5.59           C
ATOM      4  O   GLY B   1      11.421   9.253  11.703  1.00  6.04           O
ATOM      5  N   CYS B   2      12.252  10.501  10.025  1.00  5.95           N
ATOM      6  CA  CYS B   2      12.751  11.577  10.876  1.00  5.17           C
ATOM      7  C   CYS B   2      11.799  12.091  11.950  1.00  4.74           C
ATOM      8  O   CYS B   2      12.183  12.260  13.118  1.00  4.51           O
ATOM      9  CB  CYS B   2      13.149  12.762   9.999  1.00  5.99           C
ATOM     10  SG  CYS B   2      13.939  14.119  10.918  1.00  5.51           S
ATOM     11  N   CYS B   3      10.550  12.326  11.569  1.00  4.88           N
ATOM     12  CA  CYS B   3       9.604  12.903  12.504  1.00  5.24           C
ATOM     13  C   CYS B   3       9.144  12.023  13.658  1.00  6.13           C
ATOM     14  O   CYS B   3       8.554  12.527  14.612  1.00  7.85           O
ATOM     15  CB  CYS B   3       8.451  13.561  11.745  1.00  4.92           C
ATOM     16  SG  CYS B   3       9.066  14.671  10.427  1.00  6.13           S
ATOM     17  N   SER B   4       9.450  10.728  13.602  1.00  5.39           N
ATOM     18  CA  SER B   4       9.102   9.815  14.690  1.00  7.14           C
ATOM     19  C   SER B   4      10.349   9.454  15.515  1.00  6.23           C
ATOM     20  O   SER B   4      10.284   8.636  16.433  1.00  7.65           O
ATOM     21  CB  SER B   4       8.422   8.545  14.154  1.00  8.23           C
ATOM     22  OG ASER B   4       7.362   8.850  13.271  0.50  5.46           O
ATOM     23  OG BSER B   4       9.102   7.982  13.050  0.50 11.74           O
ATOM     24  N   LEU B   5      11.479  10.072  15.178  1.00  6.26           N
ATOM     25  CA  LEU B   5      12.755   9.846  15.860  1.00  6.46           C
ATOM     26  C   LEU B   5      13.115  11.181  16.520  1.00  6.72           C
ATOM     27  O   LEU B   5      13.600  12.100  15.853  1.00  5.39           O
ATOM     28  CB  LEU B   5      13.816   9.444  14.824  1.00  7.73           C
ATOM     29  CG  LEU B   5      15.240   9.109  15.285  1.00 11.40           C
ATOM     30  CD1 LEU B   5      15.217   7.901  16.228  1.00 13.82           C
ATOM     31  CD2 LEU B   5      16.123   8.819  14.067  1.00  9.71           C
ATOM     32  N   PRO B   6      12.930  11.293  17.848  1.00  6.78           N
ATOM     33  CA  PRO B   6      13.229  12.537  18.572  1.00  6.85           C
ATOM     34  C   PRO B   6      14.486  13.329  18.167  1.00  6.38           C
ATOM     35  O   PRO B   6      14.385  14.516  17.865  1.00  6.65           O
ATOM     36  CB  PRO B   6      13.242  12.092  20.037  1.00  8.54           C
ATOM     37  CG  PRO B   6      12.187  11.020  20.052  1.00  8.56           C
ATOM     38  CD  PRO B   6      12.503  10.232  18.786  1.00  7.86           C
TER
ATOM     39  N   PRO A   7      15.669  12.685  18.122  1.00  7.30           N
ATOM     40  CA  PRO A   7      16.888  13.415  17.740  1.00  8.29           C
ATOM     41  C   PRO A   7      16.787  14.047  16.349  1.00  7.53           C
ATOM     42  O   PRO A   7      17.281  15.153  16.130  1.00  8.75           O
ATOM     43  CB  PRO A   7      17.959  12.323  17.756  1.00  9.91           C
ATOM     44  CG  PRO A   7      17.448  11.345  18.745  1.00 11.94           C
ATOM     45  CD  PRO A   7      15.986  11.278  18.426  1.00  8.51           C
ATOM     46  N   CYS A   8      16.148  13.348  15.414  1.00  6.06           N
ATOM     47  CA  CYS A   8      16.011  13.879  14.059  1.00  5.71           C
ATOM     48  C   CYS A   8      14.988  15.013  13.999  1.00  6.25           C
ATOM     49  O   CYS A   8      15.243  16.055  13.392  1.00  6.08           O
ATOM     50  CB  CYS A   8      15.650  12.778  13.061  1.00  4.84           C
ATOM     51  SG  CYS A   8      15.767  13.341  11.326  1.00  5.86           S
ATOM     52  N   ALA A   9      13.843  14.827  14.653  1.00  5.45           N
ATOM     53  CA  ALA A   9      12.803  15.850  14.667  1.00  6.41           C
ATOM     54  C   ALA A   9      13.335  17.151  15.264  1.00  6.69           C
ATOM     55  O   ALA A   9      13.015  18.240  14.791  1.00  7.39           O
ATOM     56  CB  ALA A   9      11.595  15.366  15.456  1.00  6.78           C
ATOM     57  N   LEU A  10      14.164  17.025  16.296  1.00  6.28           N
ATOM     58  CA  LEU A  10      14.750  18.178  16.969  1.00  7.56           C
ATOM     59  C   LEU A  10      15.622  19.008  16.022  1.00  6.72           C
ATOM     60  O   LEU A  10      15.600  20.241  16.071  1.00  7.32           O
ATOM     61  CB  LEU A  10      15.589  17.707  18.159  1.00  8.71           C
ATOM     62  CG  LEU A  10      16.029  18.752  19.183  1.00 12.81           C
ATOM     63  CD1 LEU A  10      14.815  19.254  19.946  1.00 12.39           C
ATOM     64  CD2 LEU A  10      17.049  18.146  20.146  1.00 15.18           C
ATOM     65  N   SER A  11      16.374  18.326  15.158  1.00  5.98           N
ATOM     66  CA  SER A  11      17.264  18.984  14.204  1.00  6.98           C
ATOM     67  C   SER A  11      16.601  19.377  12.881  1.00  6.49           C
ATOM     68  O   SER A  11      17.206  20.063  12.051  1.00  7.13           O
ATOM     69  CB  SER A  11      18.483  18.099  13.933  1.00  9.58           C
ATOM     70  OG ASER A  11      19.140  17.744  15.137  0.50 10.57           O
ATOM     71  OG BSER A  11      18.095  16.764  13.665  0.50 14.70           O
ATOM     72  N   ASN A  12      15.364  18.941  12.675  1.00  4.40           N
ATOM     73  CA  ASN A  12      14.629  19.263  11.448  1.00  5.14           C
ATOM     74  C   ASN A  12      13.237  19.728  11.862  1.00  4.60           C
ATOM     75  O   ASN A  12      12.233  19.239  11.346  1.00  4.90           O
ATOM     76  CB  ASN A  12      14.509  18.014  10.554  1.00  4.86           C
ATOM     77  CG  ASN A  12      15.853  17.517  10.054  1.00  5.73           C
ATOM     78  OD1 ASN A  12      16.262  17.832   8.935  1.00  6.58           O
ATOM     79  ND2 ASN A  12      16.543  16.730  10.871  1.00  6.19           N
ATOM     80  N   PRO A  13      13.163  20.772  12.707  1.00  5.42           N
ATOM     81  CA  PRO A  13      11.865  21.269  13.177  1.00  5.25           C
ATOM     82  C   PRO A  13      10.874  21.803  12.148  1.00  5.40           C
ATOM     83  O   PRO A  13       9.687  21.521  12.239  1.00  5.97           O
ATOM     84  CB  PRO A  13      12.257  22.332  14.209  1.00  6.25           C
ATOM     85  CG  PRO A  13      13.542  22.876  13.659  1.00  7.10           C
ATOM     86  CD  PRO A  13      14.264  21.630  13.186  1.00  6.94           C
ATOM     87  N   ASP A  14      11.347  22.596  11.191  1.00  6.61           N
ATOM     88  CA  ASP A  14      10.446  23.162  10.189  1.00  6.75           C
ATOM     89  C   ASP A  14       9.983  22.111   9.190  1.00  8.16           C
ATOM     90  O   ASP A  14       8.881  22.181   8.654  1.00  9.76           O
ATOM     91  CB  ASP A  14      11.121  24.345   9.495  1.00 11.62           C
ATOM     92  CG  ASP A  14      11.449  25.472  10.468  1.00 16.46           C
ATOM     93  OD1 ASP A  14      10.514  26.009  11.101  1.00 16.79           O
ATOM     94  OD2 ASP A  14      12.640  25.810  10.619  1.00 23.02           O
ATOM     95  N   TYR A  15      10.848  21.143   8.941  1.00  6.51           N
ATOM     96  CA  TYR A  15      10.575  20.041   8.035  1.00  6.05           C
ATOM     97  C   TYR A  15       9.451  19.175   8.616  1.00  6.38           C
ATOM     98  O   TYR A  15       8.505  18.830   7.906  1.00  7.26           O
ATOM     99  CB  TYR A  15      11.880  19.250   7.881  1.00  8.86           C
ATOM    100  CG  TYR A  15      11.846  17.947   7.104  1.00  8.00           C
ATOM    101  CD1 TYR A  15      11.970  17.933   5.710  1.00  9.32           C
ATOM    102  CD2 TYR A  15      11.797  16.721   7.772  1.00 10.46           C
ATOM    103  CE1 TYR A  15      12.050  16.717   5.000  1.00 10.27           C
ATOM    104  CE2 TYR A  15      11.876  15.510   7.072  1.00  9.92           C
ATOM    105  CZ  TYR A  15      12.004  15.518   5.692  1.00 11.82           C
ATOM    106  OH  TYR A  15      12.070  14.316   5.023  1.00 14.60           O
ATOM    107  N   CYS A  16       9.539  18.866   9.910  1.00  5.42           N
ATOM    108  CA  CYS A  16       8.523  18.046  10.566  1.00  6.46           C
ATOM    109  C   CYS A  16       7.269  18.829  10.953  1.00  6.39           C
ATOM    110  O   CYS A  16       6.182  18.217  10.961  1.00  7.99           O
ATOM    111  CB  CYS A  16       9.106  17.351  11.800  1.00  6.56           C
ATOM    112  SG  CYS A  16      10.260  16.005  11.390  1.00  6.50           S
HETATM  113  N   NH2 A  17       7.380  20.110  11.278  1.00  6.64           N
TER
HETATM  115  O   HOH A  18      13.116  26.276  14.154  1.00 44.08           O
HETATM  116  O   HOH A  19       7.962  15.148  14.993  1.00 33.96           O
HETATM  117  O   HOH A  20      12.068  12.411   6.851  1.00 42.45           O
HETATM  118  O   HOH A  21      11.655   5.104   8.183  1.00 24.79           O
HETATM  119  O   HOH A  22       8.105   8.534  10.700  1.00  8.75           O
HETATM  120  O   HOH A  23       5.690   9.248  15.550  1.00 15.41           O
HETATM  121  O   HOH A  24       6.638  11.710  16.579  1.00 15.81           O
HETATM  122  O   HOH A  25       9.520  12.356  17.554  1.00 26.35           O
HETATM  123  O   HOH A  26      12.291  15.788  19.419  1.00 18.78           O
HETATM  124  O   HOH A  27       8.859  20.005  14.376  1.00 29.82           O
HETATM  125  O   HOH A  28      10.862  19.211  16.040  1.00 25.43           O
HETATM  126  O   HOH A  29       5.000  21.620  12.241  1.00  8.98           O
HETATM  127  O   HOH A  30       6.839  21.113   7.141  1.00 13.91           O
HETATM  128  O   HOH A  31       6.796  23.830   9.383  1.00 36.19           O
HETATM  129  O   HOH A  32       8.114  26.306   9.738  1.00 30.78           O
HETATM  130  O   HOH A  33      12.065   5.889  12.528  1.00 29.94           O
HETATM  131  O   HOH A  34      19.477  16.482  17.322  1.00 29.97           O
HETATM  132  O   HOH A  35      13.830  21.756   9.327  1.00 10.45           O
HETATM  133  O   HOH A  36       9.945  27.174  13.425  1.00 28.36           O
HETATM  134  O   HOH A  37      21.851  17.796  14.782  1.00 33.69           O
HETATM  135  O   HOH A  38      14.188  26.193   7.341  1.00 42.44           O
HETATM  136  O   HOH A  39      20.407  14.294  15.721  1.00 30.99           O
HETATM  137  O   HOH A  40       9.267   5.000  14.413  1.00 35.54           O
TER
END
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Apply scale to F-obs
  """
  pdb_file = "%s.pdb"%prefix
  hkl_file = "%s.mtz"%prefix
  with open(pdb_file, "w") as fo:
    fo.write(pdb_str)
  #
  args = [
    pdb_file,
    "high_resolution=3.9",
    "scale=0.0000001",
    "k_sol=0.35 b_sol=60",
    "type=real r_free=0.3",
    "output.file_name=%s"%hkl_file]
  r = run_fmodel(args = args, prefix = prefix)
  #
  args = [
    "%s.pdb"%prefix,
    "%s.mtz"%prefix,
    "strategy=none",
    "main.number_of_mac=1"]
  r = run_phenix_refine(args = args, prefix = prefix)
  #
  miller_arrays = reflection_file_reader.any_reflection_file(file_name =
    r.mtz).as_miller_arrays()
  for ma in miller_arrays:
    if(ma.info().labels == ['2FOFCWT', 'PH2FOFCWT']):
      mc1 = ma.deep_copy()
    if(ma.info().labels == ['2FOFCWT_no_fill', 'PH2FOFCWT_no_fill']):
      mc2 = ma.deep_copy()
  fft_map_1 = mc1.fft_map(resolution_factor=0.25)
  fft_map_1.apply_sigma_scaling()
  map_1 = fft_map_1.real_map_unpadded()
  fft_map_2 = mc2.fft_map(resolution_factor=0.25)
  fft_map_2.apply_sigma_scaling()
  map_2 = fft_map_2.real_map_unpadded()
  #
  sites_frac = iotbx.pdb.input(file_name=r.pdb).\
    xray_structure_simple().sites_frac()
  mv1 = flex.double()
  mv2 = flex.double()
  for sf in sites_frac:
    mv1.append(map_1.eight_point_interpolation(sf))
    mv2.append(map_2.eight_point_interpolation(sf))
  assert flex.mean(mv1) > 3, flex.mean(mv1)
  assert flex.mean(mv2) > 2.5, flex.mean(mv2)

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
