from __future__ import division
from __future__ import print_function
import libtbx.load_env
import os
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Another version of IAS test
  """
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/ygg.pdb", test=os.path.isfile)
  args = [
    pdb_file,
    "r_free_flags_fraction=0.1",
    'high_resolution=0.8',
    'label="FOBS"',
    'type=real']
  r = run_fmodel(args = args, prefix = prefix)
  args = [
    pdb_file,
    r.mtz,
    'strategy=individual_sites+individual_adp+occupancies',
    'ias=True',
    'refinement.main.ordered_solvent=False','refinement.target_weights.wc=0',
    'main.number_of_macro_cycles=2']
  r = run_phenix_refine(args = args, prefix = prefix)

if __name__ == '__main__':
  run()
  print("OK")
