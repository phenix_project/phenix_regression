#! /bin/csh -fe

set PHENIX_REGRESSION_DIR="`libtbx.find_in_repositories phenix_regression`"

phenix.refine "$PHENIX_REGRESSION_DIR"/pdb/mah.pdb "$PHENIX_REGRESSION_DIR"/reflection_files/mah.mtz strategy=tls adp.tls="chain A" --overwrite

echo "OK"
