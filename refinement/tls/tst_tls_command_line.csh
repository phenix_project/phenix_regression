#! /bin/csh -fe

set PHENIX_REGRESSION_DIR="`libtbx.find_in_repositories phenix_regression`"

phenix.find_tls_groups "$PHENIX_REGRESSION_DIR"/pdb/1yjp.pdb

echo "OK"
