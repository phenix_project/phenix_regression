from __future__ import print_function
from libtbx import easy_run
import libtbx.load_env
import os, time
import iotbx.pdb
from scitbx.array_family import flex
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Model should not move within specified tolerances.
  """
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/data/%s.pdb"%prefix,
    test=os.path.isfile)
  #
  args = [
    pdb_file,
    "high_res=1.5",
    "type=real",
    "r_free=0.1"]
  r = run_fmodel(args = args, prefix = prefix)
  #
  args = [
    pdb_file,
    r.mtz,
    "xray_data.r_free_flags.ignore_r_free_flags=true",
    "real_space_refinement=local",
    "main.number_of_mac=3",
    "strategy=individual_sites_real_space",
    "main.bulk_solvent_and_sc=false"]
  r = run_phenix_refine(args = args, prefix = prefix)
  #
  x1 = iotbx.pdb.input(file_name=r.pdb).xray_structure_simple()
  x0 = iotbx.pdb.input(file_name=pdb_file).xray_structure_simple()
  diff = x0.distances(x1)
  assert r.r_work_start  < 1.e-4, r.r_work_start
  assert r.r_work_final  < 0.008, r.r_work_final
  assert flex.mean(diff) < 0.001, flex.mean(diff)

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
