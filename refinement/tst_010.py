from __future__ import division
from __future__ import print_function
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel
import os

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Exercise disable C-beta restraints.
  """
  pdb_in = """\
ATOM   3308  N   ASN A 392      -2.806 -27.732 -22.261  1.00  6.88           N
ATOM   3309  CA  ASN A 392      -3.392 -26.713 -23.104  1.00  7.86           C
ATOM   3310  C   ASN A 392      -2.402 -25.965 -23.992  1.00  7.60           C
ATOM   3311  O   ASN A 392      -2.750 -24.952 -24.599  1.00  9.52           O
ATOM   3312  CB AASN A 392      -4.432 -25.889 -22.497  0.50  7.41           C
ATOM   3313  CG AASN A 392      -5.770 -26.579 -22.351  0.50  6.70           C
ATOM   3314  OD1AASN A 392      -6.378 -27.027 -23.337  0.50 13.64           O
ATOM   3315  ND2AASN A 392      -6.328 -26.514 -21.165  0.50  8.53           N
ATOM   3316  CB BASN A 392      -3.973 -25.581 -22.072  0.50  8.61           C
ATOM   3317  CG BASN A 392      -4.880 -26.163 -20.932  0.50  9.15           C
ATOM   3318  OD1BASN A 392      -5.763 -26.978 -21.230  0.50 11.26           O
ATOM   3319  ND2BASN A 392      -4.634 -25.795 -19.649  0.50 10.17           N
"""
  pdb_file = "%s.pdb"%prefix
  open(pdb_file, "w").write(pdb_in)
  args = [
    pdb_file,
    "high_resolution=2.0",
    "r_free_flags_fraction=0.1",
    "random_seed=12345",
    "type=real",
    "label=F",
    "generate_fake_p1_symmetry=True",
  ]
  r = run_fmodel(args = args, prefix = prefix)
  args = [
    pdb_file,
    r.mtz,
    "refine.strategy=individual_sites",
    "c_beta_restraints=False",
  ]
  r = run_phenix_refine(args = args, prefix = prefix)
  cntr=0
  fo = open(r.log,"r")
  for l in fo.readlines():
    if(l.count("Adding C-beta torsion restraints") or
       l.count("num c-beta restraints")):
      cntr+=1
  assert cntr==0
  assert (r.r_free_final < 0.03)

if (__name__ == "__main__") :
  run()
  print("OK")
