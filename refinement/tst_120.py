from __future__ import print_function
import iotbx.mtz
import libtbx.load_env
import time, os, random
from libtbx import easy_run
from scitbx.array_family import flex
from libtbx.test_utils import approx_equal
from phenix_regression.refinement import run_phenix_refine

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Compare reported R factors with those re-calculated using arrays in output MTZ
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/pdb1akg_very_well_phenix_refined_001.pdb",
    test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/1akg.mtz",
    test=os.path.isfile)
  for strategy in ["strategy=none","","strategy=tls","strategy=rigid_body",
                   "hydrogens.refine=individual", "hydrogens.refine=riding"]:
    output_file_prefix = str(random.randint(1, 1000000))
    args = [
      hkl,
      pdb,
      "main.number_of_mac=1",
      "xray_data.high_res=3.9"]
    if strategy:
      args.append("%s"%strategy)
    r = run_phenix_refine(args = args, prefix = prefix)
    r_work_from_file = r.r_work_final
    r_free_from_file = r.r_free_final
    mtz_obj_out = iotbx.mtz.object(file_name=r.mtz)
    f_obs, r_free_flags, f_model = None, None, None
    for ma in mtz_obj_out.as_miller_arrays():
      if(ma.info().labels == ['F-obs-filtered', 'SIGF-obs-filtered']):
        f_obs = ma
      if(ma.info().labels == ['R-free-flags']):
        r_free_flags = ma
      if(ma.info().labels == ['F-model', 'PHIF-model']):
        f_model = ma
    assert [f_obs, r_free_flags, f_model].count(None) == 0
    r_free_flags = r_free_flags.common_set(f_obs)
    fo = f_obs.data()
    fc = flex.abs(f_model.data())
    r_free_flags = r_free_flags.data() == 1
    r_work=flex.sum(flex.abs(fo.select(~r_free_flags)-fc.select(~r_free_flags)))/\
      flex.sum(fo.select(~r_free_flags))
    r_free=flex.sum(flex.abs(fo.select(r_free_flags)-fc.select(r_free_flags)))/\
      flex.sum(fo.select(r_free_flags))
    assert approx_equal(r_work, r_work_from_file, 0.0001)
    assert approx_equal(r_free, r_free_from_file, 0.0001)

if (__name__ == "__main__"):
  t0 = time.time()
  run() #XXX add the analogous test for twin_f_model
  print("OK time = %8.3f" % (time.time() - t0))
