from __future__ import division
from __future__ import print_function
import iotbx.pdb
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel
import os

pdb_str = """
CRYST1   49.169   20.644   38.413  90.00  90.00  90.00 P 1
ATOM      1  H   HYD S   1       5.000   8.857  33.413  2.05 21.02           H
ATOM      3  H   HYD S   3      21.447  13.472  25.265  1.28 33.05           H
ATOM      4  H   HYD S   4      37.195   5.049  13.193  1.71 33.67           H
ATOM      5  H   HYD S   5      31.198  10.025  30.176  2.51 34.18           H
ATOM      6  H   HYD S   6      31.400  10.430  17.873  1.36 35.56           H
ATOM      7  H   HYD S   7       6.376   5.945  22.835  2.40 26.89           H
ATOM      8  H   HYD S   8      15.957  12.455   5.000  2.15 29.36           H
ATOM      9  H   HYD S   9      44.169  15.644  12.305  2.07 24.56           H
ATOM     10  H   HYD S  10      26.952   5.000   8.934  1.09 32.89           H
TER
END
"""

params_str = """
refinement {
  refine {
    strategy = individual_sites *individual_sites_real_space rigid_body \
               *individual_adp group_adp tls *occupancies group_anomalous
    occupancies {
      individual = all
    }
    anomalous_scatterers {
      group {
        selection = chain S
      }
    }
  }
  main {
    bulk_solvent_and_scale = False
    nqh_flips = False
    number_of_macro_cycles = 5
    target = auto ml mlhl ml_sad *ls
    fake_f_obs = True
    occupancy_max = 20
  }
  fake_f_obs {
    structure_factors_accuracy {
      algorithm = fft *direct
    }
  }
  hydrogens {
    refine = *individual riding Auto
    optimize_scattering_contribution = False
    real_space_optimize_x_h_orientation = False
  }
  pdb_interpretation {
    clash_guard {
      nonbonded_distance_threshold = None
    }
  }
  bulk_solvent_and_scale {
    bulk_solvent = False
    k_sol_b_sol_grid_search = False
    minimization_k_sol_b_sol = False
  }
  target_weights {
    wc = 0
  }
  structure_factors_and_gradients_accuracy {
    algorithm = fft *direct
  }
}
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Exercise refinement of an exotoc file containing all H atoms against generated
  Fobs data so that expected R-factors are 0.
  """
  pdb_in = "%s.pdb"%prefix
  open(pdb_in, "w").write(pdb_str)
  args = [
    pdb_in,
    "high_res=2",
    "type=real",
    "r_free=0.1"]
  r = run_fmodel(args = args, prefix = prefix)
  #
  phil = "%s.params"%prefix
  open(phil, "w").write(params_str)
  #
  args = [pdb_in, r.mtz, phil]
  r = run_phenix_refine(args = args, prefix = prefix)
  r.check_final_r(r_work=0., r_free=0., eps=1.e-4)
  r.check_start_r(r_work=0., r_free=0., eps=1.e-4)

if (__name__ == "__main__"):
  run()
  print("OK")
