from __future__ import division
from __future__ import print_function
import libtbx.load_env
import os
from phenix_regression.refinement import run_phenix_refine

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Assortment of old tests. Converted from primitive csh scripts.
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/gbr_with_anisou.pdb",
    test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/gbr_with_anisou.mtz",
    test=os.path.isfile)
  phil = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/phil_files/gbr_with_anisou",
    test=os.path.isfile)
  args = [pdb,hkl,phil,"xray_data.high_res=4", "main.number_of_mac=1"]
  r = run_phenix_refine(args = args, prefix = prefix)

if (__name__ == "__main__"):
  run()
  print("OK")
