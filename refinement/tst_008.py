from __future__ import division
from __future__ import print_function
import libtbx.load_env
import os
import iotbx.pdb
from phenix_regression.refinement import run_phenix_refine

def run(prefix = os.path.basename(__file__).replace(".py","")):
  pdb = libtbx.env.find_in_repositories(
        relative_path="phenix_regression/pdb/1yjp.pdb", test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
        relative_path="phenix_regression/reflection_files/1yjp.mtz", \
        test=os.path.isfile)
  args = [
      hkl,
      pdb,
      "strategy=individual_sites+individual_adp",
      "main.number_of_macro_cycles=1",
      "adp.individual.anisotropic=all",
      "modify_start_model.modify.selection='resid 1:4'",
      "modify_start_model.modify.adp.set_b_iso=0"]
  r = run_phenix_refine(args = args, prefix = prefix)
  f = open(r.pdb, 'r')
  lines = f.readlines()
  pdb_h = iotbx.pdb.input(file_name=r.pdb).construct_hierarchy()
  for a in pdb_h.atoms()[:20]:
    assert a.b > 1, "refinement ended up with the same Biso (0) after refinement"

if (__name__ == "__main__"):
  run()
  print("OK")
