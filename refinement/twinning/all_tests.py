from __future__ import print_function
from phenix_regression.refinement import check_r_factors
from libtbx.test_utils import run_command
import libtbx.load_env
import sys, os, time
from libtbx import easy_run

def exercise_01():
  d = libtbx.env.under_dist(
    module_name="phenix_regression",
    path="refinement/twinning",
    test=os.path.isdir)
  cmd = " ".join([
    'phenix.refine',
    '--overwrite xray_data.twin_law=-h,k,-l',
    #'main.target=ls --overwrite', # this is to compare times with non-twin refinement
    '"%s/test1.mtz"' % d,
    '"%s/test1.pdb"' % d,
    'input.xray_data.high_resolution=1.5',
    'main.number_of_macro_cycles=2',
    'modify_start_model.modify.sites.shake=0.1',
    'modify_start_model.modify.adp.randomize=True',
    'output.prefix=twin_test_01',
    'ordered_solvent=true',
    'export_final_f_model=true',
    '>',
    'twin_test_01.log'])
  run_command(
    command=cmd,
    log_file_name="twin_test_01_001.log",
    stdout_file_name="twin_test_01.log",
    show_diff_log_stdout=True,
    verbose=1)
  assert check_r_factors(
    file_name="twin_test_01_001.pdb",
    r_work_start_ref=0.1134,
    r_free_start_ref=0.1049,
    r_work_final_ref=0.0435,
    r_free_final_ref=0.0465)


def exercise_02():
  d = libtbx.env.under_dist(
    module_name="phenix_regression",
    path="refinement/twinning",
    test=os.path.isdir)
  cmd = " ".join([
    'phenix.refine',
    '--overwrite xray_data.twin_law=-h,k,-l',
    #'main.target=ls --overwrite', # this is to compare times with non-twin refinement
    '"%s/test1.mtz"' % d,
    '"%s/test1.pdb"' % d,
    'input.xray_data.high_resolution=1.5',
    'main.number_of_macro_cycles=3',
    'modify_start_model.modify.sites.shake=0.1',
    'modify_start_model.modify.adp.randomize=True',
    'output.prefix=twin_test_01a',
    'detwin.mode=auto',
    'ordered_solvent=true',
    'export_final_f_model=true',
    '>',
    'twin_test_01a.log'])
  run_command(
    command=cmd,
    log_file_name="twin_test_01a_001.log",
    stdout_file_name="twin_test_01a.log",
    show_diff_log_stdout=True,
    verbose=1)
  assert check_r_factors(
    file_name="twin_test_01a_001.pdb",
    r_work_start_ref=0.1143,
    r_free_start_ref=0.0942,
    r_work_final_ref=0.0268,
    r_free_final_ref=0.0296)




def exercise_03():
  d = libtbx.env.under_dist(
    module_name="phenix_regression",
    path="refinement/twinning",
    test=os.path.isdir)
  cmd = " ".join([
    'phenix.refine',
    '--overwrite xray_data.twin_law=-h,k,-l',
    #'main.target=ls --overwrite', # this is to compare times with non-twin refinement
    '"%s/test1.mtz"' % d,
    '"%s/test1.pdb"' % d,
    'input.xray_data.high_resolution=1.5',
    'main.number_of_macro_cycles=5',
    'modify_start_model.modify.sites.shake=0.1',
    'modify_start_model.modify.adp.randomize=True',
    'output.prefix=twin_test_01b',
    'detwin.mode=proportional',
    'ordered_solvent=true',
    'export_final_f_model=true',
    '>',
    'twin_test_01b.log'])
  run_command(
    command=cmd,
    log_file_name="twin_test_01b_001.log",
    stdout_file_name="twin_test_01b.log",
    show_diff_log_stdout=True,
    verbose=1)
  assert check_r_factors(
    file_name="twin_test_01b_001.pdb",
    r_work_start_ref=0.1134,
    r_free_start_ref=0.0942,
    r_work_final_ref=0.0376,
    r_free_final_ref=0.0377)



def exercise_04():
  d = libtbx.env.under_dist(
    module_name="phenix_regression",
    path="refinement/twinning",
    test=os.path.isdir)
  cmd = " ".join([
    'phenix.refine',
    '--overwrite xray_data.twin_law=-h,k,-l',
    #'main.target=ls --overwrite', # this is to compare times with non-twin refinement
    '"%s/test1.mtz"' % d,
    '"%s/test1.pdb"' % d,
    'input.xray_data.high_resolution=1.5',
    'main.number_of_macro_cycles=5',
    'modify_start_model.modify.sites.shake=0.1',
    'modify_start_model.modify.adp.randomize=True',
    'output.prefix=twin_test_01c',
    'detwin.mode=algebraic',
    'ordered_solvent=true',
    'export_final_f_model=true',
    '>',
    'twin_test_01c.log'])
  run_command(
    command=cmd,
    log_file_name="twin_test_01c_001.log",
    stdout_file_name="twin_test_01c.log",
    show_diff_log_stdout=True,
    verbose=1)
  assert check_r_factors(
    file_name="twin_test_01c_001.pdb",
    r_work_start_ref=0.1134,
    r_free_start_ref=0.1049,
    r_work_final_ref=0.0380,
    r_free_final_ref=0.0380)



def exercise_05():
  d = libtbx.env.under_dist(
    module_name="phenix_regression",
    path="refinement/twinning",
    test=os.path.isdir)
  cmd = " ".join([
    'phenix.refine',
    '--overwrite',
    'xray_data.twin_law=-h,k,-l',
    '"%s/test1.mtz"' % d,
    '"%s/test1.pdb"' % d,
    'input.xray_data.high_resolution=1.5',
    'main.number_of_macro_cycles=1',
    'main.simulated_annealing_torsion=True',
    'tardy.number_of_cooling_steps=2',
    'tardy.number_of_time_steps=3',
    'output.prefix=twin_test_01d',
    '>',
    'twin_test_01d.log'])
  run_command(
    command=cmd,
    log_file_name="twin_test_01d_001.log",
    stdout_file_name="twin_test_01d.log",
    show_diff_log_stdout=True,
    verbose=1)
  assert check_r_factors(
    file_name="twin_test_01d_001.pdb",
    r_work_start_ref=0.0170,
    r_free_start_ref=0.0168,
    r_work_final_ref=0.0245,
    r_free_final_ref=0.0201)



def exercise_06():
  d = libtbx.env.under_dist(
    module_name="phenix_regression",
    path="refinement/twinning",
    test=os.path.isdir)
  cmd = " ".join([
    'phenix.refine',
    '--overwrite xray_data.twin_law=-h,k,-l',
    #'main.target=ls --overwrite', # this is to compare times with non-twin refinement
    '"%s/test1.mtz"' % d,
    '"%s/test1.pdb"' % d,
    'input.xray_data.high_resolution=1.5',
    'main.number_of_macro_cycles=3',
    'strategy=rigid_body',
    'sites.rotate="5 5 5"',
    'rigid_body.min_number_of_reflections=100',
    'output.prefix=twin_test_02',
    'ordered_solvent=true',
    '>',
    'twin_test_02.log'])
  run_command(
    command=cmd,
    log_file_name="twin_test_02_001.log",
    stdout_file_name="twin_test_02.log",
    show_diff_log_stdout=True,
    verbose=1)
  assert check_r_factors(
    file_name="twin_test_02_001.pdb",
    r_work_start_ref=0.3159,
    r_free_start_ref=0.3153,
    r_work_final_ref=0.0186,
    r_free_final_ref=0.0215)

def exercise_07():
  d = libtbx.env.under_dist(
    module_name="phenix_regression",
    path="refinement/twinning",
    test=os.path.isdir)
  cmd = " ".join([
    'phenix.refine',
    '--overwrite xray_data.twin_law=h,-k,-l',
    '"%s/test2.mtz"' % d,
    '"%s/test2.pdb"' % d,
    'output.prefix=twin_test_03',
    'ordered_solvent=true',
    '>',
    'twin_test_03.log'])
  run_command(
    command=cmd,
    log_file_name="twin_test_03_001.log",
    stdout_file_name="twin_test_03.log",
    show_diff_log_stdout=True,
    verbose=1)

def exercise_08():
  d = libtbx.env.under_dist(
    module_name="phenix_regression",
    path="refinement/twinning",
    test=os.path.isdir)
  cmd = " ".join([
    'phenix.refine',
    '--overwrite xray_data.twin_law=h,-k,-l',
    '"%s/test2.mtz"' % d,
    '"%s/test2.pdb"' % d,
    'main.number_of_mac=1',
    'output.prefix=twin_test_03b',
    'simulated_annealing=True',
    'detwin.map_types.fofc=gradient',
    'ordered_solvent=true',
    '>',
    'twin_test_03b.log'])
  print(cmd)
  run_command(
    command=cmd,
    log_file_name="twin_test_03b_001.log",
    stdout_file_name="twin_test_03b.log",
    show_diff_log_stdout=True,
    verbose=1)




def exercise_09():
  d = libtbx.env.under_dist(
    module_name="phenix_regression",
    path="refinement/twinning",
    test=os.path.isdir)
  cmd = " ".join([
    'phenix.refine',
    '--overwrite xray_data.twin_law=h,-k,-l',
    '"%s/test3.mtz"' % d,
    '"%s/test3.pdb"' % d,
    'main.number_of_macro_cycles=1',
    'output.prefix=twin_test_04',
    'show_residual_map_peaks_and_holes=False',
    'detwin.mode=proportional', 'aniso_correct=True',
    'ordered_solvent=true',
    '>',
    'twin_test_04.log'])
  run_command(
    command=cmd,
    log_file_name="twin_test_04_001.log",
    stdout_file_name="twin_test_04.log",
    show_diff_log_stdout=True,
    verbose=1)
  #
  cmd = " ".join([
    'phenix.xtriage',
    "--skip_sanity_checks",
    '"%s/test3.mtz"'%d, '>',' xtriage.log '])
  sys.stdout.flush()
  easy_run.call(cmd)


def exercise_10():
  d = libtbx.env.under_dist(
    module_name="phenix_regression",
    path="refinement_examples/data",
    test=os.path.isdir)
  cmd = " ".join([
    'phenix.twin_map_utils',
    'data.file_name="%s/porin.cv"' % d,
    'model.file_name="%s/porin.pdb"' % d,
    'max_delta=0.5',
    '>',
    'twin_test_05.log'])
  run_command(
    command=cmd,
    log_file_name="twin_tools.log",
    stdout_file_name="twin_test_05.log",
    show_diff_log_stdout=True,
    verbose=1)


def exercise_11():
  # phenix.xtriage test1.mtz reference.structure.file=test1.pdb
  d = libtbx.env.under_dist(
    module_name="phenix_regression",
    path="refinement_examples/data",
    test=os.path.isdir)
  cmd = " ".join([
    'phenix.xtriage',
    '"%s/porin.cv"' % d,
    'reference.structure.file_name="%s/porin.pdb"' % d,
    'log=xtriage_06.log',
#    '--symmetry="%s/porin.pdb"' % d,
    '>',
    'twin_test_06.log'])
  run_command(
    command=cmd,
    log_file_name="xtriage_06.log",
    stdout_file_name="twin_test_06.log",
    show_diff_log_stdout=False,
    verbose=1)

def run_exercise (idx) :
  method_name = "exercise_%02d" % idx
  method = globals().get(method_name)
  print(method_name)
  assert (hasattr(method, "__call__"))
  method()

def run (args) :
  if (len(args) == 1) :
    idx = int(args[0])
    run_exercise(idx)
  else :
    for idx in range(1, 12) :
      run_exercise(idx)

if (__name__ == "__main__"):
  t0 = time.time()
  run(sys.argv[1:])
  print("OK", "time: %8.3f"%(time.time()-t0))
