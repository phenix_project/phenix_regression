from __future__ import division
from __future__ import print_function
import iotbx.pdb
import libtbx.load_env
import os
from phenix_regression.refinement import run_phenix_refine

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Exercise refinement with data in mmCIF format
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/1yjp.pdb",
    test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/1yjp-sf.cif",
    test=os.path.isfile)
  args = [pdb, hkl, "write_geo_file=False", "main.number_of_macro_cycles=0"]
  r = run_phenix_refine(args = args, prefix = prefix, sorry_expected=False, geo_expected=False)
  #

if (__name__ == "__main__"):
  run()
  print("OK")
