from __future__ import division
from __future__ import print_function
import os.path
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel

pdb_str = """
CRYST1   21.000   21.000   21.000  90.00  90.00  90.00 P 1           1
ATOM    834  CA  GLU A  49       5.000  13.856   5.407  0.54  6.76      A    C
ATOM    864  CA  LYS A  50       7.806  11.373   6.226  1.00  6.54      A    C
ANISOU  864  CA  LYS A  50      743    873    869     36    109    -39  A    C
ATOM    882  D  CLYS A  50       6.288  11.575   5.000  0.46  7.03      A    D
ATOM    887  DZ2CLYS A  50       8.285   5.000   6.927  0.50 14.78      A    D
ATOM    884  HZ2BLYS A  50       8.285   5.000   6.927  0.50 14.78      A    H
TER
END
"""

params_str = """
refinement {
  main {
    number_of_macro_cycles = 2
    scattering_table= neutron
  }
  hydrogens {
    refine= individual
  }
  refine {
    adp {
      individual {
        isotropic = name D or element H or (chain A and resseq 49)
        anisotropic = not (name D or element H or (chain A and resseq 49))
      }
    }
  }
}
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Neutron refinement. Non-H aniso. H/D - iso.
  """
  pdb  = "%s.pdb"%prefix
  phil = "%s.params"%prefix
  open(pdb, "w").write(pdb_str)
  open(phil, "w").write(params_str)
  args = [
    pdb,
    "high_res=3",
    "type=real",
    "label=f-obs",
    "r_free=0.1"]
  r = run_fmodel(args = args, prefix = prefix)
  args = [pdb,phil,r.mtz, "strategy=individual_adp"]
  r = run_phenix_refine(args = args, prefix = prefix)

if (__name__ == "__main__"):
  run()
  print("OK")
