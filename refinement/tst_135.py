from __future__ import absolute_import, division, print_function
import libtbx.load_env
import sys, os, time
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel
from libtbx.test_utils import assert_lines_in_file

pdb_str="""
CRYST1   13.540   14.525   13.562  90.00  90.00  90.00 P 1
SCALE1      0.073855  0.000000  0.000000        0.00000
SCALE2      0.000000  0.068847  0.000000        0.00000
SCALE3      0.000000  0.000000  0.073735        0.00000
ATOM      1  N   ILE E   1       7.686   6.898   5.000  1.00 35.78           N
ATOM      2  CA  ILE E   1       6.938   7.679   5.979  1.00 31.75           C
ATOM      3  C   ILE E   1       7.361   9.179   6.202  1.00 35.74           C
ATOM      4  O   ILE E   1       8.540   9.525   6.264  1.00 37.11           O
ATOM      5  CB  ILE E   1       6.762   6.796   7.241  1.00 32.92           C
ATOM      6  CG1 ILE E   1       5.290   6.502   7.460  1.00 37.42           C
ATOM      7  CG2 ILE E   1       7.464   7.221   8.562  1.00 35.82           C
ATOM      8  CD1 ILE E   1       5.000   5.000   7.491  1.00 35.14           C
ATOM      9  H1  ILE E   1       7.224   6.168   4.787  1.00 35.78           H
ATOM     10  H2  ILE E   1       7.820   7.388   4.269  1.00 35.78           H
ATOM     11  H3  ILE E   1       8.471   6.661   5.346  1.00 35.78           H
ATOM     12  HA  ILE E   1       6.058   7.764   5.587  1.00 31.75           H
ATOM     13  HB  ILE E   1       7.162   5.944   7.009  1.00 32.92           H
ATOM     14 HG12 ILE E   1       5.028   6.868   8.319  1.00 37.42           H
ATOM     15 HG13 ILE E   1       4.742   6.890   6.763  1.00 37.42           H
ATOM     16 HG21 ILE E   1       7.264   6.567   9.250  1.00 35.82           H
ATOM     17 HG22 ILE E   1       8.421   7.261   8.411  1.00 35.82           H
ATOM     18 HG23 ILE E   1       7.134   8.093   8.828  1.00 35.82           H
ATOM     19 HD11 ILE E   1       4.052   4.865   7.646  1.00 35.14           H
ATOM     20 HD12 ILE E   1       5.255   4.612   6.639  1.00 35.14           H
ATOM     21 HD13 ILE E   1       5.514   4.594   8.207  1.00 35.14           H1-
TER
END
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Make sure H1- does not cause any problems and it is actually used!
  """
  #
  with open("%s.pdb"%prefix,"w") as fo:
    fo.write(pdb_str)
  #
  args = ["%s.pdb"%prefix, "high_res=2 type=real r_free=0.1"]
  r = run_fmodel(args = args, prefix = prefix)
  #
  args = ["%s.pdb"%prefix, "main.number_of_mac=1", r.mtz]
  r = run_phenix_refine(args = args, prefix = prefix)
  assert_lines_in_file(
    file_name=r.log,
    lines="""
Number of scattering types: 5
  Type Number    sf(0)   Gaussians
   O       1      7.97       2
   N       1      6.97       2
   C       6      5.97       2
   H1-     1      1.99       3
   H      12      1.00       2
  sf(0) = scattering factor at diffraction angle 0."""
    )

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK", "time: %.2f"%(time.time()-t0))
