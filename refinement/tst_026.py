from __future__ import division
from __future__ import print_function
import libtbx.load_env
import os
from phenix_regression.refinement import run_phenix_refine

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Exercise ASU masks.
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/phe.pdb",
    test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/phe.mtz",
    test=os.path.isfile)
  args = [pdb,hkl, "refinement.mask.use_asu_masks=true"]
  r = run_phenix_refine(args = args, prefix = prefix)
  #
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/phe_h.pdb",
    test=os.path.isfile)
  args = [pdb,hkl,
    "refinement.mask.use_asu_masks=true",
    "refinement.mask.n_radial_shells=3",
    "bulk_solvent_and_scale.mode=slow"]
  r = run_phenix_refine(args = args, prefix = prefix)

if (__name__ == "__main__"):
  run()
  print("OK")
