from __future__ import division
from __future__ import print_function
from phenix_regression.refinement import run_phenix_refine
import libtbx.load_env
import os

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Confirm making use of external file.
  """
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/enk.pdb",
    test=os.path.isfile)
  mtz_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/enk.mtz",
    test=os.path.isfile)
  open("%s_extra.phil"%prefix, "w").write(
    "refinement.main.number_of_macro_cycles=1")
  args = [
    pdb_file,
    mtz_file,
    "xray_data.high_resolution=2.5",
    "gui.phil_file=%s_extra.phil"%prefix]
  r = run_phenix_refine(args = args, prefix = prefix)
  found = 0
  for l in open(r.log,"r").readlines():
    l = l.strip()
    if(l.count("Reading parameters from external file:")): found+=1
    if(l.count("%s_extra.phil"%prefix)):       found+=1
    if(l.count("number_of_macro_cycles = 1")): found+=1
  assert found == 6, found

if (__name__ == "__main__") :
  run()
  print("OK")
