from __future__ import division
from __future__ import print_function
import libtbx.load_env
import os
from phenix_regression.refinement import run_phenix_refine
import iotbx.pdb
from libtbx.test_utils import approx_equal

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Keep ANISOU in xyz only refinement.
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/pdb1yjo.ent", test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/1yjo.mtz", \
    test=os.path.isfile)
  args = [hkl,pdb, "output.prefix=tst_keep_aniso_in_xyz",
    "strategy=individual_sites"]
  r = run_phenix_refine(args = args, prefix = prefix)
  n_aniso1=iotbx.pdb.input(file_name=pdb).xray_structure_simple().use_u_aniso()
  n_aniso2=iotbx.pdb.input(file_name=r.pdb).xray_structure_simple().use_u_aniso()
  assert n_aniso1.count(True) == n_aniso1.size()
  assert approx_equal(n_aniso1, n_aniso2)
  assert n_aniso1.size() == 67

if (__name__ == "__main__"):
  run()
  print("OK")
