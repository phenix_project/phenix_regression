from __future__ import division
from __future__ import print_function
from phenix_regression.refinement import run_phenix_refine
import libtbx.load_env
import os

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Test IAS refinement.
  """
  pdb = libtbx.env.find_in_repositories(
       relative_path="phenix_regression/pdb/ygg.pdb", test=os.path.isfile)
  cif = libtbx.env.find_in_repositories(
       relative_path="phenix_regression/pdb/tyr.cif", test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
        relative_path="phenix_regression/reflection_files/ygg.mtz", \
        test=os.path.isfile)
  par = libtbx.env.find_in_repositories(
        relative_path="phenix_regression/refinement/params_ias", \
        test=os.path.isfile)
  args=["cdl=False",pdb,hkl,par,cif,"xray_data.outliers_rejection=false",
    "neutron_data.outliers_rejection=false"]
  r = run_phenix_refine(args = args, prefix = prefix)
  r.check_start_r(r_work=0.0386, r_free=0.0508, eps=0.001)
  r.check_final_r(r_work=0.0159, r_free=0.0217, eps=0.001)

if (__name__ == "__main__"):
  run()
  print("OK")
