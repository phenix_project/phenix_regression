from __future__ import division
from __future__ import print_function
import libtbx.load_env
import os
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel

def run(prefix = os.path.basename(__file__).replace(".py","")):
  pdb_dir = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb", test=os.path.isdir)
  pdb_in = os.path.join(pdb_dir, "phe.pdb")
  args = [
    pdb_in,
    'type=real label=fobs high_res=2',
    'r_free_flags_fraction=0.2']
  r = run_fmodel(args = args, prefix = prefix)
  args = ['strategy=none', "main.number_of_mac=0", pdb_in, r.mtz]
  r = run_phenix_refine(args = args, prefix = prefix)
  fo = open(r.log,"r")
  cntr=0
  for l in fo.readlines():
    if(l.count("(resolution: 2.00 - 15.00 A, n_refl.=895 (all), 20.00 % free)")):
      cntr+=1
  assert cntr>0

if (__name__ == "__main__"):
  run()
  print("OK")
