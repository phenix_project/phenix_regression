from __future__ import division
from __future__ import print_function
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel
from libtbx.test_utils import assert_lines_in_file
import os

pdb_str = """
CRYST1  150.879  150.879  106.439  90.00  90.00  90.00 P 42 21 2    16
ATOM   6757  N   VAL B 353       2.767  -1.524  28.588  1.00  2.00      A    N
ATOM   6758  CA  VAL B 353       1.340  -1.825  28.630  1.00  2.00      A    C
ATOM   6759  CB  VAL B 353       1.110  -3.226  29.240  1.00  2.00      A    C
ATOM   6760  CG1 VAL B 353      -0.370  -3.570  29.247  1.00  2.00      A    C
ATOM   6761  CG2 VAL B 353       1.900  -4.282  28.474  1.00  2.00      A    C
ATOM   6762  C   VAL B 353       0.488  -0.794  29.379  1.00  2.00      A    C
ATOM   6763  O   VAL B 353      -0.474  -0.264  28.821  1.00  2.00      A    O
ATOM   6764  N   TYR B 354       0.833  -0.508  30.631  1.00  2.00      A    N
ATOM   6765  CA  TYR B 354      -0.106   0.159  31.538  1.00  2.00      A    C
ATOM   6766  CB  TYR B 354      -0.030  -0.502  32.916  1.00  2.00      A    C
ATOM   6767  CG  TYR B 354      -0.418  -1.962  32.893  1.00  2.00      A    C
ATOM   6768  CD1 TYR B 354      -1.748  -2.346  32.982  1.00  2.00      A    C
ATOM   6769  CE1 TYR B 354      -2.110  -3.680  32.955  1.00  2.00      A    C
ATOM   6770  CZ  TYR B 354      -1.136  -4.647  32.836  1.00  2.00      A    C
ATOM   6771  OH  TYR B 354      -1.491  -5.977  32.808  1.00  2.00      A    O
ATOM   6772  CE2 TYR B 354       0.193  -4.291  32.743  1.00  2.00      A    C
ATOM   6773  CD2 TYR B 354       0.545  -2.955  32.770  1.00  2.00      A    C
ATOM   6774  C   TYR B 354       0.062   1.671  31.706  1.00  2.00      A    C
ATOM   6775  O   TYR B 354      -0.882   2.359  32.091  1.00  2.00      A    O
ATOM   6776  N   GLY B 355       1.250   2.192  31.427  1.00  2.00      A    N
ATOM   6777  CA  GLY B 355       1.503   3.608  31.618  1.00  2.00      A    C
ATOM   6778  C   GLY B 355       1.281   4.475  30.394  1.00  2.00      A    C
ATOM   6779  O   GLY B 355       1.645   4.098  29.279  1.00  2.00      A    O
END
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Test that refinement with an atom on special position must stop with Sorry.
  """
  pdb_in = "%s.pdb"%prefix
  open(pdb_in, "w").write(pdb_str)
  #
  args = [
    pdb_in,
    "high_res=2",
    "low_res=2.005",
    "type=real",
    "r_free=0.1",
    "label=F-obs"]
  r = run_fmodel(args = args, prefix = prefix)
  #
  args = [
    pdb_in,
    r.mtz,
    "main.number_of_mac=1",
    "strategy=individual_sites",
    "main.nqh_flips=False",
    "main.bulk_solvent_and_scale=False"]
  r = run_phenix_refine(args = args, prefix = prefix, sorry_expected=True)
  #
  assert_lines_in_file(file_name=r.log,
    lines="Sorry: Polymer crosses special position element:")

if (__name__ == "__main__"):
  run()
  print("OK")
