from __future__ import division
from __future__ import print_function
import libtbx.load_env
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel
import os
from libtbx.test_utils import assert_lines_in_file

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Make sure phenix.refine stops if incompatible r-free flags for Bijvoet mates
  are present in input data.
  """
  rp = "phenix_regression/refinement/bad_r_free_bijvoet_mates/%s"
  pdb_file = libtbx.env.find_in_repositories(
    relative_path=rp%"model.pdb", test=os.path.isfile)
  hkl_file = libtbx.env.find_in_repositories(
    relative_path=rp%"data.mtz", test=os.path.isfile)
  args = [
    pdb_file,
    hkl_file,
    "main.number_of_macro_cycles=1",
    "refine.strategy=None",
    "main.bulk_solv=false",
    "write_model_cif_file=True",
    "write_reflection_cif_file=True"]
  print(" ".join(args))
  r = run_phenix_refine(args = args, prefix = prefix+"_2", sorry_expected=True)
  assert_lines_in_file(
    file_name=r.log,
    lines="Sorry: cctbx Error: merge_equivalents_exact: incompatible flags for hkl = (1, 3, 9) (mismatch between Friedel mates)")

if (__name__ == "__main__") :
  run()
  print("OK")
