from __future__ import division
from __future__ import print_function
from phenix_regression.refinement import run_phenix_refine
import libtbx.load_env
import os
from libtbx.test_utils import assert_lines_in_file

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Twinning.
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/tst_090.pdb",
    test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/tst_090.mtz",
    test=os.path.isfile)
  base = [
    pdb,
    hkl,
    "main.number_of_mac=0",
    "strategy=none",
    "xray_data.high_res=6"]
  #
  r = run_phenix_refine(args = base+["xray_data.twin_law='%s'"%"h,-k,-l"], prefix=prefix)
  assert_lines_in_file(
    file_name=r.pdb,
    lines="REMARK   3  REFINEMENT TARGET : TWIN_LSQ_F")
  #
  r = run_phenix_refine(args = base, prefix = prefix)
  assert_lines_in_file(
    file_name=r.pdb,
    lines="REMARK   3  REFINEMENT TARGET : ML")

if (__name__ == "__main__") :
  run()
  print("OK")
