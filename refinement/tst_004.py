from __future__ import print_function
from libtbx.test_utils import approx_equal
import libtbx.load_env
import time, os
from phenix_regression.refinement import run_phenix_refine

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Check main.scattering_table functions.
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/pdb1wqz.ent", test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/r1wqzsf.mtz",
    test=os.path.isfile)
  args = [
    '%s'%hkl,
    '%s'%pdb,
    'strategy=none',
    'main.scattering_table=neutron',
    'main.number_of_macro_cycles=1']
  r = run_phenix_refine(args = args, prefix = prefix)
  r.check_final_r(r_work=0.2795, r_free=0.3158, eps=0.005, info_low_eps=0.01)
  r.check_start_r(r_work=0.2795, r_free=0.3170, eps=0.005, info_low_eps=0.01)

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK Time: %8.3f"%(time.time()-t0))
