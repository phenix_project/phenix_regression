from __future__ import division
from __future__ import print_function
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel
import os
import iotbx.pdb
from libtbx import easy_run

pdb_file_str1="""\
CRYST1   14.072   19.284   15.415  90.00  90.00  90.00 P 1
SCALE1      0.071063  0.000000  0.000000        0.00000
SCALE2      0.000000  0.051856  0.000000        0.00000
SCALE3      0.000000  0.000000  0.064872        0.00000
ATOM      1  N  ALEU A 485       8.047   8.883   5.844  0.31 11.00           N
ATOM      2  CA ALEU A 485       7.577   9.844   6.841  0.31 11.00           C
ATOM      3  C  ALEU A 485       7.474   9.287   8.281  0.31 11.00           C
ATOM      4  O  ALEU A 485       8.020   9.917   9.186  0.31 11.00           O
ATOM      5  CB ALEU A 485       6.251  10.495   6.409  0.31 11.00           C
ATOM      6  CG ALEU A 485       6.284  11.550   5.298  0.31 11.00           C
ATOM      7  CD1ALEU A 485       6.474  10.918   3.925  0.31 11.00           C
ATOM      8  CD2ALEU A 485       5.015  12.389   5.329  0.31 11.00           C
ATOM      9  HA ALEU A 485       8.238  10.567   6.877  0.31 11.00           H
ATOM     10  HG ALEU A 485       7.034  12.144   5.455  0.31 11.00           H
ATOM     11  N  BLEU A 485       8.636   9.994   9.628  0.69 33.00           N
ATOM     12  CA BLEU A 485       7.201  10.195   9.794  0.69 33.00           C
ATOM     13  C  BLEU A 485       6.399   9.394   8.771  0.69 33.00           C
ATOM     14  O  BLEU A 485       5.426   9.891   8.204  0.69 33.00           O
ATOM     15  CB BLEU A 485       6.854  11.686   9.696  0.69 33.00           C
ATOM     16  CG BLEU A 485       7.690  12.592   8.784  0.69 33.00           C
ATOM     17  CD1BLEU A 485       7.536  12.221   7.315  0.69 33.00           C
ATOM     18  CD2BLEU A 485       7.313  14.049   9.007  0.69 33.00           C
ATOM     19  HA BLEU A 485       6.941   9.886  10.687  0.69 33.00           H
ATOM     20  HG BLEU A 485       8.626  12.494   9.017  0.69 33.00           H
ATOM     21  N   ALA A 486       6.810   8.150   8.544  1.00  5.00           N
ATOM     22  CA  ALA A 486       6.129   7.276   7.580  1.00  5.00           C
ATOM     23  C   ALA A 486       4.832   6.756   8.187  1.00  5.00           C
ATOM     24  O   ALA A 486       4.392   5.648   7.878  1.00  5.00           O
ATOM     25  CB  ALA A 486       7.029   6.114   7.198  1.00  5.00           C
ATOM     26  HA  ALA A 486       5.886   7.751   6.762  1.00  5.00           H
ATOM     27  H  AALA A 486       6.740   7.846   9.345  0.31 11.00           H
ATOM     28  H  BALA A 486       7.482   7.781   8.935  0.69 33.00           H
TER
END
"""

pdb_file_str2="""\
CRYST1   14.072   19.284   15.415  90.00  90.00  90.00 P 1
SCALE1      0.071063  0.000000  0.000000        0.00000
SCALE2      0.000000  0.051856  0.000000        0.00000
SCALE3      0.000000  0.000000  0.064872        0.00000
ATOM      1  N  ALEU A 485       8.047   8.883   5.844  0.31 11.00           N
ATOM      2  CA ALEU A 485       7.577   9.844   6.841  0.31 11.00           C
ATOM      3  C  ALEU A 485       7.474   9.287   8.281  0.31 11.00           C
ATOM      4  O  ALEU A 485       8.020   9.917   9.186  0.31 11.00           O
ATOM      5  CB ALEU A 485       6.251  10.495   6.409  0.31 11.00           C
ATOM      6  CG ALEU A 485       6.284  11.550   5.298  0.31 11.00           C
ATOM      7  CD1ALEU A 485       6.474  10.918   3.925  0.31 11.00           C
ATOM      8  CD2ALEU A 485       5.015  12.389   5.329  0.31 11.00           C
ATOM      9  HA ALEU A 485       8.238  10.567   6.877  0.31 11.00           H
ATOM     10  HG ALEU A 485       7.034  12.144   5.455  0.31 11.00           H
ATOM     11  N  BLEU A 485       8.636   9.994   9.628  0.69 33.00           N
ATOM     12  CA BLEU A 485       7.201  10.195   9.794  0.69 33.00           C
ATOM     13  C  BLEU A 485       6.399   9.394   8.771  0.69 33.00           C
ATOM     14  O  BLEU A 485       5.426   9.891   8.204  0.69 33.00           O
ATOM     15  CB BLEU A 485       6.854  11.686   9.696  0.69 33.00           C
ATOM     16  CG BLEU A 485       7.690  12.592   8.784  0.69 33.00           C
ATOM     17  CD1BLEU A 485       7.536  12.221   7.315  0.69 33.00           C
ATOM     18  CD2BLEU A 485       7.313  14.049   9.007  0.69 33.00           C
ATOM     19  HA BLEU A 485       6.941   9.886  10.687  0.69 33.00           H
ATOM     20  HG BLEU A 485       8.626  12.494   9.017  0.69 33.00           H
ATOM     21  N   ALA A 486       6.810   8.150   8.544  1.00  5.00           N
ATOM     22  CA  ALA A 486       6.129   7.276   7.580  1.00  5.00           C
ATOM     23  C   ALA A 486       4.832   6.756   8.187  1.00  5.00           C
ATOM     24  O   ALA A 486       4.392   5.648   7.878  1.00  5.00           O
ATOM     25  CB  ALA A 486       7.029   6.114   7.198  1.00  5.00           C
ATOM     26  HA  ALA A 486       5.886   7.751   6.762  1.00  5.00           H
ATOM     27  H  AALA A 486       6.740   7.846   9.345  0.31 11.00           H
TER
END
"""

pdb_file_str3="""\
CRYST1   14.072   19.284   15.415  90.00  90.00  90.00 P 1
SCALE1      0.071063  0.000000  0.000000        0.00000
SCALE2      0.000000  0.051856  0.000000        0.00000
SCALE3      0.000000  0.000000  0.064872        0.00000
ATOM      1  N  ALEU A 485       8.047   8.883   5.844  0.31 11.00           N
ATOM      2  CA ALEU A 485       7.577   9.844   6.841  0.31 11.00           C
ATOM      3  C  ALEU A 485       7.474   9.287   8.281  0.31 11.00           C
ATOM      4  O  ALEU A 485       8.020   9.917   9.186  0.31 11.00           O
ATOM      5  CB ALEU A 485       6.251  10.495   6.409  0.31 11.00           C
ATOM      6  CG ALEU A 485       6.284  11.550   5.298  0.31 11.00           C
ATOM      7  CD1ALEU A 485       6.474  10.918   3.925  0.31 11.00           C
ATOM      8  CD2ALEU A 485       5.015  12.389   5.329  0.31 11.00           C
ATOM      9  HA ALEU A 485       8.238  10.567   6.877  0.31 11.00           H
ATOM     10  HG ALEU A 485       7.034  12.144   5.455  0.31 11.00           H
ATOM     11  N  BLEU A 485       8.636   9.994   9.628  0.69 33.00           N
ATOM     12  CA BLEU A 485       7.201  10.195   9.794  0.69 33.00           C
ATOM     13  C  BLEU A 485       6.399   9.394   8.771  0.69 33.00           C
ATOM     14  O  BLEU A 485       5.426   9.891   8.204  0.69 33.00           O
ATOM     15  CB BLEU A 485       6.854  11.686   9.696  0.69 33.00           C
ATOM     16  CG BLEU A 485       7.690  12.592   8.784  0.69 33.00           C
ATOM     17  CD1BLEU A 485       7.536  12.221   7.315  0.69 33.00           C
ATOM     18  CD2BLEU A 485       7.313  14.049   9.007  0.69 33.00           C
ATOM     19  HA BLEU A 485       6.941   9.886  10.687  0.69 33.00           H
ATOM     20  HG BLEU A 485       8.626  12.494   9.017  0.69 33.00           H
ATOM     21  N   ALA A 486       6.810   8.150   8.544  1.00  5.00           N
ATOM     22  CA  ALA A 486       6.129   7.276   7.580  1.00  5.00           C
ATOM     23  C   ALA A 486       4.832   6.756   8.187  1.00  5.00           C
ATOM     24  O   ALA A 486       4.392   5.648   7.878  1.00  5.00           O
ATOM     25  CB  ALA A 486       7.029   6.114   7.198  1.00  5.00           C
ATOM     26  HA  ALA A 486       5.886   7.751   6.762  1.00  5.00           H
ATOM     28  H  BALA A 486       7.482   7.781   8.935  0.69 33.00           H
TER
END
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Occupancy of H in refinement.
  """
  for pdb_file_str in [pdb_file_str1, pdb_file_str2, pdb_file_str3]:
    # Good input model
    pdb_file_name = "%s.pdb"%prefix
    open(pdb_file_name,"w").write(pdb_file_str)
    # compute Fobs
    args = [
      pdb_file_name,
      "high_res=3.5",
      "r_fre=0.1",
      "type=real",
      "label=f-obs"]
    r = run_fmodel(args = args, prefix = prefix)
    # shake occupancies
    pdb_file_name_poor = "%s_poor.pdb"%prefix
    cmd = " ".join([
      "phenix.pdbtools %s"%pdb_file_name,
      "suffix=none",
      "output.prefix=%s"%pdb_file_name_poor.replace(".pdb",""),
      "occupancies.randomize=true",
      "modify.selection='altloc A or altloc B'",
      "> %s_pdbtools.log"%prefix
      ])
    assert not easy_run.call(cmd)
    # run phenix.refine
    args = [
      pdb_file_name_poor, r.mtz,
      "strategy=occupancies",
      "main.number_of_macro_cycles=2"]
    r = run_phenix_refine(args = args, prefix=prefix)
    # check results
    p=iotbx.pdb.input(file_name = r.pdb)
    h=p.construct_hierarchy()
    x = p.xray_structure_simple()
    o = x.scatterers().extract_occupancies()
    sel_a = h.atom_selection_cache().selection(string = "altloc A")
    sel_b = h.atom_selection_cache().selection(string = "altloc B")
    sel   = h.atom_selection_cache().selection(string = "not (altloc A or altloc B)")
    for s in [sel, sel_a, sel_b]:
      os = o.select(s)
      assert os.all_eq(os[0])

if (__name__ == "__main__"):
  run()
  print("OK")
