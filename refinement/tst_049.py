from __future__ import division
from __future__ import print_function
from phenix_regression.refinement import run_phenix_refine
import libtbx.load_env
import os
from libtbx.test_utils import approx_equal

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Exercise consecutive runs making sure R at end of previous run matches start R
  of the next run.
  """
  pdb = libtbx.env.find_in_repositories(
        relative_path="phenix_regression/pdb/LWmodel.pdb", test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
        relative_path="phenix_regression/reflection_files/LWdata.mtz",
        test=os.path.isfile)
  base = [
    hkl,
    "main.number_of_macro_cycles=1",
    "main.bulk_sol=False",
    "main.random_seed=9754365",
    "xray_data.outliers_rejection=false",
    "neutron_data.outliers_rejection=false"]
  #
  args1 = base + [pdb,"strategy=individual_adp"]
  r1 = run_phenix_refine(args = args1, prefix = "%s_1"%prefix)
  #
  args2 = base + ["%s_1_001.pdb"%prefix, "strategy=tls adp.tls='chain A' adp.tls='chain B'"]
  r2 = run_phenix_refine(args = args2, prefix = "%s_2"%prefix)
  #
  args3 = base + ["%s_2_001.pdb"%prefix, "strategy=tls adp.tls='chain A' adp.tls='chain B'"]
  r3 = run_phenix_refine(args = args3, prefix = "%s_3"%prefix)
  #
  args4 = base + ["%s_3_001.pdb"%prefix, "strategy=individual_sites"]
  r4 = run_phenix_refine(args = args4, prefix = "%s_4"%prefix)
  #
  eps = 0.002
  assert approx_equal(r1.r_work_final, r2.r_work_start, eps)
  assert approx_equal(r1.r_free_final, r2.r_free_start, eps)
  #
  assert approx_equal(r2.r_work_final, r3.r_work_start, eps)
  assert approx_equal(r2.r_free_final, r3.r_free_start, eps)
  #
  assert approx_equal(r3.r_work_final, r4.r_work_start, eps)
  assert approx_equal(r3.r_free_final, r4.r_free_start, eps)

if (__name__ == "__main__"):
  run()
  print("OK")
