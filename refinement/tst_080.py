from __future__ import division
from __future__ import print_function
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel
import os
import libtbx.load_env
from libtbx import easy_run
from libtbx.test_utils import approx_equal

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Here we check B-factors of one atom.
  This is to make sure Biso and Baniso outputted fully
  (not partly as stored in xray_structure)
  """
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/1yjp_h.pdb",
    test=os.path.isfile)
  mtz_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/1yjp.mtz",
    test=os.path.isfile)
  args = [pdb_file,
          mtz_file,
          "main.number_of_macro_cycles=1",
          "refine.strategy=individual_sites+individual_adp+tls",
          "tls.find_automatically=True"]
  r = run_phenix_refine(args = args, prefix = prefix)
  #
  assert not easy_run.call("phenix.pdbtools convert_to_iso=true %s_001.pdb"%prefix)
  #
  def get_atom_recs(fn):
    result = []
    with open(fn, "r") as f1:
       for l in f1.readlines():
         l = l.strip()
         if(l.startswith("ATOM")): result.append(l)
    return result
  ar1 = get_atom_recs(fn = "%s_001.pdb"%prefix)
  ar2 = get_atom_recs(fn = "%s_001_modified.pdb"%prefix)
  assert len(ar1) == len(ar2)
  for l1, l2 in zip(ar1, ar2):
    l1 = float(l1.split()[10])
    l2 = float(l2.split()[10])
    assert approx_equal(l1, l2, 0.02)

if (__name__ == "__main__") :
  run()
  print("OK")
