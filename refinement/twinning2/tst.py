from __future__ import print_function
from libtbx.test_utils import run_command
import libtbx.load_env
import time
import sys, os
from libtbx.test_utils import approx_equal
from libtbx import easy_run


def exercise_00():
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/twinning2/pdb3loz.ent",
    test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/twinning2/r3lozsf_new_test_flags.mtz",
    test=os.path.isfile)
  cmd_ml = " ".join([
    'phenix.refine',
    '%s'%hkl,
    '%s'%pdb,
    'output.prefix=twinning2_ml',
    'main.number_of_mac=1',
    'strategy=none',
    "optimize_mask=false",
    "xray_data.high_res=2",
    '--overwrite --quiet > tst_ml'])
  cmd_twin = " ".join([
    'phenix.refine',
    '%s'%hkl,
    '%s'%pdb,
    'output.prefix=twinning2_twin',
    'main.number_of_mac=1',
    'strategy=none',
    "optimize_mask=false",
    "xray_data.high_res=2",
    "fmodel.xray_data.twin_law='%s'"%"h,-k,-l",
    '--overwrite --quiet > tst_twin'])
  cmd_twin_after_ml = " ".join([
    'phenix.refine',
    '%s'%hkl,
    '%s'%"twinning2_ml_001.pdb",
    'output.prefix=twinning2_twin_after_ml',
    'main.number_of_mac=1',
    'strategy=none',
    "optimize_mask=false",
    "xray_data.high_res=2",
    "fmodel.xray_data.twin_law='%s'"%"h,-k,-l",
    '--overwrite --quiet > tst_twin_after_ml'])
  cmd_def_after_twin = " ".join([
    'phenix.refine',
    '%s'%hkl,
    '%s'%"twinning2_twin_001.pdb",
    'output.prefix=twinning2_def_after_twin',
    'main.number_of_mac=1',
    'strategy=none',
    "optimize_mask=false",
    "xray_data.high_res=2",
    '--overwrite --quiet > tst_def_after_twin'])
  #
  for cmd in [cmd_ml, cmd_twin, cmd_twin_after_ml, cmd_def_after_twin]:
    assert not easy_run.call(cmd)
  #
  r = easy_run.go(command=
    "grep 'REMARK   3  REFINEMENT TARGET :' twinning2_ml_001.pdb").stdout_lines[0].split()
  assert r[5]=="ML"
  r = easy_run.go(command=
    "grep 'REMARK   3  REFINEMENT TARGET :' twinning2_twin_001.pdb").stdout_lines[0].split()
  assert r[5]=="TWIN_LSQ_F"
  r = easy_run.go(command=
    "grep 'REMARK   3  REFINEMENT TARGET :' twinning2_twin_after_ml_001.pdb").stdout_lines[0].split()
  assert r[5]=="TWIN_LSQ_F"
  r = easy_run.go(command=
    "grep 'REMARK   3  REFINEMENT TARGET :' twinning2_def_after_twin_001.pdb").stdout_lines[0].split()
  assert r[5]=="TWIN_LSQ_F"


if(__name__ == "__main__"):
  t0 = time.time()
  exercise_00()
  print("OK Time: %8.3f"%(time.time()-t0))
