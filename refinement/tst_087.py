from __future__ import division
from __future__ import print_function
import os.path
from phenix_regression.refinement import run_phenix_refine
from iotbx import crystal_symmetry_from_any
from libtbx.test_utils import approx_equal

pdb_str = """
CRYST1   91.680   91.680  241.650  90.00  90.00 120.00 P 61 2 2
ATOM      1  O5' GUA A   1      43.114 -20.622 -15.202  1.00 92.07      A
ATOM      2  C5' GUA A   1      44.116 -19.788 -14.624  1.00 91.54      A
ATOM      3  C4' GUA A   1      45.151 -20.684 -14.008  1.00 91.00      A
ATOM      4  O4' GUA A   1      44.553 -22.001 -13.848  1.00 90.78      A
ATOM      5  C1' GUA A   1      45.573 -22.998 -13.901  1.00 90.40      A
ATOM      6  N9  GUA A   1      45.228 -24.011 -14.906  1.00 87.97      A
ATOM      7  C4  GUA A   1      44.107 -24.052 -15.700  1.00 85.53      A
ATOM      8  N3  GUA A   1      43.113 -23.144 -15.709  1.00 84.87      A
ATOM      9  C2  GUA A   1      42.174 -23.456 -16.588  1.00 87.24      A
ATOM     10  N2  GUA A   1      41.097 -22.664 -16.731  1.00 88.99      A
ATOM     11  N1  GUA A   1      42.216 -24.571 -17.393  1.00 87.13      A
ATOM     12  C6  GUA A   1      43.233 -25.520 -17.391  1.00 85.77      A
ATOM     13  O6  GUA A   1      43.173 -26.490 -18.147  1.00 85.30      A
ATOM     14  C5  GUA A   1      44.238 -25.201 -16.461  1.00 84.89      A
ATOM     15  N7  GUA A   1      45.410 -25.870 -16.153  1.00 84.36      A
ATOM     16  C8  GUA A   1      45.968 -25.131 -15.231  1.00 87.00      A
ATOM     17  C2' GUA A   1      46.880 -22.260 -14.204  1.00 91.10      A
ATOM     18  O2' GUA A   1      47.561 -21.959 -13.001  1.00 90.87      A
ATOM     19  C3' GUA A   1      46.350 -21.004 -14.879  1.00 91.81      A
ATOM     20  O3' GUA A   1      47.318 -19.970 -14.901  1.00 91.92      A
"""

params_str = """
refinement.crystal_symmetry {
  unit_cell = 91.68 91.68 241.65 90 90 120
  space_group = "P 31 2 1"
}
"""

hkl_str = """
NREFlections=30
ANOMalous=TRUE
DECLare NAME=FOBS DOMAin=RECIprocal TYPE=REAL END
DECLare NAME=SIGMA DOMAin=RECIprocal TYPE=REAL END
DECLare NAME=TEST DOMAin=RECIprocal TYPE=INTE END
INDEx 1 0 5 FOBS= 374.4 SIGMA= 9.73 TEST= 0
INDEx 1 0 6 FOBS= 485.5 SIGMA= 9.05 TEST= 0
INDEx 1 0 7 FOBS= 1566.7 SIGMA= 36.26 TEST= 0
INDEx 1 0 9 FOBS= 123.2 SIGMA= 4.62 TEST= 0
INDEx 1 0 11 FOBS= 290.4 SIGMA= 3.42 TEST= 1
INDEx 1 0 12 FOBS= 1631.3 SIGMA= 14.88 TEST= 0
INDEx 1 0 13 FOBS= 159.8 SIGMA= 2.35 TEST= 0
INDEx 1 0 14 FOBS= 189.8 SIGMA= 1.82 TEST= 0
INDEx 1 0 15 FOBS= 700.1 SIGMA= 5.49 TEST= 0
INDEx 1 0 16 FOBS= 2280.8 SIGMA= 26.05 TEST= 0
INDEx 1 0 17 FOBS= 100.8 SIGMA= 2.52 TEST= 0
INDEx 1 0 18 FOBS= 147.9 SIGMA= 1.92 TEST= 1
INDEx 1 0 19 FOBS= 58.4 SIGMA= 3.68 TEST= 0
INDEx 1 0 20 FOBS= 1149 SIGMA= 8.75 TEST= 0
INDEx 1 0 21 FOBS= 776.4 SIGMA= 6.11 TEST= 0
INDEx 1 0 22 FOBS= 186.9 SIGMA= 1.95 TEST= 0
INDEx 1 0 23 FOBS= 699.2 SIGMA= 6.93 TEST= 0
INDEx 1 1 4 FOBS= 1051.8 SIGMA= 11.17 TEST= 1
INDEx 1 1 5 FOBS= 1714 SIGMA= 16.14 TEST= 0
INDEx 1 1 7 FOBS= 1253.8 SIGMA= 12.6 TEST= 1
INDEx 1 1 8 FOBS= 2154.4 SIGMA= 22.19 TEST= 0
INDEx 1 1 11 FOBS= 859.8 SIGMA= 6.91 TEST= 0
INDEx 1 1 12 FOBS= 1212.7 SIGMA= 8.97 TEST= 1
INDEx 1 1 13 FOBS= 1031.2 SIGMA= 8.22 TEST= 0
INDEx 1 1 14 FOBS= 932.7 SIGMA= 6.77 TEST= 0
INDEx 1 1 15 FOBS= 1441 SIGMA= 10.22 TEST= 0
INDEx 1 1 16 FOBS= 2730.6 SIGMA= 39.66 TEST= 0
INDEx 1 1 17 FOBS= 1098 SIGMA= 7.75 TEST= 0
INDEx 1 1 18 FOBS= 2097 SIGMA= 13.54 TEST= 0
INDEx 1 1 19 FOBS= 921.2 SIGMA= 6.18 TEST= 0
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Extract symmetry.
  """
  pdb  = "%s.pdb"%prefix
  phil = "%s.params"%prefix
  hkl  = "%s.cns"%prefix
  open(pdb, "w").write(pdb_str)
  open(phil, "w").write(params_str)
  open(hkl, "w").write(hkl_str)
  r = run_phenix_refine(args = [pdb, hkl, phil], prefix = prefix)
  #
  cs1 = crystal_symmetry_from_any.extract_from(r.pdb)
  cs2 = crystal_symmetry_from_any.extract_from(r.mtz)
  assert cs1.is_similar_symmetry(cs2)
  assert cs1.space_group().type().lookup_symbol()=="P 31 2 1"
  assert cs1.space_group().type().number()==152
  assert approx_equal(cs1.unit_cell().parameters(),
    (91.68, 91.68, 241.65, 90.0, 90.0, 120.0))

if (__name__ == "__main__"):
  run()
  print("OK")
