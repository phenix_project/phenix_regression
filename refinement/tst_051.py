from __future__ import division
from __future__ import print_function
from phenix_regression.refinement import run_phenix_refine
import libtbx.load_env
import os

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Make sure some other strategies play well with IAS
  """
  pdb = libtbx.env.find_in_repositories(
       relative_path="phenix_regression/pdb/ygg.pdb", test=os.path.isfile)
  cif = libtbx.env.find_in_repositories(
       relative_path="phenix_regression/pdb/tyr.cif", test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
        relative_path="phenix_regression/reflection_files/ygg.mtz", \
        test=os.path.isfile)
  par = libtbx.env.find_in_repositories(
        relative_path="phenix_regression/refinement/params_ias1", \
        test=os.path.isfile)
  args = [pdb,hkl,par,cif]
  for st in ["individual_adp", "individual_sites", "occupancies", "none"]:
    r = run_phenix_refine(args = args+["strategy=%s"%st], prefix = prefix)

if (__name__ == "__main__"):
  run()
  print("OK")
