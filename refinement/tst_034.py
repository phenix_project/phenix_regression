from __future__ import division
from __future__ import print_function
import libtbx.load_env
import os
from phenix_regression.refinement import run_phenix_refine

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Apply back biso.
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/tgg.pdb", test=os.path.isfile)
  par = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/params_tst_refinement_apply_back_biso",
    test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/ygg.mtz",
    test=os.path.isfile)
  cif = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/elbow.TYU.tgg_pdb.001.all.cif",
    test=os.path.isfile)
  args = [pdb,hkl,cif,par]
  r = run_phenix_refine(args = args, prefix = prefix)
  r.check_final_r(r_work=0.0495, r_free=0.0495, eps=0.0003)
  r.check_start_r(r_work=0.07,   r_free=0.07,   eps=0.015)

if (__name__ == "__main__"):
  run()
  print("OK")
