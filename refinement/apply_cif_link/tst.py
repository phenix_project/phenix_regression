from __future__ import print_function
from libtbx.test_utils import run_command
from libtbx.str_utils import show_string
import libtbx.load_env
import sys, os

def expected_as_blocks(expected, block_size):
  result = []
  lines = expected.splitlines()
  assert len(lines) % block_size == 0
  for i in range(0, len(lines), block_size):
    result.append("\n".join(lines[i:i+block_size]))
  return result

def check_geo(geo, expected_bonds, expected_angles):
  print("Checking %s ..." % geo)
  assert os.path.exists(geo)
  txt = "\n".join(open(geo).read().splitlines())
  for expected,block_size in [(expected_bonds, 2), (expected_angles, 3)]:
    for block in expected_as_blocks(expected=expected, block_size=block_size):
      if (txt.find(block) < 0):
        print("Expected block not found:")
        print(block)
        raise AssertionError

def exercise_nga_thr_with_segid():
  d = libtbx.env.under_dist(
    module_name="phenix_regression",
    path="refinement/apply_cif_link",
    test=os.path.isdir)
  cmd = " ".join([
    'phenix.refine',
    'dry_run=true',
    'overwrite=true',
    '"%s/1ge6.mtz"' % d, # doesn't match the model, but it doesn't matter
    '"%s/nga_thr_with_segid.pdb"' % d,
    '"%s/data_link_NGA-THR.cif"' % d,
    '"%s/geo_edits_nga_thr"' % d,
    '>',
    'tmp_apply_cif_link.log'])
  print(cmd)
  sys.stdout.flush()
  geo = "nga_thr_with_segid_refine_001.geo"
  if (1):
    run_command(
      command=cmd,
      log_file_name="nga_thr_with_segid_refine_001.log",
      stdout_file_name="tmp_apply_cif_link.log",
      result_file_names=[geo],
      show_diff_log_stdout=True)
  check_geo(
    geo=geo,
    expected_bonds="""\
bond pdb=" OG1 THR A 168 " segid="A   "
     pdb=" C1  NGA A 900 " segid="A   "
""",
    expected_angles="""\
angle pdb=" CB  THR A 168 " segid="A   "
      pdb=" OG1 THR A 168 " segid="A   "
      pdb=" C1  NGA A 900 " segid="A   "
angle pdb=" OG1 THR A 168 " segid="A   "
      pdb=" C1  NGA A 900 " segid="A   "
      pdb=" O5  NGA A 900 " segid="A   "
""")

def exercise_nga_thr_with_segid_altloc():
  d = libtbx.env.under_dist(
    module_name="phenix_regression",
    path="refinement/apply_cif_link",
    test=os.path.isdir)
  cmd = " ".join([
    'phenix.refine',
    'dry_run=true',
    'overwrite=true',
    '"%s/1ge6.mtz"' % d, # doesn't match the model, but it doesn't matter
    '"%s/nga_thr_with_segid_altloc.pdb"' % d,
    '"%s/data_link_NGA-THR.cif"' % d,
    '"%s/geo_edits_nga_thr_altloc"' % d,
    '>',
    'tmp_apply_cif_link.log'])
  print(cmd)
  sys.stdout.flush()
  geo = "nga_thr_with_segid_altloc_refine_001.geo"
  if (1):
    run_command(
      command=cmd,
      log_file_name="nga_thr_with_segid_altloc_refine_001.log",
      stdout_file_name="tmp_apply_cif_link.log",
      result_file_names=[geo],
      show_diff_log_stdout=True)
  # XXX TODO link from blank altloc to A but not B doesn't work
  # this test preserved for future work on fix

def exercise_1ge6():
  d = libtbx.env.under_dist(
    module_name="phenix_regression",
    path="refinement/apply_cif_link",
    test=os.path.isdir)
  cmd = " ".join([
    'phenix.refine',
    'dry_run=true',
    'overwrite=true',
    '"%s/1ge6.mtz"' % d,
    '"%s/pdb1ge6.ent"' % d,
    '"%s/geo_edits"' % d,
    '>',
    'tmp_apply_cif_link.log'])
  print(cmd)
  sys.stdout.flush()
  geo = "pdb1ge6_refine_001.geo"
  if (1):
    run_command(
      command=cmd,
      log_file_name="pdb1ge6_refine_001.log",
      stdout_file_name="tmp_apply_cif_link.log",
      result_file_names=[geo],
      show_diff_log_stdout=True)
  check_geo(
    geo=geo,
    expected_bonds="""\
bond pdb=" OG1 THR A  42 "
     pdb=" C1  MAN A 900 "
bond pdb=" NE2 HIS A 117 "
     pdb="ZN    ZN A 200 "
bond pdb=" NE2 HIS A 121 "
     pdb="ZN    ZN A 200 "
bond pdb=" OD1 ASP A 130 "
     pdb="ZN    ZN A 200 "
""",
    expected_angles="""\
angle pdb=" NE2 HIS A 117 "
      pdb="ZN    ZN A 200 "
      pdb=" NE2 HIS A 121 "
angle pdb=" NE2 HIS A 117 "
      pdb="ZN    ZN A 200 "
      pdb=" OD1 ASP A 130 "
angle pdb=" NE2 HIS A 121 "
      pdb="ZN    ZN A 200 "
      pdb=" OD1 ASP A 130 "
""")

def exercise_1fzo():
  d = os.environ.get("CCI_REFINE_VETTED")
  if (d is None):
    print("Skipping exercise_1fzo(): CCI_REFINE_VETTED not defined.")
    return
  if (not os.path.isdir(d)):
    raise RuntimeError("CCI_REFINE_VETTED is not a directory: %s"
      % show_string(d))
  cmd = " ".join([
    'phenix.refine',
    'dry_run=true',
    'overwrite=true',
    '"%s/1fzo.mtz"' % d,
    '"%s/1fzo.pdb"' % d,
    '"%s/1fzo_cea.cif"' % d,
    '"%s/1fzo_apply_cif"' % d,
    '>',
    'tmp_apply_cif_link.log'])
  print(cmd)
  sys.stdout.flush()
  geo = "1fzo_refine_001.geo"
  if (1):
    run_command(
      command=cmd,
      log_file_name="1fzo_refine_001.log",
      stdout_file_name="tmp_apply_cif_link.log",
      result_file_names=[geo],
      show_diff_log_stdout=True)
  check_geo(
    geo=geo,
    expected_bonds="""\
bond pdb=" ND2 ASN A  86 "
     pdb=" C1  NAG A 500 "
bond pdb=" ND2 ASN A 176 "
     pdb=" C1  NAG Q 600 "
bond pdb=" O6  NAG Q 600 "
     pdb=" C1  FUC Q 602 "
""",
    expected_angles="""\
angle pdb=" ND2 ASN A  86 "
      pdb=" C1  NAG A 500 "
      pdb=" O5  NAG A 500 "
angle pdb=" CG  ASN A 176 "
      pdb=" ND2 ASN A 176 "
      pdb=" C1  NAG Q 600 "
angle pdb=" O6  NAG Q 600 "
      pdb=" C1  FUC Q 602 "
      pdb=" C2  FUC Q 602 "
""")

def run(args):
  assert len(args) == 0
  exercise_nga_thr_with_segid()
  exercise_nga_thr_with_segid_altloc()
  exercise_1ge6()
  exercise_1fzo()
  print("OK")

if (__name__ == "__main__"):
  run(sys.argv[1:])
