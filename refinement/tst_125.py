# -*- coding: utf-8 -*-
from __future__ import division, print_function
from libtbx import easy_run
import libtbx.load_env
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel
import os

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  f_hydrogen_contribution.
  """
  pdb_file_orig = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/data/f_hydrogen_contribution.pdb",
    test=os.path.isfile)
  text = open(pdb_file_orig).read()
  text = text.replace ("r_free_flags.md5.hexdigest fa0f536f11bfc43b26aea180fea2f24f","")
  pdb_file = 'f_hydrogen_contribution.pdb'
  f=open(pdb_file,'w')
  print(text, file = f)
  f.close()

  args = [
    pdb_file,
    "high_res=2.0",
    "label=f-obs",
    "type=real",
    "r_free=0.1",
    "random_seed=2679941"]
  r0 = run_fmodel(args = args, prefix = prefix)
  args = [
    "cdl=False",
    "%s" %(pdb_file),
    "%s" %(r0.mtz),
    "strategy=individual_sites+individual_adp+occupancies",
    "nqh_flips=false",
    "main.number_of_mac=2",
    "hydrogens.refine=individual",
    "add_angle_and_dihedral_restraints_for_disulfides=False"]
  for i in range(100000000): i = i*i # to avoid routine t96 crash on anaconda
  r = run_phenix_refine(args = args, prefix = prefix)
  assert r.r_work_final < 0.009, r.r_work_final
  assert r.r_free_final < 0.009, r.r_free_final

if (__name__ == "__main__"):
  run()
