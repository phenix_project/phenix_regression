from __future__ import division
from __future__ import print_function
import libtbx.load_env
import os
from phenix_regression.refinement import run_phenix_refine

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  TLS, SA, rigid-body, real data: make sure all play together.
  """
  hkl = libtbx.env.under_dist(
    module_name="phenix_regression",
    path="reflection_files",
    test=os.path.isdir)
  pdb = libtbx.env.under_dist(
    module_name="phenix_regression",
    path="pdb",
    test=os.path.isdir)
  args = [
    hkl+"/1akg.mtz",
    pdb+"/1akg.pdb",
    'simulated_annealing=true',
    'main.number_of_mac=10',
    'ordered_solvent=true',
    'strategy=individual_sites+individual_adp+rigid_body+tls+occupancies',
    'xray_data.high_resolution=1.3',
    'simulated_annealing.mode=first_half',
    'main.random_seed=7112384',
    'add_angle_and_dihedral_restraints_for_disulfides=False']
  print(" ".join(args))
  r = run_phenix_refine(args = args, prefix = prefix)
  r.check_final_r(r_work=0.1469, r_free=0.1523, eps=0.015)
  r.check_start_r(r_work=0.5648, r_free=0.5604, eps=0.015)


if (__name__ == "__main__"):
  run()
  print("OK")
