from __future__ import print_function
import time, os
import iotbx.pdb
from scitbx.array_family import flex
import libtbx.load_env
from phenix_regression.refinement import run_phenix_refine

pdb_str = """
CRYST1   44.308   88.063   88.778  90.00 104.44  90.00 P 1 21 1                 
SCALE1      0.022569  0.000000  0.005812        0.00000                         
SCALE2      0.000000  0.011356  0.000000        0.00000                         
SCALE3      0.000000  0.000000  0.011632        0.00000                         
ATOM      1  N   ALA A  27      17.369 -23.031  10.907  1.00 49.20      A    N  
ANISOU    1  N   ALA A  27     6653   6474   5568   -406    332   -882  A    N  
ATOM      2  CA  ALA A  27      16.836 -21.674  10.953  1.00 48.75      A    C  
ANISOU    2  CA  ALA A  27     6537   6481   5504   -437    360   -820  A    C  
ATOM      3  C   ALA A  27      17.422 -20.904  12.132  1.00 49.49      A    C  
ANISOU    3  C   ALA A  27     6594   6575   5634   -398    376   -791  A    C  
ATOM      4  O   ALA A  27      17.096 -21.176  13.288  1.00 48.77      A    O  
ANISOU    4  O   ALA A  27     6520   6438   5571   -397    356   -759  A    O  
ATOM      5  CB  ALA A  27      15.318 -21.705  11.041  1.00 45.08      A    C  
ANISOU    5  CB  ALA A  27     6085   6015   5030   -506    339   -766  A    C  
ATOM      6  HA  ALA A  27      17.079 -21.210  10.137  1.00 58.50      A    H  
ATOM      7  HB1 ALA A  27      14.985 -20.795  11.070  1.00 54.10      A    H  
ATOM      8  HB2 ALA A  27      14.966 -22.162  10.262  1.00 54.10      A    H  
ATOM      9  HB3 ALA A  27      15.058 -22.178  11.847  1.00 54.10      A    H  
END

"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Make sure that choosing non-anomalous data and anomalous free-r flags does not
  cause the program crash.
  """
  #
  pdb = "%s.pdb"%prefix
  with open(pdb, "w") as fo:
    fo.write(pdb_str)
  #
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/data/%s.mtz"%prefix,
    test=os.path.isfile)
  #
  args = [
    pdb,
    hkl,
    "main.number_of_mac=0",
    "miller_array.labels.name=IMEAN"
    ]
  r = run_phenix_refine(args = args, prefix = prefix)

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
