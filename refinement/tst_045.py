from __future__ import division
from __future__ import print_function
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel
import libtbx.load_env
import os, random
import iotbx.pdb
from scitbx.array_family import flex
from libtbx.test_utils import approx_equal

if (1):
  random.seed(0)
  flex.set_random_seed(0)

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Exercise occupancy refinement.
  """
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/phe_many_altlocs_good.pdb",
    test=os.path.isfile)
  pdb_file_to_refine = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/phe_many_altlocs_bad.pdb",
    test=os.path.isfile)
  args = [
    pdb_file,
    'high_resolution=1.5',
    'algorithm=direct',
    'label=FOBS',
    'type=real']
  r = run_fmodel(args = args, prefix = prefix)
  args = [
    pdb_file_to_refine,
    r.mtz,
    'main.number_of_macro_cycles=3',
    'strategy=occupancies',
    'main.occupancy_max=100','main.occupancy_min=-100',
    'main.target=ls',
    'main.bulk_solvent_and_=false',
    'ls_target_names.target_name=ls_wunit_kunit',
    'structure_factors_and_gradients_accuracy.algorithm=direct',
    'xray_data.r_free_flags.generate=True',
    'xray_data.r_free_flags.ignore_r_free_flags=true',
    "xray_data.outliers_rejection=false",
    'pdb_interpretation.flip_symmetric_amino_acids=None',
    "neutron_data.outliers_rejection=false"]
  r = run_phenix_refine(args = args, prefix = prefix)
  r.check_start_r(r_work=0.3, r_free=0.3, eps=0.015)
  r.check_final_r(r_work=0.0, r_free=0.0, eps=1.e-4)
  occ1 = iotbx.pdb.input(file_name = pdb_file).xray_structure_simple(
    ).scatterers().extract_occupancies()
  occ2 = iotbx.pdb.input(file_name = r.pdb
    ).xray_structure_simple().scatterers().extract_occupancies()
  assert approx_equal(occ1, occ2)

if (__name__ == "__main__"):
  run()
  print("OK")
