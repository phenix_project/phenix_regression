from __future__ import print_function
import time, os
from libtbx.test_utils import approx_equal
from libtbx import easy_run
import iotbx.pdb
import mmtbx.utils
from scitbx.array_family import flex
from phenix_regression.refinement import run_phenix_refine

pdb_str = """
CRYST1   14.410   19.577   15.049  90.00  90.00  90.00 P 1
ATOM      1  P  A  G G   1       9.244  16.136   3.611  0.44 50.00           P
ATOM      2  OP1A  G G   1      10.214  16.036   4.731  0.44 50.00           O
ATOM      3  OP2A  G G   1       9.744  16.199   2.214  0.44 50.00           O
ATOM      4  O5'A  G G   1       8.240  14.904   3.729  0.44 50.00           O
ATOM      5  C5'A  G G   1       6.937  14.965   3.168  0.44 50.00           C
ATOM      6  C4'A  G G   1       6.348  13.589   2.984  0.44 50.00           C
ATOM      7  O4'A  G G   1       7.207  12.799   2.119  0.44 50.00           O
ATOM      8  C3'A  G G   1       6.213  12.744   4.242  0.44 50.00           C
ATOM      9  O3'A  G G   1       5.064  13.064   5.006  0.44 50.00           O
ATOM     10  C2'A  G G   1       6.203  11.327   3.687  0.44 50.00           C
ATOM     11  O2'A  G G   1       4.928  10.994   3.158  0.44 50.00           O
ATOM     12  C1'A  G G   1       7.199  11.449   2.535  0.44 50.00           C
ATOM     13  N9 A  G G   1       8.570  11.074   2.931  0.44 50.00           N
ATOM     14  C8 A  G G   1       9.589  11.936   3.263  0.44 50.00           C
ATOM     15  N7 A  G G   1      10.701  11.328   3.564  0.44 50.00           N
ATOM     16  C5 A  G G   1      10.400   9.982   3.419  0.44 50.00           C
ATOM     17  C4 A  G G   1       9.091   9.802   3.029  0.44 50.00           C
ATOM     18  N1 A  G G   1      10.531   7.664   3.360  0.44 50.00           N
ATOM     19  C2 A  G G   1       9.215   7.588   2.972  0.44 50.00           C
ATOM     20  N3 A  G G   1       8.438   8.644   2.788  0.44 50.00           N
ATOM     21  C6 A  G G   1      11.222   8.846   3.611  0.44 50.00           C
ATOM     22  O6 A  G G   1      12.410   8.817   3.959  0.44 50.00           O
ATOM     23  N2 A  G G   1       8.720   6.354   2.777  0.44 50.00           N
ATOM     24  P  A  G G   2       5.121  13.041   6.613  0.10 50.00           P
ATOM     25  OP1A  G G   2       3.875  13.677   7.110  0.10 50.00           O
ATOM     26  OP2A  G G   2       6.439  13.563   7.057  0.10 50.00           O
ATOM     27  O5'A  G G   2       5.064  11.493   6.983  0.15 50.00           O
ATOM     28  C5'A  G G   2       3.881  10.740   6.769  0.15 50.00           C
ATOM     29  C4'A  G G   2       4.172   9.268   6.618  0.15 50.00           C
ATOM     30  O4'A  G G   2       5.228   9.069   5.641  0.15 50.00           O
ATOM     31  C3'A  G G   2       4.686   8.548   7.854  0.15 50.00           C
ATOM     32  O3'A  G G   2       3.665   8.217   8.777  0.15 50.00           O
ATOM     33  C2'A  G G   2       5.379   7.338   7.252  0.15 50.00           C
ATOM     34  O2'A  G G   2       4.432   6.363   6.840  0.15 50.00           O
ATOM     35  C1'A  G G   2       6.017   7.956   6.010  0.15 50.00           C
ATOM     36  N9 A  G G   2       7.394   8.415   6.272  0.15 50.00           N
ATOM     37  C8 A  G G   2       7.823   9.704   6.479  0.15 50.00           C
ATOM     38  N7 A  G G   2       9.108   9.789   6.689  0.15 50.00           N
ATOM     39  C5 A  G G   2       9.553   8.477   6.622  0.15 50.00           C
ATOM     40  C4 A  G G   2       8.507   7.617   6.367  0.10 50.00           C
ATOM     41  N1 A  G G   2      10.858   6.554   6.626  0.10 50.00           N
ATOM     42  C2 A  G G   2       9.748   5.785   6.371  0.10 50.00           C
ATOM     43  N3 A  G G   2       8.529   6.275   6.230  0.10 50.00           N
ATOM     44  C6 A  G G   2      10.857   7.938   6.772  0.10 50.00           C
ATOM     45  O6 A  G G   2      11.915   8.534   7.001  0.10 50.00           O
ATOM     46  N2 A  G G   2       9.951   4.463   6.265  0.10 50.00           N
ATOM     47  P  A  C G   3       3.994   8.125  10.347  0.40 50.00           P
ATOM     48  OP1A  C G   3       2.700   8.005  11.065  0.40 50.00           O
ATOM     49  OP2A  C G   3       4.933   9.219  10.702  0.40 50.00           O
ATOM     50  O5'A  C G   3       4.777   6.747  10.507  0.40 50.00           O
ATOM     51  C5'A  C G   3       4.123   5.506  10.282  0.40 50.00           C
ATOM     52  C4'A  C G   3       5.095   4.353  10.285  0.40 50.00           C
ATOM     53  O4'A  C G   3       6.102   4.553   9.256  0.40 50.00           O
ATOM     54  C3'A  C G   3       5.913   4.161  11.554  0.40 50.00           C
ATOM     55  O3'A  C G   3       5.203   3.528  12.604  0.40 50.00           O
ATOM     56  C2'A  C G   3       7.105   3.363  11.046  0.40 50.00           C
ATOM     57  O2'A  C G   3       6.761   2.000  10.843  0.40 50.00           O
ATOM     58  C1'A  C G   3       7.340   4.020   9.687  0.40 50.00           C
ATOM     59  N1 A  C G   3       8.331   5.118   9.776  0.40 50.00           N
ATOM     60  C2 A  C G   3       9.699   4.817   9.747  0.40 50.00           C
ATOM     61  N3 A  C G   3      10.602   5.823   9.837  0.40 50.00           N
ATOM     62  C4 A  C G   3      10.185   7.084   9.955  0.40 50.00           C
ATOM     63  C5 A  C G   3       8.803   7.419   9.991  0.40 50.00           C
ATOM     64  C6 A  C G   3       7.922   6.416   9.902  0.40 50.00           C
ATOM     65  O2 A  C G   3      10.060   3.635   9.637  0.40 50.00           O
ATOM     66  N4 A  C G   3      11.109   8.043  10.040  0.40 50.00           N

ATOM    388  P  B  G G   1       8.952  16.405   4.895  0.38 50.00           P
ATOM    389  OP1B  G G   1      10.356  15.910   4.928  0.38 50.00           O
ATOM    390  OP2B  G G   1       8.612  17.577   4.047  0.38 50.00           O
ATOM    391  O5'B  G G   1       7.986  15.192   4.509  0.38 50.00           O
ATOM    392  C5'B  G G   1       7.703  14.889   3.150  0.38 50.00           C
ATOM    393  C4'B  G G   1       6.766  13.712   2.999  0.38 50.00           C
ATOM    394  O4'B  G G   1       7.317  12.809   2.000  0.38 50.00           O
ATOM    395  C3'B  G G   1       6.551  12.834   4.234  0.38 50.00           C
ATOM    396  O3'B  G G   1       5.523  13.317   5.087  0.38 50.00           O
ATOM    397  C2'B  G G   1       6.246  11.476   3.622  0.38 50.00           C
ATOM    398  O2'B  G G   1       4.915  11.426   3.128  0.38 50.00           O
ATOM    399  C1'B  G G   1       7.201  11.476   2.435  0.38 50.00           C
ATOM    400  N9 B  G G   1       8.553  11.025   2.815  0.38 50.00           N
ATOM    401  C8 B  G G   1       9.599  11.851   3.159  0.38 50.00           C
ATOM    402  N7 B  G G   1      10.692  11.203   3.451  0.38 50.00           N
ATOM    403  C5 B  G G   1      10.349   9.868   3.287  0.38 50.00           C
ATOM    404  C4 B  G G   1       9.030   9.738   2.894  0.38 50.00           C
ATOM    405  N1 B  G G   1      10.396   7.549   3.192  0.38 50.00           N
ATOM    406  C2 B  G G   1       9.079   7.524   2.805  0.38 50.00           C
ATOM    407  N3 B  G G   1       8.337   8.607   2.636  0.90 50.00           N
ATOM    408  C6 B  G G   1      11.131   8.697   3.461  0.90 50.00           C
ATOM    409  O6 B  G G   1      12.314   8.602   3.804  0.90 50.00           O
ATOM    410  N2 B  G G   1       8.561   6.305   2.597  0.90 50.00           N
ATOM    411  P  B  G G   2       5.322  12.723   6.573  0.90 50.00           P
ATOM    412  OP1B  G G   2       4.025  13.228   7.090  0.90 50.00           O
ATOM    413  OP2B  G G   2       6.554  12.978   7.361  0.90 50.00           O
ATOM    414  O5'B  G G   2       5.220  11.146   6.369  0.90 50.00           O
ATOM    415  C5'B  G G   2       4.002  10.505   6.012  0.90 50.00           C
ATOM    416  C4'B  G G   2       4.128   9.010   6.162  0.90 50.00           C
ATOM    417  O4'B  G G   2       5.236   8.529   5.355  0.90 50.00           O
ATOM    418  C3'B  G G   2       4.449   8.541   7.571  0.90 50.00           C
ATOM    419  O3'B  G G   2       3.282   8.436   8.370  0.90 50.00           O
ATOM    420  C2'B  G G   2       5.178   7.222   7.339  0.90 50.00           C
ATOM    421  O2'B  G G   2       4.263   6.158   7.118  0.90 50.00           O
ATOM    422  C1'B  G G   2       5.931   7.508   6.037  0.90 50.00           C
ATOM    423  N9 B  G G   2       7.322   7.967   6.250  0.90 50.00           N
ATOM    424  C8 B  G G   2       7.717   9.272   6.411  0.17 50.00           C
ATOM    425  N7 B  G G   2       9.001   9.418   6.564  0.17 50.00           N
ATOM    426  C5 B  G G   2       9.502   8.130   6.493  0.17 50.00           C
ATOM    427  C4 B  G G   2       8.481   7.218   6.295  0.17 50.00           C
ATOM    428  N1 B  G G   2      10.914   6.287   6.467  0.17 50.00           N
ATOM    429  C2 B  G G   2       9.837   5.458   6.270  0.17 50.00           C
ATOM    430  N3 B  G G   2       8.583   5.877   6.175  0.17 50.00           N
ATOM    431  C6 B  G G   2      10.842   7.668   6.590  0.17 50.00           C
ATOM    432  O6 B  G G   2      11.873   8.327   6.766  0.17 50.00           O
ATOM    433  N2 B  G G   2      10.114   4.150   6.173  0.17 50.00           N
ATOM    434  P  B  C G   3       3.378   8.184   9.954  0.17 50.00           P
ATOM    435  OP1B  C G   3       2.000   8.276  10.500  0.17 50.00           O
ATOM    436  OP2B  C G   3       4.431   9.058  10.537  0.17 50.00           O
ATOM    437  O5'B  C G   3       3.838   6.664  10.050  0.17 50.00           O
ATOM    438  C5'B  C G   3       4.479   6.166  11.209  0.17 50.00           C
ATOM    439  C4'B  C G   3       5.283   4.930  10.904  0.17 50.00           C
ATOM    440  O4'B  C G   3       6.121   5.147   9.738  0.17 50.00           O
ATOM    441  C3'B  C G   3       6.260   4.525  11.991  0.17 50.00           C
ATOM    442  O3'B  C G   3       5.634   3.824  13.049  0.17 50.00           O
ATOM    443  C2'B  C G   3       7.300   3.715  11.226  0.17 50.00           C
ATOM    444  O2'B  C G   3       6.853   2.387  10.995  0.17 50.00           O
ATOM    445  C1'B  C G   3       7.350   4.466   9.893  0.17 50.00           C
ATOM    446  N1 B  C G   3       8.457   5.453   9.841  0.17 50.00           N
ATOM    447  C2 B  C G   3       9.781   5.004   9.752  0.17 50.00           C
ATOM    448  N3 B  C G   3      10.792   5.904   9.705  0.17 50.00           N
ATOM    449  C4 B  C G   3      10.526   7.209   9.740  0.17 50.00           C
ATOM    450  C5 B  C G   3       9.192   7.698   9.824  0.17 50.00           C
ATOM    451  C6 B  C G   3       8.201   6.797   9.872  0.17 50.00           C
ATOM    452  O2 B  C G   3      10.011   3.786   9.724  0.17 50.00           O
ATOM    453  N4 B  C G   3      11.555   8.059   9.687  0.17 50.00           N

TER
END
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Occupancies.
  """
  pdb_file_answer = "%s_answer.pdb"%prefix
  pdb_inp = iotbx.pdb.input(source_info=None, lines=pdb_str)
  ph = pdb_inp.construct_hierarchy()
  xrs_answer = ph.extract_xray_structure()
  xrs_answer.scattering_type_registry(table = "wk1995")
  ph.write_pdb_file(file_name = pdb_file_answer)
  #
  pdb_file_poor = "%s_poor.pdb"%prefix
  xrs_poor = xrs_answer.deep_copy_scatterers()
  xrs_poor.set_occupancies(value=1)
  ph.adopt_xray_structure(xrs_poor)
  ph.write_pdb_file(file_name = pdb_file_poor)
  #
  for d_min in [4,]:
    f_obs = abs(xrs_answer.structure_factors(d_min=d_min,
      algorithm="direct").f_calc())
    mtz_dataset = f_obs.as_mtz_dataset(column_root_label="F-obs")
    mtz_dataset.add_miller_array(miller_array=f_obs.generate_r_free_flags(),
      column_root_label="R-free-flags")
    mtz_object = mtz_dataset.mtz_object()
    data_file = "%s_data.mtz"%prefix
    mtz_object.write(file_name = data_file)
    ref_prefix = "%s_d_min_%s"%(prefix, str(d_min))
    args = [
      pdb_file_poor,
      data_file,
      'main.number_of_mac=3',
      'refine.strategy=occupancies',
      'main.bulk_solv=false',
      "structure_factors_and_gradients_accuracy.algorithm=direct",
      "main.scattering_table=wk1995",
      'xray_data.r_free_flags.ignore_r_free_flags=true',
      "xray_data.outliers_rejection=false",
      "main.target=ls",
      "ls_target_names.target_name=ls_wunit_kunit"
      ]
    r = run_phenix_refine(args = args, prefix=prefix)
    pdb_inp = iotbx.pdb.input(file_name=r.pdb)
    ph = pdb_inp.construct_hierarchy()
    occs_rg = []
    for m in ph.models():
      for c in m.chains():
        for rg in c.residue_groups():
          occs = flex.double()
          for con in rg.conformers():
            o = con.atoms().extract_occ()
            occs.append(flex.mean(o))
          assert approx_equal(flex.sum(occs), 1.0)
          occs_rg.append(occs)
    assert approx_equal(occs_rg[1], occs_rg[2])

if(__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK")
  print("Time: %8.3f"%(time.time()-t0))
