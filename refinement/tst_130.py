from __future__ import print_function
import libtbx.load_env
import sys, os, time
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel

ddir = libtbx.env.under_dist(
  module_name = "phenix_regression",
  path        = "refinement/data/",
  test        = os.path.isdir)

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Exercise LINK consistency and presence between PDB and mmCIF using 3 modes:
  1) automatic, 2) apply_link, 3) geometry_restraints.edits
  """
  #
  args = ["%s/%s.pdb"%(ddir,prefix), "high_res=3 type=real r_free=0.1"]
  r = run_fmodel(args = args, prefix = prefix)
  #
  common_arg = ["strategy=none", "main.number_of_mac=0",
                "write_map_coefficients=False",
                "%s/%s.pdb"%(ddir,prefix), "%s.mtz"%prefix]
  args1 = common_arg + [
    "%s/%s_link.cif"%(ddir,prefix),
    "%s/%s_ligands.cif"%(ddir,prefix),
    "%s/%s.def"%(ddir,prefix)
    ]
  args2 = common_arg + [
    "%s/%s_ligands.cif"%(ddir,prefix),
    ]
  args3 = common_arg + [
    "%s/%s_ligands.cif"%(ddir,prefix),
    "%s/%s.edits"%(ddir,prefix)
    ]
  r1 = run_phenix_refine(args = args1, mtz_expected=False, prefix = prefix+"_1")
  r2 = run_phenix_refine(args = args2, mtz_expected=False, prefix = prefix+"_2")
  r3 = run_phenix_refine(args = args2, mtz_expected=False, prefix = prefix+"_3")
  #
  # Check mmCIF
  expected_mmCIF = [
    "covale CYS A 124 SG CYS A 31 SG . UHS A 424 C1 UHS C . C1 .",
    "metalc CYS A 176 SG CYS A 83 SG . ZN A 301 ZN ZN B . ZN . .",
    "metalc HIS A 179 ND1 HIS A 86 ND1 . ZN A 301 ZN ZN B . ZN . .",
    "metalc CYS A 238 SG CYS A 139 SG . ZN A 301 ZN ZN B . ZN . .",
    "metalc CYS A 242 SG CYS A 143 SG . ZN A 301 ZN ZN B . ZN . .",
  ]
  for cif in [r1.cif, r2.cif, r3.cif]:
    print(cif)
    found = 0
    with open(cif,"r") as fo:
      for l in fo.readlines():
        l = l.strip()
        for e in expected_mmCIF:
          if(l.count(e)>0):
            # print('found',l)
            found += 1
    assert found == 5
  #
  # Check PDB
  # XXX Add another four missing!!!
  expected_PDB = [
    'LINK         C1 AUHS A 424                 SG  CYS A 124',
    'LINK         SG  CYS A 124                 C1 AUHS A 424',
    'LINK        ZN    ZN A 301                 ND1 HIS A 179',
    'LINK        ZN    ZN A 301                 SG  CYS A 238',
    'LINK        ZN    ZN A 301                 SG  CYS A 176',
    'LINK        ZN    ZN A 301                 SG  CYS A 242',
  ]
  for pdb in [r1.pdb, r2.pdb, r3.pdb]:
    print(pdb)
    found = 0
    with open(pdb,"r") as fo:
      for l in fo.readlines():
        l = l.strip()
        for e in expected_PDB:
          if(l.find(e)>-1):
            # print('found',l)
            found += 1
    assert found == 5

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK", "time: %.2f"%(time.time()-t0))
