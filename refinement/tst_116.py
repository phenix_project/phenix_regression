from __future__ import print_function
import time
from libtbx import easy_run
import iotbx.pdb
from phenix_regression.refinement import get_r_factors
from scitbx.array_family import flex
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel
import os

pdb_str = """
CRYST1   16.319   10.284   15.464  90.00  90.00  90.00 P 1
ATOM      1  N   ILE A   3      14.542   4.832  10.890  1.00 25.00           N
ATOM      2  CA  ILE A   3      14.587   3.447  11.344  1.00 25.00           C
ATOM      3  CB  ILE A   3      13.274   3.039  12.049  1.00 25.00           C
ATOM      4  CG1 ILE A   3      12.996   3.971  13.235  1.00 25.00           C
ATOM      5  CG2 ILE A   3      13.352   1.585  12.518  1.00 25.00           C
ATOM      6  CD1 ILE A   3      11.621   3.796  13.854  1.00 25.00           C
ATOM      7  C   ILE A   3      14.829   2.522  10.156  1.00 25.00           C
ATOM      8  O   ILE A   3      14.028   2.474   9.222  1.00 25.00           O
ATOM      9  HA  ILE A   3      15.409   3.324  12.049  1.00 25.00           H
ATOM     10  HB  ILE A   3      12.454   3.130  11.336  1.00 25.00           H
ATOM     11 HG12 ILE A   3      13.736   3.779  14.012  1.00 25.00           H
ATOM     12 HG13 ILE A   3      13.075   5.005  12.899  1.00 25.00           H
ATOM     13 HG21 ILE A   3      12.476   1.358  13.126  1.00 25.00           H
ATOM     14 HG22 ILE A   3      13.375   0.928  11.649  1.00 25.00           H
ATOM     15 HG23 ILE A   3      14.258   1.452  13.109  1.00 25.00           H
ATOM     16 HD11 ILE A   3      11.435   4.621  14.542  1.00 25.00           H
ATOM     17 HD12 ILE A   3      10.873   3.798  13.061  1.00 25.00           H
ATOM     18 HD13 ILE A   3      11.590   2.849  14.392  1.00 25.00           H
TER
ATOM     19  N   ILE B   3       4.766   8.495   2.159  1.00 25.00           N
ATOM     20  CA  ILE B   3       3.355   8.357   2.498  1.00 25.00           C
ATOM     21  CB  ILE B   3       3.006   6.902   2.886  1.00 25.00           C
ATOM     22  CG1 ILE B   3       3.369   5.946   1.743  1.00 25.00           C
ATOM     23  CG2 ILE B   3       1.520   6.786   3.229  1.00 25.00           C
ATOM     24  CD1 ILE B   3       3.271   4.476   2.108  1.00 25.00           C
ATOM     25  C   ILE B   3       3.010   9.284   3.658  1.00 25.00           C
ATOM     26  O   ILE B   3       3.555   9.152   4.754  1.00 25.00           O
ATOM     27  HA  ILE B   3       2.747   8.639   1.639  1.00 25.00           H
ATOM     28  HB  ILE B   3       3.588   6.628   3.766  1.00 25.00           H
ATOM     29 HG12 ILE B   3       2.692   6.126   0.908  1.00 25.00           H
ATOM     30 HG13 ILE B   3       4.396   6.139   1.432  1.00 25.00           H
ATOM     31 HG21 ILE B   3       1.267   5.736   3.372  1.00 25.00           H
ATOM     32 HG22 ILE B   3       1.319   7.339   4.146  1.00 25.00           H
ATOM     33 HG23 ILE B   3       0.934   7.200   2.408  1.00 25.00           H
ATOM     34 HD11 ILE B   3       3.735   3.884   1.319  1.00 25.00           H
ATOM     35 HD12 ILE B   3       3.790   4.310   3.052  1.00 25.00           H
ATOM     36 HD13 ILE B   3       2.221   4.203   2.209  1.00 25.00           H
TER
END
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Riding vs individual H; neutron.
  """
  pdb_file = "%s.pdb"%prefix
  #
  pdb_inp = iotbx.pdb.input(source_info=None, lines=pdb_str)
  pdb_inp.write_pdb_file(file_name = pdb_file)
  #
  args = [
    pdb_file,
    "high_res=2.3",
    "type=real",
    "r_free=0.1",
    "algorithm=direct",
    "scattering_table=neutron"]
  r0 = run_fmodel(args = args, prefix = prefix)
  #
  r_work_starts = flex.double()
  for i, o in enumerate(["","hydrogens.refine=None","hydrogens.refine=individual"]):
    args = [
      pdb_file,
      r0.mtz,
      "main.number_of_macro_cycles=1",
      "ncs_search.enabled=true",
      "strategy=individual_sites",
      "main.scattering_table=neutron",
      "structure_factors_and_gradients_accuracy.algorithm=direct",
      "wxc_scale=100",
      "real_space_optimize_x_h_orientation=false",
      "write_eff_file=True",
      "write_geo_file=True",
      "write_def_file=True",
      "write_map_coefficients=True"]
    if o:
      args.append("%s"%o)
    r = run_phenix_refine(args = args, prefix = prefix)
    if(i==0): 
      assert r.r_work_final*100 < 0.5,   r.r_work_final
      assert r.r_work_start*100 < 1.e-4, r.r_work_start
    r_work_starts.append(r.r_work_start)
    assert r_work_starts.all_eq(r_work_starts[0])
  #
  
if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("Time: %6.2f"%(time.time()-t0))
  print("OK")
