from __future__ import division
from __future__ import print_function
from libtbx import easy_run
import sys, os
from phenix_regression.refinement import run_phenix_refine

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Exercise group_anomalous (find groups automatically).
  """
  from phenix.refinement import benchmark
  from mmtbx.regression import make_fake_anomalous_data
  mtz_file, pdb_file = make_fake_anomalous_data.generate_calcium_inputs(
    file_base=prefix)
  pdb_file = "%s.pdb"%prefix
  args = [
    "%s" % mtz_file,
    "%s" % pdb_file,
    "refine.strategy=group_anomalous",
    "group_anomalous.find_automatically=True",
    "main.number_of_macro_cycles=1",
  ]
  r = run_phenix_refine(args = args, prefix = prefix)
  found = False
  t="chain 'S' and resname CA  and name 'CA  ' and altloc ' ' and resid    1 "
  with open(r.log, "r") as fo:
    for l in fo.readlines():
      l=l.strip()
      if(l.count(t)): found = True
  assert found
  print(r.r_work_start > 0.01)
  print(r.r_work_final < 0.003)

if (__name__ == "__main__") :
  run()
