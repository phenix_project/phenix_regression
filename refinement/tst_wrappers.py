from __future__ import division, print_function
from libtbx import easy_run
from libtbx.utils import multi_out
from phenix_regression.refinement import run_fmodel
from six.moves import cStringIO as StringIO
import os, sys

from phenix.refinement.fsr import wrappers

pdb_str = """
CRYST1   26.851   32.174   25.146  90.00  90.00  90.00 P 1
ATOM      1  N   GLY B   1      10.491   7.299  10.131  1.00  7.85           N
ATOM      2  CA  GLY B   1      11.259   8.382   9.484  1.00  6.79           C
ATOM      3  C   GLY B   1      11.687   9.407  10.508  1.00  5.59           C
ATOM      4  O   GLY B   1      11.421   9.253  11.703  1.00  6.04           O
ATOM      5  N   CYS B   2      12.252  10.501  10.025  1.00  5.95           N
ATOM      6  CA  CYS B   2      12.751  11.577  10.876  1.00  5.17           C
ATOM      7  C   CYS B   2      11.799  12.091  11.950  1.00  4.74           C
ATOM      8  O   CYS B   2      12.183  12.260  13.118  1.00  4.51           O
ATOM      9  CB  CYS B   2      13.149  12.762   9.999  1.00  5.99           C
TER
END
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Make sure the phenix refine wrapper preserves sys.stderr when called with multi-out log.
  """
  pdb_file = "%s.pdb"%prefix
  hkl_file = "%s.mtz"%prefix
  with open(pdb_file, "w") as fo:
    fo.write(pdb_str)
  args = [
    pdb_file,
    "high_resolution=3.9",
    "scale=0.0000001",
    "k_sol=0.35 b_sol=60",
    "type=real r_free=0.3",
    "output.file_name=%s"%hkl_file]
  r = run_fmodel(args = args, prefix = prefix)
  args = [
    "%s.pdb" % prefix,
    "%s.mtz" % prefix,
    "strategy=none",
    "main.number_of_mac=1",
    "write_eff_file=False",
    "write_geo_file=False",
    "write_def_file=False",
    "write_model_cif_file=False",
    "write_map_coefficients=False",
    "--overwrite",
  ]
  initial_stdout = sys.stdout
  initial_stderr = sys.stderr
  log = multi_out()
  # log.register("stdout", sys.stdout)
  log.register('parser_log', StringIO())

  # print('sys.stderr before:', sys.stderr)
  wrappers.run_phenix_refine(args, log=log)
  # assert job was done
  assert os.path.isfile("%s_refine_001.pdb" % prefix)
  # print('sys.stderr after:', sys.stderr)

  if sys.stdout != initial_stdout or sys.stderr != initial_stderr:
    print ("This is an actual FAIL, FAILURE, ERROR.")
  assert sys.stdout == initial_stdout
  assert sys.stderr == initial_stderr

if (__name__ == "__main__") :
  a = run()
  print("OK")