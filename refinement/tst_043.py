from __future__ import division
from __future__ import print_function
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel
import os
from libtbx.test_utils import assert_lines_in_file

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Secondary structure restraints outliers.
  """
  pdb_str = """\
ATOM      1  N   ALA A   5      -4.768  40.795  49.311  1.00 20.00           N
ATOM      2  CA  ALA A   5      -4.569  41.829  48.303  1.00 20.00           C
ATOM      3  C   ALA A   5      -3.546  41.391  47.260  1.00 20.00           C
ATOM      4  O   ALA A   5      -3.570  41.854  46.120  1.00 20.00           O
ATOM      5  CB  ALA A   5      -4.135  43.131  48.957  1.00 20.00           C
ATOM      6  N   ALA A   6      -2.649  40.496  47.659  1.00 20.00           N
ATOM      7  CA  ALA A   6      -1.616  39.993  46.761  1.00 20.00           C
ATOM      8  C   ALA A   6      -2.123  38.805  45.951  1.00 20.00           C
ATOM      9  O   ALA A   6      -1.574  38.479  44.899  1.00 20.00           O
ATOM     10  CB  ALA A   6      -0.372  39.608  47.547  1.00 20.00           C
ATOM     11  N   ALA A   7      -3.175  38.162  46.449  1.00 20.00           N
ATOM     12  CA  ALA A   7      -3.758  37.008  45.775  1.00 20.00           C
ATOM     13  C   ALA A   7      -4.506  37.429  44.514  1.00 20.00           C
ATOM     14  O   ALA A   7      -4.607  36.662  43.556  1.00 20.00           O
ATOM     15  CB  ALA A   7      -4.687  36.258  46.717  1.00 20.00           C
ATOM     16  N   ALA A   8      -5.028  38.651  44.522  1.00 20.00           N
ATOM     17  CA  ALA A   8      -5.761  39.177  43.377  1.00 20.00           C
ATOM     18  C   ALA A   8      -4.851  40.009  42.480  1.00 20.00           C
ATOM     19  O   ALA A   8      -5.302  40.601  41.499  1.00 20.00           O
ATOM     20  CB  ALA A   8      -6.948  40.006  43.844  1.00 20.00           C
ATOM     21  N   ALA A   9      -3.568  40.050  42.823  1.00 20.00           N
ATOM     22  CA  ALA A   9      -2.592  40.810  42.051  1.00 20.00           C
ATOM     23  C   ALA A   9      -1.605  39.882  41.349  1.00 20.00           C
ATOM     24  O   ALA A   9      -1.222  40.120  40.204  1.00 20.00           O
ATOM     25  CB  ALA A   9      -1.852  41.789  42.949  1.00 20.00           C
ATOM     26  N   ALA A  10      -1.198  38.825  42.043  1.00 20.00           N
ATOM     27  CA  ALA A  10      -0.256  37.861  41.487  1.00 20.00           C
ATOM     28  C   ALA A  10      -0.946  36.925  40.501  1.00 20.00           C
ATOM     29  O   ALA A  10      -0.310  36.382  39.597  1.00 20.00           O
ATOM     30  CB  ALA A  10       0.406  37.064  42.601  1.00 20.00           C
ATOM     31  N   ALA A  11      -2.249  36.741  40.680  1.00 20.00           N
ATOM     32  CA  ALA A  11      -3.028  35.871  39.807  1.00 20.00           C
ATOM     33  C   ALA A  11      -3.771  36.679  38.748  1.00 20.00           C
ATOM     34  O   ALA A  11      -4.984  36.546  38.591  1.00 20.00           O
ATOM     35  CB  ALA A  11      -4.005  35.038  40.622  1.00 20.00           C
ATOM     36  N   ALA A  12      -3.034  37.515  38.025  1.00 20.00           N
ATOM     37  CA  ALA A  12      -3.621  38.346  36.980  1.00 20.00           C
ATOM     38  C   ALA A  12      -3.168  37.890  35.597  1.00 20.00           C
ATOM     39  O   ALA A  12      -2.329  36.998  35.471  1.00 20.00           O
ATOM     40  CB  ALA A  12      -3.263  39.807  37.201  1.00 20.00           C
ATOM     41  N   ALA A  13      -3.728  38.509  34.563  1.00 20.00           N
ATOM     42  CA  ALA A  13      -3.383  38.168  33.188  1.00 20.00           C
ATOM     43  C   ALA A  13      -2.049  38.788  32.787  1.00 20.00           C
ATOM     44  O   ALA A  13      -1.842  39.992  32.940  1.00 20.00           O
ATOM     45  CB  ALA A  13      -4.484  38.616  32.239  1.00 20.00           C
ATOM     46  N   ALA A  14      -1.147  37.958  32.272  1.00 20.00           N
ATOM     47  CA  ALA A  14       0.168  38.423  31.848  1.00 20.00           C
ATOM     48  C   ALA A  14       0.125  38.966  30.424  1.00 20.00           C
ATOM     49  O   ALA A  14       0.465  38.265  29.471  1.00 20.00           O
ATOM     50  CB  ALA A  14       1.187  37.300  31.958  1.00 20.00           C
TER
END"""
  pdb_in = "%s.pdb"%prefix
  open(pdb_in, "w").write(pdb_str)
  args = [
    pdb_in,
    "generate_fake_p1_symmetry=True",
    "high_resolution=2.5",
    "type=real",
    "label=F",
    "r_free_flags_fraction=0.1",
    "random_seed=12345"]
  r = run_fmodel(args = args, prefix = prefix)
  #This helix is deliberately made too long!
  phil = "%s.eff"%prefix
  open(phil, "w").write("""\
refinement.pdb_interpretation.secondary_structure.protein {
  helix {
    selection = chain 'A' and resid 5 through 14
  }
}""")
  #Do not change parameters to make test work.
  args = [
    pdb_in, r.mtz, phil,
    "pdb_interpretation.secondary_structure.enabled=True",
    "main.number_of_macro_cycles=1",
    "refine.strategy=individual_sites",
    "secondary_structure.protein.restrain_hbond_angles=False",
    "cdl=False"]
  r = run_phenix_refine(args = args, prefix = prefix)
  assert r.r_free_final < 0.0020
  assert_lines_in_file(file_name=r.pdb, lines="""
      HELIX    0   0 ALA A    5  ALA A   14  1                         10""")

if (__name__ == "__main__"):
  run()
  print("OK")
