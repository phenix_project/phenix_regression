from __future__ import print_function
from phenix_regression.refinement import get_r_factors
import libtbx.load_env
from libtbx.test_utils import approx_equal, run_command
import sys, os, time, random
from cctbx.array_family import flex
import iotbx.pdb
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel
import os

if (1):
  random.seed(0)
  flex.set_random_seed(0)

pdb_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="pdb",
  test=os.path.isdir)

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Occupancies.
  """
  pdb_file = os.path.join(pdb_dir, "phe_i.pdb")
  mtz_file = 'occ_exercise_04.mtz'
  pdb_file_to_refine = "occ_shifted.pdb"
  pdbtools_cmd = " ".join([
    'phenix.pdbtools',
    '%s'%pdb_file,
    'modify.selection="name CA or name CB"',
    'occupancies.randomize=True',
    'random_seed=2679941',
    'suffix=none',
    'output.prefix=%s'%pdb_file_to_refine.replace(".pdb",""),
    '> tmp_occ_ind_4.log'])
  run_command(
    command=pdbtools_cmd,
    stdout_file_name="tmp_occ_ind_4.log",
    result_file_names=[pdb_file_to_refine],
    verbose=False)
  args = [
    pdb_file_to_refine,
    'high_resolution=1.5',
    'algorithm=direct',
    'label=FOBS',
    'type=real']
  r0 = run_fmodel(args = args, prefix = prefix)

  pdbtools_cmd = " ".join([
    'phenix.pdbtools',
    '%s'%pdb_file,
    'occupancies.set=1.0',
    'suffix=none',
    'output.prefix=%s'%pdb_file_to_refine.replace(".pdb",""),
    '> tmp_occ_ind_4.log'])
  run_command(
    command=pdbtools_cmd,
    stdout_file_name="tmp_occ_ind_4.log",
    result_file_names=[pdb_file_to_refine],
    verbose=False)
  par1 = """\
  refinement.refine.occupancies {
    individual = name CA or name CB
  }
  """
  par2 = """\
  refinement.refine.occupancies {
    individual = name CA
    individual = name CB
  }
  """
  occupancies = []
  r_factors = []
  for par in [par1, par2]:
    open("par_file", "w").write(par)
    args = [
      'strategy=occupancies',
      'par_file',
      'main.target=ls',
      'main.bulk_solvent_and_=false',
      'ls_target_names.target_name=ls_wunit_kunit',
      pdb_file_to_refine,
      r0.mtz,
      'structure_factors_and_gradients_accuracy.algorithm=direct',
      'xray_data.r_free_flags.generate=True',
      'xray_data.r_free_flags.ignore_r_free_flags=true',
      "xray_data.outliers_rejection=false",
      "neutron_data.outliers_rejection=false"]
    r = run_phenix_refine(args = args, prefix = prefix)

    r_factors.append(r)
    occupancies.append(iotbx.pdb.input(file_name = r.pdb
      ).xray_structure_simple().scatterers().extract_occupancies())
    assert r.r_work_start > 0.1, r.r_work_start
    assert r.r_free_start > 0.1, r.r_free_start
    assert approx_equal(r.r_work_final, 0)
    assert approx_equal(r.r_free_final, 0)
  assert approx_equal(r_factors[0].r_work_start, r_factors[1].r_work_start)
  assert approx_equal(r_factors[0].r_free_start, r_factors[1].r_free_start)
  assert approx_equal(r_factors[0].r_work_final, r_factors[1].r_work_final)
  assert approx_equal(r_factors[0].r_free_final, r_factors[1].r_free_final)
  assert approx_equal(occupancies[0], occupancies[1])

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK", "time: %.2f"%(time.time()-t0))
