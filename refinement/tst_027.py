from __future__ import division
from __future__ import print_function
import iotbx.pdb
import math, os
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel

pdb_str_1 = """\
CRYST1   31.937   24.866   33.477  90.00 107.08  90.00 P 1 21 1                 
ATOM      1  N   GLY A   1      -9.284   4.678   5.795  1.00 10.00           N  
ATOM      2  CA  GLY A   1      -9.199   4.182   4.434  1.00 10.00           C  
ATOM      3  C   GLY A   1      -8.113   3.136   4.285  1.00 10.00           C  
ATOM      4  O   GLY A   1      -7.715   2.501   5.261  1.00 10.00           O  
ATOM      5  N   ASN A   2      -7.635   2.954   3.058  1.00 10.00           N  
ATOM      6  CA  ASN A   2      -6.548   2.019   2.800  1.00 10.00           C  
ATOM      7  C   ASN A   2      -5.247   2.524   3.404  1.00 10.00           C  
ATOM      8  O   ASN A   2      -5.024   3.730   3.492  1.00 10.00           O  
ATOM      9  CB  ASN A   2      -6.383   1.786   1.300  1.00 10.00           C  
ATOM     10  CG  ASN A   2      -7.645   1.253   0.655  1.00 10.00           C  
ATOM     11  OD1 ASN A   2      -8.043   0.113   0.895  1.00 10.00           O  
ATOM     12  ND2 ASN A   2      -8.283   2.077  -0.169  1.00 10.00           N  
ATOM     13  N   ASN A   3      -4.389   1.598   3.816  1.00 10.00           N  
ATOM     14  CA  ASN A   3      -3.172   1.962   4.529  1.00 10.00           C  
ATOM     15  C   ASN A   3      -1.906   1.366   3.921  1.00 10.00           C  
ATOM     16  O   ASN A   3      -1.794   0.152   3.756  1.00 10.00           O  
ATOM     17  CB  ASN A   3      -3.287   1.538   5.996  1.00 10.00           C  
ATOM     18  CG  ASN A   3      -2.041   1.860   6.797  1.00 10.00           C  
ATOM     19  OD1 ASN A   3      -1.776   3.018   7.117  1.00 10.00           O  
ATOM     20  ND2 ASN A   3      -1.271   0.830   7.131  1.00 10.00           N  
ATOM     21  N   GLN A   4      -0.959   2.236   3.581  1.00 10.00           N  
ATOM     22  CA  GLN A   4       0.375   1.803   3.184  1.00 10.00           C  
ATOM     23  C   GLN A   4       1.422   2.523   4.022  1.00 10.00           C  
ATOM     24  O   GLN A   4       1.541   3.747   3.968  1.00 10.00           O  
ATOM     25  CB  GLN A   4       0.636   2.060   1.699  1.00 10.00           C  
ATOM     26  CG  GLN A   4       1.950   1.456   1.215  1.00 10.00           C  
ATOM     27  CD  GLN A   4       2.462   2.081  -0.071  1.00 10.00           C  
ATOM     28  OE1 GLN A   4       2.623   3.298  -0.162  1.00 10.00           O  
ATOM     29  NE2 GLN A   4       2.727   1.245  -1.071  1.00 10.00           N  
ATOM     30  N   GLN A   5       2.176   1.759   4.801  1.00 10.00           N  
ATOM     31  CA  GLN A   5       3.238   2.326   5.618  1.00 10.00           C  
ATOM     32  C   GLN A   5       4.596   1.790   5.186  1.00 10.00           C  
ATOM     33  O   GLN A   5       4.825   0.581   5.165  1.00 10.00           O  
ATOM     34  CB  GLN A   5       2.987   2.034   7.097  1.00 10.00           C  
ATOM     35  CG  GLN A   5       1.834   2.831   7.683  1.00 10.00           C  
ATOM     36  CD  GLN A   5       1.415   2.331   9.047  1.00 10.00           C  
ATOM     37  OE1 GLN A   5       0.910   1.217   9.183  1.00 10.00           O  
ATOM     38  NE2 GLN A   5       1.623   3.152  10.070  1.00 10.00           N  
ATOM     39  N   ASN A   6       5.490   2.704   4.829  1.00 10.00           N  
ATOM     40  CA  ASN A   6       6.827   2.342   4.383  1.00 10.00           C  
ATOM     41  C   ASN A   6       7.871   2.805   5.389  1.00 10.00           C  
ATOM     42  O   ASN A   6       8.273   3.968   5.396  1.00 10.00           O  
ATOM     43  CB  ASN A   6       7.104   2.938   3.005  1.00 10.00           C  
ATOM     44  CG  ASN A   6       5.989   2.655   2.019  1.00 10.00           C  
ATOM     45  OD1 ASN A   6       5.868   1.544   1.503  1.00 10.00           O  
ATOM     46  ND2 ASN A   6       5.163   3.662   1.754  1.00 10.00           N  
ATOM     47  N   TYR A   7       8.299   1.882   6.243  1.00 10.00           N  
ATOM     48  CA  TYR A   7       9.198   2.207   7.344  1.00 10.00           C  
ATOM     49  C   TYR A   7      10.650   2.292   6.891  1.00 10.00           C  
ATOM     50  O   TYR A   7      11.016   1.813   5.817  1.00 10.00           O  
ATOM     51  CB  TYR A   7       9.054   1.170   8.460  1.00 10.00           C  
ATOM     52  CG  TYR A   7       7.638   1.031   8.971  1.00 10.00           C  
ATOM     53  CD1 TYR A   7       6.763   0.109   8.409  1.00 10.00           C  
ATOM     54  CD2 TYR A   7       7.173   1.825  10.011  1.00 10.00           C  
ATOM     55  CE1 TYR A   7       5.468  -0.020   8.871  1.00 10.00           C  
ATOM     56  CE2 TYR A   7       5.879   1.704  10.480  1.00 10.00           C  
ATOM     57  CZ  TYR A   7       5.031   0.780   9.906  1.00 10.00           C  
ATOM     58  OH  TYR A   7       3.742   0.657  10.370  1.00 10.00           O  
ATOM     59  OXT TYR A   7      11.493   2.846   7.597  1.00 10.00           O  
TER      60      TYR A   7                                                      
HETATM   60  O   HOH S   1      -7.257   5.157   6.730  1.00 10.00           O  
HETATM   61  O   HOH S   2      10.366   1.695   3.249  1.00 10.00           O  
HETATM   62  O   HOH S   3      11.895   4.239  10.010  1.00 10.00           O  
HETATM   63  O   HOH S   4     -10.505  -0.659   1.469  1.00 10.00           O  
HETATM   64  O   HOH S   5      -8.059   6.107   9.273  1.00 10.00           O  
TER      66      HOH S   5                                                      
END
"""

pdb_str_2 = """\
CRYST1   31.937   24.866   33.477  90.00 107.08  90.00 P 1 21 1      0
ATOM      1  N   GLY A   1      -9.284   4.678   5.795  1.00 10.00           N
ATOM      2  CA  GLY A   1      -9.199   4.182   4.434  1.00 10.00           C
ATOM      3  C   GLY A   1      -8.113   3.136   4.285  1.00 10.00           C
ATOM      4  O   GLY A   1      -7.715   2.501   5.261  1.00 10.00           O
ATOM      0  H1  GLY A   1      -9.623   5.501   5.791  1.00 10.00           H   new
ATOM      0  H2  GLY A   1      -9.811   4.144   6.274  1.00 10.00           H   new
ATOM      0  H3  GLY A   1      -8.471   4.694   6.157  1.00 10.00           H   new
ATOM      0  HA2 GLY A   1     -10.053   3.802   4.174  1.00 10.00           H   new
ATOM      0  HA3 GLY A   1      -9.022   4.921   3.831  1.00 10.00           H   new
ATOM      5  N   ASN A   2      -7.635   2.954   3.058  1.00 10.00           N
ATOM      6  CA  ASN A   2      -6.548   2.019   2.800  1.00 10.00           C
ATOM      7  C   ASN A   2      -5.247   2.524   3.404  1.00 10.00           C
ATOM      8  O   ASN A   2      -5.024   3.730   3.492  1.00 10.00           O
ATOM      9  CB  ASN A   2      -6.383   1.786   1.300  1.00 10.00           C
ATOM     10  CG  ASN A   2      -7.645   1.253   0.655  1.00 10.00           C
ATOM     11  OD1 ASN A   2      -8.043   0.113   0.895  1.00 10.00           O
ATOM     12  ND2 ASN A   2      -8.283   2.077  -0.169  1.00 10.00           N
ATOM      0  H   ASN A   2      -7.928   3.363   2.361  1.00 10.00           H   new
ATOM      0  HA  ASN A   2      -6.772   1.174   3.220  1.00 10.00           H   new
ATOM      0  HB2 ASN A   2      -6.131   2.619   0.872  1.00 10.00           H   new
ATOM      0  HB3 ASN A   2      -5.657   1.160   1.151  1.00 10.00           H   new
ATOM      0 HD21 ASN A   2      -9.005   1.821  -0.560  1.00 10.00           H   new
ATOM      0 HD22 ASN A   2      -7.974   2.867  -0.312  1.00 10.00           H   new
ATOM     13  N   ASN A   3      -4.389   1.598   3.816  1.00 10.00           N
ATOM     14  CA  ASN A   3      -3.172   1.962   4.529  1.00 10.00           C
ATOM     15  C   ASN A   3      -1.906   1.366   3.921  1.00 10.00           C
ATOM     16  O   ASN A   3      -1.794   0.152   3.756  1.00 10.00           O
ATOM     17  CB  ASN A   3      -3.287   1.538   5.996  1.00 10.00           C
ATOM     18  CG  ASN A   3      -2.041   1.860   6.797  1.00 10.00           C
ATOM     19  OD1 ASN A   3      -1.776   3.018   7.117  1.00 10.00           O
ATOM     20  ND2 ASN A   3      -1.271   0.830   7.131  1.00 10.00           N
ATOM      0  H   ASN A   3      -4.493   0.753   3.693  1.00 10.00           H   new
ATOM      0  HA  ASN A   3      -3.087   2.925   4.455  1.00 10.00           H   new
ATOM      0  HB2 ASN A   3      -4.050   1.982   6.398  1.00 10.00           H   new
ATOM      0  HB3 ASN A   3      -3.458   0.584   6.041  1.00 10.00           H   new
ATOM      0 HD21 ASN A   3      -0.555   0.959   7.589  1.00 10.00           H   new
ATOM      0 HD22 ASN A   3      -1.489   0.034   6.889  1.00 10.00           H   new
ATOM     21  N   GLN A   4      -0.959   2.236   3.581  1.00 10.00           N
ATOM     22  CA  GLN A   4       0.375   1.803   3.184  1.00 10.00           C
ATOM     23  C   GLN A   4       1.422   2.523   4.022  1.00 10.00           C
ATOM     24  O   GLN A   4       1.541   3.747   3.968  1.00 10.00           O
ATOM     25  CB  GLN A   4       0.636   2.060   1.699  1.00 10.00           C
ATOM     26  CG  GLN A   4       1.950   1.456   1.215  1.00 10.00           C
ATOM     27  CD  GLN A   4       2.462   2.081  -0.071  1.00 10.00           C
ATOM     28  OE1 GLN A   4       2.623   3.298  -0.162  1.00 10.00           O
ATOM     29  NE2 GLN A   4       2.727   1.245  -1.071  1.00 10.00           N
ATOM      0  H   GLN A   4      -1.072   3.089   3.574  1.00 10.00           H   new
ATOM      0  HA  GLN A   4       0.433   0.847   3.335  1.00 10.00           H   new
ATOM      0  HB2 GLN A   4      -0.095   1.692   1.178  1.00 10.00           H   new
ATOM      0  HB3 GLN A   4       0.646   3.016   1.538  1.00 10.00           H   new
ATOM      0  HG2 GLN A   4       2.621   1.561   1.907  1.00 10.00           H   new
ATOM      0  HG3 GLN A   4       1.830   0.503   1.078  1.00 10.00           H   new
ATOM      0 HE21 GLN A   4       2.602   0.400  -0.970  1.00 10.00           H   new
ATOM      0 HE22 GLN A   4       3.024   1.549  -1.819  1.00 10.00           H   new
ATOM     30  N   GLN A   5       2.176   1.759   4.801  1.00 10.00           N
ATOM     31  CA  GLN A   5       3.238   2.326   5.618  1.00 10.00           C
ATOM     32  C   GLN A   5       4.596   1.790   5.186  1.00 10.00           C
ATOM     33  O   GLN A   5       4.825   0.581   5.165  1.00 10.00           O
ATOM     34  CB  GLN A   5       2.987   2.034   7.097  1.00 10.00           C
ATOM     35  CG  GLN A   5       1.834   2.831   7.683  1.00 10.00           C
ATOM     36  CD  GLN A   5       1.415   2.331   9.047  1.00 10.00           C
ATOM     37  OE1 GLN A   5       0.910   1.217   9.183  1.00 10.00           O
ATOM     38  NE2 GLN A   5       1.623   3.152  10.070  1.00 10.00           N
ATOM      0  H   GLN A   5       2.088   0.906   4.871  1.00 10.00           H   new
ATOM      0  HA  GLN A   5       3.240   3.288   5.492  1.00 10.00           H   new
ATOM      0  HB2 GLN A   5       2.806   1.087   7.206  1.00 10.00           H   new
ATOM      0  HB3 GLN A   5       3.794   2.228   7.599  1.00 10.00           H   new
ATOM      0  HG2 GLN A   5       2.091   3.764   7.749  1.00 10.00           H   new
ATOM      0  HG3 GLN A   5       1.076   2.788   7.080  1.00 10.00           H   new
ATOM      0 HE21 GLN A   5       1.979   3.924   9.937  1.00 10.00           H   new
ATOM      0 HE22 GLN A   5       1.402   2.912  10.866  1.00 10.00           H   new
ATOM     39  N   ASN A   6       5.490   2.704   4.829  1.00 10.00           N
ATOM     40  CA  ASN A   6       6.827   2.342   4.383  1.00 10.00           C
ATOM     41  C   ASN A   6       7.871   2.805   5.389  1.00 10.00           C
ATOM     42  O   ASN A   6       8.273   3.968   5.396  1.00 10.00           O
ATOM     43  CB  ASN A   6       7.104   2.938   3.005  1.00 10.00           C
ATOM     44  CG  ASN A   6       5.989   2.655   2.019  1.00 10.00           C
ATOM     45  OD1 ASN A   6       5.868   1.544   1.503  1.00 10.00           O
ATOM     46  ND2 ASN A   6       5.163   3.662   1.754  1.00 10.00           N
ATOM      0  H   ASN A   6       5.338   3.550   4.839  1.00 10.00           H   new
ATOM      0  HA  ASN A   6       6.880   1.376   4.317  1.00 10.00           H   new
ATOM      0  HB2 ASN A   6       7.223   3.897   3.088  1.00 10.00           H   new
ATOM      0  HB3 ASN A   6       7.936   2.578   2.661  1.00 10.00           H   new
ATOM      0 HD21 ASN A   6       4.513   3.551   1.203  1.00 10.00           H   new
ATOM      0 HD22 ASN A   6       5.279   4.425   2.134  1.00 10.00           H   new
ATOM     47  N   TYR A   7       8.299   1.882   6.243  1.00 10.00           N
ATOM     48  CA  TYR A   7       9.198   2.207   7.344  1.00 10.00           C
ATOM     49  C   TYR A   7      10.650   2.292   6.891  1.00 10.00           C
ATOM     50  O   TYR A   7      11.016   1.813   5.817  1.00 10.00           O
ATOM     51  CB  TYR A   7       9.054   1.170   8.460  1.00 10.00           C
ATOM     52  CG  TYR A   7       7.638   1.031   8.971  1.00 10.00           C
ATOM     53  CD1 TYR A   7       6.763   0.109   8.409  1.00 10.00           C
ATOM     54  CD2 TYR A   7       7.173   1.825  10.011  1.00 10.00           C
ATOM     55  CE1 TYR A   7       5.468  -0.020   8.871  1.00 10.00           C
ATOM     56  CE2 TYR A   7       5.879   1.704  10.480  1.00 10.00           C
ATOM     57  CZ  TYR A   7       5.031   0.780   9.906  1.00 10.00           C
ATOM     58  OH  TYR A   7       3.742   0.657  10.370  1.00 10.00           O
ATOM     59  OXT TYR A   7      11.493   2.846   7.597  1.00 10.00           O
ATOM      0  H   TYR A   7       8.077   1.052   6.201  1.00 10.00           H   new
ATOM      0  HA  TYR A   7       8.947   3.082   7.679  1.00 10.00           H   new
ATOM      0  HB2 TYR A   7       9.359   0.309   8.134  1.00 10.00           H   new
ATOM      0  HB3 TYR A   7       9.634   1.416   9.197  1.00 10.00           H   new
ATOM      0  HD1 TYR A   7       7.055  -0.430   7.710  1.00 10.00           H   new
ATOM      0  HD2 TYR A   7       7.743   2.449  10.399  1.00 10.00           H   new
ATOM      0  HE1 TYR A   7       4.894  -0.643   8.487  1.00 10.00           H   new
ATOM      0  HE2 TYR A   7       5.582   2.242  11.178  1.00 10.00           H   new
ATOM      0  HH  TYR A   7       3.459   1.409  10.614  1.00 10.00           H   new
TER      60      TYR A   7
HETATM   60  O   HOH S   1      -7.257   5.157   6.730  1.00 10.00           O
HETATM   61  O   HOH S   2      10.366   1.695   3.249  1.00 10.00           O
HETATM   62  O   HOH S   3      11.895   4.239  10.010  1.00 10.00           O
HETATM   63  O   HOH S   4     -10.505  -0.659   1.469  1.00 10.00           O
HETATM   64  O   HOH S   5      -8.059   6.107   9.273  1.00 10.00           O
TER      66      HOH S   5
END
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Exercise ordered solvent.
  """
  for i, pdb_str in enumerate([pdb_str_1, pdb_str_2]):
    #
    pdb_poor = "%s_poor.pdb"%prefix
    of = open(pdb_poor, "w")
    for l in pdb_str.splitlines():
      if(l.count("HOH")==0):
        print(l, file=of)
    of.close()
    #
    pdb_answer = "%s_answer.pdb"%prefix
    of = open(pdb_answer, "w")
    print(pdb_str, file=of)
    of.close()
    #
    args = [
      pdb_answer,
      "high_res=1.5",
      "type=real",
      "r_free=0.1"]
    r = run_fmodel(args = args, prefix = prefix)
    #
    args = [
      pdb_poor,
      r.mtz,
      "strategy=None",
      "main.ordered_solvent=True",
      "main.number_of_mac=2"]
    cmd = "phenix.refine " + " ".join(args)
    print(cmd)
    r = run_phenix_refine(args = args, prefix = prefix)
    #
    def get_sites(file_name):
      pdb_inp = iotbx.pdb.input(file_name=file_name)
      xrs = pdb_inp.xray_structure_simple()
      sites_cart = xrs.sites_cart()
      scs = pdb_inp.construct_hierarchy().atom_selection_cache().selection
      return sites_cart.select(scs(string = "water"))
    sites_cart_1 = get_sites(file_name=r.pdb)
    sites_cart_2 = get_sites(file_name=pdb_answer)
    found = 0
    for s1 in sites_cart_1:
      for s2 in sites_cart_2:
        d = math.sqrt((s1[0]-s2[0])**2+(s1[1]-s2[1])**2+(s1[2]-s2[2])**2)
        if(d<0.1): found += 1
    assert found == 5

if (__name__ == "__main__"):
  run()
  print("OK")
