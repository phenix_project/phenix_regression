from __future__ import print_function
import libtbx.load_env
import time, os
from phenix_regression.refinement import run_phenix_refine
from libtbx.test_utils import assert_lines_in_file

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  1. Make this refinement runs without crashing due to long names.
     This may eventually pass if phenix.clashscore is fixed (enabled to take
     long names), in which case the test needs to be changed.
  2. Stop with meaningful message if no data file is provided.
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/data/%s.cif"%prefix,
    test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/data/%s.mtz"%prefix,
    test=os.path.isfile)
  cif = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/data/%s_lig_restraints.cif"%prefix,
    test=os.path.isfile)
  #
  args = [
    pdb, cif,
    "refinement.main.number_of_macro_cycles=1",
    "xray_data.high_resolution=4.5"]
  r = run_phenix_refine(args = args, prefix = prefix, sorry_expected=True)
  assert_lines_in_file(
    file_name=r.log,
    lines="""
Sorry: No reflection data provided.""")
  #
  args = [
    hkl, pdb, cif,
    "refinement.main.number_of_macro_cycles=1",
    "xray_data.high_resolution=4.5"]
  r = run_phenix_refine(args = args, prefix = prefix)

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK Time: %8.3f"%(time.time()-t0))
