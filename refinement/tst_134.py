from __future__ import absolute_import, division, print_function
import libtbx.load_env
import sys, os, time
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel
from libtbx.test_utils import assert_lines_in_file
import iotbx.pdb
from scitbx.array_family import flex

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Example from Yehudi Bloch: Stop H from flipping (which is against IUPAC).
  """
  #
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/data/%s.pdb"%prefix,
    test=os.path.isfile)
  cif_1 = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/data/%s_data_ASP.cif"%prefix,
    test=os.path.isfile)
  cif_2 = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/data/%s_data_GLU.cif"%prefix,
    test=os.path.isfile)
  #
  args = [pdb_file, "high_res=2 type=real r_free=0.1"]
  r = run_fmodel(args = args, prefix = prefix)
  mtz_file = r.mtz
  #
  args = [pdb_file,
         mtz_file,
         "main.number_of_mac=1",
         ]
  r1 = run_phenix_refine(args = args, prefix = "%s_FLIP"%prefix)
  #
  args = [pdb_file,
         cif_1, cif_2,
         mtz_file,
         "main.number_of_mac=1",
         "pdb_interpretation.flip_symmetric_amino_acids=false"]
  r2 = run_phenix_refine(args = args, prefix = "%s_noFLIP"%prefix)
  #
  s0 = iotbx.pdb.input(file_name=pdb_file).atoms().extract_xyz()
  s1 = iotbx.pdb.input(file_name=r1.pdb).atoms().extract_xyz()
  s2 = iotbx.pdb.input(file_name=r2.pdb).atoms().extract_xyz()
  s01 = flex.sqrt((s0 - s1).dot())
  s02 = flex.sqrt((s0 - s2).dot())
  assert flex.mean(s01) > 0.2
  assert flex.max(s01)  > 2
  assert flex.mean(s02) < 0.01, flex.mean(s02)
  assert flex.max(s02)  < 0.05, flex.max(s02)

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK", "time: %.2f"%(time.time()-t0))
