from __future__ import division
from __future__ import print_function
import libtbx.load_env
import os
from phenix_regression.refinement import run_phenix_refine
import iotbx.pdb

def calculate_fobs(file_name, resolution = 1.0, algorithm = "direct"):
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/enk_gor.pdb", test=os.path.isfile)
  xray_structure = iotbx.pdb.input(file_name=pdb_file).xray_structure_simple()
  xray_structure.scattering_type_registry(table = "wk1995")
  f_calc = xray_structure.structure_factors(
    d_min          = resolution,
    anomalous_flag = False,
    cos_sin_table  = False,
    algorithm      = algorithm).f_calc()
  f_calc = abs(f_calc.structure_factors_from_scatterers(
                                     xray_structure = xray_structure).f_calc())
  r_free_flags = f_calc.generate_r_free_flags(fraction = 0.01,
                                              max_free = 200000)
  mtz_dataset = f_calc.as_mtz_dataset(column_root_label = "f_obs")
  mtz_dataset.add_miller_array(miller_array      = r_free_flags,
                               column_root_label = "TEST")
  mtz_object = mtz_dataset.mtz_object()
  mtz_object.write(file_name = file_name)

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Exercise group occupancy refinement.
  """
  par_str = """
data_manager.fmodel.xray_data.r_free_flags.ignore_r_free_flags=True
refinement {
  refine {
    strategy = individual_sites rigid_body individual_adp group_adp tls \
               *occupancies group_anomalous
    occupancies {
      constrained_group {
        selection = resseq 1
      }
      constrained_group {
        selection = resseq 2
      }
      constrained_group {
        selection = resseq 3
      }
      constrained_group {
        selection = resseq 4
      }
      constrained_group {
        selection = resseq 5
      }
    }
  }
  main {
    bulk_solvent_and_scale = False
    target = *ml mlhl ml_sad ls
    scattering_table = *wk1995 it1992 n_gaussian neutron
    occupancy_max = 100
    occupancy_min = -100
    fake_f_obs = true
    number_of_macro_cycles=10
  }
  fake_f_obs {
    scattering_table = *wk1995 it1992 n_gaussian neutron
    structure_factors_accuracy {
      algorithm = fft *direct
      cos_sin_table = false
    }
  }
  modify_start_model {
    modify {
      occupancies {
        randomize = true
      }
    }
  }
  group_occupancy {
    run_finite_differences_test = True
  }
  ls_target_names {
    target_name = ls_wunit_k1 ls_wunit_k2 *ls_wunit_kunit ls_wunit_k1_fixed \
                  ls_wunit_k1ask3_fixed ls_wexp_k1 ls_wexp_k2 ls_wexp_kunit \
                  ls_wff_k1 ls_wff_k2 ls_wff_kunit ls_wff_k1_fixed \
                  ls_wff_k1ask3_fixed lsm_kunit lsm_k1 lsm_k2 lsm_k1_fixed \
                  lsm_k1ask3_fixed
  }
  structure_factors_and_gradients_accuracy {
    algorithm = fft *direct
    cos_sin_table = false
  }
}
"""
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/enk_gor.pdb", test=os.path.isfile)
  par_file = "%s.params"%prefix
  open(par_file, "w").write(par_str)
  mtz = "%s.mtz"%prefix
  calculate_fobs(file_name=mtz)
  args = [pdb, mtz, par_file, "wu=0"]
  r = run_phenix_refine(args = args, prefix = prefix)
  assert r.r_work_final < 0.005, r_work_final
  assert r.r_work_start > 0.5, r_work_start

if (__name__ == "__main__"):
  run()
  print("OK")
