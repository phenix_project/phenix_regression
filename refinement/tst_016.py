from __future__ import division
from __future__ import print_function
import libtbx.load_env
import os
from phenix_regression.refinement import run_phenix_refine

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Assortment of old tests. Converted from primitive csh scripts.
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/data/1yjp.pdb",
    test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/data/1yjp.mtz",
    test=os.path.isfile)
  args = [
    pdb,hkl,
    "main.number_of_macro_cycles=2",
    "main.simulated_annealing_torsion=True",
    "sites.individual='resid 1:7'",
    "sites.torsion_angles='resid 1:7'",
    "tardy.number_of_cooling_steps=2",
    "tardy.number_of_time_steps=3",
    "tardy.emulate_cartesian=True",
    "main.scattering_table=electron",
    "adp.individual.isotropic='optional element H'"]
  r = run_phenix_refine(args = args, prefix = prefix)

if (__name__ == "__main__"):
  run()
  print("OK")
