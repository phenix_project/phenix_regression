from __future__ import print_function
from libtbx import easy_run
import libtbx.load_env
import os
from scitbx.array_family import flex

def exercise_00():
  """
  Unclear what this test does!
  """
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/optimize_adp/model.pdb",
    test=os.path.isfile)
  hkl_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/optimize_adp/data.mtz",
    test=os.path.isfile)
  cmd = " ".join([
    "phenix.refine %s %s"%(pdb_file,hkl_file),
    "overwrite=true",
    "optimize_adp=true",
    "xray_data.high_res=2.5",
    "strategy=individual_adp",
    "adp.set_b_iso=15",
    "optimize_mask=false",
    "main.number_of_mac=1",
    "main.bulk_solvent=false",
    'output.prefix="optimize_adp_ex00"',
  ])
  assert easy_run.call(cmd) == 0
  #
  rw = flex.double()
  rf = flex.double()
  db = flex.double()
  w  = flex.double()
  read1 = False
  read2 = False
  for l in open("optimize_adp_ex00_001.log","r"):
    l = l.strip()
    if(read1 and len(l.split())==7):
      l = l.split()
      try:
        rw.append(float(l[0]))
        rf.append(float(l[1]))
        db.append(float(l[3]))
        w .append(float(l[5]))
      except: read1 = False
    if(read2):
      l = l.split()
      rw_ = float(l[0])
      rf_ = float(l[1])
      db_ = float(l[3])
      w_  = float(l[5])
      break
    if(l.count("work  free  delta                           data restr")):
      read1 = True
    if(l.count("Accepted refinement result:")):
      read2 = True
  #

if (__name__ == "__main__"):
  exercise_00()
  from libtbx.utils import format_cpu_times
  print(format_cpu_times())
