from __future__ import print_function
import libtbx.load_env
from libtbx.test_utils import approx_equal
import sys, os, time
from mmtbx import utils
from cctbx.array_family import flex
import iotbx.pdb
from libtbx import easy_run
from iotbx import reflection_file_reader
from cctbx import maptbx

def assert_eq_data(label, file, ma_target):
  miller_arrays = reflection_file_reader.any_reflection_file(file_name =
    file).as_miller_arrays()
  for ma in miller_arrays:
    if(ma.info().labels[0] == label):
      ma = ma.sort()
      ma_target = ma_target.sort()
      assert ma.indices().all_eq(ma_target.indices())
      assert approx_equal(ma.data(), ma_target.data())

def exercise_00():
  pdb1 = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/anom_diff_map/1mdg.pdb",
    test=os.path.isfile)
  pdb2 = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/anom_diff_map/mdg_noBR_poor.pdb",
    test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/anom_diff_map/1mdg.mtz", \
    test=os.path.isfile)
  cif = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/anom_diff_map/bgm.cif", \
    test=os.path.isfile)
  for i_seq, pdb_file in enumerate([pdb1, pdb2]):
    cmd = " ".join([
      'phenix.refine',
      '%s'%hkl,
      '%s'%pdb_file,
      '%s'%cif,
      'main.number_of_macro_cycles=0',
      'strategy=none',
      'output.prefix=map%s'%str(i_seq),
      '--overwrite --quiet'])
    assert not easy_run.call(cmd)
  #
  n_files_processed = 0
  for map_coeff_file in ["map0_001.mtz", "map1_001.mtz"]:
    print()
    j_miller_arrays = reflection_file_reader.any_reflection_file(file_name =
      map_coeff_file).as_miller_arrays()
    for ma in j_miller_arrays:
      l = ma.info().labels[0]
      if(l == "ANOM"):
        fft_map = ma.fft_map()
        fft_map.apply_sigma_scaling()
        map_data = fft_map.real_map_unpadded()
        xrs = iotbx.pdb.input(file_name = pdb1).xray_structure_simple()
        for sc in xrs.scatterers():
          edv = abs(map_data.eight_point_interpolation(sc.site))
          if(sc.element_symbol().upper() == "BR"):
            n_files_processed += 1
            assert edv > 19., edv
          else:
            assert edv < 4.0, edv
  assert n_files_processed == 2

def run():
  exercise_00()

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK", "time: %.2f"%(time.time()-t0))
