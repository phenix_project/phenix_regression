from __future__ import print_function
import time
from libtbx.test_utils import approx_equal
from libtbx import easy_run
import iotbx.pdb
import mmtbx.utils
from scitbx.array_family import flex
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel
import os

pdb_str = """
CRYST1   21.937    4.866   23.477  90.00 107.08  90.00 P 1 21 1
ATOM      1  N   GLY A   1      -9.009   4.612   6.102  1.00 16.77           N
ATOM      2  CA  GLY A   1      -9.052   4.207   4.651  1.00 16.57           C
ATOM      3  C   GLY A   1      -8.015   3.140   4.419  1.00 16.16           C
ATOM      4  O   GLY A   1      -7.523   2.521   5.381  1.00 16.78           O
ATOM      5  H1  GLY A   1      -9.776   5.005   6.323  1.00 16.77           H
ATOM      6  H2  GLY A   1      -8.888   3.890   6.608  1.00 16.77           H
ATOM      7  H3  GLY A   1      -8.337   5.181   6.231  1.00 16.77           H
ATOM      8  HA2 GLY A   1      -9.928   3.856   4.426  1.00 16.57           H
ATOM      9  HA3 GLY A   1      -8.858   4.970   4.084  1.00 16.57           H
TER
HETATM  108  O   HOH A   8      -6.471   5.227   7.124  1.00 22.62           O
HETATM  109  H1  HOH A   8      -5.648   5.059   7.139  0.01 22.62           H
HETATM  110  H2  HOH A   8      -6.599   5.863   7.657  0.01 22.62           H
HETATM  111  O   HOH A   9      10.431   1.858   3.216  1.00 19.71           O
HETATM  112  H1  HOH A   9      11.253   1.941   3.063  0.01 19.71           H
HETATM  113  H2  HOH A   9      10.230   2.442   3.786  0.01 19.71           H
TER
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Occupancies.
Make sure it does not crashe and runs to completion. Note: only H are subject to
occupancy refinement, and once they are filtered out from selection list then
corresponding occupancy refinement selection becomes empty that crashed
minimization.
  """
  pdb_file_answer = "%s.pdb"%prefix
  of = open(pdb_file_answer, "w")
  print(pdb_str, file=of)
  of.close()
  pdb_inp = iotbx.pdb.input(source_info=None, lines=pdb_str)
  #
  args = [
    "%s.pdb"%prefix,
    "high_res=2.0",
    "type=real",
    "r_free=0.1",
    "label=f-obs"]
  r0 = run_fmodel(args = args, prefix = prefix)
  #
  args = [
    "%s.pdb"%prefix,
    r0.mtz,
    "main.number_of_mac=1",
    "strategy=occupancies"
    ]
  r = run_phenix_refine(args = args, prefix = prefix)

if(__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK")
  print("Time: %8.3f"%(time.time()-t0))
