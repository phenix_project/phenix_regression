from __future__ import print_function
from libtbx.test_utils import run_command
import libtbx.load_env
import os, time
from libtbx import easy_run
import iotbx.pdb
from libtbx.test_utils import approx_equal
import mmtbx.utils

def exercise():
  # make sure phenix.ready_set does not crash
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/elbow/wt.pdb",
    test=os.path.isfile)
  xrsin = iotbx.pdb.input(file_name = pdb).xray_structure_simple()
  # ready_set
  cmd = "phenix.ready_set %s"%pdb
  assert not easy_run.call(cmd)
  xrs_r = iotbx.pdb.input(file_name = "wt.updated.pdb").xray_structure_simple()
  xrs_r_noH = xrs_r.select(~xrs_r.hd_selection())
  assert xrsin.scatterers().size() == xrs_r_noH.scatterers().size()

if (__name__ == "__main__"):
  t0 = time.time()
  exercise()
  print("OK", "time: %8.3f"%(time.time()-t0))
