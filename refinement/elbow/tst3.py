from __future__ import division
from __future__ import print_function
import time
from libtbx import easy_run
import iotbx.pdb
from libtbx.test_utils import approx_equal

pdb_str = """
CRYST1   35.956   35.956   38.045  90.00  90.00  90.00 I 4 2 2
SCALE1      0.027812  0.000000  0.000000        0.00000
SCALE2      0.000000  0.027812  0.000000        0.00000
SCALE3      0.000000  0.000000  0.026285        0.00000
ATOM      1  O5*   U A   1     -13.803   2.639  24.658  1.00 27.14           O
ATOM      2  C5*   U A   1     -13.007   3.811  24.477  1.00 23.39           C
ATOM      3  C4*   U A   1     -11.860   3.465  23.558  1.00 19.89           C
ATOM      4  O4*   U A   1     -11.136   2.347  24.049  1.00 18.63           O
ATOM      5  C3*   U A   1     -12.222   3.170  22.103  1.00 19.80           C
ATOM      6  O3*   U A   1     -11.455   4.177  21.446  1.00 18.56           O
ATOM      7  C2*   U A   1     -11.564   1.819  21.792  1.00 20.15           C
ATOM      8  O2*   U A   1     -10.924   1.647  20.547  1.00 23.56           O
ATOM      9  C1*   U A   1     -10.597   1.641  22.964  1.00 19.03           C
ATOM     10  N1    U A   1     -10.339   0.271  23.391  1.00 16.96           N
ATOM     11  C2    U A   1      -9.007  -0.143  23.445  1.00 15.52           C
ATOM     12  O2    U A   1      -8.065   0.602  23.187  1.00 13.87           O
ATOM     13  N3    U A   1      -8.826  -1.446  23.820  1.00 13.87           N
ATOM     14  C4    U A   1      -9.809  -2.357  24.169  1.00 14.59           C
ATOM     15  O4    U A   1      -9.484  -3.465  24.564  1.00 14.68           O
ATOM     16  C5    U A   1     -11.146  -1.857  24.091  1.00 16.18           C
ATOM     17  C6    U A   1     -11.361  -0.587  23.734  1.00 16.65           C
ATOM     18  P     A A   3      -8.373   8.194  15.685  1.00 12.90           P
ATOM     19  O1P   A A   3      -8.386   9.640  16.013  1.00 15.91           O
ATOM     20  O2P   A A   3      -9.501   7.568  14.958  1.00 13.39           O
ATOM     21  O5*   A A   3      -7.035   7.914  14.848  1.00 13.04           O
ATOM     22  C5*   A A   3      -5.730   8.391  15.242  1.00 12.88           C
ATOM     23  C4*   A A   3      -4.710   7.770  14.307  1.00 12.95           C
ATOM     24  O4*   A A   3      -4.585   6.362  14.576  1.00 12.14           O
ATOM     25  C3*   A A   3      -5.188   7.901  12.853  1.00 11.96           C
ATOM     26  O3*   A A   3      -4.040   8.318  12.105  1.00 12.93           O
ATOM     27  C2*   A A   3      -5.592   6.492  12.457  1.00 10.33           C
ATOM     28  O2*   A A   3      -5.535   6.104  11.093  1.00 13.94           O
ATOM     29  C1*   A A   3      -4.661   5.672  13.339  1.00 11.59           C
ATOM     30  N9    A A   3      -5.092   4.313  13.573  1.00  9.12           N
ATOM     31  C8    A A   3      -6.341   3.792  13.648  1.00  9.33           C
ATOM     32  N7    A A   3      -6.353   2.517  13.874  1.00  9.91           N
ATOM     33  C5    A A   3      -5.015   2.171  13.917  1.00  9.78           C
ATOM     34  C6    A A   3      -4.382   0.946  14.044  1.00 12.03           C
ATOM     35  N6    A A   3      -5.049  -0.209  14.192  1.00 11.88           N
ATOM     36  N1    A A   3      -3.031   0.935  14.023  1.00 10.48           N
ATOM     37  C2    A A   3      -2.378   2.113  13.875  1.00  8.22           C
ATOM     38  N3    A A   3      -2.879   3.320  13.753  1.00 11.14           N
ATOM     39  C4    A A   3      -4.223   3.273  13.769  1.00 10.21           C
ATOM     40  P     G A   4      -4.199   9.169  10.755  1.00 14.09           P
ATOM     41  O1P   G A   4      -4.870  10.471  11.057  1.00 15.20           O
ATOM     42  O2P   G A   4      -4.790   8.276   9.721  1.00 14.17           O
ATOM     43  O5*   G A   4      -2.695   9.420  10.288  1.00 12.57           O
ATOM     44  C5*   G A   4      -1.901  10.461  10.848  1.00 12.73           C
ATOM     45  C4*   G A   4      -0.434  10.097  10.777  1.00 11.24           C
ATOM     46  O4*   G A   4      -0.142   8.974  11.652  1.00 13.50           O
ATOM     47  C3*   G A   4       0.028   9.616   9.418  1.00 12.41           C
ATOM     48  O3*   G A   4       0.229  10.730   8.577  1.00 12.76           O
ATOM     49  C2*   G A   4       1.296   8.854   9.777  1.00 10.27           C
ATOM     50  O2*   G A   4       2.388   9.710  10.102  1.00 12.31           O
ATOM     51  C1*   G A   4       0.835   8.132  11.053  1.00  9.93           C
ATOM     52  N9    G A   4       0.227   6.825  10.840  1.00  9.26           N
ATOM     53  C8    G A   4      -1.112   6.508  10.824  1.00 11.42           C
ATOM     54  N7    G A   4      -1.327   5.225  10.726  1.00 10.86           N
ATOM     55  C5    G A   4      -0.058   4.671  10.640  1.00 10.25           C
ATOM     56  C6    G A   4       0.342   3.320  10.590  1.00 10.31           C
ATOM     57  O6    G A   4      -0.379   2.315  10.605  1.00  9.85           O
ATOM     58  N1    G A   4       1.729   3.194  10.531  1.00 11.27           N
ATOM     59  C2    G A   4       2.618   4.257  10.505  1.00  8.01           C
ATOM     60  N2    G A   4       3.928   3.912  10.401  1.00 10.53           N
ATOM     61  N3    G A   4       2.252   5.520  10.568  1.00 11.05           N
ATOM     62  C4    G A   4       0.907   5.652  10.662  1.00  8.38           C
ATOM     63  P     G A   5      -0.032  10.580   6.994  1.00 14.17           P
ATOM     64  O1P   G A   5       0.110  11.998   6.602  1.00 16.98           O
ATOM     65  O2P   G A   5      -1.274   9.842   6.735  1.00 16.51           O
ATOM     66  O5*   G A   5       1.180   9.730   6.491  1.00 12.32           O
ATOM     67  C5*   G A   5       2.458  10.327   6.408  1.00 14.68           C
ATOM     68  C4*   G A   5       3.479   9.289   6.067  1.00 13.26           C
ATOM     69  O4*   G A   5       3.531   8.310   7.145  1.00 12.54           O
ATOM     70  C3*   G A   5       3.174   8.416   4.859  1.00 15.65           C
ATOM     71  O3*   G A   5       3.481   9.131   3.691  1.00 15.73           O
ATOM     72  C2*   G A   5       4.105   7.239   5.104  1.00 12.32           C
ATOM     73  O2*   G A   5       5.467   7.565   4.830  1.00 12.92           O
ATOM     74  C1*   G A   5       3.908   7.045   6.613  1.00 11.63           C
ATOM     75  N9    G A   5       2.836   6.081   6.892  1.00 11.06           N
ATOM     76  C8    G A   5       1.479   6.321   7.007  1.00 10.71           C
ATOM     77  N7    G A   5       0.777   5.222   7.180  1.00  9.09           N
ATOM     78  C5    G A   5       1.731   4.212   7.197  1.00  9.83           C
ATOM     79  C6    G A   5       1.597   2.804   7.363  1.00 10.18           C
ATOM     80  O6    G A   5       0.570   2.130   7.527  1.00 10.81           O
ATOM     81  N1    G A   5       2.838   2.164   7.331  1.00 10.61           N
ATOM     82  C2    G A   5       4.038   2.795   7.162  1.00 10.96           C
ATOM     83  N2    G A   5       5.121   2.023   7.145  1.00 13.01           N
ATOM     84  N3    G A   5       4.182   4.097   7.011  1.00 10.81           N
ATOM     85  C4    G A   5       3.005   4.738   7.033  1.00  9.37           C
ATOM     86  P     U A   6       2.766   8.721   2.312  1.00 18.59           P
ATOM     87  O1P   U A   6       3.136   7.289   2.040  1.00 19.98           O
ATOM     88  O2P   U A   6       3.164   9.813   1.347  1.00 20.74           O
ATOM     89  O5*   U A   6       1.219   8.711   2.681  1.00 19.22           O
ATOM     90  C5*   U A   6       0.233   9.483   2.037  1.00 19.32           C
ATOM     91  C4*   U A   6      -1.106   8.875   2.343  1.00 21.40           C
ATOM     92  O4*   U A   6      -1.029   7.479   1.935  1.00 20.02           O
ATOM     93  C3*   U A   6      -1.416   8.777   3.835  1.00 21.77           C
ATOM     94  O3*   U A   6      -2.073   9.954   4.321  1.00 25.29           O
ATOM     95  C2*   U A   6      -1.248   7.283   4.098  1.00 23.48           C
ATOM     96  O2*   U A   6      -1.826   7.630   5.352  1.00 28.13           O
ATOM     97  C1*   U A   6      -1.816   6.685   2.811  1.00 18.43           C
ATOM     98  N1    U A   6      -1.026   5.537   3.298  1.00 16.07           N
ATOM     99  C2    U A   6      -1.755   4.409   3.668  1.00 14.21           C
ATOM    100  O2    U A   6      -2.962   4.382   3.653  1.00 14.60           O
ATOM    101  N3    U A   6      -1.006   3.318   4.040  1.00 12.18           N
ATOM    102  C4    U A   6       0.365   3.239   4.110  1.00 14.43           C
ATOM    103  O4    U A   6       0.877   2.156   4.379  1.00 12.82           O
ATOM    104  C5    U A   6       1.069   4.446   3.734  1.00 13.69           C
ATOM    105  C6    U A   6       0.362   5.535   3.351  1.00 15.24           C
TER
HETATM  106 NA    NA   101       0.001   0.000  19.017  0.25 15.69          Na
HETATM  107 NA    NA   102      -0.013  -0.001  15.610  0.25 17.84          Na
HETATM  108 NA    NA   103       0.018   0.005  12.469  0.25 21.01          Na
HETATM  109 NA    NA   104       0.004  -0.001   9.168  0.25  8.23          Na
HETATM  110 NA    NA   105      -0.004   0.001   5.753  0.25  8.46          Na
HETATM  111  N1  NCO   201     -10.311  11.179  21.083  0.50 18.90           N
HETATM  112  N2  NCO   201     -11.179  10.293  16.958  0.50 18.95           N
HETATM  113  N3  NCO   201     -10.420   8.590  19.177  0.50 20.87           N
HETATM  114  N4  NCO   201     -10.661  12.810  18.528  0.50 21.20           N
HETATM  115  N5  NCO   201     -12.810  10.659  19.517  0.50 21.20           N
HETATM  116  N6  NCO   201      -8.581  10.417  18.877  0.50 20.89           N
HETATM  117 CO   NCO   201     -10.723  10.726  19.019  0.25 20.28          Co
HETATM  118  O   HOH   301      -7.645   4.226  10.213  1.00 20.11           O
HETATM  119  O   HOH   302      -3.100   7.913   7.414  1.00 20.16           O
HETATM  120  O   HOH   303      -8.954   6.023  12.727  1.00 25.80           O
HETATM  121  O   HOH   304      -8.271   8.350   8.034  1.00 32.76           O
HETATM  122  O   HOH   305      -5.207  12.773   9.515  0.25 29.23           O
HETATM  123  O   HOH   306     -11.723   7.739  23.225  1.00 33.44           O
HETATM  124  O   HOH   307     -13.454   9.372  24.035  1.00 25.12           O
HETATM  125  O   HOH   308     -13.210  12.780  16.839  1.00 24.99           O
HETATM  126  O   HOH   309     -10.810   1.127  17.967  1.00 27.12           O
HETATM  127  O   HOH   310      -8.988   6.300   8.532  1.00 27.66           O
HETATM  128  O   HOH   411       2.672  12.387  10.252  1.00 30.07           O
HETATM  129  O   HOH   312       1.767  13.748   7.726  1.00 30.24           O
HETATM  130  O   HOH   313      -5.502   6.618   5.790  1.00 38.69           O
HETATM  131  O   HOH   314       5.983  10.149   2.526  1.00 42.83           O
HETATM  132  O   HOH   315       1.493  16.489   9.503  0.25 41.84           O
HETATM  133  O   HOH   316     -11.800   3.386  16.692  1.00 35.75           O
END
"""

def run_ready_set(file_in, file_out):
  """
  Run ReadySet and return non-H hierarchy of the result
  """
  cmd = "phenix.ready_set %s"%file_in
  assert not easy_run.call(cmd)
  h = iotbx.pdb.input(file_name = file_out).construct_hierarchy()
  asc = h.atom_selection_cache()
  sel = asc.selection("not (element H or element D)")
  return h.select(sel)

def run():
  """
  Make sure the non-H part of the model remains the same after phenix.ready_set
  """
  with open("tst3_elbow.pdb", "w") as fo:
    fo.write(pdb_str)

  h0 = iotbx.pdb.input(source_info=None, lines=pdb_str).construct_hierarchy()
  # ready_set
  h1 = run_ready_set(
    file_in  = "tst3_elbow.pdb",
    file_out = "tst3_elbow.updated.pdb")
  # ready_set again
  h2 = run_ready_set(
    file_in  = "tst3_elbow.updated.pdb",
    file_out = "tst3_elbow.updated.updated.pdb")
  # tests
  for h in [h1,h2]:
    assert h0.is_similar_hierarchy(h)
    assert approx_equal(h0.atoms().extract_xyz(),     h.atoms().extract_xyz())
    assert approx_equal(h0.atoms().extract_element().upper(),
                        h.atoms().extract_element().upper())
    assert approx_equal(h0.atoms().extract_b(),       h.atoms().extract_b())
    assert approx_equal(h0.atoms().extract_name(),    h.atoms().extract_name())
    assert approx_equal(h0.atoms().extract_occ(),     h.atoms().extract_occ())

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK", "time: %8.3f"%(time.time()-t0))
