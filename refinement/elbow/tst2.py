from __future__ import print_function
from libtbx.test_utils import run_command
import libtbx.load_env
import os, time
from libtbx import easy_run
import iotbx.pdb
from libtbx.test_utils import approx_equal
import mmtbx.utils

def run_02():
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/elbow/02.pdb",
    test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/elbow/01.mtz",
    test=os.path.isfile)
  cmd = 'phenix.elbow %s --do_all --silent --random_seed=123456' \
    ' --output=02_elbow' % pdb
  print(cmd)
  assert not easy_run.call(cmd)
  cmd = " ".join([
    'phenix.refine',
    '%s %s 02_elbow.cif'%(pdb, hkl),
    "strategy=none",
    "main.number_of_mac=0",
    '--overwrite',
    '>',
    '02.log'
    ])
  run_command(
    command=cmd,
    log_file_name="02_refine_001.log",
    stdout_file_name="02.log",
    show_diff_log_stdout=True,
    verbose=1)

if (__name__ == "__main__"):
  t0 = time.time()
  run_02()
  print("OK", "time: %8.3f"%(time.time()-t0))
