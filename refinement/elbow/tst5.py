from __future__ import print_function
import time
from libtbx import easy_run
import iotbx.pdb
from libtbx.test_utils import approx_equal
import mmtbx.utils

pdb_str = """\
CRYST1   15.000   15.000   15.000  90.00  90.00  90.00 P 1
ATOM      0  N  AALA     1      16.179   0.411   0.833  0.70 10.11           N
ATOM      1  CA AALA     1      15.955   1.413   1.865  0.70 10.17           C
ATOM      2  C  AALA     1      14.538   1.302   2.419  0.70 10.52           C
ATOM      3  O  AALA     1      13.597   0.920   1.715  0.70 10.99           O
ATOM      4  CB AALA     1      16.202   2.810   1.308  0.70 10.41           C
ATOM      5  N  BALA     1      17.179   2.411   3.833  0.30 10.11           N
ATOM      6  CA BALA     1      16.955   3.413   4.865  0.30 10.17           C
ATOM      7  C  BALA     1      15.538   3.302   5.419  0.30 10.52           C
ATOM      8  O  BALA     1      14.597   2.920   4.715  0.30 10.99           O
ATOM      9  CB BALA     1      17.202   4.810   4.308  0.30 10.41           C
END
"""

pdb_str_answer = """\
CRYST1   15.000   15.000   15.000  90.00  90.00  90.00 P 1
SCALE1      0.066667  0.000000  0.000000        0.00000
SCALE2      0.000000  0.066667  0.000000        0.00000
SCALE3      0.000000  0.000000  0.066667        0.00000
ATOM      1  N  AALA     1      16.179   0.411   0.833  0.70 10.11           N
ATOM      2  CA AALA     1      15.955   1.413   1.865  0.70 10.17           C
ATOM      3  C  AALA     1      14.538   1.302   2.419  0.70 10.52           C
ATOM      4  O  AALA     1      13.597   0.920   1.715  0.70 10.99           O
ATOM      5  CB AALA     1      16.202   2.810   1.308  0.70 10.41           C
ATOM      6  H1 AALA     1      16.937   0.592   0.403  0.70 10.11           H
ATOM      7  H2 AALA     1      16.236  -0.394   1.208  0.70 10.11           H
ATOM      8  H3 AALA     1      15.501   0.422   0.256  0.70 10.11           H
ATOM      9  HA AALA     1      16.577   1.264   2.594  0.70 10.17           H
ATOM     10  HB1AALA     1      16.048   3.461   2.010  0.70 10.41           H
ATOM     11  HB2AALA     1      17.119   2.868   0.998  0.70 10.41           H
ATOM     12  HB3AALA     1      15.592   2.968   0.570  0.70 10.41           H
ATOM     13  N  BALA     1      17.179   2.411   3.833  0.30 10.11           N
ATOM     14  CA BALA     1      16.955   3.413   4.865  0.30 10.17           C
ATOM     15  C  BALA     1      15.538   3.302   5.419  0.30 10.52           C
ATOM     16  O  BALA     1      14.597   2.920   4.715  0.30 10.99           O
ATOM     17  CB BALA     1      17.202   4.810   4.308  0.30 10.41           C
ATOM     18  H1 BALA     1      17.937   2.592   3.403  0.30 10.11           H
ATOM     19  H2 BALA     1      17.236   1.606   4.208  0.30 10.11           H
ATOM     20  H3 BALA     1      16.501   2.422   3.256  0.30 10.11           H
ATOM     21  HA BALA     1      17.577   3.264   5.594  0.30 10.17           H
ATOM     22  HB1BALA     1      17.048   5.461   5.010  0.30 10.41           H
ATOM     23  HB2BALA     1      18.119   4.868   3.998  0.30 10.41           H
ATOM     24  HB3BALA     1      16.592   4.968   3.570  0.30 10.41           H
TER
END
"""

def exercise(prefix = "tst_refinement_elbow_5"):
  # add H to one ALA residue presented in two conformations
  pdb_inp = iotbx.pdb.input(source_info=None, lines=pdb_str)
  pdb_inp.write_pdb_file(file_name = "%s.pdb"%prefix)
  cmd = "phenix.ready_set %s.pdb > %s.log"%(prefix, prefix)
  print(cmd)
  assert not easy_run.call(cmd)
  xrsout = iotbx.pdb.input(
    file_name = "%s.updated.pdb"%prefix).xray_structure_simple()
  xrsanswer = iotbx.pdb.input(
    source_info=None, lines=pdb_str_answer).xray_structure_simple()
  mmtbx.utils.assert_xray_structures_equal(x1=xrsanswer, x2=xrsout, eps=1.e-4)

if (__name__ == "__main__"):
  t0 = time.time()
  exercise()
  print("OK", "time: %8.3f"%(time.time()-t0))
