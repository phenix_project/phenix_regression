from __future__ import print_function
import libtbx.load_env
from libtbx.test_utils import approx_equal
import sys, os, time
from mmtbx import utils
from cctbx.array_family import flex

from phenix_regression.refinement import run_phenix_refine

ddir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="refinement/data/",
  test=os.path.isdir)

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Exercise fix for: 'KeyError: 'A_ARG_N' ' in rotamer fitting
  """
  args = [
    "%s/%s.mtz"%(ddir,prefix),
    "%s/%s.pdb"%(ddir,prefix),
    ]
  r = run_phenix_refine(args = args, prefix = prefix)

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK", "time: %.2f"%(time.time()-t0))
