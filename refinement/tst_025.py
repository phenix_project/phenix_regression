from __future__ import division
from __future__ import print_function
import libtbx.load_env
import os
from libtbx.test_utils import approx_equal
from scitbx.array_family import flex
from phenix_regression.refinement import run_phenix_refine

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Make sure memory consumptions does not increase from mc to mc.
  Works on Linux only.
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/lysozyme_h.pdb", test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/lysozyme.mtz", \
    test=os.path.isfile)
  args = [
    pdb,
    hkl,
    "main.bulk_sol=false",
    "strategy=none",
    "main.number_of_mac=10",
    "show_process_info=True",
    "nqh=false",
    "fix_wxc=1"]
  r = run_phenix_refine(args = args, prefix = prefix)
  mv = flex.double()
  fo = open(r.log, "r")
  for l in fo.readlines():
    l = l.strip()
    if(l.count("Virtual memory size")>0):
      l = l.split()
      mv.append(float( l[len(l)-1].replace(",","").replace(",","") ))
  mv = mv[2:10]
  assert approx_equal(flex.min(mv), flex.max(mv))

if (__name__ == "__main__"):
  run()
  print("OK")
