from __future__ import print_function
from libtbx import easy_run
import time, math
from libtbx.test_utils import approx_equal
import iotbx.pdb

pdb_str = """\
CRYST1   20.356   14.849   13.613  90.00  90.00  90.00 P 1
ATOM     10  C   TRP A   1      12.647   8.088   6.986  1.00 30.00           C
ATOM     11  O   TRP A   1      12.766   7.656   8.112  1.00 30.00           O
ATOM     12  CA  TRP A   1      11.475   8.940   6.572  1.00 30.00           C
ATOM     13  HA  TRP A   1      11.814   9.844   6.390  1.00 30.00           H
ATOM     14  N   TRP A   1      10.885   8.336   5.372  1.00 30.00           N
ATOM     15  H1  TRP A   1      10.006   8.715   5.239  1.00 30.00           H
ATOM     16  H2  TRP A   1      11.273   8.682   6.160  1.00 30.00           H
ATOM     17  CB  TRP A   1      10.428   9.021   7.678  1.00 30.00           C
ATOM     18  HB1 TRP A   1       9.928   9.849   7.580  1.00 30.00           H
ATOM     19  HB2 TRP A   1      10.861   8.980   8.548  1.00 30.00           H
ATOM     20  CG  TRP A   1       9.473   7.870   7.557  1.00 30.00           C
ATOM     21  CD1 TRP A   1       9.620   6.606   8.086  1.00 30.00           C
ATOM     22  HD1 TRP A   1      10.338   6.327   8.613  1.00 30.00           H
ATOM     23  NE1 TRP A   1       8.515   5.827   7.739  1.00 30.00           N
ATOM     24  HE1 TRP A   1       8.399   5.000   7.966  1.00 30.00           H
ATOM     25  CE2 TRP A   1       7.649   6.594   6.981  1.00 30.00           C
ATOM     26  CZ2 TRP A   1       6.446   6.258   6.374  1.00 30.00           C
ATOM     27  HZ2 TRP A   1       6.080   5.409   6.468  1.00 30.00           H
ATOM     28  CH2 TRP A   1       5.818   7.211   5.635  1.00 30.00           C
ATOM     29  HH2 TRP A   1       5.000   7.013   5.247  1.00 30.00           H
ATOM     30  CZ3 TRP A   1       6.325   8.479   5.507  1.00 30.00           C
ATOM     31  HZ3 TRP A   1       5.861   9.110   5.000  1.00 30.00           H
ATOM     32  CE3 TRP A   1       7.534   8.826   6.089  1.00 30.00           C
ATOM     33  HE3 TRP A   1       7.891   9.680   5.976  1.00 30.00           H
ATOM     34  CD2 TRP A   1       8.217   7.871   6.843  1.00 30.00           C
ATOM      1  O   GLY A   2      15.356   5.031   6.359  1.00 30.00           O
ATOM      2  C   GLY A   2      14.662   5.819   6.832  1.00 30.00           C
ATOM      3  OXT GLY A   2      13.857   5.488   7.786  1.00 30.00           O
ATOM      4  HXT GLY A   2      13.392   6.251   8.074  1.00 30.00           H
ATOM      5  CA  GLY A   2      14.777   7.200   6.310  1.00 30.00           C
ATOM      6  HA1 GLY A   2      15.311   7.181   5.495  1.00 30.00           H
ATOM      7  HA2 GLY A   2      15.236   7.740   6.970  1.00 30.00           H
ATOM      8  N   GLY A   2      13.471   7.762   6.028  1.00 30.00           N
ATOM      9  H   GLY A   2      13.289   7.846   5.193  1.00 30.00           H
TER
END
"""

def check(target, h_names, resname, file_name=None, pdb_str=None):
  if(file_name is not None):
    h = iotbx.pdb.input(file_name=file_name).construct_hierarchy()
  elif(pdb_str is not None):
    h = iotbx.pdb.input(source_info=None, lines=pdb_str).construct_hierarchy()
  else: assert 0
  xyzs = []
  xyz_ca=None
  atoms = None
  for m in h.models():
    for c in m.chains():
      for con in c.conformers():
        for r in con.residues():
          if(r.resname==resname):
            atoms = r.atoms()
  for a in atoms:
    if(a.name.strip() in h_names):
      xyzs.append(a.xyz)
    if(a.name.strip() == "CA"): xyz_ca=a.xyz
  d1 = math.sqrt((xyzs[0][0]-xyzs[1][0])**2 +
                 (xyzs[0][1]-xyzs[1][1])**2 +
                 (xyzs[0][2]-xyzs[1][2])**2)
  d2 = math.sqrt((xyzs[0][0]-xyz_ca[0])**2 +
                 (xyzs[0][1]-xyz_ca[1])**2 +
                 (xyzs[0][2]-xyz_ca[2])**2)
  d3 = math.sqrt((xyzs[1][0]-xyz_ca[0])**2 +
                 (xyzs[1][1]-xyz_ca[1])**2 +
                 (xyzs[1][2]-xyz_ca[2])**2)
  assert approx_equal([d1,d2,d3], target, 0.1)

def exercise(prefix="tst_correct_bad_h"):
  """
  Exercise flag to turn on/off H correction.
  """
  f = open("%s.pdb" % prefix, "w")
  f.write(pdb_str)
  f.close()
  cmd = " ".join([
    "phenix.fmodel",
    "%s.pdb"%prefix,
    "high_res=1",
    "type=real",
    "label=f-obs",
    "r_free=0.1",
    "output.file_name=%s.mtz"%prefix,
    "> %s.log"%prefix
  ])
  print(cmd)
  assert not easy_run.call(cmd)
  #
  for cbh in [True, False]:
    cmd = " ".join([
      "phenix.refine",
      "strategy=individual_sites",
      "%s.pdb" % prefix,
      "%s.mtz" % prefix,
      "output.write_eff_file=False",
      "output.write_geo_file=False",
      "output.write_def_file=False",
      "output.write_map_coefficients=False",
      "hydrogens.refine=individual",
      "correct_hydrogens=%s"%str(cbh),
      "use_neutron_distances=true",
      "output.prefix=%s_%s"%(prefix, str(cbh)),
      "--overwrite",
      "> %s_1.log" % prefix
    ])
    print(cmd)
    assert not easy_run.call(cmd)
  # check results
  check(file_name = "tst_correct_bad_h_True_001.pdb",
        h_names   = ["H1","H2"],
        resname   = "TRP",
        target    = [1.6,2.,2.])
  check(file_name="tst_correct_bad_h_False_001.pdb",
        h_names   = ["H1","H2"],
        resname   = "TRP",
        target    = [1.6,2.,2.])
  check(pdb_str   = pdb_str,
        h_names   = ["H1","H2"],
        resname   = "TRP",
        target    = [1.6,2.,0.5])
  #
  check(file_name = "tst_correct_bad_h_True_001.pdb",
        h_names   = ["HA1","HA2"],
        resname   = "GLY",
        target    = [1.8,1.1,1.1])
  check(file_name="tst_correct_bad_h_False_001.pdb",
        h_names   = ["HA1","HA2"],
        resname   = "GLY",
        target    = [1.8,1.1,1.1])


if (__name__ == "__main__"):
  t0 = time.time()
  exercise()
  print("OK. Time: %8.3f"%(time.time()-t0))
