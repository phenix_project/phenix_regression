from __future__ import division
from __future__ import print_function
from libtbx.test_utils import is_above_limit
import libtbx.load_env
import libtbx.path
import os
from mmtbx.validation.rotalyze import rotalyze
import iotbx.pdb
from phenix_regression.refinement import run_phenix_refine

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Reference model restraints.
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reference_model/start.pdb",
    test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reference_model/data.mtz",
    test=os.path.isfile)
  reference_model = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reference_model/reference.pdb",
    test=os.path.isfile)
  model_hash = {}
  model_score = {}
  pdb_hierarchy = iotbx.pdb.input(file_name=pdb).construct_hierarchy()
  pdb_hierarchy.reset_i_seq_if_necessary()
  r = rotalyze(pdb_hierarchy=pdb_hierarchy)
  for rot in r.results:
    model_hash[rot.id_str()] = rot.rotamer_name
    model_score[rot.id_str()] = rot.score
  assert model_hash[' A   3  ASN'] == 'OUTLIER', model_hash[' A   3  ASN']
  assert model_hash[' A   7  TYR'] == 'OUTLIER', model_hash[' A   7  TYR']
  #
  args = [
    pdb,
    hkl,
    "main.number_of_macro=2",
    "reference_model.enabled=True",
    "reference_model.file=%s"%reference_model,
    "strategy=individual_sites"]
  r = run_phenix_refine(args = args, prefix = prefix)
  #
  assert is_above_limit(r.r_work_start, 0.35)
  assert r.r_work_final < 0.11
  assert is_above_limit(r.r_free_start, 0.45)
  assert r.r_free_final < 0.13
  model_hash = {}
  model_score = {}
  pdb_hierarchy = iotbx.pdb.input(file_name=r.pdb).construct_hierarchy()
  pdb_hierarchy.reset_i_seq_if_necessary()
  r = rotalyze(pdb_hierarchy=pdb_hierarchy)
  for rot in r.results:
    model_hash[rot.id_str()] = rot.rotamer_name
    model_score[rot.id_str()] = rot.score
  assert model_hash[' A   3  ASN'] == 't0', model_hash[' A   3  ASN']
  assert model_hash[' A   7  TYR'] == 'm-80', model_hash[' A   7  TYR']

if (__name__ == "__main__") :
  run()
  print("OK")
