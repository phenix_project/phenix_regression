from __future__ import division
from __future__ import print_function
from phenix_regression.refinement import run_phenix_refine
from libtbx.test_utils import approx_equal
import libtbx.load_env
import os

par_str="""\
refinement.refine.strategy                              = individual_adp
refinement.refine.adp.individual.isotropic              = XXX
refinement.main.fake_f_obs                              = true
refinement.modify_start_model.modify.adp.randomize      = true
refinement.modify_start_model.output.file_name          = ZZZ
refinement.main.bulk_solvent_and_scale                  = false
refinement.modify_start_model.modify.selection          = YYY
refinement.main.scattering_table                        = it1992
refinement.structure_factors_and_gradients_accuracy.cos_sin_table=false
refinement.structure_factors_and_gradients_accuracy.algorithm=direct
refinement.fake_f_obs.structure_factors_accuracy.cos_sin_table=false
refinement.fake_f_obs.structure_factors_accuracy.algorithm=direct
refinement.main.number_of_macro_cycles                  = 20
refinement.main.target                                  = ls
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  ADP iso individual.
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/enk_gm.pdb", test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/enk.hkl", \
    test=os.path.isfile)
  pdb_mod = "%s_mod.pdb"%prefix
  for sel_ref in ["None", "chain A or chain B or chain C"]:
    for sel_mod in ["None", "chain A or chain B or chain C"]:
      ### run job
      par_in = "%s.params"%prefix
      par_str_=par_str.replace("XXX",sel_ref).replace("YYY",sel_mod).\
               replace("ZZZ",pdb_mod)
      open(par_in, "w").write(par_str_)
      args = [pdb, hkl, par_in]
      r = run_phenix_refine(args = args, prefix=prefix)
      ### collect results
      atoms0 = []
      for line in open(pdb,"r").read().splitlines():
        if(line.startswith("ATOM")): atoms0.append(line)
      atoms1 = []
      for line in open(pdb_mod,"r").read().splitlines():
        if(line.startswith("ATOM")): atoms1.append(line)
      atoms2 = []
      for line in open(r.pdb,"r").read().splitlines():
        if(line.startswith("ATOM")): atoms2.append(line)
      assert len(atoms0) == len(atoms1) == len(atoms2)
      ### analyse r-factors
      assert r.r_work_start > 0.1
      assert r.r_free_start > 0.1
      if(sel_ref == sel_mod or sel_ref == "None"):
        assert r.r_work_final < 0.0007
        assert r.r_free_final < 0.0007
      else:
        assert r.r_work_final > 0.05
        assert r.r_free_final > 0.05
      ### analyse files
      for a0, a1, a2 in zip(atoms0, atoms1, atoms2):
        ch0, ch1, ch2 = a0.split()[4], a1.split()[4], a2.split()[4]
        b0 = float(a0.split()[10])
        b1 = float(a1.split()[10])
        b2 = float(a2.split()[10])
        assert ch0 == ch1 == ch2
        if(sel_mod == "None"):
           a0 != a1
           if(sel_ref == "None"):
              assert approx_equal(b0,b2,1.e-2)
           else:
              if(ch0 in ["D","E"]):
                 a2 == a1
              else:
                 a0 != a2
        else:
           assert approx_equal(b0,b2,1.e-2)
           if(sel_ref != "None"):
              if(ch0 in ["D","E"]):
                 a2 == a1
              else:
                 a2 != a1
           else:
              a2 != a1
           if(ch0 in ["D","E"]):
              ch0 == ch1
           else:
              ch0 != ch1

if (__name__ == "__main__"):
  run()
  print("OK")
