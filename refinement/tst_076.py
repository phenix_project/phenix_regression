from __future__ import division
from __future__ import print_function
from phenix_regression.refinement import run_phenix_refine
from libtbx.test_utils import approx_equal
import os, random
from scitbx.array_family import flex
import libtbx.load_env

random.seed(0)
flex.set_random_seed(0)

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Make sure phenix.refine does not diverge starting from R=0. ML.
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/enk_gbr.pdb", test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/enk_r1A.mtz", \
    test=os.path.isfile)
  args =[
    pdb,hkl,
    "main.target=ml",
    "strategy=individual_sites+individual_adp+occupancies",
    "main.bulk_solvent_and_scale=false",
    "xray_data.high_resolution=1.0",
    "main.number_of_macro_cycles=10",
    "fake_f_obs=true","wu=0.0","wc=0.0"]
  r = run_phenix_refine(args = args, prefix=prefix)
  # with at least print out (1.e-4) precision:
  #    all r-factors must be ZERO;
  #    all start bond/angles deviations must be equal to final ones;
  r1 = r.r_work_start
  r2 = r.r_work_start
  assert approx_equal(round(r.r_work_start,2), round(r.r_work_final,2))
  assert approx_equal(round(r.r_free_start,2), round(r.r_free_final,2))
  assert approx_equal(r.r_work_start, 0.0)

if (__name__ == "__main__"):
  run()
  print("OK")
