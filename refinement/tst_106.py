from __future__ import print_function
from phenix_regression.refinement import get_r_factors
import libtbx.load_env
from libtbx.test_utils import approx_equal, not_approx_equal, run_command
import sys, os, time, random
from mmtbx import utils
from cctbx.array_family import flex
import iotbx.pdb
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel
import os

if (1):
  random.seed(0)
  flex.set_random_seed(0)

common_params = [
  'main.target=ls',
  'main.bulk_solvent_and_=false',
  'optimize_scattering_contribution=false',
  'ls_target_names.target_name=ls_wunit_kunit',
  'structure_factors_and_gradients_accuracy.algorithm=direct',
  'xray_data.r_free_flags.generate=True',
  'xray_data.r_free_flags.ignore_r_free_flags=true',
  "xray_data.outliers_rejection=false",
  "neutron_data.outliers_rejection=false"]

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Occupancies.
  """
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/enk_gor.pdb",
    test=os.path.isfile)
  pdb_file_to_refine = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/enk_gor_e.pdb",
    test=os.path.isfile)
  args = [
    pdb_file,
    'high_resolution=2.0',
    'algorithm=direct',
    'label=FOBS',
    'type=real',
    'output.file_name=%s']
  r0 = run_fmodel(args = args, prefix = prefix)
  #
  args = [
    pdb_file_to_refine,
    r0.mtz,
    'main.number_of_macro_cycles=3',
    'strategy=occupancies',
    'main.occupancy_max=100','main.occupancy_min=-100',
    'constrained_group.selection="%s"'%"chain A",
    'constrained_group.selection="%s"'%"chain B",
    'constrained_group.selection="%s"'%"chain C",
    'constrained_group.selection="%s"'%"chain D"] + common_params
  r = run_phenix_refine(args = args, prefix = prefix)
  #
  assert r.r_work_start > 0.3
  assert r.r_free_start > 0.3
  assert approx_equal(r.r_work_final, 0)
  assert approx_equal(r.r_free_final, 0)
  occ1 = iotbx.pdb.input(file_name = pdb_file).xray_structure_simple(
    ).scatterers().extract_occupancies()
  occ2 = iotbx.pdb.input(file_name = r.pdb
    ).xray_structure_simple().scatterers().extract_occupancies()
  assert approx_equal(occ1, occ2)

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK", "time: %.2f"%(time.time()-t0))
