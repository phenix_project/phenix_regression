from __future__ import print_function
import libtbx.load_env
from libtbx.test_utils import approx_equal
import sys, os, time
from mmtbx import utils
from cctbx.array_family import flex

from phenix_regression.refinement import run_phenix_refine

ddir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="refinement/data/eric_ennifar_ias",
  test=os.path.isdir)

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  ???
  """
  args = [
    "%s/%s"%(ddir,"PN33b.mtz"),
    "%s/%s"%(ddir,"start.ligands.cif"),
    "%s/%s"%(ddir,"start.updated.pdb"),
    "strategy=none main.number_of_macro=1"]
  r = run_phenix_refine(args = args, prefix = prefix)
  assert approx_equal(r.r_work_final, 0.0505, 0.001)

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK", "time: %.2f"%(time.time()-t0))
