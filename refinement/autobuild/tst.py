from __future__ import print_function
import os, time
from libtbx import easy_run
import libtbx.load_env

def exercise():
  if (not libtbx.env.has_module(name="solve_resolve")):
    print("Skipping autobuild test: solve_resolve not available.")
    return
  os.mkdir("tst_refine_autobuild")
  os.chdir("tst_refine_autobuild")
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/autobuild/coords.pdb",
    test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/autobuild/native.sca",
    test=os.path.isfile)
  cmd = " ".join([
    "phenix.autobuild",
    "%s"%hkl,
    "super_quick=true",
    "model=%s"%pdb,
    "refine=true",
    "ncycle_refine=1",
    "> tmp.log",
    "debug=true",
    "verbose=true",
  ])
  print(cmd)
  assert not easy_run.call(cmd)
  assert os.path.isfile(os.sep.join(
      ["AutoBuild_run_1_", "overall_best.pdb"])), ""+\
      "If we don't have the file, something went wrong, check logs."

if (__name__ == "__main__"):
  t0 = time.time()
  exercise()
  print("OK time=%8.3f"%(time.time() - t0))
