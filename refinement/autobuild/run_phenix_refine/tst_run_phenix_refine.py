from __future__ import division
from __future__ import print_function

from six.moves import cStringIO as StringIO
import os
import libtbx.load_env
import difflib

data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="refinement/autobuild/run_phenix_refine",
  test=os.path.isdir)

def remove_blank(text):
  return text.replace(" ","").replace("\n","")

def tst_01a():

  sg="P21"
  cell=[ 30.,35.,30.,90.,100.,90.]
  macro_cycles=3
  pdb_file=os.path.join(data_dir,'noligand.pdb')
  reference_model=pdb_file
  lig_file=None
  edited_model_file='LIG_EDITED_EDITED_noligand.pdb' # goes in this directory
  mtz_file=os.path.join(data_dir,'4chains_fobs_4A.mtz')
  cif_file=None
  refine_eff_file_list=[]
  cif_def_file_list=[]
  input_lig_file_list=[]
  output_pdb_file="refine.pdb"
  output_log_file="refine.log"
  output_mtz_file_with_free_r_flags = 'output_mtz_file.mtz'

  print("\nRunning refinement with reference model\n")

  keep_pdb_atoms=False
  allow_overlapping=False

  # make sure ligand atoms are all found
  expected_text=""
  expected_text_in_edited_model=""
  expected_text_not_in_edited_model=""

  test_run_phenix_refine(sg=sg,cell=cell,macro_cycles=macro_cycles,
    pdb_file=pdb_file,mtz_file=mtz_file,output_pdb_file=output_pdb_file,
    reference_model=reference_model,
    output_log_file=output_log_file,
    refine_eff_file_list=refine_eff_file_list,
    cif_def_file_list=cif_def_file_list,
    input_lig_file_list=input_lig_file_list,
    keep_pdb_atoms=keep_pdb_atoms,
    allow_overlapping=allow_overlapping,
    expected_text=expected_text,
    expected_text_in_edited_model=expected_text_in_edited_model,
    expected_text_not_in_edited_model=expected_text_not_in_edited_model,
    edited_model_file=edited_model_file,
    skip_refinement=False,
    output_mtz_file_with_free_r_flags = output_mtz_file_with_free_r_flags,
   )


def tst_01():

  sg="P21"
  cell=[ 30.,35.,30.,90.,100.,90.]
  macro_cycles=3
  pdb_file=os.path.join(data_dir,'noligand.pdb')
  reference_model=pdb_file
  lig_file=None
  edited_model_file='LIG_EDITED_EDITED_noligand.pdb' # goes in this directory
  mtz_file=os.path.join(data_dir,'4chains_fobs_4A.mtz')
  cif_file=None
  refine_eff_file_list=[]
  cif_def_file_list=[]
  input_lig_file_list=[]
  output_pdb_file="refine.pdb"
  output_log_file="refine.log"

  print("\nRunning refinement with reference model\n")

  keep_pdb_atoms=False
  allow_overlapping=False

  # make sure ligand atoms are all found
  expected_text=""
  expected_text_in_edited_model=""
  expected_text_not_in_edited_model=""

  test_run_phenix_refine(sg=sg,cell=cell,macro_cycles=macro_cycles,
    pdb_file=pdb_file,mtz_file=mtz_file,output_pdb_file=output_pdb_file,
    reference_model=reference_model,
    output_log_file=output_log_file,
    refine_eff_file_list=refine_eff_file_list,
    cif_def_file_list=cif_def_file_list,
    input_lig_file_list=input_lig_file_list,
    keep_pdb_atoms=keep_pdb_atoms,
    allow_overlapping=allow_overlapping,
    expected_text=expected_text,
    expected_text_in_edited_model=expected_text_in_edited_model,
    expected_text_not_in_edited_model=expected_text_not_in_edited_model,
    edited_model_file=edited_model_file,
    skip_refinement=False
   )


def tst_02():

  sg="P21"
  cell=[ 30.,35.,30.,90.,100.,90.]
  macro_cycles=0
  pdb_file=os.path.join(data_dir,'noligand.pdb')
  lig_file=os.path.join(data_dir,'ligand_overlapping.pdb')
  edited_model_file='LIG_EDITED_EDITED_noligand.pdb' # goes in this directory
  mtz_file=os.path.join(data_dir,'4chains_fobs_4A.mtz')
  cif_file=os.path.join(data_dir,'ligand.cif')
  refine_eff_file_list=[]
  cif_def_file_list=[cif_file]
  input_lig_file_list=[lig_file]
  output_pdb_file="refine.pdb"
  output_log_file="refine.log"

  print("\nRunning refinement setup with keep_pdb_atoms=False\n")

  keep_pdb_atoms=False
  allow_overlapping=False

  # make sure ligand atoms are all found
  expected_text=""
  expected_text_in_edited_model="""
HETATM   30  N1  LIG C  92      10.991  -1.451   1.716  1.00 16.05      P9   N
HETATM   31  O1  LIG C  92      10.924  -2.975   1.574  1.00 16.32      P9   O
HETATM   32  O2  LIG C  92      12.268  -3.629   1.888  0.65 16.46      P9   O
HETATM   33  S1  LIG C  92      10.579  -3.235   0.175  1.00 16.04      P9   S
"""
  expected_text_not_in_edited_model="""
ATOM      1  N   GLN R  92      10.579  -3.235   0.175  1.00 16.04           N
ATOM      2  CA  GLN R  92      10.924  -2.975   1.574  1.00 16.32           C
ATOM      3  CB  GLN R  92      12.268  -3.629   1.888  0.65 16.46           C
ATOM      4  C   GLN R  92      10.991  -1.451   1.716  1.00 16.05           C
  """


  test_run_phenix_refine(sg=sg,cell=cell,macro_cycles=macro_cycles,
    pdb_file=pdb_file,mtz_file=mtz_file,output_pdb_file=output_pdb_file,
    output_log_file=output_log_file,
    refine_eff_file_list=refine_eff_file_list,
    cif_def_file_list=cif_def_file_list,
    input_lig_file_list=input_lig_file_list,
    keep_pdb_atoms=keep_pdb_atoms,
    allow_overlapping=allow_overlapping,
    expected_text=expected_text,
    expected_text_in_edited_model=expected_text_in_edited_model,
    expected_text_not_in_edited_model=expected_text_not_in_edited_model,
    edited_model_file=edited_model_file,
    skip_refinement=True
   )

  # Now with keep_pdb_atom=True (remove overlapping ligand atoms)

  print("\nRunning refinement setup with keep_pdb_atoms=True\n")
  keep_pdb_atoms=True
  allow_overlapping=False
  expected_text_not_in_edited_model="""
HETATM   30  N1  LIG C  92      10.991  -1.451   1.716  1.00 16.05      P9   N
HETATM   31  O1  LIG C  92      10.924  -2.975   1.574  1.00 16.32      P9   O
HETATM   32  O2  LIG C  92      12.268  -3.629   1.888  0.65 16.46      P9   O
HETATM   33  S1  LIG C  92      10.579  -3.235   0.175  1.00 16.04      P9   S
  """
  expected_text_in_edited_model="""
ATOM      1  N   GLN R  92      10.579  -3.235   0.175  1.00 16.04           N
ATOM      2  CA  GLN R  92      10.924  -2.975   1.574  1.00 16.32           C
ATOM      3  C   GLN R  92      10.991  -1.451   1.716  1.00 16.05           C
ATOM      4  O   GLN R  92      11.706  -0.791   0.966  1.00 15.84           O
ATOM      5  CB  GLN R  92      12.268  -3.629   1.888  0.65 16.46           C
"""


  test_run_phenix_refine(sg=sg,cell=cell,macro_cycles=macro_cycles,
    pdb_file=pdb_file,mtz_file=mtz_file,output_pdb_file=output_pdb_file,
    output_log_file=output_log_file,
    refine_eff_file_list=refine_eff_file_list,
    cif_def_file_list=cif_def_file_list,
    input_lig_file_list=input_lig_file_list,
    keep_pdb_atoms=keep_pdb_atoms,
    allow_overlapping=allow_overlapping,
    expected_text=expected_text,
    expected_text_in_edited_model=expected_text_in_edited_model,
    expected_text_not_in_edited_model=expected_text_not_in_edited_model,
    edited_model_file=edited_model_file,
    skip_refinement=True)

  # Now with keep_pdb_atom=True (remove overlapping ligand atoms) and
  #  allow_overlapping=True

  print("\nRunning refinement setup with keep_pdb_atoms=True and allow_overlapping=True\n")
  keep_pdb_atoms=True
  allow_overlapping=True

  expected_text_not_in_edited_model="""
  """
  expected_text_in_edited_model="""
ATOM      1  N   GLN R  92      10.579  -3.235   0.175  1.00 16.04           N
ATOM      2  CA  GLN R  92      10.924  -2.975   1.574  1.00 16.32           C
ATOM      3  C   GLN R  92      10.991  -1.451   1.716  1.00 16.05           C
ATOM      4  O   GLN R  92      11.706  -0.791   0.966  1.00 15.84           O
ATOM      5  CB  GLN R  92      12.268  -3.629   1.888  0.65 16.46           C
ATOM      1  N   GLN R  92      10.579  -3.235   0.175  1.00 16.04           N
ATOM      2  CA  GLN R  92      10.924  -2.975   1.574  1.00 16.32           C
ATOM      3  C   GLN R  92      10.991  -1.451   1.716  1.00 16.05           C
ATOM      4  O   GLN R  92      11.706  -0.791   0.966  1.00 15.84           O
ATOM      5  CB  GLN R  92      12.268  -3.629   1.888  0.65 16.46           C
  """

  test_run_phenix_refine(sg=sg,cell=cell,macro_cycles=macro_cycles,
    pdb_file=pdb_file,mtz_file=mtz_file,output_pdb_file=output_pdb_file,
    output_log_file=output_log_file,
    refine_eff_file_list=refine_eff_file_list,
    cif_def_file_list=cif_def_file_list,
    input_lig_file_list=input_lig_file_list,
    keep_pdb_atoms=keep_pdb_atoms,
    allow_overlapping=allow_overlapping,
    expected_text=expected_text,
    expected_text_in_edited_model=expected_text_in_edited_model,
    expected_text_not_in_edited_model=expected_text_not_in_edited_model,
    edited_model_file=edited_model_file,
    skip_refinement=True)


  print("\nRunning refinement setup with keep_pdb_atoms=False and allow_overlapping=True\n")
  keep_pdb_atoms=False
  allow_overlapping=True

  expected_text_not_in_edited_model="""
  """
  expected_text_in_edited_model="""
ATOM      1  N   GLN R  92      10.579  -3.235   0.175  1.00 16.04           N
ATOM      2  CA  GLN R  92      10.924  -2.975   1.574  1.00 16.32           C
ATOM      3  C   GLN R  92      10.991  -1.451   1.716  1.00 16.05           C
ATOM      4  O   GLN R  92      11.706  -0.791   0.966  1.00 15.84           O
ATOM      5  CB  GLN R  92      12.268  -3.629   1.888  0.65 16.46           C
ATOM      1  N   GLN R  92      10.579  -3.235   0.175  1.00 16.04           N
ATOM      2  CA  GLN R  92      10.924  -2.975   1.574  1.00 16.32           C
ATOM      3  C   GLN R  92      10.991  -1.451   1.716  1.00 16.05           C
ATOM      4  O   GLN R  92      11.706  -0.791   0.966  1.00 15.84           O
  """

  test_run_phenix_refine(sg=sg,cell=cell,macro_cycles=macro_cycles,
    pdb_file=pdb_file,mtz_file=mtz_file,output_pdb_file=output_pdb_file,
    output_log_file=output_log_file,
    refine_eff_file_list=refine_eff_file_list,
    cif_def_file_list=cif_def_file_list,
    input_lig_file_list=input_lig_file_list,
    keep_pdb_atoms=keep_pdb_atoms,
    allow_overlapping=allow_overlapping,
    expected_text=expected_text,
    expected_text_in_edited_model=expected_text_in_edited_model,
    expected_text_not_in_edited_model=expected_text_not_in_edited_model,
    edited_model_file=edited_model_file,
    skip_refinement=True)


def tst_03():

  sg="P21"
  cell=[ 30.,35.,30.,90.,100.,90.]
  macro_cycles=0
  pdb_file=os.path.join(data_dir,'4chains.pdb')
  mtz_file=os.path.join(data_dir,'4chains_fobs_4A.mtz')
  refine_eff_file_list=[]
  cif_def_file_list=[]
  output_pdb_file="refine.pdb"
  output_log_file="refine.log"
  print("Running refinement setup with 4 chains and NCS...")

  # make sure NCS is found
  # single phi or psi on the chain edges are not selected anymore
  expected_text="""Determining NCS matches...
--------------------------------------------------------
Torsion NCS Matching Summary:
 PHE A  93  <=>  PHE D  93
 PHE A  94  <=>  PHE D  94
 ASP A  95  <=>  ASP D  95
 MET A  96  <=>  MET D  96
 ARG A  97  <=>  ARG D  97
 LEU C  93  <=>  LEU R  93
 MET C  94  <=>  MET R  94
 ASP C  95  <=>  ASP R  95
 MET C  96  <=>  MET R  96
 ARG C  97  <=>  ARG R  97
"""

  test_run_phenix_refine(sg=sg,cell=cell,macro_cycles=macro_cycles,
    pdb_file=pdb_file,mtz_file=mtz_file,output_pdb_file=output_pdb_file,
    output_log_file=output_log_file,
    refine_eff_file_list=refine_eff_file_list,
    cif_def_file_list=cif_def_file_list,
    expected_text=expected_text
   )

def tst_04():

  sg="P21"
  cell=[ 30.,35.,30.,90.,100.,90.]
  macro_cycles=0
  pdb_file=os.path.join(data_dir,'ligand.pdb')
  mtz_file=os.path.join(data_dir,'4chains_fobs_4A.mtz')
  refine_eff_file_list=[os.path.join(data_dir,'ligand.eff')]
  cif_def_file_list=[os.path.join(data_dir,'ligand.cif')]
  output_pdb_file="refine.pdb"
  output_log_file="refine.log"
  print("\nRunning setup with refine_eff_file_list and cif_def_file_list")

  # make sure twin law is used and cif file is used...will crash if cif is not
  expected_text="""twin_law = "l,-k,h"
"""

  test_run_phenix_refine(sg=sg,cell=cell,macro_cycles=macro_cycles,
    pdb_file=pdb_file,mtz_file=mtz_file,output_pdb_file=output_pdb_file,
    output_log_file=output_log_file,
    refine_eff_file_list=refine_eff_file_list,
    cif_def_file_list=cif_def_file_list,
    expected_text=expected_text
   )

def tst_05():

  sg="P21"
  cell=[ 30.,35.,30.,90.,100.,90.]
  macro_cycles=1
  pdb_file=os.path.join(data_dir,'4chains.pdb')
  mtz_file=os.path.join(data_dir,'4chains_fobs_4A.mtz')
  refine_eff_file_list=[]
  cif_def_file_list=[]
  output_pdb_file="refine.pdb"
  output_log_file="refine.log"
  print("\nRunning 1 cycle with 4 chains and NCS\n")

  # make sure R free is expected value (take 1 significant digit):
  #
  expected_text="""R(free) = 0.4
"""

  test_run_phenix_refine(sg=sg,cell=cell,macro_cycles=macro_cycles,
    pdb_file=pdb_file,mtz_file=mtz_file,output_pdb_file=output_pdb_file,
    output_log_file=output_log_file,
    refine_eff_file_list=refine_eff_file_list,
    cif_def_file_list=cif_def_file_list,
    expected_text=expected_text
   )



def get_params_from_log(text):
  spl=text.split("refinement {")
  if len(spl)>1:
    text="refinement {"+ text.split("refinement {")[1]
    text=text.split("=============")[0]
    return text
  return ""

def is_same(text1,text2,out=None):
  same=True
  for line in difflib.context_diff(text1.splitlines(),text2.splitlines()):
      if line and line[0] in ['!']:
        same=False
        if out is not None:
          print(line, file=out)
  return same


def test_run_phenix_refine(sg=None,cell=None,macro_cycles=None,
   pdb_file=None,mtz_file=None,output_pdb_file=None,output_log_file=None,
   reference_model=None,
   refine_eff_file_list=None,cif_def_file_list=None,input_lig_file_list=None,
   expected_text=None,
   expected_text_in_edited_model=None,
   expected_text_not_in_edited_model=None,
   edited_model_file=None,
   keep_pdb_atoms=True,
   allow_overlapping=True,
   output_mtz_file_with_free_r_flags = None,
   skip_refinement=False):

  from phenix.autosol.run_phenix_refine import run_phenix_refine

  print("\nFiles required... %s  %s" %(pdb_file,mtz_file))

  if edited_model_file and os.path.isfile(edited_model_file):
     # get rid of any existing
     os.remove(edited_model_file)

  f=StringIO()
  run_phenix_refine=run_phenix_refine(sg=sg,cell=cell,
     mtz_file_name=mtz_file,
     pdb_file_name=pdb_file,
     macro_cycles=macro_cycles,
     s_annealing=False,
     output_pdb_file=output_pdb_file,
     log=f,
     place_waters='No',
     verbose = True,
     debug = True,
     copy_text=False,
     write_map_files=True,
     output_mtz_file_with_free_r_flags = output_mtz_file_with_free_r_flags,
        Facts={
          'space_group':sg,
          'unit_cell':cell,
          'semet':False,
          'temp_dir':'.',
          'refine_with_ncs':'Yes',
          'ncs_in_refinement':'torsion',
          'cif_def_file_list':cif_def_file_list,
          'input_lig_file_list':input_lig_file_list,
          'keep_pdb_atoms':keep_pdb_atoms,
          'verbose':True,
          'debug':True,
          'fix_ligand_occupancy':True,
          'allow_overlapping':allow_overlapping,
          'reference_model':reference_model,
          'refine_se_occ':True,
          'chain_type':'PROTEIN',
          'refine_eff_file_list':refine_eff_file_list,
      })

  if not skip_refinement:
    refine_object=run_phenix_refine.refine_it()
    print("FINAL:",refine_object.fmodels.fmodel_xray().r_work(), \
                   refine_object.fmodels.fmodel_xray().r_free())

  # get log text
  log_text=f.getvalue()
  if output_log_file:
    ff=open(output_log_file,'w')
    print(log_text, file=ff)
    ff.close()

  # Make sure expected text is found

  if expected_text:
    print("\nChecking for expected text...")
    for line in expected_text.splitlines():
      if not line in log_text:
        print("\nLog file output not as expected:")
        print("\nNot found in log file output: \n'%s'\n"  %(line))
        raise AssertionError("FAILED")

  # and expected text from model
  if expected_text_in_edited_model:
    if not os.path.isfile(edited_model_file):
      print("\nExpected output file not found: %s" %(edited_model_file))
      raise AssertionError("FAILED")

    model_text=open(edited_model_file).read()
    for line in expected_text_in_edited_model.splitlines():
      if line.replace(" ","") and not line in model_text:
        print("\nEdited model file output not as expected:")
        print("\nNot found in edited model (%s): \n'%s'\n"  %(edited_model_file,
         line))
        raise AssertionError("FAILED")

  # and text that should not be in the model
  if expected_text_not_in_edited_model:
    model_text=open(edited_model_file).read()
    for line in expected_text_not_in_edited_model.splitlines():
      if line.replace(" ","") and line in model_text:
        print("\nEdited model file output not as expected:")
        print("\nFound in edited model (%s): \n'%s'\n"  %(edited_model_file,
         line))
        raise AssertionError("FAILED")

  # get parameters from log file
  params_as_logged=get_params_from_log(log_text)
  log_eff_file=output_log_file.replace(".log","_eff.log")
  if output_log_file:
    print("Wrote params as logged to %s" %(output_log_file))
    ff=open(log_eff_file,'w')
    print(params_as_logged, file=ff)
    ff.close()

  print("OK")


if __name__=="__main__":
  tst_01a()
  tst_01()
  tst_02()
  tst_03()
  tst_04()
  tst_05()
