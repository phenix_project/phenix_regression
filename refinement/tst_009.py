from __future__ import division
from __future__ import print_function
import libtbx.load_env
import libtbx.path
import os
from phenix_regression.refinement import run_phenix_refine

def run(prefix = os.path.basename(__file__).replace(".py","")):
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/pdb1akg_very_well_phenix_refined_001.pdb",
    test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/1akg_TenFobsZero.mtz",
    test=os.path.isfile)
  args = [
    pdb,
    hkl,
    "main.number_of_macro=1",
    "main.bulk_solv=false",
    "strategy=individual_sites",
    "xray_data.outliers_rejection=false",
    "neutron_data.outliers_rejection=false"
  ]
  r = run_phenix_refine(args = args, prefix = prefix)
  counter = 0
  for line in open(r.log,"r").readlines():
    if(line.count("A, n_refl.=")==1):
      counter += 1
      assert line.count("A, n_refl.=874 (all),")
  assert counter == 3

if (__name__ == "__main__"):
  run()
  print("OK")
