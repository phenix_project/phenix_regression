from libtbx.utils import search_for
import libtbx.load_env
import sys, os
from phenix_regression.refinement import run_phenix_refine
from libtbx.test_utils import approx_equal

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Exercise group_anomalous option in refinement.
  """
  d = libtbx.env.under_dist(
    module_name="phenix_regression",
    path="refinement/data/group_anomalous",
    test=os.path.isdir)
  args = [
    '%s/1mdg.mtz' % d,
    '%s/pdb1mdg.ent' % d,
    '%s/bgm.cif' % d,
    '%s/anomalous_scatterers' % d,
    'refine.strategy=group_anomalous',
    'main.number_of_macro_cycles=3']
  r = run_phenix_refine(args = args, prefix = prefix)
  assert approx_equal(r.r_work_start, 0.2708, 0.005)
  assert approx_equal(r.r_free_start, 0.3034, 0.005)
  assert approx_equal(r.r_work_final, 0.1858, 0.005)
  assert approx_equal(r.r_free_final, 0.2077, 0.005)
  lines = search_for(
    pattern="f_double_prime = 3.07",
    mode="find",
    file_name=r.dff)
  assert len(lines) == 1

if (__name__ == "__main__"):
  run()
