from __future__ import division
from __future__ import print_function
import libtbx.load_env
import libtbx.path
import os
from phenix_regression.refinement import run_phenix_refine

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  ADP refinement fails without ignore_line_search_failed_step_at_upper_bound=True .
  RuntimeError: lbfgs error: Line search failed: The step is at the upper bound stpmax().
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/data/%s.pdb"%prefix,
    test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/data/%s.mtz"%prefix,
    test=os.path.isfile)
  eff = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/data/%s.eff"%prefix,
    test=os.path.isfile)
  args = [
    pdb,
    hkl,
    eff,
    "ncs.type=constraints",
    "ncs_search.enabled=true",
    "strategy=individual_adp"]
  r = run_phenix_refine(args = args, prefix = prefix, geo_expected=False,
    mtz_expected=False)
  #

if (__name__ == "__main__") :
  run()
  print("OK")
