from libtbx.test_utils import is_below_limit, is_above_limit
from libtbx.utils import search_for, Sorry
from libtbx import group_args
from libtbx.test_utils import run_command
import os
from libtbx.test_utils import approx_equal
from scitbx.array_family import flex
from libtbx import introspection

class get_r_factors(object): # XXX to be removed

  def __init__(self, file_name):
    self.r_work_start = None
    self.r_free_start = None
    self.r_work_final = None
    self.r_free_final = None
    self.d_min        = None
    values = []
    for sf in ["Start", "Final"]:
      lines = search_for(
        pattern="REMARK %s: r_work = " % sf,
        mode="startswith",
        file_name=file_name)
      if(len(lines)>0):
        flds = lines[0].split()
        values.extend([float(flds[i]) for i in [4,7]])
    if(len(values)>0):
      self.r_work_start = float(values[0])
      self.r_free_start = float(values[1])
      self.r_work_final = float(values[2])
      self.r_free_final = float(values[3])
    lines = search_for(
        pattern="REMARK   3   RESOLUTION RANGE HIGH (ANGSTROMS) :",
        mode="startswith",
        file_name=file_name)
    if(len(lines)>0):
      self.d_min = float(lines[0].split()[len(lines[0].split())-1])

def run_program_and_check(program = None, args = None, sorry_expected = None,
     std_out_file = None, log_file = None):

    """ Run program with args...
     Catch crashes.  Allow them if sorry_expected, raise error otherwise,.
     Raise error if Sorry or Traceback found in output.
     If std_out_file and log_file names are supplied, write stdout to
       std_out_file, assume program will write to log_file,
       and raise error if std_out_file and log_file not identical.
    """

    # ====================================================================
    # Check inputs
    # ====================================================================

    assert args is not None
    assert program is not None
    assert (std_out_file,log_file).count(None) in [0,2]

    # ====================================================================
    # Set up log file(s) and capture sys.stdout
    # ====================================================================

    import sys
    from six.moves import StringIO

    from libtbx.utils import multi_out
    logger = multi_out()

    if std_out_file:
      std_out_f = open(std_out_file, 'w')  # logger output goes to self.std_out
      logger.register("std_out_file", std_out_f)
      compare_log_and_std_out = True
      print("\nWriting stdout to %s and assuming program will write to %s" %(
        std_out_file, log_file))
    else:
      std_out_f = StringIO()
      compare_log_and_std_out = False
      print("Capturing stdout directly (no files written)")

    parser_log = StringIO()
    logger.register("parser_log", parser_log)  # needed in phenix.refine

    sys.stdout = std_out_f # capture any unspecified output in self.std_out

    # ====================================================================
    # Run the program and catch crashes
    # ====================================================================

    try:
      result =  program(args=args, logger = logger)
    except Exception as e:
      # --------------------------------------------------------------------
      # Catch crash and decide what to do
      # --------------------------------------------------------------------
      sys.stdout.flush()
      sys.stderr.flush()
      if compare_log_and_std_out:
        std_out_f.close()
        log_text = open(std_out_f.name).read()
      else:
        log_text = std_out_f.getvalue()
      sys.stdout = sys.__stdout__ # replace original stdout
      sys.stderr = sys.__stderr__ # replace original stderr
      print()
      if sorry_expected:
        text = "Sorry: %s" %(str(e))
        print("Note: crash was expected and occurred in "+
        "this test (sorry_expected=True).\n")
        if compare_log_and_std_out:
          for fn in (std_out_file, log_file):
            assert os.path.isfile(std_out_file)
            assert os.path.isfile(log_file)
            working_text = open(fn).read() + "\n" + text
            f = open(fn,'w')
            print(working_text, file = f)
            f.close()
        return # all done with expected failure
      else: # Actual crash
        text = str(e)
        print(text)
        raise AssertionError("FAILED")


    # ====================================================================
    # Close std_out stream,  get value, restore sys.stdout and sys.stderr
    # ====================================================================

    if compare_log_and_std_out:
      std_out_f.close()
      std_out_text = open(std_out_f.name).read()
    else:
      std_out_text = std_out_f.getvalue()

    sys.stdout = sys.__stdout__ # replace original stdout REQUIRED
    sys.stderr = sys.__stderr__ # replace original stderr REQUIRED

    # ====================================================================
    # Make sure it did crash if expected to
    # ====================================================================

    if sorry_expected:
      print("\nSorry was expected but program ended without a Sorry\n")
      assert (not sorry_expected)



    # ====================================================================
    # Check output
    # ====================================================================

    # Check for Traceback or Sorry
    assert std_out_text.find("Traceback") < 0
    assert std_out_text.find("Sorry") < 0

    # Compare std_out and log file (this catches accidental writing to stdout)
    if compare_log_and_std_out:
      log_text = open(log_file).read() # captured stdout
      from libtbx.test_utils import show_diff
      if (show_diff(log_text, std_out_text)):
        print("\nLog files %s and %s do not match\n" %(std_out_file, log_file))
        introspection.show_stack(
          frames_back=1, reverse=True, prefix="INFO_LOG_STDOUT_DIFFERENCE: ")
        print("ERROR_LOG_STDOUT_DIFFERENCE")
        raise AssertionError("FAILED")
      else:
        print("Log files %s and %s match\n" %(std_out_file, log_file))

    sys.stdout.flush()
    return

def from_pdb_or_log(file_name, source):
  assert source in ["pdb","log"]
  fo = open(file_name, "r")
  rws,rfs,rwf,rff, bond_s,bond_f, angle_s,angle_f = [None,]*8
  cntr = 0
  for line in fo.readlines():
    l = line[:].strip().split()
    if(len(l)<1): continue
    if(source=="pdb"):
      if(line.startswith("REMARK   3   R VALUE            (WORKING SET) :")):
        rwf=float(l[7])
        cntr+=1
      if(line.startswith("REMARK   3   FREE R VALUE                     :")):
        rff=float(l[6])
        cntr+=1
      if(line.startswith("REMARK   3    BOND      :")):
        bond_f=float(l[4])
        cntr+=1
      if(line.startswith("REMARK   3    ANGLE     :")):
        angle_f=float(l[4])
        cntr+=1
    if(source=="log"):
      if(line.startswith("Start R-work =")):
        rws,rfs=float(l[3].replace(",","")),float(l[6].replace(",",""))
        cntr+=1
      if(line.startswith("Final R-work =")):
        rwf,rff=float(l[3].replace(",","")),float(l[6].replace(",",""))
        cntr+=1
      if(l[0]=="0" and l[1]==":" and not l[2].isalpha()):
        bond_s, angle_s = float(l[4]),float(l[5])
        cntr+=1
      if(l[0]=="end:"):
        bond_f, angle_f = float(l[3]),float(l[4])
        cntr+=1
  fo.close()
  assert cntr == 4
  return group_args(
    r_work_start = rws,
    r_work_final = rwf,
    r_free_start = rfs,
    r_free_final = rff,
    bond_start   = bond_s,
    bond_final   = bond_f,
    angle_start  = angle_s,
    angle_final  = angle_f)

class run_fmodel(object):
  def __init__(self, args, prefix):
    assert isinstance([], list)
    assert isinstance(prefix, str)
    self.mtz     = "%s.mtz"%prefix
    self.std_out = "%s.log"%prefix
    args += ["output.file_name=%s.mtz"%prefix]
    # filter out output redirect
    tmp = []
    for a in args:
      if(a.count(">")): continue
      else: tmp.append(a)
    args = tmp[:]
    #
    self.cmd = " ".join(["phenix.fmodel"] + args + [">%s"%self.std_out])
    result_file_names = [self.mtz, self.std_out]
    run_command(
      command           = self.cmd,
      verbose           = 0,
      stdout_file_name  = self.std_out,
      result_file_names = result_file_names)
    assert os.path.isfile(self.mtz)

def check_for_leading_whitespaces(file_name):
  if os.path.isfile(file_name):
    with open(file_name, 'r') as f:
      for l in f.readlines():
        assert not l.startswith(" REMARK")

class run_phenix_refine(object):
  # XXX Enable mmCIF output and compare with PDB via pdb.hierarchy and Fcalc
  def __init__(self, args, prefix, sorry_expected=False, geo_expected=True,
               dff_expected=True, mtz_expected=True, pdb_expected=True,
               cif_expected=True, eff_expected=True):
    assert isinstance([], list)
    assert isinstance(prefix, str)
    self.log     = "%s_001.log"%prefix
    self.pdb     = "%s_001.pdb"%prefix
    self.cif     = "%s_001.cif"%prefix
    self.cif_ref = "%s_001.reflections.cif"%prefix
    self.mtz     = "%s_001.mtz"%prefix
    self.mtz_x   = "%s_001_xray.mtz"%prefix
    self.mtz_n   = "%s_001_neutron.mtz"%prefix
    self.eff     = "%s_001.eff"%prefix
    self.dff     = "%s_002.def"%prefix
    self.geo     = "%s_001.geo"%prefix
    self.geo_fin = "%s_001_final.geo"%prefix
    self.std_out = "%s.log"%prefix
    isf = os.path.isfile
    args += ["output.prefix=%s"%prefix]
    args += [" overwrite=True"]
    for x in [self.log, self.pdb, self.cif, self.cif, self.cif_ref,
        self.mtz, self.mtz_x, self.mtz_n, self.eff, self.dff, self.geo,
         self.geo_fin,self.std_out]:
      if os.path.isfile(x):  os.remove(x)

    print("phenix.refine %s" %(" ".join(args)))

    from phenix.programs.phenix_refine import run_phenix_refine
    from phenix_regression.refinement import run_program_and_check
    # Run the program, check for Sorry, check that std_out_file and
    #  log_file are identical if names are supplied.

    run_program_and_check(program = run_phenix_refine,
      args = args,
      sorry_expected = sorry_expected,
      std_out_file = self.std_out,
      log_file = self.log)

    if(not sorry_expected):
      for f in [self.log, self.std_out]:
        assert isf(f), f
      if(eff_expected): assert isf(self.eff)
      if(dff_expected): assert isf(self.dff)
      if(geo_expected): assert isf(self.geo)
      if(mtz_expected):
        if(isf(self.mtz)):
          pass
        else:
          msg = "One or two of these are expected:" + \
                " ".join([self.mtz, self.mtz_x, self.mtz_n])
          assert [isf(f) for f in [self.mtz_x, self.mtz_n]].count(True), msg
      if(pdb_expected and cif_expected and not(isf(self.pdb) or isf(self.cif))):
        msg = "One or two of these are expected:" + \
              " ".join([self.pdb, self.cif])
        assert [isf(f) for f in [self.pdb, self.cif]].count(True), msg
    if(not sorry_expected and os.path.isfile(self.pdb)): # no check for joint XN
      r = from_pdb_or_log(file_name = self.pdb, source = "pdb")
      # check consistency with log
      rl = from_pdb_or_log(file_name = self.log, source = "log")
      assert approx_equal(r.r_work_final, rl.r_work_final)
      assert approx_equal(r.r_free_final, rl.r_free_final)
      assert approx_equal(r.bond_final, rl.bond_final)
      assert approx_equal(r.angle_final, rl.angle_final)
      for k,v in zip(rl.__dict__.keys(), rl.__dict__.values()):
        self.__dict__[k] = v
      #
      check_for_leading_whitespaces(self.pdb)
    if(not sorry_expected):
      self._check_low_res()

  def check_final_r(self, r_work, r_free, eps, info_low_eps=0.01):
    assert is_below_limit(
     value=self.r_work_final, limit=r_work, eps=eps, info_low_eps=info_low_eps)
    assert is_below_limit(
     value=self.r_free_final, limit=r_free, eps=eps, info_low_eps=info_low_eps)

  def check_start_r(self, r_work, r_free, eps, info_low_eps=0.01):
    assert is_above_limit(value=self.r_work_start, limit=r_work, eps=eps)
    assert is_above_limit(value=self.r_free_start, limit=r_free, eps=eps)

  def _check_low_res(self):
    if not os.path.isfile(self.pdb): return
    if not os.path.isfile(self.cif): return
    d_maxs = flex.double()
    fo = open(self.pdb, "r")
    for line in fo.readlines():
      if(line.startswith("REMARK   3   RESOLUTION RANGE LOW  (ANGSTROMS) :")):
        l = line.strip().split()
        d_maxs.append(float(l[7]))
      if(line.startswith("REMARK   3     1 ")):
        l = line.strip().split()
        d_maxs.append(float(l[3]))
    fo.close()
    fo = open(self.cif, "r")
    for line in fo.readlines():
      if(line.startswith("_refine.ls_d_res_low")):
        l = line.strip().split()
        d_maxs.append(float(l[1]))
    fo.close()
    assert d_maxs.size()==3
    d_maxs = flex.double([round(d, 2) for d in d_maxs])
    assert d_maxs.all_eq(d_maxs[0]), list(d_maxs)

def check_r_factors(
      file_name,
      r_work_final_ref,
      r_free_final_ref,
      r_work_start_ref=None,
      r_free_start_ref=None,
      eps=0.005,
      info_low_eps=0.01):
  r = get_r_factors(file_name=file_name)
  result = True
  if([r_work_start_ref, r_free_start_ref].count(None)==0):
    values_limits = [
      (r.r_work_start, r_work_start_ref),
      (r.r_free_start, r_free_start_ref),
      (r.r_work_final, r_work_final_ref),
      (r.r_free_final, r_free_final_ref)]
  else:
    values_limits = [
      (r.r_work_final, r_work_final_ref),
      (r.r_free_final, r_free_final_ref)]
  for value,limit in values_limits:
    if (not is_below_limit(
          value=value, limit=limit, eps=eps, info_low_eps=info_low_eps)):
      result = False
  return result
