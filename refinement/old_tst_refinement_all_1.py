from __future__ import print_function
from libtbx import easy_run
import libtbx.load_env
import sys, os
from cctbx.array_family import flex

task="""\
refinement.refine.strategy             = ZZZ
refinement.main.ordered_solvent        = true
refinement.main.simulated_annealing    = QQQ
refinement.output.prefix               = XXX
refinement.main.number_of_macro_cycles = 2
refinement.input.xray_data.high_resolution        = 3.0
refinement.main.max_number_of_iterations = 5

refinement.main.fake_f_obs             = true
refinement.fake_f_obs.k_sol            = 0.37
refinement.fake_f_obs.b_sol            = 37.0
refinement.fake_f_obs.b_cart           = 10 20 -60 0 0 0

refinement.modify_start_model.sites.shake            = 0.3
refinement.modify_start_model.sites.translate        = 0.1 0.2 0.3
refinement.modify_start_model.sites.rotate           = 0.4 0.5 0.6
refinement.modify_start_model.adp.randomize          = true
refinement.modify_start_model.occupancies.randomize  = true
refinement.modify_start_model.selection              = name CD1 or name CZ or water

refinement.rigid_body.max_iterations = 5

refinement.twinning.twin_law=-h,k,-l

refinement.simulated_annealing
{
  start_temperature = 500
  final_temperature = 300
  cool_rate = 100
  number_of_steps = 25
}
"""

def exercise_1(counter, pdb, hkl, strategy, sa):
  params = task.replace("ZZZ", strategy)
  params = params.replace("QQQ", sa)
  task_file = open("XXX_params", "w").write(params)
  cmd = " ".join(["phenix.refine",pdb,hkl,"XXX_params","--overwrite --quiet"])
  print("\n"+"-"*30+" run=",counter)
  sys.stdout.write(open("XXX_params").read())
  print(cmd)
  easy_run.call(cmd)

def run():
  pdb = libtbx.env.find_in_repositories(
        relative_path="phenix_regression/pdb/phe_adp_refinement_hoh_h.pdb", test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
        relative_path="phenix_regression/reflection_files/phe.mtz", \
        test=os.path.isfile)
  counter = 0
  for rbr in ["", "rigid_body"]:
    for gbr in ["", "group_adp"]:
      for tls in ["", "tls"]:
        for xyz in ["", "individual_sites"]:
          for adp in ["", "individual_adp"]:
            for occ in ["", "occupancies"]:
                counter += 1
                strategy = ""
                strategy = add_item(strategy, rbr)
                strategy = add_item(strategy, gbr)
                strategy = add_item(strategy, tls)
                strategy = add_item(strategy, xyz)
                strategy = add_item(strategy, adp)
                strategy = add_item(strategy, occ)
                if(strategy == ""): strategy = "none"
                if(xyz == ""): sa = "false"
                else:          sa = "true"
                exercise_1(counter   = counter,
                           pdb       = pdb,
                           hkl       = hkl,
                           strategy  = strategy,
                           sa        = sa)

def add_item(a, b):
  if(b != ""):
     if(a != ""): a = a + "+" + b
     else:        a = b
  return a

if (__name__ == "__main__"):
  print("*"*79)
  print("*** This is to test that most (but not all) of the startegies are runable.")
  print("*** The input / output results are not checked at all.")
  print("*"*79)
  run()
  print("OK")
