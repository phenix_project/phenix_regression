from __future__ import print_function
import time
import iotbx.pdb
from iotbx import mtz
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel
import os

pdb_str = """\
CRYST1   27.475   35.191   27.490  90.00  90.00  90.00 P 21 21 21
HELIX    1   1 ALA E    1  ALA E   16  1                                  16
ATOM      1  N   ALA E   1       6.709  31.817   5.664  1.00 20.00           N
ATOM      2  CA  ALA E   1       7.794  30.914   6.029  1.00 20.00           C
ATOM      3  C   ALA E   1       7.267  29.691   6.773  1.00 20.00           C
ATOM      4  O   ALA E   1       7.942  29.146   7.647  1.00 20.00           O
ATOM      5  CB  ALA E   1       8.831  31.643   6.870  1.00 20.00           C
ATOM      6  N   HIS E   2       6.057  29.267   6.421  1.00 20.00           N
ATOM      7  CA  HIS E   2       5.434  28.110   7.053  1.00 20.00           C
ATOM      8  C   HIS E   2       6.176  26.824   6.701  1.00 20.00           C
ATOM      9  O   HIS E   2       6.477  26.012   7.576  1.00 20.00           O
ATOM     10  CB  HIS E   2       3.965  28.000   6.640  1.00 20.00           C
ATOM     11  CG  HIS E   2       3.140  29.191   7.016  1.00 20.00           C
ATOM     12  ND1 HIS E   2       2.481  29.289   8.222  1.00 20.00           N
ATOM     13  CD2 HIS E   2       2.869  30.335   6.345  1.00 20.00           C
ATOM     14  CE1 HIS E   2       1.838  30.441   8.278  1.00 20.00           C
ATOM     15  NE2 HIS E   2       2.057  31.096   7.152  1.00 20.00           N
ATOM     16  N   CYS E   3       6.468  26.648   5.417  1.00 20.00           N
ATOM     17  CA  CYS E   3       7.193  25.473   4.949  1.00 20.00           C
ATOM     18  C   CYS E   3       8.635  25.492   5.443  1.00 20.00           C
ATOM     19  O   CYS E   3       9.223  24.444   5.712  1.00 20.00           O
ATOM     20  CB  CYS E   3       7.161  25.396   3.421  1.00 20.00           C
ATOM     21  SG  CYS E   3       5.501  25.285   2.714  1.00 20.00           S
TER
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Exercise write_map_coefficients_only.
  """
  pdb_file_name = "%s.pdb"%prefix
  of=open(pdb_file_name, "w")
  print(pdb_str, file=of)
  of.close()
  #
  args =["%s"%pdb_file_name, "high_res=2", "type=real", "r_free=0.1"]
  r = run_fmodel(args = args, prefix = prefix)
  #
  args = [
    "strategy=None",
    "main.number_of_mac=0",
    "write_map_coefficients_only=true",
    "%s"%pdb_file_name,
    r.mtz]
  r = run_phenix_refine(args = args, prefix = prefix)
  #
  expected=["2FOFCWT,PH2FOFCWT","2FOFCWT_no_fill,PH2FOFCWT_no_fill",
            "FOFCWT,PHFOFCWT"]
  mtz_obj = mtz.object(file_name=r.mtz)
  mas = mtz_obj.as_miller_arrays()
  cntr=0
  for ma in mas:
    if(ma.info().label_string() in expected):
      cntr+=1
  assert cntr==3

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
