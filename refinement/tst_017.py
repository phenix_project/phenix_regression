from __future__ import division
from __future__ import print_function
import libtbx.load_env
import os
from phenix_regression.refinement import run_phenix_refine

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Assortment of old tests. Converted from primitive csh scripts.
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/pdb1zff.ent",
    test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/1zff.mtz",
    test=os.path.isfile)
  args = [
    pdb,hkl,
    "target_weights.wu=0",
    "target_weights.wc=0",
    "strategy=none",
    "xray_data.r_free_flags.generate=True",
    "main.target=ml",
    "main.use_experimental_phases=False",
    "modify_start_model.modify.adp.set_b_iso=5.0",
    "xray_data.high_resolution=2.0",
    "structure_factors_and_gradients_accuracy.algorithm=fft"]
  r = run_phenix_refine(args = args, prefix = prefix)

if (__name__ == "__main__"):
  run()
  print("OK")
