from __future__ import division
from __future__ import print_function
import libtbx.load_env
import libtbx.path
import os
from phenix_regression.refinement import run_phenix_refine

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  FES linking causes bond/angle RMSDs explode. Make sure this does not happen.
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/data/5l7j.pdb",
    test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/data/5l7j.mtz",
    test=os.path.isfile)
  args = [
    pdb,
    hkl,
    "main.number_of_macro=1",
    "write_map_coefficients=false",
    "write_geo_file=false",
    "strategy=individual_sites"]
  r = run_phenix_refine(args = args, prefix = prefix, geo_expected=False,
    mtz_expected=False)
  #
  assert r.angle_start < 1.1, 'r.angle_start %s' % r.angle_start
  assert r.bond_start  < 0.005, 't.bond_start %s' % r.bond_start
  assert r.angle_final < 1., 'r.angle_final %s' % r.angle_final
  assert r.bond_final  < 0.05, 'r.bond_final %s' % r.bond_final

if (__name__ == "__main__") :
  run()
  print("OK")
