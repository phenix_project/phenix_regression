from __future__ import print_function
from libtbx import easy_run
import sys, os, time
import libtbx.load_env
from libtbx.utils import search_for


def run():
  data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="refinement/grow_density",
  test=os.path.isdir)

  parameters = """
  pdb_file_name = %s/tst.pdb
reflection_file_name = %s/tst.mtz
sphere {
  center = -3.033 0.305 10.099
  radius = 2.
}
atom_type = O
atom_gap = 2.0
overlap_interval = 2.0
number_of_refinement_cycles = 1
number_of_minimization_iterations = 1
stop_reset_occupancies_at_macro_cycle = 0
stop_reset_adp_at_macro_cycle = 0
start_filtering_at_macro_cycle = 0

  """ %(data_dir, data_dir)
  print("starting grow density test")
  try:
    print("trying to open parameters.txt")
    output_file = open("parameters.txt","w")
  except IOError as xxx_todo_changeme:
    (errno, strerror) = xxx_todo_changeme.args
    print("I/O error(%s): %s" % (errno, strerror))
    print("couldn't open")
  output_file.write(parameters)
  output_file.close()
  cmd = "phenix.grow_density parameters.txt > log.txt"
  print(cmd)
  assert not easy_run.call(cmd)
  # very simple est just now to make sure runs
  try:
    input_file = open("log.txt")
  except IOError as xxx_todo_changeme1:
    (errno, strerror) = xxx_todo_changeme1.args
    print("I/O error(%s): %s" % (errno, strerror))
  count = 0
  for line in input_file.readlines():
    if(line.count("Finished")): count = count +1
  assert count == 1
  print("ran ok - ouput file is of expected length")

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK", "time: %.2f"%(time.time()-t0))
