from __future__ import division
from __future__ import print_function
from libtbx import easy_run
import libtbx.load_env
import libtbx.path
import time, os
from phenix_regression.refinement import get_r_factors
import iotbx.pdb
from scitbx.math import superpose
from libtbx.test_utils import approx_equal, assert_lines_in_file
import math

input_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/ncs/ncs_solvent.pdb",
    test=os.path.isfile)
hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/ncs/ncs_solvent.mtz",
    test=os.path.isfile)

def get_difference_chainsAB(file_name):
  pdb_h = iotbx.pdb.input(file_name=file_name).construct_hierarchy()
  cache = pdb_h.atom_selection_cache()
  ref_sites = pdb_h.select(cache.selection("chain A and not resname HOH")).atoms().extract_xyz()
  other_sites = pdb_h.select(cache.selection("chain B and not resname HOH")).atoms().extract_xyz()
  lsq_obj = superpose.least_squares_fit(ref_sites, other_sites)
  bf = lsq_obj.other_sites_best_fit()
  dif = ref_sites - bf
  return dif

def dif_to_dist(dif):
  result = []
  for d in dif:
    result.append(math.sqrt(d[0]**2 + d[1]**2 + d[2]**2))
  return result

def exercise_01(prefix="tst_refinement_cartesian_ncs_solvent"):
  f = open("%s_ncs.eff"%prefix, "w")
  f.write("""
refinement.pdb_interpretation.ncs_group {
  reference = chain A
  selection = chain B
}""")
  f.close()

  dif = get_difference_chainsAB(file_name="%s" % input_file)
  dist = dif_to_dist(dif)
  assert max(dist) > 3


  base = " ".join([
    "phenix.refine",
    "%s"%input_file,
    "%s"%hkl,
    "main.number_of_macro_cycles=2",
    "main.bulk_solv=true",
    "ncs_search.enabled=true",
    "main.ordered_solvent=True",
    "ordered_solvent.mode=every_macro_cycle",
    "ncs.type=cartesian",
    "strategy=individual_sites",
    "excessive_distance_limit=None",
    "write_eff_file=False",
    "write_geo_file=False",
    "write_def_file=False",
    "write_map_coefficients=False",
    "output.prefix=%s" % prefix,
    "output.write_model_cif_file=True",
    "%s_ncs.eff" % prefix,
    "--overwrite --quiet"
  ])
  cmd = " ".join([base])
  print(cmd)
  assert not easy_run.call(cmd)
  dif = get_difference_chainsAB(file_name="%s_001.pdb" % prefix)
  dist = dif_to_dist(dif)
  assert max(dist) < 1, dist

  of_cif=prefix+"_001.cif"
  assert_lines_in_file(file_name=of_cif, lines="""\
      loop_
        _struct_ncs_dom.pdbx_ens_id
        _struct_ncs_dom.id
        _struct_ncs_dom.details
        ens_1  d_1  '(chain "A" and resid 1 through 6)'
        ens_1  d_2  'chain "B"'
    """)

  assert_lines_in_file(file_name=of_cif, lines="""\
    loop_
      _struct_ncs_dom_lim.pdbx_ens_id
      _struct_ncs_dom_lim.dom_id
      _struct_ncs_dom_lim.pdbx_component_id
      _struct_ncs_dom_lim.beg_label_alt_id
      _struct_ncs_dom_lim.beg_label_asym_id
      _struct_ncs_dom_lim.beg_label_comp_id
      _struct_ncs_dom_lim.beg_label_seq_id
      _struct_ncs_dom_lim.end_label_alt_id
      _struct_ncs_dom_lim.end_label_asym_id
      _struct_ncs_dom_lim.end_label_comp_id
      _struct_ncs_dom_lim.end_label_seq_id
      ens_1  d_1  1  .  A  LYS  1  .  A  ALA  6
      ens_1  d_2  1  .  C  LYS  1  .  C  ALA  6
    """)

  of_pdb=prefix+"_001.pdb"
  with open(of_pdb, 'r') as f:
    final_pdb = f.readlines()
  assert final_pdb.count("REMARK   3     REFERENCE SELECTION: chain 'A'\n") == 1, ""+\
      "Error in output pdb in NCS records"
  assert final_pdb.count("REMARK   3     SELECTION          : chain 'B'\n") == 1, ""+\
      "Error in output pdb in NCS records"

if (__name__ == "__main__"):
  t0 = time.time()
  exercise_01()
  print("OK. Time: %8.3f"%(time.time()-t0))
