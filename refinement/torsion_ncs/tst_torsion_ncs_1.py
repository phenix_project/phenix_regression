from __future__ import print_function
from libtbx import easy_run
import time
import iotbx.pdb
from libtbx.test_utils import assert_lines_in_file

pdb_str = """\
CRYST1  200.179  242.852  102.256  90.00  90.00  90.00 P 1
ATOM   6027  CB  HIS D  13      30.843 193.237  19.305  1.00 27.00      D    C
ATOM   6028  CG  HIS D  13      32.143 193.979  19.354  1.00 27.00      D    C
ATOM   6029  CD2 HIS D  13      33.259 193.879  18.591  1.00 27.00      D    C
ATOM   6030  ND1 HIS D  13      32.400 194.966  20.284  1.00 27.00      D    N
ATOM   6031  CE1 HIS D  13      33.618 195.444  20.089  1.00 27.00      D    C
ATOM   6032  NE2 HIS D  13      34.160 194.802  19.069  1.00 27.00      D    N
ATOM   6033  C   HIS D  13      29.277 194.753  18.014  1.00 27.00      D    C
ATOM   6034  O   HIS D  13      28.272 194.389  17.398  1.00 27.00      D    O
ATOM   6035  N   HIS D  13      28.447 193.372  19.912  1.00 27.00      D    N
ATOM   6036  CA  HIS D  13      29.610 194.148  19.390  1.00 27.00      D    C
ATOM   6037  N   GLU D  14      30.114 195.671  17.533  1.00 27.00      D    N
ATOM   6038  CA  GLU D  14      29.866 196.328  16.247  1.00 27.00      D    C
ATOM   6039  CB  GLU D  14      29.542 197.811  16.482  1.00 27.00      D    C
ATOM   6040  CG  GLU D  14      30.549 198.554  17.349  1.00 27.00      D    C
ATOM   6041  CD  GLU D  14      30.062 199.946  17.735  1.00 27.00      D    C
ATOM   6042  OE1 GLU D  14      28.861 200.073  18.066  1.00 27.00      D    O
ATOM   6043  OE2 GLU D  14      30.871 200.906  17.719  1.00 27.00      D    O
ATOM   6044  C   GLU D  14      30.973 196.219  15.196  1.00 27.00      D    C
ATOM   6045  O   GLU D  14      32.060 195.695  15.463  1.00 27.00      D    O
ATOM   6046  N   ASN D  15      30.666 196.716  13.997  1.00 27.00      D    N
ATOM   6047  CA  ASN D  15      31.598 196.728  12.868  1.00 27.00      D    C
ATOM   6048  CB  ASN D  15      30.846 196.981  11.558  1.00 27.00      D    C
ATOM   6049  CG  ASN D  15      29.948 195.826  11.151  1.00 27.00      D    C
ATOM   6050  OD1 ASN D  15      30.313 194.614  11.544  1.00 27.00      D    O
ATOM   6051  ND2 ASN D  15      28.944 196.028  10.470  1.00 27.00      D    N
ATOM   6052  C   ASN D  15      32.582 197.876  13.089  1.00 27.00      D    C
ATOM   6053  O   ASN D  15      32.166 198.992  13.392  1.00 27.00      D    O
ATOM   6054  N   SER D  16      33.877 197.614  12.936  1.00 27.00      D    N
ATOM   6055  CA  SER D  16      34.879 198.660  13.128  1.00 27.00      D    C
ATOM   6056  CB  SER D  16      36.213 198.049  13.561  1.00 27.00      D    C
ATOM   6057  OG  SER D  16      36.141 197.596  14.904  1.00 27.00      D    O
ATOM   6058  C   SER D  16      35.076 199.476  11.860  1.00 27.00      D    C
ATOM   6059  O   SER D  16      36.125 199.402  11.222  1.00 27.00      D    O
TER
ATOM   6027  CB  HISEC  13      72.612  30.494  95.554  1.00 27.00      D    C
ATOM   6028  CG  HISEC  13      72.820  30.718  94.088  1.00 27.00      D    C
ATOM   6029  CD2 HISEC  13      73.939  30.664  93.325  1.00 27.00      D    C
ATOM   6030  ND1 HISEC  13      71.787  31.044  93.233  1.00 27.00      D    N
ATOM   6031  CE1 HISEC  13      72.261  31.179  92.005  1.00 27.00      D    C
ATOM   6032  NE2 HISEC  13      73.563  30.953  92.034  1.00 27.00      D    N
ATOM   6033  C   HISEC  13      71.806  28.100  95.739  1.00 27.00      D    C
ATOM   6034  O   HISEC  13      71.904  27.385  96.740  1.00 27.00      D    O
ATOM   6035  N   HISEC  13      70.927  29.868  97.256  1.00 27.00      D    N
ATOM   6036  CA  HISEC  13      71.419  29.584  95.876  1.00 27.00      D    C
ATOM   6037  N   GLUEC  14      72.026  27.638  94.509  1.00 27.00      D    N
ATOM   6038  CA  GLUEC  14      72.368  26.233  94.274  1.00 27.00      D    C
ATOM   6039  CB  GLUEC  14      71.214  25.540  93.534  1.00 27.00      D    C
ATOM   6040  CG  GLUEC  14      70.739  26.258  92.278  1.00 27.00      D    C
ATOM   6041  CD  GLUEC  14      69.456  25.656  91.719  1.00 27.00      D    C
ATOM   6042  OE1 GLUEC  14      68.560  25.333  92.532  1.00 27.00      D    O
ATOM   6043  OE2 GLUEC  14      69.334  25.513  90.477  1.00 27.00      D    O
ATOM   6044  C   GLUEC  14      73.675  25.961  93.526  1.00 27.00      D    C
ATOM   6045  O   GLUEC  14      74.347  26.885  93.055  1.00 27.00      D    O
ATOM   6046  N   ASNEC  15      74.022  24.676  93.440  1.00 27.00      D    N
ATOM   6047  CA  ASNEC  15      75.223  24.211  92.744  1.00 27.00      D    C
ATOM   6048  CB  ASNEC  15      75.554  22.773  93.154  1.00 27.00      D    C
ATOM   6049  CG  ASNEC  15      76.016  22.653  94.596  1.00 27.00      D    C
ATOM   6050  OD1 ASNEC  15      76.629  23.706  95.120  1.00 27.00      D    O
ATOM   6051  ND2 ASNEC  15      75.838  21.610  95.222  1.00 27.00      D    N
ATOM   6052  C   ASNEC  15      74.929  24.230  91.245  1.00 27.00      D    C
ATOM   6053  O   ASNEC  15      73.892  23.731  90.816  1.00 27.00      D    O
ATOM   6054  N   SEREC  16      75.833  24.794  90.448  1.00 27.00      D    N
ATOM   6055  CA  SEREC  16      75.625  24.849  89.003  1.00 27.00      D    C
ATOM   6056  CB  SEREC  16      76.362  26.047  88.402  1.00 27.00      D    C
ATOM   6057  OG  SEREC  16      75.706  27.258  88.742  1.00 27.00      D    O
ATOM   6058  C   SEREC  16      76.091  23.569  88.329  1.00 27.00      D    C
ATOM   6059  O   SEREC  16      77.080  23.568  87.597  1.00 27.00      D    O
TER
ATOM   6027  CB  HISEF  13     187.060 147.396  78.145  1.00 27.00      D    C
ATOM   6028  CG  HISEF  13     187.000 145.964  78.579  1.00 27.00      D    C
ATOM   6029  CD2 HISEF  13     186.953 144.813  77.863  1.00 27.00      D    C
ATOM   6030  ND1 HISEF  13     186.983 145.591  79.908  1.00 27.00      D    N
ATOM   6031  CE1 HISEF  13     186.931 144.272  79.991  1.00 27.00      D    C
ATOM   6032  NE2 HISEF  13     186.911 143.776  78.766  1.00 27.00      D    N
ATOM   6033  C   HISEF  13     189.514 148.012  78.269  1.00 27.00      D    C
ATOM   6034  O   HISEF  13     190.059 148.919  77.635  1.00 27.00      D    O
ATOM   6035  N   HISEF  13     187.747 149.666  78.852  1.00 27.00      D    N
ATOM   6036  CA  HISEF  13     188.120 148.222  78.888  1.00 27.00      D    C
ATOM   6037  N   GLUEF  14     190.089 146.824  78.450  1.00 27.00      D    N
ATOM   6038  CA  GLUEF  14     191.428 146.539  77.928  1.00 27.00      D    C
ATOM   6039  CB  GLUEF  14     192.410 146.370  79.097  1.00 27.00      D    C
ATOM   6040  CG  GLUEF  14     191.963 145.385  80.168  1.00 27.00      D    C
ATOM   6041  CD  GLUEF  14     192.857 145.430  81.402  1.00 27.00      D    C
ATOM   6042  OE1 GLUEF  14     193.225 146.553  81.816  1.00 27.00      D    O
ATOM   6043  OE2 GLUEF  14     193.184 144.355  81.963  1.00 27.00      D    O
ATOM   6044  C   GLUEF  14     191.556 145.337  76.990  1.00 27.00      D    C
ATOM   6045  O   GLUEF  14     190.598 144.587  76.775  1.00 27.00      D    O
ATOM   6046  N   ASNEF  15     192.758 145.180  76.432  1.00 27.00      D    N
ATOM   6047  CA  ASNEF  15     193.087 144.078  75.526  1.00 27.00      D    C
ATOM   6048  CB  ASNEF  15     194.374 144.387  74.756  1.00 27.00      D    C
ATOM   6049  CG  ASNEF  15     194.214 145.518  73.754  1.00 27.00      D    C
ATOM   6050  OD1 ASNEF  15     193.008 145.693  73.233  1.00 27.00      D    O
ATOM   6051  ND2 ASNEF  15     195.179 146.213  73.441  1.00 27.00      D    N
ATOM   6052  C   ASNEF  15     193.322 142.831  76.377  1.00 27.00      D    C
ATOM   6053  O   ASNEF  15     194.053 142.888  77.363  1.00 27.00      D    O
ATOM   6054  N   SEREF  16     192.715 141.708  76.003  1.00 27.00      D    N
ATOM   6055  CA  SEREF  16     192.892 140.473  76.764  1.00 27.00      D    C
ATOM   6056  CB  SEREF  16     191.672 139.565  76.600  1.00 27.00      D    C
ATOM   6057  OG  SEREF  16     190.565 140.083  77.321  1.00 27.00      D    O
ATOM   6058  C   SEREF  16     194.146 139.731  76.329  1.00 27.00      D    C
ATOM   6059  O   SEREF  16     194.066 138.660  75.729  1.00 27.00      D    O
TER
ATOM   6440 CL    CL E   1      20.884 230.099   5.000  1.00 27.00      E   CL
TER
ATOM   6440 CL    CLEA   1       5.000 237.852  29.289  1.00 27.00      E   CL
TER
ATOM   6440 CL    CLAC   1      27.854   5.000  85.333  1.00 27.00      E   CL
TER
END
"""

def exercise_1(prefix="tst_torsion_ncs_1"):
  """
  Exercise torsion ncs with two-letters chain id, CL atom in NCS and
  3 NCS groups.
  """
  pdb_file_name = "%s.pdb" % prefix
  of=open(pdb_file_name, "w")
  print(pdb_str, file=of)
  of.close()
  #
  cmd = " ".join([
    "phenix.fmodel",
    "%s"%pdb_file_name,
    "r_free_flags_fraction=0.1",
    "label=\"FOBS\"",
    "type=real",
    "file_name=%s.mtz"%prefix,
    "high_res=5",
    ">%s.zlog"%prefix])
  print(cmd)
  assert not easy_run.call(cmd)
  #
  cmd = " ".join([
    "phenix.refine",
    "%s"%pdb_file_name,
    "%s.mtz"%prefix,
    "strategy=individual_sites",
    "main.bulk_solvent_and_scale=False",
    "ncs_search.enabled=True",
    "main.number_of_macro_cycles=0",
    "main.nqh_flips=False",
    "write_map_coefficients=False",
    "write_eff_file=False",
    "--overwrite",
    ">%s.zlog"%prefix])
  print(cmd)
  assert not easy_run.call(cmd)

  assert_lines_in_file(file_name="%s.zlog" % prefix, lines="""
      HIS D  13 D    <=>  HISEC  13 D    <=>  HISEF  13 D
      GLU D  14 D    <=>  GLUEC  14 D    <=>  GLUEF  14 D
      ASN D  15 D    <=>  ASNEC  15 D    <=>  ASNEF  15 D
      SER D  16 D    <=>  SEREC  16 D    <=>  SEREF  16 D
    """)

if (__name__ == "__main__"):
  t0=time.time()
  exercise_1()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
