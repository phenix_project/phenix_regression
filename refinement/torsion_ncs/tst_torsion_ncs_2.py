from __future__ import print_function
from libtbx import easy_run
import time
import iotbx.pdb
from libtbx.test_utils import assert_lines_in_file

pdb_str_1gtx_cutted_2 = """\
CRYST1   69.860  230.400   72.830  90.00 109.42  90.00 P 1 21 1
SCALE1      0.014314  0.000000  0.005046        0.00000
SCALE2      0.000000  0.004340  0.000000        0.00000
SCALE3      0.000000  0.000000  0.014559        0.00000
ATOM     87  N   PHE C  11      21.179  24.723  15.463  1.00 59.82           N
ATOM     88  CA  PHE C  11      20.982  24.906  14.008  1.00 59.01           C
ATOM     89  C   PHE C  11      20.111  23.788  13.426  1.00 53.61           C
ATOM     90  O   PHE C  11      19.482  23.028  14.167  1.00 50.30           O
ATOM     91  CB  PHE C  11      22.352  24.939  13.299  1.00 67.33           C
ATOM     92  CG  PHE C  11      22.551  26.138  12.393  1.00 74.42           C
ATOM     93  CD1 PHE C  11      21.486  27.022  12.115  1.00 75.83           C
ATOM     94  CD2 PHE C  11      23.808  26.386  11.820  1.00 77.22           C
ATOM     95  CE1 PHE C  11      21.670  28.134  11.281  1.00 78.84           C
ATOM     96  CE2 PHE C  11      24.013  27.498  10.981  1.00 79.67           C
ATOM     97  CZ  PHE C  11      22.942  28.376  10.709  1.00 80.61           C
ATOM     98  N   ASP C  12      20.112  23.714  12.091  1.00 49.56           N
ATOM     99  CA  ASP C  12      19.364  22.758  11.262  1.00 42.94           C
ATOM    100  C   ASP C  12      19.802  23.152   9.847  1.00 40.18           C
ATOM    101  O   ASP C  12      20.154  24.300   9.625  1.00 39.97           O
ATOM    102  CB  ASP C  12      17.848  22.985  11.430  1.00 38.79           C
ATOM    103  CG  ASP C  12      16.981  21.948  10.687  1.00 37.08           C
ATOM    104  OD1 ASP C  12      17.349  20.760  10.622  1.00 30.55           O
ATOM    105  OD2 ASP C  12      15.903  22.327  10.175  1.00 38.95           O
ATOM    106  N   TYR C  13      19.819  22.222   8.899  1.00 40.22           N
ATOM    107  CA  TYR C  13      20.204  22.573   7.529  1.00 38.26           C
ATOM    108  C   TYR C  13      19.026  23.091   6.700  1.00 38.38           C
ATOM    109  O   TYR C  13      17.860  23.068   7.121  1.00 38.56           O
ATOM    110  CB  TYR C  13      20.812  21.388   6.791  1.00 36.88           C
ATOM    111  CG  TYR C  13      22.069  20.828   7.400  1.00 36.90           C
ATOM    112  CD1 TYR C  13      22.012  19.971   8.494  1.00 37.89           C
ATOM    113  CD2 TYR C  13      23.310  21.091   6.839  1.00 34.55           C
ATOM    114  CE1 TYR C  13      23.163  19.384   9.009  1.00 39.03           C
ATOM    115  CE2 TYR C  13      24.461  20.511   7.346  1.00 35.32           C
ATOM    116  CZ  TYR C  13      24.382  19.657   8.430  1.00 37.85           C
ATOM    117  OH  TYR C  13      25.519  19.059   8.919  1.00 40.03           O
ATOM    118  N   ASP C  14      19.344  23.521   5.492  1.00 39.09           N
ATOM    119  CA  ASP C  14      18.347  24.087   4.607  1.00 42.04           C
ATOM    120  C   ASP C  14      17.787  23.098   3.578  1.00 38.47           C
ATOM    121  O   ASP C  14      16.940  23.461   2.754  1.00 40.32           O
ATOM    122  CB  ASP C  14      18.967  25.278   3.899  1.00 51.31           C
ATOM    123  CG  ASP C  14      20.087  24.857   2.975  1.00 63.00           C
ATOM    124  OD1 ASP C  14      20.709  23.795   3.257  1.00 65.42           O
ATOM    125  OD2 ASP C  14      20.334  25.571   1.970  1.00 70.63           O
ATOM    130  N   PHE D  11      42.938  10.747  41.297  1.00 59.82           N
ATOM    131  CA  PHE D  11      43.265  10.744  42.740  1.00 59.01           C
ATOM    132  C   PHE D  11      42.525   9.618  43.470  1.00 53.61           C
ATOM    133  O   PHE D  11      41.964   8.719  42.837  1.00 50.30           O
ATOM    134  CB  PHE D  11      42.898  12.110  43.358  1.00 67.33           C
ATOM    135  CG  PHE D  11      44.023  12.756  44.143  1.00 74.42           C
ATOM    136  CD1 PHE D  11      45.218  12.053  44.409  1.00 75.83           C
ATOM    137  CD2 PHE D  11      43.888  14.071  44.614  1.00 77.22           C
ATOM    138  CE1 PHE D  11      46.261  12.651  45.131  1.00 78.84           C
ATOM    139  CE2 PHE D  11      44.924  14.689  45.339  1.00 79.67           C
ATOM    140  CZ  PHE D  11      46.115  13.979  45.600  1.00 80.61           C
ATOM    141  N   ASP D  12      42.537   9.710  44.803  1.00 49.56           N
ATOM    142  CA  ASP D  12      41.926   8.773  45.756  1.00 42.94           C
ATOM    143  C   ASP D  12      42.245   9.434  47.102  1.00 40.18           C
ATOM    144  O   ASP D  12      43.230  10.149  47.202  1.00 39.97           O
ATOM    145  CB  ASP D  12      42.619   7.398  45.664  1.00 38.79           C
ATOM    146  CG  ASP D  12      41.964   6.315  46.545  1.00 37.08           C
ATOM    147  OD1 ASP D  12      40.727   6.292  46.690  1.00 30.55           O
ATOM    148  OD2 ASP D  12      42.701   5.461  47.089  1.00 38.95           O
ATOM    149  N   TYR D  13      41.419   9.237  48.124  1.00 40.22           N
ATOM    150  CA  TYR D  13      41.712   9.830  49.432  1.00 38.26           C
ATOM    151  C   TYR D  13      42.632   8.953  50.285  1.00 38.38           C
ATOM    152  O   TYR D  13      42.960   7.808  49.940  1.00 38.56           O
ATOM    153  CB  TYR D  13      40.442  10.093  50.230  1.00 36.88           C
ATOM    154  CG  TYR D  13      39.471  11.051  49.595  1.00 36.90           C
ATOM    155  CD1 TYR D  13      38.612  10.632  48.585  1.00 37.89           C
ATOM    156  CD2 TYR D  13      39.354  12.355  50.053  1.00 34.55           C
ATOM    157  CE1 TYR D  13      37.654  11.489  48.052  1.00 39.03           C
ATOM    158  CE2 TYR D  13      38.404  13.215  49.528  1.00 35.32           C
ATOM    159  CZ  TYR D  13      37.556  12.777  48.529  1.00 37.85           C
ATOM    160  OH  TYR D  13      36.594  13.619  48.023  1.00 40.03           O
ATOM    161  N   ASP D  14      43.010   9.493  51.429  1.00 39.09           N
ATOM    162  CA  ASP D  14      43.921   8.807  52.322  1.00 42.04           C
ATOM    163  C   ASP D  14      43.230   8.054  53.465  1.00 38.47           C
ATOM    164  O   ASP D  14      43.897   7.440  54.306  1.00 40.32           O
ATOM    165  CB  ASP D  14      44.890   9.831  52.885  1.00 51.31           C
ATOM    166  CG  ASP D  14      44.189  10.835  53.771  1.00 63.00           C
ATOM    167  OD1 ASP D  14      42.968  11.063  53.543  1.00 65.42           O
ATOM    168  OD2 ASP D  14      44.846  11.382  54.693  1.00 70.63           O
END
"""

pdb_str_1gtx_cutted = """\
CRYST1   69.860  230.400   72.830  90.00 109.42  90.00 P 1 21 1
SCALE1      0.014314  0.000000  0.005046        0.00000
SCALE2      0.000000  0.004340  0.000000        0.00000
SCALE3      0.000000  0.000000  0.014559        0.00000
ATOM      1  N   PHE A  11       9.443  30.773  22.721  1.00 59.82           N
ATOM      2  CA  PHE A  11       9.246  30.956  21.266  1.00 59.01           C
ATOM      3  C   PHE A  11       8.375  29.838  20.684  1.00 53.61           C
ATOM      4  O   PHE A  11       7.746  29.078  21.425  1.00 50.30           O
ATOM      5  CB  PHE A  11      10.616  30.989  20.557  1.00 67.33           C
ATOM      6  CG  PHE A  11      10.815  32.188  19.651  1.00 74.42           C
ATOM      7  CD1 PHE A  11       9.750  33.072  19.373  1.00 75.83           C
ATOM      8  CD2 PHE A  11      12.072  32.436  19.078  1.00 77.22           C
ATOM      9  CE1 PHE A  11       9.934  34.184  18.539  1.00 78.84           C
ATOM     10  CE2 PHE A  11      12.277  33.548  18.239  1.00 79.67           C
ATOM     11  CZ  PHE A  11      11.206  34.426  17.967  1.00 80.61           C
ATOM     12  N   ASP A  12       8.376  29.764  19.349  1.00 49.56           N
ATOM     13  CA  ASP A  12       7.628  28.808  18.520  1.00 42.94           C
ATOM     14  C   ASP A  12       8.066  29.202  17.105  1.00 40.18           C
ATOM     15  O   ASP A  12       8.418  30.350  16.883  1.00 39.97           O
ATOM     16  CB  ASP A  12       6.112  29.035  18.688  1.00 38.79           C
ATOM     17  CG  ASP A  12       5.245  27.998  17.945  1.00 37.08           C
ATOM     18  OD1 ASP A  12       5.613  26.810  17.880  1.00 30.55           O
ATOM     19  OD2 ASP A  12       4.167  28.377  17.433  1.00 38.95           O
ATOM     20  N   TYR A  13       8.083  28.272  16.157  1.00 40.22           N
ATOM     21  CA  TYR A  13       8.468  28.623  14.787  1.00 38.26           C
ATOM     22  C   TYR A  13       7.290  29.141  13.958  1.00 38.38           C
ATOM     23  O   TYR A  13       6.124  29.118  14.379  1.00 38.56           O
ATOM     24  CB  TYR A  13       9.076  27.438  14.049  1.00 36.88           C
ATOM     25  CG  TYR A  13      10.333  26.878  14.658  1.00 36.90           C
ATOM     26  CD1 TYR A  13      10.276  26.021  15.752  1.00 37.89           C
ATOM     27  CD2 TYR A  13      11.574  27.141  14.097  1.00 34.55           C
ATOM     28  CE1 TYR A  13      11.427  25.434  16.267  1.00 39.03           C
ATOM     29  CE2 TYR A  13      12.725  26.561  14.604  1.00 35.32           C
ATOM     30  CZ  TYR A  13      12.646  25.707  15.688  1.00 37.85           C
ATOM     31  OH  TYR A  13      13.783  25.109  16.177  1.00 40.03           O
ATOM     32  N   ASP A  14       7.608  29.571  12.750  1.00 39.09           N
ATOM     33  CA  ASP A  14       6.611  30.137  11.865  1.00 42.04           C
ATOM     34  C   ASP A  14       6.051  29.148  10.836  1.00 38.47           C
ATOM     35  O   ASP A  14       5.204  29.511  10.012  1.00 40.32           O
ATOM     36  CB  ASP A  14       7.231  31.328  11.157  1.00 51.31           C
ATOM     37  CG  ASP A  14       8.351  30.907  10.233  1.00 63.00           C
ATOM     38  OD1 ASP A  14       8.973  29.845  10.515  1.00 65.42           O
ATOM     39  OD2 ASP A  14       8.598  31.621   9.228  1.00 70.63           O
ATOM     44  N   PHE B  11      34.960  15.104  29.538  1.00 59.82           N
ATOM     45  CA  PHE B  11      35.287  15.101  30.981  1.00 59.01           C
ATOM     46  C   PHE B  11      34.547  13.975  31.711  1.00 53.61           C
ATOM     47  O   PHE B  11      33.986  13.076  31.078  1.00 50.30           O
ATOM     48  CB  PHE B  11      34.920  16.467  31.599  1.00 67.33           C
ATOM     49  CG  PHE B  11      36.045  17.113  32.384  1.00 74.42           C
ATOM     50  CD1 PHE B  11      37.240  16.410  32.650  1.00 75.83           C
ATOM     51  CD2 PHE B  11      35.910  18.428  32.855  1.00 77.22           C
ATOM     52  CE1 PHE B  11      38.283  17.008  33.372  1.00 78.84           C
ATOM     53  CE2 PHE B  11      36.946  19.046  33.580  1.00 79.67           C
ATOM     54  CZ  PHE B  11      38.137  18.336  33.841  1.00 80.61           C
ATOM     55  N   ASP B  12      34.559  14.067  33.044  1.00 49.56           N
ATOM     56  CA  ASP B  12      33.948  13.130  33.997  1.00 42.94           C
ATOM     57  C   ASP B  12      34.267  13.791  35.343  1.00 40.18           C
ATOM     58  O   ASP B  12      35.252  14.506  35.443  1.00 39.97           O
ATOM     59  CB  ASP B  12      34.641  11.755  33.905  1.00 38.79           C
ATOM     60  CG  ASP B  12      33.986  10.672  34.786  1.00 37.08           C
ATOM     61  OD1 ASP B  12      32.749  10.649  34.931  1.00 30.55           O
ATOM     62  OD2 ASP B  12      34.723   9.818  35.330  1.00 38.95           O
ATOM     63  N   TYR B  13      33.441  13.594  36.365  1.00 40.22           N
ATOM     64  CA  TYR B  13      33.734  14.187  37.673  1.00 38.26           C
ATOM     65  C   TYR B  13      34.654  13.310  38.526  1.00 38.38           C
ATOM     66  O   TYR B  13      34.982  12.165  38.181  1.00 38.56           O
ATOM     67  CB  TYR B  13      32.464  14.450  38.471  1.00 36.88           C
ATOM     68  CG  TYR B  13      31.493  15.408  37.836  1.00 36.90           C
ATOM     69  CD1 TYR B  13      30.634  14.989  36.826  1.00 37.89           C
ATOM     70  CD2 TYR B  13      31.376  16.712  38.294  1.00 34.55           C
ATOM     71  CE1 TYR B  13      29.676  15.846  36.293  1.00 39.03           C
ATOM     72  CE2 TYR B  13      30.426  17.572  37.769  1.00 35.32           C
ATOM     73  CZ  TYR B  13      29.578  17.134  36.770  1.00 37.85           C
ATOM     74  OH  TYR B  13      28.616  17.976  36.264  1.00 40.03           O
ATOM     75  N   ASP B  14      35.032  13.850  39.670  1.00 39.09           N
ATOM     76  CA  ASP B  14      35.943  13.164  40.563  1.00 42.04           C
ATOM     77  C   ASP B  14      35.252  12.411  41.706  1.00 38.47           C
ATOM     78  O   ASP B  14      35.919  11.797  42.547  1.00 40.32           O
ATOM     79  CB  ASP B  14      36.912  14.188  41.126  1.00 51.31           C
ATOM     80  CG  ASP B  14      36.211  15.192  42.012  1.00 63.00           C
ATOM     81  OD1 ASP B  14      34.990  15.420  41.784  1.00 65.42           O
ATOM     82  OD2 ASP B  14      36.868  15.739  42.934  1.00 70.63           O
"""


def exercise_2(prefix="tst_torsion_ncs_2"):
  """
  there are 2 chains in refining model and 2 chains in reference
  model and all of them NCS related. Torsion+reference_model.
  Note that both chains are related to the 1 chain in ref model.
  """
  for fn, pdb_str in [("%s_model.pdb" % prefix, pdb_str_1gtx_cutted),
      ("%s_ref.pdb" % prefix, pdb_str_1gtx_cutted_2)]:
    of=open(fn, "w")
    print(pdb_str, file=of)
    of.close()
  #
  cmd = " ".join([
    "phenix.fmodel",
    "%s_model.pdb" % prefix,
    "r_free_flags_fraction=0.1",
    "label=\"FOBS\"",
    "type=real",
    "file_name=%s.mtz"%prefix,
    "high_res=5",
    ">%s.zlog"%prefix])
  print(cmd)
  assert not easy_run.call(cmd)
  #
  cmd = " ".join([
    "phenix.refine",
    "%s_model.pdb" % prefix,
    "%s.mtz"%prefix,
    "strategy=individual_sites",
    "main.bulk_solvent_and_scale=False",
    "ncs_search.enabled=True",
    "reference_model.enabled=True",
    "reference_model.file=%s_ref.pdb" % prefix,
    "main.number_of_macro_cycles=0",
    "main.nqh_flips=False",
    "write_map_coefficients=False",
    "write_eff_file=False",
    "--overwrite",
    ">%s.zlog"%prefix])
  print(cmd)
  assert not easy_run.call(cmd)

  assert_lines_in_file(file_name="%s.zlog" % prefix, lines="""
      Model:              Reference:
      PHE A  11  <=====>  PHE C  11
      ASP A  12  <=====>  ASP C  12
      TYR A  13  <=====>  TYR C  13
      ASP A  14  <=====>  ASP C  14
      PHE B  11  <=====>  PHE C  11
      ASP B  12  <=====>  ASP C  12
      TYR B  13  <=====>  TYR C  13
      ASP B  14  <=====>  ASP C  14

      Total # of matched residue pairs: 8
      Total # of reference model restraints: 36
      --------------------------------------------------------
    """)
  assert_lines_in_file(file_name="%s.zlog" % prefix, lines="""
      ==================== Process input NCS or/and find new NCS ====================

      Number of NCS groups: 1
      refinement.pdb_interpretation.ncs_group {
        reference = chain 'A'
        selection = chain 'B'
      }
      Not restraining NCS-related b-factors:
      refinement.ncs.b_factor_weight = 0.0
      """)

  assert_lines_in_file(file_name="%s.zlog" % prefix, lines="""
      Determining NCS matches...
      --------------------------------------------------------
      Torsion NCS Matching Summary:
       PHE A  11  <=>  PHE B  11
       ASP A  12  <=>  ASP B  12
       TYR A  13  <=>  TYR B  13
       ASP A  14  <=>  ASP B  14
      --------------------------------------------------------
      Initializing torsion NCS restraints...
      Number of torsion NCS restraints: 30""")

if (__name__ == "__main__"):
  t0=time.time()
  exercise_2()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
