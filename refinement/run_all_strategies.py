from __future__ import print_function
from libtbx import easy_run
import libtbx.load_env
import sys, os
from cctbx.array_family import flex

task="""\
refinement.refine.strategy             = ZZZ
refinement.main.ordered_solvent        = true
refinement.main.simulated_annealing    = QQQ
refinement.output.prefix               = XXX
refinement.main.number_of_macro_cycles = 2
refinement.input.xray_data.high_resolution = 4.0
refinement.bulk_solvent_and_scale.k_sol_b_sol_grid_search=false


refinement.simulated_annealing
{
  start_temperature = 600
  final_temperature = 500
  cool_rate = 100
  number_of_steps = 5
}
"""

def exercise_1(counter, pdb, hkl, strategy, sa):
  params = task.replace("ZZZ", strategy).replace("XXX", "XXX_%d" % counter)
  params = params.replace("QQQ", sa)
  param_file = "tst_XXX_%d_params" % counter
  task_file = open(param_file, "w").write(params)
  cmd = " ".join(["phenix.refine",pdb,hkl,param_file,"--overwrite --quiet"])
  print("\n"+"-"*30+" run=",counter)
  sys.stdout.write(open(param_file).read())
  print(cmd)
  assert (easy_run.call(cmd) == 0)

def run(args):
  assert (len(args) in [0, 1])
  idx = None
  if (len(args) > 0) :
    idx = int(args[0])
    assert (1 <= idx <= 64)
  pdb = libtbx.env.find_in_repositories(
        relative_path="phenix_regression/pdb/enk_gm.pdb", test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
        relative_path="phenix_regression/reflection_files/enk.mtz", \
        test=os.path.isfile)
  counter = 0
  for rbr in ["", "rigid_body"]:
    for gbr in ["", "group_adp"]:
      for tls in ["", "tls"]:
        for xyz in ["", "individual_sites"]:
          for adp in ["", "individual_adp"]:
            for occ in ["", "occupancies"]:
              counter += 1
              if (idx is not None) and (idx != counter) :
                continue
              strategy = ""
              strategy = add_item(strategy, rbr)
              strategy = add_item(strategy, gbr)
              strategy = add_item(strategy, tls)
              strategy = add_item(strategy, xyz)
              strategy = add_item(strategy, adp)
              strategy = add_item(strategy, occ)
              if(strategy == ""): strategy = "none"
              if(xyz == ""): sa = "false"
              else:          sa = "true"
              exercise_1(counter   = counter,
                         pdb       = pdb,
                         hkl       = hkl,
                         strategy  = strategy,
                         sa        = sa)

def add_item(a, b):
  if(b != ""):
     if(a != ""): a = a + "+" + b
     else:        a = b
  return a

if (__name__ == "__main__"):
  print("*"*79)
  print("*** This is to test that most (but not all) of the startegies are runable.")
  print("*** The input / output results are not checked at all.")
  print("*"*79)
  run(sys.argv[1:])
  print("OK")
