from __future__ import division
from __future__ import print_function
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel
import os, random
from scitbx.array_family import flex

if (1):
  random.seed(0)
  flex.set_random_seed(0)

pdb_str = """
CRYST1   93.089   61.693  118.458  90.00 102.75  90.00 C 1 2 1
SCALE1      0.010742  0.000000  0.002431        0.00000
SCALE2      0.000000  0.016209  0.000000        0.00000
SCALE3      0.000000  0.000000  0.008655        0.00000
ATOM      1  N   ASN A 205      -8.850 -13.126  22.105  1.00 65.87           N
ANISOU    1  N   ASN A 205     6351   8084  10593   1054   4334   2500       N
ATOM      2  CA  ASN A 205      -9.421 -13.212  23.445  1.00 72.19           C
ANISOU    2  CA  ASN A 205     7426   8933  11068   1049   4212   2449       C
ATOM      3  C   ASN A 205      -9.461 -11.841  24.126  1.00 72.55           C
ANISOU    3  C   ASN A 205     7526   9166  10873    884   3629   2328       C
ATOM      4  CB  ASN A 205      -8.646 -14.224  24.294  1.00 77.70           C
ANISOU    4  CB  ASN A 205     8095   9619  11809   1243   4289   2908       C
ATOM      5  CG  ASN A 205      -7.598 -13.571  25.173  1.00 81.59           C
ANISOU    5  CG  ASN A 205     8464  10293  12245   1246   3746   3246       C
ATOM      6  OD1 ASN A 205      -6.572 -13.095  24.689  1.00 80.58           O
ANISOU    6  OD1 ASN A 205     8051  10235  12332   1244   3512   3470       O
ATOM      7  ND2 ASN A 205      -7.849 -13.554  26.478  1.00 86.39           N
ANISOU    7  ND2 ASN A 205     9285  10975  12563   1254   3545   3285       N
ATOM      8  O   ASN A 205      -8.831 -10.896  23.649  1.00 71.45           O
ANISOU    8  O   ASN A 205     7180   9125  10844    796   3295   2370       O
ATOM      9  N   PRO A 206     -10.207 -11.729  25.240  1.00 73.02           N
ANISOU    9  N   PRO A 206     7868   9275  10603    841   3509   2173       N
ATOM     10  CA  PRO A 206     -10.400 -10.441  25.919  1.00 70.65           C
ANISOU   10  CA  PRO A 206     7654   9144  10045    679   2977   2007       C
ATOM     11  CB  PRO A 206     -11.173 -10.824  27.185  1.00 71.01           C
ANISOU   11  CB  PRO A 206     8021   9196   9763    701   3010   1920       C
ATOM     12  CG  PRO A 206     -11.861 -12.093  26.839  1.00 70.95           C
ANISOU   12  CG  PRO A 206     8141   8995   9822    806   3618   1828       C
ATOM     13  CD  PRO A 206     -10.923 -12.820  25.924  1.00 73.30           C
ANISOU   13  CD  PRO A 206     8158   9202  10490    943   3878   2143       C
ATOM     14  C   PRO A 206      -9.084  -9.761  26.292  1.00 72.52           C
ANISOU   14  C   PRO A 206     7653   9542  10360    686   2472   2380       C
ATOM     15  O   PRO A 206      -9.016  -8.531  26.304  1.00 72.31           O
ANISOU   15  O   PRO A 206     7580   9644  10251    535   2036   2244       O
ATOM     16  N   GLU A 207      -8.056 -10.550  26.593  1.00 73.25           N
ANISOU   16  N   GLU A 207     7593   9624  10613    859   2527   2840       N
ATOM     17  CA  GLU A 207      -6.752  -9.995  26.937  1.00 77.00           C
ANISOU   17  CA  GLU A 207     7824  10243  11189    882   2068   3216       C
ATOM     18  CB  GLU A 207      -5.814 -11.092  27.448  1.00 83.55           C
ANISOU   18  CB  GLU A 207     8554  11040  12153   1097   2199   3702       C
ATOM     19  CG  GLU A 207      -4.525 -10.567  28.069  1.00 90.94           C
ANISOU   19  CG  GLU A 207     9279  12132  13143   1131   1695   4096       C
ATOM     20  CD  GLU A 207      -3.550 -11.673  28.439  1.00 96.90           C
ANISOU   20  CD  GLU A 207     9907  12843  14068   1351   1843   4579       C
ATOM     21  OE1 GLU A 207      -3.747 -12.822  27.988  1.00 98.84           O
ANISOU   21  OE1 GLU A 207    10173  12929  14452   1475   2353   4629       O
ATOM     22  OE2 GLU A 207      -2.584 -11.391  29.179  1.00 99.10           O
ANISOU   22  OE2 GLU A 207    10064  13244  14345   1401   1445   4908       O
ATOM     23  C   GLU A 207      -6.136  -9.281  25.732  1.00 75.14           C
ANISOU   23  C   GLU A 207     7292  10027  11231    802   1952   3216       C
ATOM     24  O   GLU A 207      -5.771  -8.107  25.812  1.00 75.42           O
ANISOU   24  O   GLU A 207     7227  10202  11229    675   1480   3189       O
TER
ATOM   2065  N   GLU B 207       9.820 -29.914  17.737  1.00 96.01           N
ANISOU 2065  N   GLU B 207    12548   9501  14430  -1961   1612  -1682       N
ATOM   2066  CA  GLU B 207       9.267 -28.742  17.067  1.00 96.08           C
ANISOU 2066  CA  GLU B 207    12311  10031  14162  -2066   1531  -1638       C
ATOM   2067  CB  GLU B 207       7.769 -28.924  16.792  1.00102.06           C
ANISOU 2067  CB  GLU B 207    13043  10839  14898  -2330   1679  -1669       C
ATOM   2068  CG  GLU B 207       6.876 -28.891  18.029  1.00107.54           C
ANISOU 2068  CG  GLU B 207    13951  11313  15597  -2262   1748  -1363       C
ATOM   2069  CD  GLU B 207       7.071 -30.092  18.940  1.00115.26           C
ANISOU 2069  CD  GLU B 207    15204  11739  16852  -2192   1876  -1328       C
ATOM   2070  OE1 GLU B 207       7.667 -31.096  18.493  1.00118.61           O
ANISOU 2070  OE1 GLU B 207    15664  11903  17502  -2244   1940  -1587       O
ATOM   2071  OE2 GLU B 207       6.620 -30.032  20.104  1.00117.60           O
ANISOU 2071  OE2 GLU B 207    15682  11867  17135  -2072   1910  -1050       O
ATOM   2072  C   GLU B 207       9.519 -27.477  17.889  1.00 90.85           C
ANISOU 2072  C   GLU B 207    11671   9570  13279  -1814   1368  -1286       C
ATOM   2073  O   GLU B 207      10.004 -26.472  17.368  1.00 86.73           O
ANISOU 2073  O   GLU B 207    10968   9428  12558  -1742   1213  -1268       O
ATOM   2074  N   SER B 208       9.179 -27.535  19.172  1.00 89.37           N
ANISOU 2074  N   SER B 208    11700   9135  13119  -1679   1405  -1019       N
ATOM   2075  CA  SER B 208       9.507 -26.472  20.109  1.00 85.11           C
ANISOU 2075  CA  SER B 208    11197   8744  12399  -1427   1257   -721       C
ATOM   2076  CB  SER B 208       8.258 -25.986  20.844  1.00 80.45           C
ANISOU 2076  CB  SER B 208    10664   8204  11699  -1440   1297   -515       C
ATOM   2077  OG  SER B 208       7.928 -26.855  21.916  1.00 78.57           O
ANISOU 2077  OG  SER B 208    10663   7584  11608  -1404   1435   -403       O
ATOM   2078  C   SER B 208      10.497 -27.038  21.110  1.00 90.46           C
ANISOU 2078  C   SER B 208    12085   9095  13192  -1205   1244   -616       C
ATOM   2079  O   SER B 208      11.307 -26.312  21.683  1.00 92.49           O
ANISOU 2079  O   SER B 208    12344   9479  13320   -986   1095   -455       O
ATOM   2080  N   ALA B 209      10.423 -28.350  21.307  1.00 93.78           N
ANISOU 2080  N   ALA B 209    12688   9095  13850  -1256   1400   -712       N
ATOM   2081  CA  ALA B 209      11.315 -29.049  22.221  1.00 95.07           C
ANISOU 2081  CA  ALA B 209    13082   8911  14130  -1027   1403   -620       C
ATOM   2082  CB  ALA B 209      10.880 -30.497  22.376  1.00 97.99           C
ANISOU 2082  CB  ALA B 209    13648   8803  14780  -1118   1605   -722       C
ATOM   2083  C   ALA B 209      12.762 -28.971  21.749  1.00 94.63           C
ANISOU 2083  C   ALA B 209    12961   8919  14075   -873   1254   -750       C
ATOM   2084  O   ALA B 209      13.669 -29.477  22.409  1.00 97.17           O
ANISOU 2084  O   ALA B 209    13456   9001  14465   -649   1227   -687       O
TER
ATOM   4029  CAI DRG C   1      18.001  17.513  12.323  0.72 57.95           C
ATOM   4030  CAO DRG C   1      18.135  17.405  13.705  0.72 60.91           C
ATOM   4031 BRAE DRG C   1      19.851  17.210  14.493  0.72 55.00          Br
ATOM   4032  CAM DRG C   1      17.001  17.441  14.528  0.72 61.50           C
ATOM   4033  OAC DRG C   1      17.136  17.336  15.885  0.72 59.61           O
ATOM   4034  CAP DRG C   1      15.723  17.585  13.984  0.72 62.32           C
ATOM   4035 BRAF DRG C   1      14.201  17.631  15.131  0.72 55.00          Br
ATOM   4036  CAJ DRG C   1      15.587  17.685  12.608  0.72 59.58           C
ATOM   4037  CAS DRG C   1      16.717  17.647  11.790  0.72 56.86           C
ATOM   4038  CAU DRG C   1      16.531  17.779  10.259  0.72 53.63           C
ATOM   4039  CAA DRG C   1      16.313  16.377   9.688  0.72 57.13           C
ATOM   4040  CAB DRG C   1      17.769  18.482   9.678  0.72 47.77           C
ATOM   4041  CAT DRG C   1      15.286  18.633   9.954  0.72 51.15           C
ATOM   4042  CAK DRG C   1      15.384  20.014   9.921  0.72 52.87           C
ATOM   4043  CAQ DRG C   1      14.277  20.783   9.649  0.72 51.75           C
ATOM   4044 BRAG DRG C   1      14.405  22.680   9.573  0.72 55.00          Br
ATOM   4045  CAN DRG C   1      13.059  20.151   9.424  0.72 50.98           C
ATOM   4046  OAD DRG C   1      11.958  20.905   9.152  0.72 51.93           O
ATOM   4047  CAR DRG C   1      12.939  18.761   9.473  0.72 49.45           C
ATOM   4048 BRAH DRG C   1      11.273  17.915   9.165  0.72 55.00          Br
ATOM   4049  CAL DRG C   1      14.064  17.987   9.744  0.72 47.18           C
TER
HETATM 4050  C1  PGO D6128       3.835  19.443  42.221  1.00 61.89           C
HETATM 4051  C2  PGO D6128       4.243  18.978  43.623  1.00 59.73           C
HETATM 4052  C3  PGO D6128       3.525  19.791  44.708  1.00 57.40           C
HETATM 4053  O1  PGO D6128       2.414  19.353  42.081  1.00 62.08           O
HETATM 4054  O2  PGO D6128       5.668  19.064  43.759  1.00 56.54           O
TER
HETATM 4055  O   HOH S   1      11.780   0.847  10.836  1.00 27.08           O
HETATM 4056  O   HOH S   2       7.984   9.142  12.424  1.00 32.53           O
HETATM 4057  O   HOH S   3      29.659 -10.605  20.916  1.00 19.86           O
HETATM 4058  O   HOH S   4       5.764   7.919  38.855  1.00 47.56           O
HETATM 4059  O   HOH S   5      11.353 -11.291  19.854  1.00 30.30           O
HETATM 4060  O   HOH S   6      39.298 -15.874  23.696  1.00 26.69           O
HETATM 4061  O   HOH S   7      35.178 -10.560  12.220  1.00 23.59           O
HETATM 4062  O   HOH S   8      18.536 -11.820  37.923  1.00 29.74           O
HETATM 4063  O   HOH S   9      11.323   5.715  12.561  1.00 35.30           O
HETATM 4064  O   HOH S  10      29.862 -15.436  10.722  1.00 28.32           O
TER
END
"""

cif_str = """
#
data_comp_list
loop_
_chem_comp.id
_chem_comp.three_letter_code
_chem_comp.name
_chem_comp.group
_chem_comp.number_atoms_all
_chem_comp.number_atoms_nh
_chem_comp.desc_level
PGO PGO Unknown                   ligand 13 5 .
DRG DRG Unknown                   ligand 33 21 .
#
data_comp_PGO
#
loop_
_chem_comp_atom.comp_id
_chem_comp_atom.atom_id
_chem_comp_atom.type_symbol
_chem_comp_atom.type_energy
_chem_comp_atom.partial_charge
_chem_comp_atom.x
_chem_comp_atom.y
_chem_comp_atom.z
PGO    C1     C      CH2    .      -1.049 -0.0875 -0.1973
PGO    C2     C      CH1    .      0.4417 -0.0875 -0.1973
PGO    C3     C      CH3    .      0.9393 -0.0875 1.208
PGO    O1     O      OH1    .      -1.509 0.0405 -1.492
PGO    O2     O      OH1    .      0.9014 1.0362 -0.8477
PGO    H11    H      HCH2   .      -1.4091 0.74   0.3961
PGO    H12    H      HCH2   .      -1.409 -1.0151 0.2226
PGO    H2     H      HCH1   .      0.8018 -0.9695 -0.7059
PGO    H31    H      HCH3   .      2.017  -0.1577 1.2084
PGO    H32    H      HCH3   .      0.638  0.8273 1.6966
PGO    H33    H      HCH3   .      0.5234 -0.9321 1.7372
PGO    HO1    H      HOH1   .      -2.5835 -0.0677 -1.5047
PGO    HO2    H      HOH1   .      1.6969 0.7607 -1.5242
#
loop_
_chem_comp_bond.comp_id
_chem_comp_bond.atom_id_1
_chem_comp_bond.atom_id_2
_chem_comp_bond.type
_chem_comp_bond.value_dist
_chem_comp_bond.value_dist_esd
PGO    C1     C2     single 1.491  0.02
PGO    C1     O1     single 1.38   0.02
PGO    C1     H11    single 1.08   0.02
PGO    C1     H12    single 1.08   0.02
PGO    C2     C3     single 1.491  0.02
PGO    C2     O2     single 1.377  0.02
PGO    C2     H2     single 1.08   0.02
PGO    C3     H31    single 1.08   0.02
PGO    C3     H32    single 1.08   0.02
PGO    C3     H33    single 1.08   0.02
PGO    O1     HO1    single 1.08   0.02
PGO    O2     HO2    single 1.08   0.02
#
loop_
_chem_comp_angle.comp_id
_chem_comp_angle.atom_id_1
_chem_comp_angle.atom_id_2
_chem_comp_angle.atom_id_3
_chem_comp_angle.value_angle
_chem_comp_angle.value_angle_esd
PGO    C3     C2     C1     109.5  3.0
PGO    O2     C2     C1     109.5  3.0
PGO    H2     C2     C1     109.48 3.0
PGO    HO1    O1     C1     109.48 3.0
PGO    O1     C1     C2     109.47 3.0
PGO    H11    C1     C2     109.47 3.0
PGO    H12    C1     C2     109.47 3.0
PGO    H31    C3     C2     109.47 3.0
PGO    H32    C3     C2     109.47 3.0
PGO    H33    C3     C2     109.47 3.0
PGO    HO2    O2     C2     109.48 3.0
PGO    O2     C2     C3     109.5  3.0
PGO    H2     C2     C3     109.43 3.0
PGO    H11    C1     O1     109.47 3.0
PGO    H12    C1     O1     109.47 3.0
PGO    H2     C2     O2     109.43 3.0
PGO    H12    C1     H11    109.47 3.0
PGO    H32    C3     H31    109.47 3.0
PGO    H33    C3     H31    109.47 3.0
PGO    H33    C3     H32    109.47 3.0
#
loop_
_chem_comp_tor.comp_id
_chem_comp_tor.id
_chem_comp_tor.atom_id_1
_chem_comp_tor.atom_id_2
_chem_comp_tor.atom_id_3
_chem_comp_tor.atom_id_4
_chem_comp_tor.value_angle
_chem_comp_tor.value_angle_esd
_chem_comp_tor.period
PGO    Var_01 H31    C3     C2     C1     176.05 30.0   3
PGO    Var_02 H32    C3     C2     C1     -63.96 30.0   3
PGO    Var_03 H33    C3     C2     C1     56.05  30.0   3
PGO    Var_04 HO2    O2     C2     C1     -134.71 30.0   3
PGO    Var_05 HO1    O1     C1     C2     173.86 30.0   3
PGO    Var_06 O1     C1     C2     C3     174.36 30.0   1
PGO    Var_07 H11    C1     C2     C3     54.36  30.0   1
PGO    Var_08 H12    C1     C2     C3     -65.65 30.0   1
PGO    Var_09 HO2    O2     C2     C3     105.22 30.0   3
PGO    Var_10 O2     C2     C1     O1     54.29  30.0   3
PGO    Var_11 H2     C2     C1     O1     -65.67 30.0   3
PGO    Var_12 H11    C1     C2     O2     -65.71 30.0   1
PGO    Var_13 H12    C1     C2     O2     174.29 30.0   1
PGO    Var_14 H31    C3     C2     O2     -63.89 30.0   3
PGO    Var_15 H32    C3     C2     O2     56.11  30.0   3
PGO    Var_16 H33    C3     C2     O2     176.11 30.0   3
PGO    Var_17 H2     C2     C1     H11    174.33 30.0   3
PGO    Var_18 HO1    O1     C1     H11    -66.14 30.0   3
PGO    Var_19 H2     C2     C1     H12    54.33  30.0   3
PGO    Var_20 HO1    O1     C1     H12    53.86  30.0   3
PGO    Var_21 H31    C3     C2     H2     56.05  30.0   3
PGO    Var_22 H32    C3     C2     H2     176.04 30.0   3
PGO    Var_23 H33    C3     C2     H2     -63.95 30.0   3
PGO    Var_24 HO2    O2     C2     H2     -14.72 30.0   3
#
loop_
_chem_comp_chir.comp_id
_chem_comp_chir.id
_chem_comp_chir.atom_id_centre
_chem_comp_chir.atom_id_1
_chem_comp_chir.atom_id_2
_chem_comp_chir.atom_id_3
_chem_comp_chir.volume_sign
PGO    chir_01 C2     C1     C3     O2     positiv
#
data_comp_DRG
#
loop_
_chem_comp_atom.comp_id
_chem_comp_atom.atom_id
_chem_comp_atom.type_symbol
_chem_comp_atom.type_energy
_chem_comp_atom.partial_charge
_chem_comp_atom.x
_chem_comp_atom.y
_chem_comp_atom.z
DRG    CAI    C      CR16   .      16.6885 17.6071 11.9322
DRG    CAO    C      CR6    .      17.0719 16.4581 12.609
DRG    BRAE   BR     BR     .      17.6533 14.8954 11.6159
DRG    CAM    C      CR6    .      17.0309 16.4233 13.9952
DRG    OAC    O      OH1    .      17.3966 15.2781 14.6661
DRG    CAP    C      CR6    .      16.6066 17.5372 14.7047
DRG    BRAF   BR     BR     .      16.5442 17.4866 16.6438
DRG    CAJ    C      CR16   .      16.2233 18.6861 14.0281
DRG    CAS    C      CR6    .      16.2643 18.721 12.6416
DRG    CAU    C      CT     .      15.9065 19.9754 11.9164
DRG    CAA    C      CH3    .      17.1522 20.7008 11.5334
DRG    CAB    C      CH3    .      15.0786 20.8431 12.8028
DRG    CAT    C      CR6    .      15.1312 19.636 10.6882
DRG    CAK    C      CR16   .      15.5298 20.1361 9.457
DRG    CAQ    C      CR6    .      14.7685 19.8816 8.3255
DRG    BRAG   BR     BR     .      15.3279 20.5783 6.6028
DRG    CAN    C      CR6    .      13.6089 19.127 8.4249
DRG    OAD    O      OH1    .      12.8563 18.8709 7.3018
DRG    CAR    C      CR6    .      13.2102 18.6268 9.6558
DRG    BRAH   BR     BR     .      11.5888 17.5698 9.7944
DRG    CAL    C      CR16   .      13.9713 18.8813 10.7875
DRG    HAI1   H      HCR6   .      16.7316 17.6365 10.9639
DRG    HAC1   H      HOH1   .      18.2197 15.3713 14.9831
DRG    HAJ1   H      HCR6   .      15.9431 19.4705 14.5247
DRG    HAA1   H      HCH3   .      16.9195 21.5633 11.146
DRG    HAA2   H      HCH3   .      17.6466 20.1743 10.8802
DRG    HAA3   H      HCH3   .      17.7038 20.8383 12.324
DRG    HAB1   H      HCH3   .      14.5666 21.4679 12.2591
DRG    HAB2   H      HCH3   .      14.4688 20.289 13.3217
DRG    HAB3   H      HCH3   .      15.6598 21.3388 13.4062
DRG    HAK1   H      HCR6   .      16.3307 20.6784 9.3903
DRG    HAD1   H      HOH1   .      12.2084 19.4737 7.236
DRG    HAL1   H      HCR6   .      13.6834 18.5463 11.6506
#
loop_
_chem_comp_bond.comp_id
_chem_comp_bond.atom_id_1
_chem_comp_bond.atom_id_2
_chem_comp_bond.type
_chem_comp_bond.value_dist
_chem_comp_bond.value_dist_esd
DRG    CAI    CAO    aromatic 1.388  0.02
DRG    CAI    CAS    aromatic 1.387  0.02
DRG    CAO    BRAE   single 1.941  0.02
DRG    CAO    CAM    aromatic 1.387  0.02
DRG    CAM    OAC    single 1.377  0.02
DRG    CAM    CAP    aromatic 1.387  0.02
DRG    CAP    BRAF   single 1.941  0.02
DRG    CAP    CAJ    aromatic 1.387  0.02
DRG    CAJ    CAS    aromatic 1.388  0.02
DRG    CAS    CAU    single 1.492  0.02
DRG    CAU    CAA    single 1.492  0.02
DRG    CAU    CAB    single 1.491  0.02
DRG    CAU    CAT    single 1.492  0.02
DRG    CAT    CAK    aromatic 1.387  0.02
DRG    CAT    CAL    aromatic 1.387  0.02
DRG    CAK    CAQ    aromatic 1.387  0.02
DRG    CAQ    BRAG   single 1.941  0.02
DRG    CAQ    CAN    aromatic 1.387  0.02
DRG    CAN    OAD    single 1.376  0.02
DRG    CAN    CAR    aromatic 1.387  0.02
DRG    CAR    BRAH   single 1.941  0.02
DRG    CAR    CAL    aromatic 1.387  0.02
DRG    HAI1   CAI    single 0.97   0.02
DRG    HAC1   OAC    single 0.887  0.02
DRG    HAJ1   CAJ    single 0.97   0.02
DRG    HAA1   CAA    single 0.974  0.02
DRG    HAA2   CAA    single 0.974  0.02
DRG    HAA3   CAA    single 0.974  0.02
DRG    HAB1   CAB    single 0.974  0.02
DRG    HAB2   CAB    single 0.974  0.02
DRG    HAB3   CAB    single 0.973  0.02
DRG    HAK1   CAK    single 0.97   0.02
DRG    HAD1   OAD    single 0.887  0.02
DRG    HAL1   CAL    single 0.97   0.02
#
loop_
_chem_comp_angle.comp_id
_chem_comp_angle.atom_id_1
_chem_comp_angle.atom_id_2
_chem_comp_angle.atom_id_3
_chem_comp_angle.value_angle
_chem_comp_angle.value_angle_esd
DRG    BRAE   CAO    CAI    120.0  3.0
DRG    CAM    CAO    CAI    120.0  3.0
DRG    CAJ    CAS    CAI    119.99 3.0
DRG    CAU    CAS    CAI    119.98 3.0
DRG    CAS    CAI    CAO    120.0  3.0
DRG    OAC    CAM    CAO    120.0  3.0
DRG    CAP    CAM    CAO    120.0  3.0
DRG    CAM    CAO    BRAE   120.0  3.0
DRG    BRAF   CAP    CAM    120.0  3.0
DRG    CAJ    CAP    CAM    120.0  3.0
DRG    CAP    CAM    OAC    120.0  3.0
DRG    CAS    CAJ    CAP    120.0  3.0
DRG    CAJ    CAP    BRAF   120.0  3.0
DRG    CAU    CAS    CAJ    119.98 3.0
DRG    CAA    CAU    CAS    109.47 3.0
DRG    CAB    CAU    CAS    109.47 3.0
DRG    CAT    CAU    CAS    109.48 3.0
DRG    CAK    CAT    CAU    119.96 3.0
DRG    CAL    CAT    CAU    119.96 3.0
DRG    CAB    CAU    CAA    109.47 3.0
DRG    CAT    CAU    CAA    109.47 3.0
DRG    CAT    CAU    CAB    109.47 3.0
DRG    CAQ    CAK    CAT    120.0  3.0
DRG    CAR    CAL    CAT    120.0  3.0
DRG    CAL    CAT    CAK    119.99 3.0
DRG    BRAG   CAQ    CAK    120.0  3.0
DRG    CAN    CAQ    CAK    120.0  3.0
DRG    OAD    CAN    CAQ    120.0  3.0
DRG    CAR    CAN    CAQ    120.0  3.0
DRG    CAN    CAQ    BRAG   120.0  3.0
DRG    BRAH   CAR    CAN    120.0  3.0
DRG    CAL    CAR    CAN    120.0  3.0
DRG    CAR    CAN    OAD    120.0  3.0
DRG    CAL    CAR    BRAH   120.0  3.0
DRG    HAI1   CAI    CAO    120.0  3.0
DRG    HAC1   OAC    CAM    109.47 3.0
DRG    HAJ1   CAJ    CAP    119.99 3.0
DRG    HAI1   CAI    CAS    120.0  3.0
DRG    HAJ1   CAJ    CAS    119.99 3.0
DRG    HAA1   CAA    CAU    109.47 3.0
DRG    HAA2   CAA    CAU    109.47 3.0
DRG    HAA3   CAA    CAU    109.47 3.0
DRG    HAB1   CAB    CAU    109.47 3.0
DRG    HAB2   CAB    CAU    109.47 3.0
DRG    HAB3   CAB    CAU    109.47 3.0
DRG    HAK1   CAK    CAT    119.99 3.0
DRG    HAL1   CAL    CAT    119.99 3.0
DRG    HAK1   CAK    CAQ    119.99 3.0
DRG    HAD1   OAD    CAN    109.47 3.0
DRG    HAL1   CAL    CAR    119.99 3.0
DRG    HAA2   CAA    HAA1   109.47 3.0
DRG    HAA3   CAA    HAA1   109.47 3.0
DRG    HAA3   CAA    HAA2   109.47 3.0
DRG    HAB2   CAB    HAB1   109.47 3.0
DRG    HAB3   CAB    HAB1   109.47 3.0
DRG    HAB3   CAB    HAB2   109.47 3.0
#
loop_
_chem_comp_tor.comp_id
_chem_comp_tor.id
_chem_comp_tor.atom_id_1
_chem_comp_tor.atom_id_2
_chem_comp_tor.atom_id_3
_chem_comp_tor.atom_id_4
_chem_comp_tor.value_angle
_chem_comp_tor.value_angle_esd
_chem_comp_tor.period
DRG    CONST_01 CAP    CAM    CAO    CAI    -0.0   0.0    0
DRG    CONST_02 CAP    CAJ    CAS    CAI    -0.0   0.0    0
DRG    CONST_03 CAJ    CAS    CAI    CAO    0.0    0.0    0
DRG    CONST_04 CAJ    CAP    CAM    CAO    0.0    0.0    0
DRG    CONST_05 CAS    CAI    CAO    CAM    0.0    0.0    0
DRG    CONST_06 CAS    CAJ    CAP    CAM    -0.0   0.0    0
DRG    CONST_07 CAN    CAQ    CAK    CAT    0.0    0.0    0
DRG    CONST_08 CAN    CAR    CAL    CAT    -0.0   0.0    0
DRG    CONST_09 CAR    CAL    CAT    CAK    0.0    0.0    0
DRG    CONST_10 CAR    CAN    CAQ    CAK    -0.0   0.0    0
DRG    CONST_11 CAL    CAT    CAK    CAQ    -0.0   0.0    0
DRG    CONST_12 CAL    CAR    CAN    CAQ    0.0    0.0    0
DRG    CONST_13 OAC    CAM    CAO    CAI    179.26 0.0    0
DRG    CONST_14 CAU    CAS    CAI    CAO    177.45 0.0    0
DRG    CONST_15 BRAF   CAP    CAM    CAO    179.82 0.0    0
DRG    CONST_16 CAS    CAI    CAO    BRAE   179.56 0.0    0
DRG    CONST_17 CAP    CAM    CAO    BRAE   -179.56 0.0    0
DRG    CONST_18 CAJ    CAP    CAM    OAC    -179.26 0.0    0
DRG    CONST_19 CAU    CAS    CAJ    CAP    -177.45 0.0    0
DRG    CONST_20 CAS    CAJ    CAP    BRAF   -179.82 0.0    0
DRG    CONST_21 CAQ    CAK    CAT    CAU    -176.47 0.0    0
DRG    CONST_22 CAR    CAL    CAT    CAU    176.47 0.0    0
DRG    CONST_23 BRAG   CAQ    CAK    CAT    -179.88 0.0    0
DRG    CONST_24 BRAH   CAR    CAL    CAT    179.94 0.0    0
DRG    CONST_25 OAD    CAN    CAQ    CAK    -179.79 0.0    0
DRG    CONST_26 BRAH   CAR    CAN    CAQ    -179.94 0.0    0
DRG    CONST_27 CAR    CAN    CAQ    BRAG   179.88 0.0    0
DRG    CONST_28 CAL    CAR    CAN    OAD    179.79 0.0    0
DRG    CONST_29 HAJ1   CAJ    CAS    CAI    178.82 0.0    0
DRG    CONST_30 HAI1   CAI    CAO    CAM    178.96 0.0    0
DRG    CONST_31 HAJ1   CAJ    CAP    CAM    -178.82 0.0    0
DRG    CONST_32 HAI1   CAI    CAS    CAJ    -178.96 0.0    0
DRG    CONST_33 HAL1   CAL    CAT    CAK    -178.8 0.0    0
DRG    CONST_34 HAK1   CAK    CAQ    CAN    -178.78 0.0    0
DRG    CONST_35 HAL1   CAL    CAR    CAN    178.8  0.0    0
DRG    CONST_36 HAK1   CAK    CAT    CAL    178.78 0.0    0
DRG    Var_01 CAA    CAU    CAS    CAI    -78.5  30.0   2
DRG    Var_02 CAB    CAU    CAS    CAI    161.5  30.0   2
DRG    Var_03 CAT    CAU    CAS    CAI    41.5   30.0   2
DRG    Var_04 CAA    CAU    CAS    CAJ    98.94  30.0   2
DRG    Var_05 CAB    CAU    CAS    CAJ    -21.05 30.0   2
DRG    Var_06 CAT    CAU    CAS    CAJ    -141.05 30.0   2
DRG    Var_07 CAK    CAT    CAU    CAS    -127.64 30.0   2
DRG    Var_08 CAL    CAT    CAU    CAS    55.89  30.0   2
DRG    Var_09 CAK    CAT    CAU    CAA    -7.64  30.0   2
DRG    Var_10 CAL    CAT    CAU    CAA    175.89 30.0   2
DRG    Var_11 CAK    CAT    CAU    CAB    112.36 30.0   2
DRG    Var_12 CAL    CAT    CAU    CAB    -64.11 30.0   2
DRG    Var_13 HAC1   OAC    CAM    CAO    101.33 30.0   2
DRG    Var_14 HAC1   OAC    CAM    CAP    -79.41 30.0   2
DRG    Var_15 HAA1   CAA    CAU    CAS    -173.98 30.0   3
DRG    Var_16 HAA2   CAA    CAU    CAS    66.02  30.0   3
DRG    Var_17 HAA3   CAA    CAU    CAS    -53.98 30.0   3
DRG    Var_18 HAB1   CAB    CAU    CAS    -158.24 30.0   3
DRG    Var_19 HAB2   CAB    CAU    CAS    -38.24 30.0   3
DRG    Var_20 HAB3   CAB    CAU    CAS    81.76  30.0   3
DRG    Var_21 HAB1   CAB    CAU    CAA    81.76  30.0   3
DRG    Var_22 HAB2   CAB    CAU    CAA    -158.23 30.0   3
DRG    Var_23 HAB3   CAB    CAU    CAA    -38.23 30.0   3
DRG    Var_24 HAA1   CAA    CAU    CAB    -53.99 30.0   3
DRG    Var_25 HAA2   CAA    CAU    CAB    -173.99 30.0   3
DRG    Var_26 HAA3   CAA    CAU    CAB    66.01  30.0   3
DRG    Var_27 HAA1   CAA    CAU    CAT    66.01  30.0   3
DRG    Var_28 HAA2   CAA    CAU    CAT    -53.99 30.0   3
DRG    Var_29 HAA3   CAA    CAU    CAT    -173.99 30.0   3
DRG    Var_30 HAB1   CAB    CAU    CAT    -38.24 30.0   3
DRG    Var_31 HAB2   CAB    CAU    CAT    81.77  30.0   3
DRG    Var_32 HAB3   CAB    CAU    CAT    -158.24 30.0   3
DRG    Var_33 HAD1   OAD    CAN    CAQ    -94.83 30.0   2
DRG    Var_34 HAD1   OAD    CAN    CAR    85.38  30.0   2
#
loop_
_chem_comp_plane_atom.comp_id
_chem_comp_plane_atom.plane_id
_chem_comp_plane_atom.atom_id
_chem_comp_plane_atom.dist_esd
DRG    plan-1 CAI    0.02
DRG    plan-1 CAO    0.02
DRG    plan-1 BRAE   0.02
DRG    plan-1 CAM    0.02
DRG    plan-1 OAC    0.02
DRG    plan-1 CAP    0.02
DRG    plan-1 BRAF   0.02
DRG    plan-1 CAJ    0.02
DRG    plan-1 CAS    0.02
DRG    plan-1 CAU    0.02
DRG    plan-1 HAI1   0.02
DRG    plan-1 HAJ1   0.02
DRG    plan-2 CAU    0.02
DRG    plan-2 CAT    0.02
DRG    plan-2 CAK    0.02
DRG    plan-2 CAQ    0.02
DRG    plan-2 BRAG   0.02
DRG    plan-2 CAN    0.02
DRG    plan-2 OAD    0.02
DRG    plan-2 CAR    0.02
DRG    plan-2 BRAH   0.02
DRG    plan-2 CAL    0.02
DRG    plan-2 HAK1   0.02
DRG    plan-2 HAL1   0.02
"""

par1_str = """\
refinement {
  refine {
    strategy = individual_sites individual_sites_real_space rigid_body \
               individual_adp group_adp *tls occupancies group_anomalous
    adp {
      individual {
        isotropic = not (chain C and resname DRG and resseq 1)
      }
      group_adp_refinement_mode = one_adp_group_per_residue \
                                  two_adp_groups_per_residue *group_selection
      group = chain C and resname DRG and resseq 1
      tls = chain A or chain B
    }
  }
  main {
    number_of_macro_cycles=2
    bulk_solvent_and_scale=False
  }
}
"""
par2_str = """\
refinement {
  refine {
    strategy = individual_sites individual_sites_real_space rigid_body \
               individual_adp *group_adp *tls occupancies group_anomalous
    adp {
      individual {
        isotropic = not (chain C and resname DRG and resseq 1)
      }
      group_adp_refinement_mode = one_adp_group_per_residue \
                                  two_adp_groups_per_residue *group_selection
      group = chain C and resname DRG and resseq 1
      tls = chain A or chain B
    }
  }
  main {
    number_of_macro_cycles=2
    bulk_solvent_and_scale=False
  }
}
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  TLS and/or group B refinement in presence of ligands.
  """
  pdb_in = "%s.pdb"%prefix
  open(pdb_in,"w").write(pdb_str)
  cif_in = "%s.cif"%prefix
  open(cif_in,"w").write(cif_str)
  args = [
    pdb_in,
    'high_resolution=5',
    'label=FOBS',
    'type=real',
    "r_free=0.1"]
  r0 = run_fmodel(args = args, prefix = prefix)
  for i_par, par in enumerate([par1_str,par2_str]):
    par_in = "%s_%s.params"%(prefix,str(i_par))
    open(par_in, "w").write(par)
    args = [pdb_in, par_in, r0.mtz, cif_in]
    r = run_phenix_refine(args = args, prefix=prefix)

if (__name__ == "__main__"):
  run()
  print("OK")
