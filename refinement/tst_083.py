from __future__ import division
from __future__ import print_function
from phenix_regression.refinement import run_phenix_refine
import libtbx.load_env
import os
import iotbx.pdb
from libtbx.test_utils import approx_equal

par_str="""\
refinement.main.bulk_solvent_and_scale=false
refinement.main.number_of_macro_cycles=20
refinement.main.scattering_table=wk1995
refinement.structure_factors_and_gradients_accuracy.algorithm=direct
refinement.fake_f_obs.structure_factors_accuracy.algorithm=direct
refinement.refine.strategy=rigid_body
refinement.main.target=ls
refinement.rigid_body.bulk_solvent_and_scale=false
refinement.main.fake_f_obs=true
data_manager.fmodel.xray_data.high_resolution=1.5
refinement.modify_start_model.modify.sites.translate=1.0 0 0
refinement.modify_start_model.output.file_name=%s
refinement.refine.sites.rigid_body=chain B
refinement.refine.sites.rigid_body=chain A
refinement.refine.sites.rigid_body=chain E
refinement.refine.sites.rigid_body=chain C
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Rigid-body refinement.
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/enk_rbr.pdb", test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/enk_rbr.pdb_fcalc_direct_wk1995_resolution_1.0.hkl", \
    test=os.path.isfile)
  #
  par = "%s.params"%prefix
  pdb_mod = "%s_mod.pdb"%prefix
  open(par, "w").write(par_str%pdb_mod)
  args = [pdb, hkl, par]
  r = run_phenix_refine(args = args, prefix=prefix)
  #
  atoms1 = []
  for line in iotbx.pdb.input(
      source_info = None, file_name=pdb).construct_hierarchy().\
      as_pdb_string().split("\n"):
    if(line.startswith("ATOM") and line.split()[3]=="LEU"
                               and line.split()[4]=="D"): atoms1.append(line)
  atoms2 = []
  for line in open(r.pdb,"r").read().splitlines():
    if(line.startswith("ATOM") and line.split()[3]=="LEU"
                               and line.split()[4]=="D"): atoms2.append(line)
  atoms3 = []
  for line in open(pdb_mod,"r").read().splitlines():
    if(line.startswith("ATOM") and line.split()[3]=="LEU"
                               and line.split()[4]=="D"): atoms3.append(line)

  assert len(atoms1) == len(atoms2) == len(atoms3)
  for a1, a2, a3 in zip(atoms1, atoms2, atoms3):
    assert approx_equal(float(a1.split()[6]) -float(a2.split()[6]),-1.0)
    assert approx_equal(float(a1.split()[7]), float(a2.split()[7]))
    assert approx_equal(float(a1.split()[8]), float(a2.split()[8]))
    assert approx_equal(float(a2.split()[6]), float(a3.split()[6]))
    assert approx_equal(float(a2.split()[7]), float(a3.split()[7]))
    assert approx_equal(float(a2.split()[8]), float(a3.split()[8]))

if (__name__ == "__main__"):
  run()
  print("OK")
