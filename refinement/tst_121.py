from __future__ import division
from __future__ import print_function
import iotbx.pdb
from cctbx.array_family import flex
import time, os
from libtbx import easy_run
import libtbx.load_env
from phenix_regression.refinement import run_phenix_refine

pdb_str = """
CRYST1    4.866   17.275   23.046  90.00  90.00  90.00 P 21 21 21
SCALE1      0.205508  0.000000  0.000000        0.00000
SCALE2      0.000000  0.057887  0.000000        0.00000
SCALE3      0.000000  0.000000  0.043391        0.00000
ATOM      1  N   PHE A   1       1.452   1.861  17.152  1.00  6.23           N
ATOM      2  CA  PHE A   1       0.693   3.105  17.422  1.00  6.10           C
ATOM      3  C   PHE A   1       1.531   4.310  17.050  1.00  5.73           C
ATOM      4  O   PHE A   1       2.751   4.218  16.937  1.00  6.25           O
ATOM      5  CB  PHE A   1       0.232   3.167  18.887  1.00  6.12           C
ATOM      6  CG  PHE A   1       1.326   3.476  19.879  1.00  6.04           C
ATOM      7  CD1 PHE A   1       1.690   4.789  20.134  1.00  6.26           C
ATOM      8  CD2 PHE A   1       1.980   2.472  20.571  1.00  6.40           C
ATOM      9  CE1 PHE A   1       2.687   5.087  21.038  1.00  6.62           C
ATOM     10  CE2 PHE A   1       2.974   2.771  21.486  1.00  6.66           C
ATOM     11  CZ  PHE A   1       3.332   4.077  21.718  1.00  6.73           C
ATOM     12  H1  PHE A   1       1.491   1.723  16.274  1.00  7.47           H
ATOM     13  H2  PHE A   1       2.276   1.940  17.477  1.00  7.47           H
ATOM     14  H3  PHE A   1       1.044   1.174  17.543  1.00  7.47           H
ATOM     15  HA  PHE A   1      -0.100   3.113  16.863  1.00  7.32           H
ATOM     16  HB2 PHE A   1      -0.444   3.859  18.970  1.00  7.34           H
ATOM     17  HB3 PHE A   1      -0.149   2.308  19.129  1.00  7.34           H
ATOM     18  HD1 PHE A   1       1.264   5.479  19.679  1.00  7.52           H
ATOM     19  HD2 PHE A   1       1.747   1.584  20.420  1.00  7.68           H
ATOM     20  HE1 PHE A   1       2.921   5.974  21.193  1.00  7.94           H
ATOM     21  HE2 PHE A   1       3.408   2.084  21.939  1.00  7.99           H
ATOM     22  HZ  PHE A   1       4.002   4.278  22.331  1.00  8.08           H
ATOM     23  N   PHE A   2       0.867   5.446  16.879  1.00  6.02           N
ATOM     24  CA  PHE A   2       1.539   6.723  16.727  1.00  6.16           C
ATOM     25  C   PHE A   2       0.636   7.849  17.204  1.00  6.27           C
ATOM     26  O   PHE A   2      -0.368   7.542  17.893  1.00  7.07           O
ATOM     27  CB  PHE A   2       2.096   6.931  15.300  1.00  6.31           C
ATOM     28  CG  PHE A   2       1.080   6.828  14.193  1.00  6.24           C
ATOM     29  CD1 PHE A   2       0.459   7.961  13.691  1.00  6.56           C
ATOM     30  CD2 PHE A   2       0.766   5.606  13.621  1.00  6.60           C
ATOM     31  CE1 PHE A   2      -0.457   7.879  12.655  1.00  6.81           C
ATOM     32  CE2 PHE A   2      -0.152   5.519  12.586  1.00  6.99           C
ATOM     33  CZ  PHE A   2      -0.766   6.657  12.106  1.00  6.88           C
ATOM     34  OXT PHE A   2       0.949   9.032  16.930  1.00  6.95           O
ATOM     35  H   PHE A   2       0.010   5.501  16.848  1.00  7.23           H
ATOM     36  HA  PHE A   2       2.306   6.720  17.320  1.00  7.40           H
ATOM     37  HB2 PHE A   2       2.493   7.814  15.251  1.00  7.57           H
ATOM     38  HB3 PHE A   2       2.776   6.259  15.135  1.00  7.57           H
ATOM     39  HD1 PHE A   2       0.663   8.792  14.055  1.00  7.87           H
ATOM     40  HD2 PHE A   2       1.175   4.833  13.937  1.00  7.93           H
ATOM     41  HE1 PHE A   2      -0.870   8.649  12.337  1.00  8.18           H
ATOM     42  HE2 PHE A   2      -0.359   4.690  12.218  1.00  8.39           H
ATOM     43  HZ  PHE A   2      -1.385   6.598  11.414  1.00  8.25           H
TER
ATOM     44  C   MOH B   1       5.108   7.962  21.252  1.00  7.16           C
ATOM     45  O   MOH B   1       3.978   8.323  20.450  1.00  7.11           O
ATOM     46  H1  MOH B   1       5.418   8.849  21.805  1.00  8.60           H
ATOM     47  H2  MOH B   1       4.811   7.182  21.951  1.00  8.60           H
ATOM     48  H3  MOH B   1       5.905   7.589  20.608  1.00  8.60           H
ATOM     49  HO  MOH B   1       3.254   8.641  21.030  1.00  8.53           H
ATOM     50  C   MOH B   2       3.335   2.446  14.155  1.00  6.90           C
ATOM     51  O   MOH B   2       1.969   2.158  14.465  1.00  6.84           O
ATOM     52  H1  MOH B   2       3.990   2.246  15.004  1.00  8.27           H
ATOM     53  H2  MOH B   2       3.341   3.513  13.935  1.00  8.27           H
ATOM     54  H3  MOH B   2       3.630   1.890  13.266  1.00  8.27           H
ATOM     55  HO  MOH B   2       1.702   2.661  15.263  1.00  8.20           H
TER
END
"""

params_str = """
refinement {
  refine {
  strategy = *individual_sites *individual_adp
    adp {
      individual {
        isotropic = element H
        anisotropic = not element H
      }
    }
  }
  main {
    number_of_macro_cycles = 5
    max_number_of_iterations=25
  }
  hydrogens {
    refine = *individual riding Auto
  }
  target_weights {
    wxc_scale = 5
    wxu_scale = 5
  }
  structure_factors_and_gradients_accuracy {
    algorithm = fft *direct
  }
}
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  high_res_with_rotatable_h.
  """
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/data/high_res_with_rotatable_h.mtz",
    test=os.path.isfile)
  #
  pdb_inp = iotbx.pdb.input(source_info=None, lines=pdb_str)
  ph = pdb_inp.construct_hierarchy()
  xrs_answer = pdb_inp.xray_structure_simple()
  xrs_answer.switch_to_neutron_scattering_dictionary()
  ph.write_pdb_file(file_name = "%s_start.pdb"%prefix)
  #
  cmd = "phenix.maps %s %s > zlog" % ("%s_start.pdb"%prefix, hkl)
  assert not easy_run.call(cmd)
  #
  of = open("%s.params"%prefix,"w")
  of.write(params_str)
  of.close()
  #
  args = [
    "%s_start.pdb"%prefix,
    hkl,
    "%s.params"%prefix]
  r = run_phenix_refine(args = args, prefix = prefix)
  assert r.r_work_final < 0.0240

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("Total time: %-8.4f"%(time.time()-t0))
  print("OK")
