from __future__ import division
from iotbx import file_reader
from libtbx.utils import null_out
import libtbx.load_env
import shutil
import os.path
import iotbx.pdb
from libtbx.test_utils import contains_lines
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel

pdb_str_1 = """
CRYST1   22.386   23.624   19.644  90.00  90.00  90.00 P 1
ATOM   7342  C1  MAN A3268       8.429  10.670   5.681  1.00115.35           C
ATOM   7343  C2  MAN A3268       8.255  10.789   7.190  1.00122.80           C
ATOM   7344  O2  MAN A3268       8.739   9.619   7.815  1.00115.62           O
ATOM   7345  C3  MAN A3268       9.017  12.001   7.714  1.00126.09           C
ATOM   7346  O3  MAN A3268       8.986  12.002   9.126  1.00139.63           O
ATOM   7347  C4  MAN A3268      10.449  12.023   7.187  1.00112.75           C
ATOM   7348  O4  MAN A3268      11.031  13.262   7.511  1.00110.42           O
ATOM   7349  C5  MAN A3268      10.427  11.854   5.673  1.00114.54           C
ATOM   7350  O5  MAN A3268       9.800  10.629   5.369  1.00117.42           O
ATOM   7351  C6  MAN A3268      11.798  11.861   5.000  1.00121.61           C
ATOM   7352  O6  MAN A3268      12.857  12.248   5.845  1.00123.73           O
ATOM   7353  C1  MAN A3269       7.831  12.748   9.563  1.00140.40           C
ATOM   7354  C2  MAN A3269       8.134  13.392  10.908  1.00141.58           C
ATOM   7355  O2  MAN A3269       7.050  14.199  11.314  1.00141.53           O
ATOM   7356  C3  MAN A3269       8.389  12.302  11.937  1.00138.82           C
ATOM   7357  O3  MAN A3269       8.560  12.887  13.208  1.00139.15           O
ATOM   7358  C4  MAN A3269       7.208  11.337  11.970  1.00133.28           C
ATOM   7359  O4  MAN A3269       7.525  10.218  12.770  1.00127.59           O
ATOM   7360  C5  MAN A3269       6.839  10.861  10.568  1.00130.49           C
ATOM   7361  O5  MAN A3269       6.669  11.957   9.694  1.00134.24           O
ATOM   7362  C6  MAN A3269       5.544  10.060  10.610  1.00125.78           C
ATOM   7363  O6  MAN A3269       5.157   9.721   9.298  1.00122.54           O
ATOM   7364  C1  MAN A3270      14.027  11.554   5.366  1.00139.38           C
ATOM   7365  C2  MAN A3270      14.159  10.189   6.027  1.00142.75           C
ATOM   7366  O2  MAN A3270      15.226   9.476   5.439  1.00134.57           O
ATOM   7367  C3  MAN A3270      14.409  10.333   7.524  1.00145.94           C
ATOM   7368  O3  MAN A3270      14.817   9.069   8.004  1.00143.26           O
ATOM   7369  C4  MAN A3270      15.481  11.382   7.815  1.00143.56           C
ATOM   7370  O4  MAN A3270      15.378  11.797   9.158  1.00131.47           O
ATOM   7371  C5  MAN A3270      15.346  12.628   6.944  1.00151.91           C
ATOM   7372  O5  MAN A3270      15.215  12.274   5.588  1.00145.42           O
ATOM   7373  C6  MAN A3270      16.568  13.526   7.105  1.00161.53           C
ATOM   7374  O6  MAN A3270      16.353  14.746   6.431  1.00163.34           O
ATOM   7375  C1  MAN A3271      14.923   9.073   9.439  1.00134.25           C
ATOM   7376  C2  MAN A3271      16.226   8.399   9.854  1.00128.82           C
ATOM   7377  O2  MAN A3271      16.339   8.383  11.260  1.00119.55           O
ATOM   7378  C3  MAN A3271      16.248   6.971   9.324  1.00125.96           C
ATOM   7379  O3  MAN A3271      17.386   6.293   9.815  1.00109.33           O
ATOM   7380  C4  MAN A3271      14.972   6.252   9.756  1.00128.89           C
ATOM   7381  O4  MAN A3271      14.881   5.000   9.114  1.00122.88           O
ATOM   7382  C5  MAN A3271      13.737   7.090   9.424  1.00131.85           C
ATOM   7383  O5  MAN A3271      13.855   8.365  10.017  1.00134.39           O
ATOM   7384  C6  MAN A3271      12.452   6.424   9.900  1.00130.34           C
ATOM   7385  O6  MAN A3271      11.348   7.225   9.538  1.00130.42           O
ATOM  13173  C1  MAN A3272       7.150  14.738  12.673  1.00 20.00           C
ATOM  13174  C2  MAN A3272       6.059  15.595  13.307  1.00 20.00           C
ATOM  13175  O2  MAN A3272       6.395  15.904  14.644  1.00 20.00           O
ATOM  13176  C3  MAN A3272       5.870  16.881  12.507  1.00 20.00           C
ATOM  13177  O3  MAN A3272       5.000  17.752  13.195  1.00 20.00           O
ATOM  13178  C4  MAN A3272       7.207  17.580  12.269  1.00 20.00           C
ATOM  13179  O4  MAN A3272       7.022  18.624  11.337  1.00 20.00           O
ATOM  13180  C5  MAN A3272       8.271  16.606  11.763  1.00 20.00           C
ATOM  13181  C6  MAN A3272       9.645  17.266  11.699  1.00 20.00           C
ATOM  13182  O6  MAN A3272      10.491  16.510  10.862  1.00 20.00           O
ATOM  13183  O5  MAN A3272       8.350  15.481  12.615  1.00 20.00           O
END
"""

pdb_str_2 = """
REMARK    VIRAL PROTEIN                           02-APR-08   2ZL6
CRYST1   18.398   17.352   20.243  90.00  90.00  90.00 P 1
HETATM    1  C1  FUC B   1      10.636   8.348  12.353  1.00 16.99           C
HETATM    2  C2  FUC B   1      10.554   9.104  13.687  1.00 16.61           C
HETATM    3  C3  FUC B   1       9.121   9.559  13.954  1.00 17.43           C
HETATM    4  C4  FUC B   1       8.166   8.362  13.876  1.00 17.26           C
HETATM    5  C5  FUC B   1       8.305   7.709  12.484  1.00 17.47           C
HETATM    6  C6  FUC B   1       7.397   6.492  12.262  1.00 17.92           C
HETATM    7  O2  FUC B   1      11.488  10.175  13.651  1.00 16.74           O
HETATM    8  O3  FUC B   1       9.098  10.168  15.243  1.00 16.61           O
HETATM    9  O4  FUC B   1       8.507   7.450  14.931  1.00 17.26           O
HETATM   10  O5  FUC B   1       9.660   7.290  12.256  1.00 16.19           O
HETATM   11  C1  GAL B   2      10.098   9.076   8.909  1.00 19.60           C
HETATM   12  C2  GAL B   2      11.123   8.840  10.025  1.00 17.87           C
HETATM   13  C3  GAL B   2      12.431   9.573   9.768  1.00 18.68           C
HETATM   14  C4  GAL B   2      12.991   9.221   8.397  1.00 19.53           C
HETATM   15  C5  GAL B   2      11.905   9.472   7.341  1.00 20.47           C
HETATM   16  C6  GAL B   2      12.297   9.035   5.950  1.00 22.47           C
HETATM   17  O2  GAL B   2      10.536   9.250  11.256  1.00 18.17           O
HETATM   18  O3  GAL B   2      13.398   9.258  10.789  1.00 17.44           O
HETATM   19  O4  GAL B   2      13.379   7.842   8.419  1.00 16.34           O
HETATM   20  O5  GAL B   2      10.730   8.691   7.671  1.00 19.08           O
HETATM   21  O6  GAL B   2      11.322   9.450   5.000  1.00 24.46           O
HETATM   22  C1  NAG B   3       5.336   9.202   8.340  1.00 23.19           C
HETATM   23  C2  NAG B   3       6.651   9.140   9.169  1.00 23.06           C
HETATM   24  C3  NAG B   3       7.792   8.482   8.342  1.00 21.44           C
HETATM   25  C4  NAG B   3       7.334   7.104   7.771  1.00 22.63           C
HETATM   26  C5  NAG B   3       5.982   7.237   7.010  1.00 24.10           C
HETATM   27  C6  NAG B   3       5.415   5.886   6.542  1.00 26.05           C
HETATM   28  C7  NAG B   3       6.821  10.949  10.800  1.00 24.81           C
HETATM   29  C8  NAG B   3       7.322  12.352  11.077  1.00 25.71           C
HETATM   30  N2  NAG B   3       7.076  10.495   9.581  1.00 23.17           N
HETATM   31  O3  NAG B   3       8.974   8.218   9.086  1.00 19.94           O
HETATM   32  O4  NAG B   3       8.329   6.600   6.897  1.00 22.28           O
HETATM   33  O5  NAG B   3       5.000   7.862   7.891  1.00 25.17           O
HETATM   34  O6  NAG B   3       5.194   5.000   7.645  1.00 30.48           O
HETATM   35  O7  NAG B   3       6.225  10.306  11.657  1.00 24.06           O
END

"""

pdb_str_3 = """
CRYST1   30.852   23.624   30.291  90.00  90.00  90.00 P 1
ATOM      1  N   ASN A 266       6.431   8.673   5.829  1.00 59.26           N
ATOM      2  CA  ASN A 266       7.135   7.429   5.538  1.00 50.45           C
ATOM      3  C   ASN A 266       6.182   6.377   5.000  1.00 56.41           C
ATOM      4  O   ASN A 266       5.000   6.367   5.336  1.00 69.09           O
ATOM      5  CB  ASN A 266       7.820   6.873   6.787  1.00 53.46           C
ATOM      6  CG  ASN A 266       8.952   7.747   7.272  1.00 68.50           C
ATOM      7  OD1 ASN A 266       9.592   8.449   6.487  1.00 61.27           O
ATOM      8  ND2 ASN A 266       9.203   7.709   8.580  1.00 83.77           N
TER
ATOM      9  C1  NAG A3266      10.567   8.078   8.851  1.00 62.77           C
ATOM     10  C2  NAG A3266      10.365   8.988  10.053  1.00 60.83           C
ATOM     11  C3  NAG A3266      11.710   9.482  10.564  1.00 60.78           C
ATOM     12  C4  NAG A3266      12.688   8.326  10.744  1.00 62.36           C
ATOM     13  C5  NAG A3266      12.666   7.354   9.570  1.00 62.14           C
ATOM     14  C6  NAG A3266      13.483   6.104   9.878  1.00 69.33           C
ATOM     15  C7  NAG A3266       8.205  10.078   9.952  1.00 71.27           C
ATOM     16  C8  NAG A3266       7.405  11.249   9.465  1.00 71.48           C
ATOM     17  N2  NAG A3266       9.511  10.105   9.700  1.00 64.34           N
ATOM     18  O3  NAG A3266      11.531  10.143  11.795  1.00 76.59           O
ATOM     19  O4  NAG A3266      13.988   8.857  10.839  1.00 75.05           O
ATOM     20  O5  NAG A3266      11.344   6.978   9.263  1.00 62.17           O
ATOM     21  O6  NAG A3266      12.798   5.317  10.824  1.00 81.16           O
ATOM     22  O7  NAG A3266       7.657   9.149  10.547  1.00 71.07           O
ATOM     23  C1  NAG A3267      14.478   8.785  12.188  1.00 66.35           C
ATOM     24  C2  NAG A3267      15.953   9.142  12.141  1.00 66.17           C
ATOM     25  C3  NAG A3267      16.585   9.071  13.522  1.00 76.21           C
ATOM     26  C4  NAG A3267      15.726   9.815  14.538  1.00 81.21           C
ATOM     27  C5  NAG A3267      14.247   9.472  14.420  1.00 78.06           C
ATOM     28  C6  NAG A3267      13.432  10.382  15.331  1.00 88.76           C
ATOM     29  C7  NAG A3267      16.831   8.601   9.960  1.00 72.31           C
ATOM     30  C8  NAG A3267      17.798   7.758   9.183  1.00 55.44           C
ATOM     31  N2  NAG A3267      16.618   8.242  11.222  1.00 72.55           N
ATOM     32  O3  NAG A3267      17.872   9.646  13.466  1.00 75.16           O
ATOM     33  O4  NAG A3267      16.173   9.524  15.843  1.00 99.04           O
ATOM     34  O5  NAG A3267      13.807   9.635  13.089  1.00 75.71           O
ATOM     35  O6  NAG A3267      13.862  11.717  15.170  1.00 92.82           O
ATOM     36  O7  NAG A3267      16.274   9.569   9.438  1.00 73.54           O
ATOM     37  C1  MAN A3268      16.895  10.670  16.328  1.00115.35           C
ATOM     38  C2  MAN A3268      16.721  10.789  17.837  1.00122.80           C
ATOM     39  C3  MAN A3268      17.483  12.001  18.361  1.00126.09           C
ATOM     40  C4  MAN A3268      18.915  12.023  17.834  1.00112.75           C
ATOM     41  C5  MAN A3268      18.893  11.854  16.320  1.00114.54           C
ATOM     42  C6  MAN A3268      20.264  11.861  15.647  1.00121.61           C
ATOM     43  O2  MAN A3268      17.205   9.619  18.462  1.00115.62           O
ATOM     44  O3  MAN A3268      17.452  12.002  19.773  1.00139.63           O
ATOM     45  O4  MAN A3268      19.497  13.262  18.158  1.00110.42           O
ATOM     46  O5  MAN A3268      18.266  10.629  16.016  1.00117.42           O
ATOM     47  O6  MAN A3268      21.323  12.248  16.492  1.00123.73           O
ATOM     48  C1  MAN A3269      16.297  12.748  20.210  1.00140.40           C
ATOM     49  C2  MAN A3269      16.600  13.392  21.555  1.00141.58           C
ATOM     50  C3  MAN A3269      16.855  12.302  22.584  1.00138.82           C
ATOM     51  C4  MAN A3269      15.674  11.337  22.617  1.00133.28           C
ATOM     52  C5  MAN A3269      15.305  10.861  21.215  1.00130.49           C
ATOM     53  C6  MAN A3269      14.010  10.060  21.257  1.00125.78           C
ATOM     54  O2  MAN A3269      15.516  14.199  21.961  1.00141.53           O
ATOM     55  O3  MAN A3269      17.026  12.887  23.855  1.00139.15           O
ATOM     56  O4  MAN A3269      15.991  10.218  23.417  1.00127.59           O
ATOM     57  O5  MAN A3269      15.135  11.957  20.341  1.00134.24           O
ATOM     58  O6  MAN A3269      13.623   9.721  19.945  1.00122.54           O
ATOM     59  C1  MAN A3270      22.493  11.554  16.013  1.00139.38           C
ATOM     60  C2  MAN A3270      22.625  10.189  16.674  1.00142.75           C
ATOM     61  C3  MAN A3270      22.875  10.333  18.171  1.00145.94           C
ATOM     62  C4  MAN A3270      23.947  11.382  18.462  1.00143.56           C
ATOM     63  C5  MAN A3270      23.812  12.628  17.591  1.00151.91           C
ATOM     64  C6  MAN A3270      25.034  13.526  17.752  1.00161.53           C
ATOM     65  O2  MAN A3270      23.692   9.476  16.086  1.00134.57           O
ATOM     66  O3  MAN A3270      23.283   9.069  18.651  1.00143.26           O
ATOM     67  O4  MAN A3270      23.844  11.797  19.805  1.00131.47           O
ATOM     68  O5  MAN A3270      23.681  12.274  16.235  1.00145.42           O
ATOM     69  O6  MAN A3270      24.819  14.746  17.078  1.00163.34           O
ATOM     70  C1  MAN A3271      23.389   9.073  20.086  1.00134.25           C
ATOM     71  C2  MAN A3271      24.692   8.399  20.501  1.00128.82           C
ATOM     72  C3  MAN A3271      24.714   6.971  19.971  1.00125.96           C
ATOM     73  C4  MAN A3271      23.438   6.252  20.403  1.00128.89           C
ATOM     74  C5  MAN A3271      22.203   7.090  20.071  1.00131.85           C
ATOM     75  C6  MAN A3271      20.918   6.424  20.547  1.00130.34           C
ATOM     76  O2  MAN A3271      24.805   8.383  21.907  1.00119.55           O
ATOM     77  O3  MAN A3271      25.852   6.293  20.462  1.00109.33           O
ATOM     78  O4  MAN A3271      23.347   5.000  19.761  1.00122.88           O
ATOM     79  O5  MAN A3271      22.321   8.365  20.664  1.00134.39           O
ATOM     80  O6  MAN A3271      19.814   7.225  20.185  1.00130.42           O
ATOM     81  C1  MAN A3272      15.616  14.738  23.320  1.00 20.00           C
ATOM     82  C2  MAN A3272      14.525  15.595  23.954  1.00 20.00           C
ATOM     83  C3  MAN A3272      14.336  16.881  23.154  1.00 20.00           C
ATOM     84  C4  MAN A3272      15.673  17.580  22.916  1.00 20.00           C
ATOM     85  C5  MAN A3272      16.737  16.606  22.410  1.00 20.00           C
ATOM     86  C6  MAN A3272      18.111  17.266  22.346  1.00 20.00           C
ATOM     87  O2  MAN A3272      14.861  15.904  25.291  1.00 20.00           O
ATOM     88  O3  MAN A3272      13.466  17.752  23.842  1.00 20.00           O
ATOM     89  O4  MAN A3272      15.488  18.624  21.984  1.00 20.00           O
ATOM     90  O5  MAN A3272      16.816  15.481  23.262  1.00 20.00           O
ATOM     91  O6  MAN A3272      18.957  16.510  21.509  1.00 20.00           O
END
"""

cif_links_str_1 = """
refinement.pdb_interpretation {
  apply_cif_link {
    data_link = "ALPHA1-3"
    residue_selection_1 = "chain A and resname MAN and resid 3268"
    residue_selection_2 = "chain A and resname MAN and resid 3269"
  }
  apply_cif_link {
    data_link = "ALPHA1-6"
    residue_selection_1 = "chain A and resname MAN and resid 3268"
    residue_selection_2 = "chain A and resname MAN and resid 3270"
  }
  apply_cif_link {
    data_link = "ALPHA1-3"
    residue_selection_1 = "chain A and resname MAN and resid 3270"
    residue_selection_2 = "chain A and resname MAN and resid 3271"
  }
  apply_cif_link {
    data_link = "ALPHA1-2"
    residue_selection_1 = "chain A and resname MAN and resid 3272"
    residue_selection_2 = "chain A and resname MAN and resid 3269"
  }
}
"""

cif_links_str_2 = """
refinement.pdb_interpretation {
  apply_cif_link {
    data_link = ALPHA1-2
    residue_selection_1 = chain B and resname GAL and resid 2
    residue_selection_2 = chain B and resname FUC and resid 1
  }
  apply_cif_link {
    data_link = BETA1-3
    residue_selection_1 = chain B and resname NAG and resid 3
    residue_selection_2 = chain B and resname GAL and resid 2
  }
}
"""

cif_links_str_3 = """
refinement.pdb_interpretation {
    apply_cif_link {
      data_link = "NAG-ASN"
      residue_selection_1 = "chain A and resname NAG and resid 3266"
      residue_selection_2 = "chain A and resname ASN and resid 266"
    }
    apply_cif_link {
      data_link = "BETA1-4"
      residue_selection_1 = "chain A and resname NAG and resid 3266"
      residue_selection_2 = "chain A and resname NAG and resid 3267"
    }
    apply_cif_link {
      data_link = "BETA1-4"
      residue_selection_1 = "chain A and resname NAG and resid 3267"
      residue_selection_2 = "chain A and resname MAN and resid 3268"
    }
    apply_cif_link {
      data_link = "ALPHA1-3"
      residue_selection_1 = "chain A and resname MAN and resid 3268"
      residue_selection_2 = "chain A and resname MAN and resid 3269"
    }
    apply_cif_link {
      data_link = "ALPHA1-6"
      residue_selection_1 = "chain A and resname MAN and resid 3268"
      residue_selection_2 = "chain A and resname MAN and resid 3270"
    }
    apply_cif_link {
      data_link = "ALPHA1-3"
      residue_selection_1 = "chain A and resname MAN and resid 3270"
      residue_selection_2 = "chain A and resname MAN and resid 3271"
    }
    apply_cif_link {
      data_link = "ALPHA1-2"
      residue_selection_1 = "chain A and resname MAN and resid 3269"
      residue_selection_2 = "chain A and resname MAN and resid 3272"
    }
}
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Test sugar linking
  """
  cntr=0
  for i, it in enumerate([[pdb_str_1, cif_links_str_1],
                          [pdb_str_2, cif_links_str_2],
                          [pdb_str_3, cif_links_str_3]]):
    #
    pdb_str, cif_link = it
    pdb_in = "%s_%d.pdb"%(prefix, i)
    pdb_inp = iotbx.pdb.input(source_info=None, lines=pdb_str)
    pdb_inp.write_pdb_file(file_name = pdb_in)
    #
    cif_in = "%s_%d.eff"%(prefix, i)
    with open(cif_in, "w") as fo:
      fo.write(cif_link)
    #
    args = [
      pdb_in,
      "high_res=4",
      "type=real",
      "label=F-obs",
      "r_free=0.1"]
    r0 = run_fmodel(args = args, prefix = prefix)
    #
    args = [
      r0.mtz,
      pdb_in,
      "main.number_of_mac=0",
      "strategy=none"]
    r = run_phenix_refine(args = args, prefix="%s_%d"%(prefix, i))
    #
    output_geo = "%s_%d_001.geo"%(prefix, i)
    if(i==1):
      cntr+=1
      lines = "\n".join(open(output_geo,"r").read().splitlines())
      assert contains_lines(lines=lines, expected = """\
bond pdb=" C1  FUC B   1 "
     pdb=" O2  GAL B   2 "
  ideal  model  delta    sigma   weight residual
  1.439  1.424  0.015 2.00e-02 2.50e+03 5.83e-01
""")
      assert contains_lines(lines=lines, expected = """\
bond pdb=" C1  GAL B   2 "
     pdb=" O3  NAG B   3 "
  ideal  model  delta    sigma   weight residual
  1.439  1.425  0.014 2.00e-02 2.50e+03 4.84e-01
""")
  if(i==2):
    cntr+=1
    lines = "\n".join(open(output_geo,"r").read().splitlines())
    def check(expected):
      assert contains_lines(lines=lines, expected=expected)
    check("""\
bond pdb=" ND2 ASN A 266 "
     pdb=" C1  NAG A3266 "
  ideal  model  delta    sigma   weight residual
  1.439  1.439  0.000 2.00e-02 2.50e+03 1.17e-04
""")
    check("""\
bond pdb=" O4  NAG A3266 "
     pdb=" C1  NAG A3267 "
  ideal  model  delta    sigma   weight residual
  1.439  1.437  0.002 2.00e-02 2.50e+03 9.60e-03
""")
    check("""\
bond pdb=" O4  NAG A3267 "
     pdb=" C1  MAN A3268 "
  ideal  model  delta    sigma   weight residual
  1.439  1.439  0.000 2.00e-02 2.50e+03 2.42e-04
""")
    check("""\
bond pdb=" O3  MAN A3268 "
     pdb=" C1  MAN A3269 "
  ideal  model  delta    sigma   weight residual
  1.439  1.443 -0.004 2.00e-02 2.50e+03 3.50e-02
""")
    check("""\
bond pdb=" O6  MAN A3268 "
     pdb=" C1  MAN A3270 "
  ideal  model  delta    sigma   weight residual
  1.439  1.442 -0.003 2.00e-02 2.50e+03 2.58e-02
""")
    check("""\
bond pdb=" O3  MAN A3270 "
     pdb=" C1  MAN A3271 "
  ideal  model  delta    sigma   weight residual
  1.439  1.439  0.000 2.00e-02 2.50e+03 1.80e-05
""")
    check("""\
bond pdb=" O2  MAN A3269 "
     pdb=" C1  MAN A3272 "
  ideal  model  delta    sigma   weight residual
  1.439  1.465 -0.026 2.00e-02 2.50e+03 1.74e+00
""")
#
  assert cntr==2, cntr

if (__name__ == "__main__") :
  run()
