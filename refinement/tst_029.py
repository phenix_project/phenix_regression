from __future__ import print_function
from libtbx.test_utils import approx_equal
import libtbx.load_env
import libtbx.path
import os
from cctbx.array_family import flex
import random
import iotbx.pdb
from phenix_regression.refinement import run_phenix_refine

if (1): # fixed random seed to avoid rare failures
  random.seed(0)
  flex.set_random_seed(0)

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Mixed ADP refinement (gruop mix iso/aniso).
  """
  pdb_in = "%s.pdb"%prefix
  mtz_in = "%s.mtz"%prefix
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/phe_group_adp_refinement.pdb",
    test=os.path.isfile)
  pdb_inp = iotbx.pdb.input(file_name = pdb)
  h = pdb_inp.construct_hierarchy(sort_atoms=False)
  x = h.extract_xray_structure()
  x.tidy_us()
  h.adopt_xray_structure(xray_structure = x)
  h.write_pdb_file(file_name = pdb_in)
  f_obs = abs(x.structure_factors(
    d_min          = 1.5,
    anomalous_flag = False,
    cos_sin_table  = True,
    algorithm      = "direct").f_calc())
  flags = f_obs.generate_r_free_flags(fraction = 0.1, max_free = 99999999)
  mtz_dataset = f_obs.as_mtz_dataset(column_root_label = "FOBS")
  mtz_dataset.add_miller_array(miller_array      = flags,
                               column_root_label = "TEST")
  mtz_object = mtz_dataset.mtz_object()
  mtz_object.write(file_name = mtz_in)
  #
  args = [pdb_in,mtz_in,
    "refinement.structure_factors_and_gradients_accuracy.algorithm=direct",
    "main.target=ls",
    "modify_start_model.modify.adp.shift_b_iso=5.0",
    "main.number_of_macro_cycles=2",
    "strategy=group_adp",
    "main.bulk_solvent_and_scale=false",
    "main.fake_f_obs=true",
    "target_weights.wu=0",
    "refinement.structure_factors_and_gradients_accuracy.cos_sin_table=false",
    "modify_start_model.output.file_name=modified.pdb",
    "refinement.fake_f_obs.structure_factors_accuracy.cos_sin_table=false",
    "refinement.fake_f_obs.structure_factors_accuracy.algorithm=direct",
    "xray_data.r_free_flags.ignore_r_free_flags=true",
    "group_b_iso.use_restraints=false"]
  r = run_phenix_refine(args = args, prefix = prefix)
  assert r.bond_start  == r.bond_final
  assert r.angle_start == r.angle_final
  assert r.r_work_start > 0.15
  assert r.r_free_start > 0.15
  assert approx_equal(r.r_work_final,0.025,1.e-2) # not 0 due to tidy_us() in group.py
  assert approx_equal(r.r_free_final,0.025,1.e-2) # not 0 due to tidy_us() in group.py

if (__name__ == "__main__"):
  run()
  print("OK")
