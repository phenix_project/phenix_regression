from __future__ import division
from __future__ import print_function
import iotbx.mtz
import iotbx.pdb
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel
import os

def run(prefix = os.path.basename(__file__).replace(".py","")):
  pdb_in_str = """\
ATOM     48  N   ARG A  10      35.567  58.857  73.703  1.00 35.93           N
ATOM     49  CA  ARG A  10      35.503  59.146  72.282  1.00 30.67           C
ATOM     50  C   ARG A  10      34.211  58.602  71.689  1.00 32.52           C
ATOM     51  O   ARG A  10      33.635  59.197  70.782  1.00 30.93           O
ATOM     52  CB  ARG A  10      36.691  58.514  71.564  1.00 32.21           C
ATOM     53  CG  ARG A  10      36.566  58.521  70.061  1.00 30.68           C
ATOM     54  CD  ARG A  10      37.807  58.002  69.417  1.00 31.31           C
ATOM     55  NE  ARG A  10      37.661  57.949  67.965  1.00 33.31           N
ATOM     56  CZ  ARG A  10      38.568  57.417  67.150  1.00 33.37           C
ATOM     57  NH1 ARG A  10      39.691  56.916  67.655  1.00 41.04           N
ATOM     58  NH2 ARG A  10      38.370  57.392  65.842  1.00 31.93           N
ATOM     71  N   TYR A  11      33.775  57.440  72.175  1.00 26.35           N
ANISOU   71  N   TYR A  11     3897   2646   3468    -38   -328   -303       N
ATOM     72  CA  TYR A  11      32.567  56.817  71.637  1.00 26.90           C
ANISOU   72  CA  TYR A  11     3926   2750   3545    -23   -286   -243       C
ATOM     73  C   TYR A  11      31.402  56.983  72.604  1.00 28.09           C
ANISOU   73  C   TYR A  11     4116   2904   3653     61   -258   -205       C
ATOM     74  O   TYR A  11      31.449  56.504  73.736  1.00 29.78           O
ANISOU   74  O   TYR A  11     4347   3124   3842    107   -265   -210       O
ATOM     75  CB  TYR A  11      32.825  55.327  71.337  1.00 23.43           C
ANISOU   75  CB  TYR A  11     3416   2351   3137    -52   -287   -236       C
ATOM     76  CG  TYR A  11      34.001  55.108  70.404  1.00 22.68           C
ANISOU   76  CG  TYR A  11     3279   2254   3083   -133   -314   -275       C
ATOM     77  CD1 TYR A  11      33.880  55.344  69.050  1.00 23.51           C
ANISOU   77  CD1 TYR A  11     3350   2362   3219   -187   -302   -262       C
ATOM     78  CD2 TYR A  11      35.242  54.740  70.882  1.00 22.02           C
ANISOU   78  CD2 TYR A  11     3194   2166   3007   -155   -353   -325       C
ATOM     79  CE1 TYR A  11      34.940  55.182  68.196  1.00 25.90           C
ANISOU   79  CE1 TYR A  11     3617   2665   3560   -260   -326   -298       C
ATOM     80  CE2 TYR A  11      36.321  54.584  70.052  1.00 26.06           C
ANISOU   80  CE2 TYR A  11     3670   2677   3556   -227   -378   -360       C
ATOM     81  CZ  TYR A  11      36.166  54.820  68.694  1.00 26.09           C
ANISOU   81  CZ  TYR A  11     3639   2684   3589   -280   -364   -347       C
ATOM     82  OH  TYR A  11      37.237  54.639  67.860  1.00 28.84           O
ANISOU   82  OH  TYR A  11     3949   3034   3974   -351   -387   -382       O
"""
  pdb_in = "%s.pdb"%prefix
  open(pdb_in, "w").write(pdb_in_str)
  args = [
    pdb_in,
    "high_resolution=1.2",
    "generate_fake_p1_symmetry=True",
    "type=real",
    "r_free_flags_fraction=0.1",
    "random_seed=12345",
    "label=F"]
  r = run_fmodel(args = args, prefix = prefix)
  #
  mtz_in = iotbx.mtz.object(r.mtz)
  f_obs = mtz_in.as_miller_arrays()[0]
  pdb_inp = iotbx.pdb.input(source_info=None, lines=pdb_in_str)
  hierarchy = pdb_inp.construct_hierarchy()
  xrs = pdb_inp.xray_structure_simple(crystal_symmetry=f_obs)
  xrs.convert_to_isotropic()
  hierarchy.adopt_xray_structure(xrs)
  f = open(pdb_in, "w")
  f.write(hierarchy.as_pdb_string(crystal_symmetry=xrs))
  f.close()
  for line in open(pdb_in).readlines():
    assert not line.startswith("ANISOU")
  args = [
    pdb_in,
    r.mtz,
    "adp.individual.isotropic='chain A and not (bfactor < 30)'",
    "adp.individual.anisotropic='bfactor < 30'",
    "refine.strategy=individual_adp",
    "main.number_of_macro_cycles=1"]
  r = run_phenix_refine(args = args, prefix = prefix)
  pdb_in = iotbx.pdb.input(r.pdb)
  hierarchy = pdb_in.construct_hierarchy()
  sel = hierarchy.atom_selection_cache().selection
  sel1 = sel("resname TYR")
  sel2 = sel("anisou")
  assert sel1.all_eq(sel2)

if (__name__ == "__main__") :
  run()
  print("OK")
