from __future__ import print_function
from libtbx import easy_run
import libtbx.load_env
from libtbx.test_utils import approx_equal
import os, time

def run(prefix="tst_remark3_min_f_over_sigma"):
  """
  Make sure
  REMARK   3   MIN(FOBS/SIGMA_FOBS)              :
  gets correct value in case Fobs are internally modified.
  """
  path = "phenix_regression/refinement/remark3_min_f_over_sigma/%s"
  pdb = libtbx.env.find_in_repositories(
    relative_path=path%"m.pdb", test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path=path%"d.mtz", test=os.path.isfile)
  cif = libtbx.env.find_in_repositories(
    relative_path=path%"l.cif", test=os.path.isfile)
  cmd = " ".join([
    "phenix.refine",
    "%s %s %s"%(pdb,hkl,cif),
    "strategy=none",
    "main.number_of_mac=1",
    "output.prefix=%s"%prefix,
    "main.update_f_part1=False",
    "write_map_coefficients=False",
    "--quiet",
    "> %s.zlog"%prefix])
  assert not easy_run.call(cmd)
  value = None
  for line in open("%s_001.pdb"%prefix,"r").read().splitlines():
    if(line.startswith("REMARK   3   MIN(FOBS/SIGMA_FOBS)              :")):
      value = float(line.split()[4])
  assert value is not None
  assert approx_equal(value, 1.33)

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
