from __future__ import print_function
import time, os
from libtbx.test_utils import approx_equal
from libtbx import easy_run
import iotbx.pdb
import mmtbx.utils
from phenix_regression.refinement import run_phenix_refine

pdb_str = """
CRYST1    9.090    8.927   10.980  90.00  90.00  90.00 P 1

ATOM     11  N  ALYS A  42       2.000   2.088   6.561  0.30  4.64           N
ATOM     12  CA ALYS A  42       3.129   2.882   6.951  0.30  4.77           C
ATOM     13  C  ALYS A  42       2.638   4.077   7.786  0.30  4.29           C
ATOM     14  O  ALYS A  42       3.231   4.433   8.806  0.30  5.05           O
ATOM     15  CB ALYS A  42       3.915   3.361   5.725  0.30  4.85           C
ATOM     16  CG ALYS A  42       4.442   2.275   4.824  0.30  4.99           C
ATOM     17  CD ALYS A  42       5.356   2.862   3.756  0.30  5.63           C
ATOM     18  CE ALYS A  42       4.678   3.931   2.867  0.30  5.17           C
ATOM     19  NZ ALYS A  42       3.581   3.324   2.116  0.30  6.22           N

ATOM     33  N  BLYS A  42       2.172   2.000   6.598  0.20  5.86           N
ATOM     34  CA BLYS A  42       3.266   2.910   7.021  0.20  6.17           C
ATOM     35  C  BLYS A  42       2.752   4.040   7.923  0.20  5.20           C
ATOM     36  O  BLYS A  42       3.324   4.348   8.980  0.20  5.64           O
ATOM     37  CB BLYS A  42       3.961   3.576   5.797  0.20  6.51           C
ATOM     38  CG BLYS A  42       5.210   4.466   6.080  0.20  7.40           C
ATOM     39  CD BLYS A  42       5.782   5.004   4.765  0.20  7.95           C
ATOM     40  CE BLYS A  42       7.090   5.749   4.925  0.20  8.81           C
ATOM     41  NZ BLYS A  42       6.950   6.927   5.796  0.20  9.12           N

ATOM     55  N  CLYS A  42       2.089   2.057   6.586  0.10  5.63           N
ATOM     56  CA CLYS A  42       3.197   2.922   6.981  0.10  6.23           C
ATOM     57  C  CLYS A  42       2.691   4.086   7.832  0.10  6.20           C
ATOM     58  O  CLYS A  42       3.260   4.393   8.883  0.10  6.26           O
ATOM     59  CB CLYS A  42       3.946   3.432   5.742  0.10  6.94           C
ATOM     60  CG CLYS A  42       5.124   4.387   6.050  0.10  7.61           C
ATOM     61  CD CLYS A  42       5.786   4.997   4.824  0.10  7.84           C
ATOM     62  CE CLYS A  42       5.815   6.445   5.091  0.10  8.51           C
ATOM     63  NZ CLYS A  42       6.930   6.925   5.747  0.10  8.46           N

ATOM     77  N  DLYS A  42       2.092   2.058   6.583  0.40  4.72           N
ATOM     78  CA DLYS A  42       3.224   2.890   6.979  0.40  4.67           C
ATOM     79  C  DLYS A  42       2.723   4.064   7.832  0.40  4.20           C
ATOM     80  O  DLYS A  42       3.312   4.408   8.861  0.40  4.82           O
ATOM     81  CB DLYS A  42       4.006   3.384   5.750  0.40  4.79           C
ATOM     82  CG DLYS A  42       4.543   2.274   4.851  0.40  5.08           C
ATOM     83  CD DLYS A  42       5.168   2.863   3.593  0.40  5.85           C
ATOM     84  CE DLYS A  42       4.357   2.545   2.353  0.40  5.32           C
ATOM     85  NZ DLYS A  42       3.428   3.604   2.000  0.40  6.21           N

TER
END
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Occupancies.
  Make sure can refine back to answer within 1.e-6.
  """
  pdb_file_answer = "%s_answer.pdb"%prefix
  pdb_inp = iotbx.pdb.input(source_info=None, lines=pdb_str)
  ph = pdb_inp.construct_hierarchy()
  xrs_answer = pdb_inp.xray_structure_simple()
  xrs_answer.scattering_type_registry(table = "wk1995")
  ph.write_pdb_file(file_name = pdb_file_answer)
  #
  pdb_file_poor = "%s_poor.pdb"%prefix
  xrs_poor = xrs_answer.deep_copy_scatterers()
  xrs_poor.shake_occupancies()
  ph.adopt_xray_structure(xrs_poor)
  ph.write_pdb_file(file_name = pdb_file_poor)
  #
  for d_min in [1,2,3]:
    f_obs = abs(xrs_answer.structure_factors(d_min=d_min,
      algorithm="direct").f_calc())
    mtz_dataset = f_obs.as_mtz_dataset(column_root_label="F-obs")
    mtz_dataset.add_miller_array(miller_array=f_obs.generate_r_free_flags(),
      column_root_label="R-free-flags")
    mtz_object = mtz_dataset.mtz_object()
    data_file = "%s_data.mtz"%prefix
    mtz_object.write(file_name = data_file)
    ref_prefix = "%s_d_min_%s"%(prefix, str(d_min))
    args = [
      pdb_file_poor,
      data_file,
      'main.number_of_mac=5',
      'refine.strategy=occupancies',
      'main.bulk_solv=false',
      "structure_factors_and_gradients_accuracy.algorithm=direct",
      "main.scattering_table=wk1995",
      'xray_data.r_free_flags.ignore_r_free_flags=true',
      "xray_data.outliers_rejection=false",
      "main.target=ls",
      "ls_target_names.target_name=ls_wunit_kunit"
      ]
    r = run_phenix_refine(args = args, prefix=prefix)
    r.check_start_r(r_work=0, r_free=0, eps=1.e-5)
    r.check_final_r(r_work=0, r_free=0, eps=1.e-5)
    xrs = iotbx.pdb.input(file_name=r.pdb).xray_structure_simple()
    xrs.scattering_type_registry(table = "wk1995")
    mmtbx.utils.assert_xray_structures_equal(x1 = xrs, x2 = xrs_answer)

if(__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK")
  print("Time: %8.3f"%(time.time()-t0))
