from __future__ import print_function
import libtbx.load_env
import time, os
from phenix_regression.refinement import run_phenix_refine
from libtbx.test_utils import assert_lines_in_file
import iotbx.pdb

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Find four (4) CA ions using original files for 1rfj, where CA are commented 
  out with remark cards.
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/data/%s.pdb"%prefix,
    test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/data/%s.mtz"%prefix,
    test=os.path.isfile)
  #
  args = [pdb, hkl, "place_ions=CA","main.number_of_mac=1"]
  r = run_phenix_refine(args = args, prefix = prefix)
  #
  elements = list(iotbx.pdb.input(file_name="%s_001.pdb"%prefix
    ).construct_hierarchy().atoms().extract_element())
  assert elements.count('Ca') == 4

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK Time: %8.3f"%(time.time()-t0))
