from __future__ import division
from __future__ import print_function
from phenix_regression.refinement import run_fmodel
import os
from phenix_regression.refinement import run_phenix_refine
from libtbx.test_utils import assert_lines_in_file

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Exercise stop for unknowns.
  This is just TYR from eLBOW.
  """
  pdb_raw = """\
HETATM    1  N   UNK A   1      -1.048  -0.331   2.695  1.00 20.00      A    N
HETATM    2  CA  UNK A   1       0.084  -0.660   1.899  1.00 20.00      A    C
HETATM    3  C   UNK A   1       1.181  -1.205   2.784  1.00 20.00      A    C
HETATM    4  O   UNK A   1       1.492  -2.423   2.724  1.00 20.00      A    O
HETATM    5  CB  UNK A   1       0.578   0.578   1.188  1.00 20.00      A    C
HETATM    6  CG  UNK A   1       0.204   0.503  -0.276  1.00 20.00      A    C
HETATM    7  CD1 UNK A   1      -0.884   1.219  -0.750  1.00 20.00      A    C
HETATM    8  CD2 UNK A   1       0.980  -0.239  -1.152  1.00 20.00      A    C
HETATM    9  CE1 UNK A   1      -1.196   1.192  -2.101  1.00 20.00      A    C
HETATM   10  CE2 UNK A   1       0.668  -0.266  -2.503  1.00 20.00      A    C
HETATM   11  CZ  UNK A   1      -0.420   0.450  -2.977  1.00 20.00      A    C
HETATM   12  OH  UNK A   1      -0.712   0.456  -4.353  1.00 20.00      A    O
HETATM   13  OXT UNK A   1       1.774  -0.442   3.592  1.00 20.00      A    O-1
HETATM   14  H   UNK A   1      -1.693  -0.973   2.590  1.00 20.00      A    H
HETATM   15  H2  UNK A   1      -0.800  -0.287   3.575  1.00 20.00      A    H
HETATM   16  HA  UNK A   1      -0.167  -1.336   1.239  1.00 20.00      A    H
HETATM   17  HB2 UNK A   1       1.551   0.637   1.273  1.00 20.00      A    H
HETATM   18  HB3 UNK A   1       0.168   1.370   1.589  1.00 20.00      A    H
HETATM   19  HD1 UNK A   1      -1.427   1.747  -0.134  1.00 20.00      A    H
HETATM   20  HD2 UNK A   1       1.747  -0.743  -0.819  1.00 20.00      A    H
HETATM   21  HE1 UNK A   1      -1.961   1.698  -2.435  1.00 20.00      A    H
HETATM   22  HE2 UNK A   1       1.215  -0.788  -3.120  1.00 20.00      A    H
HETATM   23  HH  UNK A   1      -1.331  -0.159  -4.527  1.00 20.00      A    H
TER
END
"""
  pdb_in = "%s.pdb"%prefix
  open(pdb_in, "w").write(pdb_raw)
  args = [
    pdb_in,
    "high_resolution=3.0",
    "type=real",
    "label=F",
    "r_free_flags_fraction=0.1",
    "generate_fake_p1_symmetry=True"]
  r = run_fmodel(args = args, prefix = prefix)
  #
  args = [pdb_in, r.mtz, "output.prefix=%s"%prefix]
  r = run_phenix_refine(args = args, prefix = prefix, sorry_expected=True)
  assert_lines_in_file(
    file_name=r.log,
    lines="""
Sorry: Fatal problems interpreting model file:"""
    )

if (__name__ == "__main__") :
  run()
  print("OK")
