from __future__ import division
from __future__ import print_function
import libtbx.load_env
import os, time
from phenix_regression.refinement import run_phenix_refine
import iotbx.pdb
from libtbx.test_utils import approx_equal
from phenix_regression.refinement import run_fmodel
from scitbx.array_family import flex
import mmtbx.model

phil_str = """
data_manager {
  model {
    file = %s
  }
  miller_array {
    file = %s
  }
  fmodel {
    xray_data {
      twin_law = "h,-h-k,-l"
    }
  }
}
refinement {
  crystal_symmetry {
    unit_cell = 152.492 152.492 125.385 90 90 120
    space_group = "P 64"
  }
  refine {
    strategy = individual_sites individual_sites_real_space *rigid_body \
               individual_adp group_adp tls occupancies group_anomalous den
    sites {
      rigid_body = (chain 'A' and resid 1 through 5)
      rigid_body = (chain 'B' and resid 1 through 5)
      rigid_body = (chain 'C' and resid 1 through 5)
    }
  }
  main.bulk_solvent_and_scale=false 
  rigid_body.bulk_solvent_and_scale=false
  output.write_geo_file=false
}
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Test for a fix of famous long-standing crash:
  
  File "/Users/pafonine/Desktop/all/phenix/modules/cctbx_project/mmtbx/twinning/twin_f_model.py", line 375, in __init__
    assert self.f_obs_.indices().all_eq( self.r_free_flags_.indices() )
AssertionError
  """    
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/data/%s.pdb"%prefix,
    test=os.path.isfile)
  mtz_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/data/%s.mtz"%prefix,
    test=os.path.isfile)
  with open("%s.eff"%prefix,"w") as fo:
    fo.write(phil_str%(pdb_file, mtz_file))
  args = ["%s.eff"%prefix]
  cmd = "phenix.refine "+" ".join(args)
  r = run_phenix_refine(args = args, prefix = prefix, geo_expected=False)  
  #
  target="twin fraction: 0.10  twin operator: h,-h-k,-l"
  found=0
  with open("%s.log"%prefix,"r") as fo:
    for l in fo.readlines():
      if target in l: found+=1
  assert found > 0

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
