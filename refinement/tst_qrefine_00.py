# Temporary file for running qrefine/tests/unit/tst_00.py

from __future__ import absolute_import, division, print_function

import os
import tempfile

def run_qrefine_tst_00():
  try:
    from qrefine.tests.unit import run_tests, tst_00
  except ImportError:
   print('qrefine module is not available... skipping test')
   return 0

  with tempfile.TemporaryDirectory() as tmp_dir:
    os.chdir(tmp_dir)
    prefix = os.path.basename(tst_00.__file__).replace('.py','')
    run_tests.runner(function=tst_00.run, prefix=prefix, disable=False)

	
if __name__ == '__main__':
  run_qrefine_tst_00()
