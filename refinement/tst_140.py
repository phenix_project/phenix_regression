from __future__ import division, print_function
from libtbx import easy_run
from phenix_regression.refinement import run_fmodel
import os

pdb_str = """
CRYST1   26.851   32.174   25.146  90.00  90.00  90.00 P 1
ATOM      1  N   GLY B   1      10.491   7.299  10.131  1.00  7.85           N
ATOM      2  CA  GLY B   1      11.259   8.382   9.484  1.00  6.79           C
ATOM      3  C   GLY B   1      11.687   9.407  10.508  1.00  5.59           C
ATOM      4  O   GLY B   1      11.421   9.253  11.703  1.00  6.04           O
ATOM      5  N   CYS B   2      12.252  10.501  10.025  1.00  5.95           N
ATOM      6  CA  CYS B   2      12.751  11.577  10.876  1.00  5.17           C
ATOM      7  C   CYS B   2      11.799  12.091  11.950  1.00  4.74           C
ATOM      8  O   CYS B   2      12.183  12.260  13.118  1.00  4.51           O
ATOM      9  CB  CYS B   2      13.149  12.762   9.999  1.00  5.99           C
TER
END
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Make sure all expected files are produced
  """
  pdb_file = "%s.pdb"%prefix
  hkl_file = "%s.mtz"%prefix
  with open(pdb_file, "w") as fo:
    fo.write(pdb_str)
  #
  args = [
    pdb_file,
    "high_resolution=3.9",
    "scale=0.0000001",
    "k_sol=0.35 b_sol=60",
    "type=real r_free=0.3",
    "output.file_name=%s"%hkl_file]
  r = run_fmodel(args = args, prefix = prefix)
  #
  args = [
    "phenix.refine",
    "%s.{pdb,mtz}"%prefix,
    "strategy=none",
    "main.number_of_mac=1",
    'output.serial_format="%d"',
    'output.write_final_geo_file=True',
    'output.write_reflection_cif_file=True',
    'output.export_final_f_model=True',
    'output.write_maps=True',
    'output.write_map_coefficients=True',
    'output.write_map_coefficients_only=True',
    'output.pickle_fmodel=True',
  ]
  assert not easy_run.call(" ".join(args))
  #
  for fname in [
      '%s_refine_1.reflections.cif'%prefix,
      '%s_refine_1_2mFo-DFc.ccp4'%prefix,
      '%s_refine_1_2mFo-DFc_filled.ccp4'%prefix,
      '%s_refine_1_f_model.mtz'%prefix,
      '%s_refine_1_final.geo'%prefix,
      '%s_refine_1_fmodel.pickle'%prefix,
      '%s_refine_1_mFo-DFc.ccp4'%prefix,
      '%s_refine_2.def'%prefix,
      ]:
    assert os.path.isfile(fname), fname

if (__name__ == "__main__") :
  run()
  print("OK")
