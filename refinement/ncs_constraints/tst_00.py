from __future__ import print_function
from libtbx import easy_run
import time, os
import iotbx.pdb
from scitbx.array_family import flex
import libtbx.load_env

phenix_refine_common_params = [
  "cdl=False",
  "xray_data.r_free_flags.ignore_r_free_flags=true",
  "strategy=none",
  "main.bulk_sol=false",
  "ncs.type=constraints",
  "ncs_search.enabled=true",
  "main.number_of_mac=10",
  "main.target=ls",
  "nqh_flips=false",
  "--overwrite --quiet"]

def check_results(answer, refined):
  xrs_answer  = iotbx.pdb.input(file_name=answer).xray_structure_simple()
  xrs_refined = iotbx.pdb.input(file_name=refined).xray_structure_simple()
  d = xrs_answer.distances(xrs_refined)
  assert flex.max(d)<0.01, flex.max(d)
  fo = open(refined,"r")
  rwork = None
  for l in fo.readlines():
    if l.count("REMARK   3   R VALUE            (WORKING SET)"):
      rwork = float(l.split()[-1])
  fo.close()
  assert rwork is not None
  assert rwork < 0.005, rwork

def run(prefix="tst_nc00"):
  """
  Refine NCS operators only.
  """
  # Answer PDB and data
  pdb_answer = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/ncs_constraints/answer_00.pdb",
    test=os.path.isfile)
  cmd = " ".join([
    "phenix.fmodel",
    "%s"%pdb_answer,
    "file_name=%s.mtz"%prefix,
    "high_res=2.5",
    "type=real",
    "r_free=0.1",
    ">%s.zlog"%prefix])
  assert not easy_run.call(cmd)
  # Rotate/Translate then refine (one chain at a time)
  for i_run, ss in enumerate(["chain A and not element MG", "chain B"]):
    pdb_poor = "%s_poor_%s.pdb"%(prefix,str(i_run))
    cmd = " ".join([
      "phenix.pdbtools",
      "%s"%pdb_answer,
      "translate='0.5,1,1.5'",
      "rotate='3,6,9'",
      "modify.selection='%s'"%ss,
      "suffix=none",
      "output.prefix=%s"%pdb_poor.replace(".pdb",""),
      ">%s.zlog"%prefix])
    assert not easy_run.call(cmd)
    pdb_refined = "%s_refined_%s.pdb"%(prefix,str(i_run))
    cmd = " ".join([
      "phenix.refine",
      "%s"%pdb_poor,
      "%s.mtz"%prefix,
      "output.prefix=%s"%pdb_refined]+phenix_refine_common_params)
    assert not easy_run.call(cmd)
    check_results(answer=pdb_answer, refined=pdb_refined+"_001.pdb")
  # Rotate/Translate both chains then refine
  pdb_poor = "%s_poor_%s.pdb"%(prefix,str(2))
  cmd = " ".join([
    "phenix.pdbtools",
    "%s"%pdb_answer,
    "translate='0.5,1,1.5'",
    "modify.selection='chain A and not element MG'",
    "suffix=none",
    "output.prefix=%s"%pdb_poor.replace(".pdb",""),
    ">%s.zlog"%prefix])
  assert not easy_run.call(cmd)
  cmd = " ".join([
    "phenix.pdbtools",
    "%s"%pdb_poor,
    "rotate='3,6,9'",
    "modify.selection='chain B'",
    "suffix=none",
    "output.prefix=%s"%pdb_poor.replace(".pdb",""),
    ">%s.zlog"%prefix])
  assert not easy_run.call(cmd)
  pdb_refined = "%s_refined_%s"%(prefix,str(2))
  cmd = " ".join([
    "phenix.refine",
    "%s"%pdb_poor,
    "%s.mtz"%prefix,
    "output.prefix=%s"%pdb_refined]+phenix_refine_common_params)
  assert not easy_run.call(cmd)
  check_results(answer=pdb_answer, refined=pdb_refined+"_001.pdb")

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
