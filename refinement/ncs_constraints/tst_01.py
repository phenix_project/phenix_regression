from __future__ import print_function
from libtbx import easy_run
import time, os
import iotbx.pdb
from scitbx.array_family import flex
import libtbx.load_env
from libtbx.test_utils import assert_lines_in_file

phenix_refine_common_params = [
  "cdl=False",
  "xray_data.r_free_flags.ignore_r_free_flags=true",
  "ncs.type=constraints",
  "ncs_search.enabled=true",
  "strategy=individual_sites",
  "main.bulk_sol=false",
  "main.number_of_mac=20",
  "main.target=ls",
  "nqh_flips=false",
  "--overwrite",
  "--quiet"]

def get_xrs(f):
  pi = iotbx.pdb.input(file_name=f)
  ph = pi.construct_hierarchy()
  ph.flip_symmetric_amino_acids()
  return ph.extract_xray_structure(crystal_symmetry = pi.crystal_symmetry())

def check_results(answer, refined):
  xrs_answer  = get_xrs(answer)
  xrs_refined = get_xrs(refined)
  d = xrs_answer.distances(xrs_refined)
  assert flex.max(d)<0.011, flex.max(d)
  fo = open(refined,"r")
  rwork = None
  for l in fo.readlines():
    if l.count("REMARK   3   R VALUE            (WORKING SET)"):
      rwork = float(l.split()[-1])
  fo.close()
  assert rwork is not None
  assert rwork < 0.005, rwork

def run(prefix="tst_nc01"):
  """
  Refine NCS operators, individual NCS constrained xyz, plus individual xyz.
  Mg ion is removed automatically now
  """
  # Answer PDB and data
  pdb_answer = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/ncs_constraints/answer_00.pdb",
    test=os.path.isfile)
  pdb_poor = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/ncs_constraints/poor_03.pdb",
    test=os.path.isfile)
  cmd = " ".join([
    "phenix.fmodel",
    "%s"%pdb_answer,
    "file_name=%s.mtz"%prefix,
    "high_res=2.5",
    "type=real",
    "r_free=0.1",
    ">%s.zlog"%prefix])
  assert not easy_run.call(cmd)
  # Go refine
  pdb_refined = "%s_refined_%s"%(prefix,str(0))
  cmd = " ".join([
    "phenix.refine",
    "%s"%pdb_poor,
    "%s.mtz"%prefix,
    "output.prefix=%s"%pdb_refined]+phenix_refine_common_params)
  print(cmd)
  assert not easy_run.call(cmd)
  check_results(answer=pdb_answer, refined=pdb_refined+"_001.pdb")
  # Additionally, check .def file.
  assert_lines_in_file(file_name="%s_002.def" % pdb_refined, lines="""
    ncs_group {
      reference = "(chain A and resid 1 through 7)"
      selection = "chain B" """)
  # and log
  assert_lines_in_file(file_name="%s_001.log" % pdb_refined, lines="""
      refinement.pdb_interpretation.ncs_group {
        reference = (chain 'A' and resid 1 through 7)
        selection = chain 'B'""")

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
