from __future__ import division
from __future__ import print_function
from libtbx import easy_run
import libtbx.load_env
import libtbx.path
import time, os
from phenix_regression.refinement import get_r_factors
import iotbx.pdb
from scitbx.math import superpose
from libtbx.test_utils import approx_equal

input_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/ncs/ncs_solvent.pdb",
    test=os.path.isfile)
hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/ncs/ncs_solvent.mtz",
    test=os.path.isfile)

def get_difference_chainsAB(file_name):
  pdb_h = iotbx.pdb.input(file_name=file_name).construct_hierarchy()
  cache = pdb_h.atom_selection_cache()
  ref_sites = pdb_h.select(cache.selection("chain A and resseq 1:6 and not water")).atoms().extract_xyz()
  other_sites = pdb_h.select(cache.selection("chain B and not water")).atoms().extract_xyz()
  lsq_obj = superpose.least_squares_fit(ref_sites, other_sites)
  bf = lsq_obj.other_sites_best_fit()
  dif = ref_sites - bf
  return dif

def exercise_01(prefix="tst_refinement_ncs_constr_solvent"):
  f = open("%s_ncs.eff"%prefix, "w")
  f.write("""
refinement.pdb_interpretation.ncs_group {
  reference = chain A
  selection = chain B
}""")
  f.close()

  base = " ".join([
    "phenix.refine",
    "%s"%input_file,
    "%s"%hkl,
    "main.number_of_macro_cycles=2",
    "main.bulk_solv=true",
    "ncs_search.enabled=true",
    "ncs.type=constraints",
    "strategy=individual_sites",
    "write_eff_file=False",
    "write_geo_file=False",
    "write_def_file=False",
    "write_map_coefficients=False",
    "output.prefix=%s" % prefix,
    "output.write_model_cif_file=True",
    "%s_ncs.eff" % prefix,
    "--overwrite --quiet"
  ])
  cmd = " ".join([base])
  print(cmd)
  assert not easy_run.call(cmd)
  #
  # check if constraints worked. Superposing one chain onto another and
  # checking difference.
  #
  dif = get_difference_chainsAB(file_name="%s_001.pdb" % prefix)
  for d in dif:
    assert approx_equal(d, (0,0,0), eps=0.005)

if (__name__ == "__main__"):
  t0 = time.time()
  exercise_01()
  print("OK. Time: %8.3f"%(time.time()-t0))
