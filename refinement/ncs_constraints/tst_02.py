from __future__ import print_function
from libtbx import easy_run
import time, os
import iotbx.pdb
import libtbx.load_env
from libtbx.test_utils import approx_equal

phenix_refine_common_params = [
  "xray_data.r_free_flags.ignore_r_free_flags=true",
  "ncs.type=constraints",
  "ncs_search.enabled=true",
  "strategy=individual_adp",
  "main.bulk_sol=false",
  "main.number_of_mac=10",
  "main.target=ls"]

def check_results(answer, refined):
  xrs_answer  = iotbx.pdb.input(file_name=answer).xray_structure_simple()
  xrs_refined = iotbx.pdb.input(file_name=refined).xray_structure_simple()
  u1 =  xrs_answer.extract_u_iso_or_u_equiv()
  u2 = xrs_refined.extract_u_iso_or_u_equiv()
  mmm = (u1-u2).min_max_mean().as_tuple()
  assert approx_equal(mmm, [0,0,0], 1.e-3)
  #
  fo = open(refined,"r")
  rwork = None
  for l in fo.readlines():
    if l.count("REMARK   3   R VALUE            (WORKING SET)"):
      rwork = float(l.split()[-1])
  fo.close()
  assert rwork is not None
  assert rwork < 0.001, rwork

def run(prefix="tst_nc02"):
  """
  Refine ADP.
  """
  # Answer PDB and data
  pdb_answer = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/ncs_constraints/answer_01.pdb",
    test=os.path.isfile)
  cmd = " ".join([
    "phenix.fmodel",
    "%s"%pdb_answer,
    "file_name=%s.mtz"%prefix,
    "high_res=2.5",
    "type=real",
    "r_free=0.1",
    "--overwrite --quiet",
    ">%s.zlog"%prefix])
  assert not easy_run.call(cmd)
  # Poor PDB
  pdb_poor = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/ncs_constraints/poor_01.pdb",
    test=os.path.isfile)
  # Refine
  pdb_refined = "%s_refined.pdb"%prefix
  cmd = " ".join([
    "phenix.refine",
    "%s"%pdb_poor,
    "%s.mtz"%prefix,
    "output.prefix=%s"%pdb_refined]+phenix_refine_common_params)
  print(cmd)
  assert not easy_run.call(cmd)
  check_results(answer=pdb_answer, refined=pdb_refined+"_001.pdb")

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
