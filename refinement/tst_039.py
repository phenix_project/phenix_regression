from __future__ import division
from __future__ import print_function
import libtbx.load_env
from libtbx.test_utils import approx_equal
import os
from phenix_regression.refinement import run_phenix_refine
import iotbx.pdb

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  watpick_to_model_with_and_without_H
  """
  pdb1 = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/lysozyme_noH.pdb", test=os.path.isfile)
  pdb2 = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/lysozyme_allHwithzeroQ.pdb",
    test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/lysozyme.mtz",
    test=os.path.isfile)
  rs = []
  for i_seq, pdb_i in enumerate([pdb1,pdb2]):
    args = [
      hkl,
      pdb_i,
      'main.number_of_macro_cycles=1',
      'strategy=none',
      'ordered_solvent=true',
      'optimize_scattering_contribution=false',
      'ordered_solvent.mode=every_macro_cycle',
      'xray_data.high_res=2.0',
      'xray_data.low_res=5.0',
      'main.bulk_solvent_and_=False']
    r = run_phenix_refine(args = args, prefix = prefix)
    rs.append(r)
  #
  assert approx_equal(rs[0].r_work_final, rs[1].r_work_final)
  nw = []
  for r in rs:
    h = iotbx.pdb.input(file_name=r.pdb).construct_hierarchy()
    nw.append(h.atom_selection_cache().selection("water").count(True))
  assert nw[0]==nw[1]

if (__name__ == "__main__"):
  run()
  print("OK")
