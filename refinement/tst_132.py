from __future__ import absolute_import, division, print_function
import libtbx.load_env
import sys, os, time
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel
from libtbx.test_utils import assert_lines_in_file

pdb_str="""
CRYST1   49.154   42.953   78.640  90.00  90.00  90.00 P 1
ATOM      1  N   ALA A   6      39.402  13.052  62.644  1.00 29.77           N
ATOM      2  CA  ALA A   6      40.354  11.993  62.981  1.00 30.42           C
ATOM      3  C   ALA A   6      40.347  10.967  61.858  1.00 28.49           C
ATOM      4  O   ALA A   6      39.658  11.132  60.845  1.00 27.48           O
ATOM      5  CB  ALA A   6      40.024  11.322  64.314  1.00 32.81           C
ATOM      6  N   ALA A   7      41.178   9.932  62.011  1.00 28.36           N
ATOM      7  CA  ALA A   7      41.231   8.841  61.048  1.00 28.18           C
ATOM      8  C   ALA A   7      40.438   7.604  61.455  1.00 27.19           C
ATOM      9  O   ALA A   7      40.060   6.818  60.579  1.00 25.95           O
ATOM     10  CB  ALA A   7      42.687   8.433  60.801  1.00 27.65           C
ATOM     11  N   ALA A   8      40.146   7.430  62.752  1.00 27.70           N
ATOM     12  CA  ALA A   8      39.450   6.215  63.192  1.00 27.99           C
ATOM     13  C   ALA A   8      38.889   6.350  64.602  1.00 30.53           C
ATOM     14  O   ALA A   8      38.562   5.331  65.218  1.00 30.76           O
ATOM     15  CB  ALA A   8      40.381   5.000  63.116  1.00 27.11           C
ATOM     16  N   HIS A   9      38.742   7.564  65.115  1.00 31.99           N
ATOM     17  CA  HIS A   9      38.144   7.806  66.409  1.00 33.73           C
ATOM     18  C   HIS A   9      36.799   8.427  66.093  1.00 33.90           C
ATOM     19  O   HIS A   9      36.637   9.645  66.178  1.00 35.08           O
ATOM     20  CB  HIS A   9      38.975   8.770  67.261  1.00 34.92           C
ATOM     21  CG  HIS A   9      40.279   8.220  67.744  1.00 36.58           C
ATOM     22  ND1 HIS A   9      41.237   9.016  68.336  1.00 39.22           N
ATOM     23  CD2 HIS A   9      40.770   6.959  67.768  1.00 36.90           C
ATOM     24  CE1 HIS A   9      42.276   8.274  68.671  1.00 40.56           C
ATOM     25  NE2 HIS A   9      42.016   7.022  68.343  1.00 39.07           N
ATOM     26  N   LEU A  10      44.154  13.979  71.496  1.00 41.58           N
ATOM     27  CA  LEU A  10      43.281  13.891  72.667  1.00 44.84           C
ATOM     28  C   LEU A  10      43.809  12.898  73.640  1.00 45.55           C
ATOM     29  O   LEU A  10      43.449  11.753  73.520  1.00 44.80           O
ATOM     30  CB  LEU A  10      41.849  13.496  72.275  1.00 45.59           C
ATOM     31  CG  LEU A  10      40.933  14.567  71.706  1.00 43.56           C
ATOM     32  CD1 LEU A  10      41.456  14.923  70.429  1.00 42.20           C
ATOM     33  CD2 LEU A  10      39.589  13.969  71.426  1.00 42.90           C
TER
ATOM     34  N   ALA B   6       9.716  37.781  13.506  1.00 10.06           N
ATOM     35  CA  ALA B   6       8.929  37.037  12.531  1.00  8.77           C
ATOM     36  C   ALA B   6       9.739  35.883  11.962  1.00  8.98           C
ATOM     37  O   ALA B   6       9.676  35.594  10.767  1.00  9.16           O
ATOM     38  CB  ALA B   6       8.455  37.953  11.418  1.00  9.28           C
ATOM     39  N   ALA B   7      10.434  35.170  12.846  1.00  8.97           N
ATOM     40  CA  ALA B   7      11.260  34.045  12.435  1.00  8.93           C
ATOM     41  C   ALA B   7      10.438  32.789  12.214  1.00  8.85           C
ATOM     42  O   ALA B   7      10.967  31.806  11.684  1.00  9.02           O
ATOM     43  CB  ALA B   7      12.347  33.773  13.476  1.00  9.78           C
ATOM     44  N   ALA B   8       9.171  32.798  12.630  1.00  8.76           N
ATOM     45  CA  ALA B   8       8.319  31.629  12.456  1.00  9.05           C
ATOM     46  C   ALA B   8       8.203  31.236  10.987  1.00  9.63           C
ATOM     47  O   ALA B   8       8.254  30.046  10.654  1.00  8.09           O
ATOM     48  CB  ALA B   8       6.935  31.904  13.044  1.00  8.29           C
ATOM     49  N   HIS B   9       8.025  32.213  10.094  1.00  9.90           N
ATOM     50  CA  HIS B   9       7.971  31.904   8.668  1.00  9.46           C
ATOM     51  C   HIS B   9       9.133  32.531   7.901  1.00  9.36           C
ATOM     52  O   HIS B   9       8.915  33.287   6.947  1.00  9.17           O
ATOM     53  CB  HIS B   9       6.631  32.346   8.068  1.00  9.29           C
ATOM     54  CG  HIS B   9       6.271  31.621   6.807  1.00  9.32           C
ATOM     55  ND1 HIS B   9       5.030  31.722   6.214  1.00  9.22           N
ATOM     56  CD2 HIS B   9       6.986  30.766   6.038  1.00  9.36           C
ATOM     57  CE1 HIS B   9       5.000  30.966   5.130  1.00  9.21           C
ATOM     58  NE2 HIS B   9       6.174  30.376   5.000  1.00  9.20           N
ATOM     59  N   LEU B  10      10.366  32.224   8.295  1.00  9.42           N
ATOM     60  CA  LEU B  10      11.552  32.672   7.576  1.00  9.61           C
ATOM     61  C   LEU B  10      12.339  31.463   7.092  1.00 10.03           C
ATOM     62  O   LEU B  10      12.501  30.481   7.823  1.00  9.96           O
ATOM     63  CB  LEU B  10      12.443  33.567   8.453  1.00  9.65           C
ATOM     64  CG  LEU B  10      13.605  34.287   7.753  1.00 10.15           C
ATOM     65  CD1 LEU B  10      13.815  35.673   8.342  1.00 10.36           C
ATOM     66  CD2 LEU B  10      14.899  33.480   7.834  1.00 10.35           C
TER
END
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Using NCS constraints is requested by NCS groupd are not provided and not
  found automatically. Catch this and stop meaningfully!
  """
  #
  with open("%s.pdb"%prefix,"w") as fo:
    fo.write(pdb_str)
  #
  args = ["%s.pdb"%prefix, "high_res=5 type=real r_free=0.1"]
  r = run_fmodel(args = args, prefix = prefix)
  #
  args = ["%s.pdb"%prefix,
         "ncs_search.enabled=true",
         "main.number_of_mac=1",
         "write_map_coefficients=False",
         r.mtz,
         "ncs.type=constraints"]
  r = run_phenix_refine(args = args, sorry_expected=True, prefix = prefix)
  assert_lines_in_file(
    file_name=r.log,
    lines="No NCS groupd found. Please provide NCS groupd manually.")

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK", "time: %.2f"%(time.time()-t0))
