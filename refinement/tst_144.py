from __future__ import print_function
import time, os
import iotbx.pdb
from scitbx.array_family import flex
import libtbx.load_env
from phenix_regression.refinement import run_phenix_refine

pdb_str = """
CRYST1   78.239   78.239   37.281  90.00  90.00  90.00 P 1
SCALE1      0.012781 -0.000000 -0.000000        0.00000
SCALE2      0.000000  0.012781  0.000000        0.00000
SCALE3      0.000000  0.000000  0.026823        0.00000
ATOM      1  N   MET A   0      -8.750  11.227  21.276  1.00 56.40           N
ATOM      2  CA  MET A   0      -8.435  11.761  19.919  1.00 56.44           C
ATOM      3  C   MET A   0      -6.957  11.581  19.574  1.00 54.59           C
ATOM      4  O   MET A   0      -6.566  11.720  18.414  1.00 55.86           O
ATOM      5  CB  MET A   0      -8.805  13.249  19.838  1.00 59.35           C
ATOM      6  CG  MET A   0      -8.679  14.010  21.158  1.00 61.96           C
ATOM      7  SD  MET A   0     -10.051  15.161  21.447  1.00 66.11           S
ATOM      8  CE  MET A   0      -9.658  15.773  23.094  1.00 63.82           C
END
"""

phil_str = """
refinement {
  crystal_symmetry {
    unit_cell = 78.239 78.239 37.281 90 90 90
    space_group = P 43 21 2
  }
}
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Make sure crystal_symmetry from parameter file is used.
  """
  #
  pdb = "%s.pdb"%prefix
  with open(pdb, "w") as fo:
    fo.write(pdb_str)
  #
  phil = "%s.eff"%prefix
  with open(phil, "w") as fo:
    fo.write(phil_str)
  #
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/data/%s.mtz"%prefix,
    test=os.path.isfile)
  #
  args = [
    pdb,
    hkl,
    phil,
    "main.number_of_mac=0",
    ]
  r = run_phenix_refine(args = args, prefix = prefix)
  #
  cs = iotbx.pdb.input(file_name=r.pdb).crystal_symmetry().space_group()
  assert cs.type().lookup_symbol() == "P 43 21 2"

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
