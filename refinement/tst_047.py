from __future__ import division
from __future__ import print_function
from libtbx.utils import null_out
from phenix_regression.refinement import run_fmodel
import os, random
import iotbx.pdb

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Run phenix.refine from phenix.refinement.fsr.wrappers
  """
  pdb_str = """\
HETATM    1  N   TYR A   1      -1.048  -0.331   2.695  1.00 10.00      A    N
HETATM    2  CA  TYR A   1       0.084  -0.660   1.899  1.00 12.00      A    C
HETATM    3  C   TYR A   1       1.181  -1.205   2.784  1.00 11.00      A    C
HETATM    4  O   TYR A   1       1.492  -2.423   2.724  1.00 12.00      A    O
HETATM    5  CB  TYR A   1       0.578   0.578   1.188  1.00 13.00      A    C
HETATM    6  CG  TYR A   1       0.204   0.503  -0.276  1.00 14.00      A    C
HETATM    7  CD1 TYR A   1      -0.884   1.219  -0.750  1.00 15.00      A    C
HETATM    8  CD2 TYR A   1       0.980  -0.239  -1.152  1.00 15.00      A    C
HETATM    9  CE1 TYR A   1      -1.196   1.192  -2.101  1.00 16.00      A    C
HETATM   10  CE2 TYR A   1       0.668  -0.266  -2.503  1.00 16.00      A    C
HETATM   11  CZ  TYR A   1      -0.420   0.450  -2.977  1.00 17.00      A    C
HETATM   12  OH  TYR A   1      -0.712   0.456  -4.353  1.00 18.00      A    O
HETATM   13  OXT TYR A   1       1.774  -0.442   3.592  1.00 11.00      A    O-1
HETATM   14  H   TYR A   1      -1.693  -0.973   2.590  1.00 20.00      A    H
HETATM   15  H2  TYR A   1      -0.800  -0.287   3.575  1.00 20.00      A    H
HETATM   16  HA  TYR A   1      -0.167  -1.336   1.239  1.00 20.00      A    H
HETATM   17  HB2 TYR A   1       1.551   0.637   1.273  1.00 20.00      A    H
HETATM   18  HB3 TYR A   1       0.168   1.370   1.589  1.00 20.00      A    H
HETATM   19  HD1 TYR A   1      -1.427   1.747  -0.134  1.00 20.00      A    H
HETATM   20  HD2 TYR A   1       1.747  -0.743  -0.819  1.00 20.00      A    H
HETATM   21  HE1 TYR A   1      -1.961   1.698  -2.435  1.00 20.00      A    H
HETATM   22  HE2 TYR A   1       1.215  -0.788  -3.120  1.00 20.00      A    H
HETATM   23  HH  TYR A   1      -1.331  -0.159  -4.527  1.00 20.00      A    H
TER
END
"""
  pdb_in = "%s.pdb"%prefix
  open(pdb_in, "w").write(pdb_str)
  args = [
    pdb_in,
    "high_resolution=3.0",
    "type=real",
    "label=F",
    "r_free_flags_fraction=0.1",
    "generate_fake_p1_symmetry=True"]
  r = run_fmodel(args = args, prefix = prefix)
  #
  pdb_start = iotbx.pdb.input(pdb_in)
  hierarchy_start = pdb_start.construct_hierarchy()
  for atom in hierarchy_start.atoms() :
    atom.b = 10
    atom.occ=random.random()
  open(pdb_in, "w").write(hierarchy_start.as_pdb_string())
  #
  args = [ pdb_in, r.mtz, "--quiet"]
  from phenix.refinement.fsr import wrappers
  ref_obj = wrappers.run_phenix_refine(args = args)
  #
  ph=iotbx.pdb.input(file_name="%s_refine_001.pdb"%prefix).construct_hierarchy()
  assert ph.is_similar_hierarchy(ref_obj.model.get_hierarchy())

if (__name__ == "__main__") :
  run()
  print("OK")
