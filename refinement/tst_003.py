import libtbx.load_env
import libtbx.path
import os
from phenix_regression.refinement import run_phenix_refine

def run(prefix = os.path.basename(__file__).replace(".py","")):
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/jcm.pdb", test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/jcd.mtz", \
    test=os.path.isfile)
  args = [pdb,hkl,"main.number_of_macro_cycles=1","strategy=none",
    "miller_array.labels.name=F_c19_4,SIGF_c19_4",
    "miller_array.labels.name=HLADM,HLBDM,HLCDM,HLDDM"]
  r = run_phenix_refine(args = args, prefix = prefix)
  r.check_final_r(r_work=0.2784, r_free=0.3364, eps=0.005, info_low_eps=0.01)

if (__name__ == "__main__"):
  run()
  print("OK")
