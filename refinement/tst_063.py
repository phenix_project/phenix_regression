from __future__ import division
from __future__ import print_function
import libtbx.load_env
import os
from phenix_regression.refinement import run_phenix_refine
from libtbx import easy_run

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Exercise adding water. Use reflection file as pickle file. Refine only
  selected coordinates.
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/lysozyme_h.pdb", test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/lysozyme.pickle", \
    test=os.path.isfile)
  #
  print("iotbx.pdb_remediator %s > %s.pdb"%(pdb, prefix))
  assert not easy_run.call("iotbx.pdb_remediator %s > %s.pdb"%(pdb, prefix))
  args = [
    "%s.pdb"%prefix,
    hkl,
    "ordered_solvent=True",
    """refine.sites.individual='resid 17:27' """,
    "main.number_of_macro_cycles=1",
    "main.max_number_of_iterations=4"]
  r = run_phenix_refine(args = args, prefix = prefix)

if (__name__ == "__main__"):
  run()
  print("OK")
