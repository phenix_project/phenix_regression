from __future__ import division
from __future__ import print_function

import os, random
from libtbx import easy_run
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel
from scitbx.array_family import flex

if (1):
  random.seed(0)
  flex.set_random_seed(0)

pdb_str = """
CRYST1    4.446    4.800    9.065  90.00  90.00  90.00 P 1
ATOM      1  N   PHE A   1       1.896   0.687   3.886  1.00  6.23           N
ATOM      2  CA  PHE A   1       1.137   1.931   4.156  1.00  6.10           C
ATOM      3  C   PHE A   1       1.975   3.136   3.784  1.00  5.73           C
ATOM      4  O   PHE A   1       3.195   3.044   3.671  1.00  6.25           O
ATOM      5  CB  PHE A   1       0.676   1.993   5.621  1.00  6.12           C
ATOM      6  CG  PHE A   1       1.770   2.302   6.613  1.00  6.04           C
ATOM      7  CD1 PHE A   1       2.134   3.615   6.868  1.00  6.26           C
ATOM      8  CD2 PHE A   1       2.424   1.298   7.305  1.00  6.40           C
ATOM      9  CE1 PHE A   1       3.131   3.913   7.772  1.00  6.62           C
ATOM     10  CE2 PHE A   1       3.418   1.597   8.220  1.00  6.66           C
ATOM     11  CZ  PHE A   1       3.776   2.903   8.452  1.00  6.73           C
TER
END
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Make sure final rmsds are correct
  """
  pdb_in = "%s.pdb"%prefix
  open(pdb_in, "w").write(pdb_str)
  #
  pdb_poor = "%s_poor.pdb"%prefix
  cmd = " ".join([
    "phenix.pdbtools",
    pdb_in,
    "sites.shake=0.5",
    "suffix=none",
    "random_seed=1",
    "output.prefix=%s"%pdb_poor.replace(".pdb",""),
    "> %s.pdbtools.log"%prefix])
  easy_run.call(cmd)
  #
  args = [
    pdb_in,
    "high_res=4",
    "type=real",
    "r_free=0.1",
    "random_seed=7"]
  r = run_fmodel(args = args, prefix = prefix)
  #
  args = [
    pdb_poor,
    r.mtz,
    "const_shrink_donor_acceptor=0.6",
    "main.bulk_sol=false",
    "write_model_cif_file=true",
    "main.number_of_mac=1",
    "c_beta_restraints=False"]
  r = run_phenix_refine(args = args, prefix = prefix)
  #
  def get_ba(f):
    b,a=None,None
    for l in open(f, "r").readlines():
      if(l.count("  Bond      :")): b = float(l.split()[2])
      if(l.count("  Angle     :")): a = float(l.split()[2])
    return b,a
  #
  bonds  = [r.bond_final]
  angles = [r.angle_final]
  f = "%s_ms.zlog"%prefix
  cmd = " ".join([
    "phenix.model_statistics",
    r.cif,
    "> %s"%f])
  assert not easy_run.call(cmd)
  b,a = get_ba(f)
  bonds.append(b)
  angles.append(a)
  #
  bonds  = [round(i,2) for i in bonds]
  angles = [round(i,2) for i in angles]
  #
  assert len(bonds)==2
  assert len(angles)==2
  assert len(list(set(bonds)))  == 1, bonds
  assert len(list(set(angles))) == 1, angles

if (__name__ == "__main__"):
  run()
  print("OK")
