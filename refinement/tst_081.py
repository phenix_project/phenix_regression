from __future__ import division
from __future__ import print_function
from phenix_regression.refinement import run_phenix_refine
import libtbx.load_env
import os

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Exercise TLS and individual aniso refinement.
  """
  pdb = libtbx.env.find_in_repositories(relative_path =
    "phenix_regression/pdb/pdb1akg_very_well_phenix_refined_001.pdb",
    test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path = "phenix_regression/reflection_files/1akg.mtz",
    test = os.path.isfile)
  par = libtbx.env.find_in_repositories(
    relative_path = "phenix_regression/phil_files/params1",
    test = os.path.isfile)
  args = [pdb, hkl, par, "xray_data.high_resolution=1.5"]
  r = run_phenix_refine(args = args, prefix=prefix)
  r.check_start_r(r_work=0.1021, r_free=0.1186, eps=0.01)
  r.check_final_r(r_work=0.0893, r_free=0.1181, eps=0.01)

if (__name__ == "__main__"):
  run()
  print("OK")
