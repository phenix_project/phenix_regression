from __future__ import division
from __future__ import print_function
from phenix_regression.refinement import run_phenix_refine
import os
from iotbx import mtz

pdb_str = """
CRYST1   21.937    4.866   23.477  90.00 107.08  90.00 P 1 21 1      2
ATOM      1  N   GLY A   1      -9.009   4.612   6.102  1.00 16.77           N
ATOM      2  CA  GLY A   1      -9.052   4.207   4.651  1.00 16.57           C
ATOM      3  C   GLY A   1      -8.015   3.140   4.419  1.00 16.16           C
ATOM      4  O   GLY A   1      -7.523   2.521   5.381  1.00 16.78           O
ATOM      5  N   ASN A   2      -7.656   2.923   3.155  1.00 15.02           N
ATOM      6  CA  ASN A   2      -6.522   2.038   2.831  1.00 14.10           C
ATOM      7  C   ASN A   2      -5.241   2.537   3.427  1.00 13.13           C
ATOM      8  O   ASN A   2      -4.978   3.742   3.426  1.00 11.91           O
ATOM      9  CB  ASN A   2      -6.346   1.881   1.341  1.00 15.38           C
ATOM     10  CG  ASN A   2      -7.584   1.342   0.692  1.00 14.08           C
ATOM     11  OD1 ASN A   2      -8.025   0.227   1.016  1.00 17.46           O
ATOM     12  ND2 ASN A   2      -8.204   2.155  -0.169  1.00 11.72           N
ATOM     13  N   ASN A   3      -4.438   1.590   3.905  1.00 12.26           N
ATOM     14  CA  ASN A   3      -3.193   1.904   4.589  1.00 11.74           C
ATOM     15  C   ASN A   3      -1.955   1.332   3.895  1.00 11.10           C
ATOM     16  O   ASN A   3      -1.872   0.119   3.648  1.00 10.42           O
ATOM     17  CB  ASN A   3      -3.259   1.378   6.042  1.00 12.15           C
ATOM     18  CG  ASN A   3      -2.006   1.739   6.861  1.00 12.82           C
ATOM     19  OD1 ASN A   3      -1.702   2.925   7.072  1.00 15.05           O
ATOM     20  ND2 ASN A   3      -1.271   0.715   7.306  1.00 13.48           N                                                                     118D 279
"""

hkl_str = \
"""    1
 -987
    21.937     4.866    23.477    90.000   107.080    90.000 p1211
 -12   0   2    12.2    16.3
 -12   0   3    11.0    16.1
 -12   0   4    53.6    62.2
 -12   0   5    78.9    95.7
 -11   0   1    89.5    78.1
 -11   0   2    24.1    31.5
 -11   0   3    40.6    47.1
 -11   0   4    72.2    83.5
 -11   0   5   304.2   281.1
 -11   0   6    34.7    42.9
 -11   0   8    40.8    49.8
 -11   1   2    32.4    24.8
 -11   1   3    42.6    33.0
 -11   1   5    52.7    46.5
 -11   1   6   122.1    76.7
 -10   0   2    39.9    47.3
 -10   0   3    87.6    87.4
 -10   0   5    51.8    59.3
 -10   0   6    23.8    31.7
 -10   0   7   140.2   128.1
 -10   0   8   210.0   189.2
 -10   0   9    38.6    48.6
 -10   0  10    30.8    37.4
 -10   1   1    21.0    13.6
 -10   1   3   153.8    90.5
 -10   1   4    82.3    44.8
 -10   1   5   296.9   207.4
 -10   1   6    21.2    19.3
 -10   1   7    40.2    30.8
 -10   1   8    81.7    52.8
  -9   0   2    65.6    64.5
  -9   0   3   202.2   145.3
  -9   0   5    36.1    45.4
  -9   0   6   109.8    98.1
  -9   0   7    77.3    79.5
  -9   0   8    37.8    44.6
  -9   0   9   105.5    85.4
  -9   0  10    19.2    26.6
  -9   0  11    52.9    60.5
  -9   1   3   115.6    51.2
  -9   1   4   140.7    46.3
  -9   1   5   119.9    71.6
  -9   1   6   177.2    68.9
  -9   1   7    65.0    47.1
"""

def check(file_name, expected):
  mtz_obj = mtz.object(file_name=file_name)
  mas = mtz_obj.as_miller_arrays()
  cntr=0
  for ma in mas:
    if(ma.info().label_string() in expected):
      cntr+=1
  assert cntr==len(expected)

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Exercise scalepack input.
  """
  pdb  = "%s.pdb"%prefix
  open(pdb, "w").write(pdb_str)
  hkl  = "%s.sca"%prefix
  open(hkl, "w").write(hkl_str)
  args = [pdb, hkl, "xray_data.r_free_flags.generate=True",
    "main.number_of_mac=0", "strategy=none"]
  r = run_phenix_refine(args = args, prefix = prefix)
  expected=[
    "I-obs,SIGI-obs",
    "R-free-flags",
    "F-obs-filtered,SIGF-obs-filtered",
    "F-model,PHIF-model",
    "2FOFCWT,PH2FOFCWT",
    "2FOFCWT_no_fill,PH2FOFCWT_no_fill",
    "FOFCWT,PHFOFCWT"]
  check(file_name=r.mtz, expected=expected)
  check(file_name="%s_data.mtz"%prefix,
    expected=["I-obs,SIGI-obs", "R-free-flags"])

if (__name__ == "__main__") :
  run()
  print("OK")
