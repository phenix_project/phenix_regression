from __future__ import division
from __future__ import print_function
import libtbx.load_env
import os
from phenix_regression.refinement import run_phenix_refine

def run(prefix = os.path.basename(__file__).replace(".py","")):
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/potassium.pdb",
    test=os.path.isfile)
  mtz_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/potassium.mtz",
    test=os.path.isfile)
  args = [
    pdb_file,
    mtz_file,
    "main.number_of_macro_cycles=1",
    "strategy=individual_sites"]
  r = run_phenix_refine(args = args, prefix = prefix)
  from iotbx import pdb
  pdb1 = pdb.input(pdb_file)
  sites1 = pdb1.construct_hierarchy().atoms().extract_xyz()
  pdb2 = pdb.input(r.pdb)
  sites2 = pdb2.construct_hierarchy().atoms().extract_xyz()
  assert (sites1.rms_difference(sites2) < 0.01), '%s>0.01' % sites1.rms_difference(sites2)
  assert (r.r_free_start < 0.005)
  assert (r.r_free_final < 0.005)

if (__name__ == "__main__") :
  run()
  print("OK")
