from __future__ import division
from __future__ import print_function
import time,os
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel
import iotbx.pdb

pdb_str = """\
CRYST1   24.463   29.040   36.283  90.00  90.00  90.00 P 1
ATOM      1  CB  ASP A 138      13.417  10.040  19.094  1.00 10.58           C
ANISOU    1  CB  ASP A 138     1317   1757    948    -88    311    -82       C
ATOM      2  CG  ASP A 138      13.125  11.238  19.990  1.00 10.80           C
ANISOU    2  CG  ASP A 138     1316   1816    971    -91    345   -123       C
ATOM      3  OD1 ASP A 138      14.059  11.927  20.444  1.00 10.69           O
ANISOU    3  OD1 ASP A 138     1316   1773    973    -70    333   -118       O
ATOM      4  OD2 ASP A 138      11.926  11.494  20.230  1.00 11.15           O
ANISOU    4  OD2 ASP A 138     1320   1915   1000   -115    384   -167       O
ATOM      5  HB2 ASP A 138      13.017  10.213  18.228  1.00 12.70           H
ATOM      6  HB3 ASP A 138      13.005   9.260  19.498  1.00 12.70           H
ATOM      7  CG  HIS A 142      15.128  12.539  12.825  1.00  9.40           C
ANISOU    7  CG  HIS A 142     1050   1548    975    185    283    -20       C
ATOM      8  ND1 HIS A 142      13.796  12.608  12.488  1.00  9.96           N
ANISOU    8  ND1 HIS A 142     1093   1652   1040    179    286    -36       N
ATOM      9  CD2 HIS A 142      15.764  13.451  12.054  1.00  9.34           C
ANISOU    9  CD2 HIS A 142     1030   1528    992    217    278      0       C
ATOM     10  CE1 HIS A 142      13.630  13.516  11.541  1.00  9.65           C
ANISOU   10  CE1 HIS A 142     1028   1616   1023    210    276    -22       C
ATOM     11  NE2 HIS A 142      14.806  14.044  11.261  1.00  9.51           N
ANISOU   11  NE2 HIS A 142     1023   1571   1019    230    272      2       N
ATOM     12  HD1 HIS A 142      13.170  12.131  12.835  1.00 11.95           H
ATOM     13  HD2 HIS A 142      16.675  13.639  12.056  1.00 11.21           H
ATOM     14  HE1 HIS A 142      12.822  13.745  11.143  1.00 11.58           H
ATOM     15  CD  GLU A 143      18.894  15.176  12.403  1.00 20.08           C
ANISOU   15  CD  GLU A 143     2397   2819   2415    254    270     41       C
ATOM     16  OE1 GLU A 143      19.214  15.057  11.201  1.00 18.00           O
ANISOU   16  OE1 GLU A 143     2128   2566   2144    260    275     57       O
ATOM     17  OE2 GLU A 143      18.002  15.967  12.788  1.00 20.32           O
ANISOU   17  OE2 GLU A 143     2412   2853   2455    259    268     30       O
ATOM     18  HA  GLU A 143      19.122  12.640  11.354  1.00 20.65           H
ATOM     19  CG  HIS A 146      17.982  12.605   7.678  1.00  9.59           C
ANISOU   19  CG  HIS A 146     1078   1590    977    234    292     43       C
ATOM     20  ND1 HIS A 146      16.933  12.606   6.788  1.00  9.77           N
ANISOU   20  ND1 HIS A 146     1099   1643    971    225    282     50       N
ATOM     21  CD2 HIS A 146      17.599  13.392   8.711  1.00  9.46           C
ANISOU   21  CD2 HIS A 146     1052   1555    987    242    281     50       C
ATOM     22  CE1 HIS A 146      15.950  13.350   7.263  1.00  9.76           C
ANISOU   22  CE1 HIS A 146     1082   1640    988    231    264     59       C
ATOM     23  NE2 HIS A 146      16.332  13.842   8.430  1.00  9.58           N
ANISOU   23  NE2 HIS A 146     1054   1589    998    241    267     55       N
ATOM     24  HD1 HIS A 146      16.917  12.182   6.040  1.00 11.73           H
ATOM     25  HD2 HIS A 146      18.098  13.588   9.471  1.00 11.35           H
ATOM     26  HE1 HIS A 146      15.133  13.504   6.847  1.00 11.72           H
ATOM     27  HE1 TYR A 157      16.840  14.703   5.000  1.00 41.79           H
ATOM     28  CA  GLU A 166      11.659  13.014   7.207  1.00 11.74           C
ANISOU   28  CA  GLU A 166     1269   1954   1238    213    214     22       C
ATOM     29  CB  GLU A 166      10.973  14.378   7.260  1.00 13.13           C
ANISOU   29  CB  GLU A 166     1404   2123   1461    240    187     30       C
ATOM     30  CG  GLU A 166      11.911  15.540   7.025  1.00 15.23           C
ANISOU   30  CG  GLU A 166     1681   2358   1749    261    175     65       C
ATOM     31  CD  GLU A 166      12.985  15.615   8.085  1.00 11.07           C
ANISOU   31  CD  GLU A 166     1168   1805   1232    265    206     55       C
ATOM     32  OE1 GLU A 166      12.649  15.476   9.279  1.00 13.89           O
ANISOU   32  OE1 GLU A 166     1512   2161   1605    263    224     19       O
ATOM     33  OE2 GLU A 166      14.160  15.790   7.722  1.00 15.85           O
ANISOU   33  OE2 GLU A 166     1798   2397   1828    264    213     80       O
ATOM     34  HA  GLU A 166      12.531  13.101   7.624  1.00 14.09           H
ATOM     35  HB3 GLU A 166      10.572  14.493   8.136  1.00 15.75           H
ATOM     36  HG2 GLU A 166      12.343  15.433   6.163  1.00 18.28           H
ATOM     37  HG3 GLU A 166      11.406  16.368   7.046  1.00 18.28           H
ATOM     38  CB  SER A 169      14.464  10.639   9.858  1.00 10.86           C
ANISOU   38  CB  SER A 169     1245   1769   1111    179    270    -12       C
ATOM     39  OG  SER A 169      15.581  10.444   9.010  1.00 10.16           O
ANISOU   39  OG  SER A 169     1170   1668   1021    194    273     -5       O
ATOM     40  HB2 SER A 169      13.850  11.258   9.432  1.00 13.03           H
ATOM     41  HB3 SER A 169      14.770  11.006  10.702  1.00 13.03           H
ATOM     42  HG  SER A 169      15.970  11.176   8.873  1.00 12.19           H
ATOM     43  HA  THR A 174      11.178   6.671  18.130  1.00 17.44           H
ATOM     44  CB  GLU A 177      12.931   5.955  21.187  1.00 11.85           C
ANISOU   44  CB  GLU A 177     1663   1890    948   -345    226      4       C
ATOM     45  CG  GLU A 177      12.382   6.394  22.532  1.00 14.06           C
ANISOU   45  CG  GLU A 177     1951   2230   1161   -424    261    -17       C
ATOM     46  CD  GLU A 177      11.724   7.760  22.467  1.00 12.25           C
ANISOU   46  CD  GLU A 177     1645   2063    948   -384    329    -80       C
ATOM     47  OE1 GLU A 177      12.293   8.658  21.822  1.00 11.76           O
ANISOU   47  OE1 GLU A 177     1548   1976    946   -296    329    -90       O
ATOM     48  OE2 GLU A 177      10.639   7.937  23.054  1.00 12.75           O
ANISOU   48  OE2 GLU A 177     1680   2197    966   -443    381   -121       O
ATOM     49  HB2 GLU A 177      13.446   6.693  20.826  1.00 14.22           H
ATOM     50  HB3 GLU A 177      12.175   5.771  20.608  1.00 14.22           H
ATOM     51  HG2 GLU A 177      11.717   5.753  22.828  1.00 16.87           H
ATOM     52  HG3 GLU A 177      13.110   6.440  23.172  1.00 16.87           H
ATOM     53  CA  ASN A 183      10.560   5.906  28.364  1.00 18.73           C
ANISOU   53  CA  ASN A 183     2707   3054   1355   -918    348    -41       C
ATOM     54  C   ASN A 183      10.043   6.104  26.946  1.00 20.08           C
ANISOU   54  C   ASN A 183     2797   3227   1607   -821    380    -71       C
ATOM     55  O   ASN A 183       9.838   7.237  26.516  1.00 18.87           O
ANISOU   55  O   ASN A 183     2565   3099   1507   -737    429   -134       O
ATOM     56  CB  ASN A 183      10.830   7.267  29.002  1.00 23.82           C
ANISOU   56  CB  ASN A 183     3316   3728   2007   -881    389   -102       C
ATOM     57  HB2 ASN A 183      11.511   7.728  28.487  1.00 28.59           H
ATOM     58  HB3 ASN A 183      10.009   7.783  29.006  1.00 28.59           H
ATOM     59  N   PRO A 184       9.829   5.000  26.211  1.00 15.26           N
ANISOU   59  N   PRO A 184     2205   2578   1014   -830    340    -27       N
ATOM     60  CA  PRO A 184       9.489   5.123  24.789  1.00 14.81           C
ANISOU   60  CA  PRO A 184     2080   2507   1042   -731    351    -48       C
ATOM     61  C   PRO A 184       8.099   5.704  24.532  1.00 16.10           C
ANISOU   61  C   PRO A 184     2148   2762   1206   -733    434   -123       C
ATOM     62  O   PRO A 184       7.228   5.649  25.397  1.00 15.60           O
ANISOU   62  O   PRO A 184     2073   2768   1087   -822    477   -155       O
ATOM     63  HA  PRO A 184      10.153   5.658  24.328  1.00 17.78           H
ATOM     64  N   ASP A 185       7.911   6.251  23.336  1.00 15.61           N
ANISOU   64  N   ASP A 185     2016   2689   1224   -629    444   -150       N
ATOM     65  CA  ASP A 185       6.648   6.862  22.951  1.00 15.82           C
ANISOU   65  CA  ASP A 185     1945   2791   1276   -612    507   -219       C
ATOM     66  C   ASP A 185       6.661   7.147  21.455  1.00 15.58           C
ANISOU   66  C   ASP A 185     1866   2723   1331   -502    484   -218       C
ATOM     67  O   ASP A 185       7.657   6.887  20.779  1.00 16.16           O
ANISOU   67  O   ASP A 185     1982   2722   1437   -446    431   -169       O
ATOM     68  CB  ASP A 185       6.426   8.156  23.732  1.00 17.62           C
ANISOU   68  CB  ASP A 185     2120   3068   1507   -597    562   -290       C
ATOM     69  CG  ASP A 185       7.600   9.111  23.621  1.00 15.54           C
ANISOU   69  CG  ASP A 185     1868   2748   1287   -508    539   -284       C
ATOM     70  OD1 ASP A 185       8.001   9.458  22.486  1.00 14.51           O
ANISOU   70  OD1 ASP A 185     1715   2567   1232   -409    509   -267       O
ATOM     71  OD2 ASP A 185       8.131   9.511  24.674  1.00 17.36           O
ANISOU   71  OD2 ASP A 185     2134   2985   1477   -541    548   -293       O
ATOM     72  H   ASP A 185       8.511   6.280  22.721  1.00 18.73           H
ATOM     73  HB3 ASP A 185       6.301   7.943  24.670  1.00 21.15           H
ATOM     74  N   GLU A 187       6.189  10.220  20.231  1.00 14.37           N
ANISOU   74  N   GLU A 187     1533   2587   1340   -288    528   -328       N
ATOM     75  CA  GLU A 187       6.448  11.650  20.125  1.00 14.25           C
ANISOU   75  CA  GLU A 187     1472   2556   1388   -205    534   -364       C
ATOM     76  C   GLU A 187       7.902  11.837  19.695  1.00 13.85           C
ANISOU   76  C   GLU A 187     1480   2425   1357   -149    486   -304       C
ATOM     77  O   GLU A 187       8.702  10.913  19.816  1.00 13.73           O
ANISOU   77  O   GLU A 187     1536   2375   1303   -182    458   -250       O
ATOM     78  CB  GLU A 187       6.213  12.337  21.473  1.00 17.37           C
ANISOU   78  CB  GLU A 187     1846   2997   1756   -243    586   -431       C
ATOM     79  H   GLU A 187       6.547   9.860  20.925  1.00 17.25           H
ATOM     80  HB2 GLU A 187       6.858  11.995  22.112  1.00 20.84           H
ATOM     81  N   ILE A 188       8.236  13.028  19.200  1.00 14.63           N
ANISOU   81  N   ILE A 188     1544   2492   1521    -68    474   -315       N
ATOM     82  CA  ILE A 188       9.579  13.319  18.692  1.00 12.94           C
ANISOU   82  CA  ILE A 188     1374   2211   1332    -16    434   -262       C
ATOM     83  C   ILE A 188      10.239  14.458  19.473  1.00 13.29           C
ANISOU   83  C   ILE A 188     1419   2235   1396      8    440   -286       C
ATOM     84  H   ILE A 188       7.696  13.694  19.147  1.00 17.55           H
ATOM     85  HA  ILE A 188      10.130  12.528  18.801  1.00 15.53           H
ATOM     86 HD13 ILE A 188       9.791  10.904  17.388  1.00 17.03           H
ATOM     87  N   GLY A 189      11.388  14.155  20.068  1.00 12.34           N
ANISOU   87  N   GLY A 189     1362   2082   1244    -15    424   -251       N
ATOM     88  H   GLY A 189      11.708  13.359  20.121  1.00 14.81           H
ATOM     89  N   GLU A 190      10.835  14.892  22.659  1.00 12.30           N
ANISOU   89  N   GLU A 190     1355   2152   1166   -109    494   -356       N
ATOM     90  O   GLU A 190      11.104  16.207  25.991  1.00 13.63           O
ANISOU   90  O   GLU A 190     1560   2393   1226   -225    568   -487       O
ATOM     91  CG  GLU A 190       9.651  12.864  24.733  1.00 13.46           C
ANISOU   91  CG  GLU A 190     1570   2424   1121   -337    555   -376       C
ATOM     92  CD  GLU A 190      10.084  12.032  23.537  1.00 12.81           C
ANISOU   92  CD  GLU A 190     1512   2285   1072   -298    501   -300       C
ATOM     93  OE1 GLU A 190       9.642  12.317  22.402  1.00 12.53           O
ANISOU   93  OE1 GLU A 190     1421   2241   1099   -231    500   -308       O
ATOM     94  OE2 GLU A 190      10.869  11.082  23.738  1.00 12.67           O
ANISOU   94  OE2 GLU A 190     1568   2228   1017   -336    457   -236       O
ATOM     95  H   GLU A 190      10.523  14.158  22.340  1.00 14.76           H
ATOM     96  HB3 GLU A 190       8.540  14.170  23.628  1.00 16.24           H
ATOM     97  HG2 GLU A 190      10.411  12.943  25.331  1.00 16.16           H
ATOM     98  HG3 GLU A 190       8.930  12.391  25.178  1.00 16.16           H
ATOM     99  CG  ASP A 191      13.632  12.147  26.272  1.00 12.90           C
ANISOU   99  CG  ASP A 191     1731   2188    984   -380    394   -207       C
ATOM    100  OD1 ASP A 191      12.680  12.181  27.082  1.00 13.55           O
ANISOU  100  OD1 ASP A 191     1804   2343   1002   -453    446   -257       O
ATOM    101  OD2 ASP A 191      14.044  11.083  25.769  1.00 14.51           O
ANISOU  101  OD2 ASP A 191     1972   2348   1192   -384    345   -143       O
ATOM    102  CA  TYR A 193      12.402  20.104  23.810  1.00 13.30           C
ANISOU  102  CA  TYR A 193     1405   2160   1490     74    483   -508       C
ATOM    103  C   TYR A 193      11.779  20.828  24.994  1.00 13.83           C
ANISOU  103  C   TYR A 193     1440   2269   1544     49    532   -615       C
ATOM    104  O   TYR A 193      11.280  20.191  25.913  1.00 14.08           O
ANISOU  104  O   TYR A 193     1484   2369   1496    -28    577   -653       O
ATOM    105  CB  TYR A 193      11.303  19.604  22.869  1.00 13.14           C
ANISOU  105  CB  TYR A 193     1334   2168   1490     91    493   -507       C
ATOM    106  CD2 TYR A 193       9.162  20.881  22.444  1.00 13.81           C
ANISOU  106  CD2 TYR A 193     1266   2289   1692    164    527   -642       C
ATOM    107  H   TYR A 193      12.768  18.254  24.371  1.00 19.05           H
ATOM    108  HB3 TYR A 193      10.676  19.068  23.380  1.00 15.77           H
ATOM    109  HD2 TYR A 193       8.734  20.324  23.052  1.00 16.57           H
ATOM    110  N   THR A 194      11.830  22.157  24.952  1.00 17.33           N
ANISOU  110  N   THR A 194     1846   2670   2068    111    521   -665       N
ATOM    111  CA  THR A 194      11.180  23.035  25.933  1.00 21.48           C
ANISOU  111  CA  THR A 194     2327   3227   2606    106    565   -785       C
ATOM    112  C   THR A 194      11.243  22.570  27.387  1.00 22.28           C
ANISOU  112  C   THR A 194     2472   3397   2597      9    615   -831       C
ATOM    113  O   THR A 194      10.239  22.115  27.941  1.00 22.44           O
ANISOU  113  O   THR A 194     2468   3486   2571    -46    660   -877       O
ATOM    114  CB  THR A 194       9.701  23.301  25.566  1.00 20.96           C
ANISOU  114  CB  THR A 194     2166   3197   2600    140    596   -863       C
ATOM    115  OG1 THR A 194       8.987  22.061  25.477  1.00 22.59           O
ANISOU  115  OG1 THR A 194     2377   3466   2742     81    619   -831       O
ATOM    116  CG2 THR A 194       9.609  24.040  24.240  1.00 20.91           C
ANISOU  116  CG2 THR A 194     2115   3118   2712    239    539   -832       C
ATOM    117  H   THR A 194      12.252  22.592  24.342  1.00 20.80           H
ATOM    118  HA  THR A 194      11.633  23.893  25.899  1.00 25.77           H
ATOM    119  HB  THR A 194       9.296  23.855  26.252  1.00 25.15           H
ATOM    120 HG23 THR A 194      10.014  23.510  23.536  1.00 25.10           H
ATOM    121  N   PRO A 195      12.425  22.678  28.010  1.00 15.58           N
ANISOU  121  N   PRO A 195     1695   2517   1708    -19    587   -796       N
ATOM    122  CA  PRO A 195      12.456  22.468  29.457  1.00 23.89           C
ANISOU  122  CA  PRO A 195     2788   3632   2657   -113    628   -850       C
ATOM    123  HA  PRO A 195      12.138  21.585  29.700  1.00 28.67           H
ATOM    124  N   ILE A 197       8.300  23.322  30.233  1.00 22.50           N
ANISOU  124  N   ILE A 197     2393   3584   2572   -121    741  -1086       N
ATOM    125  CA  ILE A 197       6.990  22.834  29.811  1.00 22.95           C
ANISOU  125  CA  ILE A 197     2388   3681   2652   -127    757  -1098       C
ATOM    126  C   ILE A 197       7.047  21.314  29.708  1.00 19.14           C
ANISOU  126  C   ILE A 197     1960   3239   2075   -204    761  -1012       C
ATOM    127  O   ILE A 197       7.788  20.764  28.892  1.00 18.16           O
ANISOU  127  O   ILE A 197     1875   3080   1944   -181    729   -927       O
ATOM    128  CB  ILE A 197       6.547  23.444  28.459  1.00 25.97           C
ANISOU  128  CB  ILE A 197     2702   4006   3162    -16    721  -1095       C
ATOM    129  CG2 ILE A 197       5.169  22.892  28.045  1.00 19.76           C
ANISOU  129  CG2 ILE A 197     1851   3264   2393    -28    736  -1110       C
ATOM    130  H   ILE A 197       8.907  23.248  29.629  1.00 27.00           H
ATOM    131  HB  ILE A 197       7.196  23.201  27.781  1.00 31.17           H
ATOM    132 HG22 ILE A 197       5.230  21.928  27.955  1.00 23.71           H
ATOM    133  N   SER A 198       6.257  20.644  30.542  1.00 25.30           N
ANISOU  133  N   SER A 198     2738   4090   2784   -297    799  -1037       N
ATOM    134  CA  SER A 198       6.188  19.187  30.546  1.00 25.31           C
ANISOU  134  CA  SER A 198     2790   4127   2698   -381    798   -960       C
ATOM    135  HA  SER A 198       7.012  18.824  30.183  1.00 30.37           H
ATOM    136  N   GLY A 199       5.252  17.611  28.965  1.00 22.28           N
ANISOU  136  N   GLY A 199     2381   3755   2330   -378    778   -872       N
ATOM    137  H   GLY A 199       6.021  17.232  28.888  1.00 26.74           H
ATOM    138  N   ASP A 200       5.033  18.103  26.265  1.00 20.15           N
ANISOU  138  N   ASP A 200     2024   3401   2231   -192    721   -828       N
ATOM    139  CA  ASP A 200       5.025  18.503  24.858  1.00 17.97           C
ANISOU  139  CA  ASP A 200     1706   3076   2046    -94    684   -804       C
ATOM    140  C   ASP A 200       6.198  17.897  24.097  1.00 16.90           C
ANISOU  140  C   ASP A 200     1635   2904   1884    -79    652   -711       C
ATOM    141  O   ASP A 200       7.035  17.194  24.667  1.00 16.62           O
ANISOU  141  O   ASP A 200     1675   2873   1765   -140    655   -666       O
ATOM    142  CB  ASP A 200       5.000  20.029  24.715  1.00 22.32           C
ANISOU  142  CB  ASP A 200     2202   3580   2699     -4    668   -864       C
ATOM    143  CG  ASP A 200       6.234  20.692  25.277  1.00 19.02           C
ANISOU  143  CG  ASP A 200     1834   3123   2269      9    662   -863       C
ATOM    144  OD1 ASP A 200       6.974  20.028  26.030  1.00 17.82           O
ANISOU  144  OD1 ASP A 200     1756   2993   2022    -62    677   -832       O
ATOM    145  OD2 ASP A 200       6.459  21.883  24.972  1.00 18.07           O
ANISOU  145  OD2 ASP A 200     1683   2948   2236     89    636   -894       O
ATOM    146  H   ASP A 200       5.717  18.379  26.706  1.00 24.18           H
ATOM    147 HH22 ARG A 203      11.966  15.847  13.194  1.00 12.72           H
ATOM    148  CD2 HIS A 231      11.834  18.549   9.682  1.00 16.86           C
ANISOU  148  CD2 HIS A 231     2297   2087   2023   -418    329   -710       C
ATOM    149  NE2 HIS A 231      13.049  18.856   9.126  1.00 17.26           N
ANISOU  149  NE2 HIS A 231     2339   2150   2070   -469    309   -706       N
ATOM    150  HD2 HIS A 231      11.679  17.941  10.368  1.00 20.24           H
ATOM    151  HE2 HIS A 231      13.804  18.513   9.354  1.00 20.71           H
TER
HETATM  152 ZN    ZN X   1      15.181  15.082   9.552  1.00 16.64          Zn
ANISOU  152 ZN    ZN X   1     2164   2063   2094    240    469   -134      Zn
HETATM  153 CA    CA X   2      10.195  10.094  21.447  1.00  9.93          Ca2+
ANISOU  153 CA    CA X   2      706   1965   1103   -201    250   -274      Ca2+
HETATM  154 CA    CA X   3      10.475   9.118  25.145  1.00 15.56          Ca2+
ANISOU  154 CA    CA X   3     2448   1851   1613   -176    921   -309      Ca2+
HETATM  155 CA    CA X   4       9.112  20.107  27.022  1.00 16.05          Ca2+
ANISOU  155 CA    CA X   4     2192   2282   1626   -396    547   -441      Ca2+
TER
HETATM  156  O   HOH S   1      16.682  17.985  12.814  1.00 16.63           O
HETATM  157  O   HOH S   6       9.007  17.877  26.112  1.00 10.81           O
HETATM  158  O   HOH S  16      10.675  10.439  27.164  1.00  9.97           O
HETATM  159  O   HOH S  20      13.613  18.857  12.406  1.00 24.48           O
HETATM  160  O   HOH S  29       9.633   8.235  19.816  1.00 12.14           O
HETATM  161  O   HOH S  40      16.044  16.921   9.899  1.00 19.11           O
HETATM  162  O   HOH S  41      19.463  16.614   8.835  1.00 23.34           O
HETATM  163  O   HOH S  44      12.918   8.836  25.364  1.00 12.78           O
HETATM  164  O   HOH S  54      13.179  12.647  23.042  1.00 17.99           O
HETATM  165  O   HOH S 113      18.169  18.813   9.735  1.00 42.33           O
HETATM  166  O   HOH S 128       9.418  19.715  31.283  1.00 35.24           O
HETATM  167  O   HOH S 135      15.940  19.462  10.074  1.00 26.53           O
HETATM  168  O   HOH S 148      15.231  17.935   8.081  1.00 18.39           O
HETATM  169  O   HOH S 150      14.790  17.615   6.019  1.00 21.45           O
HETATM  170  O   HOH S 186       6.874   9.953  27.060  1.00 27.50           O
HETATM  171  O   HOH S 190      12.978   5.945  26.545  1.00 21.73           O
HETATM  172  O   HOH S 191      13.626  19.123  27.977  1.00 24.93           O
HETATM  173  O   HOH S 222      11.615  16.559  28.518  1.00 27.82           O
HETATM  174  O   HOH S 242       8.077  16.253  29.298  1.00 35.14           O
TER
END
"""

params_str = """\
refinement {
  refine {
    strategy = *individual_sites individual_sites_real_space rigid_body \
               *individual_adp group_adp tls occupancies group_anomalous
    adp {
      individual {
        anisotropic = element ZN or element CA
      }
    }
  }
  main {
    bulk_solvent_and_scale=False
  }
}
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Make sure everything converts to isotropic except selected atoms.
  Also make sure show_residual_map_peaks_and_holes=true works (does not crash,
  result is not checked).
  """
  pdb_file_name = "%s.pdb"%prefix
  of=open(pdb_file_name, "w")
  print(pdb_str, file=of)
  of.close()
  #
  params_file_name = "%s.eff"%prefix
  of=open(params_file_name, "w")
  print(params_str, file=of)
  of.close()
  #
  args = ["%s"%pdb_file_name, "high_res=6 type=real r_free=0.1"]
  r = run_fmodel(args = args, prefix = prefix)
  #
  args = ["%s"%pdb_file_name, "%s"%params_file_name, r.mtz,
    "main.number_of_mac=1", "show_residual_map_peaks_and_holes=true"]
  r = run_phenix_refine(args = args, prefix = prefix)
  #
  xrs = iotbx.pdb.input(file_name = r.pdb).xray_structure_simple()
  cntr=0
  for s in xrs.scatterers().select(xrs.use_u_aniso()):
    assert s.element_symbol().strip().upper() in ["ZN","CA"]
    cntr+=1
  assert cntr==4

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
