from __future__ import print_function
import time, os
import iotbx.pdb
from scitbx.array_family import flex
from libtbx.test_utils import assert_lines_in_file
from phenix_regression.refinement import run_fmodel
from phenix_regression.refinement import run_phenix_refine

pdb_str_answer = """
CRYST1   35.937   25.866   35.477  90.00  90.00  90.00 P 21 21 21
HETATM    0 MG    MG A   0     -12.962  -0.502   5.005  1.00 30.00          MG  
ATOM      1  N   GLY A   1      -9.644   4.477   5.109  1.00 10.00           N
ATOM      2  CA  GLY A   1      -9.698   3.485   4.051  1.00 10.00           C
ATOM      3  C   GLY A   1      -8.456   2.619   3.976  1.00 10.00           C
ATOM      4  O   GLY A   1      -8.191   1.818   4.872  1.00 10.00           O
ATOM      5  N   ASN A   2      -7.693   2.781   2.900  1.00 10.00           N
ATOM      6  CA  ASN A   2      -6.470   2.010   2.706  1.00 10.00           C
ATOM      7  C   ASN A   2      -5.326   2.577   3.540  1.00 10.00           C
ATOM      8  O   ASN A   2      -5.275   3.778   3.806  1.00 10.00           O
ATOM      9  CB  ASN A   2      -6.082   1.985   1.226  1.00 10.00           C
ATOM     10  CG  ASN A   2      -7.155   1.366   0.352  1.00 10.00           C
ATOM     11  OD1 ASN A   2      -7.231   0.145   0.214  1.00 10.00           O
ATOM     12  ND2 ASN A   2      -7.991   2.207  -0.244  1.00 10.00           N
ATOM     13  N   ASN A   3      -4.411   1.705   3.950  1.00 10.00           N
ATOM     14  CA  ASN A   3      -3.267   2.117   4.754  1.00 10.00           C
ATOM     15  C   ASN A   3      -1.969   1.548   4.190  1.00 10.00           C
ATOM     16  O   ASN A   3      -1.846   0.340   3.987  1.00 10.00           O
ATOM     17  CB  ASN A   3      -3.448   1.679   6.209  1.00 10.00           C
ATOM     18  CG  ASN A   3      -2.288   2.097   7.092  1.00 10.00           C
ATOM     19  OD1 ASN A   3      -2.233   3.230   7.569  1.00 10.00           O
ATOM     20  ND2 ASN A   3      -1.355   1.179   7.317  1.00 10.00           N
ATOM     21  N   GLN A   4      -1.003   2.425   3.940  1.00 10.00           N
ATOM     22  CA  GLN A   4       0.287   2.012   3.400  1.00 10.00           C
ATOM     23  C   GLN A   4       1.433   2.707   4.127  1.00 10.00           C
ATOM     24  O   GLN A   4       1.638   3.911   3.976  1.00 10.00           O
ATOM     25  CB  GLN A   4       0.360   2.306   1.900  1.00 10.00           C
ATOM     26  CG  GLN A   4       1.662   1.875   1.246  1.00 10.00           C
ATOM     27  CD  GLN A   4       1.714   2.218  -0.230  1.00 10.00           C
ATOM     28  OE1 GLN A   4       1.140   3.215  -0.668  1.00 10.00           O
ATOM     29  NE2 GLN A   4       2.405   1.390  -1.006  1.00 10.00           N
ATOM     30  N   GLN A   5       2.178   1.940   4.917  1.00 10.00           N
ATOM     31  CA  GLN A   5       3.304   2.480   5.669  1.00 10.00           C
ATOM     32  C   GLN A   5       4.626   1.918   5.156  1.00 10.00           C
ATOM     33  O   GLN A   5       4.798   0.703   5.057  1.00 10.00           O
ATOM     34  CB  GLN A   5       3.148   2.178   7.161  1.00 10.00           C
ATOM     35  CG  GLN A   5       1.894   2.769   7.786  1.00 10.00           C
ATOM     36  CD  GLN A   5       1.752   2.414   9.253  1.00 10.00           C
ATOM     37  OE1 GLN A   5       1.799   1.243   9.628  1.00 10.00           O
ATOM     38  NE2 GLN A   5       1.576   3.428  10.092  1.00 10.00           N
ATOM     39  N   ASN A   6       5.555   2.810   4.829  1.00 10.00           N
ATOM     40  CA  ASN A   6       6.862   2.405   4.325  1.00 10.00           C
ATOM     41  C   ASN A   6       7.955   2.685   5.351  1.00 10.00           C
ATOM     42  O   ASN A   6       8.483   3.795   5.422  1.00 10.00           O
ATOM     43  CB  ASN A   6       7.176   3.122   3.011  1.00 10.00           C
ATOM     44  CG  ASN A   6       6.156   2.829   1.929  1.00 10.00           C
ATOM     45  OD1 ASN A   6       6.253   1.826   1.222  1.00 10.00           O
ATOM     46  ND2 ASN A   6       5.168   3.707   1.794  1.00 10.00           N
TER
ATOM     47  N   TYR X   0      10.552   1.512   6.674  1.00 10.00           N  
ATOM     48  CA  TYR X   0      11.582   1.647   7.696  1.00 10.00           C  
ATOM     49  C   TYR X   0      12.976   1.571   7.082  1.00 10.00           C  
ATOM     50  O   TYR X   0      13.154   1.064   5.974  1.00 10.00           O  
ATOM     51  CB  TYR X   0      11.420   0.568   8.769  1.00 10.00           C  
ATOM     52  CG  TYR X   0      10.098   0.625   9.501  1.00 10.00           C  
ATOM     53  CD1 TYR X   0       9.007  -0.115   9.063  1.00 10.00           C  
ATOM     54  CD2 TYR X   0       9.940   1.418  10.630  1.00 10.00           C  
ATOM     55  CE1 TYR X   0       7.796  -0.067   9.728  1.00 10.00           C  
ATOM     56  CE2 TYR X   0       8.733   1.473  11.302  1.00 10.00           C  
ATOM     57  CZ  TYR X   0       7.665   0.729  10.847  1.00 10.00           C  
ATOM     58  OH  TYR X   0       6.462   0.780  11.513  1.00 10.00           O  
ATOM     59  OXT TYR X   0      13.957   2.015   7.679  1.00 10.00           O  
TER
ATOM      1  N   GLY B   1      -9.644  11.477   5.109  1.00 30.00           N
ATOM      2  CA  GLY B   1      -9.698  10.485   4.051  1.00 30.00           C
ATOM      3  C   GLY B   1      -8.456   9.619   3.976  1.00 30.00           C
ATOM      4  O   GLY B   1      -8.191   8.818   4.872  1.00 30.00           O
ATOM      5  N   ASN B   2      -7.693   9.781   2.900  1.00 30.00           N
ATOM      6  CA  ASN B   2      -6.470   9.010   2.706  1.00 30.00           C
ATOM      7  C   ASN B   2      -5.326   9.577   3.540  1.00 30.00           C
ATOM      8  O   ASN B   2      -5.275  10.778   3.806  1.00 30.00           O
ATOM      9  CB  ASN B   2      -6.082   8.985   1.226  1.00 30.00           C
ATOM     10  CG  ASN B   2      -7.155   8.366   0.352  1.00 30.00           C
ATOM     11  OD1 ASN B   2      -7.231   7.145   0.214  1.00 30.00           O
ATOM     12  ND2 ASN B   2      -7.991   9.207  -0.244  1.00 30.00           N
ATOM     13  N   ASN B   3      -4.411   8.705   3.950  1.00 30.00           N
ATOM     14  CA  ASN B   3      -3.267   9.117   4.754  1.00 30.00           C
ATOM     15  C   ASN B   3      -1.969   8.548   4.190  1.00 30.00           C
ATOM     16  O   ASN B   3      -1.846   7.340   3.987  1.00 30.00           O
ATOM     17  CB  ASN B   3      -3.448   8.679   6.209  1.00 30.00           C
ATOM     18  CG  ASN B   3      -2.288   9.097   7.092  1.00 30.00           C
ATOM     19  OD1 ASN B   3      -2.233  10.230   7.569  1.00 30.00           O
ATOM     20  ND2 ASN B   3      -1.355   8.179   7.317  1.00 30.00           N
ATOM     21  N   GLN B   4      -1.003   9.425   3.940  1.00 30.00           N
ATOM     22  CA  GLN B   4       0.287   9.012   3.400  1.00 30.00           C
ATOM     23  C   GLN B   4       1.433   9.707   4.127  1.00 30.00           C
ATOM     24  O   GLN B   4       1.638  10.911   3.976  1.00 30.00           O
ATOM     25  CB  GLN B   4       0.360   9.306   1.900  1.00 30.00           C
ATOM     26  CG  GLN B   4       1.662   8.875   1.246  1.00 30.00           C
ATOM     27  CD  GLN B   4       1.714   9.218  -0.230  1.00 30.00           C
ATOM     28  OE1 GLN B   4       1.140  10.215  -0.668  1.00 30.00           O
ATOM     29  NE2 GLN B   4       2.405   8.390  -1.006  1.00 30.00           N
ATOM     30  N   GLN B   5       2.178   8.940   4.917  1.00 30.00           N
ATOM     31  CA  GLN B   5       3.304   9.480   5.669  1.00 30.00           C
ATOM     32  C   GLN B   5       4.626   8.918   5.156  1.00 30.00           C
ATOM     33  O   GLN B   5       4.798   7.703   5.057  1.00 30.00           O
ATOM     34  CB  GLN B   5       3.148   9.178   7.161  1.00 30.00           C
ATOM     35  CG  GLN B   5       1.894   9.769   7.786  1.00 30.00           C
ATOM     36  CD  GLN B   5       1.752   9.414   9.253  1.00 30.00           C
ATOM     37  OE1 GLN B   5       1.799   8.243   9.628  1.00 30.00           O
ATOM     38  NE2 GLN B   5       1.576  10.428  10.092  1.00 30.00           N
ATOM     39  N   ASN B   6       5.555   9.810   4.829  1.00 30.00           N
ATOM     40  CA  ASN B   6       6.862   9.405   4.325  1.00 30.00           C
ATOM     41  C   ASN B   6       7.955   9.685   5.351  1.00 30.00           C
ATOM     42  O   ASN B   6       8.483  10.795   5.422  1.00 30.00           O
ATOM     43  CB  ASN B   6       7.176  10.122   3.011  1.00 30.00           C
ATOM     44  CG  ASN B   6       6.156   9.829   1.929  1.00 30.00           C
ATOM     45  OD1 ASN B   6       6.253   8.826   1.222  1.00 30.00           O
ATOM     46  ND2 ASN B   6       5.168  10.707   1.794  1.00 30.00           N
ATOM     47  N   TYR B   7       8.291   8.672   6.143  1.00 30.00           N
ATOM     48  CA  TYR B   7       9.321   8.807   7.165  1.00 30.00           C
ATOM     49  C   TYR B   7      10.715   8.731   6.551  1.00 30.00           C
ATOM     50  O   TYR B   7      10.893   8.224   5.443  1.00 30.00           O
ATOM     51  CB  TYR B   7       9.159   7.728   8.238  1.00 30.00           C
ATOM     52  CG  TYR B   7       7.837   7.785   8.970  1.00 30.00           C
ATOM     53  CD1 TYR B   7       6.746   7.045   8.532  1.00 30.00           C
ATOM     54  CD2 TYR B   7       7.679   8.578  10.099  1.00 30.00           C
ATOM     55  CE1 TYR B   7       5.535   7.093   9.197  1.00 30.00           C
ATOM     56  CE2 TYR B   7       6.472   8.633  10.771  1.00 30.00           C
ATOM     57  CZ  TYR B   7       5.404   7.889  10.316  1.00 30.00           C
ATOM     58  OH  TYR B   7       4.201   7.940  10.982  1.00 30.00           O
ATOM     59  OXT TYR B   7      11.696   9.175   7.148  1.00 30.00           O
TER
END
"""

pdb_str_poor = """
CRYST1   35.937   25.866   35.477  90.00  90.00  90.00 P 21 21 21               
SCALE1      0.027826  0.000000  0.000000        0.00000                         
SCALE2      0.000000  0.038661  0.000000        0.00000                         
SCALE3      0.000000  0.000000  0.028187        0.00000                         
HETATM    1 MG    MG A   0     -13.241  -0.268   5.348  1.00 30.00          Mg  
ATOM      2  N   GLY A   1      -9.803   4.708   5.110  1.00 10.00           N  
ATOM      3  CA  GLY A   1      -9.830   3.946   4.636  1.00 10.00           C  
ATOM      4  C   GLY A   1      -8.132   2.550   4.372  1.00 10.00           C  
ATOM      5  O   GLY A   1      -8.236   1.780   5.203  1.00 10.00           O  
ATOM      6  N   ASN A   2      -7.622   3.238   3.060  1.00 10.00           N  
ATOM      7  CA  ASN A   2      -6.590   1.914   3.237  1.00 10.00           C  
ATOM      8  C   ASN A   2      -5.318   2.878   3.858  1.00 10.00           C  
ATOM      9  O   ASN A   2      -5.365   3.945   3.936  1.00 10.00           O  
ATOM     10  CB  ASN A   2      -5.853   2.413   1.389  1.00 10.00           C  
ATOM     11  CG  ASN A   2      -6.894   1.495   0.829  1.00 10.00           C  
ATOM     12  OD1 ASN A   2      -6.962   0.414   0.807  1.00 10.00           O  
ATOM     13  ND2 ASN A   2      -7.645   2.660   0.235  1.00 10.00           N  
ATOM     14  N   ASN A   3      -4.072   1.986   4.052  1.00 10.00           N  
ATOM     15  CA  ASN A   3      -3.346   2.405   4.945  1.00 10.00           C  
ATOM     16  C   ASN A   3      -1.584   1.504   4.409  1.00 10.00           C  
ATOM     17  O   ASN A   3      -1.629   0.775   4.037  1.00 10.00           O  
ATOM     18  CB  ASN A   3      -3.488   1.743   6.668  1.00 10.00           C  
ATOM     19  CG  ASN A   3      -2.429   2.335   7.530  1.00 10.00           C  
ATOM     20  OD1 ASN A   3      -1.942   3.238   7.987  1.00 10.00           O  
ATOM     21  ND2 ASN A   3      -1.118   1.421   7.372  1.00 10.00           N  
ATOM     22  N   GLN A   4      -1.131   2.920   4.224  1.00 10.00           N  
ATOM     23  CA  GLN A   4       0.610   2.145   3.632  1.00 10.00           C  
ATOM     24  C   GLN A   4       1.547   3.145   4.578  1.00 10.00           C  
ATOM     25  O   GLN A   4       1.938   3.844   4.510  1.00 10.00           O  
ATOM     26  CB  GLN A   4       0.755   2.485   1.976  1.00 10.00           C  
ATOM     27  CG  GLN A   4       1.951   2.215   1.533  1.00 10.00           C  
ATOM     28  CD  GLN A   4       1.595   2.663   0.197  1.00 10.00           C  
ATOM     29  OE1 GLN A   4       1.459   3.654  -0.188  1.00 10.00           O  
ATOM     30  NE2 GLN A   4       2.706   1.489  -0.611  1.00 10.00           N  
ATOM     31  N   GLN A   5       2.543   2.172   5.223  1.00 10.00           N  
ATOM     32  CA  GLN A   5       3.337   2.752   5.813  1.00 10.00           C  
ATOM     33  C   GLN A   5       5.017   1.913   5.547  1.00 10.00           C  
ATOM     34  O   GLN A   5       5.152   1.162   5.111  1.00 10.00           O  
ATOM     35  CB  GLN A   5       3.374   2.094   7.232  1.00 10.00           C  
ATOM     36  CG  GLN A   5       1.826   3.250   7.810  1.00 10.00           C  
ATOM     37  CD  GLN A   5       1.952   2.604   9.471  1.00 10.00           C  
ATOM     38  OE1 GLN A   5       1.900   1.543  10.123  1.00 10.00           O  
ATOM     39  NE2 GLN A   5       1.707   3.857  10.251  1.00 10.00           N  
ATOM     40  N   ASN A   6       5.438   3.205   4.878  1.00 10.00           N  
ATOM     41  CA  ASN A   6       7.054   2.535   4.879  1.00 10.00           C  
ATOM     42  C   ASN A   6       7.881   3.011   5.777  1.00 10.00           C  
ATOM     43  O   ASN A   6       8.440   4.183   5.780  1.00 10.00           O  
ATOM     44  CB  ASN A   6       7.233   3.318   3.556  1.00 10.00           C  
ATOM     45  CG  ASN A   6       6.058   3.196   2.318  1.00 10.00           C  
ATOM     46  OD1 ASN A   6       6.514   2.180   1.592  1.00 10.00           O  
ATOM     47  ND2 ASN A   6       4.990   3.696   1.865  1.00 10.00           N  
TER      48      ASN A   6                                                      
ATOM     47  N   TYR X   0      10.635   1.582   6.545  1.00 10.00           N  
ATOM     48  CA  TYR X   0      11.665   1.717   7.567  1.00 10.00           C  
ATOM     49  C   TYR X   0      13.059   1.641   6.953  1.00 10.00           C  
ATOM     50  O   TYR X   0      13.237   1.134   5.845  1.00 10.00           O  
ATOM     51  CB  TYR X   0      11.503   0.638   8.640  1.00 10.00           C  
ATOM     52  CG  TYR X   0      10.181   0.695   9.372  1.00 10.00           C  
ATOM     53  CD1 TYR X   0       9.090  -0.045   8.934  1.00 10.00           C  
ATOM     54  CD2 TYR X   0      10.023   1.488  10.501  1.00 10.00           C  
ATOM     55  CE1 TYR X   0       7.879   0.003   9.599  1.00 10.00           C  
ATOM     56  CE2 TYR X   0       8.816   1.543  11.173  1.00 10.00           C  
ATOM     57  CZ  TYR X   0       7.748   0.799  10.718  1.00 10.00           C  
ATOM     58  OH  TYR X   0       6.545   0.850  11.384  1.00 10.00           O  
ATOM     59  OXT TYR X   0      14.040   2.085   7.550  1.00 10.00           O  
TER      62      TYR X   0                                                      
ATOM     61  N   GLY B   1      -9.774  11.509   5.385  1.00 30.00           N  
ATOM     62  CA  GLY B   1      -9.683  10.144   4.557  1.00 30.00           C  
ATOM     63  C   GLY B   1      -8.474   9.623   4.570  1.00 30.00           C  
ATOM     64  O   GLY B   1      -8.306   8.679   5.018  1.00 30.00           O  
ATOM     65  N   ASN B   2      -7.698   9.438   3.350  1.00 30.00           N  
ATOM     66  CA  ASN B   2      -6.623   8.731   2.952  1.00 30.00           C  
ATOM     67  C   ASN B   2      -5.306   9.804   3.583  1.00 30.00           C  
ATOM     68  O   ASN B   2      -5.328  10.870   4.049  1.00 30.00           O  
ATOM     69  CB  ASN B   2      -6.282   8.832   1.303  1.00 30.00           C  
ATOM     70  CG  ASN B   2      -7.529   8.151   0.436  1.00 30.00           C  
ATOM     71  OD1 ASN B   2      -7.543   7.027   0.515  1.00 30.00           O  
ATOM     72  ND2 ASN B   2      -8.277   9.217  -0.129  1.00 30.00           N  
ATOM     73  N   ASN B   3      -4.453   8.737   4.086  1.00 30.00           N  
ATOM     74  CA  ASN B   3      -3.378   9.132   5.122  1.00 30.00           C  
ATOM     75  C   ASN B   3      -1.816   8.645   4.171  1.00 30.00           C  
ATOM     76  O   ASN B   3      -2.134   7.308   4.047  1.00 30.00           O  
ATOM     77  CB  ASN B   3      -3.355   8.584   6.179  1.00 30.00           C  
ATOM     78  CG  ASN B   3      -2.040   8.729   7.440  1.00 30.00           C  
ATOM     79  OD1 ASN B   3      -2.045   9.966   7.506  1.00 30.00           O  
ATOM     80  ND2 ASN B   3      -1.044   7.966   7.577  1.00 30.00           N  
ATOM     81  N   GLN B   4      -0.983   9.363   4.216  1.00 30.00           N  
ATOM     82  CA  GLN B   4       0.097   9.083   3.296  1.00 30.00           C  
ATOM     83  C   GLN B   4       1.280   9.989   4.345  1.00 30.00           C  
ATOM     84  O   GLN B   4       1.500  11.001   3.780  1.00 30.00           O  
ATOM     85  CB  GLN B   4       0.213   9.248   2.000  1.00 30.00           C  
ATOM     86  CG  GLN B   4       1.552   8.921   1.338  1.00 30.00           C  
ATOM     87  CD  GLN B   4       1.428   9.727  -0.048  1.00 30.00           C  
ATOM     88  OE1 GLN B   4       1.046  10.777  -0.647  1.00 30.00           O  
ATOM     89  NE2 GLN B   4       2.185   8.624  -1.287  1.00 30.00           N  
ATOM     90  N   GLN B   5       2.327   9.234   4.812  1.00 30.00           N  
ATOM     91  CA  GLN B   5       3.431   9.566   5.454  1.00 30.00           C  
ATOM     92  C   GLN B   5       4.912   9.052   5.277  1.00 30.00           C  
ATOM     93  O   GLN B   5       5.045   7.953   4.726  1.00 30.00           O  
ATOM     94  CB  GLN B   5       3.112   9.050   7.040  1.00 30.00           C  
ATOM     95  CG  GLN B   5       2.147   9.747   7.703  1.00 30.00           C  
ATOM     96  CD  GLN B   5       2.139   9.319   9.282  1.00 30.00           C  
ATOM     97  OE1 GLN B   5       1.689   8.276   9.722  1.00 30.00           O  
ATOM     98  NE2 GLN B   5       1.752   9.868   9.879  1.00 30.00           N  
ATOM     99  N   ASN B   6       5.799   9.716   4.509  1.00 30.00           N  
ATOM    100  CA  ASN B   6       7.014   9.442   3.935  1.00 30.00           C  
ATOM    101  C   ASN B   6       8.038   9.596   5.176  1.00 30.00           C  
ATOM    102  O   ASN B   6       8.331  11.086   5.267  1.00 30.00           O  
ATOM    103  CB  ASN B   6       7.032  10.590   3.047  1.00 30.00           C  
ATOM    104  CG  ASN B   6       6.063  10.117   1.965  1.00 30.00           C  
ATOM    105  OD1 ASN B   6       5.839   9.017   1.160  1.00 30.00           O  
ATOM    106  ND2 ASN B   6       5.324  10.993   1.852  1.00 30.00           N  
ATOM    107  N   TYR B   7       8.550   8.769   6.044  1.00 30.00           N  
ATOM    108  CA  TYR B   7       9.158   8.880   6.929  1.00 30.00           C  
ATOM    109  C   TYR B   7      10.653   8.680   6.347  1.00 30.00           C  
ATOM    110  O   TYR B   7      10.708   8.268   5.211  1.00 30.00           O  
ATOM    111  CB  TYR B   7       9.152   7.704   8.028  1.00 30.00           C  
ATOM    112  CG  TYR B   7       8.255   7.578   8.810  1.00 30.00           C  
ATOM    113  CD1 TYR B   7       6.877   6.785   8.367  1.00 30.00           C  
ATOM    114  CD2 TYR B   7       7.942   8.177   9.567  1.00 30.00           C  
ATOM    115  CE1 TYR B   7       5.541   7.180   9.177  1.00 30.00           C  
ATOM    116  CE2 TYR B   7       6.844   8.697  10.839  1.00 30.00           C  
ATOM    117  CZ  TYR B   7       5.640   7.754  10.193  1.00 30.00           C  
ATOM    118  OH  TYR B   7       4.304   7.708  10.876  1.00 30.00           O  
ATOM    119  OXT TYR B   7      11.870   9.519   6.532  1.00 30.00           O  
TER     122      TYR B   7                                                      
END
"""

def check_results(answer, refined, poor):
  asc = iotbx.pdb.input(
    file_name=answer).construct_hierarchy().atom_selection_cache()
  sel_moving = asc.selection(string = "not chain X")
  sel_fixed = asc.selection(string = "chain X")
  xrs_answer  = iotbx.pdb.input(file_name=answer).xray_structure_simple()
  xrs_refined = iotbx.pdb.input(file_name=refined).xray_structure_simple()
  xrs_poor = iotbx.pdb.input(file_name=poor).xray_structure_simple()
  # moving
  x1 = xrs_answer.select(sel_moving)
  x2 = xrs_refined.select(sel_moving)
  x3 = xrs_poor.select(sel_moving)
  d = x1.distances(x2)
  assert flex.max(d)<0.05, flex.max(d)
  d = x1.distances(x3)
  assert flex.max(d) > 0.7
  # fixed
  x1 = xrs_poor.select(sel_fixed)
  x2 = xrs_refined.select(sel_fixed)
  d = x1.distances(x2)
  # If below fails this means that sites excluded from xyz refinement 'chain X'
  # moved from their initial position.
  assert flex.max(d)<1.e-3, flex.max(d)

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  NCS constrained xyz refinement, NCS does not cover whole model,
  fix some model parts (defined by user)
  """
  # Answer PDB and data
  pdb_file_answer = "%s_answer.pdb"%prefix
  with open(pdb_file_answer, "w") as fo:
    fo.write(pdb_str_answer)
  #
  args = [
    pdb_file_answer,
    "file_name=%s.mtz"%prefix,
    "high_res=2.5",
    "type=real",
    "r_free=0.1"]
  r = run_fmodel(args = args, prefix = prefix)
  # PDB poor
  pdb_file_poor = "%s_poor.pdb"%prefix
  with open(pdb_file_poor, "w") as fo:
    fo.write(pdb_str_poor)
  # Go refine
  args = [
    pdb_file_poor,
    r.mtz,
    "xray_data.r_free_flags.ignore_r_free_flags=true",
    "ncs.type=constraints",
    "ncs_search.enabled=true",
    "strategy=individual_sites",
    "refine.sites.individual='not chain X' ",
    "main.bulk_sol=false",
    "main.number_of_mac=5",
    "main.target=ls",
    "nqh_flips=false",
    "pdb_interpretation.flip_symmetric_amino_acids=False",
    ]
  r = run_phenix_refine(args = args, prefix = prefix)
  # before checking results, make sure ncs restraints were found.
  assert_lines_in_file(file_name=r.log, lines="""Number of NCS groups: 1""")
  check_results(answer=pdb_file_answer, refined=r.pdb, poor=pdb_file_poor)
  assert r.r_work_start > 0.25
  assert r.r_work_final < 0.05

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
