from __future__ import print_function
from libtbx.test_utils import run_command
from libtbx.utils import search_for
import libtbx.load_env
import time
import sys, os
from mmtbx import utils
from phenix_regression.refinement import get_r_factors
from libtbx.test_utils import approx_equal
from libtbx import easy_run
import mmtbx.model
import iotbx.pdb

def check_for_expected_output(file_name, expected):
  for line in expected.splitlines():
    line = line.strip()
    found = False
    for line_f in open(file_name).read().splitlines():
      if (line == line_f.strip()):
        found = True
        break
    assert found

def exercise_01(verbose):
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/t.pdb", test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/1167B_peak_cut.sca",
    test=os.path.isfile)
  cmd = " ".join([
    'phenix.refine',
    '%s'%hkl,
    '%s'%pdb,
    'refine.strategy=individual_sites+individual_adp+occupancies',
    'refinement.input.xray_data.r_free_flags.generate=True',
    'main.number_of_macro_cycles=1',
    'output.prefix=tst_misc_exercise_01',
    '--overwrite > tst_misc_exercise_01_stdout'])
  print(cmd)
  run_command(
    command=cmd,
    log_file_name="tst_misc_exercise_01_001.log",
    stdout_file_name="tst_misc_exercise_01_stdout",
    result_file_names=["tst_misc_exercise_01_001.eff"],
    show_diff_log_stdout=True,
    verbose=verbose)
  lines = search_for(
    pattern="1167B_peak_cut.sca",
    mode="find",
    file_name="tst_misc_exercise_01_001.eff")
  assert len(lines) == 1
  lines = search_for(
    pattern='  labels = "I(+),SIGI(+),I(-),SIGI(-)"',
    mode="find",
    file_name="tst_misc_exercise_01_001.eff")
  assert len(lines) == 1

def exercise_02(verbose):
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/tom01.pdb", test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/tom01.mtz",
    test=os.path.isfile)
  params = """\
#phil __ON__
refinement {
  input {
    xray_data {
      labels = "FP,SIGFP"
      high_resolution = 4
      low_resolution = 500
      r_free_flags {
        label = "FreeR_flag"
        test_flag_value = 1
      }
    }
  }
  output {
    prefix = "test"
    serial = 1
    write_eff_file = False
    write_geo_file = False
    write_def_file = False
    export_final_f_model = true
    write_maps = False
  }
  main {
    ordered_solvent = True
    target = ml mlhl ml_sad *ls
    use_experimental_phases = False
    random_seed = 609463
  }
  pdb_interpretation.ncs_search.enabled=True
  ncs {
    special_position_warnings_only = True
  }
  ordered_solvent {
    low_resolution = 3
  }
  mask {
    ignore_zero_occupancy_atoms = False
  }
}
#phil __OFF__
  """
  task_file = open("tst_misc_exercise_02.params", "w").write(params)
  cmd = " ".join([
    'phenix.refine',
    '%s'%hkl,
    '%s'%pdb,
    'tst_misc_exercise_02.params',
    '--overwrite > tst_misc_exercise_02_stdout'])
  print(cmd)
  run_command(
    command=cmd,
    log_file_name="test_001.log",
    stdout_file_name="tst_misc_exercise_02_stdout",
    show_diff_log_stdout=True,
    verbose=verbose)

def exercise_03(verbose):
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/ygg_01.pdb", test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/ygg_peaks_and_holes.mtz",
    test=os.path.isfile)
  cmd = " ".join([
    'phenix.refine',
    '%s'%hkl,
    '%s'%pdb,
    'input.xray_data.outliers_rejection=false',
    'input.neutron_data.outliers_rejection=false',
    'main.number_of_macro_cycles=0',
    'main.bulk_s=false',
    'show_residual_map_peaks_and_holes=true',
    'output.prefix=tst_misc_exercise_03',
    '--overwrite > tst_misc_exercise_03_stdout'])
  run_command(
    command=cmd,
    log_file_name="tst_misc_exercise_03_001.log",
    stdout_file_name="tst_misc_exercise_03_stdout",
    show_diff_log_stdout=True,
    verbose=verbose)
  #
  look_peaks=False
  look_holes=False
  mapped_peaks = []
  mapped_holes = []
  for l in open("tst_misc_exercise_03_001.log", "r").readlines():
    l = l.strip()
    ls = l.split()
    if(l.startswith("----------peaks----------")):
      look_peaks=True
      look_holes=False
    if(l.startswith("----------holes----------")):
      look_peaks=False
      look_holes=True
    if(look_peaks and l.startswith("peak=")):
      mapped_peaks.append(float(ls[1]))
      mapped_peaks.append(float(ls[10]))
    if(look_holes and l.startswith("peak=")):
      mapped_holes.append(float(ls[1]))
      mapped_holes.append(float(ls[10]))
  print(mapped_peaks)
  assert approx_equal(mapped_peaks, [13.220, 0.956, 12.614, 0.952], 1.0)
  assert len(mapped_holes)==0

def exercise_04(verbose):
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/ygg_02.pdb", test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/ygg_peaks_and_holes.mtz",
    test=os.path.isfile)
  cmd = " ".join([
    'phenix.refine',
    '%s'%hkl,
    '%s'%pdb,
    'input.xray_data.outliers_rejection=false',
    'input.neutron_data.outliers_rejection=false',
    'main.number_of_macro_cycles=0',
    'main.bulk_s=false',
    'output.prefix=tst_misc_exercise_04',
    'show_residual_map_peaks_and_holes=true',
    '--overwrite > tst_misc_exercise_04_stdout'])
  run_command(
    command=cmd,
    log_file_name="tst_misc_exercise_04_001.log",
    stdout_file_name="tst_misc_exercise_04_stdout",
    show_diff_log_stdout=True,
    verbose=verbose)
  #
  look_peaks=False
  look_holes=False
  mapped_peaks = []
  mapped_holes = []
  for l in open("tst_misc_exercise_04_001.log", "r").readlines():
    l = l.strip()
    ls = l.split()
    if(l.startswith("----------peaks----------")):
      look_peaks=True
      look_holes=False
    if(l.startswith("----------holes----------")):
      look_peaks=False
      look_holes=True
    if(look_peaks and l.startswith("peak=")):
      mapped_peaks.append(float(ls[1]))
      mapped_peaks.append(float(ls[10]))
    if(look_holes and l.startswith("peak=")):
      mapped_holes.append(float(ls[1]))
      mapped_holes.append(float(ls[10]))
  print(mapped_peaks)
  assert approx_equal(mapped_peaks, [10.425, 1.162, 9.019, 2.411, 6.785, 1.445])
  assert len(mapped_holes)==0

def exercise_05(verbose):
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/ygg_03.pdb", test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/ygg_peaks_and_holes.mtz",
    test=os.path.isfile)
  cmd = " ".join([
    'phenix.refine',
    '%s'%hkl,
    '%s'%pdb,
    'main.number_of_macro_cycles=0',
    'input.xray_data.outliers_rejection=false',
    'input.neutron_data.outliers_rejection=false',
    'main.bulk_s=false',
    'output.prefix=tst_misc_exercise_05',
    'show_residual_map_peaks_and_holes=true',
    '--overwrite > tst_misc_exercise_05_stdout'])
  run_command(
    command=cmd,
    log_file_name="tst_misc_exercise_05_001.log",
    stdout_file_name="tst_misc_exercise_05_stdout",
    show_diff_log_stdout=True,
    verbose=verbose)
  #
  look_peaks=False
  look_holes=False
  mapped_peaks = []
  mapped_holes = []
  for l in open("tst_misc_exercise_05_001.log", "r").readlines():
    l = l.strip()
    ls = l.split()
    if(l.startswith("----------peaks----------")):
      look_peaks=True
      look_holes=False
    if(l.startswith("----------holes----------")):
      look_peaks=False
      look_holes=True
    if(look_peaks and l.startswith("peak=")):
      mapped_peaks.append(float(ls[1]))
      mapped_peaks.append(float(ls[10]))
    if(look_holes and l.startswith("peak=")):
      mapped_holes.append(float(ls[1]))
      mapped_holes.append(float(ls[10]))
  assert approx_equal(mapped_peaks, [16.81, 1.496, 4.918, 0.886], 1.0)
  assert len(mapped_holes)==0

def exercise_06(verbose):
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/arg_bad.pdb", test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/phe.mtz",
    test=os.path.isfile)
  cmd = " ".join([
    'phenix.refine',
    '%s'%hkl,
    '%s'%pdb,
    'ordered_solvent=True',
    '--overwrite > tst_misc_exercise_06_stdout'])
  run_command(
    command=cmd,
    log_file_name="arg_bad_refine_001.log",
    stdout_file_name="tst_misc_exercise_06_stdout",
    show_diff_log_stdout=True,
    verbose=verbose)

def exercise_07(verbose):
  params = """\
#phil __ON__
refinement {
  main {
    ordered_solvent = True
    number_of_macro_cycles = 1
    bulk_solvent_and_scale=false
  }
  ordered_solvent {
    mode = *every_macro_cycle
    low_resolution = 9
    b_iso_min = 1
    b_iso_max = 999
    primary_map_type = mFobs-DFmodel
    primary_map_cutoff = 1
    secondary_map_and_map_cc_filter
    {
      cc_map_2_type = None
    }
  }
  peak_search {
    map_next_to_model {
      min_model_peak_dist = 0.5
      max_model_peak_dist = 4
      min_peak_peak_dist = 0.5
    }
  }
}
refinement.input.xray_data.r_free_flags.generate=True
#phil __OFF__
  """
  open("jf.params", "w").write(params)
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/jf.pdb", test=os.path.isfile)
  cmd = " ".join([
    'phenix.fmodel',
    '%s'%pdb,
    'high_resolution=1.69',
    'label=FOBS',
    'type=real',
    'output.file_name=jf.mtz',
    '> tst_misc_exercise_07a_stdout'])
  run_command(
    command=cmd,
    stdout_file_name="tst_misc_exercise_07a_stdout",
    verbose=verbose)
  for opts in [("adp.individual.iso=None","adp.individual.aniso='%s'"%"chain d or chain g"),
               ("adp.individual.iso='%s'"%"chain d or chain g","adp.individual.aniso=None")]:
    cmd = " ".join([
      'phenix.refine',
      'jf.mtz',
      'jf.params',
      '%s'%pdb,
      '%s'%opts[0],
      '%s'%opts[1],
      'refine.occupancies.individual="%s"'%"chain d",
      '--overwrite > tst_misc_exercise_07b_stdout'])
    print(cmd)
    run_command(
      command=cmd,
      log_file_name="jf_refine_001.log",
      stdout_file_name="tst_misc_exercise_07b_stdout",
      result_file_names=["jf_refine_001.pdb"],
      show_diff_log_stdout=True,
      verbose=verbose)

def exercise_08(verbose):
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/lys_hoh_with_h.pdb", test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/lysozyme.pickle",
    test=os.path.isfile)
  cmd = " ".join([
    'phenix.refine',
    '%s'%hkl,
    '%s'%pdb,
    'ordered_solvent=True',
    'ordered_solvent.low_resolution=10',
    'xray_data.high_res=3',
    'output.prefix=tst_misc_08a',
    'hydrogens.contribute_to_f_calc=True',
    'strategy=individual_sites',
    'main.number_of_macro=1',
    'hydrogens.refine=individual',
    'main.bulk_sol=false',
    'show_residual_map_peaks_and_holes=false',
    'main.max_number_of_iter=25',
    '--overwrite > tst_misc_08a_stdout'])
  run_command(
    command=cmd,
    log_file_name="tst_misc_08a_001.log",
    stdout_file_name="tst_misc_08a_stdout",
    result_file_names=["tst_misc_08a_001.pdb"],
    show_diff_log_stdout=True,
    verbose=verbose)
  cmd = " ".join([
    'phenix.refine',
    '%s'%hkl,
    '%s'%'tst_misc_08a_001.pdb',
    'ordered_solvent=True',
    'ordered_solvent.low_resolution=10',
    'xray_data.high_res=8',
    'output.prefix=tst_misc_08b',
    'hydrogens.contribute_to_f_calc=True',
    'strategy=individual_sites',
    'main.number_of_macro=1',
    'hydrogens.refine=individual',
    'main.bulk_sol=false',
    'show_residual_map_peaks_and_holes=false',
    'main.max_number_of_iter=25',
    '--overwrite > tst_misc_08b_stdout'])
  run_command(
    command=cmd,
    log_file_name="tst_misc_08b_001.log",
    stdout_file_name="tst_misc_08b_stdout",
    result_file_names=["tst_misc_08b_001.pdb"],
    show_diff_log_stdout=True,
    verbose=verbose)

def exercise_09(verbose):
  params = """\
#phil __ON__
refinement.electron_density_maps {
    map {
      map_type = 2Fo-Fc
    }
    map {
      map_type = mFo-DFc
    }
    map_coefficients {
      mtz_label_amplitudes = 2FOFCWT
      mtz_label_phases = PH2FOFCWT
      map_type = 2Fo-Fc
    }
    map_coefficients {
      mtz_label_amplitudes = FOFCWT
      mtz_label_phases = PHFOFCWT
      map_type = mFo-DFc
    }
  }
#phil __OFF__
  """
  open("exercise_09.params", "w").write(params)
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/ygg_01.pdb", test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/ygg_peaks_and_holes.mtz",
    test=os.path.isfile)
  cmd = " ".join([
    'phenix.refine',
    '%s'%hkl,
    '%s'%pdb,
    'main.number_of_macro_cycles=0',
    'main.bulk_s=false',
    'exercise_09.params',
    'output.prefix=tst_misc_exercise_09',
    '--overwrite > tst_misc_exercise_09_stdout'])
  run_command(
      command=cmd,
      log_file_name="tst_misc_exercise_09_001.log",
      stdout_file_name="tst_misc_exercise_09_stdout",
      show_diff_log_stdout=True,
      verbose=verbose,
      sorry_expected=True)

def exercise_10(verbose):
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/outside_4.pdb", test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/outside_4_exptl_fobs_phases_freeR_flags.mtz",
    test=os.path.isfile)
  # case a
  cmd = " ".join([
    'phenix.refine',
    '%s'%hkl,
    '%s'%pdb,
    'output.prefix=tst_misc_exercise_10a',
    '--overwrite > tst_misc_exercise_10a_stdout'])
  print(cmd)
  run_command(
      command=cmd,
      log_file_name="tst_misc_exercise_10a_stdout",
      stdout_file_name="tst_misc_exercise_10a_stdout",
      show_diff_log_stdout=True,
      verbose=verbose,
      sorry_expected=True)
  check_for_expected_output(
    file_name="tst_misc_exercise_10a_stdout", expected="""\
Sorry: All occupancies in input PDB file are zero.
""")
  # case b
  cmd = " ".join([
    'phenix.refine',
    '%s'%hkl,
    '%s'%pdb,
    'modify_start_model.modify.occupancies.set=1',
    'strategy=none main.number_of_macro_cycles=0',
    'output.prefix=tst_misc_exercise_10b',
    '--overwrite > tst_misc_exercise_10b_stdout'])
  print(cmd)
  run_command(
      command=cmd,
      log_file_name="tst_misc_exercise_10b_001.log",
      stdout_file_name="tst_misc_exercise_10b_stdout",
      show_diff_log_stdout=True,
      verbose=verbose,
      sorry_expected=False)

def exercise_11(verbose):
  # prevent accidental individual aniso ADP refinement at low resolution
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/phe_e.pdb", test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/phe.mtz",
    test=os.path.isfile)
  cmd = " ".join([
    'phenix.refine',
    '%s'%hkl,
    '%s'%pdb,
    'output.prefix=tst_misc_exercise_11',
    'strategy=individual_adp+tls',
    'main.number_of_macro=1 xray_data.high_res=2',
    'main.bulk_sol=false',
    'adp.individual.aniso="%s"'%"name CZ and resseq 2",
    'adp.tls="%s"'%"chain A",
    '--overwrite > tst_misc_exercise_11_stdout'])
  run_command(
    command=cmd,
    log_file_name="tst_misc_exercise_11_001.log",
    stdout_file_name="tst_misc_exercise_11_stdout",
    show_diff_log_stdout=True,
    verbose=verbose)
  model = mmtbx.model.manager(
      model_input = iotbx.pdb.input(file_name="tst_misc_exercise_11_001.pdb"),
      process_input = True)
  selections = []
  for sel_s in ["chain A", "chain B", "chain C"]:
    selections.append(utils.get_atom_selections(
      model = model,
      selection_strings     = sel_s,
      iselection            = False,
      one_group_per_residue = False,
      hydrogens_only        = False,
      one_selection_array   = True))
  res1 = model.get_xray_structure().select(selections[0]).use_u_aniso()
  assert res1.count(True) == res1.size()
  res2 = model.get_xray_structure().select(selections[1]).use_u_aniso()
  assert res2.count(True) == 1
  res3 = model.get_xray_structure().select(selections[2]).use_u_aniso()
  assert res3.count(True) == 0

def exercise_12(verbose):
  # prevent accidental individual aniso ADP refinement at low resolution
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/phe_e.pdb", test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/phe.mtz",
    test=os.path.isfile)
  cmd = " ".join([
    'phenix.refine',
    '%s'%hkl,
    '%s'%pdb,
    'output.prefix=tst_misc_exercise_12',
    'main.number_of_macro=1 xray_data.high_res=2',
    'main.bulk_sol=false',
    '--overwrite > tst_misc_exercise_12_stdout'])
  run_command(
    command=cmd,
    log_file_name="tst_misc_exercise_12_001.log",
    stdout_file_name="tst_misc_exercise_12_stdout",
    show_diff_log_stdout=True,
    verbose=verbose)
  processed_pdb_files_srv = utils.process_pdb_file_srv()
  processed_pdb_file, pdb_inp = processed_pdb_files_srv.process_pdb_files(
    pdb_file_names = ["tst_misc_exercise_12_001.pdb"])
  all_chain_proxies = processed_pdb_file.all_chain_proxies
  xray_structure = processed_pdb_file.xray_structure(show_summary = False)
  res = xray_structure.use_u_aniso()
  assert res.count(True) == 0

def exercise_13(verbose):
  # prevent accidental individual aniso ADP refinement at low resolution
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/phe_e.pdb", test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/phe.mtz",
    test=os.path.isfile)
  cmd = " ".join([
    'phenix.refine',
    '%s'%hkl,
    '%s'%pdb,
    'output.prefix=tst_misc_exercise_13',
    'main.number_of_macro=1 xray_data.high_res=2',
    'main.bulk_sol=false',
    'adp.individual.iso="%s"'%"chain A",
    'adp.individual.aniso="%s"'%"chain B",
    '--overwrite > tst_misc_exercise_13_stdout'])
  run_command(
    command=cmd,
    log_file_name="tst_misc_exercise_13_001.log",
    stdout_file_name="tst_misc_exercise_13_stdout",
    show_diff_log_stdout=True,
    verbose=verbose)
  model = mmtbx.model.manager(
      model_input = iotbx.pdb.input(file_name="tst_misc_exercise_13_001.pdb"),
      process_input = True)
  selections = []
  for sel_s in ["chain A", "chain B", "chain C"]:
    selections.append(utils.get_atom_selections(
      model = model,
      selection_strings     = sel_s,
      iselection            = False,
      one_group_per_residue = False,
      hydrogens_only        = False,
      one_selection_array   = True))
  res1 = model.get_xray_structure().select(selections[0]).use_u_aniso()
  assert res1.count(False) == res1.size()
  res2 = model.get_xray_structure().select(selections[1]).use_u_aniso()
  assert res2.count(True) == res2.size()
  res3 = model.get_xray_structure().select(selections[2]).use_u_aniso()
  assert res3.count(True) == 0

def exercise_14(verbose):
  # make sure gzipped PDB files are good input for phenix.refine
  pdb1 = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/t_part1.pdb.gz", test=os.path.isfile)
  pdb2 = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/t_part2.pdb.gz", test=os.path.isfile)
  pdb3 = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/t.pdb", test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/t.mtz",
    test=os.path.isfile)
  cmd1 = " ".join([
    'phenix.refine',
    '%s'%hkl,
    '%s'%pdb1,
    '%s'%pdb2,
    'output.prefix=tst_misc_exercise_14_1',
    'main.number_of_macro=0 strategy=none',
    '--overwrite > tst_misc_exercise_14_1_stdout'])
  cmd2 = " ".join([
    'phenix.refine',
    '%s'%hkl,
    '%s'%pdb3,
    'output.prefix=tst_misc_exercise_14_2',
    'main.number_of_macro=0 strategy=none',
    '--overwrite > tst_misc_exercise_14_2_stdout'])
  print(cmd1)
  run_command(
    command=cmd1,
    log_file_name="tst_misc_exercise_14_1_001.log",
    stdout_file_name="tst_misc_exercise_14_1_stdout",
    show_diff_log_stdout=True,
    verbose=verbose)
  print(cmd2)
  run_command(
    command=cmd2,
    log_file_name="tst_misc_exercise_14_2_001.log",
    stdout_file_name="tst_misc_exercise_14_2_stdout",
    show_diff_log_stdout=True,
    verbose=verbose)
  r_factors1 = get_r_factors(file_name = "tst_misc_exercise_14_1_001.pdb")
  r_factors2 = get_r_factors(file_name = "tst_misc_exercise_14_2_001.pdb")
  assert approx_equal(r_factors1.r_work_start, r_factors2.r_work_start)
  assert approx_equal(r_factors1.r_free_start, r_factors2.r_free_start)
  assert approx_equal(r_factors1.r_work_final, r_factors2.r_work_final)
  assert approx_equal(r_factors1.r_free_final, r_factors2.r_free_final)

def exercise_15(verbose):
  # do not crash is bulk solvent mask is all empty (no solvent)
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/ur0013.pdb", test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/ur0013.sf.mtz",
    test=os.path.isfile)
  cif = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/cif_files/elbow.ur0013_ent.all.001.cif",
    test=os.path.isfile)
  cmd = " ".join([
    'phenix.refine',
    '%s'%hkl,
    '%s'%pdb,
    '%s'%cif,
    'output.prefix=tst_misc_exercise_15',
    'main.number_of_macro=1 strategy=none wu=0 wc=0',
    'xray_data.r_free_flags.generate=True main.scattering_table=neutron',
    '--overwrite > tst_misc_exercise_15_stdout'])
  run_command(
    command=cmd,
    log_file_name="tst_misc_exercise_15_001.log",
    stdout_file_name="tst_misc_exercise_15_stdout",
    show_diff_log_stdout=True,
    verbose=verbose)

def exercise_16(verbose):
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/pdb1akg_very_well_phenix_refined_001.pdb",
    test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/1akg.mtz",
    test=os.path.isfile)
  cmd = " ".join([
    'phenix.refine',
    '%s'%hkl,
    '%s'%pdb,
    'output.prefix=tst_misc_exercise_16',
    'main.number_of_macro=1 strategy=individual_adp',
    'main.max_number_of_iterations=5',
    'ordered_solvent=True',
    'ordered_solvent.new_solvent=anisotropic',
    'write_maps=false write_map_coefficients=false write_geo_file=false',
    'adp.iso.max_number_of_iterations=5',
    'xray_data.high_res=2.78',
    '--overwrite > tst_misc_exercise_16_stdout'])
  run_command(
    command=cmd,
    log_file_name="tst_misc_exercise_16_001.log",
    stdout_file_name="tst_misc_exercise_16_stdout",
    show_diff_log_stdout=True,
    verbose=verbose)

def exercise_17(verbose):
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/pdb1akg_very_well_phenix_refined_001.pdb",
    test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/1akg.mtz",
    test=os.path.isfile)
  cmd = " ".join([
    'phenix.refine',
    '%s'%hkl,
    '%s'%pdb,
    'output.prefix=tst_misc_exercise_17',
    'strategy=rigid_body',
    'sites.rigid_body=all',
    'write_maps=false write_map_coefficients=false write_geo_file=false',
    '--overwrite > tst_misc_exercise_17_stdout'])
  run_command(
    command=cmd,
    log_file_name="tst_misc_exercise_17_001.log",
    stdout_file_name="tst_misc_exercise_17_stdout",
    show_diff_log_stdout=True,
    verbose=verbose)
  easy_run.call("iotbx.phil tst_misc_exercise_17_001.eff tst_misc_exercise_17_002.def --diff > exercise_17.log")
  for line in open("exercise_17.log", "r"):
    if(line.count("bulk_solvent_and_scale")):
      assert 0

def exercise_18(verbose):
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/pdb1akg_very_well_phenix_refined_001.pdb",
    test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/1akg.mtz",
    test=os.path.isfile)
  cmd = " ".join([
    'phenix.refine',
    '%s'%hkl,
    '%s'%pdb,
    'output.prefix=tst_misc_exercise_18',
    'use_geometry_restraints=false xray_data.high_res=5',
    'strategy=individual_sites+individual_adp+group_adp+tls+rigid_body+occupancies',
    '--overwrite > tst_misc_exercise_18_stdout'])
  run_command(
    command=cmd,
    log_file_name="tst_misc_exercise_18_001.log",
    stdout_file_name="tst_misc_exercise_18_stdout",
    show_diff_log_stdout=True,
    verbose=verbose)

def exercise_19(verbose=None):
  cmds = [
  "phenix.list",
  "phenix.list --help",
  "phenix.list refine",
  "phenix.list show_ignore",
  "phenix.list check"
  ]
  for cmd in cmds:
    assert (easy_run.fully_buffered(cmd).return_code == 0)

def exercise_20(verbose):
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/pdb1akg_very_well_phenix_refined_001.pdb",
    test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/1akg.mtz",
    test=os.path.isfile)
  cmd = " ".join([
    'phenix.refine',
    '%s'%hkl,
    '%s'%pdb,
    'main.use_geometry_restraints=False',
    'output.prefix=tst_misc_exercise_20',
    'xray_data.high_res=5',
    '--overwrite > tst_misc_exercise_20_stdout'])
  run_command(
    command=cmd,
    log_file_name="tst_misc_exercise_20_001.log",
    stdout_file_name="tst_misc_exercise_20_stdout",
    show_diff_log_stdout=True,
    verbose=verbose)

def run_exercise (idx) :
  method_name = "exercise_%02d" % idx
  method = globals().get(method_name)
  print(method_name)
  assert (hasattr(method, "__call__"))
  method(verbose=False)

def run (args) :
  verbose = "--verbose" in args
  if (verbose) :
    args.remove("--verbose")
  t0 = time.time()
  if (len(args) == 1) :
    idx = int(args[0])
    run_exercise(idx)
  else :
    for idx in range(1, 20) :
      run_exercise(idx)
  print("OK Time: %8.3f"%(time.time()-t0))

if (__name__ == "__main__"):
  run(sys.argv[1:])
