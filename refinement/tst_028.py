from __future__ import division
from __future__ import print_function
import libtbx.load_env
import os
from phenix_regression.refinement import run_phenix_refine

def run(prefix = os.path.basename(__file__).replace(".py","")):
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/mmtbx/bulk_solvent_and_scaling/pdb1l2h.ent",
    test=os.path.isfile)
  mtz_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/mmtbx/bulk_solvent_and_scaling/1l2h.mtz",
    test=os.path.isfile)
  args = [
    pdb_file,
    mtz_file,
    'xray_data.high_res=2.5',
    'main.number_of_mac=3',
    'refine.strategy=individual_sites+individual_adp+occupancies',
    'xray_data.twin_law="h,-k,-l"']
  r = run_phenix_refine(args = args, prefix = prefix)
  assert r.r_work_start<0.1450
  assert r.r_free_start<0.1780
  assert r.r_work_final<0.1380
  assert r.r_free_final<0.2085

if(__name__ == "__main__"):
  run()
  print("OK")
