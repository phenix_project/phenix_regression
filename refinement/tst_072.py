from __future__ import division
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel
import os
import iotbx.pdb
from libtbx import easy_run

pdb_file_str="""\
CRYST1   16.092   17.781   18.439  90.00  90.00  90.00 P 1           0
ATOM      1  N  CMET A  29       5.350  10.405   7.080  0.70 10.72           N
ATOM      2  CA CMET A  29       5.742   9.130   7.676  0.70  9.05           C
ATOM      3  C  CMET A  29       6.072   9.258   9.157  0.70  7.83           C
ATOM      4  O  CMET A  29       5.824   8.336   9.938  0.70  7.92           O
ATOM      5  CB CMET A  29       6.944   8.550   6.933  0.70  9.09           C

ATOM      1  N  DMET A  29       5.350  10.405   7.080  0.30 10.72           N
ATOM      2  CA DMET A  29       5.742   9.130   7.676  0.30  9.05           C
ATOM      3  C  DMET A  29       6.072   9.258   9.157  0.30  7.83           C
ATOM      4  O  DMET A  29       5.824   8.336   9.938  0.30  7.92           O
ATOM      5  CB DMET A  29       6.944   8.550   6.933  0.30  9.09           C

ATOM      9  N   MET A  30       6.653  10.386   9.547  1.00  6.77           N
ATOM     10  CA  MET A  30       6.904  10.640  10.966  1.00  7.66           C
ATOM     11  C   MET A  30       5.561  10.665  11.727  1.00  7.62           C
ATOM     12  O   MET A  30       5.468  10.153  12.846  1.00  9.05           O
ATOM     13  CB  MET A  30       7.700  11.928  11.150  1.00  7.65           C
ATOM      0  H   MET A  30       6.908  11.012   9.016  1.00  6.77           H

TER
END
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Occupancy of H in refinement.
  """
  # write out a good model
  pdb_file_name = "%s.pdb"%prefix
  open(pdb_file_name,"w").write(pdb_file_str)
  # compute Fobs
  args = [
    pdb_file_name,
    "high_res=3.5",
    "r_fre=0.1",
    "type=real",
    "label=f-obs"]
  r = run_fmodel(args = args, prefix = prefix)
  # shake occupancies
  pdb_file_name_poor = "%s_poor.pdb"%prefix
  cmd = " ".join([
    "phenix.pdbtools %s"%pdb_file_name,
    "suffix=none",
    "output.prefix=%s"%pdb_file_name_poor.replace(".pdb",""),
    "occupancies.randomize=true",
    "modify.selection='altloc C or altloc D'",
    "> tmp_occ_of_h5.log"
    ])
  assert not easy_run.call(cmd)
  # run phenix.refine
  args = [
    r.mtz,
    pdb_file_name_poor,
    "strategy=occupancies",
    "main.number_of_macro_cycles=2"]
  r = run_phenix_refine(args = args, prefix=prefix)
  # check results
  p=iotbx.pdb.input(file_name = r.pdb)
  h=p.construct_hierarchy()
  x = p.xray_structure_simple()
  o = x.scatterers().extract_occupancies()
  sel_H = h.atom_selection_cache().selection(string = "element H")
  assert sel_H.count(True) == 1
  o_H = o.select(sel_H)
  assert o_H.all_eq(1)

if (__name__ == "__main__"):
  run()
