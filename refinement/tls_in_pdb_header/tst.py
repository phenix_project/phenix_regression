from __future__ import print_function
from libtbx import easy_run
import libtbx.load_env
from scitbx.array_family import flex
import time, os
import iotbx.pdb
import mmtbx.tls.tools
from libtbx.test_utils import approx_equal

def get_tls_selections(file_name):
  pdb_inp = iotbx.pdb.input(file_name = file_name)
  pdb_inp_tls = mmtbx.tls.tools.tls_from_pdb_inp(
    remark_3_records = pdb_inp.extract_remark_iii_records(3),
    pdb_hierarchy = pdb_inp.construct_hierarchy())
  result = []
  for t in pdb_inp_tls.tls_params:
    result.append(t.selection_string)
  return result

def run():
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/tls_in_pdb_header/m1.pdb",
    test=os.path.isfile)
  cmd = " ".join([
    "phenix.fmodel",
    "%s"%pdb_file,
    "high_res=10",
    "type=real",
    "label=f-obs",
    "r_free=0.1",
    "output.file_name=m1_data.mtz",
    "> tmp_tls_in_header.log"
  ])
  assert (easy_run.call(cmd) == 0)
  # case 1
  cmd = " ".join([
    "phenix.refine",
    "%s %s"%(pdb_file, "m1_data.mtz"),
    "main.bulk_sol=false",
    "main.number_of_mac=1",
    "output.prefix=c1",
    "strategy=tls", # XXX temporarily added for 1.7.3 release
    "--overwrite --quiet"
  ])
  print(cmd)
  assert (easy_run.call(cmd) == 0)
  s1 = get_tls_selections(file_name=pdb_file)
  s2 = get_tls_selections(file_name="c1_001.pdb")
  assert len(s1) == len(s2)
  assert len(s1) == 2
  for s1_,s2_ in zip(s1,s2):
   assert s1_==s2_
  # case 2:
  cmd = " ".join([
    "phenix.refine",
    "%s %s"%(pdb_file, "m1_data.mtz"),
    "main.bulk_sol=false",
    "main.number_of_mac=1",
    "output.prefix=c2",
    "strategy=tls",
    "tls='chain A'",
    "--overwrite --quiet"
  ])
  assert (easy_run.call(cmd) == 0)
  s2 = get_tls_selections(file_name="c2_001.pdb")
  assert s2 == ['chain A']
  # case 3:
  for i_run, fn in enumerate(["m2.pdb", "m3.pdb"]):
    i_run = i_run+3
    pdb_file = libtbx.env.find_in_repositories(
      relative_path="phenix_regression/refinement/tls_in_pdb_header/%s"%fn,
      test=os.path.isfile)
    cmd = " ".join([
      "phenix.refine",
      "%s %s"%(pdb_file, "m1_data.mtz"),
      "main.bulk_sol=false",
      "main.number_of_mac=1",
      "output.prefix=c%s"%str(i_run),
      "--overwrite --quiet"
    ])
    assert (easy_run.call(cmd) == 0)
    s2 = get_tls_selections(file_name="c%s_001.pdb"%str(i_run))
    assert len(s2) == 0

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("Time: %6.3f"%(time.time()-t0))
