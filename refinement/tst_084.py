from __future__ import division
from __future__ import print_function
from phenix_regression.refinement import run_phenix_refine
import libtbx.load_env
import os
import iotbx.pdb
from libtbx.test_utils import approx_equal

par_str="""\
refinement.main.bulk_solvent_and_scale=false
refinement.main.number_of_macro_cycles=20
refinement.main.scattering_table=wk1995
refinement.structure_factors_and_gradients_accuracy.algorithm=direct
refinement.fake_f_obs.structure_factors_accuracy.algorithm=direct
refinement.refine.strategy=rigid_body
refinement.refine.sites.rigid_body=chain B
refinement.refine.sites.rigid_body=chain A
refinement.refine.sites.rigid_body=chain D
refinement.refine.sites.rigid_body=chain C
refinement.main.target=ls
refinement.rigid_body.bulk_solvent_and_scale=false
refinement.main.fake_f_obs=true
data_manager.fmodel.xray_data.high_resolution=1.5
refinement.modify_start_model.modify.sites.translate=1.0 0 0
refinement.modify_start_model.output.file_name=%s
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Rigid-body refinement.
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/enk_rbr.pdb", test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/enk_rbr.pdb_fcalc_direct_wk1995_resolution_1.0.hkl", \
    test=os.path.isfile)
  #
  par = "%s.params"%prefix
  pdb_mod = "%s_mod.pdb"%prefix
  open(par, "w").write(par_str%pdb_mod)
  args = [pdb, hkl, par]
  r = run_phenix_refine(args = args, prefix=prefix)
  #
  f1 = iotbx.pdb.input(pdb)
  h1 = f1.construct_hierarchy()
  h1.atoms().reset_i_seq()
  f2 = iotbx.pdb.input(r.pdb)
  h2 = f2.construct_hierarchy()
  h2.atoms().reset_i_seq()
  f3 = iotbx.pdb.input(pdb_mod)
  h3 = f3.construct_hierarchy()
  h3.atoms().reset_i_seq()
  for atom in h1.atoms() :
    atom2 = h2.atoms()[atom.i_seq]
    atom3 = h3.atoms()[atom.i_seq]
    labels = atom.fetch_labels()
    if (labels.resname == "HOH") :
      assert (atom2.xyz == atom3.xyz)
    else :
      assert approx_equal((atom3.xyz[0] - atom.xyz[0]), 1.0)
      for k in range(3) :
        assert (abs(atom2.xyz[k] - atom.xyz[k]) < 0.4)

if (__name__ == "__main__"):
  run()
  print("OK")
