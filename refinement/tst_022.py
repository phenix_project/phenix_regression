from __future__ import division
from __future__ import print_function
import iotbx.pdb
from phenix_regression.refinement import run_phenix_refine
import os
from libtbx.test_utils import assert_lines_in_file

pdb_str = """

CRYST1   30.832   38.293   32.211  90.00  90.00  90.00 C 2 2 21
ATOM      1  N   GLY A   1      42.375 -12.180  24.780  1.00 35.31      2MLT 1
ATOM      2  CA  GLY A   1      43.603 -11.488  24.325  1.00 35.57      2MLT 1
ATOM      3  C   GLY A   1      43.288 -10.171  23.615  1.00 34.64      2MLT 1
ATOM      4  O   GLY A   1      42.111  -9.896  23.277  1.00 35.82      2MLT 1
ATOM      5  N   ILE A   2      44.323  -9.391  23.299  1.00 32.23      2MLT 1
ATOM      6  CA  ILE A   2      44.200  -8.183  22.475  1.00 27.55      2MLT 1
ATOM      7  C   ILE A   2      43.750  -8.629  21.119  1.00 24.92      2MLT 1
ATOM      8  O   ILE A   2      43.068  -7.904  20.409  1.00 23.73      2MLT 1
ATOM      9  CB  ILE A   2      45.525  -7.320  22.425  1.00 30.10      2MLT 1
ATOM     10  CG1 ILE A   2      45.924  -6.837  23.820  1.00 29.64      2MLT 1
ATOM     11  CG2 ILE A   2      45.555  -6.173  21.386  1.00 30.54      2MLT 1
ATOM     12  CD1 ILE A   2      44.837  -6.338  24.762  1.00 32.44      2MLT 1
ATOM     13  N   GLY A   3      44.161  -9.867  20.749  1.00 22.69      2MLT 1
ATOM     14  CA  GLY A   3      43.999 -10.264  19.329  1.00 21.05      2MLT 1
ATOM     15  C   GLY A   3      42.433 -10.405  19.166  1.00 22.08      2MLT 1
ATOM     16  O   GLY A   3      41.912 -10.061  18.096  1.00 22.86      2MLT 1
ATOM     17  N   ALA A   4      41.862 -10.961  20.191  1.00 21.60      2MLT 1
ATOM     18  CA  ALA A   4      40.378 -11.260  20.106  1.00 21.80      2MLT 1
ATOM     19  C   ALA A   4      39.584  -9.950  20.087  1.00 21.67      2MLT 1
ATOM     20  O   ALA A   4      38.676  -9.747  19.278  1.00 22.21      2MLT 1
ATOM     21  CB  ALA A   4      40.061 -12.080  21.350  1.00 22.97      2MLT 1
ATOM     22  N   VAL A   5      39.936  -9.001  20.956  1.00 22.13      2MLT 1
ATOM     23  CA  VAL A   5      39.355  -7.658  21.083  1.00 19.34      2MLT 1
ATOM     24  C   VAL A   5      39.536  -6.896  19.795  1.00 18.81      2MLT 1
ATOM     25  O   VAL A   5      38.626  -6.314  19.126  1.00 17.22      2MLT 1
ATOM     26  CB  VAL A   5      39.843  -6.933  22.338  1.00 20.40      2MLT 1
ATOM     27  CG1 VAL A   5      39.237  -5.519  22.413  1.00 23.06      2MLT 1
ATOM     28  CG2 VAL A   5      39.745  -7.587  23.653  1.00 21.67      2MLT 1
ATOM     29  N   LEU A   6      40.752  -7.021  19.222  1.00 16.05      2MLT 1
ATOM     30  CA  LEU A   6      41.062  -6.432  17.957  1.00 17.59      2MLT 1
ATOM     31  C   LEU A   6      40.230  -6.935  16.870  1.00 20.01      2MLT 1
ATOM     32  O   LEU A   6      39.649  -6.121  16.029  1.00 21.90      2MLT 1
ATOM     33  CB  LEU A   6      42.627  -6.461  17.880  1.00 24.58      2MLT 1
ATOM     34  CG  LEU A   6      43.125  -6.023  16.524  1.00 23.91      2MLT 1
ATOM     35  CD1 LEU A   6      42.706  -4.584  16.210  1.00 27.44      2MLT 1
ATOM     36  CD2 LEU A   6      44.669  -6.152  16.638  1.00 29.31      2MLT 1
ATOM     37  N   LYS A   7      39.981  -8.229  16.721  1.00 19.83      2MLT 1
ATOM     38  CA  LYS A   7      39.079  -8.646  15.636  1.00 22.55      2MLT 1
ATOM     39  C   LYS A   7      37.648  -8.063  15.784  1.00 19.04      2MLT 1
ATOM     40  O   LYS A   7      37.031  -7.839  14.731  1.00 21.18      2MLT 1
ATOM     41  CB  LYS A   7      38.854 -10.176  15.616  1.00 27.62      2MLT 1
ATOM     42  CG  LYS A   7      40.011 -10.993  15.144  1.00 40.15      2MLT 1
ATOM     43  CD  LYS A   7      39.691 -12.487  15.325  1.00 47.84      2MLT 1
ATOM     44  CE  LYS A   7      40.599 -13.394  14.493  1.00 53.11      2MLT 1
ATOM     45  NZ  LYS A   7      39.966 -14.755  14.319  1.00 55.47      2MLT 1
ATOM     46  N   VAL A   8      37.111  -7.988  16.981  1.00 19.69      2MLT 1
ATOM     47  CA  VAL A   8      35.792  -7.369  17.211  1.00 20.52      2MLT 1
ATOM     48  C   VAL A   8      35.776  -5.881  16.885  1.00 20.31      2MLT 1
ATOM     49  O   VAL A   8      34.775  -5.402  16.257  1.00 20.12      2MLT 1
ATOM     50  CB  VAL A   8      35.113  -7.600  18.562  1.00 23.09      2MLT 1
ATOM     51  CG1 VAL A   8      34.774  -9.045  18.851  1.00 24.22      2MLT 1
ATOM     52  CG2 VAL A   8      35.769  -6.970  19.726  1.00 27.95      2MLT 1
BREAK
ATOM     53  N   THR A  11      35.315  -5.870  13.113  1.00 21.12      2MLT 1
ATOM     54  CA  THR A  11      34.132  -6.405  12.343  1.00 24.14      2MLT 1
ATOM     55  C   THR A  11      32.874  -6.299  13.140  1.00 24.13      2MLT 1
ATOM     56  O   THR A  11      31.823  -5.889  12.630  1.00 27.78      2MLT 1
ATOM     57  CB  THR A  11      34.462  -7.925  11.935  1.00 27.78      2MLT 1
ATOM     58  OG1 THR A  11      34.702  -8.591  13.199  1.00 31.22      2MLT 1
ATOM     59  CG2 THR A  11      35.695  -7.959  11.047  1.00 30.31      2MLT 1
ATOM     60  N   GLY A  12      32.918  -6.606  14.420  1.00 23.00      2MLT 1
ATOM     61  CA  GLY A  12      31.584  -6.595  15.140  1.00 24.17      2MLT 1
ATOM     62  C   GLY A  12      31.264  -5.168  15.584  1.00 24.45      2MLT 1
ATOM     63  O   GLY A  12      30.060  -4.914  15.603  1.00 23.79      2MLT 1
ATOM     64  N   LEU A  13      32.195  -4.276  15.948  1.00 22.05      2MLT 1
ATOM     65  CA  LEU A  13      31.923  -2.919  16.364  1.00 23.24      2MLT 1
ATOM     66  C   LEU A  13      31.251  -2.004  15.324  1.00 21.24      2MLT 1
ATOM     67  O   LEU A  13      30.232  -1.308  15.679  1.00 23.02      2MLT 1
ATOM     68  CB  LEU A  13      33.146  -2.183  17.008  1.00 25.55      2MLT 1
ATOM     69  CG  LEU A  13      32.913  -1.523  18.351  1.00 27.01      2MLT 1
ATOM     70  CD1 LEU A  13      33.999  -0.517  18.760  1.00 27.53      2MLT 1
ATOM     71  CD2 LEU A  13      31.587  -0.842  18.531  1.00 24.54      2MLT 1
ATOM     72  N   PRO A  14      31.689  -1.979  14.106  1.00 17.38      2MLT 1
ATOM     73  CA  PRO A  14      31.026  -1.278  13.030  1.00 17.52      2MLT 1
ATOM     74  C   PRO A  14      29.521  -1.670  12.857  1.00 18.98      2MLT 2
ATOM     75  O   PRO A  14      28.657  -0.801  12.744  1.00 16.75      2MLT 2
ATOM     76  CB  PRO A  14      31.845  -1.614  11.816  1.00 17.80      2MLT 2
ATOM     77  CG  PRO A  14      33.118  -2.205  12.303  1.00 18.05      2MLT 2
ATOM     78  CD  PRO A  14      33.098  -2.363  13.769  1.00 17.96      2MLT 2
ATOM     79  N   ALA A  15      29.207  -2.952  12.868  1.00 16.39      2MLT 2
ATOM     80  CA  ALA A  15      27.822  -3.418  12.724  1.00 17.10      2MLT 2
ATOM     81  C   ALA A  15      27.023  -3.016  13.951  1.00 16.98      2MLT 2
ATOM     82  O   ALA A  15      25.872  -2.551  13.769  1.00 16.78      2MLT 2
ATOM     83  CB  ALA A  15      27.741  -4.906  12.502  1.00 19.58      2MLT 2
ATOM     84  N   LEU A  16      27.570  -3.117  15.127  1.00 15.97      2MLT 2
ATOM     85  CA  LEU A  16      26.958  -2.649  16.351  1.00 18.20      2MLT 2
ATOM     86  C   LEU A  16      26.614  -1.169  16.344  1.00 20.28      2MLT 2
ATOM     87  O   LEU A  16      25.599  -0.734  16.933  1.00 18.32      2MLT 2
ATOM     88  CB  LEU A  16      27.811  -3.027  17.542  1.00 19.70      2MLT 2
ATOM     89  CG  LEU A  16      27.384  -2.550  18.921  1.00 22.23      2MLT 2
ATOM     90  CD1 LEU A  16      26.031  -3.234  19.257  1.00 27.80      2MLT 2
ATOM     91  CD2 LEU A  16      28.445  -2.970  19.933  1.00 21.91      2MLT 2
ATOM     92  N   ILE A  17      27.514  -0.365  15.791  1.00 20.97      2MLT 2
ATOM     93  CA  ILE A  17      27.343   1.056  15.618  1.00 20.41      2MLT 2
ATOM     94  C   ILE A  17      26.081   1.392  14.758  1.00 18.17      2MLT 2
ATOM     95  O   ILE A  17      25.380   2.240  15.282  1.00 16.46      2MLT 2
ATOM     96  CB  ILE A  17      28.579   1.847  15.132  1.00 21.10      2MLT 2
ATOM     97  CG1 ILE A  17      29.586   1.858  16.352  1.00 25.66      2MLT 2
ATOM     98  CG2 ILE A  17      28.268   3.288  14.691  1.00 22.04      2MLT 2
ATOM     99  CD1 ILE A  17      30.856   2.696  16.161  1.00 27.00      2MLT 2
ATOM    100  N   SER A  18      25.930   0.759  13.657  1.00 16.97      2MLT 2
ATOM    101  CA  SER A  18      24.825   0.827  12.744  1.00 19.98      2MLT 2
ATOM    102  C   SER A  18      23.499   0.405  13.438  1.00 18.89      2MLT 2
ATOM    103  O   SER A  18      22.557   1.165  13.352  1.00 18.37      2MLT 2
ATOM    104  CB  SER A  18      25.076   0.039  11.491  1.00 20.39      2MLT 2
ATOM    105  OG  SER A  18      23.902   0.046  10.670  1.00 23.87      2MLT 2
ATOM    106  N   TRP A  19      23.512  -0.661  14.161  1.00 17.71      2MLT 2
ATOM    107  CA  TRP A  19      22.492  -1.085  15.081  1.00 15.72      2MLT 2
ATOM    108  C   TRP A  19      22.083   0.004  16.012  1.00 18.02      2MLT 2
ATOM    109  O   TRP A  19      20.820   0.244  16.160  1.00 16.93      2MLT 2
ATOM    110  CB  TRP A  19      22.854  -2.410  15.767  1.00 15.59      2MLT 2
ATOM    111  CG  TRP A  19      21.803  -2.993  16.678  1.00 17.94      2MLT 2
ATOM    112  CD1 TRP A  19      20.917  -3.950  16.210  1.00 18.03      2MLT 2
ATOM    113  CD2 TRP A  19      21.448  -2.745  18.041  1.00 16.03      2MLT 2
ATOM    114  NE1 TRP A  19      20.060  -4.304  17.222  1.00 21.28      2MLT 2
ATOM    115  CE2 TRP A  19      20.357  -3.624  18.372  1.00 20.45      2MLT 2
ATOM    116  CE3 TRP A  19      21.879  -1.892  19.048  1.00 14.41      2MLT 2
ATOM    117  CZ2 TRP A  19      19.784  -3.690  19.612  1.00 17.15      2MLT 2
ATOM    118  CZ3 TRP A  19      21.292  -1.950  20.288  1.00 17.24      2MLT 2
ATOM    119  CH2 TRP A  19      20.230  -2.805  20.601  1.00 15.13      2MLT 2
ATOM    120  N   ILE A  20      22.930   0.594  16.823  1.00 14.82      2MLT 2
ATOM    121  CA  ILE A  20      22.628   1.633  17.766  1.00 15.67      2MLT 2
ATOM    122  C   ILE A  20      21.917   2.819  17.080  1.00 17.51      2MLT 2
ATOM    123  O   ILE A  20      20.942   3.365  17.655  1.00 17.70      2MLT 2
ATOM    124  CB  ILE A  20      23.902   2.192  18.499  1.00 16.25      2MLT 2
ATOM    125  CG1 ILE A  20      24.481   0.986  19.363  1.00 15.50      2MLT 2
ATOM    126  CG2 ILE A  20      23.599   3.421  19.390  1.00 14.54      2MLT 2
ATOM    127  CD1 ILE A  20      26.033   1.304  19.637  1.00 18.18      2MLT 2
ATOM    128  N   LYS A  21      22.464   3.177  15.957  1.00 16.61      2MLT 2
ATOM    129  CA  LYS A  21      21.888   4.236  15.157  1.00 19.84      2MLT 2
ATOM    130  C   LYS A  21      20.436   3.910  14.752  1.00 21.02      2MLT 2
ATOM    131  O   LYS A  21      19.685   4.899  14.971  1.00 22.80      2MLT 2
ATOM    132  CB  LYS A  21      22.699   4.646  13.935  1.00 16.73      2MLT 2
ATOM    133  CG  LYS A  21      23.944   5.416  14.471  1.00 22.19      2MLT 2
ATOM    134  CD  LYS A  21      24.919   5.669  13.300  1.00 25.86      2MLT 2
ATOM    135  CE  LYS A  21      26.173   6.287  13.908  1.00 32.91      2MLT 2
ATOM    136  NZ  LYS A  21      27.199   6.564  12.863  1.00 39.11      2MLT 2
ATOM    137  N   ARG A  22      20.075   2.728  14.351  1.00 19.18      2MLT 2
ATOM    138  CA  ARG A  22      18.740   2.273  14.020  1.00 20.38      2MLT 2
ATOM    139  C   ARG A  22      17.807   2.365  15.177  1.00 22.46      2MLT 2
ATOM    140  O   ARG A  22      16.648   2.899  15.096  1.00 23.24      2MLT 2
ATOM    141  CB  ARG A  22      18.733   0.860  13.418  1.00 22.73      2MLT 2
ATOM    142  CG  ARG A  22      19.309   0.956  12.004  1.00 22.29      2MLT 2
ATOM    143  CD  ARG A  22      19.117  -0.300  11.254  1.00 25.78      2MLT 2
ATOM    144  NE  ARG A  22      19.382  -1.562  11.991  1.00 28.94      2MLT 2
ATOM    145  CZ  ARG A  22      20.624  -2.104  11.889  1.00 33.77      2MLT 2
ATOM    146  NH1 ARG A  22      21.664  -1.554  11.252  1.00 31.21      2MLT 2
ATOM    147  NH2 ARG A  22      20.870  -3.271  12.498  1.00 33.11      2MLT 2
ATOM    148  N   LYS A  23      18.257   1.937  16.323  1.00 20.49      2MLT 2
ATOM    149  CA  LYS A  23      17.500   1.928  17.550  1.00 22.62      2MLT 2
ATOM    150  C   LYS A  23      17.216   3.361  18.100  1.00 25.47      2MLT 2
ATOM    151  O   LYS A  23      16.204   3.554  18.811  1.00 24.62      2MLT 2
ATOM    152  CB  LYS A  23      18.257   1.128  18.589  1.00 24.16      2MLT 2
ATOM    153  CG  LYS A  23      17.979  -0.388  18.463  1.00 31.03      2MLT 2
ATOM    154  CD  LYS A  23      16.858  -0.657  19.514  1.00 38.52      2MLT 2
ATOM    155  CE  LYS A  23      16.197  -1.986  19.278  1.00 44.05      2MLT 2
ATOM    156  NZ  LYS A  23      15.412  -2.493  20.477  1.00 48.30      2MLT 2
ATOM    157  N   ARG A  24      18.155   4.268  17.844  1.00 23.99      2MLT 2
ATOM    158  CA  ARG A  24      18.059   5.674  18.276  1.00 27.11      2MLT 2
ATOM    159  C   ARG A  24      16.996   6.355  17.441  1.00 28.34      2MLT 2
ATOM    160  O   ARG A  24      16.224   7.166  18.029  1.00 31.82      2MLT 2
ATOM    161  CB  ARG A  24      19.446   6.379  18.188  1.00 22.24      2MLT 2
ATOM    162  CG  ARG A  24      20.182   6.236  19.542  1.00 20.01      2MLT 2
ATOM    163  CD  ARG A  24      21.577   6.742  19.433  1.00 25.00      2MLT 2
ATOM    164  NE  ARG A  24      21.715   8.115  18.950  1.00 24.24      2MLT 2
ATOM    165  CZ  ARG A  24      21.745   9.136  19.837  1.00 22.72      2MLT 2
ATOM    166  NH1 ARG A  24      21.508   8.848  21.086  1.00 24.10      2MLT 2
ATOM    167  NH2 ARG A  24      22.134  10.375  19.512  1.00 24.09      2MLT 2
ATOM    168  N   GLN A  25      16.889   6.080  16.192  1.00 29.90      2MLT 2
ATOM    169  CA  GLN A  25      15.836   6.730  15.339  1.00 37.50      2MLT 2
ATOM    170  C   GLN A  25      14.463   6.162  15.554  1.00 39.92      2MLT 2
ATOM    171  O   GLN A  25      13.435   6.799  15.227  1.00 42.39      2MLT 2
ATOM    172  CB  GLN A  25      16.283   6.713  13.874  1.00 40.82      2MLT 2
ATOM    173  CG  GLN A  25      17.607   7.505  13.800  1.00 46.91      2MLT 2
ATOM    174  CD  GLN A  25      18.413   7.205  12.564  1.00 50.89      2MLT 3
ATOM    175  OE1 GLN A  25      19.596   7.559  12.477  1.00 54.23      2MLT 3
ATOM    176  NE2 GLN A  25      17.757   6.512  11.624  1.00 51.61      2MLT 3
ATOM    177  N   GLN A  26      14.363   4.945  16.064  1.00 43.06      2MLT 3
ATOM    178  CA  GLN A  26      13.132   4.360  16.583  1.00 46.66      2MLT 3
ATOM    179  C   GLN A  26      12.618   5.071  17.837  1.00 48.00      2MLT 3
ATOM    180  O   GLN A  26      11.409   5.344  17.597  1.00 50.85      2MLT 3
ATOM    181  CB  GLN A  26      13.144   2.874  16.862  1.00 49.14      2MLT 3
ATOM    182  CG  GLN A  26      13.166   1.972  15.625  1.00 54.49      2MLT 3
ATOM    183  CD  GLN A  26      13.506   0.551  16.116  1.00 58.10      2MLT 3
ATOM    184  OE1 GLN A  26      13.083   0.180  17.239  1.00 59.27      2MLT 3
ATOM    185  NE2 GLN A  26      14.304  -0.117  15.290  1.00 57.61      2MLT 3
BREAK
END
"""

dummy="""
CRYST1   30.832   28.293   22.211  90.00  90.00  90.00 C 2 2 21
ATOM      1  N   GLY A   1      42.375 -12.180  24.780  1.00 35.31      2MLT N
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  pdb_file = "%s.pdb"%prefix
  mtz_file = "%s.mtz"%prefix
  of = open(pdb_file, "w")
  print(pdb_str, file=of)
  of.close()
  #
  mc = abs(iotbx.pdb.input(source_info=None,
    lines=dummy).xray_structure_simple().structure_factors(d_min=3).f_calc())
  mtz_dataset = mc.as_mtz_dataset(column_root_label="Fobs")
  mtz_dataset.add_miller_array(
    miller_array=mc.generate_r_free_flags(),
    column_root_label="R-free-flags")
  mtz_object = mtz_dataset.mtz_object()
  mtz_object.write(file_name = mtz_file)
  #
  args = [
    "main.number_of_mac=1",
    pdb_file, mtz_file,
    "main.max_number_of_iter=5",
    "strategy=individual_sites"]
  r = run_phenix_refine(args = args, prefix = prefix, sorry_expected=True)
  assert_lines_in_file(
    file_name=r.log,
    lines="""
Sorry: Unknown chemical element type:"""
    )

if (__name__ == "__main__"):
  run()
  print("OK")
