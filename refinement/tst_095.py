from __future__ import division
from __future__ import print_function
import iotbx.pdb
import libtbx.load_env
import os
from phenix_regression.refinement import run_phenix_refine
from libtbx.test_utils import assert_lines_in_file

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Exercise 'Sorry: Using MLHL target requires HL present: no HL provided.'
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/%s.pdb"%prefix,
    test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/%s.mtz"%prefix, \
    test=os.path.isfile)
  args = [pdb, hkl, "main.target=mlhl", "miller_array.labels.name=IMEAN,SIGIMEAN"]
  r = run_phenix_refine(args = args, prefix = prefix, sorry_expected=True)
  #
  assert_lines_in_file(
    file_name=r.log,
    lines="Sorry: Using MLHL target requires HL present: no HL provided.")

if (__name__ == "__main__"):
  run()
  print("OK")
