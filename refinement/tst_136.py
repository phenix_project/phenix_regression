from __future__ import print_function
from libtbx.test_utils import run_command
import libtbx.load_env
import sys, os, time
from phenix_regression.refinement import run_phenix_refine
from libtbx.test_utils import assert_lines_in_file

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Test apply_cif_modification
  """
  d = libtbx.env.under_dist(
    module_name="phenix_regression",
    path="refinement/data/%s"%prefix)
  assert os.path.isdir(d)
  args = [
    '%s/dna_o5t.pdb' % d,
    '%s/dna_o5t.mtz' % d,
    '%s/mod_5pho.cif' % d,
    '%s/dna_o5t_geo_edits' % d,
    'translate_cns_dna_rna_residue_names=True',
    'show_max_items.bond_restraints_sorted_by_residual=10',
    'dry_run=true']
  r = run_phenix_refine(args = args, prefix = prefix, dff_expected=False,
    mtz_expected=False, pdb_expected=False, cif_expected=False)
  assert_lines_in_file(
    file_name=r.log,
    lines="""\
  bond pdb=" P   GUA A   3 "
       pdb=" OFT GUA A   3 "
    ideal  model  delta    sigma   weight residual
    1.520  1.107  0.413 2.00e-02 2.50e+03 4.27e+02
""")

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK", "time: %.2f"%(time.time()-t0))

