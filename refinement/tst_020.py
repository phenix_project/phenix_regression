from __future__ import division
from __future__ import print_function
import libtbx.load_env
import os
from phenix_regression.refinement import run_phenix_refine
from libtbx.test_utils import assert_lines_in_file

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Test ensures that CRYST1 in resulting pdb after refinement is the same with
  and without water picking procedure and is grabbed from mtz.
  """
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/5cq4_cut.pdb",
    test=os.path.isfile)
  mtz_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/5cq8_cut.mtz",
    test=os.path.isfile)
  args = [
    pdb_file,
    mtz_file,
    "main.number_of_macro_cycles=1",
    "refine.strategy=individual_sites"]
  r = run_phenix_refine(args = args, prefix = prefix)
  assert_lines_in_file(
    file_name=r.pdb,
    lines="CRYST1   81.759   97.216   58.147  90.00  90.00  90.00 C 2 2 21")
  args = args + ["main.ordered_solvent=True"]
  r = run_phenix_refine(args = args, prefix = prefix)
  assert_lines_in_file(
    file_name=r.pdb,
    lines="CRYST1   81.759   97.216   58.147  90.00  90.00  90.00 C 2 2 21")

if (__name__ == "__main__"):
  run()
  print("OK")
