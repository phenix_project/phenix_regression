from __future__ import division
from __future__ import print_function
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel
import os
from libtbx.test_utils import assert_lines_in_file

pdb_str = """\n
CRYST1   34.917   22.246   44.017  90.00  90.00  90.00 P 1
ATOM      1  N   ALA A   1      27.344  16.348  30.784  1.00 10.00           N
ATOM      2  CA  ALA A   1      26.429  15.281  31.335  1.00 10.00           C
ATOM      3  C   ALA A   1      26.610  14.025  30.603  1.00 10.00           C
ATOM      4  O   ALA A   1      26.479  13.979  29.356  1.00 10.00           O
ATOM      5  CB  ALA A   1      24.874  15.800  31.300  1.00 10.00           C
ATOM      1  N   ALA A   2      26.812  12.925  31.345  1.00 10.00           N
ATOM      2  CA  ALA A   2      27.084  11.577  30.797  1.00 10.00           C
ATOM      3  C   ALA A   2      25.856  10.737  30.707  1.00 10.00           C
ATOM      4  O   ALA A   2      25.741   9.860  29.891  1.00 10.00           O
ATOM      5  CB  ALA A   2      28.151  10.950  31.721  1.00 10.00           C
ATOM      1  N   ALA A   3      25.009  10.973  31.714  1.00 10.00           N
ATOM      2  CA  ALA A   3      23.621  10.543  31.560  1.00 10.00           C
ATOM      3  C   ALA A   3      23.023  11.008  30.214  1.00 10.00           C
ATOM      4  O   ALA A   3      22.786  10.233  29.249  1.00 10.00           O
ATOM      5  CB  ALA A   3      22.760  11.040  32.654  1.00 10.00           C
ATOM      1  N   ALA A   4      22.798  12.304  30.175  1.00 10.00           N
ATOM      2  CA  ALA A   4      22.329  13.084  28.981  1.00 10.00           C
ATOM      3  C   ALA A   4      23.116  12.816  27.721  1.00 10.00           C
ATOM      4  O   ALA A   4      22.533  12.805  26.670  1.00 10.00           O
ATOM      5  CB  ALA A   4      22.372  14.607  29.318  1.00 10.00           C
ATOM      1  N   ALA A   5      24.448  12.622  27.823  1.00 10.00           N
ATOM      2  CA  ALA A   5      25.228  12.407  26.573  1.00 10.00           C
ATOM      3  C   ALA A   5      25.222  10.947  26.143  1.00 10.00           C
ATOM      4  O   ALA A   5      25.386  10.664  24.983  1.00 10.00           O
"""

params_str = """
refinement {
  refine {
    strategy = individual_sites individual_sites_real_space rigid_body \
               individual_adp group_adp *tls occupancies group_anomalous
  }
  main {
    number_of_macro_cycles = 1
  }
  tls {
    find_automatically = False
    one_residue_one_group = False
    refine_T = False
    refine_L = False
    refine_S = False
  }
}
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Exercise Sorry:
  TLS refinement is requested but refinement of TLS matrices is disabled.
  """
  pdb_in = "%s.pdb"%prefix
  open(pdb_in, "w").write(pdb_str)
  #
  args = [
    pdb_in,
    "high_res=6",
    "type=real",
    "label=f-obs",
    "r_free=0.1"]
  r = run_fmodel(args = args, prefix = prefix)
  #
  phil = "%s.eff"%prefix
  open(phil, "w").write(params_str)
  #
  args = [pdb_in, r.mtz, phil]
  r = run_phenix_refine(args = args, prefix = prefix, sorry_expected=True)
  assert_lines_in_file(
    file_name=r.log,
    lines="Sorry: TLS refinement is requested but refinement of TLS matrices is disabled.")

if (__name__ == "__main__"):
  run()
  print("OK")
