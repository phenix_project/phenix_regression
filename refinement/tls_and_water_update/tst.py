from __future__ import division
from __future__ import print_function
import time, os
from libtbx import easy_run
import libtbx.load_env

def exercise():
  """
  Test making sure this combination of parameters does not crash phenix.refine .

  Model_2 is the same as above, now water not being the last atoms.
  Changed last water to Cl in diffenent chain.

  """
  for pdb_name in ["model.pdb", "model_2.pdb"]:
    pdb = libtbx.env.find_in_repositories(
      relative_path="phenix_regression/refinement/tls_and_water_update/%s" % pdb_name,
      test=os.path.isfile)
    mtz = libtbx.env.find_in_repositories(
      relative_path="phenix_regression/refinement/tls_and_water_update/data.mtz",
      test=os.path.isfile)
    params = libtbx.env.find_in_repositories(
      relative_path="phenix_regression/refinement/tls_and_water_update/p.eff",
      test=os.path.isfile)
    cmd = " ".join([
      "phenix.refine",
      "%s"%pdb,
      "%s"%mtz,
      "%s"%params,
      "--quiet",
      "--overwrite"])
    print(cmd)
    assert not easy_run.call(cmd)

if (__name__ == "__main__"):
  t0 = time.time()
  exercise()
  print("Total time: %-8.4f"%(time.time()-t0))
  print("OK")
