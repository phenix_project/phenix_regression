from __future__ import division
from __future__ import print_function
import iotbx.pdb
from libtbx.test_utils import approx_equal, not_approx_equal
import libtbx.load_env
import os
from phenix_regression.refinement import run_phenix_refine

params="""\
refinement.main {
 target                 = ls
 number_of_macro_cycles = 10
 bulk_solvent_and_scale = false
 fake_f_obs             = true
}
refinement.structure_factors_and_gradients_accuracy.algorithm=direct
refinement.structure_factors_and_gradients_accuracy.cos_sin_table=false
refinement.fake_f_obs.structure_factors_accuracy.algorithm=direct
refinement.fake_f_obs.structure_factors_accuracy.cos_sin_table=false
refinement.modify_start_model {
 output {
   file_name = %s
 }
 modify.adp.convert_to_isotropic = true
 modify.selection                = water
}
refinement.refine.strategy                   = individual_adp
refinement.refine.adp.individual.anisotropic = water
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Exercise switch_to_isotropic_high_res_limit and modify_start_model
  """
  pdb_modified = prefix+"_write_modified.pdb"
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/phe_adp_refinement_hoh.pdb",
    test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/phe_adp_refinement_hoh.mtz", \
    test=os.path.isfile)
  phil = "%s.eff"%prefix
  open(phil, "w").write(params%pdb_modified)
  args = [pdb, hkl, phil, "switch_to_isotropic_high_res_limit=10"]
  r = run_phenix_refine(args = args, prefix = prefix)
  #
  sc_s = iotbx.pdb.input(file_name = pdb) \
    .construct_hierarchy().extract_xray_structure().scatterers()
  sc_m = iotbx.pdb.input(file_name = pdb_modified) \
    .construct_hierarchy().extract_xray_structure().scatterers()
  sc_f = iotbx.pdb.input(file_name = r.pdb) \
    .construct_hierarchy().extract_xray_structure().scatterers()
  last_element = sc_f.size() - 1
  counter = 0
  for sc_s_i, sc_m_i, sc_f_i in zip(sc_s, sc_m, sc_f):
    assert approx_equal(sc_s_i.site, sc_m_i.site, 1.e-4)
    assert approx_equal(sc_s_i.site, sc_f_i.site, 1.e-4)
    if(counter != last_element):
      assert approx_equal(sc_s_i.u_iso, sc_m_i.u_iso, 1.e-4)
      assert approx_equal(sc_s_i.u_iso, sc_f_i.u_iso, 1.e-4)
      assert approx_equal(sc_s_i.u_star, sc_m_i.u_star, 1.e-4)
      assert approx_equal(sc_s_i.u_star, sc_f_i.u_star, 1.e-4)
    else:
      assert approx_equal(sc_s_i.u_iso, sc_f_i.u_iso, 1.e-4)
      assert approx_equal(sc_s_i.u_star, sc_f_i.u_star, 1.e-4)
      assert approx_equal(sc_s_i.u_iso, sc_f_i.u_iso, 1.e-4)
      assert approx_equal(sc_s_i.u_iso, -1.0)
      assert not_approx_equal(sc_s_i.u_star, sc_m_i.u_star, 1.e-4)
    counter += 1
  r.check_final_r(r_work=0., r_free=0., eps=1.e-4)
  r.check_start_r(r_work=0.0865, r_free=0.0945, eps=0.015)

if (__name__ == "__main__"):
  run()
  print("OK")
