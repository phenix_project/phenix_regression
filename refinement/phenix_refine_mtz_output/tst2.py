from __future__ import print_function
import iotbx.mtz
import libtbx.load_env
import time, os, random
from libtbx import easy_run
from scitbx.array_family import flex
from libtbx.test_utils import approx_equal

params_str = """\
data_manager {
  miller_array {
    file = %s
    labels {
      name = FOBS
    }
  }
  miller_array {
    file = %s
    labels {
      name = PA,PB,PC,PD
    }
  }
}
"""

def check_output(file_name_data, file_name_current):
  mtz_obj_out = iotbx.mtz.object(file_name=file_name_data)
  assert mtz_obj_out.column_labels() == [
    "H",
    "K",
    "L",
    "F-obs",
    "R-free-flags",
    "HLA(+)",
    "HLB(+)",
    "HLC(+)",
    "HLD(+)",
    "HLA(-)",
    "HLB(-)",
    "HLC(-)",
    "HLD(-)"]
  counter = 0
  f_obs1, flags1 = None,None
  for ma in mtz_obj_out.as_miller_arrays():
    if(ma.info().labels == ['F-obs']):
      assert ma.data().size() == 78, ma.data().size()
      counter += 1
      f_obs1 = ma
    if(ma.info().labels == ['R-free-flags']):
      assert ma.data().size() == 78, ma.data().size()
      counter += 1
      flags1 = ma
    if(ma.info().labels ==
       ['HLA(+)','HLB(+)','HLC(+)','HLD(+)','HLA(-)','HLB(-)','HLC(-)','HLD(-)']):
      assert ma.data().size() == 234
      counter += 1
  assert counter == 3
  #
  mtz_obj_out = iotbx.mtz.object(file_name=file_name_current)
  counter = 0
  f_obs2, flags2 = None,None
  for ma in mtz_obj_out.as_miller_arrays():
    if(ma.info().labels == ['F-obs']):
      assert ma.data().size() == 78, ma.data().size()
      counter += 1
      f_obs2 = ma
    if(ma.info().labels == ['R-free-flags']):
      assert ma.data().size() == 78, ma.data().size()
      counter += 1
      flags2 = ma
    if(ma.info().labels ==
       ['HLA(+)','HLB(+)','HLC(+)','HLD(+)','HLA(-)','HLB(-)','HLC(-)','HLD(-)']):
      assert ma.data().size() == 234
      counter += 1
  assert counter == 3
  #
  assert approx_equal(f_obs1.data(), f_obs2.data())
  assert approx_equal(flags1.data(), flags2.data())
  #
  f_obs_filtered, f_model, map_no_fill, res_map_no_fill, map_fill = \
    None,None,None,None,None
  for ma in mtz_obj_out.as_miller_arrays():
    if(ma.info().labels == ['F-obs-filtered']):
      f_obs_filtered = ma
    if(ma.info().labels == ['F-model', 'PHIF-model']):
      f_model = ma
    if(ma.info().labels == ['2FOFCWT_no_fill', 'PH2FOFCWT_no_fill']):
      map_no_fill = ma
    if(ma.info().labels == ['2FOFCWT', 'PH2FOFCWT']):
      map_fill = ma
    if(ma.info().labels == ['FOFCWT', 'PHFOFCWT']):
      res_map_no_fill = ma
  assert f_obs_filtered.data().size() == f_model.data().size()
  assert f_obs_filtered.data().size() == map_no_fill.data().size()
  assert f_obs_filtered.data().size() == res_map_no_fill.data().size()
  assert f_obs_filtered.data().size() < map_fill.data().size()
  assert isinstance(f_obs_filtered.data(), flex.double)
  assert isinstance(f_model.data(), flex.complex_double)
  assert isinstance(map_no_fill.data(), flex.complex_double)
  assert isinstance(res_map_no_fill.data(), flex.complex_double)
  assert isinstance(map_fill.data(), flex.complex_double)
  assert f_obs_filtered.data().size() < f_obs1.data().size()
  assert f_obs1.data().size() < map_fill.data().size()

def exercise_01():
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/phenix_refine_mtz_output/model.pdb",
    test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/phenix_refine_mtz_output/fobs2.cv",
    test=os.path.isfile)
  hl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/phenix_refine_mtz_output/hl.hkl",
    test=os.path.isfile)
  output_file_prefix = str(random.randint(1, 1000000))
  #
  with open("tst2_phenix_refine_mtz_output.eff", "w") as fo:
    fo.write(params_str%(hkl,hl))
  #
  cmd = " ".join([
    'phenix.refine',
    "tst2_phenix_refine_mtz_output.eff",
    '%s'%pdb,
    "strategy=none",
    "main.bulk_sol=true",
    "main.number_of_mac=1",
    "xray_data.r_free_flags.generate=true",
    'output.prefix=%s'%output_file_prefix,
    '--overwrite --quiet > %s.log'%output_file_prefix])
  print(cmd)
  assert not easy_run.call(cmd)
  check_output(
    file_name_data="%s_data.mtz"%output_file_prefix,
    file_name_current="%s_001.mtz"%output_file_prefix)
  with open("tst2_phenix_refine_mtz_output.eff", "w") as fo:
    fo.write("""
data_manager {
  miller_array {
    file = %s_data.mtz
    labels {
      name = HLA(+),HLB(+),HLC(+),HLD(+),HLA(-),HLB(-),HLC(-),HLD(-)
    }
  }
}
""" % output_file_prefix)
  #
  cmd = " ".join([
    'phenix.refine',
    "tst2_phenix_refine_mtz_output.eff",
    '%s'%pdb,
    "%s"%hl,
    "strategy=none",
    "main.bulk_sol=true",
    "main.number_of_mac=1",
    'output.prefix=%s'%output_file_prefix,
    '--overwrite --quiet > %s.log'%output_file_prefix])
  print(cmd)
  assert not easy_run.call(cmd)
  check_output(
    file_name_data="%s_data.mtz"%output_file_prefix,
    file_name_current="%s_001.mtz"%output_file_prefix)
  #
  with open("tst2_phenix_refine_mtz_output.eff", "w") as fo:
    fo.write("""
data_manager {
  miller_array {
    file = %s_001.mtz
    labels {
      name = F-obs
    }
    labels {
      name = HLA(+),HLB(+),HLC(+),HLD(+),HLA(-),HLB(-),HLC(-),HLD(-)
    }
  }
}
""" % output_file_prefix)
  cmd = " ".join([
    'phenix.refine',
    "tst2_phenix_refine_mtz_output.eff",
    '%s'%pdb,
    "%s"%hl,
    "strategy=none",
    "main.bulk_sol=true",
    "main.number_of_mac=1",
    'output.prefix=%s'%output_file_prefix,
    '--overwrite --quiet > %s.log'%output_file_prefix])
  print(cmd)
  assert not easy_run.call(cmd)
  check_output(
    file_name_data="%s_data.mtz"%output_file_prefix,
    file_name_current="%s_001.mtz"%output_file_prefix)
  #
  easy_run.call("rm -rf %s*"%output_file_prefix)

def run():
  t0 = time.time()
  exercise_01()
  print("OK time = %8.3f" % (time.time() - t0))

if (__name__ == "__main__"):
  run()
