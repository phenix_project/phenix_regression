from __future__ import print_function
from phenix_regression.refinement import get_r_factors
import libtbx.load_env
from libtbx.test_utils import approx_equal, not_approx_equal, run_command
import sys, os, time, random
import mmtbx.model
from cctbx.array_family import flex
import iotbx.pdb
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel
import os

if (1):
  random.seed(0)
  flex.set_random_seed(0)

pdb_str_good = """
CRYST1   15.000   15.000   15.000  90.00  90.00  90.00 P 1
ATOM      0  O1  SO4 A   0       4.051   6.839   8.433  1.00 10.00           O
ATOM      1  O3  SO4 A   0       4.919   8.288  10.201  1.00 10.00           O
ATOM      2  O4  SO4 A   0       2.504   7.958   9.965  1.00 10.00           O
ATOM      3  S   SO4 A   0       3.847   7.303   9.843  1.00 10.00           S
ATOM      4  O2 ASO4 A   0       3.917   6.129  10.771  0.40 10.00           O
ATOM      5  O2 BSO4 A   0       3.517   6.529  10.571  0.60 10.00           O

ATOM      6  O1 ASO4 A   1       1.503   1.292   0.081  0.70 10.00           O
ATOM      7  O2 ASO4 A   1       1.093   0.060   2.155  0.70 10.00           O
ATOM      8  O3 ASO4 A   1       2.513   2.053   2.174  0.70 10.00           O
ATOM      9  O4 ASO4 A   1       0.099   2.273   1.830  0.70 10.00           O
ATOM     10  S  ASO4 A   1       1.301   1.419   1.560  0.70 10.00           S
ATOM     11  O1 BSO4 A   1       3.503   3.292   2.081  0.30 10.00           O
ATOM     12  O2 BSO4 A   1       3.093   2.060   4.155  0.30 10.00           O
ATOM     13  O3 BSO4 A   1       4.513   4.053   4.174  0.30 10.00           O
ATOM     14  O4 BSO4 A   1       2.099   4.273   3.830  0.30 10.00           O
ATOM     15  S  BSO4 A   1       3.301   3.419   3.560  0.30 10.00           S

ATOM     16  O  AHOH S   2       5.131   5.251   5.823  0.60 10.00           O
ATOM     17  O  BHOH S   2       6.132   6.203   6.906  0.40 10.00           O

ATOM     18  O   HOH S   3       1.132   5.924   7.074  1.00 15.00           O
ATOM     19  H1  HOH S   3       1.157   5.258   6.547  1.00 15.00           H
ATOM     20  H2  HOH S   3       1.125   5.571   7.848  1.00 15.00           H

ATOM     21  O   HOH S   4       6.073   7.375   5.045  0.50 15.00           O

ATOM     22  O   HOH S   5       0.131   7.251   5.000  1.00 15.00           O

ATOM     23  S   SO4 C   6       7.349   7.468   2.587  0.30 10.00           S
ATOM     24  O1 ASO4 C   6       7.155   7.917   1.171  0.60 10.00           O
ATOM     25  O2 ASO4 C   6       6.860   6.258   3.321  0.60 10.00           O
ATOM     26  O3 ASO4 C   6       8.132   6.409   1.871  0.60 10.00           O
ATOM     27  O4 ASO4 C   6       5.881   7.772   2.617  0.60 10.00           O
ATOM     28  O1 BSO4 C   6       7.621   7.364   1.117  0.40 10.00           O
ATOM     29  O2 BSO4 C   6       7.165   6.096   3.160  0.40 10.00           O
ATOM     30  O3 BSO4 C   6       8.507   8.141   3.260  0.40 10.00           O
ATOM     31  O4 BSO4 C   6       6.104   8.274   2.808  0.40 10.00           O

HETATM   32 MG    MG A 408      12.192  12.896  11.607  1.00  7.06          Mg

ATOM     33  O1  SO4 X   0       3.067  10.039   5.585  0.40 10.00           O
ATOM     34  O2  SO4 X   0       2.034   8.459   7.141  0.40 10.00           O
ATOM     35  O3  SO4 X   0       3.891   9.872   7.881  0.40 10.00           O
ATOM     36  O4  SO4 X   0       1.698  10.865   7.437  0.40 10.00           O
ATOM     37  S   SO4 X   0       2.672   9.808   7.011  0.40 10.00           S

ATOM     38  O1  SO4 X   1       6.067  13.039   8.585  0.70 10.00           O
ATOM     39  O2  SO4 X   1       5.034  11.459  10.141  0.70 10.00           O
ATOM     40  O3  SO4 X   1       6.891  12.872  10.881  0.70 10.00           O
ATOM     41  O4  SO4 X   1       4.698  13.865  10.437  0.70 10.00           O
ATOM     42  S   SO4 X   1       5.672  12.808  10.011  0.70 10.00           S
END
"""

pdb_str_poor = """
CRYST1   15.000   15.000   15.000  90.00  90.00  90.00 P 1
ATOM      1  O1  SO4 A   0       4.051   6.839   8.433  1.00 10.00           O
ATOM      2  O3  SO4 A   0       4.919   8.288  10.201  1.00 10.00           O
ATOM      3  O4  SO4 A   0       2.504   7.958   9.965  1.00 10.00           O
ATOM      4  S   SO4 A   0       3.847   7.303   9.843  1.00 10.00           S
ATOM      5  O2 ASO4 A   0       3.917   6.129  10.771  1.00 10.00           O
ATOM      6  O2 BSO4 A   0       3.517   6.529  10.571  1.00 10.00           O

ATOM      7  O1 ASO4 A   1       1.503   1.292   0.081  1.00 10.00           O
ATOM      8  O2 ASO4 A   1       1.093   0.060   2.155  1.00 10.00           O
ATOM      9  O3 ASO4 A   1       2.513   2.053   2.174  1.00 10.00           O
ATOM     10  O4 ASO4 A   1       0.099   2.273   1.830  1.00 10.00           O
ATOM     11  S  ASO4 A   1       1.301   1.419   1.560  1.00 10.00           S
ATOM     12  O1 BSO4 A   1       3.503   3.292   2.081  1.00 10.00           O
ATOM     13  O2 BSO4 A   1       3.093   2.060   4.155  1.00 10.00           O
ATOM     14  O3 BSO4 A   1       4.513   4.053   4.174  1.00 10.00           O
ATOM     15  O4 BSO4 A   1       2.099   4.273   3.830  1.00 10.00           O
ATOM     16  S  BSO4 A   1       3.301   3.419   3.560  1.00 10.00           S
ATOM     17  O  AHOH S   2       5.131   5.251   5.823  1.00 10.00           O
ATOM     18  O  BHOH S   2       6.132   6.203   6.906  1.00 10.00           O

ATOM     19  O   HOH S   3       1.132   5.924   7.074  1.00 15.00           O
ATOM     20  H1  HOH S   3       1.157   5.258   6.547  1.00 15.00           H
ATOM     21  H2  HOH S   3       1.125   5.571   7.848  1.00 15.00           H

ATOM     22  O   HOH S   4       6.073   7.375   5.045  0.30 15.00           O

ATOM     23  O   HOH S   5       0.131   7.251   5.000  0.00 15.00           O

ATOM     24  S   SO4 C   6       7.349   7.468   2.587  0.50 10.00           S
ATOM     25  O1 ASO4 C   6       7.155   7.917   1.171  1.00 10.00           O
ATOM     26  O2 ASO4 C   6       6.860   6.258   3.321  1.00 10.00           O
ATOM     27  O3 ASO4 C   6       8.132   6.409   1.871  1.00 10.00           O
ATOM     28  O4 ASO4 C   6       5.881   7.772   2.617  1.00 10.00           O
ATOM     29  O1 BSO4 C   6       7.621   7.364   1.117  1.00 10.00           O
ATOM     30  O2 BSO4 C   6       7.165   6.096   3.160  1.00 10.00           O
ATOM     31  O3 BSO4 C   6       8.507   8.141   3.260  1.00 10.00           O
ATOM     32  O4 BSO4 C   6       6.104   8.274   2.808  1.00 10.00           O

HETATM   33 MG    MG A 408      12.192  12.896  11.607  0.00  7.06          Mg

ATOM     34  O1  SO4 X   0       3.067  10.039   5.585  0.40 10.00           O
ATOM     35  O2  SO4 X   0       2.034   8.459   7.141  0.40 10.00           O
ATOM     36  O3  SO4 X   0       3.891   9.872   7.881  0.40 10.00           O
ATOM     37  O4  SO4 X   0       1.698  10.865   7.437  0.40 10.00           O
ATOM     38  S   SO4 X   0       2.672   9.808   7.011  0.40 10.00           S

ATOM     39  O1  SO4 X   1       6.067  13.039   8.585  0.50 10.00           O
ATOM     40  O2  SO4 X   1       5.034  11.459  10.141  0.60 10.00           O
ATOM     41  O3  SO4 X   1       6.891  12.872  10.881  0.70 10.00           O
ATOM     42  O4  SO4 X   1       4.698  13.865  10.437  0.80 10.00           O
ATOM     43  S   SO4 X   1       5.672  12.808  10.011  0.90 10.00           S
END
"""

common_params = [
  'main.target=ls',
  'main.bulk_solvent_and_=false',
  'ls_target_names.target_name=ls_wunit_kunit',
  'structure_factors_and_gradients_accuracy.algorithm=direct',
  'xray_data.r_free_flags.ignore_r_free_flags=true']

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Occupancies: very comprehensive, but not all 13 strategies.
  """
  with open("%s_good.pdb"%prefix, "w") as fo:
    fo.write(pdb_str_good)
  with open("%s_poor.pdb"%prefix, "w") as fo:
    fo.write(pdb_str_poor)
  #
  args = [
      "%s_good.pdb"%prefix,
      'high_resolution=1.2',
      'algorithm=direct',
      'type=real',
      'r_free=0.1']
  r = run_fmodel(args = args, prefix = prefix)
  #
  args = [
    'strategy=occupancies',
    "%s_poor.pdb"%prefix,
    r.mtz,
    'main.number_of_macro_cycles=5',
    ] + common_params

  cmd = "phenix.refine " + " ".join(args)

  r = run_phenix_refine(args = args, prefix = prefix)

  # check results
  r_pdb = r.pdb
  pdb_inp = iotbx.pdb.input(file_name = r_pdb)
  model = mmtbx.model.manager(model_input = pdb_inp)
  occ = model.get_hierarchy().atoms().extract_occ()
  #
  sel1 = model.selection("chain A and resseq 0 and altloc ' ' ")
  assert occ.select(sel1).all_eq(1.)
  selA = model.selection("chain A and resseq 0 and altloc 'A' ")
  selB = model.selection("chain A and resseq 0 and altloc 'B' ")
  assert approx_equal(occ.select(selA)[0]+occ.select(selB)[0], 1., 1.e-3)
  #
  selA = model.selection("chain A and resseq 1 and altloc 'A' ")
  selB = model.selection("chain A and resseq 1 and altloc 'B' ")
  occA = occ.select(selA)
  occB = occ.select(selB)
  assert occA.all_eq(occA[0])
  assert occB.all_eq(occB[0])
  assert approx_equal(occA[0]+occB[0], 1., 1.e-3)
  #
  selA = model.selection("chain S and resseq 2 and altloc 'A' ")
  selB = model.selection("chain S and resseq 2 and altloc 'B' ")
  occA = occ.select(selA)
  occB = occ.select(selB)
  assert approx_equal(occA[0]+occB[0], 1., 1.e-3)
  #
  sel1 = model.selection("chain S and resseq 4")
  occ1 = occ.select(sel1)
  assert occ1.size()==1
  assert occ1[0]<1 and occ1[0]>0
  #
  sel1 = model.selection("chain S and resseq 5")
  occ1 = occ.select(sel1)
  assert occ1.size()==1
  assert approx_equal(occ1[0], 0, 1.e-3)
  #
  sel1 = model.selection("chain C and resseq 6 and altloc ' ' ")
  selA = model.selection("chain C and resseq 6 and altloc 'A' ")
  selB = model.selection("chain C and resseq 6 and altloc 'B' ")
  occ1 = occ.select(sel1)
  occA = occ.select(selA)
  occB = occ.select(selB)
  assert occ1.size()==1
  assert occ1[0]>0 and occ1[0]<1
  assert occA.all_eq(occA[0])
  assert occB.all_eq(occB[0])
  assert approx_equal(occA[0]+occB[0], 1., 1.e-3)
  #
  sel1 = model.selection("chain A and resseq 408")
  occ1 = occ.select(sel1)
  assert occ1.size()==1
  assert approx_equal(occ1[0], 0, 1.e-3)
  #
  sel1 = model.selection("chain X and resseq 0")
  occ1 = occ.select(sel1)
  assert occ1.all_eq(occ1[0])
  assert occ1[0]>0 and occ1[0]<1
  #
  sel1 = model.selection("chain X and resseq 1")
  occ1 = occ.select(sel1)
  mi,ma,me = round(flex.min(occ1),2), round(flex.max(occ1),2), \
             round(flex.mean(occ1),2)
  assert len(list(set([mi,ma,me])))==3

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK", "time: %.2f"%(time.time()-t0))
