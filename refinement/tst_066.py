from __future__ import division
from __future__ import print_function
from phenix_regression.refinement import run_phenix_refine
from libtbx.test_utils import approx_equal
import libtbx.load_env
import libtbx.path
import iotbx.pdb
import os

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  ADP refinement: mix iso/aniso.
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/phe_adp_refinement_hoh.pdb",
    test=os.path.isfile)
  xrs = iotbx.pdb.input(file_name=pdb).xray_structure_simple()
  f_obs = abs(xrs.structure_factors(
    d_min          = 1.0,
    anomalous_flag = False,
    cos_sin_table  = True,
    algorithm      = "fft").f_calc())
  flags = f_obs.generate_r_free_flags(fraction = 0.1, max_free = 99999999)
  mtz_dataset = f_obs.as_mtz_dataset(column_root_label = "FOBS")
  mtz_dataset.add_miller_array(miller_array      = flags,
                               column_root_label = "TEST")
  mtz_object = mtz_dataset.mtz_object()
  hkl_in = "%s.mtz"%prefix
  mtz_object.write(file_name = hkl_in)
  #
  args = [pdb,hkl_in,
    "refinement.structure_factors_and_gradients_accuracy.algorithm=direct",
    "main.target=ls",
    "fake_f_obs.structure_factors_accuracy.algorithm=direct",
    "modify_start_model.modify.adp.randomize=true",
    "main.number_of_macro_cycles=20",
    "strategy=individual_adp",
    "main.bulk_solvent_and_scale=false",
    "main.fake_f_obs=true",
    "target_weights.wu=0",
    "structure_factors_and_gradients_accuracy.cos_sin_table=false",
    "modify_start_model.output.file_name=%s_modified.pdb"%prefix]
  r = run_phenix_refine(args = args, prefix = prefix)
  r.check_final_r(r_work=0.0, r_free=0.0, eps=1.e-4)
  r.check_start_r(r_work=0.1, r_free=0.1, eps=0.05)
  assert approx_equal(r.bond_start, r.bond_final)
  assert approx_equal(r.angle_start,r.angle_final)

if (__name__ == "__main__"):
  run()
  print("OK")
