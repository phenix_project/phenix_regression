from __future__ import print_function
import time, os
from libtbx.test_utils import approx_equal
from libtbx import easy_run
import iotbx.pdb
import mmtbx.utils
from scitbx.array_family import flex
from phenix_regression.refinement import run_phenix_refine

pdb_str = """
CRYST1   15.319   15.496   15.323  90.00  90.00  90.00 P 1
ATOM      1  N   SER A  41       3.956   5.797   7.974  1.00  9.00           N
ATOM      2  CA  SER A  41       4.592   4.986   9.005  1.00  9.00           C
ATOM      3  C   SER A  41       5.860   5.653   9.528  1.00  9.00           C
ATOM      4  O   SER A  41       6.364   5.301  10.594  1.00  9.00           O
ATOM      5  CB  SER A  41       4.916   3.591   8.466  1.00  9.00           C
ATOM      6  OG  SER A  41       5.796   3.665   7.357  1.00  9.00           O

ATOM      7  N  ALYS A  42       6.370   6.618   8.770  0.30  9.00           N
ATOM      8  CA ALYS A  42       7.579   7.336   9.155  0.30  9.00           C
ATOM      9  C  ALYS A  42       7.247   8.721   9.702  0.30  9.00           C
ATOM     10  O  ALYS A  42       8.140   9.480  10.079  0.30  9.00           O

ATOM     11  N  BLYS A  42       6.370   6.618   8.770  0.20  9.00           N
ATOM     12  CA BLYS A  42       7.579   7.336   9.155  0.20  9.00           C
ATOM     13  C  BLYS A  42       7.246   8.716   9.714  0.20  9.00           C
ATOM     14  O  BLYS A  42       7.912   9.203  10.628  0.20  9.00           O

ATOM     15  N  CLYS A  42       6.370   6.618   8.770  0.10  9.00           N
ATOM     16  CA CLYS A  42       7.579   7.336   9.155  0.10  9.00           C
ATOM     17  C  CLYS A  42       7.265   8.778   9.542  0.10  9.00           C
ATOM     18  O  CLYS A  42       8.168   9.595   9.715  0.10  9.00           O

ATOM     19  N  DLYS A  42       7.691   7.728   7.602  0.40  9.00           N
ATOM     20  CA DLYS A  42       8.656   7.951   8.671  0.40  9.00           C
ATOM     21  C  DLYS A  42       8.168   9.023   9.640  0.40  9.00           C
ATOM     22  O  DLYS A  42       8.819   9.308  10.645  0.40  9.00           O


ATOM     23  N  AGLU A  43       5.959   9.043   9.741  0.20  9.00           N
ATOM     24  CA AGLU A  43       5.507  10.337  10.240  0.20  9.00           C
ATOM     25  C  AGLU A  43       5.468  10.356  11.765  0.20  9.00           C
ATOM     26  O  AGLU A  43       4.455  10.009  12.373  0.20  9.00           O

ATOM     27  N  BGLU A  43       6.213   9.341   9.159  0.40  9.00           N
ATOM     28  CA BGLU A  43       5.791  10.664   9.601  0.40  9.00           C
ATOM     29  C  BGLU A  43       4.945  10.579  10.867  0.40  9.00           C
ATOM     30  O  BGLU A  43       5.473  10.424  11.968  0.40  9.00           O

ATOM     31  N  CGLU A  43       5.978   9.082   9.676  0.10  9.00           N
ATOM     32  CA CGLU A  43       5.542  10.425  10.042  0.10  9.00           C
ATOM     33  C  CGLU A  43       5.317  10.540  11.546  0.10  9.00           C
ATOM     34  O  CGLU A  43       4.293  10.096  12.066  0.10  9.00           O

ATOM     35  N  DGLU A  43       7.019   9.614   9.330  0.30  9.00           N
ATOM     36  CA DGLU A  43       6.442  10.655  10.172  0.30  9.00           C
ATOM     37  C  DGLU A  43       5.615  10.054  11.303  0.30  9.00           C
ATOM     38  O  DGLU A  43       5.332  10.720  12.300  0.30  9.00           O
TER
END
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Occupancies.
  Make sure can refine back to answer within 1.e-6 accuracy.
  Adjucent residues have the same number of conformers.
  """
  pdb_file_answer = "%s_answer.pdb"%prefix
  pdb_inp = iotbx.pdb.input(source_info=None, lines=pdb_str)
  ph = pdb_inp.construct_hierarchy()
  xrs_answer = pdb_inp.xray_structure_simple()
  xrs_answer.scattering_type_registry(table = "wk1995")
  ph.write_pdb_file(file_name = pdb_file_answer)
  #
  pdb_file_poor = "%s_poor.pdb"%prefix
  xrs_poor = xrs_answer.deep_copy_scatterers()
  xrs_poor.set_occupancies(value=1)
  ph.adopt_xray_structure(xrs_poor)
  ph.write_pdb_file(file_name = pdb_file_poor)
  #
  for d_min in [1,2,3]:
    f_obs = abs(xrs_answer.structure_factors(d_min=d_min,
      algorithm="direct").f_calc())
    mtz_dataset = f_obs.as_mtz_dataset(column_root_label="F-obs")
    mtz_dataset.add_miller_array(miller_array=f_obs.generate_r_free_flags(),
      column_root_label="R-free-flags")
    mtz_object = mtz_dataset.mtz_object()
    data_file = "%s_data.mtz"%prefix
    mtz_object.write(file_name = data_file)
    ref_prefix = "%s_d_min_%s"%(prefix, str(d_min))
    args = [
      pdb_file_poor,
      data_file,
      'main.number_of_mac=5',
      'refine.strategy=occupancies',
      'main.bulk_solv=false',
      "structure_factors_and_gradients_accuracy.algorithm=direct",
      "main.scattering_table=wk1995",
      'xray_data.r_free_flags.ignore_r_free_flags=true',
      "xray_data.outliers_rejection=false",
      "main.target=ls",
      "ls_target_names.target_name=ls_wunit_kunit"
      ]
    r = run_phenix_refine(args = args, prefix=prefix)
    pdb_inp = iotbx.pdb.input(file_name=r.pdb)
    ph = pdb_inp.construct_hierarchy()
    occs_rg = []
    for m in ph.models():
      for c in m.chains():
        for rg in c.residue_groups():
          occs = flex.double()
          for con in rg.conformers():
            o = con.atoms().extract_occ()
            occs.append(flex.mean(o))
          assert approx_equal(flex.sum(occs), 1.0)
          occs_rg.append(occs)
    assert approx_equal(occs_rg[1], occs_rg[2])

if(__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK")
  print("Time: %8.3f"%(time.time()-t0))
