from __future__ import division
from __future__ import print_function
import libtbx.load_env
import os, time
from phenix_regression.refinement import run_phenix_refine
import iotbx.pdb
from libtbx.test_utils import approx_equal
from phenix_regression.refinement import run_fmodel
from scitbx.array_family import flex
import mmtbx.model

pdb_str = """
CRYST1   37.760   27.626   25.852  90.00  90.00  90.00 P 1
ATOM      6  N   GLU A  15      25.146   6.450  15.463  1.00  9.36           N
ATOM      7  CA  GLU A  15      25.986   7.220  16.361  1.00  9.27           C
ATOM      8  C   GLU A  15      25.721   8.699  16.097  1.00  9.44           C
ATOM      9  O   GLU A  15      26.012   9.206  15.007  1.00  9.94           O
ATOM     10  CB  GLU A  15      27.470   6.814  16.189  1.00  9.64           C
ATOM     11  CG  GLU A  15      28.226   6.689  17.506  1.00 10.21           C
ATOM     12  CD  GLU A  15      28.226   7.934  18.322  1.00 10.28           C
ATOM     13  OE1 GLU A  15      28.278   9.045  17.729  1.00 10.96           O
ATOM     14  OE2 GLU A  15      28.186   7.812  19.581  1.00 11.33           O
ATOM     24  N   ASP A  24      26.719  19.091  10.101  1.00  9.27           N
ATOM     25  CA  ASP A  24      27.059  20.449  10.440  1.00  9.45           C
ATOM     26  C   ASP A  24      28.231  20.964   9.596  1.00  9.21           C
ATOM     27  O   ASP A  24      28.135  22.010   8.965  1.00  9.69           O
ATOM     28  CB  ASP A  24      27.325  20.470  11.953  1.00  9.54           C
ATOM     29  CG  ASP A  24      27.680  21.791  12.544  1.00  9.46           C
ATOM     30  OD1 ASP A  24      28.315  22.626  11.925  1.00 10.66           O
ATOM     31  OD2 ASP A  24      27.319  21.915  13.787  1.00 10.60           O
remark ATOM     39  N  ALYS A  41       9.559   9.840   8.565  0.48 19.64           N
remark ATOM     40  CA ALYS A  41       9.643  11.208   8.968  0.48 17.69           C
remark ATOM     41  C  ALYS A  41       9.895  11.391  10.471  0.48 15.76           C
remark ATOM     42  O  ALYS A  41      10.366  12.424  10.899  0.48 18.30           O
remark ATOM     43  CB ALYS A  41       8.320  11.886   8.608  0.48 20.38           C
remark ATOM     44  CG ALYS A  41       8.082  12.032   7.130  0.48 22.87           C
remark ATOM     45  CD ALYS A  41       6.673  12.507   6.782  0.48 27.24           C
remark ATOM     46  CE ALYS A  41       6.418  12.213   5.333  0.48 32.50           C
remark ATOM     47  NZ ALYS A  41       5.000  12.519   5.000  0.48 42.53           N
remark ATOM     48  N  BLYS A  41       9.713  10.012   8.496  0.52 19.61           N
remark ATOM     49  CA BLYS A  41      10.122  11.334   9.018  0.52 18.77           C
remark ATOM     50  C  BLYS A  41       9.876  11.458  10.537  0.52 23.43           C
remark ATOM     51  O  BLYS A  41       9.910  12.563  11.056  0.52 31.42           O
remark ATOM     52  CB BLYS A  41       9.445  12.518   8.262  0.52 20.27           C
remark ATOM     53  CG BLYS A  41      10.072  13.938   8.440  0.52 24.36           C
remark ATOM     54  CD BLYS A  41       9.176  14.961   7.767  0.52 29.24           C
remark ATOM     55  CE BLYS A  41       9.681  16.375   7.921  0.52 31.22           C
remark ATOM     56  NZ BLYS A  41      10.174  16.795   9.263  0.52 46.70           N
TER
END
"""

phil_str = """
refinement {
  pdb_interpretation {
    apply_cif_restraints {
      restraints_file_name = %s
      residue_selection = chain A and resseq 15
    }
    apply_cif_restraints {
      restraints_file_name = %s
      residue_selection = chain A and resseq 24
    }
  }
}
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Test apply custom restraints in presence of altlocs.
  """
  with open("%s.pdb"%prefix,"w") as fo:
    fo.write(pdb_str)
    
  cif_a = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/data/%s_a.cif"%prefix,
    test=os.path.isfile)
  cif_b = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/data/%s_b.cif"%prefix,
    test=os.path.isfile)
  with open("%s.params"%(prefix),"w") as fo:
    fo.write(phil_str%(cif_a,cif_b))
  #
  args = [
    "%s.pdb"%prefix,
    "high_resolution=3",
    "type=real r_free=0.1"]
  r = run_fmodel(args = args, prefix = prefix)
  #
  args = [
    "%s.pdb"%prefix,
    r.mtz,
    "wxc_scale=0",
    "%s.params"%prefix,
    "strategy=individual_sites",
    ]
  cmd = "phenix.refine "+" ".join(args)
  r = run_phenix_refine(args = args, prefix = prefix, 
    dff_expected=False, pdb_expected=False, cif_expected=False,
    mtz_expected=False)
  #
  found_bonds = 0
  found_angles = 0
  with open("%s_001.geo"%prefix) as fo:
    for l in fo.readlines():
      l = l.strip()
      if l.startswith("1.249"):
        found_bonds += 1
        assert "1.00e+01" in l
      if l.startswith("118.40"):
        found_angles += 1
        assert "1.00e+01" in l
  assert found_bonds == 4
  assert found_angles == 4

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
