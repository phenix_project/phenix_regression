from __future__ import division
from __future__ import print_function
from phenix_regression.refinement import run_phenix_refine
import libtbx.load_env
import os
import iotbx.pdb
from libtbx.test_utils import approx_equal
from scitbx.array_family import flex

par_str="""\
refinement.main.number_of_macro_cycles=20
refinement.main.bulk_solvent_and_scale=false
refinement.main.scattering_table=wk1995
refinement.structure_factors_and_gradients_accuracy.algorithm=direct
refinement.fake_f_obs.structure_factors_accuracy.algorithm=direct
refinement.refine.strategy=rigid_body
refinement.main.target=ls
refinement.rigid_body.bulk_solvent_and_scale=false
refinement.main.fake_f_obs=true
data_manager.fmodel.xray_data.high_resolution=1.5
refinement.rigid_body.min_number_of_reflections=200
refinement.refine.sites.rigid_body=chain B
refinement.refine.sites.rigid_body=chain A
refinement.refine.sites.rigid_body=chain E
refinement.refine.sites.rigid_body=chain C
refinement.refine.sites.rigid_body=chain D
"""
def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Rigid-body refinement.
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/enk_rbr.pdb", test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/enk_rbr.pdb_fcalc_direct_wk1995_resolution_1.0.hkl", \
    test=os.path.isfile)
  #
  par = "%s.params"%prefix
  open(par, "w").write(par_str)
  args = [pdb, hkl, par]
  r = run_phenix_refine(args = args, prefix=prefix)
  #
  r.check_start_r(r_work=0, r_free=0, eps=0.001)
  r.check_final_r(r_work=0, r_free=0, eps=0.001, info_low_eps=0.01)
  assert approx_equal(r.bond_start, r.bond_final)
  assert approx_equal(r.angle_start, r.angle_final)
  a1 = iotbx.pdb.input(file_name=  pdb).construct_hierarchy().atoms()
  a2 = iotbx.pdb.input(file_name=r.pdb).construct_hierarchy().atoms()
  dist = flex.mean(flex.sqrt((a1.extract_xyz() - a2.extract_xyz()).dot()))
  assert approx_equal(dist, 0, 1.e-3)
  assert approx_equal(flex.mean(flex.abs(a1.extract_b()-a2.extract_b())), 0)

if (__name__ == "__main__"):
  run()
  print("OK")
