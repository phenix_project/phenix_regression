
from __future__ import division
from __future__ import print_function
from libtbx.utils import null_out
import libtbx.load_env
import os.path
from libtbx import easy_run

def exercise () :
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/1yjp_h.pdb",
    test=os.path.isfile)
  mtz_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/1yjp.mtz",
    test=os.path.isfile)
  assert (not None in [pdb_file, mtz_file])
  args = [
    pdb_file,
    mtz_file,
    'refinement.main.number_of_macro_cycles=1',
    "output.prefix=tst_rotamer_restraints",
    'write_final_geo=True',
    'rotamer_restraints=true',
    'overwrite=true',
  ]
  # Rotamer restraints are used
  cmd = 'phenix.refine '+' '.join(args)
  print(cmd)
  easy_run.go(cmd)

  geo = 'tst_rotamer_restraints_001_final.geo'
  assert os.path.exists(geo)
  f = open(geo, 'r')
  lines=f.read()
  del f
  side = 'Dihedral angle | Side chain | restraints: 14'
  assert lines.find(side)>-1

  # Rotamer restraints are NOT used
  cmd += ' rotamer_restraints=False'
  print(cmd)
  easy_run.go(cmd)

  geo = 'tst_rotamer_restraints_001_final.geo'
  assert os.path.exists(geo)
  f = open(geo, 'r')
  lines=f.read()
  del f
  side = 'Dihedral angle | Side chain | restraints:'
  assert lines.find(side)==-1, 'There should be no side chain restraints'


if (__name__ == "__main__") :
  exercise()
  print("OK")
