from __future__ import division
from __future__ import print_function
import libtbx.load_env
import os, time
from phenix_regression.refinement import run_phenix_refine
import iotbx.pdb
from libtbx.test_utils import approx_equal
from phenix_regression.refinement import run_fmodel
from scitbx.array_family import flex
import mmtbx.model

pdb_str_prot = """
CRYST1   34.486   15.250   23.658  90.00  90.00  90.00 P 1
ATOM      1  N   GLY A   1       7.219   9.790  12.193  1.00 10.00           N  
ATOM      2  CA  GLY A   1       7.176   9.385  10.742  1.00 10.00           C  
ATOM      3  C   GLY A   1       8.213   8.318  10.510  1.00 10.00           C  
ATOM      4  O   GLY A   1       8.705   7.699  11.472  1.00 10.00           O  
ATOM      5  N   ASN A   2       8.572   8.101   9.246  1.00 10.00           N  
ATOM      6  CA  ASN A   2       9.706   7.216   8.922  1.00 10.00           C  
ATOM      7  C   ASN A   2      10.987   7.715   9.518  1.00 10.00           C  
ATOM      8  O   ASN A   2      11.250   8.920   9.517  1.00 10.00           O  
ATOM      9  CB  ASN A   2       9.882   7.059   7.432  1.00 10.00           C  
ATOM     10  CG  ASN A   2       8.644   6.520   6.783  1.00 10.00           C  
ATOM     11  OD1 ASN A   2       8.203   5.405   7.107  1.00 10.00           O  
ATOM     12  ND2 ASN A   2       8.024   7.333   5.922  1.00 10.00           N  
ATOM     13  N   ASN A   3      11.790   6.768   9.996  1.00 10.00           N  
ATOM     14  CA  ASN A   3      13.035   7.082  10.680  1.00 10.00           C  
ATOM     15  C   ASN A   3      14.273   6.510   9.986  1.00 10.00           C  
ATOM     16  O   ASN A   3      14.356   5.297   9.739  1.00 10.00           O  
ATOM     17  CB  ASN A   3      12.969   6.556  12.133  1.00 10.00           C  
ATOM     18  CG  ASN A   3      14.222   6.917  12.952  1.00 10.00           C  
ATOM     19  OD1 ASN A   3      14.526   8.103  13.163  1.00 10.00           O  
ATOM     20  ND2 ASN A   3      14.957   5.893  13.397  1.00 10.00           N  
ATOM     21  N   GLN A   4      15.223   7.406   9.689  1.00 10.00           N  
ATOM     22  CA  GLN A   4      16.612   7.066   9.290  1.00 10.00           C  
ATOM     23  C   GLN A   4      17.663   7.784  10.179  1.00 10.00           C  
ATOM     24  O   GLN A   4      17.775   9.021  10.206  1.00 10.00           O  
ATOM     25  CB  GLN A   4      16.884   7.326   7.802  1.00 10.00           C  
ATOM     26  CG  GLN A   4      18.172   6.636   7.304  1.00 10.00           C  
ATOM     27  CD  GLN A   4      18.732   7.222   6.002  1.00 10.00           C  
ATOM     28  OE1 GLN A   4      18.972   8.446   5.901  1.00 10.00           O  
ATOM     29  NE2 GLN A   4      18.978   6.339   5.000  1.00 10.00           N  
ATOM     30  N   GLN A   5      18.382   6.999  10.962  1.00 10.00           N  
ATOM     31  CA  GLN A   5      19.498   7.539  11.731  1.00 10.00           C  
ATOM     32  C   GLN A   5      20.822   6.946  11.263  1.00 10.00           C  
ATOM     33  O   GLN A   5      20.996   5.724  11.145  1.00 10.00           O  
ATOM     34  CB  GLN A   5      19.284   7.361  13.238  1.00 10.00           C  
ATOM     35  CG  GLN A   5      18.057   8.128  13.738  1.00 10.00           C  
ATOM     36  CD  GLN A   5      17.572   7.592  15.045  1.00 10.00           C  
ATOM     37  OE1 GLN A   5      17.002   6.503  15.093  1.00 10.00           O  
ATOM     38  NE2 GLN A   5      17.777   8.365  16.130  1.00 10.00           N  
ATOM     39  N   ASN A   6      21.742   7.842  10.947  1.00 10.00           N  
ATOM     40  CA  ASN A   6      23.059   7.488  10.409  1.00 10.00           C  
ATOM     41  C   ASN A   6      24.082   7.939  11.415  1.00 10.00           C  
ATOM     42  O   ASN A   6      24.447   9.121  11.465  1.00 10.00           O  
ATOM     43  CB  ASN A   6      23.293   8.194   9.084  1.00 10.00           C  
ATOM     44  CG  ASN A   6      22.189   7.913   8.094  1.00 10.00           C  
ATOM     45  OD1 ASN A   6      22.026   6.782   7.642  1.00 10.00           O  
ATOM     46  ND2 ASN A   6      21.423   8.925   7.770  1.00 10.00           N  
ATOM     47  N   TYR A   7      24.520   6.995  12.238  1.00 10.00           N  
ATOM     48  C   TYR A   7      26.831   7.509  12.976  1.00 10.00           C  
ATOM     49  O   TYR A   7      27.269   6.989  11.946  1.00 10.00           O  
ATOM     50  CA ATYR A   7      25.387   7.322  13.390  0.50 10.00           C  
ATOM     51  CB ATYR A   7      25.289   6.243  14.460  0.50 10.00           C  
ATOM     52  CG ATYR A   7      23.893   6.107  14.993  0.50 10.00           C  
ATOM     53  CD2ATYR A   7      22.999   5.199  14.418  0.50 10.00           C  
ATOM     54  CD1ATYR A   7      23.438   6.934  16.011  0.50 10.00           C  
ATOM     55  CE2ATYR A   7      21.708   5.084  14.887  0.50 10.00           C  
ATOM     56  CE1ATYR A   7      22.132   6.827  16.507  0.50 10.00           C  
ATOM     57  CZ ATYR A   7      21.275   5.907  15.922  0.50 10.00           C  
ATOM     58  OH ATYR A   7      19.994   5.767  16.382  0.50 10.00           O  
ATOM     59  OXTATYR A   7      27.586   8.177  13.703  0.50 10.00           O  
ATOM     60  CA BTYR A   7      25.487   7.322  13.390  0.50 10.00           C  
ATOM     61  CB BTYR A   7      25.389   6.243  14.460  0.50 10.00           C  
ATOM     62  CG BTYR A   7      26.380   6.458  15.566  0.50 10.00           C  
ATOM     63  CD1BTYR A   7      26.152   7.430  16.544  0.50 10.00           C  
ATOM     64  CD2BTYR A   7      27.577   5.755  15.593  0.50 10.00           C  
ATOM     65  CE1BTYR A   7      27.069   7.649  17.550  0.50 10.00           C  
ATOM     66  CE2BTYR A   7      28.518   5.958  16.611  0.50 10.00           C  
ATOM     67  CZ BTYR A   7      28.254   6.921  17.574  0.50 10.00           C  
ATOM     68  OH BTYR A   7      29.144   7.144  18.589  0.50 10.00           O  
ATOM     69  OXTBTYR A   7      27.686   8.177  13.703  0.50 10.00           O 
TER
"""

pdb_str_for_fobs = """
%s
HETATM   70  O  AHOH S   1       5.000   9.728  12.654  0.50 10.00           O  
HETATM   71  O  BHOH S   2       5.692  10.250  13.724  0.50 10.00           O  
HETATM   72  O   HOH S   3      26.659   7.036   9.307  1.00 10.00           O  
HETATM   73  O   HOH S   4       6.200   5.995   5.661  1.00 10.00           O  
HETATM   74  O   HOH S   5      28.036   9.357  16.061  1.00 10.00           O  
HETATM   75  O  AHOH S   6      19.001   5.705  16.564  0.80 10.00           O  
HETATM   76  O  XHOH S   7      29.485   7.764  17.503  0.40 10.00           O  
HETATM   78  O   HOH S   9      29.209   3.996  15.960  1.00 10.00           O  
HETATM   79  O   HOH S  10      13.863   5.233  18.658  1.00 10.00           O  
HETATM   80  O   HOH S  11      15.228   5.860  17.058  1.00 10.00           O 
HETATM   77  O   HOH S  12      11.559  10.005  15.413  1.00 10.00           O
HETATM   80  O   HOH S  13      11.457  10.067   9.363  1.00 10.00           O

END
"""%pdb_str_prot

# This is 'answer' in terms of atom content, not altloc labels or occ.
pdb_str_answer = """
HETATM   70  O  AHOH S   1       5.000   9.728  12.654  0.50 10.00           O  
HETATM   71  O  BHOH S   2       5.692  10.250  13.724  0.50 10.00           O  
HETATM   72  O   HOH S   3      26.659   7.036   9.307  1.00 10.00           O  
HETATM   73  O   HOH S   4       6.200   5.995   5.661  1.00 10.00           O  
HETATM   74  O   HOH S   5      28.036   9.357  16.061  1.00 10.00           O  
HETATM   75  O  AHOH S   6      19.001   5.705  16.564  0.80 10.00           O  
HETATM   76  O  XHOH S   7      29.485   7.764  17.503  0.40 10.00           O  
HETATM   79  O   HOH S   8      13.863   5.233  18.658  1.00 10.00           O  
HETATM   80  O   HOH S   9      15.228   5.860  17.058  1.00 10.00           O 
%s
"""%pdb_str_prot

def check_match(f1,f2):
  # 
  # Check both structures match. Has to do all vs all since labels may be 
  # different.
  #
  def _get_atoms(f):
    m = mmtbx.model.manager(model_input = iotbx.pdb.input(f))
    return m.select(m.solvent_selection()).get_hierarchy().atoms()
  atoms_1 = _get_atoms(f1)
  atoms_2 = _get_atoms(f2)
  assert atoms_1.size() == atoms_2.size()
  diff = flex.double()
  for a1 in atoms_1:
    tmp = flex.double()
    for a2 in atoms_2:
      tmp.append(a1.distance(a2))
    diff.append(flex.min(tmp))
  result = flex.max(diff)
  assert result < 0.2, result # Can't be larger!

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Comprehensive tests for water (ordered solvent) picking: no H.
  Deviations from expected water possitions should be very small = DO NOT change
  tolerances or expected values before thorough investigation.
  """
  with open("%s_for_fobs.pdb"%prefix,"w") as fo:
    fo.write(pdb_str_for_fobs)
  with open("%s_noW.pdb"%prefix,"w") as fo:
    fo.write(pdb_str_prot)
  with open("%s_answer.pdb"%prefix,"w") as fo:
    fo.write(pdb_str_answer)
  #
  args = [
    "%s_for_fobs.pdb"%prefix,
    "high_resolution=1.2",
    "type=real r_free=0.05"]
  r1 = run_fmodel(args = args, prefix = prefix+"_for_fobs")
  #
  args = [
    "%s_answer.pdb"%prefix,
    "high_resolution=1.2",
    "type=real r_free=0.05"]
  r2 = run_fmodel(args = args, prefix = prefix+"_answer")
  #
  # Refinement 1
  #
  args = [
    "%s_for_fobs.pdb"%prefix,
    r1.mtz,
    "const_shrink_donor_acceptor=0.6",
    'strategy=individual_sites+occupancies',
    'nqh_flips=false',
    'xray_data.r_free_flags.ignore_r_free_flags=True',
    'main.number_of_mac=2',
    "ordered_solvent.mode=every_macro_cycle",
    'ordered_solvent=True',
    "include_altlocs=True",
    "ordered_solvent.refine_oat=True",
    "primary_map_cutoff=10"
    ]
  cmd = "phenix.refine "+" ".join(args)
  print(cmd)
  r = run_phenix_refine(args = args, prefix = prefix+"_1")
  check_match(f1="%s_answer.pdb"%prefix, f2=r.pdb)
  #
  # Refinement 2
  #
  args = [
    "%s_noW.pdb"%prefix,
    r1.mtz,
    "const_shrink_donor_acceptor=0.6",
    'strategy=individual_sites',
    'nqh_flips=false',
    'xray_data.r_free_flags.ignore_r_free_flags=True',
    'ordered_solvent=True',
    'main.number_of_mac=2',
    "include_altlocs=True",
    "ordered_solvent.refine_oat=True",
    "ordered_solvent.mode=every_macro_cycle",
    "primary_map_cutoff=6"
    ]
  cmd = "phenix.refine "+" ".join(args)
  print(cmd)
  r = run_phenix_refine(args = args, prefix = prefix+"_2")
  check_match(f1="%s_answer.pdb"%prefix, f2=r.pdb)
  #
  # Refinement 3
  #
  args = [
    "%s_answer.pdb"%prefix,
    r2.mtz,
    "const_shrink_donor_acceptor=0.6",
    'strategy=individual_sites+occupancies',
    'nqh_flips=false',
    'xray_data.r_free_flags.ignore_r_free_flags=True',
    'main.number_of_mac=1',
    'ordered_solvent=True',
    "include_altlocs=True",
    "ordered_solvent.refine_oat=True",
    "primary_map_cutoff=6"
    ]
  cmd = "phenix.refine "+" ".join(args)
  print(cmd)
  r = run_phenix_refine(args = args, prefix = prefix+"_3")
  check_match(f1="%s_answer.pdb"%prefix, f2=r.pdb)
  #
  # Refinement 4
  #
  args = [
    "%s_noW.pdb"%prefix,
    r2.mtz,
    "const_shrink_donor_acceptor=0.6",
    'strategy=individual_sites+occupancies',
    'nqh_flips=false',
    'xray_data.r_free_flags.ignore_r_free_flags=True',
    "ordered_solvent.mode=every_macro_cycle",
    'main.number_of_mac=2',
    'ordered_solvent=True',
    "include_altlocs=True",
    "ordered_solvent.refine_oat=True",
    "primary_map_cutoff=6"
    ]
  cmd = "phenix.refine "+" ".join(args)
  print(cmd)
  r = run_phenix_refine(args = args, prefix = prefix+"_4")
  check_match(f1="%s_answer.pdb"%prefix, f2=r.pdb)

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
