from __future__ import division
from __future__ import print_function
import libtbx.load_env
import os, time
from phenix_regression.refinement import run_phenix_refine
import iotbx.pdb
from libtbx.test_utils import approx_equal
from phenix_regression.refinement import run_fmodel
from scitbx.array_family import flex
import mmtbx.model

pdb_str_prot = """
CRYST1   21.937    4.866   23.477  90.00 107.08  90.00 P 1 21 1      2          
ATOM      2  N   GLY A   1      -9.009   4.612   6.102  1.00 16.77           N
ATOM      3  CA  GLY A   1      -9.052   4.207   4.651  1.00 16.57           C
ATOM      4  C   GLY A   1      -8.015   3.140   4.419  1.00 16.16           C
ATOM      5  O   GLY A   1      -7.523   2.521   5.381  1.00 16.78           O
ATOM      6  H1  GLY A   1      -9.832   4.590   6.441  1.00 16.77           H
ATOM      7  H2  GLY A   1      -8.488   4.048   6.552  1.00 16.77           H
ATOM      8  H3  GLY A   1      -8.682   5.437   6.170  1.00 16.77           H
ATOM      9  HA2 GLY A   1      -9.928   3.854   4.429  1.00 16.57           H
ATOM     10  HA3 GLY A   1      -8.860   4.969   4.083  1.00 16.57           H
ATOM     11  N   ASN A   2      -7.656   2.923   3.155  1.00 15.02           N
ATOM     12  CA  ASN A   2      -6.522   2.038   2.831  1.00 14.10           C
ATOM     13  C   ASN A   2      -5.241   2.537   3.427  1.00 13.13           C
ATOM     14  O   ASN A   2      -4.978   3.742   3.426  1.00 11.91           O
ATOM     15  CB  ASN A   2      -6.346   1.881   1.341  1.00 15.38           C
ATOM     16  CG  ASN A   2      -7.584   1.342   0.692  1.00 14.08           C
ATOM     17  OD1 ASN A   2      -8.025   0.227   1.016  1.00 17.46           O
ATOM     18  ND2 ASN A   2      -8.204   2.155  -0.169  1.00 11.72           N
ATOM     19  H   ASN A   2      -8.044   3.269   2.470  1.00 15.02           H
ATOM     20  HA  ASN A   2      -6.698   1.159   3.202  1.00 14.10           H
ATOM     21  HB2 ASN A   2      -6.150   2.746   0.949  1.00 15.38           H
ATOM     22  HB3 ASN A   2      -5.619   1.262   1.169  1.00 15.38           H
ATOM     23 HD21 ASN A   2      -8.923   1.896  -0.563  1.00 11.72           H
ATOM     24 HD22 ASN A   2      -7.868   2.925  -0.352  1.00 11.72           H
ATOM     25  N   ASN A   3      -4.438   1.590   3.905  1.00 12.26           N
ATOM     26  CA  ASN A   3      -3.193   1.904   4.589  1.00 11.74           C
ATOM     27  C   ASN A   3      -1.955   1.332   3.895  1.00 11.10           C
ATOM     28  O   ASN A   3      -1.872   0.119   3.648  1.00 10.42           O
ATOM     29  CB  ASN A   3      -3.271   1.348   6.030  1.00 12.15           C
ATOM     30  CG  ASN A   3      -2.003   1.648   6.851  1.00 12.82           C
ATOM     31  OD1 ASN A   3      -1.289   0.726   7.280  1.00 15.05           O
ATOM     32  ND2 ASN A   3      -1.716   2.937   7.053  1.00 13.48           N
ATOM     33  H   ASN A   3      -4.597   0.747   3.843  1.00 12.26           H
ATOM     34  HA  ASN A   3      -3.090   2.867   4.643  1.00 11.74           H
ATOM     35  HB2 ASN A   3      -4.026   1.753   6.485  1.00 12.15           H
ATOM     36  HB3 ASN A   3      -3.383   0.385   5.992  1.00 12.15           H
ATOM     37 HD21 ASN A   3      -1.018   3.156   7.504  1.00 13.48           H
ATOM     38 HD22 ASN A   3      -2.233   3.547   6.737  1.00 13.48           H
ATOM     39  N   GLN A   4      -1.005   2.228   3.598  1.00 10.29           N
ATOM     40  CA  GLN A   4       0.384   1.888   3.199  1.00 10.53           C
ATOM     41  C   GLN A   4       1.435   2.606   4.088  1.00 10.24           C
ATOM     42  O   GLN A   4       1.547   3.843   4.115  1.00  8.86           O
ATOM     43  CB  GLN A   4       0.656   2.148   1.711  1.00  9.80           C
ATOM     44  CG  GLN A   4       1.944   1.458   1.213  1.00 10.25           C
ATOM     45  CD  GLN A   4       2.504   2.044  -0.089  1.00 12.43           C
ATOM     46  OE1 GLN A   4       2.744   3.268  -0.190  1.00 14.62           O
ATOM     47  NE2 GLN A   4       2.750   1.161  -1.091  1.00  9.05           N
ATOM     48  H   GLN A   4      -1.142   3.077   3.620  1.00 10.29           H
ATOM     49  HA  GLN A   4       0.502   0.937   3.339  1.00 10.53           H
ATOM     50  HB2 GLN A   4      -0.088   1.808   1.189  1.00  9.80           H
ATOM     51  HB3 GLN A   4       0.753   3.103   1.569  1.00  9.80           H
ATOM     52  HG2 GLN A   4       2.630   1.544   1.893  1.00 10.25           H
ATOM     53  HG3 GLN A   4       1.754   0.520   1.057  1.00 10.25           H
ATOM     54 HE21 GLN A   4       2.577   0.326  -0.981  1.00  9.05           H
ATOM     55 HE22 GLN A   4       3.067   1.439  -1.841  1.00  9.05           H
ATOM     56  N   GLN A   5       2.154   1.821   4.871  1.00 10.38           N
ATOM     57  CA  GLN A   5       3.270   2.361   5.640  1.00 11.39           C
ATOM     58  C   GLN A   5       4.594   1.768   5.172  1.00 11.52           C
ATOM     59  O   GLN A   5       4.768   0.546   5.054  1.00 12.05           O
ATOM     60  CB  GLN A   5       3.056   2.183   7.147  1.00 11.96           C
ATOM     61  CG  GLN A   5       1.829   2.950   7.647  1.00 10.81           C
ATOM     62  CD  GLN A   5       1.344   2.414   8.954  1.00 13.10           C
ATOM     63  OE1 GLN A   5       0.774   1.325   9.002  1.00 10.65           O
ATOM     64  NE2 GLN A   5       1.549   3.187  10.039  1.00 12.30           N
ATOM     65  H   GLN A   5       2.021   0.978   4.977  1.00 10.38           H
ATOM     66  HA  GLN A   5       3.322   3.315   5.479  1.00 11.39           H
ATOM     67  HB2 GLN A   5       2.923   1.242   7.340  1.00 11.96           H
ATOM     68  HB3 GLN A   5       3.834   2.514   7.621  1.00 11.96           H
ATOM     69  HG2 GLN A   5       2.063   3.883   7.769  1.00 10.81           H
ATOM     70  HG3 GLN A   5       1.111   2.866   7.000  1.00 10.81           H
ATOM     71 HE21 GLN A   5       1.950   3.944   9.959  1.00 12.30           H
ATOM     72 HE22 GLN A   5       1.285   2.920  10.813  1.00 12.30           H
ATOM     73  N   ASN A   6       5.514   2.664   4.856  1.00 11.99           N
ATOM     74  CA  ASN A   6       6.831   2.310   4.318  1.00 12.30           C
ATOM     75  C   ASN A   6       7.854   2.761   5.324  1.00 13.40           C
ATOM     76  O   ASN A   6       8.219   3.943   5.374  1.00 13.92           O
ATOM     77  CB  ASN A   6       7.065   3.016   2.993  1.00 12.13           C
ATOM     78  CG  ASN A   6       5.961   2.735   2.003  1.00 12.77           C
ATOM     79  OD1 ASN A   6       5.798   1.604   1.551  1.00 14.27           O
ATOM     80  ND2 ASN A   6       5.195   3.747   1.679  1.00 10.07           N
ATOM     81  H   ASN A   6       5.401   3.512   4.945  1.00 11.99           H
ATOM     82  HA  ASN A   6       6.903   1.351   4.191  1.00 12.30           H
ATOM     83  HB2 ASN A   6       7.100   3.974   3.143  1.00 12.13           H
ATOM     84  HB3 ASN A   6       7.901   2.706   2.610  1.00 12.13           H
ATOM     85 HD21 ASN A   6       4.551   3.637   1.120  1.00 10.07           H
ATOM     86 HD22 ASN A   6       5.340   4.523   2.020  1.00 10.07           H
ATOM     87  N   TYR A   7       8.292   1.817   6.147  1.00 14.70           N
ATOM     88  CA  TYR A   7       9.159   2.144   7.299  1.00 15.18           C
ATOM     89  C   TYR A   7      10.603   2.331   6.885  1.00 15.91           C
ATOM     90  O   TYR A   7      11.041   1.811   5.855  1.00 15.76           O
ATOM     91  CB  TYR A   7       9.061   1.065   8.369  1.00 15.35           C
ATOM     92  CG  TYR A   7       7.665   0.929   8.902  1.00 14.45           C
ATOM     93  CD1 TYR A   7       7.210   1.756   9.920  1.00 14.80           C
ATOM     94  CD2 TYR A   7       6.771   0.021   8.327  1.00 15.68           C
ATOM     95  CE1 TYR A   7       5.904   1.649  10.416  1.00 14.33           C
ATOM     96  CE2 TYR A   7       5.480  -0.094   8.796  1.00 13.46           C
ATOM     97  CZ  TYR A   7       5.047   0.729   9.831  1.00 15.09           C
ATOM     98  OH  TYR A   7       3.766   0.589  10.291  1.00 14.39           O
ATOM     99  OXT TYR A   7      11.358   2.999   7.612  1.00 17.49           O
ATOM    100  H   TYR A   7       8.109   0.980   6.070  1.00 14.70           H
ATOM    101  HA  TYR A   7       8.852   2.975   7.692  1.00 15.18           H
ATOM    102  HB2 TYR A   7       9.327   0.215   7.987  1.00 15.35           H
ATOM    103  HB3 TYR A   7       9.645   1.296   9.108  1.00 15.35           H
ATOM    104  HD1 TYR A   7       7.794   2.369  10.304  1.00 14.80           H
ATOM    105  HD2 TYR A   7       7.058  -0.530   7.635  1.00 15.68           H
ATOM    106  HE1 TYR A   7       5.612   2.197  11.109  1.00 14.33           H
ATOM    107  HE2 TYR A   7       4.899  -0.713   8.417  1.00 13.46           H
ATOM    108  HH  TYR A   7       3.630   1.119  10.928  1.00 14.39           H
END
"""

pdb_str_answer = """
HETATM   63  O   HOH A  10     -11.882   1.946  -1.405  1.00 17.08           O  
%s
"""%pdb_str_prot


def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Test for water (ordered solvent) picking: with H.
  Symmetry water.
  """
  with open("%s_noW.pdb"%prefix,"w") as fo:
    fo.write(pdb_str_prot)
  with open("%s_answer.pdb"%prefix,"w") as fo:
    fo.write(pdb_str_answer)
  #
  args = [
    "%s_answer.pdb"%prefix,
    "high_resolution=1.2",
    "type=real r_free=0.05"]
  r = run_fmodel(args = args, prefix = prefix)
  #
  args = [
    "%s_noW.pdb"%prefix,
    r.mtz,
    "ordered_solvent=true",
    "strategy=none",
    "main.number_of_mac=1"
    ]
  cmd = "phenix.refine "+" ".join(args)
  print(cmd)
  r = run_phenix_refine(args = args, prefix = prefix)
  assert r.r_work_start > 0.1
  assert r.r_work_final < 0.01

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
