from __future__ import print_function
import os, time
from libtbx.test_utils import approx_equal
import libtbx.load_env
import mmtbx.f_model
from iotbx import reflection_file_utils
from iotbx import reflection_file_reader
from libtbx.test_utils import approx_equal
import iotbx.pdb

def run():
  # get neutron structure
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/aldose_joint_001.pdb",
    test=os.path.isfile)
  xrs = iotbx.pdb.input(file_name = pdb_file).xray_structure_simple()
  xrs.switch_to_neutron_scattering_dictionary()
  # get neutron f_obs
  reflection_file_name = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/neutron_data_new.hkl",
    test=os.path.isfile)
  reflection_file = reflection_file_reader.any_reflection_file(
    file_name          = reflection_file_name,
    ensure_read_access = False)
  reflection_file_server = reflection_file_utils.reflection_file_server(
    crystal_symmetry = xrs.crystal_symmetry(),
    force_symmetry   = True,
    reflection_files = [reflection_file],
    err              = None)
  neutron_data = reflection_file_server.get_xray_data(
    file_name        = reflection_file_name,
    labels           = ["FOBS","SIGMA"],
    ignore_all_zeros = True,
    parameter_scope  = "refinement.input.xray_data")
  r_free_flags, test_flag_value = reflection_file_server.get_r_free_flags(
    file_name                = reflection_file_name,
    label                    = "TEST",
    test_flag_value          = 1,
    disable_suitability_test = False,
    parameter_scope          = "refinement.input.r_free_flags")
  r_free_flags = r_free_flags.array(
   data = r_free_flags.data() == test_flag_value)
  fmodel = mmtbx.f_model.manager(
    xray_structure    = xrs,
    r_free_flags      = r_free_flags,
    target_name       = "ls_wunit_k1",
    f_obs             = neutron_data)
  assert approx_equal(fmodel.r_work(), 0.3065, 1.e-4)
  assert approx_equal(fmodel.r_free(), 0.3215, 1.e-4)
  fmodel.update_xray_structure(xray_structure = xrs,
                               update_f_calc = False,
                               update_f_mask = True)
  fmodel.update_all_scales()
  assert approx_equal(fmodel.r_work(), 0.2627, 0.0005), fmodel.r_work()
  assert approx_equal(fmodel.r_free(), 0.2920, 0.0005), fmodel.r_free()

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("Time: %6.2f"%(time.time()-t0))
  print("OK")
