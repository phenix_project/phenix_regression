from __future__ import print_function
from libtbx import easy_run
import iotbx.pdb

pdb_no_h = """
CRYST1   67.145   67.145   41.119  90.00  90.00 120.00 P 63          6
HETATM 1987  C1  HC4 A 169      12.270  -2.851   7.690  1.00  2.51           C
HETATM 1988  O1  HC4 A 169      12.412  -3.051   8.915  1.00  5.58           O
HETATM 1989  C2  HC4 A 169      13.073  -1.969   6.902  1.00  3.66           C
HETATM 1990  C3  HC4 A 169      13.948  -1.115   7.480  1.00  4.10           C
HETATM 1991  C1' HC4 A 169      14.960  -0.357   6.862  1.00  3.50           C
HETATM 1992  C2' HC4 A 169      15.734   0.515   7.642  1.00  7.24           C
HETATM 1993  C3' HC4 A 169      16.796   1.251   7.105  1.00  7.06           C
HETATM 1994  C4' HC4 A 169      17.093   1.090   5.743  1.00  6.80           C
HETATM 1995  C5' HC4 A 169      16.317   0.232   4.947  1.00  7.54           C
HETATM 1996  C6' HC4 A 169      15.255  -0.474   5.508  1.00  4.60           C
HETATM 1997  O4' HC4 A 169      18.097   1.748   5.166  1.00  7.92           O
END
"""

def run():
  pdb_inp = iotbx.pdb.input(source_info=None, lines=pdb_no_h)
  pdb_inp.write_pdb_file(file_name = "start_no_h.pdb")
  cmd = " ".join([
      "phenix.ready_set",
      "start_no_h.pdb",
      "neutron_option=all_d",
      "output_file_name=tst_HC4_neutron_all_d",
      "> tst_HC4_neutron_all_d.log"])
  print(cmd)
  assert not easy_run.call(cmd)
  sel = iotbx.pdb.input(file_name = "tst_HC4_neutron_all_d.pdb"
    ).xray_structure_simple().hd_selection()
  n_h = sel.count(True)
  assert n_h == 7, n_h 

if __name__=="__main__":
  run()
