from __future__ import print_function
from libtbx import easy_run
import iotbx.pdb

pdb_no_h = """
CRYST1   14.228   15.246   13.577  90.00  90.00  90.00 P 1
ATOM    577  N   SER A  37       8.872   5.000   5.995  1.00 25.33           N
ATOM    578  CA  SER A  37       7.827   5.915   6.440  1.00 31.94           C
ATOM    579  CB  SER A  37       7.709   5.892   7.965  1.00 42.16           C
ATOM    580  OG  SER A  37       8.926   6.283   8.577  1.00 54.74           O
ATOM    581  C   SER A  37       8.095   7.336   5.956  1.00 32.18           C
ATOM    582  O   SER A  37       9.228   7.814   6.002  1.00 31.99           O
ATOM    588  N   LEU A  38       7.045   8.005   5.492  1.00 34.04           N
ATOM    589  CA  LEU A  38       7.165   9.372   5.000  1.00 36.58           C
ATOM    594  C   LEU A  38       6.026  10.246   5.515  1.00 37.03           C
ATOM    595  O   LEU A  38       5.000   9.741   5.969  1.00 35.00           O
TER
END
"""

pdb_all_h = """
CRYST1   14.228   15.246   13.577  90.00  90.00  90.00 P 1
ATOM      1  N   SER A  37       8.872   5.000   5.995  1.00 25.33           N
ATOM      2  CA  SER A  37       7.827   5.915   6.440  1.00 31.94           C
ATOM      3  CB  SER A  37       7.709   5.892   7.965  1.00 42.16           C
ATOM      4  OG  SER A  37       8.926   6.283   8.577  1.00 54.74           O
ATOM      5  C   SER A  37       8.095   7.336   5.956  1.00 32.18           C
ATOM      6  O   SER A  37       9.228   7.814   6.002  1.00 31.99           O
ATOM      7  HA  SER A  37       6.988   5.617   6.055  1.00 31.94           H
ATOM      8  HB2 SER A  37       6.996   6.487   8.246  1.00 42.16           H
ATOM      9  HB3 SER A  37       7.467   5.000   8.260  1.00 42.16           H
ATOM     10  HG  SER A  37       9.580   5.993   8.119  1.00 54.74           H
ATOM     11  N   LEU A  38       7.045   8.005   5.492  1.00 34.04           N
ATOM     12  CA  LEU A  38       7.165   9.372   5.000  1.00 36.58           C
ATOM     13  C   LEU A  38       6.026  10.246   5.515  1.00 37.03           C
ATOM     14  O   LEU A  38       5.000   9.741   5.969  1.00 35.00           O
ATOM     15  H   LEU A  38       6.249   7.682   5.454  1.00 34.04           H
TER
END
"""

pdb_all_d = """
CRYST1   14.228   15.246   13.577  90.00  90.00  90.00 P 1
ATOM      1  N   SER A  37       8.872   5.000   5.995  1.00 25.33           N
ATOM      2  CA  SER A  37       7.827   5.915   6.440  1.00 31.94           C
ATOM      3  CB  SER A  37       7.709   5.892   7.965  1.00 42.16           C
ATOM      4  OG  SER A  37       8.926   6.283   8.577  1.00 54.74           O
ATOM      5  C   SER A  37       8.095   7.336   5.956  1.00 32.18           C
ATOM      6  O   SER A  37       9.228   7.814   6.002  1.00 31.99           O
ATOM      7  DA  SER A  37       6.988   5.617   6.055  1.00 31.94           D
ATOM      8  DB2 SER A  37       6.996   6.487   8.246  1.00 42.16           D
ATOM      9  DB3 SER A  37       7.467   5.000   8.260  1.00 42.16           D
ATOM     10  DG  SER A  37       9.580   5.993   8.119  1.00 54.74           D
ATOM     11  N   LEU A  38       7.045   8.005   5.492  1.00 34.04           N
ATOM     12  CA  LEU A  38       7.165   9.372   5.000  1.00 36.58           C
ATOM     13  C   LEU A  38       6.026  10.246   5.515  1.00 37.03           C
ATOM     14  O   LEU A  38       5.000   9.741   5.969  1.00 35.00           O
ATOM     15  D   LEU A  38       6.249   7.682   5.454  1.00 34.04           D
TER
END
"""

pdb_all_hd = """
CRYST1   14.228   15.246   13.577  90.00  90.00  90.00 P 1
ATOM      1  N   SER A  37       8.872   5.000   5.995  1.00 25.33           N
ATOM      2  CA  SER A  37       7.827   5.915   6.440  1.00 31.94           C
ATOM      3  CB  SER A  37       7.709   5.892   7.965  1.00 42.16           C
ATOM      4  OG  SER A  37       8.926   6.283   8.577  1.00 54.74           O
ATOM      5  C   SER A  37       8.095   7.336   5.956  1.00 32.18           C
ATOM      6  O   SER A  37       9.228   7.814   6.002  1.00 31.99           O
ATOM      7  HA ASER A  37       6.988   5.617   6.055  0.50 31.94           H
ATOM      8  HB2ASER A  37       6.996   6.487   8.246  0.50 42.16           H
ATOM      9  HB3ASER A  37       7.467   5.000   8.260  0.50 42.16           H
ATOM     10  HG ASER A  37       9.580   5.993   8.119  0.50 54.74           H
ATOM     11  DA BSER A  37       6.988   5.617   6.055  0.50 31.94           D
ATOM     12  DB2BSER A  37       6.996   6.487   8.246  0.50 42.16           D
ATOM     13  DB3BSER A  37       7.467   5.000   8.260  0.50 42.16           D
ATOM     14  DG BSER A  37       9.580   5.993   8.119  0.50 54.74           D
ATOM     15  N   LEU A  38       7.045   8.005   5.492  1.00 34.04           N
ATOM     16  CA  LEU A  38       7.165   9.372   5.000  1.00 36.58           C
ATOM     17  C   LEU A  38       6.026  10.246   5.515  1.00 37.03           C
ATOM     18  O   LEU A  38       5.000   9.741   5.969  1.00 35.00           O
ATOM     19  H  ALEU A  38       6.249   7.682   5.454  0.50 34.04           H
ATOM     20  D  BLEU A  38       6.249   7.682   5.454  0.50 34.04           D
TER
END
"""

pdb_hd_and_h = """
CRYST1   14.228   15.246   13.577  90.00  90.00  90.00 P 1
ATOM      1  N   SER A  37       8.872   5.000   5.995  1.00 25.33           N
ATOM      2  CA  SER A  37       7.827   5.915   6.440  1.00 31.94           C
ATOM      3  CB  SER A  37       7.709   5.892   7.965  1.00 42.16           C
ATOM      4  OG  SER A  37       8.926   6.283   8.577  1.00 54.74           O
ATOM      5  C   SER A  37       8.095   7.336   5.956  1.00 32.18           C
ATOM      6  O   SER A  37       9.228   7.814   6.002  1.00 31.99           O
ATOM      7  HA  SER A  37       6.988   5.617   6.055  1.00 31.94           H
ATOM      8  HB2 SER A  37       6.996   6.487   8.246  1.00 42.16           H
ATOM      9  HB3 SER A  37       7.467   5.000   8.260  1.00 42.16           H
ATOM     10  HG ASER A  37       9.580   5.993   8.119  0.50 54.74           H
ATOM     11  DG BSER A  37       9.580   5.993   8.119  0.50 54.74           D
ATOM     12  N   LEU A  38       7.045   8.005   5.492  1.00 34.04           N
ATOM     13  CA  LEU A  38       7.165   9.372   5.000  1.00 36.58           C
ATOM     14  C   LEU A  38       6.026  10.246   5.515  1.00 37.03           C
ATOM     15  O   LEU A  38       5.000   9.741   5.969  1.00 35.00           O
ATOM     16  H  ALEU A  38       6.249   7.682   5.454  0.50 34.04           H
ATOM     17  D  BLEU A  38       6.249   7.682   5.454  0.50 34.04           D
TER
END
"""

pdb_hd_and_d = """
CRYST1   14.228   15.246   13.577  90.00  90.00  90.00 P 1
ATOM      1  N   SER A  37       8.872   5.000   5.995  1.00 25.33           N
ATOM      2  CA  SER A  37       7.827   5.915   6.440  1.00 31.94           C
ATOM      3  CB  SER A  37       7.709   5.892   7.965  1.00 42.16           C
ATOM      4  OG  SER A  37       8.926   6.283   8.577  1.00 54.74           O
ATOM      5  C   SER A  37       8.095   7.336   5.956  1.00 32.18           C
ATOM      6  O   SER A  37       9.228   7.814   6.002  1.00 31.99           O
ATOM      7  DA  SER A  37       6.988   5.617   6.055  1.00 31.94           D
ATOM      8  DB2 SER A  37       6.996   6.487   8.246  1.00 42.16           D
ATOM      9  DB3 SER A  37       7.467   5.000   8.260  1.00 42.16           D
ATOM     10  HG ASER A  37       9.580   5.993   8.119  0.50 54.74           H
ATOM     11  DG BSER A  37       9.580   5.993   8.119  0.50 54.74           D
ATOM     12  N   LEU A  38       7.045   8.005   5.492  1.00 34.04           N
ATOM     13  CA  LEU A  38       7.165   9.372   5.000  1.00 36.58           C
ATOM     14  C   LEU A  38       6.026  10.246   5.515  1.00 37.03           C
ATOM     15  O   LEU A  38       5.000   9.741   5.969  1.00 35.00           O
ATOM     16  H  ALEU A  38       6.249   7.682   5.454  0.50 34.04           H
ATOM     17  D  BLEU A  38       6.249   7.682   5.454  0.50 34.04           D
TER
END
"""

def assert_identical(f1, f2):
  atom_lines_1 = []
  atom_lines_2 = []
  for line in iotbx.pdb.input(
      source_info = None, file_name=f1).construct_hierarchy().\
      as_pdb_string().split("\n"):
    if(line.startswith("ATOM")): atom_lines_1.append(line)
  for line in iotbx.pdb.input(
      source_info = None, file_name=f2).construct_hierarchy().\
      as_pdb_string().split("\n"):
    if(line.startswith("ATOM")): atom_lines_2.append(line)
  for l1,l2 in zip(atom_lines_1, atom_lines_2):
    assert l1[:27] == l2[:27], [l1[:27], l2[:27]]
    assert l1[55:] == l2[55:], [l1[55:], l2[55:]]

def run():
  pdb_inp = iotbx.pdb.input(source_info=None, lines=pdb_no_h)
  pdb_inp.write_pdb_file(file_name = "start_no_h.pdb")
  for o in [(pdb_all_h, "all_h"),
            (pdb_all_d, "all_d"),
            (pdb_all_hd, "all_hd"),
            (pdb_hd_and_h, "hd_and_h"),
            (pdb_hd_and_d, "hd_and_d")]:
    pdb_inp = iotbx.pdb.input(source_info=None, lines=o[0])
    pdb_inp.write_pdb_file(file_name = "%s_answer.pdb"%o[1])
    print("exercising option:", o[1])
    #
    print("  ...start with H-free model")
    ofn = "%s_1"%o[1]
    cmd = " ".join([
      "phenix.ready_set",
      "start_no_h.pdb",
      "neutron_option=%s"%o[1],
      "output_file_name=%s"%ofn,
      "> tmp.log"])
    assert not easy_run.call(cmd)
    assert_identical(f1="%s_answer.pdb"%o[1], f2 = ofn+".pdb")
    #
    print("  ...start with model containing H")
    ofn = "%s_2"%o[1]
    cmd = " ".join([
      "phenix.ready_set",
      "%s_answer.pdb"%o[1],
      "neutron_option=%s"%o[1],
      "output_file_name=%s"%ofn,
      "> tmp.log"])
    assert not easy_run.call(cmd)
    assert_identical(f1="%s_answer.pdb"%o[1], f2 = ofn+".pdb")
    #

if __name__=="__main__":
  run()
