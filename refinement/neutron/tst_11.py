from __future__ import division
from __future__ import print_function
import iotbx.pdb
import time
from libtbx import easy_run
from libtbx.test_utils import approx_equal

pdb_noH = """\n
CRYST1   12.670   14.862   15.186  90.00  90.00  90.00 P 1
ATOM    780  N   HIS A  45       5.209   7.219   5.800  1.00  9.38           N
ATOM    781  CA  HIS A  45       6.450   6.586   6.229  1.00 10.30           C
ATOM    782  C   HIS A  45       6.629   5.221   5.572  1.00 11.06           C
ATOM    783  O   HIS A  45       7.669   4.940   4.976  1.00 12.76           O
ATOM    784  CB  HIS A  45       6.482   6.444   7.752  1.00 11.71           C
ATOM    785  CG  HIS A  45       6.466   7.752   8.482  1.00 17.20           C
ATOM    786  ND1 HIS A  45       6.374   7.837   9.854  1.00 28.53           N
ATOM    787  CD2 HIS A  45       6.532   9.027   8.030  1.00 22.78           C
ATOM    788  CE1 HIS A  45       6.383   9.107  10.217  1.00 26.38           C
ATOM    789  NE2 HIS A  45       6.478   9.850   9.129  1.00 20.23           N
TER
"""

pdb_hnp_false = """\n
CRYST1   12.670   14.862   15.186  90.00  90.00  90.00 P 1
ATOM      1  N   HIS A  45       5.209   7.219   5.800  1.00  9.38           N
ATOM      2  CA  HIS A  45       6.450   6.586   6.229  1.00 10.30           C
ATOM      3  C   HIS A  45       6.629   5.221   5.572  1.00 11.06           C
ATOM      4  O   HIS A  45       7.669   4.940   4.976  1.00 12.76           O
ATOM      5  CB  HIS A  45       6.482   6.444   7.752  1.00 11.71           C
ATOM      6  CG  HIS A  45       6.466   7.752   8.482  1.00 17.20           C
ATOM      7  ND1 HIS A  45       6.374   7.837   9.854  1.00 28.53           N
ATOM      8  CD2 HIS A  45       6.532   9.027   8.030  1.00 22.78           C
ATOM      9  CE1 HIS A  45       6.383   9.107  10.217  1.00 26.38           C
ATOM     10  NE2 HIS A  45       6.478   9.850   9.129  1.00 20.23           N
ATOM     11  HA  HIS A  45       7.289   7.215   5.932  1.00 10.30           H
ATOM     12  HB2 HIS A  45       5.609   5.874   8.070  1.00 11.71           H
ATOM     13  HB3 HIS A  45       7.392   5.915   8.036  1.00 11.71           H
ATOM     14  HD2 HIS A  45       6.611   9.338   6.999  1.00 22.78           H
ATOM     15  HE1 HIS A  45       6.323   9.475  11.231  1.00 26.38           H
ATOM     16  HE2AHIS A  45       6.507  10.869   9.108  0.50 20.23           H
ATOM     17  DE2BHIS A  45       6.507  10.869   9.108  0.50 20.23           D
TER
"""

pdb_hnp_true = """\n
CRYST1   12.670   14.862   15.186  90.00  90.00  90.00 P 1
ATOM      1  N   HIS A  45       5.209   7.219   5.800  1.00  9.38           N
ATOM      2  CA  HIS A  45       6.450   6.586   6.229  1.00 10.30           C
ATOM      3  C   HIS A  45       6.629   5.221   5.572  1.00 11.06           C
ATOM      4  O   HIS A  45       7.669   4.940   4.976  1.00 12.76           O
ATOM      5  CB  HIS A  45       6.482   6.444   7.752  1.00 11.71           C
ATOM      6  CG  HIS A  45       6.466   7.752   8.482  1.00 17.20           C
ATOM      7  ND1 HIS A  45       6.374   7.837   9.854  1.00 28.53           N
ATOM      8  CD2 HIS A  45       6.532   9.027   8.030  1.00 22.78           C
ATOM      9  CE1 HIS A  45       6.383   9.107  10.217  1.00 26.38           C
ATOM     10  NE2 HIS A  45       6.478   9.850   9.129  1.00 20.23           N
ATOM     11  HA  HIS A  45       7.289   7.215   5.932  1.00 10.30           H
ATOM     12  HB2 HIS A  45       5.609   5.874   8.070  1.00 11.71           H
ATOM     13  HB3 HIS A  45       7.392   5.915   8.036  1.00 11.71           H
ATOM     14  HD2 HIS A  45       6.611   9.338   6.999  1.00 22.78           H
ATOM     15  HE1 HIS A  45       6.323   9.475  11.231  1.00 26.38           H
ATOM     16  HD1AHIS A  45       6.309   7.043  10.491  0.50 28.53           H
ATOM     17  HE2AHIS A  45       6.507  10.869   9.108  0.50 20.23           H
ATOM     18  DD1BHIS A  45       6.309   7.043  10.491  0.50 28.53           D
ATOM     19  DE2BHIS A  45       6.507  10.869   9.108  0.50 20.23           D
TER
"""

def check(lines, file_name):
  xrs1 = iotbx.pdb.input(source_info=None,
    lines = lines).xray_structure_simple()
  xrs2 = iotbx.pdb.input(file_name=file_name).xray_structure_simple()
  d = xrs1.distances(other = xrs2).min_max_mean().as_tuple()
  assert xrs1.scatterers().size() == xrs1.scatterers().size()
  assert approx_equal(d, [0,0,0], 0.01)

def exercise_11():
  """
  Make sure H/D are added correctly to HIS.
  """
  #
  pdb_in = "exercise_11_input.pdb"
  fo = open(pdb_in, "w")
  print(pdb_noH, file=fo)
  fo.close()
  #
  for o in [["true", pdb_hnp_true,  "hnp_true.pdb"],
            ["false",pdb_hnp_false, "hnp_false.pdb"]]:
    cmd = " ".join([
      "phenix.ready_set",
      "%s"%pdb_in,
      "histidine_nitrogen_protonation=%s"%str(o[0]),
      "neutron_option=hd_and_h",
      "output_file_name=hnp_%s"%str(o[0]),
      "> zlog"])
    assert not easy_run.call(cmd)
    check(lines=o[1], file_name=o[2])

if (__name__ == "__main__"):
  t0 = time.time()
  exercise_11()
  print("Total time: %-8.4f"%(time.time()-t0))
  print("OK")
