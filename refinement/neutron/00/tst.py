from __future__ import print_function
import libtbx.load_env
from libtbx.test_utils import approx_equal, not_approx_equal, run_command
import sys, os, time, random
from mmtbx import utils
from cctbx.array_family import flex
import iotbx.pdb
from libtbx import easy_run
import iotbx.ccp4_map
from iotbx import reflection_file_reader

par_str = """
data_manager {
  miller_array {
    file = %s
    labels {
      name = "F-obs,SIGF-obs"
      type = x_ray
    }
    labels {
      name = "R-free-flags"
      type = x_ray
    }
    labels {
      name = "F-obs-neutron,SIGF-obs-neutron"
      type = neutron
    }
    labels {
      name = "R-free-flags-neutron"
      type = neutron
    }
  }
  model {
    file = %s
    type = x_ray
  }
  model {
    file = %s
    type = neutron
  }
}
xray {
  refinement {
    refine {
      strategy = *individual_sites *rigid_body
    }
    main {
      simulated_annealing = True
      ordered_solvent = True
      number_of_macro_cycles = 0
    }
  }
}
neutron {
  refinement {
    refine {
      strategy = *individual_sites *individual_adp
    }
    main {
      simulated_annealing = False
      ordered_solvent = False
      number_of_macro_cycles = 0
    }
  }
}
joint {
  dummy_parameter = 0
}
"""

def run():
  """
  Make sure joint refinement at least not crashes.
  XXX Weak test. Make it stronger.
  """
  pdb_x = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/neutron/00/1iu5_1iu6_x.pdb",
    test=os.path.isfile)
  pdb_n = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/neutron/00/1iu5_1iu6_n.pdb",
    test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/neutron/00/1iu5_1iu6.mtz", \
    test=os.path.isfile)
  par = par_str % (hkl, pdb_x, pdb_n)
  with open("1iu5_1iu6_joint.params","w") as fo:
    fo.write(par)
  #
  cmd = " ".join([
      'phenix.refine',
      "1iu5_1iu6_joint.params",
      '--overwrite --quiet',
      "> 1iu5_1iu6_joint.zlog"])
  assert not easy_run.call(cmd)

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK", "time: %.2f"%(time.time()-t0))
