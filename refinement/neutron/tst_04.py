from __future__ import print_function
from libtbx.test_utils import run_command
import libtbx.load_env
import time
import os
import mmtbx.model
import mmtbx.monomer_library

pdb_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="pdb",
  test=os.path.isdir)

hkl_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="reflection_files",
  test=os.path.isdir)

def exercise_01():
  pdbin = os.path.join(pdb_dir, "x_001_HD_ricardo_leal.pdb")
  hklin = os.path.join(hkl_dir, "xn_data_ricardo_leal.mtz")
  params = """\
#phil __ON__
refinement {
  refine {
    strategy = *individual_sites rigid_body *individual_adp group_adp tls \
               *occupancies group_anomalous
  }
  main {
    ordered_solvent = True
    number_of_macro_cycles = 3
    scattering_table = neutron
  }
  ordered_solvent {
    low_resolution = 3.0
    mode = auto filter_only *every_macro_cycle
  }
  target_weights {
    wxc_scale = 0.1
    wxu_scale = 0.1
  }
}
#phil __OFF__
  """
  task_file = open("tst_refinement_ricardo_leal_pure_neutron.params", "w").write(params)
  cmd = " ".join([
    'phenix.refine',
    '%s'%hklin,
    '%s'%pdbin,
    'tst_refinement_ricardo_leal_pure_neutron.params',
    '--overwrite > tst_refinement_ricardo_leal_pure_neutron_stdout'])
  print(cmd)
  run_command(
    command=cmd,
    log_file_name="x_001_HD_ricardo_leal_refine_001.log",
    stdout_file_name="tst_refinement_ricardo_leal_pure_neutron_stdout",
    show_diff_log_stdout=True)

  processed_pdb_file = mmtbx.monomer_library.pdb_interpretation.process(
    mon_lib_srv    = mmtbx.monomer_library.server.server(),
    ener_lib       = mmtbx.monomer_library.server.ener_lib(),
    file_name      = "x_001_HD_ricardo_leal_refine_001.pdb",
    raw_records    = None,
    force_symmetry = True)
  xray_structure = processed_pdb_file.xray_structure()
  geometry = processed_pdb_file.geometry_restraints_manager(
    show_energies      = False,
    plain_pairs_radius = 5.0)
  restraints_manager = mmtbx.restraints.manager(geometry = geometry,
    normalization = False)
  ct = mmtbx.model.xh_connectivity_table(
    geometry=restraints_manager, xray_structure=xray_structure)
  for t in ct.table:
    assert t[4] > 1.0, t[4]

if (__name__ == "__main__"):
  t0 = time.time()
  exercise_01()
  print("OK Time: %8.3f"%(time.time()-t0))
