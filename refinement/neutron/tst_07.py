from __future__ import division
from __future__ import print_function
import iotbx.pdb
from cctbx.array_family import flex
import time, os
from libtbx import easy_run

m5_str = """
CRYST1   12.562   13.497   13.567  90.00  90.00  90.00 P 1
ATOM      1  N   SER H   7       8.250   7.651   4.068  1.00 10.00           N
ATOM      2  CA  SER H   7       6.970   7.434   4.749  1.00 10.00           C
ATOM      3  C   SER H   7       6.801   7.023   6.218  1.00 10.00           C
ATOM      4  O   SER H   7       7.309   5.986   6.645  1.00 10.00           O
ATOM      5  CB  SER H   7       5.756   7.214   3.829  1.00 10.00           C
ATOM      6  HA  SER H   7       7.187   6.376   4.606  1.00 10.00           H
ATOM      7  OG ASER H   7       4.573   7.016   4.583  0.50 10.00           O
ATOM      8  HG ASER H   7       4.387   7.808   5.129  0.50 10.00           H
ATOM      9  OG BSER H   7       5.936   6.078   3.000  0.50 10.00           O
ATOM     10  HG BSER H   7       5.830   5.257   3.524  0.50 10.00           H
ATOM     11  N   THR H   8       6.086   7.844   6.979  1.00 10.00           N
ATOM     12  CA  THR H   8       5.850   7.570   8.392  1.00 10.00           C
ATOM     13  C   THR H   8       4.652   6.646   8.585  1.00 10.00           C
ATOM     14  O   THR H   8       4.802   5.499   9.008  1.00 10.00           O
ATOM     15  CB  THR H   8       5.627   8.869   9.193  1.00 10.00           C
ATOM     16  OG1 THR H   8       5.375   8.550  10.567  1.00 10.00           O
ATOM     17  CG2 THR H   8       4.446   9.652   8.633  1.00 10.00           C
ATOM     18 DG21 THR H   8       4.595   9.849   7.571  1.00 10.00           D
ATOM     19 DG22 THR H   8       3.523   9.087   8.763  1.00 10.00           D
ATOM     20 DG23 THR H   8       4.351  10.603   9.157  1.00 10.00           D
TER
ATOM      1  O   DOD S   1       4.985   3.897   6.641  1.00 10.00           O
ATOM      1  D1  DOD S   1       5.709   4.346   6.221  1.00 10.00           D
ATOM      1  D2  DOD S   1       5.192   3.849   7.567  1.00 10.00           D
ATOM      1  O   DOD S   2       9.562   5.228   7.196  1.00 10.00           O
ATOM      1  D1  DOD S   2       9.256   5.489   6.336  1.00 10.00           D
ATOM      1  D2  DOD S   2       8.805   5.231   7.770  1.00 10.00           D
TER
"""

# XXX THIS FAILS and I know why - I will fix it later
#m5_str_HD_rotated = """
#CRYST1   12.562   13.497   13.567  90.00  90.00  90.00 P 1
#ATOM      1  O   DOD X  -1       3.000   3.052   5.501  1.00 10.00           O
#ATOM      1  D1  DOD X  -1       3.657   3.456   5.143  1.00 10.00           D
#ATOM      1  D2  DOD X  -1       3.168   3.000   6.333  1.00 10.00           D
#TER
#ATOM      1  N   SER H   7       8.250   7.651   4.068  1.00 10.00           N
#ATOM      2  CA  SER H   7       6.970   7.434   4.749  1.00 10.00           C
#ATOM      3  C   SER H   7       6.801   7.023   6.218  1.00 10.00           C
#ATOM      4  O   SER H   7       7.309   5.986   6.645  1.00 10.00           O
#ATOM      5  CB  SER H   7       5.756   7.214   3.829  1.00 10.00           C
#ATOM      6  HA  SER H   7       7.162   6.492   4.621  1.00 10.00           H
#ATOM      7  OG ASER H   7       4.573   7.016   4.583  0.50 10.00           O
#ATOM      8  HG ASER H   7       3.899   7.297   4.147  0.50 10.00           H
#ATOM      9  OG BSER H   7       5.936   6.078   3.000  0.50 10.00           O
#ATOM     10  HG BSER H   7       6.068   6.321   2.196  0.50 10.00           H
#ATOM     11  N   THR H   8       6.086   7.844   6.979  1.00 10.00           N
#ATOM     12  CA  THR H   8       5.850   7.570   8.392  1.00 10.00           C
#ATOM     13  C   THR H   8       4.652   6.646   8.585  1.00 10.00           C
#ATOM     14  O   THR H   8       4.802   5.499   9.008  1.00 10.00           O
#ATOM     15  CB  THR H   8       5.627   8.869   9.193  1.00 10.00           C
#ATOM     16  OG1 THR H   8       5.375   8.550  10.567  1.00 10.00           O
#ATOM     17  CG2 THR H   8       4.446   9.652   8.633  1.00 10.00           C
#ATOM     18 DG21 THR H   8       3.767   9.043   8.304  1.00 10.00           D
#ATOM     19 DG22 THR H   8       4.059  10.213   9.324  1.00 10.00           D
#ATOM     20 DG23 THR H   8       4.742  10.216   7.902  1.00 10.00           D
#"""

m5_str_HD_rotated = """
CRYST1   12.562   13.497   13.567  90.00  90.00  90.00 P 1
ATOM      1  N   SER H   7       8.250   7.651   4.068  1.00 10.00           N
ATOM      2  CA  SER H   7       6.970   7.434   4.749  1.00 10.00           C
ATOM      3  C   SER H   7       6.801   7.023   6.218  1.00 10.00           C
ATOM      4  O   SER H   7       7.309   5.986   6.645  1.00 10.00           O
ATOM      5  CB  SER H   7       5.756   7.214   3.829  1.00 10.00           C
ATOM      6  HA  SER H   7       7.162   6.492   4.621  1.00 10.00           H
ATOM      7  OG ASER H   7       4.573   7.016   4.583  0.50 10.00           O
ATOM      8  HG ASER H   7       3.899   7.297   4.147  0.50 10.00           H
ATOM      9  OG BSER H   7       5.936   6.078   3.000  0.50 10.00           O
ATOM     10  HG BSER H   7       6.068   6.321   2.196  0.50 10.00           H
ATOM     11  N   THR H   8       6.086   7.844   6.979  1.00 10.00           N
ATOM     12  CA  THR H   8       5.850   7.570   8.392  1.00 10.00           C
ATOM     13  C   THR H   8       4.652   6.646   8.585  1.00 10.00           C
ATOM     14  O   THR H   8       4.802   5.499   9.008  1.00 10.00           O
ATOM     15  CB  THR H   8       5.627   8.869   9.193  1.00 10.00           C
ATOM     16  OG1 THR H   8       5.375   8.550  10.567  1.00 10.00           O
ATOM     17  CG2 THR H   8       4.446   9.652   8.633  1.00 10.00           C
ATOM     18 DG21 THR H   8       3.767   9.043   8.304  1.00 10.00           D
ATOM     19 DG22 THR H   8       4.059  10.213   9.324  1.00 10.00           D
ATOM     20 DG23 THR H   8       4.742  10.216   7.902  1.00 10.00           D
TER
ATOM      1  O   DOD S   1       4.985   3.897   6.641  1.00 10.00           O
ATOM      1  O   DOD S   2       9.562   5.228   7.196  1.00 10.00           O
TER
"""

def exercise_01():
  #
  here = os.getcwd()
  dd = 'neutron_07'
  if not os.path.isdir(dd): os.mkdir(dd)
  os.chdir(dd)
  pdb_inp = iotbx.pdb.input(source_info=None, lines=m5_str)
  ph = pdb_inp.construct_hierarchy()
  xrs_answer = pdb_inp.xray_structure_simple()
  xrs_answer.switch_to_neutron_scattering_dictionary()
  ph.write_pdb_file(file_name = "answer.pdb")
  #
  pdb_inp = iotbx.pdb.input(source_info=None, lines=m5_str_HD_rotated)
  ph = pdb_inp.construct_hierarchy()
  ph.atoms().reset_i_seq()
  ph.write_pdb_file(file_name = "poor.pdb")
  #
  f_obs = abs(xrs_answer.structure_factors(d_min=1,
    algorithm="direct").f_calc())
  mtz_dataset = f_obs.as_mtz_dataset(column_root_label="F-obs")
  mtz_dataset.add_miller_array(miller_array=f_obs.generate_r_free_flags(),
    column_root_label="R-free-flags")
  mtz_object = mtz_dataset.mtz_object()
  mtz_object.write(file_name = "data.mtz")
  #
  cmd = " ".join([
    "phenix.refine",
    "poor.pdb",
    "data.mtz",
    "main.scattering_table=neutron",
    "real_space_optimize_x_h_orientation=True",
    "output.prefix=result",
    "find_and_add_hydrogens=True hydrogens.build.dod_and_od=True",
    "--overwrite"])
  assert not easy_run.call(cmd)
  #
  # The following test seems to shift O-DOD-S1 closer to the position of D2-DOD-S1
  cmd = " ".join([
    "phenix.refine",
    "poor.pdb",
    "data.mtz",
    "main.scattering_table=neutron",
    "real_space_optimize_x_h_orientation=True",
    "output.prefix=result2",
    "--overwrite"])
  print(cmd)
  assert not easy_run.call(cmd)
  os.chdir(here)

if (__name__ == "__main__"):
  t0 = time.time()
  exercise_01()
  print("Total time: %-8.4f"%(time.time()-t0))
  print("OK")
