from __future__ import division
from __future__ import print_function
import iotbx.pdb
from cctbx.array_family import flex
import time, os
from libtbx import easy_run
import libtbx.load_env
from libtbx.test_utils import approx_equal

def exercise_01():
  """
  Make sure exchengeable H/D have identical coordinates and ADPs.
  """
  #
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/neutron/m_tst10.pdb",
    test=os.path.isfile)
  mtz = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/neutron/m_tst10.mtz",
    test=os.path.isfile)
  #
  cmd = " ".join([
    "phenix.refine",
    "%s %s"%(pdb,mtz),
    "overwrite=true",
    "output.prefix=tst_10_neutron",
    "main.scattering_table=neutron",
    "xray_data.low_res=1.6",
    "modify_start_model.modify.sites.shake=1.",
    "main.number_of_mac=1",
    ">zlog_tst_10_neutron"])
  print(cmd)
  assert not easy_run.call(cmd)
  #
  pdb_inp = iotbx.pdb.input(file_name = "tst_10_neutron_001.pdb")
  ph = pdb_inp.construct_hierarchy()
  ascs = ph.atom_selection_cache().selection
  xrs = pdb_inp.xray_structure_simple()
  #
  s1h = ascs(string = "chain A and resseq 16 and name HG and element H")
  s1d = ascs(string = "chain A and resseq 16 and name DG and element D")
  #
  s2h = ascs(string = "chain A and resseq 15 and name HH and element H")
  s2d = ascs(string = "chain A and resseq 15 and name DH and element D")
  #
  def check(xrs, s1, s2):
    site1 = xrs.sites_cart().select(s1)
    site2 = xrs.sites_cart().select(s2)
    assert approx_equal(site1, site2)
    u1 = xrs.extract_u_iso_or_u_equiv().select(s1)
    u2 = xrs.extract_u_iso_or_u_equiv().select(s2)
    assert approx_equal(u1, u2)
    occ1 = xrs.scatterers().extract_occupancies().select(s1)
    occ2 = xrs.scatterers().extract_occupancies().select(s2)
    assert occ1.size() == occ2.size() == 1
    assert approx_equal(occ1[0]+occ2[0], 1, 0.01)
  #
  check(xrs=xrs, s1=s1h, s2=s1d)
  check(xrs=xrs, s1=s2h, s2=s2d)


if (__name__ == "__main__"):
  t0 = time.time()
  exercise_01()
  print("Total time: %-8.4f"%(time.time()-t0))
  print("OK")
