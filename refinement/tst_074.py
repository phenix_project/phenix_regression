from __future__ import division
from __future__ import print_function
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel
import iotbx.pdb
import os
from libtbx import easy_run
from scitbx.array_family import flex
from libtbx.test_utils import approx_equal

pdb_str = """
CRYST1    4.446    4.800    9.065  90.00  90.00  90.00 P 1
ATOM      1  N   PHE A   1       1.896   0.687   3.886  1.00  6.23           N
ATOM      2  CA  PHE A   1       1.137   1.931   4.156  1.00  6.10           C
ATOM      3  C   PHE A   1       1.975   3.136   3.784  1.00  5.73           C
ATOM      4  O   PHE A   1       3.195   3.044   3.671  1.00  6.25           O
ATOM      5  CB  PHE A   1       0.676   1.993   5.621  1.00  6.12           C
ATOM      6  CG  PHE A   1       1.770   2.302   6.613  1.00  6.04           C
ATOM      7  CD1 PHE A   1       2.134   3.615   6.868  1.00  6.26           C
ATOM      8  CD2 PHE A   1       2.424   1.298   7.305  1.00  6.40           C
ATOM      9  CE1 PHE A   1       3.131   3.913   7.772  1.00  6.62           C
ATOM     10  CE2 PHE A   1       3.418   1.597   8.220  1.00  6.66           C
ATOM     11  CZ  PHE A   1       3.776   2.903   8.452  1.00  6.73           C
ATOM     12  H1  PHE A   1       1.935   0.549   3.008  1.00  7.47           D
ATOM     13  H2  PHE A   1       2.720   0.766   4.211  1.00  7.47           D
ATOM     14  H3  PHE A   1       1.488   0.000   4.277  1.00  7.47           D
ATOM     15  HA  PHE A   1       0.344   1.939   3.597  1.00  7.32           D
ATOM     16  HB2 PHE A   1       0.000   2.685   5.704  1.00  7.34           D
ATOM     17  HB3 PHE A   1       0.295   1.134   5.863  1.00  7.34           D
ATOM     18  HD1 PHE A   1       1.708   4.305   6.413  1.00  7.52           D
ATOM     19  HD2 PHE A   1       2.191   0.410   7.154  1.00  7.68           D
ATOM     20  HE1 PHE A   1       3.365   4.800   7.927  1.00  7.94           D
ATOM     21  HE2 PHE A   1       3.852   0.910   8.673  1.00  7.99           D
ATOM     22  HZ  PHE A   1       4.446   3.104   9.065  1.00  8.08           D
TER
ATOM     50  C   MOH B   2       3.779   1.272   0.889  1.00  6.90           C
ATOM     51  O   MOH B   2       2.413   0.984   1.199  1.00  6.84           O
ATOM     52  H1  MOH B   2       4.434   1.072   1.738  1.00  8.27           D
ATOM     53  H2  MOH B   2       3.785   2.339   0.669  1.00  8.27           D
ATOM     54  H3  MOH B   2       4.074   0.716  -0.000  1.00  8.27           D
ATOM     55  HO  MOH B   2       2.146   1.487   1.997  1.00  8.20           D
TER
END
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Check consistency between PDB and log files.
  """
  pdb_inp = iotbx.pdb.input(source_info=None, lines=pdb_str)
  ph = pdb_inp.construct_hierarchy()
  xrs_answer = pdb_inp.xray_structure_simple()
  pdb_in = "%s_start.pdb"%prefix
  ph.write_pdb_file(file_name = pdb_in,
    crystal_symmetry=xrs_answer.crystal_symmetry())
  #
  pdb_poor = "%s_poor.pdb"%prefix
  cmd = " ".join([
    "phenix.pdbtools",
    pdb_in,
    "sites.shake=0.5",
    "suffix=none",
    "output.prefix=%s"%pdb_poor.replace(".pdb",""),
    "> zlog"])
  assert not easy_run.call(cmd)
  #
  for st in ["n_gaussian", "neutron"]:
    for twin_law in [None, "-h,k-l,l"]:
      twin_fraction = None
      if(twin_law is not None): twin_fraction=0.4
      args = [
        pdb_in,
        "high_res=1.8",
        "twin_law=%s"%str(twin_law),
        "twin_fraction=%s"%twin_fraction,
        "type=real",
        "r_free=0.1",
        "scattering_table=%s"%st]
      r0 = run_fmodel(args = args, prefix = prefix)
      for h_mode in ["riding", "individual"]:
        args = [
          "strategy=None",
          pdb_poor,
          r0.mtz,
          "main.scattering_table=%s"%st,
          "xray_data.twin_law=%s"%str(twin_law),
          "main.number_of_mac=1",
          "main.max_number_of_iterations=10",
          "hydrogens.refine=%s"%h_mode,
          "write_model_cif_file=true",
          "xray_data.high_res=1.8"]
        r = run_phenix_refine(args = args, prefix=prefix)
        #
        assert r.r_work_start is not None
        assert r.r_work_final is not None
        assert r.r_free_start is not None
        assert r.r_free_final is not None
        assert r.bond_start   is not None
        assert r.bond_final   is not None
        assert r.angle_start  is not None
        assert r.angle_final  is not None
        #

if (__name__ == "__main__"):
  run()
  print("OK")
