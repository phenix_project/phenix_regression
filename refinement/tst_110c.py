from __future__ import print_function
from phenix_regression.refinement import get_r_factors
import libtbx.load_env
from libtbx.test_utils import approx_equal, not_approx_equal, run_command
import sys, os, time, random
from mmtbx import utils
from cctbx.array_family import flex
import iotbx.pdb
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel
import os

if (1):
  random.seed(0)
  flex.set_random_seed(0)

pdb_str_good = """
CRYST1   15.000   15.000   15.000  80.00  70.00 100.00 P 1
ATOM      1  CB  PHE A   1      12.354  10.563  11.851  1.00  5.00           C
ATOM      2  CG  PHE A   1      11.301   9.391  12.948  1.00  5.00           C
ATOM      3  CD1 PHE A   1      10.062   8.250  12.392  1.00  5.00           C
ATOM      4  CD2 PHE A   1      11.539   9.418  14.552  1.00  5.00           C
ATOM      5  CE1 PHE A   1       9.060   7.136  13.428  1.00  5.00           C
ATOM      6  CE2 PHE A   1      10.552   8.316  15.612  1.00  5.00           C
ATOM      7  CZ  PHE A   1       9.307   7.170  15.045  1.00  5.00           C
ATOM      8  C   PHE A   1      12.599  13.317  10.077  1.00  5.00           C
ATOM      9  O   PHE A   1      13.328  12.587   8.964  1.00  5.00           O
ATOM     10  OXT PHE A   1      12.799  14.936  10.417  1.00  5.00           O
ATOM     11  N   PHE A   1      10.023  11.473  10.134  1.00  5.00           N
ATOM     12  CA  PHE A   1      11.393  12.163  11.077  1.00  5.00           C

HETATM   25  O   HOH     1      16.077  12.587   8.964  1.00  5.00           O
HETATM   26  O   HOH     2      15.549  14.936  10.417  1.00  5.00           O
HETATM   27  O   HOH     3       8.695  12.035  12.673  1.00  5.00           O

ATOM     13  CB  PHE A   2       9.354   7.563   8.851  1.00  5.00           C
ATOM     14  CG  PHE A   2       8.301   6.391   9.948  1.00  5.00           C
ATOM     15  CD1 PHE A   2       7.062   5.250   9.392  1.00  5.00           C
ATOM     16  CD2 PHE A   2       8.539   6.418  11.552  1.00  5.00           C
ATOM     17  CE1 PHE A   2       6.060   4.136  10.428  1.00  5.00           C
ATOM     18  CE2 PHE A   2       7.552   5.316  12.612  1.00  5.00           C
ATOM     19  CZ  PHE A   2       6.307   4.170  12.045  1.00  5.00           C
ATOM     20  C   PHE A   2       9.599  10.317   7.077  1.00  5.00           C
ATOM     21  O   PHE A   2      10.328   9.587   5.964  1.00  5.00           O
ATOM     22  OXT PHE A   2       9.799  11.936   7.417  1.00  5.00           O
ATOM     23  N   PHE A   2       7.023   8.473   7.134  1.00  5.00           N
ATOM     24  CA  PHE A   2       8.393   9.163   8.077  1.00  5.00           C

HETATM   28  O   HOH     4      13.077   9.587   5.964  1.00  5.00           O
HETATM   29  O   HOH     5       5.695   9.035   9.673  1.00  5.00           O

END
"""

pdb_str_bad = """
CRYST1   15.000   15.000   15.000  80.00  70.00 100.00 P 1
ATOM      1  CB  PHE A   1      12.354  10.563  11.851  0.20  5.00           C
ATOM      2  CG  PHE A   1      11.301   9.391  12.948  0.50  5.00           C
ATOM      3  CD1 PHE A   1      10.062   8.250  12.392  1.00  5.00           C
ATOM      4  CD2 PHE A   1      11.539   9.418  14.552  1.00  5.00           C
ATOM      5  CE1 PHE A   1       9.060   7.136  13.428  1.00  5.00           C
ATOM      6  CE2 PHE A   1      10.552   8.316  15.612  1.00  5.00           C
ATOM      7  CZ  PHE A   1       9.307   7.170  15.045  0.80  5.00           C
ATOM      8  C   PHE A   1      12.599  13.317  10.077  1.00  5.00           C
ATOM      9  O   PHE A   1      13.328  12.587   8.964  1.00  5.00           O
ATOM     10  OXT PHE A   1      12.799  14.936  10.417  1.00  5.00           O
ATOM     11  N   PHE A   1      10.023  11.473  10.134  1.00  5.00           N
ATOM     12  CA  PHE A   1      11.393  12.163  11.077  0.70  5.00           C
ATOM     13  CB  PHE A   2       9.354   7.563   8.851  0.30  5.00           C
ATOM     14  CG  PHE A   2       8.301   6.391   9.948  0.60  5.00           C
ATOM     15  CD1 PHE A   2       7.062   5.250   9.392  1.00  5.00           C
ATOM     16  CD2 PHE A   2       8.539   6.418  11.552  1.00  5.00           C
ATOM     17  CE1 PHE A   2       6.060   4.136  10.428  1.00  5.00           C
ATOM     18  CE2 PHE A   2       7.552   5.316  12.612  1.00  5.00           C
ATOM     19  CZ  PHE A   2       6.307   4.170  12.045  0.40  5.00           C
ATOM     20  C   PHE A   2       9.599  10.317   7.077  1.00  5.00           C
ATOM     21  O   PHE A   2      10.328   9.587   5.964  1.00  5.00           O
ATOM     22  OXT PHE A   2       9.799  11.936   7.417  1.00  5.00           O
ATOM     23  N   PHE A   2       7.023   8.473   7.134  1.00  5.00           N
ATOM     24  CA  PHE A   2       8.393   9.163   8.077  0.90  5.00           C
HETATM   28  O   HOH     4      13.077   9.587   5.964  1.00  5.00           O
HETATM   25  O   HOH    10       1.000   1.000   1.000  1.00 50.00           O
HETATM   26  O   HOH    11       1.500   1.500   1.500  1.00 50.00           O
HETATM   27  O   HOH     3       8.695  12.035  12.673  1.00  5.00           O
HETATM   27  O   HOH    12       2.000   2.000   2.000  1.00 50.00           O
HETATM   28  O   HOH    13       2.500   2.500   2.500  1.00 50.00           O
END
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Occupancies.
  """
  with open("%s_good.pdb"%prefix,"w") as fo:
    fo.write(pdb_str_good)
  with open("%s_bad.pdb"%prefix,"w") as fo:
    fo.write(pdb_str_bad)
  #  
  args = [
     "%s_good.pdb"%prefix,
     'high_resolution=1.5',
     'algorithm=direct',
     'label=FOBS',
     'type=real']
  r0 = run_fmodel(args = args, prefix = prefix)

  args = [
    "const_shrink_donor_acceptor=0.6",
    'strategy=occupancies+individual_sites+individual_adp',
    "%s_bad.pdb"%prefix,
    'main.target=ls',
    'main.bulk_solvent_and_=false',
    'ls_target_names.target_name=ls_wunit_kunit',
    r0.mtz,
    'main.number_of_macro_cycles=15',
    'structure_factors_and_gradients_accuracy.algorithm=direct',
    'xray_data.r_free_flags.ignore_r_free_flags=true',
    'ordered_solvent=true',
    'ordered_solvent.mode=every_macro_cycle',
    'wc=0',
    'ordered_solvent.refine_oat=True',
    'primary_map_cutoff=5.5',
    'secondary_map_and_map_cc_filter.poor_map_value_threshold=5.0']
  r = run_phenix_refine(args = args, prefix = prefix)
  # check results
  assert approx_equal(iotbx.pdb.input(file_name =
    r.pdb).xray_structure_simple().scatterers(
    ).extract_occupancies().min_max_mean().as_tuple(), [1.0,1.0,1.0])
  assert r.r_work_start > 0.1
  assert r.r_free_start > 0.1
  assert r.r_work_final < 0.001, r.r_work_final
  assert r.r_free_final < 0.001, r.r_free_final

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK", "time: %.2f"%(time.time()-t0))
