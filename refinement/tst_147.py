from __future__ import print_function
import time, os
import iotbx.pdb
import libtbx.load_env
from phenix_regression.refinement import run_phenix_refine
from libtbx import easy_run

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Make sure correct_drifted_waters of ordered_solvent never kicks in if water
  are not only oxygen but also H or D (DOD, OD, HOH etc).
  correct_drifted_waters badly distorts D-O bonds and angles!!!
  Also, this is an indirect test for phenix.model_statistics.
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/data/%s.pdb"%prefix,
    test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/data/%s.mtz"%prefix,
    test=os.path.isfile)
  #
  args = [
    pdb,
    hkl,
    "const_shrink_donor_acceptor=0.6",
    "main.scattering_table=neutron",
    "main.number_of_macro_cycles=1",
    "ordered_solvent=true"
    ]
  r = run_phenix_refine(args = args, prefix = prefix)
  #
  assert not easy_run.call(
    "phenix.pdbtools %s output.prefix=%s_water keep=water"%(r.pdb,prefix))
  #
  args = [
    "phenix.model_statistics",
    "%s_water_modified.pdb"%prefix,
    "use_hydrogens=true",
    "use_neutron_distances=true",
    " > %s.model_statistics"%prefix
  ]
  assert not easy_run.call(" ".join(args))
  #
  b, a = None,None
  with open("%s.model_statistics"%prefix,"r") as fo:
    for l in fo.readlines():
      if(l.startswith("  Bond      :")): b = float(l.strip().split()[2])
      if(l.startswith("  Angle     :")): a = float(l.strip().split()[2])
  assert b is not None
  assert a is not None
  assert b < 0.022, b
  assert a < 2.0, a

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
