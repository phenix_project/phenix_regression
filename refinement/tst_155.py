from __future__ import division
from __future__ import print_function
from phenix_regression.refinement import run_fmodel
import os
from phenix_regression.refinement import run_phenix_refine
from libtbx.test_utils import assert_lines_in_file

pdb_str_1 = """\
ATOM    489  N   CYS A  63      16.809   1.821   1.124  1.00  7.72           N
ATOM    490  CA  CYS A  63      16.521   0.993  -0.034  1.00  8.45           C
ATOM    491  C   CYS A  63      16.034   1.957  -1.105  1.00  8.63           C
ATOM    492  O   CYS A  63      15.116   2.745  -0.862  1.00  9.58           O
ATOM    493  CB  CYS A  63      15.451  -0.053   0.310  1.00  9.35           C
ATOM    494  SG  CYS A  63      15.157  -1.361  -0.920  1.00  9.30           S
ATOM    495  N   HIS A  64      16.663   1.921  -2.275  1.00  8.88           N
ATOM    496  CA  HIS A  64      16.338   2.874  -3.327  1.00  9.42           C
ATOM    497  C   HIS A  64      16.247   2.174  -4.675  1.00  8.52           C
ATOM    498  O   HIS A  64      15.158   2.021  -5.234  1.00 10.04           O
ATOM    499  CB  HIS A  64      17.389   3.986  -3.358  1.00  9.67           C
ATOM    500  CG  HIS A  64      17.093   5.067  -4.346  1.00 10.21           C
ATOM    501  ND1 HIS A  64      15.923   5.795  -4.316  1.00 12.00           N
ATOM    502  CD2 HIS A  64      17.819   5.560  -5.375  1.00 10.18           C
ATOM    503  CE1 HIS A  64      15.935   6.682  -5.294  1.00 13.21           C
ATOM    504  NE2 HIS A  64      17.075   6.563  -5.951  1.00 11.92           N
ATOM    505  OXT HIS A  64      17.254   1.723  -5.221  1.00  8.83           O
TER
END
CRYST1   45.900   40.700   30.100  90.00  90.00  90.00 P 21 21 21
ATOM   1897  O  AHOH S   1       6.003   4.057  -0.363  0.50  4.89           O
ATOM   1898  O  BHOH S   1       5.706   3.826  -0.598  0.50  5.75           O
ATOM   1899  O  AHOH S   2       9.297   1.441  -6.047  0.50  6.78           O
ATOM   1900  O  BHOH S   2       9.438   1.582  -6.293  0.50  6.41           O
ATOM   1901  O  AHOH S   3      12.696   2.959   0.294  0.50  6.84           O
ATOM   1902  O  BHOH S   3      12.691   3.403   0.120  0.50  7.62           O
"""

pdb_str_2 = """\
CRYST1   45.900   40.700   30.100  90.00  90.00  90.00 P 21 21 21
ATOM      1  N  AVAL A   1      -5.310   0.009  13.097  0.50  7.86           N
ATOM      2  CA AVAL A   1      -4.861   0.497  11.798  0.50  7.65           C
ATOM      3  C  AVAL A   1      -3.748   1.520  11.980  0.50  7.41           C
ATOM      4  O  AVAL A   1      -3.553   2.054  13.080  0.50  7.46           O
ATOM      5  CB AVAL A   1      -6.027   1.064  10.967  0.50  7.78           C
ATOM      6  CG1AVAL A   1      -7.008  -0.055  10.620  0.50  7.82           C
ATOM      7  CG2AVAL A   1      -6.715   2.206  11.705  0.50  7.85           C
ATOM      8  H1 AVAL A   1      -5.095  -0.850  13.182  0.50  7.48           H
ATOM      9  H2 AVAL A   1      -4.917   0.478  13.743  0.50  7.48           H
ATOM     10  H3 AVAL A   1      -6.193   0.101  13.160  0.50  7.48           H
ATOM     11  HA AVAL A   1      -4.505  -0.254  11.297  0.50  7.40           H
ATOM     12  HB AVAL A   1      -5.677   1.429  10.139  0.50  7.68           H
ATOM     13 HG11AVAL A   1      -7.740   0.307  10.097  0.50  7.69           H
ATOM     14 HG12AVAL A   1      -6.551  -0.739  10.106  0.50  7.69           H
ATOM     15 HG13AVAL A   1      -7.357  -0.445  11.437  0.50  7.69           H
ATOM     16 HG21AVAL A   1      -7.445   2.548  11.165  0.50  7.78           H
ATOM     17 HG22AVAL A   1      -7.063   1.882  12.550  0.50  7.78           H
ATOM     18 HG23AVAL A   1      -6.076   2.916  11.870  0.50  7.78           H
ATOM     19  N  BVAL A   1      -4.907   0.086  13.366  0.50  7.48           N
ATOM     20  CA BVAL A   1      -4.623   0.708  12.088  0.50  7.40           C
ATOM     21  C  BVAL A   1      -3.460   1.690  12.233  0.50  7.34           C
ATOM     22  O  BVAL A   1      -3.124   2.108  13.339  0.50  7.40           O
ATOM     23  CB BVAL A   1      -5.874   1.392  11.507  0.50  7.68           C
ATOM     24  CG1BVAL A   1      -6.976   0.364  11.250  0.50  7.69           C
ATOM     25  CG2BVAL A   1      -6.371   2.489  12.440  0.50  7.78           C
ATOM     26  H1 BVAL A   1      -4.820  -0.797  13.293  0.50  7.48           H
ATOM     27  H2 BVAL A   1      -4.337   0.390  13.978  0.50  7.48           H
ATOM     28  H3 BVAL A   1      -5.739   0.282  13.613  0.50  7.48           H
ATOM     29  HA BVAL A   1      -4.363   0.017  11.459  0.50  7.40           H
ATOM     30  HB BVAL A   1      -5.633   1.800  10.661  0.50  7.68           H
ATOM     31 HG11BVAL A   1      -7.756   0.810  10.885  0.50  7.69           H
ATOM     32 HG12BVAL A   1      -6.658  -0.300  10.618  0.50  7.69           H
ATOM     33 HG13BVAL A   1      -7.214  -0.071  12.084  0.50  7.69           H
ATOM     34 HG21BVAL A   1      -7.158   2.907  12.057  0.50  7.78           H
ATOM     35 HG22BVAL A   1      -6.596   2.104  13.301  0.50  7.78           H
ATOM     36 HG23BVAL A   1      -5.675   3.155  12.556  0.50  7.78           H
ATOM     37  N  ALYS A   2      -3.031   1.813  10.890  0.50  7.10           N
ATOM     38  CA ALYS A   2      -1.867   2.692  10.952  0.50  6.99           C
ATOM     39  C  ALYS A   2      -1.663   3.380   9.609  0.50  6.78           C
ATOM     40  O  ALYS A   2      -2.049   2.856   8.560  0.50  6.90           O
ATOM     41  CB ALYS A   2      -0.619   1.892  11.315  0.50  7.22           C
ATOM     42  CG ALYS A   2      -0.290   0.787  10.324  0.50  7.18           C
ATOM     43  CD ALYS A   2       0.957   0.059  10.761  0.50  7.38           C
ATOM     44  CE ALYS A   2       1.511  -0.789   9.653  0.50  7.31           C
ATOM     45  NZ ALYS A   2       2.922  -1.160   9.932  0.50  7.32           N
ATOM     46  H  ALYS A   2      -3.206   1.510  10.104  0.50  7.12           H
ATOM     47  HA ALYS A   2      -2.021   3.363  11.635  0.50  7.07           H
ATOM     48  HB2ALYS A   2       0.137   2.497  11.375  0.50  7.11           H
ATOM     49  HB3ALYS A   2      -0.740   1.501  12.194  0.50  7.11           H
ATOM     50  HG2ALYS A   2      -1.032   0.165  10.263  0.50  7.08           H
ATOM     51  HG3ALYS A   2      -0.161   1.163   9.439  0.50  7.08           H
ATOM     52  HD2ALYS A   2       1.626   0.701  11.045  0.50  7.10           H
ATOM     53  HD3ALYS A   2       0.756  -0.499  11.528  0.50  7.10           H
ATOM     54  HE2ALYS A   2       0.974  -1.591   9.554  0.50  7.13           H
ATOM     55  HE3ALYS A   2       1.458  -0.307   8.813  0.50  7.13           H
ATOM     56  HZ1ALYS A   2       3.117  -1.921   9.514  0.50  7.18           H
ATOM     57  HZ2ALYS A   2       3.462  -0.515   9.641  0.50  7.18           H
ATOM     58  HZ3ALYS A   2       3.034  -1.266  10.809  0.50  7.18           H
ATOM     59  N  BLYS A   2      -2.856   2.066  11.109  0.50  7.12           N
ATOM     60  CA BLYS A   2      -1.754   3.021  11.117  0.50  7.07           C
ATOM     61  C  BLYS A   2      -1.663   3.678   9.748  0.50  6.86           C
ATOM     62  O  BLYS A   2      -2.239   3.203   8.763  0.50  6.83           O
ATOM     63  CB BLYS A   2      -0.426   2.343  11.456  0.50  7.11           C
ATOM     64  CG BLYS A   2      -0.026   1.287  10.455  0.50  7.08           C
ATOM     65  CD BLYS A   2       1.342   0.731  10.740  0.50  7.10           C
ATOM     66  CE BLYS A   2       1.734  -0.252   9.669  0.50  7.13           C
ATOM     67  NZ BLYS A   2       3.135  -0.696   9.838  0.50  7.18           N
ATOM     68  H  BLYS A   2      -3.071   1.778  10.328  0.50  7.12           H
ATOM     69  HA BLYS A   2      -1.927   3.687  11.801  0.50  7.07           H
ATOM     70  HB2BLYS A   2       0.271   3.016  11.504  0.50  7.11           H
ATOM     71  HB3BLYS A   2      -0.491   1.939  12.335  0.50  7.11           H
ATOM     72  HG2BLYS A   2      -0.676   0.567  10.468  0.50  7.08           H
ATOM     73  HG3BLYS A   2      -0.041   1.666   9.562  0.50  7.08           H
ATOM     74  HD2BLYS A   2       1.990   1.452  10.780  0.50  7.10           H
ATOM     75  HD3BLYS A   2       1.348   0.295  11.607  0.50  7.10           H
ATOM     76  HE2BLYS A   2       1.143  -1.020   9.699  0.50  7.13           H
ATOM     77  HE3BLYS A   2       1.624   0.156   8.796  0.50  7.13           H
ATOM     78  HZ1BLYS A   2       3.186  -1.575   9.708  0.50  7.18           H
ATOM     79  HZ2BLYS A   2       3.652  -0.279   9.246  0.50  7.18           H
ATOM     80  HZ3BLYS A   2       3.411  -0.502  10.662  0.50  7.18           H
ATOM     81  N  AASP A   3      -1.048   4.558   9.652  0.50  6.70           N
ATOM     82  CA AASP A   3      -0.649   5.287   8.461  0.50  6.68           C
ATOM     83  C  AASP A   3       0.834   5.050   8.207  0.50  6.57           C
ATOM     84  O  AASP A   3       1.585   4.699   9.118  0.50  6.52           O
ATOM     85  CB AASP A   3      -0.875   6.793   8.630  0.50  6.86           C
ATOM     86  CG AASP A   3      -2.273   7.153   9.124  0.50  6.89           C
ATOM     87  OD1AASP A   3      -3.259   6.496   8.729  0.50  6.91           O
ATOM     88  OD2AASP A   3      -2.376   8.137   9.890  0.50  7.24           O
ATOM     89  H  AASP A   3      -0.849   4.959  10.386  0.50  6.80           H
ATOM     90  HA AASP A   3      -1.186   4.970   7.718  0.50  6.54           H
ATOM     91  HB2AASP A   3      -0.220   7.142   9.255  0.50  6.79           H
ATOM     92  HB3AASP A   3      -0.718   7.233   7.780  0.50  6.79           H
ATOM     93  N  BASP A   3      -0.917   4.783   9.696  0.50  6.80           N
ATOM     94  CA BASP A   3      -0.617   5.479   8.454  0.50  6.54           C
ATOM     95  C  BASP A   3       0.882   5.419   8.182  0.50  6.24           C
ATOM     96  O  BASP A   3       1.682   5.340   9.120  0.50  6.20           O
ATOM     97  CB BASP A   3      -1.030   6.956   8.507  0.50  6.79           C
ATOM     98  CG BASP A   3      -2.429   7.176   9.052  0.50  6.88           C
ATOM     99  OD1BASP A   3      -3.258   6.246   9.021  0.50  6.94           O
ATOM    100  OD2BASP A   3      -2.689   8.315   9.503  0.50  6.99           O
ATOM    101  H  BASP A   3      -0.569   5.150  10.392  0.50  6.80           H
ATOM    102  HA BASP A   3      -1.120   5.039   7.751  0.50  6.54           H
ATOM    103  HB2BASP A   3      -0.396   7.442   9.058  0.50  6.79           H
ATOM    104  HB3BASP A   3      -0.976   7.331   7.614  0.50  6.79           H
TER
ATOM   1923  O  ZHOH S   1       6.006   4.060  -0.359  0.50  4.97           O
ATOM   1924  O  YHOH S   1       5.710   3.827  -0.597  0.50  5.59           O
ATOM   1925  O  ZHOH S   2       9.296   1.442  -6.050  0.50  6.47           O
ATOM   1926  O  YHOH S   2       9.442   1.583  -6.296  0.50  6.54           O
ATOM   1927  O  ZHOH S   3      12.692   2.960   0.293  0.50  6.91           O
ATOM   1928  O  YHOH S   3      12.696   3.399   0.124  0.50  7.55           O
ATOM   1929  O  ZHOH S   4      15.568   4.286   2.039  0.50  9.37           O
ATOM   1930  O  YHOH S   4      15.306   4.495   1.831  0.50 10.94           O
ATOM   1931  O  ZHOH S   5     -10.019   5.403  -1.445  0.45  9.34           O
ATOM   1932  O  YHOH S   5     -10.175   4.909  -1.385  0.45  9.26           O
ATOM   1933  O  ZHOH S   6      -6.145   9.877  -3.209  0.49  8.23           O
ATOM   1934  O  YHOH S   6      -5.967  10.341  -2.677  0.49  9.06           O
ATOM   1935  O  ZHOH S   7       5.498   1.935  -7.744  0.50 11.36           O
ATOM   1936  O  YHOH S   7       5.176   1.839  -7.043  0.50  7.18           O
ATOM   1937  O  ZHOH S   8      11.420   7.239  -5.059  0.48  7.06           O
ATOM   1938  O  YHOH S   8      12.387   6.331  -5.075  0.48 15.49           O
ATOM   1939  O  ZHOH S   9      11.664  10.846  -6.749  0.41  8.25           O
ATOM   1940  O  YHOH S   9      11.931  10.348  -6.663  0.41  9.79           O
ATOM   1941  O  ZHOH S  10      14.994   5.930  -0.552  0.47 15.76           O
ATOM   1942  O  YHOH S  10      15.491   5.897  -0.196  0.47 10.08           O
END
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Ask to fit rotamers for altlocs but

  1) the model does not havae altlocs in protein.

  2) Water altlocs do not match protein altlocs.

  Don't crash, just keep going!
  """
  for i, pdb_raw in enumerate([pdb_str_1, pdb_str_2]):
    prefix = prefix + "_"+str(i)
    pdb_in = "%s.pdb"%prefix
    open(pdb_in, "w").write(pdb_raw)
    args = [
      pdb_in,
      "high_resolution=2.7",
      "type=real",
      "label=F",
      "r_free_flags_fraction=0.1"]
    r = run_fmodel(args = args, prefix = prefix)
    #
    args = [pdb_in, r.mtz,
      "refinement.main.fit_altlocs_method=masking",
      "refinement.refine.sites.individual=protein",
      "refinement.refine.adp.individual.isotropic=protein",
      "refinement.refine.occupancies.remove_selection=water",
      "refinement.bulk_solvent_and_scale.apply_back_trace=False",
      "refinement.pdb_interpretation.const_shrink_donor_acceptor=0.6",
      "refinement.target_weights.wc=4",
      "main.number_of_mac=1"]
    r = run_phenix_refine(args = args, prefix = prefix)

if (__name__ == "__main__") :
  run()
  print("OK")
