from __future__ import division, print_function
from libtbx.test_utils import assert_lines_in_file
import time
import os
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel
from libtbx.test_utils import assert_lines_in_file

pdb_str = """
CRYST1   30.410   14.706   21.507  90.00  90.00  90.00 P 1
ATOM      1  N   GLY A   1       5.043   9.706  12.193  1.00 16.77           N
ATOM      2  CA  GLY A   1       5.000   9.301  10.742  1.00 16.57           C
ATOM      3  C   GLY A   1       6.037   8.234  10.510  1.00 16.16           C
ATOM      4  O   GLY A   1       6.529   7.615  11.472  1.00 16.78           O
ATOM      5  N   ASN A   2       6.396   8.017   9.246  1.00 15.02           N
ATOM      6  CA  ASN A   2       7.530   7.132   8.922  1.00 14.10           C
ATOM      7  C   ASN A   2       8.811   7.631   9.518  1.00 13.13           C
ATOM      8  O   ASN A   2       9.074   8.836   9.517  1.00 11.91           O
ATOM      9  CB  ASN A   2       7.706   6.975   7.432  1.00 15.38           C
ATOM     10  CG  ASN A   2       6.468   6.436   6.783  1.00 14.08           C
ATOM     11  OD1 ASN A   2       6.027   5.321   7.107  1.00 17.46           O
ATOM     12  ND2 ASN A   2       5.848   7.249   5.922  1.00 11.72           N
ATOM     13  N   ASN A   3       9.614   6.684   9.996  1.00 12.26           N
ATOM     14  CA  ASN A   3      10.859   6.998  10.680  1.00 11.74           C
ATOM     15  C   ASN A   3      12.097   6.426   9.986  1.00 11.10           C
ATOM     16  O   ASN A   3      12.180   5.213   9.739  1.00 10.42           O
ATOM     17  CB  ASN A   3      10.793   6.472  12.133  1.00 12.15           C
ATOM     18  CG  ASN A   3      12.046   6.833  12.952  1.00 12.82           C
ATOM     19  OD1 ASN A   3      12.350   8.019  13.163  1.00 15.05           O
ATOM     20  ND2 ASN A   3      12.781   5.809  13.397  1.00 13.48           N
ATOM     21  N   GLN A   4      13.047   7.322   9.689  1.00 10.29           N
ATOM     22  CA  GLN A   4      14.436   6.982   9.290  1.00 10.53           C
ATOM     23  C   GLN A   4      15.487   7.700  10.179  1.00 10.24           C
ATOM     24  O   GLN A   4      15.599   8.937  10.206  1.00  8.86           O
ATOM     25  CB  GLN A   4      14.708   7.242   7.802  1.00  9.80           C
ATOM     26  CG  GLN A   4      15.996   6.552   7.304  1.00 10.25           C
ATOM     27  CD  GLN A   4      16.556   7.138   6.002  1.00 12.43           C
ATOM     28  OE1 GLN A   4      16.796   8.362   5.901  1.00 14.62           O
ATOM     29  NE2 GLN A   4      16.802   6.255   5.000  1.00  9.05           N
ATOM     30  N   GLN A   5      16.206   6.915  10.962  1.00 10.38           N
ATOM     31  CA  GLN A   5      17.322   7.455  11.731  1.00 11.39           C
ATOM     32  C   GLN A   5      18.646   6.862  11.263  1.00 11.52           C
ATOM     33  O   GLN A   5      18.820   5.640  11.145  1.00 12.05           O
ATOM     34  CB  GLN A   5      17.108   7.277  13.238  1.00 11.96           C
ATOM     35  CG  GLN A   5      15.881   8.044  13.738  1.00 10.81           C
ATOM     36  CD  GLN A   5      15.396   7.508  15.045  1.00 13.10           C
ATOM     37  OE1 GLN A   5      14.826   6.419  15.093  1.00 10.65           O
ATOM     38  NE2 GLN A   5      15.601   8.281  16.130  1.00 12.30           N
ATOM     39  N   ASN A   6      19.566   7.758  10.947  1.00 11.99           N
ATOM     40  CA  ASN A   6      20.883   7.404  10.409  1.00 12.30           C
ATOM     41  C   ASN A   6      21.906   7.855  11.415  1.00 13.40           C
ATOM     42  O   ASN A   6      22.271   9.037  11.465  1.00 13.92           O
ATOM     43  CB  ASN A   6      21.117   8.110   9.084  1.00 12.13           C
ATOM     44  CG  ASN A   6      20.013   7.829   8.094  1.00 12.77           C
ATOM     45  OD1 ASN A   6      19.850   6.698   7.642  1.00 14.27           O
ATOM     46  ND2 ASN A   6      19.247   8.841   7.770  1.00 10.07           N
ATOM     47  N   TYR A   7      22.344   6.911  12.238  1.00 14.70           N
ATOM     48  CA  TYR A   7      23.211   7.238  13.390  1.00 15.18           C
ATOM     49  C   TYR A   7      24.655   7.425  12.976  1.00 15.91           C
ATOM     50  O   TYR A   7      25.093   6.905  11.946  1.00 15.76           O
ATOM     51  CB  TYR A   7      23.113   6.159  14.460  1.00 15.35           C
ATOM     52  CG  TYR A   7      21.717   6.023  14.993  1.00 14.45           C
ATOM     53  CD1 TYR A   7      20.823   5.115  14.418  1.00 15.68           C
ATOM     54  CD2 TYR A   7      21.262   6.850  16.011  1.00 14.80           C
ATOM     55  CE1 TYR A   7      19.532   5.000  14.887  1.00 13.46           C
ATOM     56  CE2 TYR A   7      19.956   6.743  16.507  1.00 14.33           C
ATOM     57  CZ  TYR A   7      19.099   5.823  15.922  1.00 15.09           C
ATOM     58  OH  TYR A   7      17.818   5.683  16.382  1.00 14.39           O
ATOM     59  OXT TYR A   7      25.410   8.093  13.703  1.00 17.49           O
TER
END
"""

pdb_ref_str = """
CRYST1   37.644   18.093   26.884  90.00  90.00  90.00 P 1
ATOM      1  N   GLY A   1       7.277   9.706  12.570  1.00 16.77           N
ATOM      2  CA  GLY A   1       7.234   9.301  11.119  1.00 16.57           C
ATOM      3  C   GLY A   1       8.271   8.234  10.887  1.00 16.16           C
ATOM      4  O   GLY A   1       8.763   7.615  11.849  1.00 16.78           O
ATOM      5  N   ASN A   2       8.630   8.017   9.623  1.00 15.02           N
ATOM      6  CA  ASN A   2       9.764   7.132   9.299  1.00 14.10           C
ATOM      7  C   ASN A   2      11.045   7.631   9.895  1.00 13.13           C
ATOM      8  O   ASN A   2      11.308   8.836   9.894  1.00 11.91           O
ATOM      9  CB  ASN A   2       9.940   6.975   7.809  1.00 15.38           C
ATOM     10  CG  ASN A   2       8.702   6.436   7.160  1.00 14.08           C
ATOM     11  OD1 ASN A   2       8.261   5.321   7.484  1.00 17.46           O
ATOM     12  ND2 ASN A   2       8.082   7.249   6.299  1.00 11.72           N
ATOM     13  N   ASN A   3      11.848   6.684  10.373  1.00 12.26           N
ATOM     14  CA  ASN A   3      13.093   6.998  11.057  1.00 11.74           C
ATOM     15  C   ASN A   3      14.331   6.426  10.363  1.00 11.10           C
ATOM     16  O   ASN A   3      14.414   5.213  10.116  1.00 10.42           O
ATOM     17  CB  ASN A   3      13.027   6.472  12.510  1.00 12.15           C
ATOM     18  CG  ASN A   3      14.280   6.833  13.329  1.00 12.82           C
ATOM     19  OD1 ASN A   3      14.584   8.019  13.540  1.00 15.05           O
ATOM     20  ND2 ASN A   3      15.015   5.809  13.774  1.00 13.48           N
ATOM     21  N   GLN A   4      15.281   7.322  10.066  1.00 10.29           N
ATOM     22  CA  GLN A   4      16.670   6.982   9.667  1.00 10.53           C
ATOM     23  C   GLN A   4      17.721   7.700  10.556  1.00 10.24           C
ATOM     24  O   GLN A   4      17.833   8.937  10.583  1.00  8.86           O
ATOM     25  CB  GLN A   4      16.942   7.242   8.179  1.00  9.80           C
ATOM     26  CG  GLN A   4      18.230   6.552   7.681  1.00 10.25           C
ATOM     27  CD  GLN A   4      18.790   7.138   6.379  1.00 12.43           C
ATOM     28  OE1 GLN A   4      19.030   8.362   6.278  1.00 14.62           O
ATOM     29  NE2 GLN A   4      19.036   6.255   5.377  1.00  9.05           N
ATOM     30  N   GLN A   5      18.440   6.915  11.339  1.00 10.38           N
ATOM     31  CA  GLN A   5      19.556   7.455  12.108  1.00 11.39           C
ATOM     32  C   GLN A   5      20.880   6.862  11.640  1.00 11.52           C
ATOM     33  O   GLN A   5      21.054   5.640  11.522  1.00 12.05           O
ATOM     34  CB  GLN A   5      19.342   7.277  13.615  1.00 11.96           C
ATOM     35  CG  GLN A   5      18.115   8.044  14.115  1.00 10.81           C
ATOM     36  CD  GLN A   5      17.630   7.508  15.422  1.00 13.10           C
ATOM     37  OE1 GLN A   5      17.060   6.419  15.470  1.00 10.65           O
ATOM     38  NE2 GLN A   5      17.835   8.281  16.507  1.00 12.30           N
ATOM     39  N   ASN A   6      21.800   7.758  11.324  1.00 11.99           N
ATOM     40  CA  ASN A   6      23.117   7.404  10.786  1.00 12.30           C
ATOM     41  C   ASN A   6      24.140   7.855  11.792  1.00 13.40           C
ATOM     42  O   ASN A   6      24.505   9.037  11.842  1.00 13.92           O
ATOM     43  CB  ASN A   6      23.351   8.110   9.461  1.00 12.13           C
ATOM     44  CG  ASN A   6      22.247   7.829   8.471  1.00 12.77           C
ATOM     45  OD1 ASN A   6      22.084   6.698   8.019  1.00 14.27           O
ATOM     46  ND2 ASN A   6      21.481   8.841   8.147  1.00 10.07           N
ATOM     47  N   TYR A   7      24.578   6.911  12.615  1.00 14.70           N
ATOM     48  CA  TYR A   7      25.445   7.238  13.767  1.00 15.18           C
ATOM     49  C   TYR A   7      26.889   7.425  13.353  1.00 15.91           C
ATOM     50  O   TYR A   7      27.327   6.905  12.323  1.00 15.76           O
ATOM     51  CB  TYR A   7      25.347   6.159  14.837  1.00 15.35           C
ATOM     52  CG  TYR A   7      23.951   6.023  15.370  1.00 14.45           C
ATOM     53  CD1 TYR A   7      23.057   5.115  14.795  1.00 15.68           C
ATOM     54  CD2 TYR A   7      23.496   6.850  16.388  1.00 14.80           C
ATOM     55  CE1 TYR A   7      21.766   5.000  15.264  1.00 13.46           C
ATOM     56  CE2 TYR A   7      22.190   6.743  16.884  1.00 14.33           C
ATOM     57  CZ  TYR A   7      21.333   5.823  16.299  1.00 15.09           C
ATOM     58  OH  TYR A   7      20.052   5.683  16.759  1.00 14.39           O
ATOM     59  OXT TYR A   7      27.644   8.093  14.080  1.00 17.49           O
TER
ATOM     60  N   WT6 B   7      29.578  11.911  17.615  1.00 14.70           N
ATOM     61  CA  WT6 B   7      30.445  12.238  18.767  1.00 15.18           C
ATOM     62  C   WT6 B   7      31.889  12.425  18.353  1.00 15.91           C
ATOM     63  O   WT6 B   7      32.327  11.905  17.323  1.00 15.76           O
ATOM     64  CB  WT6 B   7      30.347  11.159  19.837  1.00 15.35           C
ATOM     65  CG  WT6 B   7      28.951  11.023  20.370  1.00 14.45           C
ATOM     66  CD1 WT6 B   7      28.057  10.115  19.795  1.00 15.68           C
ATOM     67  CD2 WT6 B   7      28.496  11.850  21.388  1.00 14.80           C
ATOM     68  CE1 WT6 B   7      26.766  10.000  20.264  1.00 13.46           C
ATOM     69  CE2 WT6 B   7      27.190  11.743  21.884  1.00 14.33           C
ATOM     70  CZ  WT6 B   7      26.333  10.823  21.299  1.00 15.09           C
ATOM     71  OH  WT6 B   7      25.052  10.683  21.759  1.00 14.39           O
ATOM     72  OXT WT6 B   7      32.644  13.093  19.080  1.00 17.49           O
HETATM   73  O   HOH A   8       9.815  10.321  13.592  1.00 22.62           O
HETATM   74  O   HOH A   9      26.717   6.952   9.684  1.00 19.71           O
HETATM   75  O   HOH A  10       5.000   6.850   5.000  1.00 17.08           O
HETATM   76  O   HOH A  11      28.094   9.273  16.438  1.00 23.99           O
HETATM   77  O   HOH A  12      29.891   6.421  15.666  1.00 26.17           O
HETATM   78  O   HOH A  13      13.537   8.523  16.492  1.00 39.15           O
HETATM   79  O   HOH A  14      14.786   5.776  17.435  1.00 43.49           O
END
"""

params_str = """
refinement {
  reference_model {
    enabled = True
    file = %s
  }
}
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Do not care about unknown ligands in reference model (torsion).
  """
  with open("%s.pdb"%prefix, "w") as fo:
    fo.write(pdb_str)
  ref_file_name = "%s_ref.pdb"%prefix
  with open(ref_file_name, "w") as fo:
    fo.write(pdb_ref_str)
  with open("%s.eff"%prefix, "w") as fo:
    fo.write(params_str%ref_file_name)
  #
  args = ["%s.pdb"%prefix, "high_res=5 type=real r_free=0.1"]
  r = run_fmodel(args = args, prefix = prefix)
  #
  args = ["%s.pdb"%prefix, r.mtz, "%s.eff"%prefix, "main.number_of_mac=1"]
  r = run_phenix_refine(args = args, prefix = prefix)
  #
  assert_lines_in_file(r.log, "Total # of matched residue pairs: 7")

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK", "time: %.2f"%(time.time()-t0))

