from __future__ import division
from __future__ import print_function
import libtbx.load_env
import os
from phenix_regression.refinement import run_phenix_refine

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Assortment of old tests. Converted from primitive csh scripts.
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/Build_combine_extend_4.pdb",
    test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/exp.mtz",
    test=os.path.isfile)
  args = [
    pdb,hkl,
    "target_weights.wu=0",
    "modify_start_model.modify.adp.set_b_iso=0.0",
    "strategy=none",
    "xray_data.high_resolution=4.0"]
  r = run_phenix_refine(args = args, prefix = prefix)

if (__name__ == "__main__"):
  run()
  print("OK")
