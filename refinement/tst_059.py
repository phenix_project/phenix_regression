from __future__ import division
from __future__ import print_function
from phenix_regression.refinement import run_phenix_refine
import libtbx.load_env
import os
import libtbx.load_env

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Purpose unknown.
  """
  mtz_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/3p4j.pdb",
    test=os.path.isfile)
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/3p4j.mtz",
    test=os.path.isfile)
  args = [
    mtz_file,
    pdb_file,
    "xray_data.high_resolution=6",
    "main.number_of_macro_cycles=1"]
  r = run_phenix_refine(args = args, prefix = prefix)

if (__name__ == "__main__"):
  run()
  print("OK")
