from __future__ import division
import libtbx.load_env
import os, time
from phenix_regression.refinement import run_phenix_refine
from libtbx.test_utils import approx_equal
from libtbx import easy_pickle

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Test output fmodel pkl file.
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/pdb1akg_very_well_phenix_refined_001.pdb",
    test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/1akg.mtz", \
    test=os.path.isfile)
  args = [
    pdb,hkl,
    "pickle_fmodel=true",
    "xray_data.high_res=3.",
    "main.number_of_macro_cycles=1"]
  r = run_phenix_refine(args = args, prefix = prefix)
  fmodel = easy_pickle.load("%s_001_fmodel.pickle"%prefix)
  assert approx_equal(fmodel.r_work(), r.r_work_final, 1.e-4)
  assert approx_equal(fmodel.r_free(), r.r_free_final, 1.e-4)

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK", "time: %.2f"%(time.time()-t0))