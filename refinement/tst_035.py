from __future__ import division
from __future__ import print_function
import os
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel
import iotbx.pdb
from scitbx.array_family import flex

pdb_str_answer = """\
CRYST1   27.475   35.191   27.490  90.00  90.00  90.00 P 21 21 21
HELIX    1   1 ALA E    1  ALA E   16  1                                  16
ATOM      1  N   ALA E   1       6.709  31.817   5.664  1.00 20.00           N
ATOM      2  CA  ALA E   1       7.794  30.914   6.029  1.00 20.00           C
ATOM      3  C   ALA E   1       7.267  29.691   6.773  1.00 20.00           C
ATOM      4  O   ALA E   1       7.942  29.146   7.647  1.00 20.00           O
ATOM      5  CB  ALA E   1       8.831  31.643   6.870  1.00 20.00           C
ATOM      6  N   LYS E   2       6.057  29.267   6.421  1.00 20.00           N
ATOM      7  CA  LYS E   2       5.434  28.110   7.053  1.00 20.00           C
ATOM      8  C   LYS E   2       6.176  26.824   6.701  1.00 20.00           C
ATOM      9  O   LYS E   2       6.477  26.012   7.576  1.00 20.00           O
ATOM     10  CB  LYS E   2       3.966  28.000   6.639  1.00 20.00           C
ATOM     11  CG  LYS E   2       3.086  29.125   7.161  1.00 20.00           C
ATOM     12  CD  LYS E   2       1.650  28.966   6.693  1.00 20.00           C
ATOM     13  CE  LYS E   2       0.766  30.077   7.235  1.00 20.00           C
ATOM     14  NZ  LYS E   2      -0.641  29.948   6.764  1.00 20.00           N
ATOM     15  N   CYS E   3       6.468  26.648   5.417  1.00 20.00           N
ATOM     16  CA  CYS E   3       7.193  25.473   4.949  1.00 20.00           C
ATOM     17  C   CYS E   3       8.635  25.492   5.443  1.00 20.00           C
ATOM     18  O   CYS E   3       9.223  24.444   5.712  1.00 20.00           O
ATOM     19  CB  CYS E   3       7.161  25.396   3.421  1.00 20.00           C
ATOM     20  SG  CYS E   3       5.501  25.285   2.714  1.00 20.00           S
ATOM     21  N   ALA E   4       9.197  26.690   5.560  1.00 20.00           N
ATOM     22  CA  ALA E   4      10.560  26.855   6.050  1.00 20.00           C
ATOM     23  C   ALA E   4      10.655  26.475   7.523  1.00 20.00           C
ATOM     24  O   ALA E   4      11.599  25.805   7.942  1.00 20.00           O
ATOM     25  CB  ALA E   4      11.031  28.285   5.837  1.00 20.00           C
ATOM     26  N   ILE E   5       9.669  26.907   8.304  1.00 20.00           N
ATOM     27  CA  ILE E   5       9.621  26.584   9.725  1.00 20.00           C
ATOM     28  C   ILE E   5       9.325  25.103   9.932  1.00 20.00           C
ATOM     29  O   ILE E   5       9.836  24.482  10.865  1.00 20.00           O
ATOM     30  CB  ILE E   5       8.580  27.459  10.467  1.00 20.00           C
ATOM     31  CG1 ILE E   5       9.071  28.905  10.559  1.00 20.00           C
ATOM     32  CG2 ILE E   5       8.307  26.928  11.866  1.00 20.00           C
ATOM     33  CD1 ILE E   5       8.105  29.836  11.259  1.00 20.00           C
ATOM     34  N   MET E   6       8.498  24.543   9.054  1.00 20.00           N
ATOM     35  CA  MET E   6       8.157  23.127   9.116  1.00 20.00           C
ATOM     36  C   MET E   6       9.375  22.264   8.811  1.00 20.00           C
ATOM     37  O   MET E   6       9.598  21.239   9.454  1.00 20.00           O
ATOM     38  CB  MET E   6       7.022  22.803   8.144  1.00 20.00           C
ATOM     39  CG  MET E   6       5.694  23.451   8.499  1.00 20.00           C
ATOM     40  SD  MET E   6       5.087  22.950  10.120  1.00 20.00           S
ATOM     41  CE  MET E   6       4.738  21.217   9.831  1.00 20.00           C
ATOM     42  N   THR E   7      10.161  22.688   7.825  1.00 20.00           N
ATOM     43  CA  THR E   7      11.380  21.976   7.459  1.00 20.00           C
ATOM     44  C   THR E   7      12.441  22.134   8.541  1.00 20.00           C
ATOM     45  O   THR E   7      13.199  21.204   8.823  1.00 20.00           O
ATOM     46  CB  THR E   7      11.948  22.456   6.109  1.00 20.00           C
ATOM     47  OG1 THR E   7      11.987  23.888   6.086  1.00 20.00           O
ATOM     48  CG2 THR E   7      11.086  21.957   4.959  1.00 20.00           C
ATOM     49  N   ILE E   8      12.489  23.318   9.145  1.00 20.00           N
ATOM     50  CA  ILE E   8      13.433  23.596  10.220  1.00 20.00           C
ATOM     51  C   ILE E   8      13.128  22.736  11.441  1.00 20.00           C
ATOM     52  O   ILE E   8      14.038  22.232  12.099  1.00 20.00           O
ATOM     53  CB  ILE E   8      13.437  25.097  10.601  1.00 20.00           C
ATOM     54  CG1 ILE E   8      14.259  25.898   9.591  1.00 20.00           C
ATOM     55  CG2 ILE E   8      14.003  25.309  11.997  1.00 20.00           C
ATOM     56  CD1 ILE E   8      14.305  27.383   9.880  1.00 20.00           C
ATOM     57  N   VAL E   9      11.843  22.573  11.737  1.00 20.00           N
ATOM     58  CA  VAL E   9      11.415  21.732  12.848  1.00 20.00           C
ATOM     59  C   VAL E   9      11.639  20.260  12.519  1.00 20.00           C
ATOM     60  O   VAL E   9      11.974  19.460  13.395  1.00 20.00           O
ATOM     61  CB  VAL E   9       9.931  21.956  13.193  1.00 20.00           C
ATOM     62  CG1 VAL E   9       9.479  20.976  14.264  1.00 20.00           C
ATOM     63  CG2 VAL E   9       9.699  23.391  13.642  1.00 20.00           C
ATOM     64  N   SER E  10      11.450  19.912  11.250  1.00 20.00           N
ATOM     65  CA  SER E  10      11.664  18.546  10.785  1.00 20.00           C
ATOM     66  C   SER E  10      13.125  18.147  10.944  1.00 20.00           C
ATOM     67  O   SER E  10      13.431  17.029  11.357  1.00 20.00           O
ATOM     68  CB  SER E  10      11.237  18.394   9.324  1.00 20.00           C
ATOM     69  OG  SER E  10      12.163  19.021   8.455  1.00 20.00           O
ATOM     70  N   VAL E  11      14.025  19.068  10.613  1.00 20.00           N
ATOM     71  CA  VAL E  11      15.454  18.835  10.782  1.00 20.00           C
ATOM     72  C   VAL E  11      15.820  18.850  12.262  1.00 20.00           C
ATOM     73  O   VAL E  11      16.689  18.095  12.705  1.00 20.00           O
ATOM     74  CB  VAL E  11      16.292  19.885  10.015  1.00 20.00           C
ATOM     75  CG1 VAL E  11      17.776  19.723  10.312  1.00 20.00           C
ATOM     76  CG2 VAL E  11      16.036  19.776   8.520  1.00 20.00           C
ATOM     77  N   ASP E  12      15.147  19.714  13.017  1.00 20.00           N
ATOM     78  CA  ASP E  12      15.358  19.816  14.457  1.00 20.00           C
ATOM     79  C   ASP E  12      15.062  18.486  15.137  1.00 20.00           C
ATOM     80  O   ASP E  12      15.780  18.073  16.046  1.00 20.00           O
ATOM     81  CB  ASP E  12      14.495  20.925  15.064  1.00 20.00           C
ATOM     82  CG  ASP E  12      15.107  22.301  14.890  1.00 20.00           C
ATOM     83  OD1 ASP E  12      16.350  22.397  14.824  1.00 20.00           O
ATOM     84  OD2 ASP E  12      14.344  23.288  14.820  1.00 20.00           O
ATOM     85  N   ALA E  13      14.001  17.822  14.690  1.00 20.00           N
ATOM     86  CA  ALA E  13      13.688  16.481  15.167  1.00 20.00           C
ATOM     87  C   ALA E  13      14.741  15.506  14.655  1.00 20.00           C
ATOM     88  O   ALA E  13      15.423  14.826  15.444  1.00 20.00           O
ATOM     89  CB  ALA E  13      12.303  16.062  14.706  1.00 20.00           C
ATOM     90  N   LYS E  14      14.885  15.479  13.328  1.00 20.00           N
ATOM     91  CA  LYS E  14      15.842  14.623  12.623  1.00 20.00           C
ATOM     92  C   LYS E  14      17.192  14.548  13.323  1.00 20.00           C
ATOM     93  O   LYS E  14      17.881  13.533  13.245  1.00 20.00           O
ATOM     94  CB  LYS E  14      16.033  15.120  11.189  1.00 20.00           C
ATOM     95  CG  LYS E  14      14.795  14.988  10.316  1.00 20.00           C
ATOM     96  CD  LYS E  14      15.047  15.521   8.916  1.00 20.00           C
ATOM     97  CE  LYS E  14      13.818  15.368   8.035  1.00 20.00           C
ATOM     98  NZ  LYS E  14      14.043  15.909   6.667  1.00 20.00           N
ATOM     99  N   ALA E  15      17.558  15.630  14.003  1.00 20.00           N
ATOM    100  CA  ALA E  15      18.699  15.619  14.904  1.00 20.00           C
ATOM    101  C   ALA E  15      18.268  15.190  16.307  1.00 20.00           C
ATOM    102  O   ALA E  15      19.035  14.538  17.024  1.00 20.00           O
ATOM    103  CB  ALA E  15      19.357  16.988  14.944  1.00 20.00           C
ATOM    104  N   GLU E  16      17.036  15.542  16.687  1.00 20.00           N
ATOM    105  CA  GLU E  16      16.549  15.284  18.047  1.00 20.00           C
ATOM    106  C   GLU E  16      16.685  13.831  18.518  1.00 20.00           C
ATOM    107  O   GLU E  16      17.064  13.664  19.676  1.00 20.00           O
ATOM    108  CB  GLU E  16      15.119  15.792  18.273  1.00 20.00           C
ATOM    109  CG  GLU E  16      15.040  17.145  18.965  1.00 20.00           C
ATOM    110  CD  GLU E  16      13.612  17.612  19.170  1.00 20.00           C
ATOM    111  OE1 GLU E  16      12.682  16.879  18.772  1.00 20.00           O
ATOM    112  OE2 GLU E  16      13.420  18.711  19.730  1.00 20.00           O
END
"""

pdb_str_poor = """\
CRYST1   27.475   35.191   27.490  90.00  90.00  90.00 P 21 21 21
SCALE1      0.036397  0.000000  0.000000        0.00000
SCALE2      0.000000  0.028416  0.000000        0.00000
SCALE3      0.000000  0.000000  0.036377        0.00000
ATOM      1  N   ALA E   1       5.565  32.051   5.059  1.00 20.00           N
ATOM      2  CA  ALA E   1       6.863  31.514   5.398  1.00 20.00           C
ATOM      3  C   ALA E   1       6.751  30.315   6.342  1.00 20.00           C
ATOM      4  O   ALA E   1       7.646  30.050   7.152  1.00 20.00           O
ATOM      5  CB  ALA E   1       7.718  32.564   6.032  1.00 20.00           C
ATOM      6  N   LYS E   2       5.645  29.575   6.183  1.00 20.00           N
ATOM      7  CA  LYS E   2       5.381  28.374   6.937  1.00 20.00           C
ATOM      8  C   LYS E   2       6.155  27.225   6.304  1.00 20.00           C
ATOM      9  O   LYS E   2       6.433  26.188   6.959  1.00 20.00           O
ATOM     10  CB  LYS E   2       3.858  28.084   6.999  1.00 20.00           C
ATOM     11  CG  LYS E   2       3.056  29.252   7.445  1.00 20.00           C
ATOM     12  CD  LYS E   2       1.594  28.873   7.533  1.00 20.00           C
ATOM     13  CE  LYS E   2       0.767  30.112   7.387  1.00 20.00           C
ATOM     14  NZ  LYS E   2      -0.649  29.753   7.412  1.00 20.00           N
ATOM     15  N   CYS E   3       6.578  27.399   5.040  1.00 20.00           N
ATOM     16  CA  CYS E   3       7.300  26.315   4.314  1.00 20.00           C
ATOM     17  C   CYS E   3       8.749  26.156   4.733  1.00 20.00           C
ATOM     18  O   CYS E   3       9.364  25.148   4.461  1.00 20.00           O
ATOM     19  CB  CYS E   3       7.235  26.564   2.831  1.00 20.00           C
ATOM     20  SG  CYS E   3       5.661  26.021   2.121  1.00 20.00           S
ATOM     21  N   ALA E   4       9.275  27.201   5.385  1.00 20.00           N
ATOM     22  CA  ALA E   4      10.613  27.176   5.900  1.00 20.00           C
ATOM     23  C   ALA E   4      10.621  26.636   7.311  1.00 20.00           C
ATOM     24  O   ALA E   4      11.586  25.967   7.715  1.00 20.00           O
ATOM     25  CB  ALA E   4      11.218  28.548   5.855  1.00 20.00           C
ATOM     26  N   ILE E   5       9.533  26.929   8.011  1.00 20.00           N
ATOM     27  CA  ILE E   5       9.353  26.509   9.387  1.00 20.00           C
ATOM     28  C   ILE E   5       9.269  24.987   9.543  1.00 20.00           C
ATOM     29  O   ILE E   5      10.107  24.372  10.208  1.00 20.00           O
ATOM     30  CB  ILE E   5       8.095  27.233  10.008  1.00 20.00           C
ATOM     31  CG1 ILE E   5       8.338  28.754  10.092  1.00 20.00           C
ATOM     32  CG2 ILE E   5       7.817  26.774  11.381  1.00 20.00           C
ATOM     33  CD1 ILE E   5       7.182  29.576  10.506  1.00 20.00           C
ATOM     34  N   MET E   6       8.277  24.435   8.843  1.00 20.00           N
ATOM     35  CA  MET E   6       7.974  22.997   8.974  1.00 20.00           C
ATOM     36  C   MET E   6       9.114  22.179   8.412  1.00 20.00           C
ATOM     37  O   MET E   6       9.013  20.943   8.332  1.00 20.00           O
ATOM     38  CB  MET E   6       6.645  22.629   8.274  1.00 20.00           C
ATOM     39  CG  MET E   6       5.509  23.547   8.564  1.00 20.00           C
ATOM     40  SD  MET E   6       4.782  23.326  10.218  1.00 20.00           S
ATOM     41  CE  MET E   6       4.528  21.551  10.352  1.00 20.00           C
ATOM     42  N   THR E   7      10.228  22.817   8.025  1.00 20.00           N
ATOM     43  CA  THR E   7      11.419  22.088   7.653  1.00 20.00           C
ATOM     44  C   THR E   7      12.522  22.299   8.638  1.00 20.00           C
ATOM     45  O   THR E   7      13.433  21.468   8.763  1.00 20.00           O
ATOM     46  CB  THR E   7      11.952  22.523   6.260  1.00 20.00           C
ATOM     47  OG1 THR E   7      12.311  23.888   6.259  1.00 20.00           O
ATOM     48  CG2 THR E   7      10.861  22.307   5.140  1.00 20.00           C
ATOM     49  N   ILE E   8      12.479  23.439   9.359  1.00 20.00           N
ATOM     50  CA  ILE E   8      13.419  23.715  10.431  1.00 20.00           C
ATOM     51  C   ILE E   8      13.029  22.843  11.592  1.00 20.00           C
ATOM     52  O   ILE E   8      13.868  22.248  12.245  1.00 20.00           O
ATOM     53  CB  ILE E   8      13.398  25.194  10.831  1.00 20.00           C
ATOM     54  CG1 ILE E   8      14.362  25.983   9.957  1.00 20.00           C
ATOM     55  CG2 ILE E   8      13.869  25.393  12.261  1.00 20.00           C
ATOM     56  CD1 ILE E   8      14.235  27.460  10.106  1.00 20.00           C
ATOM     57  N   VAL E   9      11.710  22.770  11.823  1.00 20.00           N
ATOM     58  CA  VAL E   9      11.130  21.891  12.847  1.00 20.00           C
ATOM     59  C   VAL E   9      11.439  20.435  12.600  1.00 20.00           C
ATOM     60  O   VAL E   9      11.615  19.715  13.539  1.00 20.00           O
ATOM     61  CB  VAL E   9       9.608  22.135  12.936  1.00 20.00           C
ATOM     62  CG1 VAL E   9       9.004  21.294  14.024  1.00 20.00           C
ATOM     63  CG2 VAL E   9       9.251  23.582  13.176  1.00 20.00           C
ATOM     64  N   SER E  10      11.556  20.047  11.336  1.00 20.00           N
ATOM     65  CA  SER E  10      11.764  18.647  10.971  1.00 20.00           C
ATOM     66  C   SER E  10      13.184  18.194  11.252  1.00 20.00           C
ATOM     67  O   SER E  10      13.404  17.228  11.967  1.00 20.00           O
ATOM     68  CB  SER E  10      11.437  18.419   9.478  1.00 20.00           C
ATOM     69  OG  SER E  10      12.205  19.295   8.683  1.00 20.00           O
ATOM     70  N   VAL E  11      14.160  18.968  10.716  1.00 20.00           N
ATOM     71  CA  VAL E  11      15.575  18.577  10.813  1.00 20.00           C
ATOM     72  C   VAL E  11      16.008  18.361  12.296  1.00 20.00           C
ATOM     73  O   VAL E  11      16.931  17.625  12.582  1.00 20.00           O
ATOM     74  CB  VAL E  11      16.460  19.660  10.152  1.00 20.00           C
ATOM     75  CG1 VAL E  11      17.921  19.231  10.195  1.00 20.00           C
ATOM     76  CG2 VAL E  11      16.014  19.807   8.723  1.00 20.00           C
ATOM     77  N   ASP E  12      15.262  18.992  13.202  1.00 20.00           N
ATOM     78  CA  ASP E  12      15.516  18.906  14.630  1.00 20.00           C
ATOM     79  C   ASP E  12      14.890  17.633  15.142  1.00 20.00           C
ATOM     80  O   ASP E  12      15.428  16.971  16.058  1.00 20.00           O
ATOM     81  CB  ASP E  12      14.924  20.147  15.354  1.00 20.00           C
ATOM     82  CG  ASP E  12      15.552  21.443  14.886  1.00 20.00           C
ATOM     83  OD1 ASP E  12      16.755  21.448  14.492  1.00 20.00           O
ATOM     84  OD2 ASP E  12      14.859  22.485  14.923  1.00 20.00           O
ATOM     85  N   ALA E  13      13.786  17.258  14.512  1.00 20.00           N
ATOM     86  CA  ALA E  13      13.145  15.944  14.726  1.00 20.00           C
ATOM     87  C   ALA E  13      13.930  14.877  13.948  1.00 20.00           C
ATOM     88  O   ALA E  13      13.370  13.861  13.530  1.00 20.00           O
ATOM     89  CB  ALA E  13      11.746  15.998  14.265  1.00 20.00           C
ATOM     90  N   LYS E  14      15.202  15.144  13.725  1.00 20.00           N
ATOM     91  CA  LYS E  14      16.161  14.221  13.113  1.00 20.00           C
ATOM     92  C   LYS E  14      17.564  14.338  13.792  1.00 20.00           C
ATOM     93  O   LYS E  14      18.440  13.561  13.489  1.00 20.00           O
ATOM     94  CB  LYS E  14      16.284  14.456  11.614  1.00 20.00           C
ATOM     95  CG  LYS E  14      14.991  14.573  10.883  1.00 20.00           C
ATOM     96  CD  LYS E  14      15.199  14.939   9.442  1.00 20.00           C
ATOM     97  CE  LYS E  14      13.975  15.754   8.919  1.00 20.00           C
ATOM     98  NZ  LYS E  14      13.727  15.429   7.450  1.00 20.00           N
ATOM     99  N   ALA E  15      17.741  15.364  14.639  1.00 20.00           N
ATOM    100  CA  ALA E  15      18.993  15.580  15.315  1.00 20.00           C
ATOM    101  C   ALA E  15      18.807  15.570  16.850  1.00 20.00           C
ATOM    102  O   ALA E  15      19.747  15.882  17.595  1.00 20.00           O
ATOM    103  CB  ALA E  15      19.637  16.915  14.830  1.00 20.00           C
ATOM    104  N   GLU E  16      17.591  15.257  17.336  1.00 20.00           N
ATOM    105  CA  GLU E  16      17.381  15.054  18.798  1.00 20.00           C
ATOM    106  C   GLU E  16      17.607  13.574  19.120  1.00 20.00           C
ATOM    107  O   GLU E  16      18.162  13.237  20.185  1.00 20.00           O
ATOM    108  CB  GLU E  16      16.032  15.513  19.266  1.00 20.00           C
ATOM    109  CG  GLU E  16      15.845  17.021  19.199  1.00 20.00           C
ATOM    110  CD  GLU E  16      14.463  17.488  19.734  1.00 20.00           C
ATOM    111  OE1 GLU E  16      13.537  16.659  19.856  1.00 20.00           O
ATOM    112  OE2 GLU E  16      14.372  18.663  20.092  1.00 20.00           O
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Combined individual XYZ, ADP, SS restraints and rigid body refinement.
  """
  pdb_answer = prefix+"_answer.pdb"
  open(pdb_answer, "w").write(pdb_str_answer)
  #
  pdb_poor = prefix+"_poor.pdb"
  fo = open(pdb_poor, "w").write(pdb_str_poor)
  #
  args = [
    pdb_answer,
    "high_res=2.0",
    "type=real",
    "label=F-obs",
    "r_free=0.1"]
  r = run_fmodel(args = args, prefix = prefix)
  #
  args = [
    "cdl=False",
    r.mtz,
    pdb_poor,
    "const_shrink_donor_acceptor=0.6",
    "xray_data.r_free_flags.ignore_r_free_flags=True",
    "strategy=individual_sites+individual_adp+rigid_body+individual_sites_real_space",
    "main.bulk_sol=false",
    "pdb_interpretation.secondary_structure.enabled=true",
    "secondary_structure.protein.remove_outliers=False",
    "main.number_of_mac=4",
    "main.max_number_of_iterations=50",
    "rigid_body.min_number_of_reflections=50",
    "ramachandran_restraints=true",
    "reference_model.enabled=True",
    "reference_model.file=%s"%(prefix+"_answer.pdb"),
    "main.target=ls",
    "wxu_scale=0.25"]
  r = run_phenix_refine(args = args, prefix = prefix)
  #
  xrs_answer = iotbx.pdb.input(source_info=None,
    lines=pdb_str_answer).xray_structure_simple()
  xrs_poor = iotbx.pdb.input(source_info=None,
    lines=pdb_str_poor).xray_structure_simple()
  xrs_refined = iotbx.pdb.input(
    file_name=r.pdb).xray_structure_simple()
  d1 = flex.max(xrs_answer.distances(xrs_poor))
  d2 = flex.max(xrs_answer.distances(xrs_refined))
  assert d1 > 2.5, "Expecting %f > 2.5" % d1
  assert d2 < 0.1, "Expecting %f < 0.1" % d2
  r.check_final_r(r_work=0.0171, r_free=0.0171, eps=0.015)
  r.check_start_r(r_work=0.4410, r_free=0.4410, eps=0.015)

if (__name__ == "__main__"):
  run()
  print("OK")
