from __future__ import print_function
import time, os
import iotbx.pdb
from scitbx.array_family import flex
import libtbx.load_env
from phenix_regression.refinement import run_phenix_refine
from libtbx.test_utils import assert_lines_in_file, approx_equal

body = """
CRYST1   94.658   94.658   54.350  90.00  90.00 120.00 P 65
ATOM      1  N   GLY A   1      20.107 -29.161  35.731  1.00 60.27           N
ATOM      2  CA  GLY A   1      20.096 -28.697  34.361  1.00 61.41           C
ATOM      3  C   GLY A   1      21.395 -28.037  33.968  1.00 70.76           C
ATOM      4  O   GLY A   1      22.406 -28.211  34.645  1.00 79.76           O
ATOM      5  N   SER A   2      21.382 -27.273  32.881  1.00 66.81           N
ATOM      6  CA  SER A   2      22.602 -26.641  32.398  1.00 58.02           C
ATOM      7  C   SER A   2      22.310 -25.322  31.711  1.00 54.47           C
ATOM      8  O   SER A   2      21.183 -24.836  31.753  1.00 57.71           O
ATOM      9  CB  SER A   2      23.334 -27.571  31.436  1.00 57.35           C
ATOM     10  N   ILE A   3      23.320 -24.744  31.070  1.00 57.19           N
ATOM     11  CA  ILE A   3      23.143 -23.462  30.405  1.00 55.89           C
ATOM     12  C   ILE A   3      21.967 -23.518  29.454  1.00 61.33           C
ATOM     13  O   ILE A   3      21.078 -22.676  29.509  1.00 64.11           O
ATOM     14  CB  ILE A   3      24.400 -23.062  29.618  1.00 51.62           C
END
"""

pdb_str1 = """
REMARK   3
REMARK   3  TWINNING INFORMATION.
REMARK   3   FRACTION: 0.500
REMARK   3   OPERATOR: h,-h-k,-l
REMARK   3  ERROR ESTIMATES.
REMARK   3   COORDINATE ERROR (MAXIMUM-LIKELIHOOD BASED)     :     None
REMARK   3   PHASE ERROR (DEGREES, MAXIMUM-LIKELIHOOD BASED) : 29.39
REMARK   3
%s
"""%body

pdb_str2 = """
%s
"""%body

cif_str = """
data_tst_145_sc1
_cell.length_a                    94.658
_cell.length_b                    94.658
_cell.length_c                    54.350
_cell.angle_alpha                 90.000
_cell.angle_beta                  90.000
_cell.angle_gamma                 120.000
_cell.volume                      421739.996
_space_group.crystal_system       hexagonal
_space_group.IT_number            170
_space_group.name_H-M_alt         'P 65'
_space_group.name_Hall            ' P 65'
_symmetry.space_group_name_H-M    'P 65'
_symmetry.space_group_name_Hall   ' P 65'
_symmetry.Int_Tables_number       170
_pdbx_reflns_twin.operator        h,-h-k,-l
_pdbx_reflns_twin.fraction        0.4800
loop_
  _space_group_symop.id
  _space_group_symop.operation_xyz
   1 x,y,z
   2 x-y,x,z+5/6
   3 y,-x+y,z+1/6
   4 -y,x-y,z+2/3
   5 -x+y,-x,z+1/3
   6 -x,-y,z+1/2

loop_
  _atom_site.group_PDB
  _atom_site.id
  _atom_site.label_atom_id
  _atom_site.label_alt_id
  _atom_site.label_comp_id
  _atom_site.auth_asym_id
  _atom_site.auth_seq_id
  _atom_site.pdbx_PDB_ins_code
  _atom_site.Cartn_x
  _atom_site.Cartn_y
  _atom_site.Cartn_z
  _atom_site.occupancy
  _atom_site.B_iso_or_equiv
  _atom_site.type_symbol
  _atom_site.pdbx_formal_charge
  _atom_site.label_asym_id
  _atom_site.label_entity_id
  _atom_site.label_seq_id
  _atom_site.pdbx_PDB_model_num
   ATOM 1 N . GLY A 1 ? 20.10700 -29.16100 35.73100 1.000 60.27000 N ? A ? 1 1
   ATOM 2 CA . GLY A 1 ? 20.09600 -28.69700 34.36100 1.000 61.41000 C ? A ? 1 1
   ATOM 3 C . GLY A 1 ? 21.39500 -28.03700 33.96800 1.000 70.76000 C ? A ? 1 1
   ATOM 4 O . GLY A 1 ? 22.40600 -28.21100 34.64500 1.000 79.76000 O ? A ? 1 1
   ATOM 5 N . SER A 2 ? 21.38200 -27.27300 32.88100 1.000 66.81000 N ? A ? 2 1
   ATOM 6 CA . SER A 2 ? 22.60200 -26.64100 32.39800 1.000 58.02000 C ? A ? 2 1
   ATOM 7 C . SER A 2 ? 22.31000 -25.32200 31.71100 1.000 54.47000 C ? A ? 2 1
   ATOM 8 O . SER A 2 ? 21.18300 -24.83600 31.75300 1.000 57.71000 O ? A ? 2 1
   ATOM 9 CB . SER A 2 ? 23.33400 -27.57100 31.43600 1.000 57.35000 C ? A ? 2 1
   ATOM 10 N . ILE A 3 ? 23.32000 -24.74400 31.07000 1.000 57.19000 N ? A ? 3 1
   ATOM 11 CA . ILE A 3 ? 23.14300 -23.46200 30.40500 1.000 55.89000 C ? A ? 3 1
   ATOM 12 C . ILE A 3 ? 21.96700 -23.51800 29.45400 1.000 61.33000 C ? A ? 3 1
   ATOM 13 O . ILE A 3 ? 21.07800 -22.67600 29.50900 1.000 64.11000 O ? A ? 3 1
   ATOM 14 CB . ILE A 3 ? 24.40000 -23.06200 29.61800 1.000 51.62000 C ? A ? 3 1

"""

common_args = ["main.number_of_mac=0", "strategy=None"]

def get_data_file(prefix):
  return libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/data/%s.mtz"%prefix,
    test=os.path.isfile)

def test1(prefix):
  hkl = get_data_file(prefix)
  pdb = "%s.pdb"%prefix
  with open(pdb, "w") as fo:
    fo.write(pdb_str1)
  r = run_phenix_refine(args = [pdb, hkl]+common_args, prefix = prefix+"_sc1")
  found=False
  with open(r.pdb, "r") as fo:
    for l in fo.readlines():
      if(l.count("REMARK   3   OPERATOR: h,-h-k,-l")):
        found=True
        break
  assert found
  assert_lines_in_file(r.cif, """ _pdbx_reflns_twin.operator        h,-h-k,-l
    _pdbx_reflns_twin.fraction        0.4800 """ )
  inp = iotbx.pdb.input(r.cif)
  r = inp.extract_f_model_core_constants()
  assert approx_equal(r.twin_fraction, 0.48, eps=0.01)
  assert r.twin_law == "h,-h-k,-l"

def test2(prefix):
  hkl = get_data_file(prefix)
  pdb = "%s.pdb"%prefix
  with open(pdb, "w") as fo:
    fo.write(pdb_str1)
  r = run_phenix_refine(
    args = [pdb, hkl, "xray_data.twin_law=h,k,l"]+common_args, prefix = prefix+"_sc2")
  found=False
  with open(r.pdb, "r") as fo:
    for l in fo.readlines():
      if(l.count("REMARK   3   OPERATOR: h,k,l")):
        found=True
        break
  assert found
  assert_lines_in_file(r.cif, """ _pdbx_reflns_twin.operator        h,k,l""")
  inp = iotbx.pdb.input(r.cif)
  r = inp.extract_f_model_core_constants()
  assert r.twin_law == "h,k,l"

def test3(prefix):
  hkl = get_data_file(prefix)
  pdb = "%s.pdb"%prefix
  with open(pdb, "w") as fo:
    fo.write(pdb_str2)
  r = run_phenix_refine(
    args = [pdb, hkl, "xray_data.twin_law=h,-h-k,-l"]+common_args, prefix = prefix+"_sc3")
  found=False
  with open(r.pdb, "r") as fo:
    for l in fo.readlines():
      if(l.count("REMARK   3   OPERATOR: h,-h-k,-l")):
        found=True
        break
  assert found
  assert_lines_in_file(r.cif, """_pdbx_reflns_twin.operator        h,-h-k,-l""")
  inp = iotbx.pdb.input(r.cif)
  r = inp.extract_f_model_core_constants()
  assert r.twin_law == "h,-h-k,-l"

def test4(prefix):
  hkl = get_data_file(prefix)
  cif = "%s.cif"%prefix
  with open(cif, "w") as fo:
    fo.write(cif_str)
  r = run_phenix_refine(args = [cif, hkl]+common_args, prefix = prefix+"_sc4")
  found=False
  with open(r.pdb, "r") as fo:
    for l in fo.readlines():
      if(l.count("REMARK   3   OPERATOR: h,-h-k,-l")):
        found=True
        break
  assert found
  assert_lines_in_file(r.cif, """ _pdbx_reflns_twin.operator        h,-h-k,-l""" )
  inp = iotbx.pdb.input(r.cif)
  r = inp.extract_f_model_core_constants()
  assert r.twin_law == "h,-h-k,-l"

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Twin law usage model file header vs CL.
  """
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/data/%s.mtz"%prefix,
    test=os.path.isfile)
  #
  # SCENARIO 1: model file header contains twin law info, all defaults
  #
  test1(prefix)
  #
  # SCENARIO 2: model file header contains twin law info, twin_law is specified
  #             in the command line
  #
  test2(prefix)
 #
 # SCENARIO 3: model file header does not contain twin law info, twin_law is
 #             specified in the command line
 #
  test3(prefix)
 #
 # SCENARIO 4: Same as #1 (model contains twin law info), but model in mmCIF format.
 #
  test4(prefix)

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
