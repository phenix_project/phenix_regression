from __future__ import print_function
import time, os
from libtbx import easy_run
import libtbx.load_env

def run(prefix="tst_00"):
  """
  Make sure correct_special_position_tolerance works.
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/correct_special_position_tolerance/%s.pdb"%prefix,
    test=os.path.isfile)
  mtz = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/correct_special_position_tolerance/%s.mtz"%prefix,
    test=os.path.isfile)
  #
  results = []
  for i, csp in enumerate([0.3, 500]):
    cmd = " ".join([
      "phenix.refine",
      "%s"%pdb,
      "%s"%mtz,
      "strategy=individual_sites",
      "main.number_of_macro_cycles=1",
      "allow_polymer_cross_special_position=True",
      "pdb_interpretation.clash_guard.nonbonded_distance_threshold=None",
      "correct_special_position_tolerance=%s"%str(csp),
      "output.prefix=%s_%s"%(prefix,str(csp))])
    print(cmd)
    r = easy_run.go(cmd).stdout_lines
    if(i==0): # expected crash
      assert "Excessive special position correction:" in r
    else:
      assert "=========================== phenix.refine: finished ===========================" in r,r

if (__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK time =%8.3f"%(time.time() - t0))
