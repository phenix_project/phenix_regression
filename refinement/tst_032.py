from __future__ import division
from __future__ import print_function
import libtbx.load_env
import os
from phenix_regression.refinement import run_phenix_refine

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Atom on special position. And something else.
  """
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/mmtbx/one_special_position.pdb",
    test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/mmtbx/one_special_position.mtz",
    test=os.path.isfile)
  cif = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/mmtbx/ALA_renamed.cif",
    test=os.path.isfile)
  args = [
    pdb,hkl,cif,
    "xray_data.r_free_flags.test_flag_value=0",
    "main.number_of_macro_cycles=2",
    "allow_polymer_cross_special_position=True",
    "main.max_number_of_iterations=2",
    "xray_data.high_resolution=4"]
  r = run_phenix_refine(args = args, prefix = prefix)

if (__name__ == "__main__"):
  run()
  print("OK")
