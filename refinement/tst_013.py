from __future__ import division
from __future__ import print_function
from iotbx import pdb
from cctbx.array_family import flex
from libtbx.test_utils import approx_equal
import libtbx.load_env
import os
import random
from phenix_regression.refinement import run_phenix_refine

random.seed(0)
flex.set_random_seed(0)

def calculate_fobs(file_name, resolution = 1.0, algorithm = "direct"):
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/enk_gbr.pdb", test=os.path.isfile)
  xray_structure = pdb.input(file_name=pdb_file).xray_structure_simple()
  xray_structure.scattering_type_registry(table = "wk1995")
  f_calc = xray_structure.structure_factors(
    d_min          = resolution,
    anomalous_flag = False,
    cos_sin_table  = False,
    algorithm      = algorithm).f_calc()
  f_calc = abs(f_calc.structure_factors_from_scatterers(
                                     xray_structure = xray_structure).f_calc())
  r_free_flags = f_calc.generate_r_free_flags(fraction = 0.01,
                                              max_free = 200000)
  mtz_dataset = f_calc.as_mtz_dataset(column_root_label = "FOBS")
  mtz_dataset.add_miller_array(miller_array      = r_free_flags,
                               column_root_label = "TEST")
  sigmas = r_free_flags.array(data = flex.double(r_free_flags.data().size(),1))
  mtz_dataset.add_miller_array(miller_array      = sigmas,
                               column_root_label = "SIGMA")
  mtz_object = mtz_dataset.mtz_object()
  mtz_object.write(file_name = file_name)

def run(prefix = os.path.basename(__file__).replace(".py","")):
  pdb_in = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/enk_gbr_e.pdb", test=os.path.isfile)
  mtz_in = "%s.mtz"%prefix
  calculate_fobs(file_name=mtz_in)
  args = [
    pdb_in,
    mtz_in,
    "main.number_of_macro_cycles=3",
    "strategy=group_adp",
    "fake_f_obs.structure_factors_accuracy.cos_sin_table=false",
    "main.target=ls",
    "group_b_iso.use_restraints=False",
    "group_b_iso.run_finite_differences_test=true",
    "structure_factors_and_gradients_accuracy.cos_sin_table=false",
    "group_adp_refinement_mode=one_adp_group_per_residue",
    "fake_f_obs.structure_factors_accuracy.algorithm=direct",
    "structure_factors_and_gradients_accuracy.algorithm=direct",
    "main.scattering_table=wk1995",
    "fake_f_obs.scattering_table=wk1995",
    "data_manager.miller_array.labels.name=FOBS",
    "xray_data.r_free_flags.ignore_r_free_flags=True",
    "main.bulk_solvent_and_scale=false"]
  r1 = run_phenix_refine(args = args, prefix = prefix)
  args = [
    pdb_in,
    mtz_in,
    "main.number_of_macro_cycles=3",
    "strategy=group_adp",
    "fake_f_obs.structure_factors_accuracy.cos_sin_table=false",
    "main.target=ls",
    "group_b_iso.use_restraints=False",
    "group_b_iso.run_finite_differences_test=true",
    "group_adp_refinement_mode=group_selection",
    "structure_factors_and_gradients_accuracy.cos_sin_table=false",
    "structure_factors_and_gradients_accuracy.algorithm=direct",
    "fake_f_obs.structure_factors_accuracy.algorithm=direct",
    "main.scattering_table=wk1995",
    "fake_f_obs.scattering_table=wk1995",
    "data_manager.miller_array.labels.name=FOBS",
    "main.bulk_solvent_and_scale=false",
    "xray_data.r_free_flags.ignore_r_free_flags=True",
    "group_adp_refinement_mode=group_selection",
    'adp.group="chain A"',
    'adp.group="chain B"',
    'adp.group="chain C"',
    'adp.group="chain D"']
  r2 = run_phenix_refine(args = args, prefix = prefix)
  assert approx_equal(r1.r_work_start, r2.r_work_start)
  assert approx_equal(r1.r_work_final, r2.r_work_final)
  assert r1.r_work_start > 0.5
  assert r1.r_work_final < 0.01

if (__name__ == "__main__"):
  run()
  print("OK")
