from __future__ import division
from __future__ import print_function
from libtbx import easy_run
import libtbx.load_env
import os

def exercise():
  cif_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/ignore_f000/2i7s_sf_part.cif",
    test=os.path.isfile)
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/ignore_f000/2i7s_part.pdb",
    test=os.path.isfile)
  cmd = " ".join([
    "phenix.refine",
    "%s"%pdb_file,
    "%s"%cif_file,
    "main.number_of_mac=1",
    "main.update_f_part1=False",
    "strategy=none",
    "--quiet",
    "--overwrite"])
  assert not easy_run.call(cmd)
  #
  cmd = " ".join([
    "phenix.model_vs_data",
    "%s"%pdb_file,
    "%s"%cif_file])
  assert not easy_run.call(cmd)

if (__name__ == "__main__") :
  exercise()
  print("OK")
