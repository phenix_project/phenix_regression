from cctbx.array_family import flex
import iotbx.pdb
from libtbx.utils import null_out
import mmtbx.utils
from libtbx import easy_run
from iotbx import mtz
import libtbx
import os
from iotbx import extract_xtal_data

def wmtzf(fn, fo, fl):
  mtz_dataset = fo.as_mtz_dataset(column_root_label="F-obs")
  mtz_dataset.add_miller_array(
    miller_array      = fl,
    column_root_label = "R-free-flags")
  mtz_object = mtz_dataset.mtz_object()
  mtz_object.write(file_name = fn)

def exercise(prefix="tst_apply_scale_k1_to_f_obs"):
  """
  Make sure apply_scale_k1_to_f_obs is run as part of update_all_scales.
  """
  path = "phenix_regression/refinement/apply_scale_k1_to_f_obs/"
  pdb = libtbx.env.find_in_repositories(
        relative_path=path+"1yjp.pdb",
        test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
        relative_path=path+"1yjp.mtz", \
        test=os.path.isfile)
  inputs = mmtbx.utils.process_command_line_args(args = [pdb, hkl])
  df = extract_xtal_data.run(
    reflection_file_server  = inputs.get_reflection_file_server())
  f_obs = df.f_obs.average_bijvoet_mates()
  f_obs, r_free_flags = f_obs.common_sets(df.r_free_flags)
  #
  map_mean = flex.double()
  cntr=0
  for sc in [10000., 1., 1./10000.]:
    for o1 in ["apply_back_trace=true","apply_back_trace=false"]:
      for o2 in ["strategy=individual_adp", "strategy=none"]:
        cntr+=1
        fo = f_obs.deep_copy()
        fl = r_free_flags.deep_copy()
        fo = fo.deep_copy().array(data = fo.data()*sc)
        wmtzf(fn=prefix+str(cntr)+".mtz", fo=fo, fl=fl)
        cmd = " ".join([
          "phenix.refine",
          pdb,
          prefix+str(cntr)+".mtz",
          o1,
          o2,
          "output.prefix=%s"%(prefix+str(cntr)),
          "main.number_of_mac=1",
          "main.max_number_of_iterations=0",
          "structure_factors_and_gradients_accuracy.algorithm=direct",
          "--quiet",
          "--overwrite",
        ])
        #print cmd
        assert not easy_run.call(cmd)
        mas = mtz.object(prefix+str(cntr)+"_001.mtz").as_miller_arrays()
        for ma in mas:
          if(ma.info().label_string()=="FOFCWT,PHFOFCWT"):
            map_mean.append(flex.mean(abs(ma).data()))
  assert map_mean.size() == 12
  assert flex.min(map_mean) > 4.0
  assert flex.max(map_mean) < 6.5

if (__name__ == "__main__"):
  exercise()
