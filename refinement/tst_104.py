from __future__ import print_function
import time, os
from libtbx.test_utils import approx_equal
from libtbx import easy_run
import iotbx.pdb
import mmtbx.utils
from scitbx.array_family import flex
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel

pdb_str = """
CRYST1   10.707   11.101   13.552  90.00  90.00  90.00 P 1
ATOM      1  N  AALA A   9       3.452   6.807   3.508  0.10  9.33      A    N
ATOM      2  CA AALA A   9       4.572   6.204   4.211  0.10  9.82      A    C
ATOM      3  C  AALA A   9       4.165   5.990   5.664  0.10 10.34      A    C
ATOM      4  O  AALA A   9       3.000   6.165   6.021  0.10 10.96      A    O
ATOM      5  CB AALA A   9       5.792   7.098   4.116  0.10 10.31      A    C
ATOM      6  H  AALA A   9       3.466   7.667   3.487  0.10  8.78      A    H
ATOM      7  HA AALA A   9       4.802   5.351   3.810  0.10  9.23      A    H
ATOM      8  HB1AALA A   9       6.533   6.686   4.588  0.10  9.91      A    H
ATOM      9  HB2AALA A   9       6.031   7.221   3.184  0.10  9.91      A    H
ATOM     10  HB3AALA A   9       5.594   7.960   4.515  0.10  9.91      A    H
ATOM     11  N  BALA A   9       3.348   6.697   3.518  0.20  8.28      A    N
ATOM     12  CA BALA A   9       4.461   6.052   4.195  0.20  9.14      A    C
ATOM     13  C  BALA A   9       4.138   5.964   5.683  0.20  9.84      A    C
ATOM     14  O  BALA A   9       3.003   6.215   6.089  0.20 10.68      A    O
ATOM     15  CB BALA A   9       5.726   6.829   3.952  0.20  9.20      A    C
ATOM     16  H  BALA A   9       3.422   7.551   3.454  0.20  8.78      A    H
ATOM     17  HA BALA A   9       4.597   5.156   3.849  0.20  9.23      A    H
ATOM     18  HB1BALA A   9       6.465   6.395   4.406  0.20  9.91      A    H
ATOM     19  HB2BALA A   9       5.907   6.863   3.000  0.20  9.91      A    H
ATOM     20  HB3BALA A   9       5.623   7.731   4.294  0.20  9.91      A    H
ATOM     21  N  CALA A   9       3.608   6.763   3.402  0.30  8.32      A    N
ATOM     22  CA CALA A   9       4.617   6.060   4.177  0.30  9.56      A    C
ATOM     23  C  CALA A   9       4.219   6.081   5.651  0.30 10.15      A    C
ATOM     24  O  CALA A   9       3.126   6.528   6.006  0.30 10.64      A    O
ATOM     25  CB CALA A   9       5.981   6.684   3.973  0.30 10.39      A    C
ATOM     26  H  CALA A   9       3.801   7.579   3.210  0.30  8.78      A    H
ATOM     27  HA CALA A   9       4.671   5.139   3.876  0.30  9.23      A    H
ATOM     28  HB1CALA A   9       6.639   6.202   4.497  0.30  9.91      A    H
ATOM     29  HB2CALA A   9       6.220   6.639   3.034  0.30  9.91      A    H
ATOM     30  HB3CALA A   9       5.959   7.611   4.257  0.30  9.91      A    H
ATOM     31  N  DALA A   9       3.518   6.930   3.530  0.40  8.78      A    N
ATOM     32  CA DALA A   9       4.639   6.333   4.232  0.40  9.23      A    C
ATOM     33  C  DALA A   9       4.203   6.093   5.674  0.40 10.10      A    C
ATOM     34  O  DALA A   9       3.051   6.346   6.031  0.40 10.72      A    O
ATOM     35  CB DALA A   9       5.837   7.255   4.177  0.40  9.91      A    C
ATOM     36  H  DALA A   9       3.490   7.789   3.568  0.40  8.78      A    H
ATOM     37  HA DALA A   9       4.898   5.494   3.819  0.40  9.23      A    H
ATOM     38  HB1DALA A   9       6.581   6.848   4.648  0.40  9.91      A    H
ATOM     39  HB2DALA A   9       6.086   7.408   3.252  0.40  9.91      A    H
ATOM     40  HB3DALA A   9       5.614   8.101   4.595  0.40  9.91      A    H
REMARK
ATOM     41  N   VAL A  10       5.119   5.606   6.502  1.00 11.13      A    N
ATOM     42  CA  VAL A  10       4.846   5.470   7.925  1.00 12.50      A    C
ATOM     43  C   VAL A  10       4.347   6.801   8.520  1.00 11.26      A    C
ATOM     44  O   VAL A  10       4.763   7.871   8.095  1.00 11.53      A    O
ATOM     45  HA  VAL A  10       4.118   4.835   8.017  1.00 12.50      A    H
ATOM     46  CB AVAL A  10       5.994   4.806   8.722  0.25 14.17      A    C
ATOM     47  CG1AVAL A  10       6.640   3.699   7.889  0.25 14.17      A    C
ATOM     48  CG2AVAL A  10       7.005   5.815   9.197  0.25 15.20      A    C
ATOM     49  H  AVAL A  10       5.926   5.421   6.269  0.10 11.13      A    H
ATOM     50  HB AVAL A  10       5.616   4.404   9.520  0.25 14.91      A    H
ATOM     51 HG11AVAL A  10       7.358   3.289   8.396  0.25 16.29      A    H
ATOM     52 HG12AVAL A  10       5.975   3.028   7.671  0.25 16.29      A    H
ATOM     53 HG13AVAL A  10       6.998   4.077   7.070  0.25 16.29      A    H
ATOM     54 HG21AVAL A  10       7.707   5.363   9.691  0.25 15.63      A    H
ATOM     55 HG22AVAL A  10       7.391   6.271   8.433  0.25 15.63      A    H
ATOM     56 HG23AVAL A  10       6.570   6.462   9.774  0.25 15.63      A    H
ATOM     57  CB BVAL A  10       6.135   4.987   8.645  0.75 14.91      A    C
ATOM     58  CG1BVAL A  10       6.081   5.228  10.144  0.75 16.28      A    C
ATOM     59  CG2BVAL A  10       6.351   3.507   8.360  0.75 15.63      A    C
ATOM     60  H  BVAL A  10       5.928   5.441   6.263  0.75 11.13      A    H
ATOM     61  HB BVAL A  10       6.879   5.504   8.299  0.75 14.91      A    H
ATOM     62 HG11BVAL A  10       6.902   4.913  10.552  0.75 16.29      A    H
ATOM     63 HG12BVAL A  10       5.978   6.177  10.316  0.75 16.29      A    H
ATOM     64 HG13BVAL A  10       5.328   4.748  10.522  0.75 16.29      A    H
ATOM     65 HG21BVAL A  10       7.156   3.205   8.809  0.75 15.63      A    H
ATOM     66 HG22BVAL A  10       5.590   3.000   8.685  0.75 15.63      A    H
ATOM     67 HG23BVAL A  10       6.445   3.372   7.404  0.75 15.63      A    H
ATOM     68  H  CVAL A  10       5.907   5.353   6.270  0.30 11.13      A    H
ATOM     69  H  DVAL A  10       5.903   5.349   6.260  0.40 11.13      A    H
TER
END
"""

pdb_str_poor = """
CRYST1   10.707   11.101   13.552  90.00  90.00  90.00 P 1
SCALE1      0.093397  0.000000  0.000000        0.00000
SCALE2      0.000000  0.090082  0.000000        0.00000
SCALE3      0.000000  0.000000  0.073790        0.00000
ATOM      1  N  AALA A   9       3.452   6.807   3.508  0.96  9.33      A    N
ATOM      2  CA AALA A   9       4.572   6.204   4.211  0.48  9.82      A    C
ATOM      3  C  AALA A   9       4.165   5.990   5.664  0.32 10.34      A    C
ATOM      4  O  AALA A   9       3.000   6.165   6.021  0.53 10.96      A    O
ATOM      5  CB AALA A   9       5.792   7.098   4.116  0.56 10.31      A    C
ATOM      6  H  AALA A   9       3.466   7.667   3.487  0.21  8.78      A    H
ATOM      7  HA AALA A   9       4.802   5.351   3.810  0.44  9.23      A    H
ATOM      8  HB1AALA A   9       6.533   6.686   4.588  0.83  9.91      A    H
ATOM      9  HB2AALA A   9       6.031   7.221   3.184  0.23  9.91      A    H
ATOM     10  HB3AALA A   9       5.594   7.960   4.515  0.63  9.91      A    H
ATOM     11  N  BALA A   9       3.348   6.697   3.518  0.67  8.28      A    N
ATOM     12  CA BALA A   9       4.461   6.052   4.195  0.70  9.14      A    C
ATOM     13  C  BALA A   9       4.138   5.964   5.683  0.70  9.84      A    C
ATOM     14  O  BALA A   9       3.003   6.215   6.089  0.28 10.68      A    O
ATOM     15  CB BALA A   9       5.726   6.829   3.952  0.21  9.20      A    C
ATOM     16  H  BALA A   9       3.422   7.551   3.454  0.04  8.78      A    H
ATOM     17  HA BALA A   9       4.597   5.156   3.849  0.97  9.23      A    H
ATOM     18  HB1BALA A   9       6.465   6.395   4.406  0.32  9.91      A    H
ATOM     19  HB2BALA A   9       5.907   6.863   3.000  0.21  9.91      A    H
ATOM     20  HB3BALA A   9       5.623   7.731   4.294  0.96  9.91      A    H
ATOM     21  N  CALA A   9       3.608   6.763   3.402  0.23  8.32      A    N
ATOM     22  CA CALA A   9       4.617   6.060   4.177  0.03  9.56      A    C
ATOM     23  C  CALA A   9       4.219   6.081   5.651  0.92 10.15      A    C
ATOM     24  O  CALA A   9       3.126   6.528   6.006  0.15 10.64      A    O
ATOM     25  CB CALA A   9       5.981   6.684   3.973  0.24 10.39      A    C
ATOM     26  H  CALA A   9       3.801   7.579   3.210  0.81  8.78      A    H
ATOM     27  HA CALA A   9       4.671   5.139   3.876  0.81  9.23      A    H
ATOM     28  HB1CALA A   9       6.639   6.202   4.497  0.58  9.91      A    H
ATOM     29  HB2CALA A   9       6.220   6.639   3.034  0.57  9.91      A    H
ATOM     30  HB3CALA A   9       5.959   7.611   4.257  0.74  9.91      A    H
ATOM     31  N  DALA A   9       3.518   6.930   3.530  0.11  8.78      A    N
ATOM     32  CA DALA A   9       4.639   6.333   4.232  0.81  9.23      A    C
ATOM     33  C  DALA A   9       4.203   6.093   5.674  0.38 10.10      A    C
ATOM     34  O  DALA A   9       3.051   6.346   6.031  0.38 10.72      A    O
ATOM     35  CB DALA A   9       5.837   7.255   4.177  0.30  9.91      A    C
ATOM     36  H  DALA A   9       3.490   7.789   3.568  0.84  8.78      A    H
ATOM     37  HA DALA A   9       4.898   5.494   3.819  0.61  9.23      A    H
ATOM     38  HB1DALA A   9       6.581   6.848   4.648  0.56  9.91      A    H
ATOM     39  HB2DALA A   9       6.086   7.408   3.252  0.67  9.91      A    H
ATOM     40  HB3DALA A   9       5.614   8.101   4.595  0.23  9.91      A    H
ATOM     41  N   VAL A  10       5.119   5.606   6.502  0.48 11.13      A    N
ATOM     42  CA  VAL A  10       4.846   5.470   7.925  0.89 12.50      A    C
ATOM     43  C   VAL A  10       4.347   6.801   8.520  0.91 11.26      A    C
ATOM     44  O   VAL A  10       4.763   7.871   8.095  0.25 11.53      A    O
ATOM     45  HA  VAL A  10       4.118   4.835   8.017  0.15 12.50      A    H
ATOM     46  CB AVAL A  10       5.994   4.806   8.722  0.51 14.17      A    C
ATOM     47  CG1AVAL A  10       6.640   3.699   7.889  0.82 14.17      A    C
ATOM     48  CG2AVAL A  10       7.005   5.815   9.197  0.24 15.20      A    C
ATOM     49  H  AVAL A  10       5.926   5.421   6.269  0.58 11.13      A    H
ATOM     50  HB AVAL A  10       5.616   4.404   9.520  0.47 14.91      A    H
ATOM     51 HG11AVAL A  10       7.358   3.289   8.396  0.40 16.29      A    H
ATOM     52 HG12AVAL A  10       5.975   3.028   7.671  0.88 16.29      A    H
ATOM     53 HG13AVAL A  10       6.998   4.077   7.070  0.81 16.29      A    H
ATOM     54 HG21AVAL A  10       7.707   5.363   9.691  0.84 15.63      A    H
ATOM     55 HG22AVAL A  10       7.391   6.271   8.433  0.46 15.63      A    H
ATOM     56 HG23AVAL A  10       6.570   6.462   9.774  0.41 15.63      A    H
ATOM     57  CB BVAL A  10       6.135   4.987   8.645  0.13 14.91      A    C
ATOM     58  CG1BVAL A  10       6.081   5.228  10.144  0.91 16.28      A    C
ATOM     59  CG2BVAL A  10       6.351   3.507   8.360  0.18 15.63      A    C
ATOM     60  H  BVAL A  10       5.928   5.441   6.263  0.73 11.13      A    H
ATOM     61  HB BVAL A  10       6.879   5.504   8.299  0.17 14.91      A    H
ATOM     62 HG11BVAL A  10       6.902   4.913  10.552  0.12 16.29      A    H
ATOM     63 HG12BVAL A  10       5.978   6.177  10.316  0.30 16.29      A    H
ATOM     64 HG13BVAL A  10       5.328   4.748  10.522  0.16 16.29      A    H
ATOM     65 HG21BVAL A  10       7.156   3.205   8.809  0.86 15.63      A    H
ATOM     66 HG22BVAL A  10       5.590   3.000   8.685  0.95 15.63      A    H
ATOM     67 HG23BVAL A  10       6.445   3.372   7.404  0.12 15.63      A    H
ATOM     68  H  CVAL A  10       5.907   5.353   6.270  0.05 11.13      A    H
ATOM     69  H  DVAL A  10       5.903   5.349   6.260  0.59 11.13      A    H
TER
END
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Occupancies.
  # make sure C and D conformers of H in residue 10 match mates in residue 9
  """
  pdb_file_answer = "%s.pdb"%prefix
  of = open(pdb_file_answer, "w")
  print(pdb_str, file=of)
  of.close()
  pdb_inp = iotbx.pdb.input(source_info=None, lines=pdb_str)
  #
  pdb_file_poor = "%s_poor.pdb"%prefix
  of = open(pdb_file_poor, "w")
  print(pdb_str_poor, file=of)
  of.close()
  #
  args = [
    "%s.pdb"%prefix,
    "scattering_table=wk1995",
    "algorithm=direct",
    "high_res=1.0",
    "type=real",
    "r_free=0.1",
    "label=f-obs"]
  r0 = run_fmodel(args = args, prefix = prefix)
  #
  args = [
    pdb_file_poor,
    r0.mtz,
    "main.bulk_solvent_and_scale=false",
    "structure_factors_and_gradients_accuracy.algorithm=direct",
    "strategy=occupancies",
    "main.target=ls",
    "main.scattering_table=wk1995",
    "xray_data.r_free_flags.ignore_r_free_flags=true",
    "main.number_of_mac=2",
    ]
  r = run_phenix_refine(args = args, prefix = prefix)
  #
  pdb_inp = iotbx.pdb.input(file_name=r.pdb)
  xrs = pdb_inp.xray_structure_simple()
  o = xrs.scatterers().extract_occupancies()
  scs = pdb_inp.construct_hierarchy().atom_selection_cache().selection
  # check 1
  selA_9 = o.select(scs(string = "resseq 9 and altloc A"))
  selB_9 = o.select(scs(string = "resseq 9 and altloc B"))
  selC_9 = o.select(scs(string = "resseq 9 and altloc C"))
  selD_9 = o.select(scs(string = "resseq 9 and altloc D"))
  for sel in [selA_9, selB_9, selC_9, selD_9]:
    for si in sel:
      for sj in sel:
        assert approx_equal(round(si,2), round(sj,2), 1.e-2)
  assert approx_equal(selA_9[0]+selB_9[0]+selC_9[0]+selD_9[0], 1, 0.011)
  # check 2
  selA_10 = o.select(scs(string = "resseq 10 and altloc A and name H"))
  selB_10 = o.select(scs(string = "resseq 10 and altloc B and name H"))
  selC_10 = o.select(scs(string = "resseq 10 and altloc C and name H"))
  selD_10 = o.select(scs(string = "resseq 10 and altloc D and name H"))
  assert selA_10[0]==selA_9[0]
  assert selB_10[0]==selB_9[0]
  assert selC_10[0]==selC_9[0]
  assert selD_10[0]==selD_9[0]
  # check 3
  selA_10 = o.select(scs(string = "resseq 10 and altloc A and not name H"))
  selB_10 = o.select(scs(string = "resseq 10 and altloc B and not name H"))
  for sel in [selA_10, selB_10]:
    assert sel.all_eq(sel[0])
  assert approx_equal(selA_10[0]+selB_10[0], 1, 0.011)

if(__name__ == "__main__"):
  t0 = time.time()
  run()
  print("OK")
  print("Time: %8.3f"%(time.time()-t0))
