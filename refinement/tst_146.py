from __future__ import print_function
import time, os
import iotbx.pdb
from scitbx.array_family import flex
import libtbx.load_env
from phenix_regression.refinement import run_phenix_refine
from phenix_regression.refinement import run_fmodel

pdb_str = """
CRYST1   18.239   18.239   17.281  90.00  90.00  90.00 P 1
ATOM      1  N   MET A   0      -8.750  11.227  21.276  1.00 56.40           N
ATOM      2  CA  MET A   0      -8.435  11.761  19.919  1.00 56.44           C
ATOM      3  C   MET A   0      -6.957  11.581  19.574  1.00 54.59           C
ATOM      4  O   MET A   0      -6.566  11.720  18.414  1.00 55.86           O
ATOM      5  CB  MET A   0      -8.805  13.249  19.838  1.00 59.35           C
ATOM      6  CG  MET A   0      -8.679  14.010  21.158  1.00 61.96           C
ATOM      7  SD  MET A   0     -10.051  15.161  21.447  1.00 66.11           S
ATOM      8  CE  MET A   0      -9.658  15.773  23.094  1.00 63.82           C
END
"""

phil_str = """
refinement {
  electron_density_maps {
   apply_default_maps = False
    map_coefficients {
      map_type = 5mFo-4DFc
      format = *mtz phs
      mtz_label_amplitudes = 5FO4FCWT
      mtz_label_phases = PH5FO4FCWT
    }
  }
}
"""

def run(prefix = os.path.basename(__file__).replace(".py","")):
  """
  Make sure supplied map parameters REPLACE default ones and not being ADDED to
  default parameters.
  """
  #
  pdb = "%s.pdb"%prefix
  with open(pdb, "w") as fo:
    fo.write(pdb_str)
  #
  phil = "%s.eff"%prefix
  with open(phil, "w") as fo:
    fo.write(phil_str)
  #
  args = [
    pdb,
    "high_resolution=6",
    "type=real r_free=0.3"]
  r = run_fmodel(args = args, prefix = prefix)
  #
  args = [
    pdb,
    r.mtz,
    phil,
    "main.number_of_mac=0",
    ]
  r = run_phenix_refine(args = args, prefix = prefix)
  #
  c1, c2, c3 = 0,0,0
  with open(r.eff, "r") as fo:
    for l in fo.readlines():
      if("    map_coefficients {" in l): c1+=1
      if('      mtz_label_amplitudes = "5FO4FCWT"' in l): c2+=1
      if('      mtz_label_phases = "PH5FO4FCWT"' in l): c3+=1
  assert c1 ==1 and c2 == 1 and c3 == 1


if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
