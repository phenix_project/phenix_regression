from __future__ import print_function
from libtbx import easy_run
import sys

def run(args):
  assert len(args) == 0
  pids_to_kill = []
  for line in easy_run.fully_buffered(
                "ps -u $USER").raise_if_errors().stdout_lines:
    flds = line.split()
    print(flds)
    if (flds[3].startswith(("all_tests_", "tst_phenix_refi"))):
      pids_to_kill.append(flds[0])
  for pid in pids_to_kill:
    cmd = "kill %s" % pid
    print(cmd)
    sys.stdout.flush()
    easy_run.call(command=cmd)

if (__name__ == "__main__"):
  run(args=sys.argv[1:])
