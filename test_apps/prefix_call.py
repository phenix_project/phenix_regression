from __future__ import print_function
def run(args):
  assert len(args) == 1
  for line in open(args[0]).read().splitlines():
    print("call %s" % line)

if (__name__ == "__main__"):
  import sys
  run(args=sys.argv[1:])
