
"""
Data (3v14) are slightly pathological - interpolation of mean_obs in the
relative Wilson analysis will result in negative intensities which used to
crash the entire program.
"""

from __future__ import division
from __future__ import print_function
from mmtbx.scaling import xtriage
from mmtbx import scaling
from libtbx.test_utils import Exception_expected
from libtbx.utils import null_out, Sorry
import libtbx.load_env
import warnings
import os.path

def exercise () :
  mtz_file = libtbx.env.find_in_repositories(
    relative_path="phenix_dev/test_data/3v14.mtz",
    test=os.path.isfile)
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_dev/test_data/3v14.pdb",
    test=os.path.isfile)
  if (None in [mtz_file, pdb_file]) :
    warnings.warn("phenix_dev tree not found, skipping test", ImportWarning)
    return
  args = [mtz_file, pdb_file, "obs_labels='IOBS,SIGIOBS'"]
  try : # only 7 centrics
    result = xtriage.run(args=args, out=null_out())
  except Sorry :
    pass
  else :
    raise Exception_expected
  args = [mtz_file, pdb_file, "obs_labels='FOBS,SIGFOBS'"]
  result = xtriage.run(args=args, out=null_out())
  assert (result.relative_wilson is not None)
  out = scaling.printed_output(null_out())
  result.show(out)
  print("OK")

if (__name__ == "__main__") :
  exercise()
