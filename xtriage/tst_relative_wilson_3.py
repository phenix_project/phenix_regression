
"""
Data (2bf1) pass initial resolution filter but filtering in the relative
Wilson analysis removes everything past 4A, causing the analysis to abort.
"""

from __future__ import division
from __future__ import print_function
from mmtbx.scaling import xtriage
from mmtbx import scaling
from libtbx.utils import null_out
import libtbx.load_env
import warnings
import os.path

def exercise () :
  mtz_file = libtbx.env.find_in_repositories(
    relative_path="phenix_dev/test_data/2bf1.mtz",
    test=os.path.isfile)
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_dev/test_data/2bf1.pdb",
    test=os.path.isfile)
  if (None in [mtz_file, pdb_file]) :
    warnings.warn("phenix_dev tree not found, skipping test", ImportWarning)
    return
  args = [mtz_file, pdb_file, "obs_labels='FOBS,SIGFOBS'"]
  result = xtriage.run(args=args, out=null_out())
  assert (result.relative_wilson is None)
  out = scaling.printed_output(null_out())
  result.show(out)
  print("OK")

if (__name__ == "__main__") :
  exercise()
