
"""
Severely corrupted data - huge ice ring around 2.25A.  The relative Wilson
analysis will stop early because all resolution shells have Z > 6.
"""

from __future__ import division
from __future__ import print_function
from mmtbx.scaling import xtriage
from mmtbx import scaling
from libtbx.utils import null_out
import libtbx.load_env
import warnings
import os.path

def exercise () :
  mtz_file = libtbx.env.find_in_repositories(
    relative_path="phenix_dev/test_data/1p7c.mtz",
    test=os.path.isfile)
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_dev/test_data/1p7c.pdb",
    test=os.path.isfile)
  if (None in [mtz_file, pdb_file]) :
    warnings.warn("phenix_dev tree not found, skipping test", ImportWarning)
    return
  args = [mtz_file, pdb_file, "obs_labels='FOBS,SIGFOBS'"]
  result = xtriage.run(args=args, out=null_out())
  out = scaling.printed_output(null_out())
  result.show(out)
  print(len(out._warnings))
  print("OK")

if (__name__ == "__main__") :
  exercise()
