from __future__ import division
from __future__ import print_function
from six.moves import cStringIO as StringIO
from mmtbx.monomer_library import server
from mmtbx.monomer_library import pdb_interpretation
import sys

pdb_str_1dpl_cutted="""\
CRYST1   24.627   42.717   46.906  90.00  90.00  90.00 P 21 21 21    8
ATOM    184  P    DG A   9       9.587  13.026  19.037  1.00  6.28           P
ATOM    185  OP1  DG A   9       9.944  14.347  19.602  1.00  8.07           O
ATOM    186  OP2  DG A   9      10.654  12.085  18.639  1.00  8.27           O
ATOM    187  O5'  DG A   9       8.717  12.191  20.048  1.00  5.88           O
ATOM    188  C5'  DG A   9       7.723  12.833  20.854  1.00  5.45           C
ATOM    189  C4'  DG A   9       7.145  11.818  21.807  1.00  5.40           C
ATOM    190  O4'  DG A   9       6.435  10.777  21.087  1.00  5.77           O
ATOM    191  C3'  DG A   9       8.142  11.036  22.648  1.00  5.10           C
ATOM    192  O3'  DG A   9       8.612  11.838  23.723  1.00  5.90           O
ATOM    193  C2'  DG A   9       7.300   9.857  23.068  1.00  5.97           C
ATOM    194  C1'  DG A   9       6.619   9.536  21.805  1.00  5.97           C
ATOM    195  N9   DG A   9       7.390   8.643  20.931  1.00  5.97           N
ATOM    196  C8   DG A   9       8.074   8.881  19.775  1.00  6.62           C
ATOM    197  N7   DG A   9       8.647   7.820  19.249  1.00  6.57           N
ATOM    198  C5   DG A   9       8.308   6.806  20.141  1.00  6.22           C
ATOM    199  C6   DG A   9       8.620   5.431  20.136  1.00  6.03           C
ATOM    200  O6   DG A   9       9.297   4.803  19.296  1.00  7.21           O
ATOM    201  N1   DG A   9       8.101   4.773  21.247  1.00  6.10           N
ATOM    202  C2   DG A   9       7.365   5.351  22.260  1.00  6.24           C
ATOM    203  N2   DG A   9       6.948   4.569  23.241  1.00  7.88           N
ATOM    204  N3   DG A   9       7.051   6.652  22.257  1.00  6.53           N
ATOM    205  C4   DG A   9       7.539   7.295  21.184  1.00  5.69           C
ATOM    206  P    DC A  10      10.081  11.538  24.300  1.00  5.91           P
ATOM    207  OP1  DC A  10      10.273  12.645  25.291  1.00  7.27           O
ATOM    208  OP2  DC A  10      11.063  11.363  23.228  1.00  6.84           O
ATOM    209  O5'  DC A  10       9.953  10.128  25.026  1.00  5.75           O
ATOM    210  C5'  DC A  10       9.077   9.959  26.149  1.00  5.87           C
ATOM    211  C4'  DC A  10       9.188   8.549  26.672  1.00  5.56           C
ATOM    212  O4'  DC A  10       8.708   7.612  25.667  1.00  5.70           O
ATOM    213  C3'  DC A  10      10.580   8.059  27.007  1.00  5.27           C
ATOM    214  O3'  DC A  10      11.010   8.447  28.315  1.00  5.83           O
ATOM    215  C2'  DC A  10      10.422   6.549  26.893  1.00  5.34           C
ATOM    216  C1'  DC A  10       9.436   6.405  25.754  1.00  5.23           C
ATOM    217  N1   DC A  10      10.113   6.168  24.448  1.00  5.30           N
ATOM    218  C2   DC A  10      10.514   4.871  24.152  1.00  5.28           C
ATOM    219  O2   DC A  10      10.283   3.972  25.000  1.00  5.75           O
ATOM    220  N3   DC A  10      11.131   4.627  22.965  1.00  5.65           N
ATOM    221  C4   DC A  10      11.395   5.628  22.138  1.00  5.80           C
ATOM    222  N4   DC A  10      12.034   5.327  21.005  1.00  6.75           N
ATOM    223  C5   DC A  10      11.029   6.970  22.449  1.00  5.99           C
ATOM    224  C6   DC A  10      10.394   7.203  23.612  1.00  5.56           C
ATOM    226  O5'  DG B  11      12.424  -4.393  18.427  1.00 22.70           O
ATOM    227  C5'  DG B  11      12.380  -5.516  19.282  1.00 14.75           C
ATOM    228  C4'  DG B  11      11.969  -5.112  20.676  1.00 10.42           C
ATOM    229  O4'  DG B  11      12.972  -4.192  21.210  1.00 10.51           O
ATOM    230  C3'  DG B  11      10.649  -4.394  20.782  1.00  8.57           C
ATOM    231  O3'  DG B  11       9.618  -5.363  20.846  1.00  8.69           O
ATOM    232  C2'  DG B  11      10.822  -3.597  22.051  1.00  8.63           C
ATOM    233  C1'  DG B  11      12.236  -3.233  21.980  1.00  9.81           C
ATOM    234  N9   DG B  11      12.509  -1.902  21.305  1.00  8.66           N
ATOM    235  C8   DG B  11      13.175  -1.667  20.135  1.00  9.57           C
ATOM    236  N7   DG B  11      13.255  -0.407  19.824  1.00  9.04           N
ATOM    237  C5   DG B  11      12.613   0.235  20.869  1.00  7.63           C
ATOM    238  C6   DG B  11      12.388   1.612  21.119  1.00  7.05           C
ATOM    239  O6   DG B  11      12.723   2.590  20.419  1.00  7.81           O
ATOM    240  N1   DG B  11      11.715   1.819  22.317  1.00  6.27           N
ATOM    241  C2   DG B  11      11.264   0.828  23.159  1.00  6.05           C
ATOM    242  N2   DG B  11      10.611   1.219  24.248  1.00  5.85           N
ATOM    243  N3   DG B  11      11.483  -0.457  22.942  1.00  6.55           N
ATOM    244  C4   DG B  11      12.150  -0.687  21.797  1.00  6.84           C
ATOM    245  P    DC B  12       8.134  -5.009  20.350  1.00  8.13           P
ATOM    246  OP1  DC B  12       7.367  -6.252  20.459  1.00 10.02           O
ATOM    247  OP2  DC B  12       8.172  -4.307  19.052  1.00  9.79           O
ATOM    248  O5'  DC B  12       7.564  -3.912  21.389  1.00  8.18           O
ATOM    249  C5'  DC B  12       7.275  -4.296  22.719  1.00  8.00           C
ATOM    250  C4'  DC B  12       6.856  -3.057  23.487  1.00  8.01           C
ATOM    251  O4'  DC B  12       8.006  -2.146  23.615  1.00  7.35           O
ATOM    252  C3'  DC B  12       5.763  -2.208  22.890  1.00  7.04           C
ATOM    253  O3'  DC B  12       4.456  -2.800  23.100  1.00  9.82           O
ATOM    254  C2'  DC B  12       6.019  -0.916  23.630  1.00  6.50           C
ATOM    255  C1'  DC B  12       7.467  -0.808  23.608  1.00  7.35           C
ATOM    256  N1   DC B  12       8.040  -0.143  22.396  1.00  6.64           N
ATOM    257  C2   DC B  12       8.017   1.257  22.382  1.00  5.68           C
ATOM    258  O2   DC B  12       7.524   1.832  23.357  1.00  6.32           O
ATOM    259  N3   DC B  12       8.543   1.930  21.312  1.00  6.18           N
ATOM    260  C4   DC B  12       9.009   1.236  20.266  1.00  6.48           C
ATOM    261  N4   DC B  12       9.518   1.926  19.243  1.00  7.43           N
ATOM    262  C5   DC B  12       9.012  -0.198  20.248  1.00  6.83           C
ATOM    263  C6   DC B  12       8.502  -0.825  21.311  1.00  6.80           C
  """

def check_na_restraints(geometry, bonds=87, angles=130, planarities=4,
    hbonds=6, hangles=12, stacking_parallelities=2, basepair_planarities=0,
    basepair_parallelities=2, verbose=False):
  actual_bonds = geometry.get_n_bond_proxies()
  actual_angles = geometry.get_n_angle_proxies()
  actual_planarities = geometry.get_n_planarity_proxies()
  actual_hbonds = geometry.get_n_hbond_proxies()
  actual_hangles = geometry.get_n_hangle_proxies()
  actual_stacking = geometry.get_n_stacking_proxies()
  actual_bp_parall = geometry.get_n_parallelity_bp_proxies()
  actual_bp_plan = geometry.get_n_planarity_bp_proxies()
  if verbose:
    print(actual_bonds)
    print(actual_angles)
    print(actual_planarities)
    print(actual_hbonds)
    print(actual_hangles)
    print(actual_stacking)
    print(actual_bp_parall)
    print(actual_bp_plan)
    print("="*50)
  templ = "Getting {:d} {:s}, expecting {:d}"
  assert actual_bonds == bonds, templ.format(
      actual_bonds, "reg bonds", bonds)
  assert actual_angles == angles, templ.format(
      actual_angles, "angles", angles)
  assert actual_planarities == planarities, templ.format(
      actual_planarities, "planarities", planarities)
  assert actual_hbonds == hbonds, templ.format(
      actual_hbonds, "h-bonds", hbonds)
  assert actual_hangles == hangles, templ.format(
      actual_hangles, "h-angles", hangles)
  assert actual_stacking == stacking_parallelities, templ.format(
      actual_stacking, "stacking_parallelities", stacking_parallelities)
  assert actual_bp_parall == basepair_parallelities, templ.format(
      actual_bp_parall, "basepair_parallelities", basepair_parallelities)
  assert actual_bp_plan == basepair_planarities, templ.format(
      actual_bp_plan, "basepair_planarities", basepair_planarities)
  return

def get_geometry(mon_lib_srv, ener_lib, params):
  output = StringIO()
  log = sys.stdout
  processed_pdb_file = pdb_interpretation.process(
    mon_lib_srv    = mon_lib_srv,
    ener_lib       = ener_lib,
    raw_records    = pdb_str_1dpl_cutted,
    params         = params,
    force_symmetry = True,
    log            = output)
  pdb_hierarchy = processed_pdb_file.all_chain_proxies.pdb_hierarchy
  geometry = processed_pdb_file.geometry_restraints_manager()
  return geometry

def exercise(args):
  verbose = False
  if len(args) > 0:
    if args[0].find("verbose") >=0:
      verbose=True
  mon_lib_srv = server.server()
  ener_lib = server.ener_lib()

  params = pdb_interpretation.master_params.extract()
  geometry = get_geometry(mon_lib_srv, ener_lib, params)
  check_na_restraints(geometry, hbonds=0, hangles=0, stacking_parallelities=0,
      basepair_planarities=0, basepair_parallelities=0, verbose=verbose)

  params = pdb_interpretation.master_params.extract()
  params.secondary_structure.enabled = True
  geometry = get_geometry(mon_lib_srv, ener_lib, params)
  check_na_restraints(geometry, hbonds=6, hangles=12, stacking_parallelities=2,
      basepair_planarities=0, basepair_parallelities=2, verbose=verbose)

  params = pdb_interpretation.master_params.extract()
  params.secondary_structure.enabled = True
  params.secondary_structure.nucleic_acid.base_pair[0].restrain_hbonds = False
  geometry = get_geometry(mon_lib_srv, ener_lib, params)
  check_na_restraints(geometry, hbonds=0, hangles=12, stacking_parallelities=2,
      basepair_planarities=0, basepair_parallelities=2, verbose=verbose)

  params = pdb_interpretation.master_params.extract()
  params.secondary_structure.enabled = True
  params.secondary_structure.nucleic_acid.base_pair[0].restrain_hb_angles = False
  geometry = get_geometry(mon_lib_srv, ener_lib, params)
  check_na_restraints(geometry, hbonds=6, hangles=0, stacking_parallelities=2,
      basepair_planarities=0, basepair_parallelities=2, verbose=verbose)

  params = pdb_interpretation.master_params.extract()
  params.secondary_structure.enabled = True
  params.secondary_structure.nucleic_acid.base_pair[0].restrain_planarity = True
  geometry = get_geometry(mon_lib_srv, ener_lib, params)
  check_na_restraints(geometry, hbonds=6, hangles=12, stacking_parallelities=2,
      basepair_planarities=2, basepair_parallelities=2, verbose=verbose)

  params = pdb_interpretation.master_params.extract()
  params.secondary_structure.enabled = True
  params.secondary_structure.nucleic_acid.base_pair[0].restrain_planarity = True
  params.secondary_structure.nucleic_acid.base_pair[0].restrain_parallelity = False
  geometry = get_geometry(mon_lib_srv, ener_lib, params)
  check_na_restraints(geometry, hbonds=6, hangles=12, stacking_parallelities=2,
      basepair_planarities=2, basepair_parallelities=0, verbose=verbose)

  params = pdb_interpretation.master_params.extract()
  params.secondary_structure.enabled = True
  params.secondary_structure.nucleic_acid.base_pair[0].enabled = False
  geometry = get_geometry(mon_lib_srv, ener_lib, params)
  check_na_restraints(geometry, hbonds=0, hangles=0, stacking_parallelities=2,
      basepair_planarities=0, basepair_parallelities=0, verbose=verbose)

  params = pdb_interpretation.master_params.extract()
  params.secondary_structure.enabled = True
  params.secondary_structure.nucleic_acid.stacking_pair[0].enabled = False
  geometry = get_geometry(mon_lib_srv, ener_lib, params)
  check_na_restraints(geometry, hbonds=6, hangles=12, stacking_parallelities=0,
      basepair_planarities=0, basepair_parallelities=2, verbose=verbose)

if (__name__ == "__main__"):
  exercise(sys.argv[1:]) # test various parameters in ss scope
  print("OK")
