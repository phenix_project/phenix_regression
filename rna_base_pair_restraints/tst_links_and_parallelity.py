from __future__ import division
from __future__ import print_function
import os
from six.moves import cStringIO as StringIO
from mmtbx.monomer_library import server, linking_utils
from mmtbx.monomer_library import pdb_interpretation
import libtbx
import shutil
from mmtbx.secondary_structure import manager

DNA_residues = ['DA', 'DC', 'DG', 'DT']

def count_na_param():
  f = open('na_restraints.param', 'r')
  f_cont = f.read()
  f.close()
  f_cont_splitted = f_cont.split()
  n_base_pairs = f_cont_splitted.count('base_pair')
  n_stack_found_pairs = f_cont_splitted.count('stacking_pair')
  f.close()
  if n_base_pairs == 1: n_base_pairs = 0
  return n_stack_found_pairs, n_base_pairs

def determine_pairs(fname,
                    read_na_params=False,
                    write_na_params=False,
                    find_automatically_bonds=True,
                    find_automatically_stacking=True,
                    find_automatically_planarity=True):
  mon_lib_srv = server.server()
  ener_lib = server.ener_lib()
  output = StringIO()
  import sys
  log = sys.stdout
  params = pdb_interpretation.master_params.extract()
  params.secondary_structure.enabled = True
  params.secondary_structure.nucleic_acid.base_pair[0].restrain_planarity=True
  params.secondary_structure.nucleic_acid.base_pair[0].restrain_parallelity=False
  params.secondary_structure.nucleic_acid.hbond_distance_cutoff=3.4
  processed_pdb_file = pdb_interpretation.process(
    mon_lib_srv    = mon_lib_srv,
    ener_lib       = ener_lib,
    file_name      = fname,
    params         = params,
    force_symmetry = True,
    log            = output)
  pdb_hierarchy = processed_pdb_file.all_chain_proxies.pdb_hierarchy
  geometry = processed_pdb_file.geometry_restraints_manager()

  ss_m = manager(
            pdb_hierarchy=pdb_hierarchy,
            geometry_restraints_manager=geometry,
            params=params.secondary_structure,
            log=output)
  phil_str = ss_m.as_phil_str()
  out = open("na_restraints.param", "w")
  phil_str.show(out=out)
  out.close()
  cont = output.getvalue()
  # print "="*50
  # print cont
  # print "="*50
  sp_cont = [x[:30] for x in cont.split('\n')]
  n_ac_stack_rejected = sp_cont.count('  Stacking of residues  pdbres')
  n_ac_plan_rejected = sp_cont.count('  Basepair planarity of residu')
  ind = cont.find("Restraints generated for nucleic acids:")
  s = cont[ind+45:ind+48]
  # print "s:",s
  nlinks = 0
  if ind != -1:
    nlinks = int(s)
  # count basepair planarity restraints
  n_bp_plan = 0
  for p in geometry.planarity_proxies:
    resid1 = pdb_hierarchy.atoms()[p.i_seqs[0]].parent().parent().resid()
    fl = False
    class1 = linking_utils.get_classes(pdb_hierarchy.atoms()[p.i_seqs[0]])
    if class1.common_rna_dna:
      for i_seq in p.i_seqs[1:]:
        fl = fl or (resid1 != pdb_hierarchy.atoms()[i_seq].parent().parent().resid())
      if fl:
        n_bp_plan += 1
  return nlinks, n_bp_plan, len(geometry.parallelity_proxies), \
      n_ac_stack_rejected, n_ac_plan_rejected


# results: [auto, search_pairs, auto+manual, manual], each is
#   nlinks, n_basepair_planarities, n_parallelities,
#   rejected parallelities, rejected planarities
# or stack_found, basepair_found in generated na_params

# in 3dw4 it seems 18 hbonds ok as well as 20.
# in 3p49 it seems 58, 62 hbonds ok as well as 77.
# in 1v9g n_parall changed 9 to 10
#with additional check
correct_results = {
'1dpl_m.pdb': [(22, 12, 11, 12, 0), (8, 8), (22, 12, 11, 12, 0), (22, 12, 11, 0, 0), (22, 12, 11, 0, 0)],
'1ick_m.pdb': [(18, 6, 4, 6, 0), (4, 6), (18, 6, 4, 6, 0), (18, 6, 4, 0, 0), (18, 6, 4, 0, 0)],
'3dw4_m.pdb': [(18, 7, 16, 8, 0), (16, 7), (20, 7, 16, 8, 0), (18, 7, 16, 0, 0), (20, 7, 16, 0, 0)],
'3nj6_m.pdb': [(0, 0, 5, 4, 0), (5, 0), (0, 0, 5, 4, 0), (0, 0, 5, 0, 0), (0, 0, 5, 0, 0)],
'3p4j_m.pdb': [(18, 6, 4, 6, 0), (4, 6), (18, 6, 4, 6, 0), (18, 6, 4, 0, 0), (18, 6, 4, 0, 0)],
'3u89_m.pdb': [(0, 0, 10, 5, 0), (7, 0), (0, 0, 10, 5, 0), (0, 0, 10, 0, 0), (0, 0, 10, 0, 0)],
'3p49.pdb'  : [(58, 25, 85, 83, 0), (85, 25), (77, 25, 85, 83, 0), (58, 25, 85, 0, 0), (77, 25, 85, 0, 0)],
'1v9g.pdb'  : [(18, 11, 10, 14, 0), (4, 6), (18, 11, 9, 14, 0), (18, 11, 9, 0, 0), (18, 11, 9, 0, 0)],
}

if (__name__ == "__main__"):
  results = {}
  n_links_auto = []
  n_par_prox_auto = []
  n_bases_found = []
  n_stack_found = []
  n_links_auto_plus_manual = []
  n_bases_auto_plus_manual = []
  n_links_manual = []
  n_bases_manual = []
  print("(nlinks, n_basepair_planarities, n_parallelities,", end=' ')
  print("rejected parallelities, rejected planarities)")
  print("(stack_found, basepair_found)")
  # 1v9g.pdb is not consistent between py2/3 for stacking parallelities
  # In fact, it contains more than 2 alternative conformations for a plane,
  # when algorithm does not seem designed to handle more than 2 ACs.
  for fname in ['1dpl_m.pdb',
                '1ick_m.pdb', '3dw4_m.pdb',
                '3nj6_m.pdb','3p4j_m.pdb',
                '3u89_m.pdb',
                '3p49.pdb',
                # '1v9g.pdb' # is not consistent between py2/3
                ]:
    pdbfpath = libtbx.env.find_in_repositories(
            relative_path="phenix_regression/rna_base_pair_restraints/"+fname,
            test=os.path.isfile)

    results[fname] = []
    # auto determination and ouput file
    results[fname].append(determine_pairs(pdbfpath, write_na_params=True))

    # counting bases and stacks in param file
    results[fname].append(count_na_param())

    # auto+manual determination
    # results[fname].append(determine_pairs(pdbfpath, read_na_params=True))

    # manual only determination
    # results[fname].append(determine_pairs(pdbfpath,
    #                                       read_na_params=True,
    #                                       find_automatically_bonds=False,
    #                                       find_automatically_stacking=False,
    #                                       find_automatically_planarity=False))

    # # manual + bonds only determination
    # results[fname].append(determine_pairs(pdbfpath,
    #                                       read_na_params=True,
    #                                       find_automatically_bonds=True,
    #                                       find_automatically_stacking=False,
    #                                       find_automatically_planarity=False))

    shutil.move("na_restraints.param", "%s_na_restr.param" % fname)
    print(fname)
    print("Actual results:", results[fname])
    print("Correct results:", correct_results[fname])
    assert results[fname][0][:3] == correct_results[fname][0][:3]
    assert results[fname][1] == correct_results[fname][1]
    # assert results[fname] == correct_results[fname]

  # print "Actual results:", results
  # print "Reference results:", correct_results
  print("All OK")
