from __future__ import division
from __future__ import print_function
from six.moves import cStringIO as StringIO
from mmtbx.monomer_library import server
from mmtbx.monomer_library import pdb_interpretation
import sys
from mmtbx.conformation_dependent_library import nucleotides
from libtbx import easy_run

pdb_427d = '''
CRYST1   27.818   27.818   51.858  90.00  90.00  90.00 P 41 21 2     8
ATOM     40  P  A DC A   3      17.648  22.084  16.541  0.49 10.24           P
ATOM     41  P  B DC A   3      18.497  22.092  17.903  0.51 10.78           P
ATOM     42  OP1A DC A   3      18.139  23.109  17.553  0.49 21.03           O
ATOM     43  OP1B DC A   3      18.655  22.454  19.271  0.51 14.93           O
ATOM     44  OP2A DC A   3      16.836  22.486  15.308  0.49 13.80           O
ATOM     45  OP2B DC A   3      18.552  23.174  16.986  0.51 10.60           O
ATOM     46  O5'A DC A   3      18.767  21.153  15.974  0.49  9.86           O
ATOM     47  O5'B DC A   3      19.594  20.974  17.558  0.51 10.51           O
ATOM     48  C5'A DC A   3      19.627  20.587  16.971  0.49 12.25           C
ATOM     49  C5'B DC A   3      19.606  20.449  16.225  0.51  8.91           C
ATOM     50  C4'A DC A   3      19.821  19.084  16.548  0.49  5.63           C
ATOM     51  C4'B DC A   3      19.761  18.897  16.153  0.51  8.96           C
ATOM     52  O4'A DC A   3      18.506  18.446  16.660  0.49  6.60           O
ATOM     53  O4'B DC A   3      18.490  18.283  16.618  0.51  8.55           O
ATOM     54  C3'A DC A   3      20.339  18.809  15.143  0.49  7.19           C
ATOM     55  C3'B DC A   3      19.941  18.260  14.808  0.51  7.86           C
ATOM     56  O3'A DC A   3      21.534  17.984  15.265  0.49  6.00           O
ATOM     57  O3'B DC A   3      21.399  18.283  14.666  0.51  5.79           O
ATOM     58  C2'A DC A   3      19.185  18.053  14.393  0.49 15.54           C
ATOM     59  C2'B DC A   3      19.465  16.856  15.149  0.51  7.47           C
ATOM     60  C1'A DC A   3      18.392  17.462  15.760  0.49 10.70           C
ATOM     61  C1'B DC A   3      18.375  17.075  16.004  0.51 12.96           C
ATOM     62  N1   DC A   3      16.992  17.136  15.398  1.00  7.56           N
ATOM     63  C2   DC A   3      16.572  15.854  15.134  1.00  6.58           C
ATOM     64  O2   DC A   3      17.378  14.929  15.230  1.00  7.61           O
ATOM     65  N3   DC A   3      15.236  15.676  14.790  1.00  6.47           N
ATOM     66  C4   DC A   3      14.439  16.735  14.726  1.00  7.01           C
ATOM     67  N4   DC A   3      13.135  16.501  14.481  1.00  7.10           N
ATOM     68  C5   DC A   3      14.919  18.072  14.905  1.00  7.71           C
ATOM     69  C6   DC A   3      16.212  18.227  15.215  1.00  7.57           C
HETATM   70  P  AG49 A   4      22.439  17.641  13.962  0.49  6.58           P
HETATM   71  P  BG49 A   4      21.946  18.123  13.173  0.51  6.64           P
HETATM   72  O1PAG49 A   4      23.769  17.337  14.480  0.49  7.24           O
HETATM   73  O1PBG49 A   4      23.397  18.379  13.272  0.51  9.18           O
HETATM   74  O2PAG49 A   4      22.299  18.723  12.939  0.49  7.46           O
HETATM   75  O2PBG49 A   4      21.141  18.902  12.192  0.51  7.81           O
HETATM   76  O5'AG49 A   4      21.714  16.314  13.395  0.49  7.39           O
HETATM   77  O5'BG49 A   4      21.699  16.531  12.855  0.51  6.34           O
HETATM   78  C5'AG49 A   4      22.272  15.093  13.951  0.49 15.69           C
HETATM   79  C5'BG49 A   4      22.198  15.400  13.659  0.51 10.79           C
HETATM   80  C4' G49 A   4      21.786  14.087  13.010  1.00  9.26           C
HETATM   81  O4' G49 A   4      20.323  13.971  13.010  1.00  9.43           O
HETATM   82  C3' G49 A   4      22.260  14.075  11.587  1.00  9.78           C
HETATM   83  O3'AG49 A   4      22.865  12.903  11.140  0.52  9.73           O
HETATM   84  O3'BG49 A   4      22.422  12.605  11.342  0.48  7.78           O
HETATM   85  C2' G49 A   4      20.961  14.507  10.812  1.00  9.13           C
HETATM   86  C1' G49 A   4      19.841  13.947  11.635  1.00  8.19           C
HETATM   87  N9  G49 A   4      18.612  14.667  11.587  1.00  7.03           N
HETATM   88  C8  G49 A   4      18.393  16.019  11.735  1.00  7.33           C
HETATM   89  N7  G49 A   4      17.143  16.357  11.725  1.00  6.42           N
HETATM   90  C5  G49 A   4      16.493  15.147  11.602  1.00  5.59           C
HETATM   91  C6  G49 A   4      15.100  14.853  11.487  1.00  5.74           C
HETATM   92  O6  G49 A   4      14.120  15.647  11.480  1.00  6.43           O
HETATM   93  N1  G49 A   4      14.874  13.496  11.359  1.00  5.82           N
HETATM   94  C2  G49 A   4      15.856  12.508  11.327  1.00  5.92           C
HETATM   95  N2  G49 A   4      15.392  11.253  11.218  1.00  6.12           N
HETATM   96  CM2 G49 A   4      16.313  10.190  11.131  1.00  7.38           C
HETATM   97  N3  G49 A   4      17.145  12.790  11.374  1.00  6.57           N
HETATM   98  C4  G49 A   4      17.354  14.134  11.525  1.00  6.26           C
ATOM     99  P  A DC A   5      23.515  12.764   9.646  0.52 11.35           P
ATOM    100  P  B DC A   5      23.203  12.098  10.068  0.48  9.09           P
ATOM    101  OP1A DC A   5      24.370  11.511   9.792  0.52 12.37           O
ATOM    102  OP1B DC A   5      23.886  10.896  10.415  0.48 10.23           O
ATOM    103  OP2A DC A   5      24.091  14.005   9.172  0.52 13.51           O
ATOM    104  OP2B DC A   5      24.076  13.090   9.589  0.48 12.47           O
ATOM    105  O5'A DC A   5      22.158  12.363   8.794  0.52 11.96           O
ATOM    106  O5'B DC A   5      22.100  11.897   8.963  0.48 10.14           O
ATOM    107  C5'A DC A   5      21.470  11.219   8.921  0.52 12.90           C
ATOM    108  C5'B DC A   5      21.311  10.792   9.020  0.48  8.28           C
ATOM    109  C4'  DC A   5      20.228  11.020   8.023  1.00  9.65           C
ATOM    110  O4'  DC A   5      19.244  11.913   8.491  1.00  8.47           O
ATOM    111  C3'  DC A   5      20.633  11.404   6.632  1.00  9.17           C
ATOM    112  O3'  DC A   5      19.991  10.676   5.603  1.00  9.67           O
ATOM    113  C2'  DC A   5      20.036  12.861   6.566  1.00  9.00           C
ATOM    114  C1'  DC A   5      18.830  12.667   7.398  1.00  6.93           C
ATOM    115  N1   DC A   5      18.139  13.900   7.866  1.00  6.17           N
ATOM    116  C2   DC A   5      16.799  13.789   8.130  1.00  5.64           C
ATOM    117  O2   DC A   5      16.298  12.630   8.169  1.00  6.01           O
ATOM    118  N3   DC A   5      16.042  14.890   8.295  1.00  5.63           N
ATOM    119  C4   DC A   5      16.677  16.072   8.355  1.00  5.84           C
ATOM    120  N4   DC A   5      15.890  17.155   8.485  1.00  6.81           N
ATOM    121  C5   DC A   5      18.032  16.215   8.239  1.00  6.93           C
ATOM    122  C6   DC A   5      18.749  15.112   7.991  1.00  7.11           C
'''

g49 = '''
data_comp_list
loop_
_chem_comp.id
_chem_comp.three_letter_code
_chem_comp.name
_chem_comp.group
_chem_comp.number_atoms_all
_chem_comp.number_atoms_nh
_chem_comp.desc_level
G49        G49 '2'-deoxy-N-methylguanosine 5'-(dihydrogen phosphate)' RNA 38 24 .
#
data_comp_G49
#
loop_
_chem_comp_atom.comp_id
_chem_comp_atom.atom_id
_chem_comp_atom.type_symbol
_chem_comp_atom.type_energy
_chem_comp_atom.charge
_chem_comp_atom.partial_charge
_chem_comp_atom.x
_chem_comp_atom.y
_chem_comp_atom.z
G49         P      P   P      0    .       2.0277    3.6998    1.5452
G49         O1P    O   O      0    .       2.3913    3.8401    3.0022
G49         O2P    O   OP    -1    .       0.8527    4.5993    1.2296
G49         O3P    O   OP    -1    .       3.2122    4.0967    0.6885
G49        O5'     O   O2     0    .       1.6200    2.1287    1.2278
G49        C5'     C   CH2    0    .       1.2454    1.7858   -0.0828
G49        C4'     C   CR15   0    .       2.2979    1.0608   -0.7021
G49        O4'     O   O      0    .       2.4027   -0.2813   -0.1173
G49        C3'     C   CR15   0    .       1.9756    0.8320   -2.2464
G49        O3'     O   OH1    0    .       2.9100    1.6756   -3.1070
G49        C2'     C   CH2    0    .       2.1780   -0.3518   -2.4479
G49        C1'     C   CR15   0    .       1.9308   -1.0904   -1.0209
G49         N9     N   NR5    0    .       0.5174   -1.3040   -0.8089
G49         C8     C   CR15   0    .       0.6024   -2.3569   -1.6461
G49         N7     N   N      0    .      -0.4602   -3.1614   -1.4166
G49         C5     C   CR56   0    .      -1.1914   -2.6128   -0.4508
G49         C6     C   CR6    0    .      -2.4863   -3.0659    0.2462
G49         O6     O   O      0    .      -2.9967   -4.0959   -0.0580
G49         N1     N   NR16   0    .      -3.0828   -2.2413    1.2512
G49         C2     C   CR6    0    .      -2.4139   -1.0387    1.6757
G49         N2     N   NH1    0    .      -3.0161   -0.1917    2.7077
G49         CM2    C   CH3    0    .      -4.4682   -0.1227    2.8393
G49         N3     N   N      0    .      -1.1403   -0.6513    1.0596
G49         C4     C   CR56   0    .      -0.5820   -1.4521   -0.0700
G49        H5'1    H   HCH2   0    .       0.3520    1.1722   -0.0544
G49        H5'2    H   HCH2   0    .       1.0472    2.6832   -0.6460
G49        H4'     H   HCR5   0    .       3.2400    1.5895   -0.5924
G49        H3'     H   HCR5   0    .       0.9418    1.0755   -2.4528
G49         HA     H   HOH1   0    .       2.4233    2.0524   -3.8243
G49        H2'1    H   HCH2   0    .       3.2061   -0.5054   -2.7765
G49        H2'2    H   HCH2   0    .       1.4806   -0.7289   -3.1900
G49        H1'     H   HCR5   0    .       2.4639   -2.0296   -0.9924
G49         H8     H   HCR5   0    .       1.3968   -2.5433   -2.3498
G49         H1     H   HNR6   0    .      -3.9582   -2.4978    1.6564
G49         H2     H   HNH1   0    .      -2.4326    0.3492    3.3142
G49        HM21    H   HCH3   0    .      -4.7356    0.7276    3.4583
G49        HM22    H   HCH3   0    .      -4.9179   -0.0093    1.8513
G49        HM23    H   HCH3   0    .      -4.8337   -1.0360    3.3006
#
loop_
_chem_comp_bond.comp_id
_chem_comp_bond.atom_id_1
_chem_comp_bond.atom_id_2
_chem_comp_bond.type
_chem_comp_bond.value_dist
_chem_comp_bond.value_dist_esd
G49   P       O1P   deloc         1.508 0.020
G49   P       O2P   deloc         1.513 0.020
G49   P       O3P   deloc         1.515 0.020
G49   P      O5'    single        1.654 0.020
G49  O5'     C5'    single        1.406 0.020
G49  C5'     C4'    single        1.420 0.020
G49  C5'     H5'1   single        1.084 0.020
G49  C5'     H5'2   single        1.078 0.020
G49  C4'     O4'    single        1.468 0.020
G49  C4'     C3'    single        1.594 0.020
G49  C4'     H4'    single        1.086 0.020
G49  O4'     C1'    single        1.302 0.020
G49  C3'     O3'    single        1.525 0.020
G49  C3'     C2'    single        1.218 0.020
G49  C3'     H3'    single        1.082 0.020
G49  O3'      HA    single        0.945 0.020
G49  C2'     C1'    single        1.626 0.020
G49  C2'     H2'1   single        1.090 0.020
G49  C2'     H2'2   single        1.086 0.020
G49  C1'      N9    single        1.445 0.020
G49  C1'     H1'    single        1.080 0.020
G49   N9      C8    aromatic      1.348 0.020
G49   N9      C4    aromatic      1.333 0.020
G49   C8      N7    aromatic      1.352 0.020
G49   C8      H8    single        1.078 0.020
G49   N7      C5    aromatic      1.330 0.020
G49   C5      C6    single        1.539 0.020
G49   C5      C4    aromatic      1.365 0.020
G49   C6      O6    double        1.189 0.020
G49   C6      N1    single        1.430 0.020
G49   N1      C2    single        1.440 0.020
G49   N1      H1    single        0.998 0.020
G49   C2      N2    single        1.465 0.020
G49   C2      N3    double        1.467 0.020
G49   N2      CM2   single        1.460 0.020
G49   N2      H2    single        1.000 0.020
G49   CM2    HM21   single        1.085 0.020
G49   CM2    HM22   single        1.091 0.020
G49   CM2    HM23   single        1.086 0.020
G49   N3      C4    single        1.493 0.020
#
'''

def get_geometry(mon_lib_srv, ener_lib, params):
  output = StringIO()
  log = sys.stdout
  processed_pdb_file = pdb_interpretation.process(
    mon_lib_srv    = mon_lib_srv,
    ener_lib       = ener_lib,
    raw_records    = pdb_427d,
    params         = params,
    force_symmetry = True,
    log            = output)
  pdb_hierarchy = processed_pdb_file.all_chain_proxies.pdb_hierarchy
  geometry = processed_pdb_file.geometry_restraints_manager()
  return pdb_hierarchy, geometry, output

def exercise00(args):
  verbose = False
  if len(args) > 0:
    if args[0].find("verbose") >=0:
      verbose=True
  mon_lib_srv = server.server()
  ener_lib = server.ener_lib()

  f=open('g49_short.cif', 'w')
  f.write(g49)
  del f
  mon_lib_srv.process_cif('g49_short.cif')

  params = pdb_interpretation.master_params.extract()
  hierarchy, geometry, output = get_geometry(mon_lib_srv, ener_lib, params)

  rc = nucleotides.update_restraints(hierarchy,
                                     geometry,
                                     # verbose=1,
                                     )
  print(rc)
  '''
  CDL for nucleotides adjusted restraints counts
    coord. : total (unchanged)
    bonds  :    41 (   34)
    angles :    65 (   59)
    '''
  assert rc[0]==41, 'number of bonds in standard RNA/DNA not is 41'
  assert rc[1]==34, 'number of bonds in standard RNA/DNA not is 34'

  params = pdb_interpretation.master_params.extract()
  params.restraints_library.cdl_nucleotides = True
  hierarchy, geometry, output = get_geometry(mon_lib_srv, ener_lib, params)

  rc = nucleotides.update_restraints(hierarchy,
                                     geometry,
                                     )
  print(rc)
  assert rc[1]==0, 'bond restraints should remain the same'
  assert rc[3]==0, 'angle restraints should remain the same'

def exercise01(args):
  verbose = False
  if len(args) > 0:
    if args[0].find("verbose") >=0:
      verbose=True

  cmds = ['',
  ' cdl_nucleotides.enable=True',
  ' cdl_nucleotides.enable=True cdl_nucleotides.esd=phenix',
  ' cdl_nucleotides.enable=True cdl_nucleotides.esd=csd',
  ]
  cmds.append(cmds[2]+' cdl_nucleotides.factor=3.')
  cmds.append(cmds[3]+' cdl_nucleotides.factor=3.')
  answers = ['''angle pdb=" C6   DC A   3 "
      pdb=" N1   DC A   3 "
      pdb=" C1'A DC A   3 "
    ideal   model   delta    sigma   weight residual
   119.70  113.58    6.12 1.50e+00 4.44e-01 1.67e+01''',
  '''angle pdb=" C6   DC A   3 "
      pdb=" N1   DC A   3 "
      pdb=" C1'A DC A   3 "
    ideal   model   delta    sigma   weight residual
   120.40  113.58    6.82 1.50e+00 4.44e-01 2.07e+01''',
  '''angle pdb=" C6   DC A   3 "
      pdb=" N1   DC A   3 "
      pdb=" C1'A DC A   3 "
    ideal   model   delta    sigma   weight residual
   120.40  113.58    6.82 1.50e+00 4.44e-01 2.07e+01''',
  '''angle pdb=" C6   DC A   3 "
      pdb=" N1   DC A   3 "
      pdb=" C1'A DC A   3 "
    ideal   model   delta    sigma   weight residual
   120.40  113.58    6.82 1.60e+00 3.91e-01 1.82e+01''',
  ]
  answers.append(answers[2])
  answers.append('''angle pdb=" C6   DC A   3 "
      pdb=" N1   DC A   3 "
      pdb=" C1'A DC A   3 "
    ideal   model   delta    sigma   weight residual
   120.40  113.58    6.82 2.40e+00 1.74e-01 8.08e+00''')

  pf = 'tst_cdl_nucleotides_filename.pdb'
  f=open(pf, 'w')
  f.write(pdb_427d)
  del f
  for i, additions in enumerate(cmds):
    cmd = 'phenix.pdb_interpretation %s write_geo=True' % pf
    cmd += additions
    print('\n ~> %s\n' % cmd)
    rc = easy_run.go(cmd)
    assert rc.return_code==0
    f=open('%s.geo' % pf, 'r')
    lines=f.read()
    del f
    print('Checking\n%s' % answers[i])
    assert lines.find(answers[i])>-1, 'lines "%s" not found' % answers[i]

if (__name__ == "__main__"):
  # exercise00(sys.argv[1:])
  exercise01(sys.argv[1:])
  print("OK")
