from __future__ import print_function
import iotbx.pdb
import mmtbx.f_model
from iotbx import reflection_file_reader
from iotbx import reflection_file_utils
from six.moves import cStringIO as StringIO
import mmtbx
import sys
import mmtbx.masks
from scitbx.array_family import flex
from libtbx.test_utils import not_approx_equal, approx_equal
import libtbx
from cctbx.development import debug_utils
from cctbx.development import random_structure
import iotbx.mtz
from cctbx import adptbx
from iotbx import extract_xtal_data

if (libtbx.env.has_module("solve_resolve")):
  import solve_resolve.masks as phenix_masks
else:
  phenix_masks = None

def get_fmodel(pdb_file_name, data_file_name):
  xrs = iotbx.pdb.input(file_name = pdb_file_name).xray_structure_simple()
  reflection_file = reflection_file_reader.any_reflection_file(
    file_name = data_file_name)
  reflection_file_server = reflection_file_utils.reflection_file_server(
    reflection_files = [reflection_file])
  data_and_flags = extract_xtal_data.run(
    reflection_file_server = reflection_file_server,
    log                    = StringIO())
  fmodel = mmtbx.f_model.manager(
    xray_structure = xrs,
    f_obs          = data_and_flags.f_obs,
    r_free_flags   = data_and_flags.r_free_flags)
  return fmodel

def exercise_00(space_group_info):
  ccp4_sg_sybol = iotbx.mtz.extract_from_symmetry_lib.ccp4_symbol(
    space_group_info=space_group_info, lib_name="syminfo.lib")
  if(ccp4_sg_sybol is not None):
    xrs = random_structure.xray_structure(
        space_group_info=space_group_info,
        elements=(("O",)*2),
        volume_per_atom=1000,
        u_iso=adptbx.b_as_u(15.),
        min_distance=6)
    f_obs = abs(xrs.structure_factors(d_min=1.7).f_calc())
    r_free_flags = f_obs.generate_r_free_flags()
    import mmtbx.f_model
    fmodel = mmtbx.f_model.manager(
      xray_structure = xrs,
      f_obs          = f_obs,
      r_free_flags   = r_free_flags)
    assert not_approx_equal(flex.mean(abs(fmodel.f_masks()[0]).data()),0)
    import mmtbx.resolve_resources
    params = \
        mmtbx.resolve_resources.get_phenix_masks_master_params().extract()
    params.solvent_content = 0.95
    params.diff_map_cutoff = 3.5
    params.probability_mask = False
    params.output_all_masks = True
    r = phenix_masks.nu(fmodel = fmodel, params = params)
    for i, site_f in enumerate(fmodel.xray_structure.sites_frac()):
      assert approx_equal(r.bulk_solvent_mask.value_at_closest_grid_point(site_f), 0)
      assert approx_equal(r.rmask            .value_at_closest_grid_point(site_f), 1)
      assert approx_equal(r.new              .value_at_closest_grid_point(site_f), 0)
      # Undefined: assert approx_equal(r.diff_map_mask    .value_at_closest_grid_point(site_f), 0)
      assert approx_equal(r.dmmrm            .value_at_closest_grid_point(site_f), 0)
      assert approx_equal(r.new_bs_mask      .value_at_closest_grid_point(site_f), 0)

def exercise_01(space_group_info):
  ccp4_sg_sybol = iotbx.mtz.extract_from_symmetry_lib.ccp4_symbol(
    space_group_info=space_group_info, lib_name="syminfo.lib")
  if(ccp4_sg_sybol is not None):
    xrs = random_structure.xray_structure(
        space_group_info=space_group_info,
        elements=(("O",)*2),
        volume_per_atom=1000,
        u_iso=adptbx.b_as_u(15.),
        min_distance=6)
    f_obs = abs(xrs.structure_factors(d_min=1.7).f_calc())
    r_free_flags = f_obs.generate_r_free_flags()
    import mmtbx.f_model
    fmodel = mmtbx.f_model.manager(
      xray_structure = xrs.select(flex.size_t([0])),
      f_obs          = f_obs,
      r_free_flags   = r_free_flags)
    assert not_approx_equal(flex.mean(abs(fmodel.f_masks()[0]).data()),0)
    import mmtbx.resolve_resources
    params = \
        mmtbx.resolve_resources.get_phenix_masks_master_params().extract()
    params.solvent_content = 0.95
    params.diff_map_cutoff = 3.5
    params.probability_mask = False
    params.output_all_masks = True
    r = phenix_masks.nu(fmodel = fmodel, params = params)
    for i, site_f in enumerate(xrs.sites_frac()):
      print("atom:", i)
      print("bulk_solvent_mask:", r.bulk_solvent_mask.value_at_closest_grid_point(site_f))
      print("rmask            :", r.rmask            .value_at_closest_grid_point(site_f))
      print("new              :", r.new              .value_at_closest_grid_point(site_f))
      print("diff_map_mask    :", r.diff_map_mask    .value_at_closest_grid_point(site_f))
      print("dmmrm            :", r.dmmrm            .value_at_closest_grid_point(site_f))
      print("new_bs_mask      :", r.new_bs_mask      .value_at_closest_grid_point(site_f))

def run_call_back(flags, space_group_info):
  exercise_00(space_group_info)
  exercise_01(space_group_info)

def run(args):
  if (phenix_masks is None):
    print("Skipping tests: solve_resolve.masks.phenix_masks not available")
  else:
    debug_utils.parse_options_loop_space_groups(sys.argv[1:], run_call_back)
  print("OK")

if (__name__ == "__main__"):
  import sys
  run(args=sys.argv[1:])
