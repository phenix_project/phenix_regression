from __future__ import print_function
import iotbx.pdb
import mmtbx
import os
import mmtbx.masks
from scitbx.array_family import flex
from libtbx.test_utils import not_approx_equal
import libtbx
if (libtbx.env.has_module("solve_resolve")):
  import solve_resolve.masks as phenix_masks
else:
  phenix_masks = None

def exercise_00():
  pdb_file_name = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/phenix_masks/model_two_atoms_P1.pdb",
    test=os.path.isfile)
  xrs = iotbx.pdb.input(file_name = pdb_file_name).xray_structure_simple()
  f_obs = abs(xrs.structure_factors(d_min=2.0).f_calc())
  r_free_flags = f_obs.generate_r_free_flags()
  import mmtbx.f_model
  fmodel = mmtbx.f_model.manager(
    xray_structure = xrs.select(flex.size_t([0])),
    f_obs          = f_obs,
    r_free_flags   = r_free_flags)
  assert not_approx_equal(flex.mean(abs(fmodel.f_masks()[0]).data()),0)
  import mmtbx.resolve_resources
  params = \
        mmtbx.resolve_resources.get_phenix_masks_master_params().extract()
  params.solvent_content = 0.99
  params.diff_map_cutoff = 3.5
  params.probability_mask = False
  params.output_all_masks = True
  r = phenix_masks.nu(fmodel = fmodel, params = params, solvent_radius_inc = 1.)

if (__name__ == "__main__"):
  if(phenix_masks is None):
    print("Skipping tests: solve_resolve.masks.phenix_masks not available")
  else:
    exercise_00()
  print("OK")
