from __future__ import print_function
import os
import sys

def introduction():
  from libtbx import adopt_init_args

  class cartesian_dynamics:
    def __init__(self, temperature     = 300,
                       number_of_steps = 200,
                       time_step       = 0.0005,
                       n_print         = 100,
                       verbose         = 1):
      adopt_init_args(self, locals())

  class simulated_annealing:
    def __init__(self, do_simulated_annealing = False,
                       start_temperature      = 2500,
                       final_temperature      = 300,
                       cool_rate              = 25,
                       number_of_steps        = 25,
                       time_step              = 0.0005,
                       n_print                = 100,
                       update_grads_shift     = 0.3,
                       verbose                = -1):
      adopt_init_args(self, locals())

  def some_algorithm(
        cartesian_dynamics_params,
        simulated_annealing_params):
    print(cartesian_dynamics_params.temperature)
    print(cartesian_dynamics_params.number_of_steps)
    print(simulated_annealing_params.start_temperature)
    print(simulated_annealing_params.final_temperature)

  my_cartesian_dynamics_params = cartesian_dynamics(number_of_steps=300)
  my_simulated_annealing_params = simulated_annealing(final_temperature=200)
  some_algorithm(
    cartesian_dynamics_params=my_cartesian_dynamics_params,
    simulated_annealing_params=my_simulated_annealing_params)

def merging():
  import iotbx.phil

  master_params = iotbx.phil.parse("""
    refinement.crystal_symmetry {
      unit_cell = None
        .type=unit_cell
      space_group = None
        .type=space_group
    }
    """)

  user_params = iotbx.phil.parse("""
    refinement.crystal_symmetry {
      unit_cell = 10 12 12 90 90 120
      space_group = None
    }
    """)

  command_line_params = iotbx.phil.parse(
    "refinement.crystal_symmetry.space_group=19")

  effective_params = master_params.fetch(
    sources=[user_params, command_line_params])
  effective_params.show()

  import libtbx.phil.command_line

  argument_interpreter = libtbx.phil.command_line.argument_interpreter(
    master_phil=master_params,
    home_scope="refinement")

  command_line_params = argument_interpreter.process(
    arg="space_group=19")

  effective_params = master_params.fetch(
    sources=[user_params, command_line_params])
  effective_params.show()

  if ("--fail1" in sys.argv[1:]):
    argument_interpreter.process("u=19")

  params = effective_params.extract()

  print(params.refinement.crystal_symmetry.unit_cell)
  print(params.refinement.crystal_symmetry.space_group)

  print(repr(params.refinement.crystal_symmetry.space_group))

  for s in params.refinement.crystal_symmetry.space_group.group():
    print(s)

  from cctbx import uctbx

  params.refinement.crystal_symmetry.unit_cell = uctbx.unit_cell(
    (10,12,15,90,90,90))
  modified_params = master_params.format(python_object=params)
  modified_params.show()

def variable_substitution():

  import libtbx.phil

  params = libtbx.phil.parse("""
    root_name = peak
    file_name = $root_name.mtz
    full_path = $HOME/$file_name
    related_file_name = $(root_name)_data.mtz
    message = "Reading $file_name"
    as_is = ' $file_name '
    """)
  params.show()
  params.fetch(source=params).show()

def to_mtz():
  from iotbx import reflection_file_reader
  import os

  reflection_file = reflection_file_reader.any_reflection_file(
    file_name=os.path.expandvars(
      "$CNS_SOLVE/doc/html/tutorial/data/pen/scale.hkl"))

  from cctbx import crystal

  crystal_symmetry = crystal.symmetry(
    unit_cell=(97.37, 46.64, 65.47, 90, 115.4, 90),
    space_group_symbol="C2")

  miller_arrays = reflection_file.as_miller_arrays(
    crystal_symmetry=crystal_symmetry)

  from iotbx import mtz

  mtz_dataset = None
  for miller_array in miller_arrays:
    if (mtz_dataset is None):
      mtz_dataset = miller_array.as_mtz_dataset(
        column_root_label=miller_array.info().labels[0])
    else:
      mtz_dataset.add_miller_array(
        miller_array=miller_array,
        column_root_label=miller_array.info().labels[0])

  mtz_object = mtz_dataset.mtz_object()
  mtz_object.show_summary()

  mtz_object.write("pen_data.mtz")

if (__name__ == "__main__"):
  if (os.environ.get("CNS_SOLVE") is None) :
    print("$CNS_SOLVE undefined, skipping test.")
  else :
    introduction()
    merging()
    variable_substitution()
    to_mtz()
    print("OK")
