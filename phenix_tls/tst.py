from __future__ import print_function
import libtbx.load_env
import time, sys, os
from libtbx.test_utils import approx_equal
from libtbx import easy_run
import iotbx.pdb
from scitbx.array_family import flex
from cctbx import adptbx

def exercise_01():
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/phenix_tls/phe_tls_1.pdb", test=os.path.isfile)
  cmd = " ".join([
    'phenix.tls',
    '%s'%pdb,
    'combine_tls=true',
    '> log_exercise_01'])
  print(cmd)
  assert not easy_run.call(cmd)
  us = iotbx.pdb.input(file_name = "phe_tls_1_combine_tls.pdb").\
    xray_structure_simple().extract_u_iso_or_u_equiv()
  assert adptbx.u_as_b(flex.mean(us)) > 30.
  #
  cmd = " ".join([
    'phenix.tls',
    'phe_tls_1_combine_tls.pdb',
    'extract_tls=true',
    'selection="%s"'%"chain A",
    'selection="%s"'%"chain B",
    'selection="%s"'%"chain C",
    '> log_exercise_01'])
  print(cmd)
  assert not easy_run.call(cmd)
  us = iotbx.pdb.input(file_name = "phe_tls_1_combine_tls_extract_tls.pdb").\
    xray_structure_simple().extract_u_iso_or_u_equiv()
  assert us.min_max_mean().as_tuple() == (0.0, 0.0, 0.0)


if (__name__ == "__main__"):
  t0 = time.time()
  exercise_01()
  print("OK Time: %8.3f"%(time.time()-t0))
