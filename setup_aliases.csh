alias all_tests '"`libtbx.find_in_repositories phenix_regression/all_tests.csh`" > & zlog &'
alias all_tests_p2 'mkdir -p all_tests_0 all_tests_1; (cd all_tests_0 && "`libtbx.find_in_repositories phenix_regression/all_tests_0.csh`" > & zlog &); (cd all_tests_1 && "`libtbx.find_in_repositories phenix_regression/all_tests_1.csh`" > & zlog &)'
alias eval_tests 'phenix_regression.find_errors_and_warnings zlog tst_phenix_refine/zlog'
alias eval_test_logs phenix_regression.find_errors_and_warnings
alias refine_tests_p2 'mkdir -p tst_phenix_refine_0 tst_phenix_refine_1; (cd tst_phenix_refine_0 && "`libtbx.find_in_repositories phenix_regression/tst_phenix_refine_0.csh`" > & zlog &); (cd tst_phenix_refine_1 && "`libtbx.find_in_repositories phenix_regression/tst_phenix_refine_1.csh`" > & zlog &)'
alias tests_p4 'all_tests_p2; refine_tests_p2'
alias eval_tests_p4 'ps -u $USER | egrep "tst[_]phenix|all[_]tests"; phenix_regression.find_errors_and_warnings all_tests_0/zlog all_tests_1/zlog tst_phenix_refine_0/zlog tst_phenix_refine_1/zlog'
alias t96 'phenix_regression.test_all_parallel nproc=96'
alias t48 'phenix_regression.test_all_parallel nproc=48'
alias t32 'phenix_regression.test_all_parallel nproc=32'
alias t24 'phenix_regression.test_all_parallel nproc=24'
