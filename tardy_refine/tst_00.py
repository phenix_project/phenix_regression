from __future__ import print_function
import libtbx.load_env
import time, os
from libtbx import easy_run

def exercise_01(prefix="tardy_refine_00"):
  pdb = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/tardy_refine/tpp.pdb", test=os.path.isfile)
  cmd = " ".join([
    'phenix.fmodel',
    '%s'%pdb,
    'high_res=3',
    "type=real",
    "r_free=0.1",
    "output.file_name=%s.mtz"%prefix,
    '> %s.log'%prefix])
  assert not easy_run.call(cmd)
  #
  cmd = " ".join([
    'phenix.refine',
    pdb,
    "%s.mtz"%prefix,
    "simulated_annealing=true simulated_annealing_torsion=true",
    "main.number_of_mac=1",
    "strategy=individual_sites",
    "main.bulk_solvent_and_scale=False",
    '> %s.log'%prefix])
  print(' ~> %s' % cmd)
  assert easy_run.call(cmd)==0, "Expected to file. Nigel?"

if (__name__ == "__main__"):
  t0 = time.time()
  exercise_01()
  print("OK Time: %8.3f"%(time.time()-t0))
