import os, sys
from libtbx import easy_run

sdp = easy_run.go('libtbx.show_dist_paths libtbx')
print(sdp.stdout_lines)
libtbx_dir = sdp.stdout_lines[0]

cmds = [
  'libtbx.exec python -V',
  'libtbx.env_run LIBTBX_BUILD %s -V' % os.path.join('bin','libtbx.python'),
  'libtbx.show_python_sys_executable',
  'libtbx.show_host_and_user',
  'libtbx.show_build_path',
  'libtbx.show_bin_path',
  'libtbx.show_lib_path',
  'libtbx.show_include_paths',
  'libtbx.show_include_paths cctbx_common_includes',
  'libtbx.show_dist_paths libtbx',
  'libtbx.show_dist_paths libtbx --dirname',
  'libtbx.show_repository_paths',
  'libtbx.show_pythonpath',
  'libtbx.show_build_options | grep Build.mode',
  'libtbx.show_commands | grep refresh | head -1 | libtbx.assert_stdin "libtbx.refresh"',
  'libtbx.show_all_on_path',
  'libtbx.show_environ_usage | grep VALGRIND | libtbx.assert_stdin LIBTBX_VALGRIND',
  'libtbx.show_number_of_processors',
  'libtbx.show_full_command_path libtbx.python',
  'libtbx.show_full_command_path libtbx.never_this_name | libtbx.assert_stdin None',
  'libtbx.chunk n i',
  'libtbx.prime_factors_of 60480',
  'libtbx.list_source_files_sorted_by_size "%s"' % libtbx_dir,
  'phenix.show_build_path',
  'libtbx.list_files "%s" -q' % (os.path.join(libtbx_dir, 'phil')),
  'libtbx.find_files -t "%s" -g zip -g fatal -i -f -q' % libtbx_dir,
  'phenix.show_dist_paths phenix',
  'phenix.python -V',
  ]

def grepper(lines, s):
  s=s.strip()
  tmp = []
  for line in lines:
    if line.find(s)>-1:
      tmp.append(line)
  return tmp

def header(lines, args):
  args = args.replace('-','')
  args = int(args)
  tmp = []
  for line in lines:
    tmp.append(line)
    if len(tmp)>=args: break
  return tmp

def libtbx_assert_stdin(lines, s):
  s = s.replace('"', '').strip()
  tmp = '\n'.join(lines)
  assert tmp.find(s)>-1

def run_and_test(cmd):
  print('\n ~> %s\n' % cmd)
  if cmd.find('|'):
    tmp = cmd.split('|')
    cmd = tmp[0]
    del tmp[0]
  rc = easy_run.go(cmd)
  if rc.return_code==0:
    print('OK')
  if tmp:
    print(tmp)
    lines = rc.stdout_lines
    for pip in tmp:
      if pip.find('grep')>-1:
        lines = grepper(lines, pip.replace('grep',''))
      elif pip.find('head')>-1:
        lines = header(lines, pip.replace('head',''))
      elif pip.find('libtbx.assert_stdin')>-1:
        libtbx_assert_stdin(lines, pip.replace('libtbx.assert_stdin',''))
      print('\n'.join(lines))
  return rc.return_code

def main(only_i=None):
  try: only_i=int(only_i)
  except: only_i=None
  print('only_i',only_i)
  rcs = []
  for i, cmd in enumerate(cmds):
    print('  i',i)
    if only_i is not None and only_i!=i+1: continue
    rc = run_and_test(cmd)
    if rc:
      print('FAILED: %s' % cmd)
      rcs.append(cmd)

  if len(rcs):
    print('FAILED')
    for cmd in rcs:
      print('. %s' % cmd)
  assert len(rcs)==0

if __name__ == '__main__':
  main(*tuple(sys.argv[1:]))
