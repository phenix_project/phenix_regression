from __future__ import print_function

import sys

def method_to_run(value_1=None,value_2=None,out=sys.stdout):
  # This is the method we want to run and return something
  # any keywords and any method below are allowed, except that
  #  the values of all the keywords must be picklable

  # It is best for the return object to be small (not a huge molecule) because
  # the multiprocessing system can hang if the return objects are very large.

  value=value_1*value_2
  return value

def run_group(nproc=1, out=sys.stdout):

  # This is a method that runs method_to_run in parallel. You create
  #  the two methods together. This method sets up what is to be run
  #  and calls run_parallel to run method_to_run

  # You pass individual keyworded inputs to method_to_run.

  # You can specify the output stream, normally you will want to set it
  #  to null_out() if nproc>1 and to out if nproc==1

  if nproc==1:
    out_use=out
  else:
    from libtbx.utils import null_out
    out_use=null_out()

  #  The kw_list is a list of keyword dicts, one for each call of method_to_run

  # Put a loop here to create all the kw sets that you want run
  # You are going to run method_to_run(**kw) with each kw set.

  kw_list=[]
  for value_1 in range(2): # this is any loop you want to set up
    for value_2 in range(2):

      # Here you add a dict to kw_list. The keys for the dict must match the
      #   allowed keywords in your method_to_run. In this example they are
      #   value_1, value_2 and out

      kw_list.append(
       {'value_1':value_1,
        'value_2':value_2,
        'out':out_use })

  print("\nRunning %d jobs on %d processors\n" %(len(kw_list),nproc), file=out)

  # Now run the jobs in parallel.  Note that if method_to_run returns a single
  # item, then results will contain a list of those items.

  from libtbx.easy_mp import run_parallel
  results=run_parallel(
     method='multiprocessing',
     qsub_command=None,
     nproc=nproc,
     target_function=method_to_run,
     kw_list=kw_list)

  # Summarize what inputs and outputs were for each run
  for r,kw in zip(results,kw_list):
    print("\nValue 1: %d  Value 2: %d    Result: %d " %(
      kw['value_1'],kw['value_2'],r), file=out)

def remove_blank(text):
  return text.replace(" ","").replace("\n","")


expected_1="""
Running 4 jobs on 1 processors
Value 1: 0  Value 2: 0    Result: 0
Value 1: 0  Value 2: 1    Result: 0
Value 1: 1  Value 2: 0    Result: 0
Value 1: 1  Value 2: 1    Result: 1
"""

expected_2="""
Running 4 jobs on 2 processors
Value 1: 0  Value 2: 0    Result: 0
Value 1: 0  Value 2: 1    Result: 0
Value 1: 1  Value 2: 0    Result: 0
Value 1: 1  Value 2: 1    Result: 1
"""

def tst_01():
  from six.moves import cStringIO as StringIO

  f=StringIO()
  run_group(nproc=1,out=f)
  result1=f.getvalue()

  f=StringIO()
  run_group(nproc=2,out=f)
  result2=f.getvalue()

  if remove_blank(result1) != remove_blank(expected_1):
      print("Expected: \n%s \nFound: \n%s" %(expected_1,result1))
      raise AssertionError("FAILED")

  if remove_blank(result2) != remove_blank(expected_2):
      print("Expected: \n%s \nFound: \n%s" %(expected_2,result2))
      raise AssertionError("FAILED")

  print("OK")

if __name__=="__main__":
  tst_01()
