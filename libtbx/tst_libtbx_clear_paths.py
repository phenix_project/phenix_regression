from __future__ import print_function
from libtbx import easy_run
import os.path as op
import sys

def run(args):
  assert len(args) == 0
  c = easy_run.call
  if (op.exists("clear_paths_target")):
    c('chmod 755 clear_paths_target')
  c('rm -rf clear_paths_target')
  assert not op.exists("clear_paths_target")
  c('cp -r "`libtbx.show_dist_paths libtbx`"/command_line clear_paths_target')
  assert op.exists("clear_paths_target")
  c('chmod 000 clear_paths_target')
  c('libtbx.clear_paths clear_paths_target')
  assert not op.exists("clear_paths_target")
  #
  open("clear_paths_target", "w")
  c('chmod 000 clear_paths_target')
  assert op.exists("clear_paths_target")
  c('libtbx.clear_paths clear_paths_target')
  assert not op.exists("clear_paths_target")
  #
  print("OK")

if (__name__ == "__main__"):
  run(args=sys.argv[1:])
