from __future__ import print_function
from libtbx import group_args

import sys

'''

Examples of how to use simple_parallel

First example is a very simple one, with one argument passed to the
function that is to be run in parallel.

Second example shows how to pass one argument that varies and any number
of keyword arguments that are fixed.  The argument that varies can be a
group_args object (it can be anything) which in turn can contain multiple
objects or values.

'''

# SIMPLE EXAMPLE

def run_something(value):
  return value * 2

def run_as_is(): # run in usual way
  iteration_list = [5,7,9]  # list of anything

  result_list = []
  for i in range(len(iteration_list)):
    result = run_something(iteration_list[i])
    result_list.append(result)
  return result_list

def run_parallel(): # run in parallel

  iteration_list = [5,7,9]  # list of anything

  from libtbx.easy_mp import simple_parallel
  result_list = simple_parallel(
    iteration_list = iteration_list,
    function = run_something,
    nproc = 4, )

  return result_list

# ADVANCED EXAMPLE

def run_advanced(info, fixed_value = None,
     log = sys.stdout):
  value = info.value  # we called with group_args object...get value here
  output_value = value * 2 + fixed_value
  print("Running with: %s  %s: %s" %(
    value, fixed_value, output_value), file = log)
  return output_value

def advanced_run_as_is(): # run in usual way
  iteration_list = []  # create an arbitrary iteration_list
  log = sys.stdout
  x = 3
  fixed_value = x

  for v in [5,7,9]:
    iteration_list.append(
      group_args(
       group_args_type = 'variable inputs for one run',
       value = v,  # could have more things here
       ))

  result_list = []
  for i in range(len(iteration_list)):
    result = run_advanced(iteration_list[i], fixed_value = fixed_value)
    result_list.append(result)
  return result_list

def advanced_run_parallel(): # run in parallel

  iteration_list = []  # create an arbitrary iteration_list
  log = sys.stdout
  x = 3
  fixed_value = x

  for v in [5,7,9]:
    iteration_list.append(
      group_args(
       group_args_type = 'variable inputs for one run',
       value = v,  # could have more things here
       ))

  from libtbx.easy_mp import simple_parallel
  result_list = simple_parallel(
    iteration_list = iteration_list, # list of varying inputs
    function = run_advanced,  # function to run
    nproc = 1,   # number of processors
    fixed_value = fixed_value, # any number of keyword arguments allowed
    )

  return result_list


def tst_01():
  print("\nRunning simple example of simple_parallel")
  result_list = run_as_is()
  print(result_list)
  assert result_list == [10, 14, 18]

  result_list = run_parallel() # prints [10, 14, 18]
  print(result_list)
  assert result_list == [10, 14, 18]


def tst_02():
  print("\nRunning advanced example of simple_parallel")
  result_list = advanced_run_as_is()
  print(result_list)
  assert result_list == [13, 17, 21]

  result_list = advanced_run_parallel() # prints [10, 14, 18]
  print(result_list)
  assert result_list == [13, 17, 21]


if __name__=="__main__":
  tst_01()
  tst_02()
  print("OK")

