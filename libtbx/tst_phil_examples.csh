#! /bin/csh -fe

libtbx.extract_code_from_txt "`libtbx.show_dist_paths libtbx`"/phil/doc.txt
libtbx.python libtbx_phil_examples.py

echo "OK"
