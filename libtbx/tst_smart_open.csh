#! /bin/csh -fe

set PHENIX_REGRESSION_DIR="`libtbx.find_in_repositories phenix_regression`"

libtbx.python "`libtbx.show_dist_paths libtbx`"/smart_open.py "$PHENIX_REGRESSION_DIR"/smart_open/plain_text
libtbx.python "`libtbx.show_dist_paths libtbx`"/smart_open.py "$PHENIX_REGRESSION_DIR"/smart_open/gzipped.gz
libtbx.python "`libtbx.show_dist_paths libtbx`"/smart_open.py "$PHENIX_REGRESSION_DIR"/smart_open/compressed.Z

echo "OK"
