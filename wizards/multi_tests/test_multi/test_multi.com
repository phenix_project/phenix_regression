#!/bin/csh  -f
#$ -cwd
#
setenv LIBTBX_FULL_TESTING
echo "----------------------------------------------------- "
echo "test_multi: multi-crystal averaging"
#
#
phenix.multi_crystal_average test_multi.eff debug=True
