from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_maps")
  print("")
  log_file=os.path.join(os.getcwd(),"test_maps.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  cmds='''symfile p21.sym
CELL 25.0 25. 25. 90 100. 90
resolution 2.49 20.0
infile scratch_fc_native.fmt
outfile scratch_fc_native.drg
ftob
infile scratch_fc_native.drg
ncolf 1
ncolphi 2
natfourier
fftfile fourier.fft
fftgrid 0 6 6 0 12 12 0  81 81
maps
peaksearch
infile scratch_fc_native.drg
ncolf 1
ncolphi 2
natfourier
fftgrid 0 2 2 0 8 8 0  16  16
fftfile fourier1.fft
maps
peaksearch
fftfile fourier.fft          ! input fft file
mapviewfile fourier.mapview  ! output mapviewfile for PHASES
fourier                      ! this is a fourier, not a patterson
bossgrid 0 100 0 10 0 10
ffttomapview
infile scratch_fc_native.drg
ncolpatt 1
ncolpattsig 2
PATTERSON               ! map type is patterson
FFTFILE ha.patt         ! output fft goes to ha.patt
MAPS                    ! calculate the map
infile scratch_fc_native.drg
ncolpatt 1
ncolpattsig 2
ORIGIN_REMOVED_PATT
FFTFILE ha.patt         ! output fft goes to ha.patt
MAPS                    ! calculate the map
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.solve', cmds,'phenix.solve.log',None,None,None)
  print(open('phenix.solve.log').read(), file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_maps',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['solve_tests','test_maps']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
