#!/bin/csh -ef
#$ -cwd
phenix.solve<<EOD|grep -v ' [3456] '  # get rid of a couple order changes
 symfile p21.sym         
 CELL 25.0 25. 25. 90 100. 90            
 resolution 2.49 20.0             
 infile scratch_fc_native.fmt
 outfile scratch_fc_native.drg
 ftob
 infile scratch_fc_native.drg            
 ncolf 1         
 ncolphi 2               
 natfourier
 fftfile fourier.fft   
 fftgrid 0 6 6 0 12 12 0  81 81
 maps
 peaksearch

 infile scratch_fc_native.drg            
 ncolf 1         
 ncolphi 2               
 natfourier
 fftgrid 0 2 2 0 8 8 0  16  16 
 fftfile fourier1.fft   
 maps
 peaksearch

fftfile fourier.fft          ! input fft file
mapviewfile fourier.mapview  ! output mapviewfile for PHASES
fourier                      ! this is a fourier, not a patterson
bossgrid 0 100 0 10 0 10
ffttomapview
infile scratch_fc_native.drg
ncolpatt 1
ncolpattsig 2
PATTERSON               ! map type is patterson
FFTFILE ha.patt         ! output fft goes to ha.patt
MAPS                    ! calculate the map
infile scratch_fc_native.drg
ncolpatt 1
ncolpattsig 2
ORIGIN_REMOVED_PATT
FFTFILE ha.patt         ! output fft goes to ha.patt
MAPS                    ! calculate the map
EOD
