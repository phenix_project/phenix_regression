from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_scale")
  print("")
  log_file=os.path.join(os.getcwd(),"test_scale.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  cmds='''SOLVEFILE dataset_1_scale.log
RESOLUTION 1000.0 2.5
CELL  25.0 25.0 25.0 90.0 100.0 90.0
symfile p21.sym
readdenzo
premerged
require_nat
ratio_out 3.0
ikeepflag 1
id_scale_ref 1
projectname project
crystalname crystal
datasetname dataset
MAD_ATOM Se
LAMBDA 1
RAWMADFILE peak.sca
ATOMNAME Se
WAVELENGTH 0.9798
FPRIMV_MAD -8.0
FPRPRV_MAD 4.5
LAMBDA 2
RAWMADFILE inf.sca
ATOMNAME Se
WAVELENGTH 0.9792
FPRIMV_MAD -9.0
FPRPRV_MAD 1.5
NRES 7
NANOMALOUS 2
SKIP_SOLVE
SCALE_MAD
ANALYZE_MAD
SOLVE
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.solve', cmds,'phenix.solve.log',None,None,None)
  print(open('phenix.solve.log').read(), file=log)
  print(open('dataset_1_scale.log').read(), file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_scale',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['solve_tests','test_scale']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
