#!/bin/csh -ef
#$ -cwd
phenix.solve<<EOD
SOLVEFILE dataset_1_scale.log                                                  
RESOLUTION 1000.0 2.5                                                          
CELL  25.0 25.0 25.0 90.0 100.0 90.0                                           
symfile p21.sym
readdenzo                                                                      
premerged                                                                      
require_nat                                                                    
                                                                               
ratio_out 3.0                                                                  
                                                                               
ikeepflag 1                                                                    
                                                                               
id_scale_ref 1                                                                 
projectname project                                                            
crystalname crystal                                                            
datasetname dataset                                                            
                                                                               
MAD_ATOM Se                                                                    
LAMBDA 1                                                                       
RAWMADFILE peak.sca                                                            
ATOMNAME Se                                                                    
WAVELENGTH 0.9798                                                              
FPRIMV_MAD -8.0                                                                
FPRPRV_MAD 4.5                                                                 
LAMBDA 2                                                                       
RAWMADFILE inf.sca                                                             
ATOMNAME Se                                                                    
WAVELENGTH 0.9792                                                              
FPRIMV_MAD -9.0                                                                
FPRPRV_MAD 1.5                                                                 
NRES 7                                                                         
NANOMALOUS 2                                                                   
SKIP_SOLVE                                                                     
SCALE_MAD                                                                      
ANALYZE_MAD                                                                    
SOLVE    
EOD
cat dataset_1_scale.log
