#!/bin/csh -ef
#$ -cwd
phenix.solve<<EOD
resolution 20 4.5
cell 35 35 35 90 90 90
symfile p21.sym

coordinatefile coords.pdb 
percent_error 10
iranseed -124093
OVERWRITE                               ! overwrite duplicate file names without
                                        !     asking
logfile generate.logfile
solvefile generate.prt
!
deriv 1                               ! enter parameters for deriv 1
!
inano                                 ! use anom diffs
atom hg
occ 0.8 
bvalue 40. 
xyz -0.620932 -0.0765346 -0.637333
atom hg
occ 0.4 
bvalue 25. 
xyz -0.315098 -0.512727 -0.664318
!
deriv 2
inano
atom hg
occ  0.8 
bvalue 30. 
xyz 0.620932 0.5765346 0.337333
atom hg
occ 0.6 
bvalue 25. 
xyz 0.515098 0.212727 0.364318

GENERATE_MIR                            ! generate the MIR dataset now.
!
EOD
