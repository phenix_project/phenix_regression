from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_generate_mir")
  print("")
  log_file=os.path.join(os.getcwd(),"test_generate_mir.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  cmds='''resolution 20 4.5
cell 35 35 35 90 90 90
symfile p21.sym
coordinatefile coords.pdb
percent_error 10
iranseed -124093
OVERWRITE                               ! overwrite duplicate file names without
!     asking
logfile generate.logfile
solvefile generate.prt
!
deriv 1                               ! enter parameters for deriv 1
!
inano                                 ! use anom diffs
atom hg
occ 0.8
bvalue 40.
xyz -0.620932 -0.0765346 -0.637333
atom hg
occ 0.4
bvalue 25.
xyz -0.315098 -0.512727 -0.664318
!
deriv 2
inano
atom hg
occ  0.8
bvalue 30.
xyz 0.620932 0.5765346 0.337333
atom hg
occ 0.6
bvalue 25.
xyz 0.515098 0.212727 0.364318
GENERATE_MIR                            ! generate the MIR dataset now.
!
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.solve', cmds,'phenix.solve.log',None,None,None)
  print(open('phenix.solve.log').read(), file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_generate_mir',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['solve_tests','test_generate_mir']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
