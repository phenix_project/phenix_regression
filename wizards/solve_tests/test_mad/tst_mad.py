from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_mad")
  print("")
  log_file=os.path.join(os.getcwd(),"test_mad.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  cmds='''SOLVEFILE solve_1.prt
HAPDBFILE ha_1.pdb
HKLOUT solve_1.mtz
RESOLUTION 1000.0 2.5
res_phase 2.5
CELL  25.0 25.0 25.0 90.0 100.0 90.0
symfile p21.sym
infile mad_fbar.fmt
outfile mad_fbar.scl
ftob
infile mad_fpfm.fmt
outfile mad_fpfm.scl
ftob
readdenzo
premerged
analyze_solve
no_patt
no_fourier
no_native
require_nat
ratio_out 3.0
ikeepflag 1
id_scale_ref 1
projectname project
crystalname crystal
datasetname dataset
MAD_ATOM Se
LAMBDA 1
RAWMADFILE peak.sca
ATOMNAME Se
WAVELENGTH 0.9798
FPRIMV_MAD -8.000098
FPRPRV_MAD 4.784209
XYZ   -0.452255   0.003242  -0.383497
XYZ   -0.412927  -0.672010  -0.519344
LAMBDA 2
RAWMADFILE inf.sca
ATOMNAME Se
WAVELENGTH 0.9792
FPRIMV_MAD -9.00009
FPRPRV_MAD 4.751194
XYZ   -0.452255   0.003242  -0.383497
XYZ   -0.412927  -0.672010  -0.519344
NRES 7
NANOMALOUS 2
ANALYZE_MAD
SOLVE
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.solve', cmds,'phenix.solve.log',None,None,None)
  print(open('phenix.solve.log').read(), file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_mad',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['solve_tests','test_mad']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
