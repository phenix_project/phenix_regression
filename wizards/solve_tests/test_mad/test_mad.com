#!/bin/csh -ef
#$ -cwd
phenix.solve<<EOD
SOLVEFILE solve_1.prt    
HAPDBFILE ha_1.pdb     
HKLOUT solve_1.mtz     
RESOLUTION 1000.0 2.5    
res_phase 2.5     
CELL  25.0 25.0 25.0 90.0 100.0 90.0  
symfile p21.sym

infile mad_fbar.fmt
outfile mad_fbar.scl
ftob
infile mad_fpfm.fmt
outfile mad_fpfm.scl
ftob


readdenzo      
premerged      
analyze_solve     
no_patt   
no_fourier     
no_native      
require_nat    
ratio_out 3.0     
ikeepflag 1    
id_scale_ref 1      
projectname project    
crystalname crystal    
datasetname dataset    
MAD_ATOM Se    
LAMBDA 1    
RAWMADFILE peak.sca    
ATOMNAME Se    
WAVELENGTH 0.9798   
FPRIMV_MAD -8.000098     
FPRPRV_MAD 4.784209    
XYZ   -0.452255   0.003242  -0.383497    
XYZ   -0.412927  -0.672010  -0.519344    
LAMBDA 2    
RAWMADFILE inf.sca     
ATOMNAME Se    
WAVELENGTH 0.9792   
FPRIMV_MAD -9.00009    
FPRPRV_MAD 4.751194    
XYZ   -0.452255   0.003242  -0.383497    
XYZ   -0.412927  -0.672010  -0.519344    
NRES 7    
NANOMALOUS 2   
ANALYZE_MAD    
SOLVE     
EOD
