#!/bin/csh -ef
#$ -cwd
phenix.solve<<EOD
 symfile p21.sym         
 CELL 25.0 25. 25. 90 100. 90            
 resolution 2.5 20.0             
 test_extra increase
 quit
EOD
phenix.solve<<EOD
 symfile p21.sym         
 CELL 25.0 25. 25. 90 100. 90            
 resolution 2.5 20.0             
 test_extra redimension
 quit
EOD
phenix.solve<<EOD
 symfile p21.sym         
 CELL 25.0 25. 25. 90 100. 90            
 resolution 2.5 20.0             
 test_extra nothing
 quit
EOD

