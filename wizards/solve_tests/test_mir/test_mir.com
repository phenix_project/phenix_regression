#!/bin/csh -ef
#$ -cwd
phenix.solve<<EOD
resolution 20 4.5
cell 35 35 35 90 90 90
symfile p21.sym

inhend 1
ihassplist
readformatted        ! readformatted/readdenzo/readtrek/readccp4_unmerged
premerged            ! premerged/ unmerged
read_intensities     ! read_intensities/read_amplitudes
fixscattfactors      ! fixscattfactors/refscattfactors

rawnativefile native.formatted ! native data H K L Iobs Sigma usually
                          ! NOTE: all datafiles must be in the same format

derivative 1                   ! about to enter information on derivative #1
label deriv 1 HG               ! a label for this deriv
atom hg

rawderivfile der1.formatted !  derivative data
inano             ! You need to tell it if anomalous diffs are to be used
noanorefine       ! use anomalous differences in phasing
                  ! but not refinement (best option for MIR)
nsolsite_deriv 2         ! max 2 sites this deriv

derivative 2
label deriv 2 also hg
atom hg

inano
noanorefine                     ! use anomalous differences in phasing
                                ! but not refinement (best option for MIR)
rawderivfile der2.formatted ! the derivative data is in this file


acceptance 0.35                 ! accept a new site if it has a
                                ! peak height about 1/3 of avg or more
nsolsite 2                      ! number of sites per deriv
                                ! (use nsolsite_deriv to set individual values) 
SCALE_NATIVE                    ! scale the native dataset
SCALE_MIR                       ! scale the derivs to the native
ANALYZE_MIR                     ! analyze this mir data and set up for SOLVE
SOLVE
EOD
#
