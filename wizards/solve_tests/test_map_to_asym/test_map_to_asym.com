#!/bin/csh -ef
#$ -cwd
phenix.solve<<EOD
 symfile p21.sym         
 CELL 25.0 25. 25. 90 100. 90            
 resolution 2.5 20.0             

infile(1) coords.pdb ! input file with coordinates to be examined
outfile asym.pdb
maptoasym

infile(1) coords.pdb
infile(2) coords1.pdb
outfile out.pdb
maptoobject

EOD
cat out.pdb
cat asym.pdb
