#!/bin/csh -ef
#$ -cwd
phenix.solve<<EOD
SOLVEFILE solve_1.prt    
HAPDBFILE ha_1.pdb     
HKLOUT solve_1.mtz     
RESOLUTION 1000.0 2.5    
res_phase 2.5     
readdenzo      
premerged      
analyze_solve     
no_patt   
no_fourier     
no_native      
require_nat    
ratio_out 3.0     
ikeepflag 1    
id_scale_ref 1      
projectname project    
crystalname crystal    
datasetname dataset    

labin FPH1=FP SIGFPH1=SIGFP DPH1=FP SIGDPH1=SIGFP
labin FPH2=FP SIGFPH2=SIGFP DPH2=FP SIGDPH2=SIGFP
hklin mad.mtz

MAD_ATOM Se    
LAMBDA 1    
RAWMADFILE peak.sca    
ATOMNAME Se    
WAVELENGTH 0.9798   
FPRIMV_MAD -8.000098     
FPRPRV_MAD 4.784209    
XYZ   -0.452255   0.003242  -0.383497    
XYZ   -0.412927  -0.672010  -0.519344    
LAMBDA 2    
RAWMADFILE inf.sca     
ATOMNAME Se    
WAVELENGTH 0.9792   
FPRIMV_MAD -9.00009    
FPRPRV_MAD 4.751194    
XYZ   -0.452255   0.003242  -0.383497    
XYZ   -0.412927  -0.672010  -0.519344    
NRES 7    
NANOMALOUS 2   
ANALYZE_MAD    
EOD
