#!/bin/csh -ef
#$ -cwd
phenix.solve<<EOD
checksolve     
SYMFILE i4.sym      
RESOLUTION 1000.0 3.5
CELL  113.949 113.949  32.474 90.000  90.000  90.00
readdenzo      
unmerged      
MAD_ATOM Se    
LAMBDA 1    
RAWMADFILE p9_se_w2.sca    
ATOMNAME Se    
FPRIMV_MAD -8.0     
FPRPRV_MAD 4.5      
NRES 45   
NANOMALOUS 4
quick
skip_solve
SAD    
EOD
