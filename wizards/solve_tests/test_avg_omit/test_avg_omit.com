#!/bin/csh -ef
#$ -cwd
phenix.solve<<EOD
 symfile p21.sym         
 CELL 25.0 25. 25. 90 100. 90            
 resolution 2.5 20.0             
 infile scratch_fc_native.fmt
 outfile scratch_fc_native.drg
 ftob

 infile scratch_fc_native.drg
 ncolf 1         
 ncolphi 2               
 natfourier
 fftfile fourier.fft   
 maps
avg_omit                ! call avg_omit.  It reads from standard input
2.0,                    !  XCUT = region around atoms to be excluded
2,                      ! # of maps to average
coords.pdb           
fourier.fft
coords.pdb
fourier.fft
average.fft

 infile average.fft
 peaksearch
EOD
