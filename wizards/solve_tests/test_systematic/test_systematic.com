#!/bin/csh -ef
#$ -cwd
phenix.solve<<EOD
 symfile p21.sym         
 CELL 25.0 25. 25. 90 100. 90            
 resolution 2.5 20.0             
 infile scratch_fc_native.fmt
 outfile scratch_fc_native.drg
 ftob
 infile scratch_fc_native.drg            
 nnatf 1
 nnats 2
 systematic
EOD
