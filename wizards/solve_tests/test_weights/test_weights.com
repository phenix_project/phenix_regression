#!/bin/csh -ef
#$ -cwd
phenix.solve<<EOD
 symfile p21.sym         
 CELL 25.0 25. 25. 90 100. 90            
 resolution 2.49 20.0             
 infile scratch_fc_native.fmt
 outfile scratch_fc_native.drg
 ftob
 infile scratch_fc_native.drg            
outfile weights.xplr
NCOLFOWT  1              !  column # for F of data
NCOLSWT   2              !  column # of sigma-obs
NCOLFC    2              !column # of Fcalc
NCOLRTEST  0             ! column # for RTEST indicator (0 if not present)
weights                  ! get Bayesian weights...
EOD
