#!/bin/csh -ef
#$ -cwd
phenix.solve<<EOD
 symfile p21.sym         
 CELL 25.0 25. 25. 90 100. 90            
 resolution 2.5 20.0             
 infile scratch_fc_native.fmt
 outfile scratch_fc_native.drg
 ftob
 infile scratch_fc_native.drg            
 ncolf 1         
 ncolphi 2               
 natfourier
 fftfile fourier.fft   
 maps
EOD
cp fourier.fft fourier1.fft
phenix.solve<<EOD
 symfile p21.sym         
 CELL 25.0 25. 25. 90 100. 90            
 resolution 2.5 20.0             
 infile(1) fourier.fft
 infile(2) fourier1.fft
 cc
EOD
