from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_cc")
  print("")
  log_file=os.path.join(os.getcwd(),"test_cc.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  cmds='''symfile p21.sym
CELL 25.0 25. 25. 90 100. 90
resolution 2.5 20.0
infile scratch_fc_native.fmt
outfile scratch_fc_native.drg
ftob
infile scratch_fc_native.drg
ncolf 1
ncolphi 2
natfourier
fftfile fourier.fft
maps
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.solve', cmds,'phenix.solve.log',None,None,None)
  print(open('phenix.solve.log').read(), file=log)

  import shutil
  shutil.copyfile('fourier.fft','fourier1.fft')

  cmds='''symfile p21.sym
CELL 25.0 25. 25. 90 100. 90
resolution 2.5 20.0
infile(1) fourier.fft
infile(2) fourier1.fft
cc
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.solve', cmds,'phenix.solve.log',None,None,None)
  print(open('phenix.solve.log').read(), file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_cc',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['solve_tests','test_cc']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
