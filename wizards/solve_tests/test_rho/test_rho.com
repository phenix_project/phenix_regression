#!/bin/csh -ef
#$ -cwd
phenix.solve<<EOD
 symfile p21.sym         
 CELL 25.0 25. 25. 90 100. 90            
 resolution 2.5 20.0             
 infile scratch_fc_native.fmt
 outfile scratch_fc_native.drg
 ftob

 infile scratch_fc_native.drg
 ncolf 1
 ncolphi 2
 natfourier
 fftfile fourier.fft
 maps

infile(1) coords.pdb ! input file with coordinates to be examined
fftfile fourier.fft ! map file
rho ! interpolate value of fft at all coords in model

EOD
