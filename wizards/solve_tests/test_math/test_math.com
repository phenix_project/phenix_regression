#!/bin/csh -ef
#$ -cwd
phenix.solve<<EOD
 symfile p21.sym         
 CELL 25.0 25. 25. 90 100. 90            
 resolution 2.5 20.0             
 infile scratch_fc_native.fmt
 outfile scratch_fc_native.drg
 ftob
 infile scratch_fc_native.drg            
 outfile ab.drg
 F_PHI_TO_A_B  1 2 
 math
 view

 infile ab.drg
 outfile fphi.drg
 a_b_to_f_phi 1 2
 math
 view

 infile scratch_fc_native.drg
 outfile err.drg
 FOBS_SIG_FROM_F_ERR 1 0.5
 math
 view

 infile ab.drg
 outfile sum.drg
 sum 1 2
 math
 view

 infile ab.drg
 outfile phases.drg
 ncolfa 1
 ncolfb 2
 getphases
 view

 history
EOD
