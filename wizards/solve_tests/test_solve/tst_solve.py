from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_solve")
  print("")
  log_file=os.path.join(os.getcwd(),"test_solve.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  cmds='''checksolve
RESOLUTION 1000.0 3.5
CELL  35.0 35.0 35.0 90.0 100.0 90.0
symfile p1.sym
infile scratch_fc_native.fmt
outfile scratch_fc_native.drg
ftob
CELL 25.0 25. 25. 90 100. 90
resolution 2.5 20.0
infile scratch_fc_native.drg
ncolf 1
ncolphi 2
natfourier
fftfile fourier.fft
maps
comparisonfile fourier.fft
xyz_compare_file check.xyz
checksolve
LOGFILE solve.log
SOLVEFILE solve.prt
readdenzo
premerged
phases_labin FC=FP PHIC=PHIC FOM=FOM
MAD_ATOM Se
LAMBDA 1
RAWMADFILE w1_rna.sca
ATOMNAME Se
FPRIMV_MAD -8.0
FPRPRV_MAD 4.5
NRES 45
NANOMALOUS 2
SAD
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.solve', cmds,'phenix.solve.log',None,None,None)
  print(open('phenix.solve.log').read(), file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_solve',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['solve_tests','test_solve']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
