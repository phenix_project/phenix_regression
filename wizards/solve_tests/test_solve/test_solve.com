#!/bin/csh -ef
#$ -cwd
phenix.solve<<EOD
checksolve     
RESOLUTION 1000.0 3.5
CELL  35.0 35.0 35.0 90.0 100.0 90.0  
symfile p1.sym
 infile scratch_fc_native.fmt
 outfile scratch_fc_native.drg
 ftob
 CELL 25.0 25. 25. 90 100. 90
 resolution 2.5 20.0
 infile scratch_fc_native.drg
 ncolf 1
 ncolphi 2
 natfourier
 fftfile fourier.fft
 maps

comparisonfile fourier.fft
xyz_compare_file check.xyz    
checksolve
LOGFILE solve.log   
SOLVEFILE solve.prt    
readdenzo      
premerged      
phases_labin FC=FP PHIC=PHIC FOM=FOM  
MAD_ATOM Se    
LAMBDA 1    
RAWMADFILE w1_rna.sca    
ATOMNAME Se    
FPRIMV_MAD -8.0     
FPRPRV_MAD 4.5      
NRES 45   
NANOMALOUS 2   
SAD    
EOD
