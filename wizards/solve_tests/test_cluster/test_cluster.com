#!/bin/csh -ef
#$ -cwd
phenix.solve<<EOD
SOLVEFILE solve_1.prt    
HAPDBFILE ha_1.pdb     
HKLOUT solve_1.mtz     
RESOLUTION 1000.0 2.49
res_phase 2.49
CELL  25.0 25.0 25.0 90.0 100.0 90.0  
symfile p21.sym

infile mad_fbar.fmt
outfile mad_fbar.scl
ftob
infile mad_fpfm.fmt
outfile mad_fpfm.scl
ftob


readdenzo      
premerged      
analyze_solve     
addsolve
require_nat    
ratio_out 3.0     
ikeepflag 1    
id_scale_ref 1      
projectname project    
crystalname crystal    
datasetname dataset    
newatomtype wclu

fprimv 100.0    ! fprime value, to be multiplied by clus_fp form factor
fprprv 50.0   ! fprime value, to be multiplied by clus_fpp form factor

clus_aval 2093 5109.4 -1197.1 5254.3   ! form factors for f_o scattering
clus_bval 509.3 -37.8 849.4 108.5
clus_cval 184 30 1.2

clus_fp_aval 0.185886 0.453782 -0.10632 0.466651  ! form factors for fprime
clus_fp_bval 509.3 -37.8 849.4 108.5
clus_fp_cval 184 30 1.2

clus_fpp_aval 0.185886 0.453782 -0.10632 0.466651  !form factors, fdoubleprime
clus_fpp_bval 509.3 -37.8 849.4 108.5
clus_fpp_cval 184 30 1.2

plot_formfactors wclu

MAD_ATOM wclu
LAMBDA 1    
RAWMADFILE peak.sca    
ATOMNAME wclu    
WAVELENGTH 0.9798   
FPRIMV_MAD -8.000098     
FPRPRV_MAD 4.784209    
XYZ   -0.452255   0.003242  -0.383497    
XYZ   -0.412927  -0.672010  -0.519344    
LAMBDA 2    
RAWMADFILE inf.sca     
ATOMNAME wclu    
WAVELENGTH 0.9792   
FPRIMV_MAD -9.00009    
FPRPRV_MAD 4.751194    
XYZ   -0.452255   0.003242  -0.383497    
XYZ   -0.412927  -0.672010  -0.519344    
NRES 7    
NANOMALOUS 2   
ANALYZE_MAD    
quick
SOLVE     
EOD
