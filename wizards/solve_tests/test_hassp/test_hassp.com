#!/bin/csh -ef
#$ -cwd
phenix.solve<<EOD
 symfile p21.sym         
 CELL 25.0 25. 25. 90 100. 90            
 resolution 2.5 20.0             
 infile scratch_fc_native.fmt
 outfile scratch_fc_native.drg
 ftob
infile scratch_fc_native.drg
ncolpatt 1
ncolpattsig 2
PATTERSON               ! map type is patterson
FFTFILE ha.patt         ! output fft goes to ha.patt
MAPS                    ! calculate the map
infile ha.patt
ihassptype -6
trialsite 0.23 0.11 0.94
uvw_remove .15 .15 .15
hassp

infile ha.patt
ihassptype -4
trialsite 0.23 0.11 0.94
trialsite 0.15 0.21 0.54
hassp
EOD
