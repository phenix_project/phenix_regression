#!/bin/csh -ef
#$ -cwd
phenix.solve<<EOD
 symfile p21.sym         
 CELL 25.0 25. 25. 90 100. 90            
 resolution 2.5 20.0             
 ? symfile
 ? cell
 ? resolution
 ? all
 @ test.commands
 preferences
EOD
