#!/bin/csh -ef
#$ -cwd
phenix.solve<<EOD
checksolve     
SYMFILE p1.sym      
RESOLUTION 1000.0 3.5
CELL  35.0 35.0 35.0 90.0 100.0 90.0  
pdb_xyz_in ha.pdb
EOD
