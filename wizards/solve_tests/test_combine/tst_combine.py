from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_combine")
  print("")
  log_file=os.path.join(os.getcwd(),"test_combine.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  cmds='''checksolve
SYMFILE p1.sym
RESOLUTION 1000.0 3.5
CELL  35.0 35.0 35.0 90.0 100.0 90.0
readdenzo
premerged
MAD_ATOM Se
LAMBDA 1
RAWMADFILE w1_rna.sca
ATOMNAME Se
FPRIMV_MAD -8.0
FPRPRV_MAD 4.5
LAMBDA 2
RAWMADFILE w1_rna.sca
ATOMNAME Se
FPRIMV_MAD -9.0
FPRPRV_MAD 3.5
scale_mad
analyze_mad
new_dataset
MAD_ATOM Se
LAMBDA 1
RAWMADFILE w1_rna.sca
ATOMNAME Se
FPRIMV_MAD -8.2
FPRPRV_MAD 4.5
LAMBDA 2
RAWMADFILE w1_rna.sca
ATOMNAME Se
FPRIMV_MAD -9.2
FPRPRV_MAD 3.5
NRES 45
NANOMALOUS 2
scale_mad
analyze_mad
combine_all_data
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.solve', cmds,'phenix.solve.log',None,None,None)
  print(open('phenix.solve.log').read(), file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_combine',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['solve_tests','test_combine']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
