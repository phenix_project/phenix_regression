#!/bin/csh -ef
#$ -cwd
phenix.solve<<EOD
checksolve     
SYMFILE p1.sym      
RESOLUTION 1000.0 3.5
CELL  35.0 35.0 35.0 90.0 100.0 90.0  
readdenzo      
premerged      
MAD_ATOM Se    
LAMBDA 1    
RAWMADFILE w1_rna.sca    
ATOMNAME Se    
FPRIMV_MAD -8.0     
FPRPRV_MAD 4.5      
LAMBDA 2
RAWMADFILE w1_rna.sca    
ATOMNAME Se    
FPRIMV_MAD -9.0     
FPRPRV_MAD 3.5      
scale_mad
analyze_mad
new_dataset
MAD_ATOM Se    
LAMBDA 1    
RAWMADFILE w1_rna.sca    
ATOMNAME Se    
FPRIMV_MAD -8.2     
FPRPRV_MAD 4.5      
LAMBDA 2
RAWMADFILE w1_rna.sca    
ATOMNAME Se    
FPRIMV_MAD -9.2     
FPRPRV_MAD 3.5      
NRES 45   
NANOMALOUS 2   
scale_mad
analyze_mad
combine_all_data
EOD
