#!/bin/csh -ef
#$ -cwd
phenix.solve<<EOD
resolution 20 4.5
cell 35 35 35 90 90 90
symfile p21.sym

labin FP=FP SIGFP=SIGFP FPH1=FP SIGFPH1=SIGFP DPH1=FP SIGDPH1=SIGFP
labin FPH2=FP SIGFPH2=SIGFP DPH2=FP SIGDPH2=SIGFP
hklin mir.mtz

derivative 1                   ! about to enter information on derivative #1
label deriv 1 HG               ! a label for this deriv
atom hg

inano             ! You need to tell it if anomalous diffs are to be used
noanorefine       ! use anomalous differences in phasing
                  ! but not refinement (best option for MIR)
nsolsite_deriv 2         ! max 2 sites this deriv

derivative 2
label deriv 2 also hg
atom hg

inano
noanorefine                     ! use anomalous differences in phasing
                                ! but not refinement (best option for MIR)


acceptance 0.35                 ! accept a new site if it has a
                                ! peak height about 1/3 of avg or more
nsolsite 2                      ! number of sites per deriv
                                ! (use nsolsite_deriv to set individual values) 
SCALE_NATIVE                    ! scale the native dataset
SCALE_MIR                       ! scale the derivs to the native
EOD
#
