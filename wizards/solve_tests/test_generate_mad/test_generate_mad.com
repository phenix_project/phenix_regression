#!/bin/csh -ef
#$ -cwd
phenix.solve<<EOD
resolution 20 4.5
cell 35 35 35 90 90 90
symfile p21.sym
percent_error 0.5
coordinatefile coords.pdb
iranseed -199753
logfile generate.logfile
solvefile generate.prt

mad_atom se                              ! define the scattering factors...
!
lambda 1
label set 1 with 2 se atoms, lambda 1
wavelength 0.9782             ! wavelength value
fprimv_mad  -10              ! f' value at this wavelength
fprprv_mad  3  
ATOMNAME Se
xyz  0.44 0.16 0.38 
occ 1.5
bvalue 20.
ATOMNAME Se
xyz  0.23 0.45 0.165 
occ 1.5
bvalue 20. 
!
lambda 2
wavelength 0.977865
fprimv_mad  -7.5
fprprv_mad  5

lambda 3
wavelength 0.8856
fprimv_mad  -2
fprprv_mad  3.5
!
GENERATE_MAD                            ! generate the MAD dataset now.
!
EOD
