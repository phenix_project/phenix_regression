from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_generate_mad")
  print("")
  log_file=os.path.join(os.getcwd(),"test_generate_mad.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  cmds='''resolution 20 4.5
cell 35 35 35 90 90 90
symfile p21.sym
percent_error 0.5
coordinatefile coords.pdb
iranseed -199753
logfile generate.logfile
solvefile generate.prt
mad_atom se                              ! define the scattering factors...
!
lambda 1
label set 1 with 2 se atoms, lambda 1
wavelength 0.9782             ! wavelength value
fprimv_mad  -10              ! f' value at this wavelength
fprprv_mad  3
ATOMNAME Se
xyz  0.44 0.16 0.38
occ 1.5
bvalue 20.
ATOMNAME Se
xyz  0.23 0.45 0.165
occ 1.5
bvalue 20.
!
lambda 2
wavelength 0.977865
fprimv_mad  -7.5
fprprv_mad  5
lambda 3
wavelength 0.8856
fprimv_mad  -2
fprprv_mad  3.5
!
GENERATE_MAD                            ! generate the MAD dataset now.
!
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.solve', cmds,'phenix.solve.log',None,None,None)
  print(open('phenix.solve.log').read(), file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_generate_mad',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['solve_tests','test_generate_mad']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
