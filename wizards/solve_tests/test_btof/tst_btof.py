from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_btof")
  print("")
  log_file=os.path.join(os.getcwd(),"test_btof.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  cmds='''access_file mirima.dat
symfile p21.sym
CELL 25.0 25. 25. 90 100. 90
resolution 2.5 20.0
infile scratch_fc_native.fmt
outfile scratch_fc_native.drg
ftob
infile scratch_fc_native.drg
outfile scratch_fc_native.fmt
btof
infile scratch_fc_native.drg
outfile scratch_fc_native.xplor
drgtoxplor
infile scratch_fc_native.drg
outfile scratch_fc_native.export
export
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.solve', cmds,'phenix.solve.log',None,None,None)
  print(open('phenix.solve.log').read(), file=log)

  for line in open('scratch_fc_native.fmt').readlines()[:20]:
    print(line, file=log)

  for line in open('scratch_fc_native.xplor').readlines()[:20]:
    print(line, file=log)

  for line in open('scratch_fc_native.export').readlines()[:20]:
    print(line, file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_btof',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['solve_tests','test_btof']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
