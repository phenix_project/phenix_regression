#!/bin/csh -ef
#$ -cwd
phenix.solve<<EOD
 access_file mirima.dat
 symfile p21.sym         
 CELL 25.0 25. 25. 90 100. 90            
 resolution 2.5 20.0             
 infile scratch_fc_native.fmt
 outfile scratch_fc_native.drg
 ftob
 infile scratch_fc_native.drg            
 outfile scratch_fc_native.fmt
 btof
 infile scratch_fc_native.drg            
 outfile scratch_fc_native.xplor
 drgtoxplor
 infile scratch_fc_native.drg            
 outfile scratch_fc_native.export
 export 
EOD
head -20 scratch_fc_native.fmt
head -20 scratch_fc_native.xplor
head -20 scratch_fc_native.export
