#!/bin/csh -ef
#$ -cwd
phenix.solve<<EOD
 symfile p21.sym         
 CELL 25.0 25. 25. 90 100. 90            
 resolution 2.5 20.0             
 infile scratch_fc_native.fmt
 outfile scratch_fc_native.drg
 ftob
 infile scratch_fc_native.drg            
outfile fdiff.drg           ! output file with Fdiff, sigma,weight, RTEST
ncolfowt 1                  ! column # for WT Fobs
ncolswt  2                  ! column # for sigma of WT Fobs
ncolfc   2                  ! column # for WT Fcalc
ncolfomut 1                 ! column # for Mutant Fobs
ncolsmut 2                ! column # for sigma of mutant Fobs
ncolfcmut   2                  ! column # for WT Fcalc
ncolrtest 2                 ! column # for RTEST indicator
fdiff                       ! setup up difference refinement
EOD
