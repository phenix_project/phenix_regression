#!/bin/csh -ef
#$ -cwd
phenix.solve<<EOD
SYMFILE p1.sym      
RESOLUTION 1000.0 3.5
CELL  35.0 35.0 35.0 90.0 100.0 90.0  
 infile(1) start.xyz
 outfile check.XYZ
 fract_to_cart
 infile check.XYZ
 outfile check.out
 cart_to_fract
EOD
cat check.XYZ
cat check.out
