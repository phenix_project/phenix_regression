#!/bin/csh -ef
#$ -cwd
phenix.solve<<EOD
checksolve     
xyz_compare_file check.xyz    
SYMFILE p1.sym      
LOGFILE solve.log   
SOLVEFILE solve.prt    
HKLOUT dummy.mtz    
RESOLUTION 1000.0 2.5    
CELL  35.0 35.0 35.0 90.0 100.0 90.0  
FIXSCATTFACTORS     
readdenzo      
premerged      
no_sim    
skip_madbst    
skip_hl   
phases_labin FC=FP PHIC=PHIC FOM=FOM  
phases_mtz perfect_rna.mtz    
get_sites_only      
require_nat    
ratio_out 3.0     
ikeepflag 1    
id_scale_ref 1      
projectname project    
crystalname crystal    
datasetname dataset    
MAD_ATOM Se    
LAMBDA 1    
RAWMADFILE w1_rna.sca    
ATOMNAME Se    
FPRIMV_MAD -8.0     
FPRPRV_MAD 4.5      
NRES 45   
NANOMALOUS 2   
alias test_sad sad
test_SAD    
EOD
cat peaks_1.dat
