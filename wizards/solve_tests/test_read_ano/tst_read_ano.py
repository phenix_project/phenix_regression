from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_read_ano")
  print("")
  log_file=os.path.join(os.getcwd(),"test_read_ano.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  cmds='''checksolve
xyz_compare_file check.xyz
SYMFILE p1.sym
LOGFILE solve.log
SOLVEFILE solve.prt
HKLOUT dummy.mtz
RESOLUTION 1000.0 2.5
CELL  35.0 35.0 35.0 90.0 100.0 90.0
FIXSCATTFACTORS
readdenzo
premerged
no_sim
skip_madbst
skip_hl
phases_labin FC=FP PHIC=PHIC FOM=FOM
phases_mtz perfect_rna.mtz
get_sites_only
require_nat
ratio_out 3.0
ikeepflag 1
id_scale_ref 1
projectname project
crystalname crystal
datasetname dataset
MAD_ATOM Se
LAMBDA 1
RAWMADFILE w1_rna.sca
ATOMNAME Se
FPRIMV_MAD -8.0
FPRPRV_MAD 4.5
NRES 45
NANOMALOUS 2
alias test_sad sad
test_SAD
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.solve', cmds,'phenix.solve.log',None,None,None)
  print(open('phenix.solve.log').read(), file=log)
  print(open('peaks_1.dat').read(), file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_read_ano',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['solve_tests','test_read_ano']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
