from __future__ import division

def run(args):
  from phenix_regression.wizards.solve_tests.test_sym.tst_sym import run
  run()

if __name__=="__main__":
  run(sys.argv[1:])
