#!/bin/csh  -ef
#$ -cwd
#
setenv LIBTBX_FULL_TESTING
#
echo " "
echo "----------------------------------------------------- "
echo "find_ncs : simple_ncs_from_pdb"
#
 phenix.simple_ncs_from_pdb pdb_in = anb.pdb write_spec_files=True
cat anb_simple_ncs_from_pdb.ncs_spec

phenix.python test_extract_point_group.py
if ( $status )then
  echo "FAILED"
  exit 1
endif
