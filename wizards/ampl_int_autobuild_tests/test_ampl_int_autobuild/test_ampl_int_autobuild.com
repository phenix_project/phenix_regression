#!/bin/csh  -f
#$ -cwd
#
setenv LIBTBX_FULL_TESTING
#
if ($#argv > 0) then
  if ($argv[1] == "--help") then
    echo "test autobuild with intensities and amplitudes in MTZ file"
    exit 0
  endif
endif

echo " "
echo "----------------------------------------------------- "
echo "testing autobuild with intensities and amplitudes in MTZ file"
#
phenix.autobuild \
test_ampl_int_autobuild.mtz \
hires_file=test_ampl_int_autobuild.mtz \
refinement_file=test_ampl_int_autobuild.mtz \
test_ampl_int_autobuild.pdb seq_file=seq.dat rebuild_in_place=False debug=True refine=False n_cycle_rebuild_max=0 resolution=4 >& test_ampl_int_autobuild.log

if ( $status == 1) goto bad

grep -i "Input labels:" test_ampl_int_autobuild.log>test.dat

echo "Differences test.dat  from test_ampl_int_autobuild.dat : \
      `diff -b test.dat test_ampl_int_autobuild.dat`"
#
goto finished
bad:

    echo "*****************************************************"
    echo "Error:test failed to run "
    echo "*****************************************************"
    exit 1
  goto finished
endif
finished:
echo "OK"
echo "------------------------------------------------------------"
echo " "
exit 0
