from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_pdb_in")
  print("")
  log_file=os.path.join(os.getcwd(),"test_pdb_in.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  cmds='''hklin resolve_2.mtz
labin FP=FP PHIB=PHIM FOM=FOMM
resolution 500 4.0
local_pattern_setup
read_patterns
rad_pattern 1.5
n_group 1
pdb_in coords.pdb
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.resolve_pattern', cmds,'phenix.resolve_pattern.log',None,None,None)
  print(open('phenix.resolve_pattern.log').read(), file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_pdb_in',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['resolve_pattern_tests','test_pdb_in']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
