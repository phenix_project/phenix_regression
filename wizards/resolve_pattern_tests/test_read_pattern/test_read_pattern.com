#!/bin/csh -ef
#$ -cwd
phenix.resolve_pattern <<EOD|grep -i '[a-z]'
hklin resolve_2.mtz   
labin FP=FP PHIB=PHIM FOM=FOMM   
resolution 500 4.0
local_pattern_setup
read_patterns
rad_pattern 1.5
n_group 1
EOD
