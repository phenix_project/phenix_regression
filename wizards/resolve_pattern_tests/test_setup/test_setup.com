#!/bin/csh -ef
#$ -cwd
phenix.resolve_pattern <<EOD
hklin resolve_2.mtz   
labin FP=FP PHIB=PHIM FOM=FOMM   
resolution 500 3.5
local_pattern_setup
n_group 1
rad_pattern 1.5
EOD
cat box001.dat
head -50 dump.map
