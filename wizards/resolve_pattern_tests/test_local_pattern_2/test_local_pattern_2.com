#!/bin/csh -ef
#$ -cwd
phenix.resolve_pattern <<EOD|grep -i '[a-z]'
hklin resolve_2.mtz   
labin FP=FP PHIB=PHIM FOM=FOMM   
resolution 500 4.0
local_pattern_3 1 100.
read_patterns
rad_pattern 1.5
local_pattern_2
EOD
