#!/bin/csh -ef
#$ -cwd
phenix.resolve_pattern <<EOD > /dev/null
hklin resolve_2.mtz   
labin FP=FP PHIB=PHIM FOM=FOMM   
resolution 500 3.5
read_corr
local_pattern_setup
read_patterns
n_group 1
rad_pattern 1.5
EOD
cat box001.dat
cat box_asisbx014.dat
cat box_unique015.pdb
