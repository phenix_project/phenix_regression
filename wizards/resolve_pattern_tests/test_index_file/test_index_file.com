#!/bin/csh -ef
#$ -cwd
phenix.resolve_pattern <<EOD
hklin resolve_2.mtz   
labin FP=FP PHIB=PHIM FOM=FOMM   
resolution 500 3.0     
n_xyz 36 36 36      
recover_image     
cc_map_file dump.map     
cc_index_file_out index_file.txt
EOD
