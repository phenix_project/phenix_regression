#!/bin/csh -f
echo "Checking multiple input files..."
#
#ls -lt   map_coeffs.mtz  w1_c.sca  w1.sca w1_c_freer.mtz coords.pdb
#
echo "w1_c_freer.mtz : starting freeR flags from seed=2557"
phenix.reflection_file_converter w1_c_freer.mtz --label=TEST --cns=w1_c_freer.dump > /dev/null

foreach t ( "STD" "REF" "MAP_REF" "MAP")
echo "TYPE: $t"
if ( $t == "STD") then
set ref = " verbose=True "
set map = " verbose=True "
else if ( $t == "REF") then
set ref = "input_refinement_file=w1_c.sca"
set map = " verbose=True "
else if ( $t == "MAP") then
set ref = " verbose=True "
set map = "input_map_file=map_coeffs.mtz  "
else if ( $t == "MAP_REF") then
set ref = "input_refinement_file=w1_c.sca"
set map = "input_map_file=map_coeffs.mtz  "
endif
#
phenix.python<<EOD
import time
time.sleep(2.)
EOD
phenix.autobuild delete_runs=1-100 > /dev/null
####
phenix.python<<EOD
import time
time.sleep(2.)
EOD
echo "run1 : generate new freer flags with same seed=2557:"
phenix.autobuild remove_aniso=False \
    coords.pdb w1_c_freer.mtz n_cycle_rebuild_max=0 \
   nbatch=1 i_ran_seed=2557  \
    $ref $map  input_labels='FP SIGFP ' debug=true> run1.log

#
phenix.reflection_file_converter --label=Free \
 --cns=run1.dump AutoBuild_run_1_/exptl_fobs_phases_freeR_flags.mtz > /dev/null
####
#
echo "run2 : generate new freer flags with new seed=7121:"
phenix.autobuild remove_aniso=false \
   coords.pdb w1_c_freer.mtz n_cycle_rebuild_max=0  \
   nbatch=1 i_ran_seed=7121 \
    $map  input_labels='FP SIGFP '  debug=true> run2.log
#
phenix.reflection_file_converter --label=Free \
 --cns=run2.dump AutoBuild_run_2_/exptl_fobs_phases_freeR_flags.mtz  > /dev/null
####
#
echo "run3 : take w1_c_freer flags by default, different seed=7121:"
phenix.autobuild remove_aniso=false \
   coords.pdb w1_c_freer.mtz n_cycle_rebuild_max=0 \
   nbatch=1 i_ran_seed=7121  \
    $ref $map  \
    debug=true > run3.log
#
phenix.reflection_file_converter --label=Free \
--cns=run3.dump AutoBuild_run_3_/exptl_fobs_phases_freeR_flags.mtz  > /dev/null
echo "run4 : take w1_c_freer flags from hires file unless ref supplied, different seed=7121"
phenix.autobuild remove_aniso=false \
   coords.pdb hires_file=w1_c_freer.mtz \
      n_cycle_rebuild_max=0 data=w1_c.sca nbatch=1 i_ran_seed=7121  \
    $ref  $map  debug=true > run4.log
phenix.reflection_file_converter --label=Free --cns=run4.dump AutoBuild_run_4_/exptl_fobs_phases_freeR_flags.mtz  > /dev/null
#
sort w1_c_freer.dump.cns > cns.dat
foreach r (1 2 3 4)
sort run$r.dump.cns > tmp.dat
echo "diff run$r.dump.cns w1_c_freer.dump.cns |grep INDE |wc `diff -b tmp.dat cns.dat |grep INDE|wc`"
end
#
end
exit 0
