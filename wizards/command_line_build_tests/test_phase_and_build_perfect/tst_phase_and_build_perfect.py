from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os,sys
  print("Test script for test_phase_and_build_perfect")
  print("")
  log_file=os.path.join(os.getcwd(),"test_phase_and_build_perfect.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  local_log_file='test_phase_and_build_perfect.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.phase_and_build  import phase_and_build
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''data=perfect.mtz labin="FP=FP SIGFP=SIGFP PHIB=PHIM" map_coeffs=perfect.mtz seq.dat insert_helices=true quick=true labin_map_coeffs="FP=FWT PHIB=PHWT" rs_refine=False ncycle=0 macro_cycles_in_refine_before_merge=1 macro_cycles=1''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  phase_and_build(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)


  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_phase_and_build_perfect',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['command_line_build_tests','test_phase_and_build_perfect']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
