#!/bin/csh -ef
#$ -cwd
phenix.phase_and_build data=perfect.mtz labin="FP=FP SIGFP=SIGFP PHIB=PHIM" map_coeffs=perfect.mtz seq.dat insert_helices=true quick=true labin_map_coeffs="FP=FWT PHIB=PHWT" rs_refine=False ncycle=0 macro_cycles_in_refine_before_merge=1 macro_cycles=1
