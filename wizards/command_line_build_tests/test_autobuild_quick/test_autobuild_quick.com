#!/bin/csh -ef
#$ -cwd

phenix.autobuild super_quick=True bad_refine_data.mtz verybad.pdb super_quick=True skip_clash_guard=True>& should_stop.log
if ( $status )then
  echo "FAILED"
  exit 1
endif


phenix.autobuild super_quick=True seq_file=seq.dat data=native.sca map_file=refine_map_coeffs.mtz super_quick=true input_ha_file=ha.pdb number_of_parallel_models=2 n_cycle_rebuild_max=0
if ( $status )then
  echo "FAILED"
  exit 1
endif
phenix.autobuild super_quick=True coords.pdb data=native.sca map_file=refine_map_coeffs.mtz super_quick=true
if ( $status )then
  echo "FAILED"
  exit 1
endif
