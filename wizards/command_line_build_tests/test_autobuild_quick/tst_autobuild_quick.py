from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os,sys
  print("Test script for test_autobuild_quick")
  print("")
  log_file=os.path.join(os.getcwd(),"test_autobuild_quick.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  local_log_file='test_autobuild_quick.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''bad_refine_data.mtz verybad.pdb super_quick=True skip_clash_guard=True''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)



  local_log_file='test_autobuild_quick.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''seq_file=seq.dat data=native.sca map_file=refine_map_coeffs.mtz super_quick=true input_ha_file=ha.pdb number_of_parallel_models=2 n_cycle_rebuild_max=0''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)



  local_log_file='test_autobuild_quick.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''coords.pdb data=native.sca map_file=refine_map_coeffs.mtz super_quick=true''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)


  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_autobuild_quick',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['command_line_build_tests','test_autobuild_quick']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
