#!/bin/csh -ef
#$ -cwd
phenix.build_rna_helices conservative_build=True pdb472d.pdb.mtz pdb472d.seq > conservative.log
if ( $status )then
  echo "FAILED"
  exit 1
endif
phenix.build_rna_helices conservative_build=True pdb472d.pdb.mtz pdb472d.seq build_rna_helices_pseudo_bp.pdb > conservative_2.log
if ( $status )then
  echo "FAILED"
  exit 1
endif
phenix.get_cc_mtz_pdb pdb472d.pdb.mtz build_rna_helices.pdb  |grep Correlation|grep overall
if ( $status )then
  echo "FAILED"
  exit 1
endif
phenix.build_rna_helices conservative_build=True pdb472d.pdb.mtz GU.seq build_rna_helices_pseudo_bp.pdb > conservative_3.log
if ( $status )then
  echo "FAILED"
  exit 1
endif
phenix.get_cc_mtz_pdb pdb472d.pdb.mtz build_rna_helices.pdb  |grep Correlation|grep overall
if ( $status )then
  echo "FAILED"
  exit 1
endif
