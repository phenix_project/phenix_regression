from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_build_rna_helices")
  print("")
  log_file=os.path.join(os.getcwd(),"test_build_rna_helices.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  from phenix.command_line.build_rna_helices  import build_rna_helices
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''conservative_build=True pdb472d.pdb.mtz pdb472d.seq target_output_format=pdb''')
  build_rna_helices(args=args,out=log)


  from phenix.command_line.build_rna_helices  import build_rna_helices
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''conservative_build=True pdb472d.pdb.mtz pdb472d.seq build_rna_helices_pseudo_bp.pdb target_output_format=pdb''')
  build_rna_helices(args=args,out=log)


  from phenix.command_line.get_cc_mtz_pdb  import get_cc_mtz_pdb
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''pdb472d.pdb.mtz build_rna_helices.pdb''')
  get_cc_mtz_pdb(args=args,out=log)


  from phenix.command_line.build_rna_helices  import build_rna_helices
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''conservative_build=True pdb472d.pdb.mtz GU.seq build_rna_helices_pseudo_bp.pdb target_output_format=pdb''')
  build_rna_helices(args=args,out=log)


  from phenix.command_line.get_cc_mtz_pdb  import get_cc_mtz_pdb
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''pdb472d.pdb.mtz build_rna_helices.pdb''')
  get_cc_mtz_pdb(args=args,out=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_build_rna_helices',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['command_line_build_tests','test_build_rna_helices']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
