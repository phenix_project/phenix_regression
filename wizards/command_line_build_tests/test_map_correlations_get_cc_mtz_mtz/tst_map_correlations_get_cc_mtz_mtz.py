from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print ("Test script for test_map_correlations_get_cc_mtz_mtz")
  print ("")


  log = sys.stdout
  from iotbx.cli_parser import run_program
  from phenix.programs import map_correlations

  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''map_coeffs_1.mtz map_coeffs_2.mtz''')
  run_program(map_correlations.Program, args = args, logger = log)

  print ('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['command_line_build_tests','test_map_correlations_get_cc_mtz_mtz']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
