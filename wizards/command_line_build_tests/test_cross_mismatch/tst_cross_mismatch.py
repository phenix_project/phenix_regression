from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_cross_mismatch")
  print("")
  log_file=os.path.join(os.getcwd(),"test_cross_mismatch.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  cmds='''hklin overall_best_denmod_map_coeffs.mtz
labin FP=FWT PHIB=PHWT
pdb_in bad_strand.pdb
pdb_in_extra helix_1.pdb
cross_mismatch
offset_in_cross
dist_close 1.5
zero_incomplete
skip_hetatm
extend_only
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.resolve', cmds,'phenix.resolve.log',None,None,None)
  print(open('phenix.resolve.log').read(), file=log)
  print(open('edit_model.pdb').read(), file=log)

  cmds='''hklin overall_best_denmod_map_coeffs.mtz
labin FP=FWT PHIB=PHWT
pdb_in edited_bad_strand.pdb
pdb_in_extra short_bad_strand-symmetry-8.pdb
cross
offset_in_cross
dist_close 1.5
zero_incomplete
skip_hetatm
extend_only
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.resolve', cmds,'phenix.resolve.log',None,None,None)
  print(open('phenix.resolve.log').read(), file=log)
  print(open('edit_model.pdb').read(), file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_cross_mismatch',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['command_line_build_tests','test_cross_mismatch']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
