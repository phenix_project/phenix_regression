#!/bin/csh -ef
#$ -cwd
phenix.resolve<<EOD
hklin overall_best_denmod_map_coeffs.mtz
labin FP=FWT PHIB=PHWT
pdb_in bad_strand.pdb
pdb_in_extra helix_1.pdb
cross_mismatch
offset_in_cross
dist_close 1.5
zero_incomplete
skip_hetatm
extend_only
EOD
if ( $status )then
  echo "FAILED"
  exit 1
endif
cat edit_model.pdb|grep ' CA '

phenix.resolve<<EOD
hklin overall_best_denmod_map_coeffs.mtz
labin FP=FWT PHIB=PHWT
pdb_in edited_bad_strand.pdb 
pdb_in_extra short_bad_strand-symmetry-8.pdb
cross
offset_in_cross
dist_close 1.5
zero_incomplete
skip_hetatm
extend_only
EOD
if ( $status )then
  echo "FAILED"
  exit 1
endif
cat edit_model.pdb|grep ' CA '

