#!/bin/csh -ef
#$ -cwd
phenix.phase_and_build macro_cycles_in_refine_before_merge=1 macro_cycles=1  ncycle=0 quick=true sequence_autosol.dat data=solve_2.mtz debug=true resolution=4 assign_sequence=false extend=false trace_loops=False
grep r_work build_one_model.pdb
