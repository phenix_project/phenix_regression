#!/bin/csh -ef
#$ -cwd
phenix.build_one_model quick=True sequence_autosol.dat all.pdb cycle_2.mtz labin_map_coeffs="FP=FWT PHIB=PHWT " free_in=working_data_file.mtz debug=true> test_build_one_model_current.log
if ( $status )then
  echo "FAILED"
  exit 1
endif
grep "CC" temp_dir/refine_build.pdb
