from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os,sys
  print("Test script for test_build_one_model")
  print("")
  log_file=os.path.join(os.getcwd(),"test_build_one_model.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  local_log_file='test_build_one_model.log'
  local_log=open(local_log_file,'w')

  from iotbx.cli_parser import run_program
  from phenix.programs import build_one_model as run

  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''quick=True macro_cycles=1 sequence_autosol.dat all.pdb cycle_2.mtz labin_map_coeffs="FP=FWT PHIB=PHWT " free_in=working_data_file.mtz debug=true''')
  results = run_program(program_class=run.Program,
      args = args, logger = local_log)
  print(open(local_log_file).read(), file=log)
  assert results.overall_cc >= 0.5


  for file_name in ['temp_dir/refine_build.pdb']:
    for line in open(file_name).readlines():
      if line.lower().find("cc")>-1:
        print(line, file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_build_one_model',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['command_line_build_tests','test_build_one_model']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
