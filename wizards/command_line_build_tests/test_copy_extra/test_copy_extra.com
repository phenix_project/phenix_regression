#!/bin/csh -ef
#$ -cwd
phenix.python <<EOD
from phenix.autosol.copy_extra import copy_extra_to_pdb
copy_extra_to_pdb(pdb1="resolve.pdb",pdb2="se.pdb",pdb3="out.pdb",
        copy_coords=True)
EOD
if ( $status )then
  echo "FAILED"
  exit 1
endif
diff expected.pdb out.pdb
