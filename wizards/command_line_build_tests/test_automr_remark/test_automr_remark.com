#!/bin/csh -ef
#$ -cwd
phenix.automr data.sca seq_file=seq.dat ensemble_merged.pdb copies=1 mass=5000 n_cycle_rebuild_max=0 rebuild_in_place=false
if ( $status )then
  echo "FAILED"
  exit 1
endif
