#!/bin/csh -ef
#$ -cwd
phenix.parallel_autobuild model=coords.pdb data=fobs.mtz map_file=refine_map_coeffs.mtz super_quick=true iterations=2
if ( $status )then
  echo "FAILED"
  exit 1
endif
