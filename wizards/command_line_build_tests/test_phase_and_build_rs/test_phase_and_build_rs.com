#!/bin/csh -ef
#$ -cwd
phenix.phase_and_build map_coeffs=short_box.mtz seq_file=seq.dat resolution=10 find_ncs=false resolve_command_list="\\n'delta_phi 150'\\n 'i_ran_seed 625769' \\n 'start_rot 82 243 89'" refine=false rs_refine=true cc_min=0.01 cc_min_rna=0.01
if ( $status )then
  echo "FAILED"
  exit 1
endif
cat temp_dir/cc.log
