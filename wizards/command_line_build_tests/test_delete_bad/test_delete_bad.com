#!/bin/csh -ef
#$ -cwd
phenix.autobuild delete_bad_residues_only=true coords.pdb data.mtz
if ( $status )then
  echo "FAILED"
  exit 1
endif
