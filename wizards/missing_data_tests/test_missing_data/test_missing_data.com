#!/bin/csh  -f
#$ -cwd
#
setenv LIBTBX_FULL_TESTING
#
if ($#argv > 0) then
  if ($argv[1] == "--help") then
    echo "test missing data for autobuild"
    exit 0
  endif
endif

echo " "
echo "----------------------------------------------------- "
echo "test_missing: test missing data for autobuild"
#
#
echo "run1 : fobs_std.mtz"
phenix.autobuild remove_aniso=False \
    coords.pdb fobs_std.mtz n_cycle_rebuild_max=0 \
   nbatch=1  skip_xtriage=true
phenix.python<<EOD
import time
time.sleep(1.)
EOD
phenix.reflection_file_converter --label=Free \
 --cns=run1_free.dump AutoBuild_run_1_/exptl_fobs_phases_freeR_flags.mtz
phenix.reflection_file_converter --label=FP \
 --cns=run1_fp.dump AutoBuild_run_1_/exptl_fobs_phases_freeR_flags.mtz 
#
echo "run2 : fobs_std_edit.mtz"
phenix.autobuild remove_aniso=False \
   coords.pdb fobs_std_edit.mtz n_cycle_rebuild_max=0 \
   nbatch=1  skip_xtriage=true
phenix.python<<EOD
import time
time.sleep(1.)
EOD
phenix.reflection_file_converter --label=Free \
 --cns=run2_free.dump AutoBuild_run_2_/exptl_fobs_phases_freeR_flags.mtz 
phenix.reflection_file_converter --label=FP \
 --cns=run2_fp.dump AutoBuild_run_2_/exptl_fobs_phases_freeR_flags.mtz 
#
