#!/bin/csh -ef
#$ -cwd
setenv SOLVETMPDIR /var/tmp
#
solve_resolve.extract_segment<<EOD |grep -i '[a-d,f-z]'
2,2,    ! 5=type, 0=rna 1=dna 2=prot
STRAND
360.
1.0
1bav_265_268.pdb
1bav_chaina.pdb
solve_30.mtz
labin FP=FP FOM=FOM PHIB=PHIB
template_30_mean_2.ezd
template_30_corr_2.ezd
template_30_mask_2.ezd
EOD
