#!/bin/csh
setenv SOLVETMPDIR /var/tmp
#
#  put name of PDB model with all source models here:
setenv input_model  1bav_chaina.pdb
#
#
echo "Working in `pwd`"
if (-d segments )rm -rf segments
mkdir segments
#
echo "starting template generation "
rm -rf segment_*_2.pdb *.ezd template_map_coeffs.mtz
#
solve_resolve.extract_segment<<EOD >extract_segment.log
2,2,    ! 5=type, 0=rna 1=dna 2=prot
STRAND
360.
1.0
1bav_265_268.pdb
${input_model}
solve_30.mtz
labin FP=FP FOM=FOM PHIB=PHIB
template_30_mean_2.ezd
template_30_corr_2.ezd
template_30_mask_2.ezd
EOD
cp -p segment_*_2.pdb *.ezd template_map_coeffs.mtz segments/
rm -rf segment_*_2.pdb *.ezd template_map_coeffs.mtz
#
echo "ready..."
echo "Results are in `cd segments; pwd ; cd ..`"
