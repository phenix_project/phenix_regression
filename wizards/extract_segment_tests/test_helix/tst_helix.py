from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_helix")
  print("")
  log_file=os.path.join(os.getcwd(),"test_helix.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  cmds='''1,2,    ! 5=type, 0=rna 1=dna 2=prot
HELIX
100.
0.5
mb_133_138_main.pdb
lia_2chains.pdb
solve_30.mtz
labin FP=FP FOM=FOM PHIB=PHIB
template_30_mean_1.ezd
template_30_corr_1.ezd
template_30_mask_1.ezd
  '''
  from phenix.autosol.run_program import run_program
  run_program('solve_resolve.extract_segment', cmds,'solve_resolve.extract_segment.log',None,None,None)
  print(open('solve_resolve.extract_segment.log').read(), file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_helix',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['extract_segment_tests','test_helix']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
