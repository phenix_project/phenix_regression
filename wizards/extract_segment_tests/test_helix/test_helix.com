#!/bin/csh -ef
#$ -cwd
setenv SOLVETMPDIR /var/tmp
#
solve_resolve.extract_segment<<EOD |grep -i '[a-d,f-z]'
1,2,    ! 5=type, 0=rna 1=dna 2=prot
HELIX
100.
0.5
mb_133_138_main.pdb
lia_2chains.pdb
solve_30.mtz
labin FP=FP FOM=FOM PHIB=PHIB
template_30_mean_1.ezd
template_30_corr_1.ezd
template_30_mask_1.ezd
EOD
