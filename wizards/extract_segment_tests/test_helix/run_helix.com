#!/bin/csh
setenv SOLVETMPDIR /var/tmp
#
#  put name of PDB model with all source models here:
setenv input_model lia_2chains.pdb
#
#
echo "Working in `pwd`"
if (-d segments )rm -rf segments
mkdir segments
#
echo "starting template generation "
rm -rf segment_*_1.pdb *.ezd template_map_coeffs.mtz
#
solve_resolve.extract_segment<<EOD >extract_segment.log
1,2,    ! 5=type, 0=rna 1=dna 2=prot
HELIX
100.
0.5
mb_133_138_main.pdb
${input_model}
solve_30.mtz
labin FP=FP FOM=FOM PHIB=PHIB
template_30_mean_1.ezd
template_30_corr_1.ezd
template_30_mask_1.ezd
EOD
cp -p segment_*_1.pdb *.ezd template_map_coeffs.mtz segments/
rm -rf segment_*_1.pdb *.ezd template_map_coeffs.mtz
#
echo "ready..."
echo "Results are in `cd segments; pwd ; cd ..`"
