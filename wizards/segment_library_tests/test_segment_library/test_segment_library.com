#!/bin/csh -ef
#$ -cwd
setenv SOLVETMPDIR /var/tmp
#
solve_resolve.segment_library<<EOD
0.5
2      ! end type is 2 (C-terminal library)
c_term_050_3.lib
1
nsf-d2_reference.pdb
EOD
