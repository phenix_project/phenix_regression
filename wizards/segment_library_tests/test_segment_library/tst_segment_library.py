from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_segment_library")
  print("")
  log_file=os.path.join(os.getcwd(),"test_segment_library.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  cmds='''0.5
2      ! end type is 2 (C-terminal library)
c_term_050_3.lib
1
nsf-d2_reference.pdb
  '''
  from phenix.autosol.run_program import run_program
  run_program('solve_resolve.segment_library', cmds,'solve_resolve.segment_library.log',None,None,None)
  print(open('solve_resolve.segment_library.log').read(), file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_segment_library',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['segment_library_tests','test_segment_library']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
