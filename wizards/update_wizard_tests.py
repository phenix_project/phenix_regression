from __future__ import division
from __future__ import print_function
import sys, os, shutil
from libtbx.utils import Sorry

def remove_arg(arg,args):
  new_args=[]
  for a in args:
   if not a==arg: new_args.append(a)
  return new_args

def run(args):

  if 'allow_missing' in args:
    args=remove_arg('allow_missing',args)
    allow_missing=True
  else:
    allow_missing=False
   
  if 'copy_expected_output' in args:
    args=remove_arg('copy_expected_output',args)
    copy_expected_output=True
  else:
    copy_expected_output=False
   
  if 'copy_ignored_output' in args:
    args=remove_arg('copy_ignored_output',args)
    copy_ignored_output=True
  else:
    copy_ignored_output=False

  if 'list_useful_lines' in args:
    args=remove_arg('list_useful_lines',args)
    list_useful_lines=True
  else:
    list_useful_lines=False

  # Figure out what tests we are working with
  if 'all' in args:
    args=remove_arg('all',args)
    all=True
  else:
    all=False

  if all:
    from phenix_regression.wizards.run_wizard_test import get_test_dict
    test_dict=get_test_dict()

  else:
    if not args or len(args)>2:
      print("\nPlease supply a test name (test_resno) or "+\
         "test_group name (solve_tests)\n")
      return
    test_name=None
    group_name=None
    for text in args:
      if text.startswith("test_"):
        test_name=text
      elif text.endswith("_tests"):
        group_name=text

    if test_name:
      from phenix_regression.wizards.run_wizard_test import find_test
      group_name,test_name=find_test(
         test_name=test_name,group_name=group_name)
      test_dict={group_name:[test_name]}
    elif group_name:
      from phenix_regression.wizards.run_wizard_test import get_test_dict
      test_dict=get_test_dict(group_name=group_name)
    else:
      print("\nTest name should start with test_ (single test) +"\
        "or end with _tests (group of tests)\n")
      return


  import libtbx.load_env
  base_path = libtbx.env.under_dist(
    module_name="phenix_regression",
    path='wizards',
    test=os.path.isdir)
  assert base_path is not None

  print("Directory tree to copy from: %s" %(os.getcwd()))
  print("Directory tree to copy to:   %s" %(base_path))

  # verify that all the directories we need are in both places
  print("\nTests to work on: ")
  group_names=sorted(test_dict.keys())
  if not allow_missing:
    for group_name in group_names:
      print()
      test_names=sorted(test_dict[group_name])
      for test_name in test_names:
        print("%s  %s"  %(group_name,test_name))
        from_dir=os.path.join(os.getcwd(),group_name,test_name)
        to_dir=os.path.join(base_path,group_name,test_name)
        if not os.path.isdir(from_dir):
          raise Sorry("Missing: %s" %(from_dir) +
            "\n\nUse allow_missing to ignore this")
          
        if not os.path.isdir(to_dir):
          raise Sorry("Missing: %s" %(to_dir) +
            "\n\nUse allow_missing to ignore this")

  # Ready to copy files...
  if copy_ignored_output:
    print("Copying ignored_output files...")
  if copy_expected_output:
    print("Copying expected_output files...")
  if list_useful_lines:
    print("Listing useful lines")

  if not copy_ignored_output and not copy_expected_output and not list_useful_lines:
    print("\nNothing to do...")
    return
  
  for group_name in group_names:
    test_names=sorted(test_dict[group_name])
    for test_name in test_names:
      if copy_ignored_output or copy_expected_output:
        from_dir=os.path.join(os.getcwd(),group_name,test_name)
        to_dir=os.path.join(base_path,group_name,test_name)
        from_files_to_copy=[]
        to_files_to_copy=[]
        if copy_ignored_output: 
          from_files_to_copy.append('%s.draft_ignored_output' %(test_name))
          to_files_to_copy.append('%s.ignored_output' %(test_name))
        if copy_expected_output: 
          from_files_to_copy.append('%s.draft_expected_output' %(test_name))
          to_files_to_copy.append('%s.expected_output' %(test_name))
        for from_file_name,to_file_name in zip(from_files_to_copy,to_files_to_copy):
          full_from_file=os.path.join(from_dir,from_file_name)
          full_to_file=os.path.join(to_dir,to_file_name)
          if os.path.isfile(full_from_file):
            print(" Copying... \n   %s to \n   %s" %(full_from_file,full_to_file))
            shutil.copyfile(full_from_file,full_to_file)
          else:
            pass #print "NOTE: Missing the file %s (not copied)" %(full_from_file)
      # Print out how many usful lines
      dd=os.path.join(os.getcwd(),group_name,test_name)
      if not allow_missing:
        assert os.path.isdir(dd)
      if os.path.isdir(dd):
        current_dir=os.getcwd()
        os.chdir(dd)
        from phenix_regression.wizards.check_results import check_results
        check_results(test=test_name,lines=[],write_expected_file=None,
          list_useful_lines=list_useful_lines)
        os.chdir(current_dir)

if (__name__ == "__main__"):
  print("\nphenix_regression.wizards.update_wizard_tests "+\
     "[test_resno] [solve_tests] [all] [copy_expected_output] [copy_ignored_output] [list_useful_lines] [allow_missing]")
  run(args=sys.argv[1:])
