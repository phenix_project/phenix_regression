#!/bin/csh  -f
#$ -cwd
#
setenv LIBTBX_FULL_TESTING
#
if ($#argv > 0) then
  if ($argv[1] == "--help") then
    echo "test_ncs_in_phenix_refine"
    exit 0
  endif
endif

echo " "
echo "----------------------------------------------------- "
echo "test_ncs_in_phenix_refine: "
#
#
set select = "|grep resseq"
#
echo "Testing mlt with offset, not accepting user ncs"
set logname = "mlt_offset.log"
set cmds = " mlt.mtz mlt_offset.pdb main.number_of_macro_cycles=0 refinement.pdb_interpretation.ncs_search.enabled=True refinement.ncs.excessive_distance_limit=None --overwrite"
phenix.refine $cmds > ${logname}_test
if ( $status == 1) goto bad
echo "Differences in ${logname}_test  from $logname : \
      `diff -b $logname ${logname}_test $select `"
#
goto finished
bad:

    echo "*****************************************************"
    echo "Error:test failed to run with commands $cmds"
    echo "*****************************************************"
    exit 1
  goto finished
endif
finished:
echo "OK"
echo "------------------------------------------------------------"
echo " "
exit 0
