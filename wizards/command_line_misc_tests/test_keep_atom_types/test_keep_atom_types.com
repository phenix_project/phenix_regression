#!/bin/csh -ef
#$ -cwd
phenix.autosol seq.dat peak.sca 2 se sites_file= ha_1.pdb skip_xtriage=true remove_aniso=false quick=true build=false keep_atom_types=true mask_type=wang
if ( $status )then
  echo "FAILED"
  exit 1
endif
