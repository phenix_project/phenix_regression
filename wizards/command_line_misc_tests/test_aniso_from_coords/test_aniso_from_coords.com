#!/bin/csh -ef
#$ -cwd

foreach xyz ("010" "001" "100")
echo ""
echo "------------------------------------------------------------"
echo "Generating random aniso atoms with rotation of (1,0,0) about $xyz"

phenix.python<<EOD
from phenix.utilities.generate_aniso import run
args=["$xyz"]
run(args)
EOD

phenix.fmodel pdb_aniso.pdb high_resolution=2 > fmodel_pdb_aniso.log
phenix.fmodel random.pdb high_resolution=2 > fmodel_random.log
echo "Comparison of maps from random.pdb with aniso record and 1 atom:"
phenix.get_cc_mtz_mtz pdb_aniso.pdb.mtz random.pdb.mtz|grep offsett
echo ""
cp pdb_aniso.pdb pdb_aniso_$xyz.pdb
cp pdb_aniso.pdb.mtz pdb_aniso_$xyz.pdb.mtz
cp random_as_models.pdb random_as_models_$xyz.pdb
phenix.python<<EOD
from phenix.utilities.aniso_from_ensemble import run
args=["random_as_models_$xyz.pdb"]
run(args)
EOD
cp aniso_from_ensemble.pdb aniso_from_ensemble_$xyz.pdb
phenix.fmodel aniso_from_ensemble_$xyz.pdb high_resolution=2 > fmodel_from_ensemble_$xyz.log
echo ""
echo "Comparison of maps from random.pdb with aniso_from_ensemble:"
phenix.get_cc_mtz_mtz random.pdb.mtz aniso_from_ensemble_$xyz.pdb.mtz|grep offsett
echo ""

end
