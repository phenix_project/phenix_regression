from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):

  
  
  # Auto-generated script prepared by create_py.py
  #
  import os,sys
  print("Test script for test_aniso_from_coords")
  print("")
  log_file=os.path.join(os.getcwd(),"test_aniso_from_coords.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this
  
  
  cmds='''from phenix.utilities.generate_aniso import run
args=["010"]
run(args)
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print(open('phenix.python.log').read(), file=log)
  
  
  local_log_file='test_aniso_from_coords.log'
  local_log=open(local_log_file,'w')
  from mmtbx.programs import fmodel
  from iotbx.cli_parser import run_program
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  
  args=split_except_quotes('''pdb_aniso.pdb high_resolution=2''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_program(program_class=fmodel.Program, args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)
  
  
  
  local_log_file='test_aniso_from_coords.log'
  local_log=open(local_log_file,'w')
  from mmtbx.programs import fmodel
  from iotbx.cli_parser import run_program
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  
  args=split_except_quotes('''random.pdb high_resolution=2''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_program(program_class=fmodel.Program, args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)
  
  
  
  from phenix.command_line.get_cc_mtz_mtz  import get_cc_mtz_mtz
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''pdb_aniso.pdb.mtz random.pdb.mtz''')
  get_cc_mtz_mtz(args=args,out=log)
  
  import shutil
  shutil.copyfile('pdb_aniso.pdb','pdb_aniso_010.pdb')
  
  import shutil
  shutil.copyfile('pdb_aniso.pdb.mtz','pdb_aniso_010.pdb.mtz')
  
  import shutil
  shutil.copyfile('random_as_models.pdb','random_as_models_010.pdb')
  
  cmds='''from phenix.utilities.aniso_from_ensemble import run
args=["random_as_models_010.pdb"]
run(args)
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print(open('phenix.python.log').read(), file=log)
  
  import shutil
  shutil.copyfile('aniso_from_ensemble.pdb','aniso_from_ensemble_010.pdb')
  
  
  local_log_file='test_aniso_from_coords.log'
  local_log=open(local_log_file,'w')
  from mmtbx.programs import fmodel
  from iotbx.cli_parser import run_program
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  
  args=split_except_quotes('''aniso_from_ensemble_010.pdb high_resolution=2''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_program(program_class=fmodel.Program, args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)
  
  
  
  from phenix.command_line.get_cc_mtz_mtz  import get_cc_mtz_mtz
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''random.pdb.mtz aniso_from_ensemble_010.pdb.mtz''')
  get_cc_mtz_mtz(args=args,out=log)
  
  cmds='''from phenix.utilities.generate_aniso import run
args=["001"]
run(args)
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print(open('phenix.python.log').read(), file=log)
  
  
  local_log_file='test_aniso_from_coords.log'
  local_log=open(local_log_file,'w')
  from mmtbx.programs import fmodel
  from iotbx.cli_parser import run_program
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  
  args=split_except_quotes('''pdb_aniso.pdb high_resolution=2''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_program(program_class=fmodel.Program, args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)
  
  
  
  local_log_file='test_aniso_from_coords.log'
  local_log=open(local_log_file,'w')
  from mmtbx.programs import fmodel
  from iotbx.cli_parser import run_program
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  
  args=split_except_quotes('''random.pdb high_resolution=2''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_program(program_class=fmodel.Program, args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)
  
  
  
  from phenix.command_line.get_cc_mtz_mtz  import get_cc_mtz_mtz
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''pdb_aniso.pdb.mtz random.pdb.mtz''')
  get_cc_mtz_mtz(args=args,out=log)
  
  import shutil
  shutil.copyfile('pdb_aniso.pdb','pdb_aniso_001.pdb')
  
  import shutil
  shutil.copyfile('pdb_aniso.pdb.mtz','pdb_aniso_001.pdb.mtz')
  
  import shutil
  shutil.copyfile('random_as_models.pdb','random_as_models_001.pdb')
  
  cmds='''from phenix.utilities.aniso_from_ensemble import run
args=["random_as_models_001.pdb"]
run(args)
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print(open('phenix.python.log').read(), file=log)
  
  import shutil
  shutil.copyfile('aniso_from_ensemble.pdb','aniso_from_ensemble_001.pdb')
  
  
  local_log_file='test_aniso_from_coords.log'
  local_log=open(local_log_file,'w')
  from mmtbx.programs import fmodel
  from iotbx.cli_parser import run_program
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  
  args=split_except_quotes('''aniso_from_ensemble_001.pdb high_resolution=2''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_program(program_class=fmodel.Program, args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)
  
  
  
  from phenix.command_line.get_cc_mtz_mtz  import get_cc_mtz_mtz
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''random.pdb.mtz aniso_from_ensemble_001.pdb.mtz''')
  get_cc_mtz_mtz(args=args,out=log)
  
  cmds='''from phenix.utilities.generate_aniso import run
args=["100"]
run(args)
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print(open('phenix.python.log').read(), file=log)
  
  
  local_log_file='test_aniso_from_coords.log'
  local_log=open(local_log_file,'w')
  from mmtbx.programs import fmodel
  from iotbx.cli_parser import run_program
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  
  args=split_except_quotes('''pdb_aniso.pdb high_resolution=2''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_program(program_class=fmodel.Program, args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)
  
  
  
  local_log_file='test_aniso_from_coords.log'
  local_log=open(local_log_file,'w')
  from mmtbx.programs import fmodel
  from iotbx.cli_parser import run_program
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  
  args=split_except_quotes('''random.pdb high_resolution=2''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_program(program_class=fmodel.Program, args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)
  
  
  
  from phenix.command_line.get_cc_mtz_mtz  import get_cc_mtz_mtz
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''pdb_aniso.pdb.mtz random.pdb.mtz''')
  get_cc_mtz_mtz(args=args,out=log)
  
  import shutil
  shutil.copyfile('pdb_aniso.pdb','pdb_aniso_100.pdb')
  
  import shutil
  shutil.copyfile('pdb_aniso.pdb.mtz','pdb_aniso_100.pdb.mtz')
  
  import shutil
  shutil.copyfile('random_as_models.pdb','random_as_models_100.pdb')
  
  cmds='''from phenix.utilities.aniso_from_ensemble import run
args=["random_as_models_100.pdb"]
run(args)
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print(open('phenix.python.log').read(), file=log)
  
  import shutil
  shutil.copyfile('aniso_from_ensemble.pdb','aniso_from_ensemble_100.pdb')
  
  
  local_log_file='test_aniso_from_coords.log'
  local_log=open(local_log_file,'w')
  from mmtbx.programs import fmodel
  from iotbx.cli_parser import run_program
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  
  args=split_except_quotes('''aniso_from_ensemble_100.pdb high_resolution=2''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_program(program_class=fmodel.Program, args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)
  
  
  
  from phenix.command_line.get_cc_mtz_mtz  import get_cc_mtz_mtz
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''random.pdb.mtz aniso_from_ensemble_100.pdb.mtz''')
  get_cc_mtz_mtz(args=args,out=log)
  
  log.close()
  

  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_aniso_from_coords',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')


  
if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['command_line_misc_tests','test_aniso_from_coords']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
