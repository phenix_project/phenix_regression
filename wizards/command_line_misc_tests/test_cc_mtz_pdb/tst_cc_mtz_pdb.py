from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_cc_mtz_pdb")
  print("")
  log_file=os.path.join(os.getcwd(),"test_cc_mtz_pdb.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  import shutil
  shutil.copyfile('model1.pdb','model.pdb')

  cmds='''verbose
fix_rad_max
hklin random_1.mtz
labin FP=FWT PHIB=PHWT
no_build
ha_file NONE
evaluate_model
b_overall 0
fill_ratio 1.0
fill
res_fill 0.0
verbose
model model.pdb
use_wang
no_ha
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.resolve', cmds,'phenix.resolve.log',None,None,None)
  print(open('phenix.resolve.log').read(), file=log)

  import shutil
  shutil.copyfile('model3.pdb','model.pdb')

  cmds='''verbose
fix_rad_max
hklin random_1.mtz
labin FP=FWT PHIB=PHWT
no_build
ha_file NONE
evaluate_model
b_overall 0
fill_ratio 1.0
fill
res_fill 0.0
verbose
model model.pdb
use_wang
no_ha
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.resolve', cmds,'phenix.resolve.log',None,None,None)
  print(open('phenix.resolve.log').read(), file=log)


  from phenix.command_line.get_cc_mtz_pdb  import get_cc_mtz_pdb
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''short.mtz short.pdb fix_xyz=true split_conformers=False''')
  get_cc_mtz_pdb(args=args,out=log)
  print(open('cc.log').read(), file=log)


  from phenix.command_line.get_cc_mtz_pdb  import get_cc_mtz_pdb
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''short.mtz short.pdb fix_xyz=true split_conformers=true''')
  get_cc_mtz_pdb(args=args,out=log)
  print(open('cc.log').read(), file=log)


  from phenix.command_line.get_cc_mtz_mtz  import get_cc_mtz_mtz
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''short1.mtz short2.mtz''')
  get_cc_mtz_mtz(args=args,out=log)


  from phenix.command_line.get_cc_mtz_mtz  import get_cc_mtz_mtz
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''short1.ccp4 short2.ccp4''')
  get_cc_mtz_mtz(args=args,out=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_cc_mtz_pdb',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['command_line_misc_tests','test_cc_mtz_pdb']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
