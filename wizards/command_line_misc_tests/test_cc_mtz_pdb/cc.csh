#!/bin/csh -f
phenix.resolve<<EOD
verbose
fix_rad_max
hklin random_1.mtz
labin FP=FWT PHIB=PHWT
no_build
ha_file NONE
evaluate_model
b_overall 0
fill_ratio 1.0
fill
res_fill 0.0
verbose
model model.pdb
use_wang
no_ha
EOD
