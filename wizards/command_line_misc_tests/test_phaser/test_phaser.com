#!/bin/csh -ef
#$ -cwd

echo "Running phaser automr"

phenix.automr coords.pdb native.sca seq.dat build=false  copies=1 RMS=0.9 use_tncs=false
if ( $status )then
  echo "FAILED"
  exit 1
endif

echo "Running phaser phasing in autosol"
phenix.autosol peak.sca 2 se seq.dat build=false quick=true skip_extra_phasing=True thorough_denmod=False skip_xtriage=True remove_aniso=False
if ( $status )then
  echo "FAILED"
  exit 1
endif

echo "Running phaser with partpdb_file"

if ( $status )then
  echo "FAILED"
  exit 1
endif
phenix.autosol peak.sca 2 se seq.dat build=false quick=true skip_extra_phasing=True thorough_denmod=False partpdb_file=coords.pdb
if ( $status )then
  echo "FAILED"
  exit 1
endif
echo "All Phaser tests OK"

