#!/bin/csh -ef
#$ -cwd
mkdir test\\xtal\\nat\\sp\ ace\\here\\b\\t\\r\\z\\ending/
cp perfect.mtz test\\xtal\\nat\\sp\ ace\\here\\b\\t\\r\\z\\ending/perfect.mtz
cp side.pdb    test\\xtal\\nat\\sp\ ace\\here\\b\\t\\r\\z\\ending/side.pdb 
cp partial.pdb test\\xtal\\nat\\sp\ ace\\here\\b\\t\\r\\z\\ending/partial.pdb
cd test\\xtal\\nat\\sp\ ace\\here\\b\\t\\r\\z\\ending/
phenix.ligandfit data=perfect.mtz model=partial.pdb ligand=side.pdb quick=True resolution=3.0 n_group_search=1 ligand_cc_min=0.5 remove_path_word_list=dummy input_labels="FP PHIC FOM"
if ( $status )then
  echo "FAILED"
  exit 1
endif

