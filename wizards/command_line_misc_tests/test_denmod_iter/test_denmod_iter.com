#!/bin/csh -ef
#$ -cwd
phenix.autobuild maps_only=true data=phaser.mtz input_map_file=map_coeffs.mtz seq_file=seq.dat find_ncs=False skip_xtriage=true remove_aniso=False resolve_command_list="'mask_cycles 1' 'minor_cycles 1'" map_dmin_start=3
if ( $status )then
  echo "FAILED"
  exit 1
endif
