#!/bin/csh -ef
#$ -cwd
echo "Running test_run_phenix_refine"
phenix.python <<EOD
from __future__ import division
from __future__ import print_function
from phenix_regression.refinement.autobuild.run_phenix_refine.tst_run_phenix_refine import tst_01, tst_02, tst_03, tst_04,tst_05
tst_01()
tst_02()
tst_03()
tst_04()
tst_05()
print ("OK")
EOD
if ( $status )then
  echo "FAILED"
  exit 1
endif

echo "All run_phenix_refine tests OK"

