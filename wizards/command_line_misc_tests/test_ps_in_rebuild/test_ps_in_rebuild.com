#!/bin/csh -ef
#$ -cwd
phenix.autobuild ps_in_rebuild=True native.sca coords.pdb rebuild_in_place=False number_of_parallel_models=1 maps_only=True refine=false
if ( $status )then
  echo "FAILED"
  exit 1
endif
phenix.get_cc_mtz_mtz prime_and_switch.mtz AutoBuild_run_1_/prime_and_switch.mtz|grep offsett
