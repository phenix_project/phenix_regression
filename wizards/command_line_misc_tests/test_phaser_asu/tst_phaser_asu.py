from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_phaser_asu")
  print("")
  log_file=os.path.join(os.getcwd(),"test_phaser_asu.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  os.mkdir('AutoSol_run_3_/TEMP0')

  import shutil
  shutil.copyfile('AutoSol_run_3_/ANISO_outputsplit2n.sca_ano_1.sca','AutoSol_run_3_/TEMP0/ANISO_outputsplit2n.sca_ano_1.sca')

  import shutil
  shutil.copyfile('AutoSol_run_3_/TEMP_sca.mtz','AutoSol_run_3_/TEMP0/TEMP_sca.mtz')

  import shutil
  shutil.copyfile('AutoSol_run_3_/add_classic_denmod_3.mtz','AutoSol_run_3_/TEMP0/add_classic_denmod_3.mtz')

  cmds='''from phenix.autosol.run_phaser import run_pickle
run_pickle(["phaser_args.pkl"])
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print(open('phenix.python.log').read(), file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_phaser_asu',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['command_line_misc_tests','test_phaser_asu']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
