#!/bin/csh -ef
#$ -cwd

echo "Running phaser automr"
mkdir AutoSol_run_3_/TEMP0
cp AutoSol_run_3_/ANISO_outputsplit2n.sca_ano_1.sca AutoSol_run_3_/TEMP0/ANISO_outputsplit2n.sca_ano_1.sca
cp  AutoSol_run_3_/TEMP_sca.mtz AutoSol_run_3_/TEMP0/TEMP_sca.mtz
cp AutoSol_run_3_/add_classic_denmod_3.mtz AutoSol_run_3_/TEMP0/add_classic_denmod_3.mtz

phenix.python<<EOD
from phenix.autosol.run_phaser import run_pickle
run_pickle(["phaser_args.pkl"])
EOD
if ( $status )then
  echo "FAILED"
  exit 1
endif

echo "All Phaser asu tests OK"

