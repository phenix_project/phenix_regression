#!/bin/csh -ef
#$ -cwd
phenix.python<<EOD
from mmtbx.scaling.tst_bayesian_estimator import exercise
exercise(["verbose"])
EOD
if ( $status )then
  echo "FAILED"
  exit 1
endif

