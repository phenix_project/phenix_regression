#!/bin/csh -ef
#$ -cwd

echo "Adding..."
phenix.average_map_coeffs mtz1.mtz mtz2.mtz labels="FMODEL,PHIFMODEL" map_coeffs_out=mtz_average.mtz weights="1 1"
phenix.get_cc_mtz_mtz mtz1.mtz mtz_average.mtz |grep offsett
phenix.get_cc_mtz_mtz mtz2.mtz mtz_average.mtz | grep offsett
phenix.get_cc_mtz_mtz mtz1.mtz  mtz2.mtz  | grep offsett

echo "now subtract"
phenix.average_map_coeffs mtz1.mtz mtz2.mtz labels="FMODEL,PHIFMODEL" map_coeffs_out=mtz_average.mtz weights="1 -1"
phenix.get_cc_mtz_mtz mtz1.mtz mtz_average.mtz |grep offsett
phenix.get_cc_mtz_mtz mtz2.mtz mtz_average.mtz | grep offsett

