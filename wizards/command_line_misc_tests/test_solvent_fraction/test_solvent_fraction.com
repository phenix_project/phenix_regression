#!/bin/csh -ef
#$ -cwd
phenix.autobuild generate_hl_if_missing=true seq_file=seq.dat data=resolve_1_offset.mtz quick=true maps_only=true refine=false |tee autobuild.log
if ( $status )then
  echo "FAILED"
  exit 1
endif
phenix.python<<EOD
text=open('autobuild.log').read()
if text.find('Not enough information to guess solvent fraction')>-1:
  raise AssertionError("Failed to guess solvent fraction")
if not text.find('Estimate of solvent fraction'):
  raise AssertionError("Failed to estimate solvent fraction")
EOD

