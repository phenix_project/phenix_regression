from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os,sys
  print("Test script for test_md")
  print("")
  log_file=os.path.join(os.getcwd(),"test_md.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  local_log_file='test_md.log'
  local_log=open(local_log_file,'w')
  from phenix.utilities.get_struct_fact_from_md  import run
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''pdb_file=one_two_prot_0.pdb d_min=4.0 output_prefix=wat_prot_0 solvent_file=one_to_solv_0.pdb''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)


  for line in open('wat_prot_0_complex_averaged_then_squared.dat').readlines()[:10]:
    print(line, file=log)

  for line in open('wat_prot_0_squared_then_averaged.dat').readlines()[:10]:
    print(line, file=log)


  local_log_file='test_md.log'
  local_log=open(local_log_file,'w')
  from phenix.utilities.get_struct_fact_from_md  import run
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''merge=true template=wat_prot first=0 last=0''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)


  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.mtz.dump -c wat_prot_0_to_0_complex_averaged.mtz')
  for line in result.stdout_lines:
    print(line, file=log)

  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.mtz.dump -c wat_prot_0_to_0_square_averaged.mtz')
  for line in result.stdout_lines:
    print(line, file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_md',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['command_line_misc_tests','test_md']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
