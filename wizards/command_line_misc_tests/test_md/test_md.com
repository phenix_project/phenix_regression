#!/bin/csh -ef
#$ -cwd
phenix.get_struct_fact_from_md \
 pdb_file=one_two_prot_0.pdb d_min=4.0 output_prefix=wat_prot_0 solvent_file=one_to_solv_0.pdb 
if ( $status )then
  echo "FAILED"
  exit 1
endif
echo "wat_prot_0_complex_averaged_then_squared.dat:"
head -10 wat_prot_0_complex_averaged_then_squared.dat
echo ""
echo "wat_prot_0_squared_then_averaged.dat"
head -10 wat_prot_0_squared_then_averaged.dat 

phenix.get_struct_fact_from_md merge=true template=wat_prot first=0 last=0
if ( $status )then
  echo "FAILED"
  exit 1
endif
echo "wat_prot_0_to_0_complex_averaged.mtz"
phenix.mtz.dump -c wat_prot_0_to_0_complex_averaged.mtz > tmp.dat; head -50 tmp.dat
echo "wat_prot_0_to_0_square_averaged.mtz"
phenix.mtz.dump -c wat_prot_0_to_0_square_averaged.mtz> tmp.dat; head -50 tmp.dat

