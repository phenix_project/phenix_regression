#!/bin/csh -ef
#$ -cwd
phenix.python<<EOD
from phenix.loop_lib.get_ca_segments import get_ca_segments
g=get_ca_segments()
g.exercise_protein()
g.exercise_rna()
g.exercise_protein_conformer()
EOD
if ( $status )then
  echo "FAILED"
  exit 1
endif
