#!/bin/csh -ef
#$ -cwd
phenix.autobuild two_fofc_in_rebuild=True native.sca coords.pdb rebuild_in_place=False number_of_parallel_models=1 maps_only=True refine=false
if ( $status )then
  echo "FAILED"
  exit 1
endif
phenix.get_cc_mtz_mtz map_coeffs.mtz AutoBuild_run_1_/map_coeffs.mtz|grep offsett
if ( $status )then
  echo "FAILED"
  exit 1
endif

