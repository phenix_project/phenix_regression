from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_plan_sad_experiment")
  print("")
  log_file=os.path.join(os.getcwd(),"test_plan_sad_experiment.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  cmds='''from mmtbx.scaling.tst_plan_sad_experiment import exercise
exercise()
print ("OK")
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print(open('phenix.python.log').read(), file=log)
  print(open('tst_plan_sad_experiment.fa').read(), file=log)

  args = "sites=2 wavelength=0.9792 atom_type=se residues=200 resolution=3".split()
  from iotbx.cli_parser import run_program
  from phenix.programs import plan_sad_experiment
  result = run_program(program_class=plan_sad_experiment.Program, args = args,
    logger = log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_plan_sad_experiment',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['command_line_misc_tests','test_plan_sad_experiment']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
