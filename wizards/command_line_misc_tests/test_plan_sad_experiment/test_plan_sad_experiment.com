#!/bin/csh -ef
#$ -cwd
phenix.python <<EOD
from mmtbx.scaling.tst_plan_sad_experiment import exercise
exercise()
print "OK"
EOD
cat tst_plan_sad_experiment.fa
