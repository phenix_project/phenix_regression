from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os,sys
  print("Test script for test_guess_ncs_and_sites")
  print("")
  log_file=os.path.join(os.getcwd(),"test_guess_ncs_and_sites.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  local_log_file='test_guess_ncs_and_sites.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.ncs_and_number_of_ha  import ncs_and_number_of_ha
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''seq.dat perfect.mtz ab.ncs_spec atom_type=I''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  ncs_and_number_of_ha(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)



  local_log_file='test_guess_ncs_and_sites.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.ncs_and_number_of_ha  import ncs_and_number_of_ha
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''seq.dat atom_type=Se unit_cell="50 50 50 90 100 90" space_group=P21''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  ncs_and_number_of_ha(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)


  cmds='''from phenix.command_line.ncs_and_number_of_ha import run
ncs_copies, solvent_fraction, number_of_sites=run(seq_file="seq.dat",data="perfect.mtz",ncs_file="ab.ncs_spec",chain_type="PROTEIN",atom_type="S")
print "NCS COPIES: %d" %(ncs_copies)
print "SOLVENT FRACTION: %7.2f " %(solvent_fraction)
print "NUMBER OF SITES: %s" %(str(number_of_sites))
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print(open('phenix.python.log').read(), file=log)

  cmds='''from phenix.command_line.ncs_and_number_of_ha import run
ncs_copies, solvent_fraction, number_of_sites=run(seq_file="seq.dat",space_group_symbol="P3",unit_cell_as_list="40 40 30 90 90 120",chain_type="PROTEIN",atom_type="BR")
print "NCS COPIES: %d" %(ncs_copies)
print "SOLVENT FRACTION: %7.2f " %(solvent_fraction)
print "NUMBER OF SITES: %s" %(str(number_of_sites))
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print(open('phenix.python.log').read(), file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_guess_ncs_and_sites',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['command_line_misc_tests','test_guess_ncs_and_sites']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
