#!/bin/csh -ef
#$ -cwd
echo "GUESSING NCS and sites with I"
phenix.ncs_and_number_of_ha seq.dat perfect.mtz ab.ncs_spec atom_type=I
if ( $status )then
  echo "FAILED"
  exit 1
endif
echo "GUESSING NCS and sites with Se"
phenix.ncs_and_number_of_ha seq.dat atom_type=Se unit_cell="50 50 50 90 100 90" space_group=P21
if ( $status )then
  echo "FAILED"
  exit 1
endif
echo ""
echo "GUESSING NCS and sites with S"
phenix.python <<EOD
from phenix.command_line.ncs_and_number_of_ha import run
ncs_copies, solvent_fraction, number_of_sites=run(seq_file="seq.dat",data="perfect.mtz",ncs_file="ab.ncs_spec",chain_type="PROTEIN",atom_type="S")
print "NCS COPIES: %d" %(ncs_copies)
print "SOLVENT FRACTION: %7.2f " %(solvent_fraction)
print "NUMBER OF SITES: %s" %(str(number_of_sites))
EOD
if ( $status )then
  echo "FAILED"
  exit 1
endif

echo ""
echo "GUESSING NCS and sites with BR"

phenix.python <<EOD
from phenix.command_line.ncs_and_number_of_ha import run
ncs_copies, solvent_fraction, number_of_sites=run(seq_file="seq.dat",space_group_symbol="P3",unit_cell_as_list="40 40 30 90 90 120",chain_type="PROTEIN",atom_type="BR")
print "NCS COPIES: %d" %(ncs_copies)
print "SOLVENT FRACTION: %7.2f " %(solvent_fraction)
print "NUMBER OF SITES: %s" %(str(number_of_sites))
EOD
if ( $status )then
  echo "FAILED"
  exit 1
endif

