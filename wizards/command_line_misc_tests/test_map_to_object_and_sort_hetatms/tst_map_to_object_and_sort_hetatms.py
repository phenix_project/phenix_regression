from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os,sys
  print("Test script for test_map_to_object_and_sort_hetatms")
  print("")
  log_file=os.path.join(os.getcwd(),"test_map_to_object_and_sort_hetatms.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  from phenix.command_line.map_to_object  import map_to_object
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''fixed_pdb=fixed.pdb moving_pdb=coords1.pdb map_by_chains=true output_pdb=map_by_chains.pdb contact_dist=20''')
  map_to_object(args=args,out=log)

  import difflib
  expected=open('map_by_chains.pdb').readlines()
  found=open('map_by_chains_expected.pdb').readlines()
  for line in difflib.ndiff(expected,found):
    print(line, file=log)


  from phenix.command_line.map_to_object  import map_to_object
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''fixed_pdb=fixed.pdb moving_pdb=coords1.pdb map_by_chains=false output_pdb=no_map_by_chains.pdb contact_dist=20''')
  map_to_object(args=args,out=log)

  import difflib
  expected=open('no_map_by_chains.pdb').readlines()
  found=open('no_map_by_chains_expected.pdb').readlines()
  for line in difflib.ndiff(expected,found):
    print(line, file=log)


  local_log_file='test_map_to_object_and_sort_hetatms.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''fobs.mtz coords1.pdb rebuild_in_place=True touch_up=true quick=true include_input_model=false sort_hetatms=true map_to_object=test.pdb refine_before_rebuild=false input_map_file=resolve_work.mtz place_waters=false ncycle_refine=1 redo_side_chains=false''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)



  from phenix.command_line.superpose_pdbs  import run
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''overall_best_mapped.pdb AutoBuild_run_1_/overall_best_mapped.pdb''')
  run(args=args,log=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_map_to_object_and_sort_hetatms',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['command_line_misc_tests','test_map_to_object_and_sort_hetatms']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
