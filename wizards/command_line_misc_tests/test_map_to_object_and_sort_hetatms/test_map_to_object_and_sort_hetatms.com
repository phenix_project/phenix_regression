#!/bin/csh -ef
#$ -cwd

echo "Map-to-object with map_by_chains=True"
phenix.map_to_object fixed_pdb=fixed.pdb moving_pdb=coords1.pdb map_by_chains=true output_pdb=map_by_chains.pdb contact_dist=20
if ( $status )then
  echo "FAILED"
  exit 1
endif
diff map_by_chains.pdb map_by_chains_expected.pdb 
if ( $status )then
  echo "FAILED"
  exit 1
endif

echo ""
echo "Map-to-object with map_by_chains=False"
phenix.map_to_object fixed_pdb=fixed.pdb moving_pdb=coords1.pdb map_by_chains=false output_pdb=no_map_by_chains.pdb contact_dist=20
if ( $status )then
  echo "FAILED"
  exit 1
endif
diff no_map_by_chains.pdb no_map_by_chains_expected.pdb
if ( $status )then
  echo "FAILED"
  exit 1
endif

echo ""
echo "Map_to_object in autobuild"
phenix.autobuild fobs.mtz coords1.pdb rebuild_in_place=True touch_up=true quick=true include_input_model=false sort_hetatms=true map_to_object= test.pdb refine_before_rebuild=false input_map_file=resolve_work.mtz place_waters=false ncycle_refine=1 redo_side_chains=false
if ( $status )then
  echo "FAILED"
  exit 1
endif
phenix.superpose_pdbs overall_best_mapped.pdb AutoBuild_run_1_/overall_best_mapped.pdb
if ( $status )then
  echo "FAILED"
  exit 1
endif

