from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_cc_ano")
  print("")
  log_file=os.path.join(os.getcwd(),"test_cc_ano.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.get_cc_ano half_dataset_a.mtz half_dataset_b.mtz')
  for line in result.stdout_lines:
    print(line, file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_cc_ano',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['command_line_misc_tests','test_cc_ano']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
