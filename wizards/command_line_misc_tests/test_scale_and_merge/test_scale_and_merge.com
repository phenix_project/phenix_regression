#!/bin/csh -ef
#$ -cwd

phenix.scale_and_merge p22121.sca 
if ( $status )then
  echo "FAILED"
  exit 1
endif

phenix.scale_and_merge test_shelx.eff
if ( $status )then
  echo "FAILED"
  exit 1
endif

phenix.scale_and_merge test.eff
if ( $status )then
  echo "FAILED"
  exit 1
endif


phenix.scale_and_merge unit_cell="113.949   113.949    32.474    90.000    90.000    90.000"  a.sca  resolution=3 anomalous=true optimize_anomalous=false"

phenix.scale_and_merge unit_cell="113.949   113.949    32.474    90.000    90.000    90.000"  a.sca  resolution=3 anomalous=true optimize_anomalous=true"

phenix.scale_and_merge unit_cell="113.949   113.949    32.474    90.000    90.000    90.000"  a.sca  resolution=3 anomalous=false optimize_anomalous=false"

phenix.scale_and_merge unit_cell="113.949   113.949    32.474    90.000    90.000    90.000"  a.sca  resolution=3

phenix.scale_and_merge unit_cell="113.949   113.949    32.474    90.000    90.000    90.000" a.sca half_dataset_cc_by_files=False  resolution=3


