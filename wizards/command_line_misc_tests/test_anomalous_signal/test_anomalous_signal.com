#!/bin/csh -ef
#$ -cwd
phenix.anomalous_signal data=scaled_data.mtz half_dataset_a=half_dataset_a.mtz half_dataset_b=half_dataset_b.mtz sites=3 atom_type=se seq_file=seq.dat
if ( $status )then
  echo "FAILED"
  exit 1
endif

