#!/bin/csh -ef
#$ -cwd
phenix.autobuild two_fofc_denmod_in_rebuild=True native.sca coords.pdb rebuild_in_place=False number_of_parallel_models=1 maps_only=True refine=false
if ( $status )then
  echo "FAILED"
  exit 1
endif
phenix.get_cc_mtz_mtz two_fofc_denmod_map.mtz AutoBuild_run_1_/two_fofc_denmod_map.mtz|grep offsett
