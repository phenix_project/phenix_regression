#!/bin/csh -ef
#$ -cwd

phenix.hyss space_group=p21 peak.sca 2 se random_seed=12471 direct_methods=false max_multiple=1 minimum_reflections_for_phaser=20 rescore=phaser-complete input_emma_model_list=ha.pdb resolution=3.0
if ( $status )then
  echo "FAILED"
  exit 1
endif
phenix.emma ha.pdb peak_hyss_consensus_model.pdb

phenix.hyss space_group=p21 peak.sca 2 se random_seed=12471
if ( $status )then
  echo "FAILED"
  exit 1
endif
phenix.emma ha.pdb peak_hyss_consensus_model.pdb

phenix.hyss wavelength=1.54 space_group=p21 peak.sca 3 I random_seed=15151
if ( $status )then
  echo "FAILED"
  exit 1
endif
phenix.emma ha.pdb peak_hyss_consensus_model.pdb

phenix.hyss rescore=correlation wavelength=1.54 space_group=p21 peak.sca 3 I random_seed=15151 score_only=True
if ( $status )then
  echo "FAILED"
  exit 1
endif
phenix.emma ha.pdb peak_hyss_consensus_model.pdb

