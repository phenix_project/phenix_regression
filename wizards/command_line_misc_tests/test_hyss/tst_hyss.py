from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os,sys
  print("Test script for test_hyss")
  print("")
  log_file=os.path.join(os.getcwd(),"test_hyss.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  from phenix.command_line.hyss  import run
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''space_group=p21 peak.sca 2 se random_seed=12471 direct_methods=false max_multiple=1 minimum_reflections_for_phaser=20 rescore=phaser-complete input_emma_model_list=ha.pdb resolution=3.0''')
  run(args=args,out=log)


  local_log_file='test_hyss.log'
  local_log=open(local_log_file,'w')
  from iotbx.command_line.emma  import run
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''ha.pdb peak_hyss_consensus_model.pdb''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)
  # Read results here

  get_pairs(local_log_file, minimum_pairs = 2)

  from phenix.command_line.hyss  import run
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''space_group=p21 peak.sca 2 se random_seed=12471''')
  run(args=args,out=log)


  local_log_file='test_hyss.log'
  local_log=open(local_log_file,'w')
  from iotbx.command_line.emma  import run
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''ha.pdb peak_hyss_consensus_model.pdb''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)
  get_pairs(local_log_file, minimum_pairs = 2)



  from phenix.command_line.hyss  import run
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''wavelength=1.54 space_group=p21 peak.sca 3 I random_seed=15151''')
  run(args=args,out=log)


  local_log_file='test_hyss.log'
  local_log=open(local_log_file,'w')
  from iotbx.command_line.emma  import run
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''ha.pdb peak_hyss_consensus_model.pdb''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)
  get_pairs(local_log_file, minimum_pairs = 2)



  from phenix.command_line.hyss  import run
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''rescore=correlation wavelength=1.54 space_group=p21 peak.sca 3 I random_seed=15151 score_only=True''')
  run(args=args,out=log)


  local_log_file='test_hyss.log'
  local_log=open(local_log_file,'w')
  from iotbx.command_line.emma  import run
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''ha.pdb peak_hyss_consensus_model.pdb''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)
  get_pairs(local_log_file, minimum_pairs = 2)


  log.close()


  print()
  print('Done with test')
  print('OK')

def get_pairs(local_log_file, minimum_pairs = None):
  lines = open(local_log_file).readlines()
  pairs = None
  for line in lines:
    if pairs: continue
    if line.startswith("  Pairs:") and len(line.split()) == 2:
      pairs = int(line.split()[-1])
  assert pairs >= minimum_pairs



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['command_line_misc_tests','test_hyss']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
