#!/bin/csh -ef
#$ -cwd
phenix.autosol dos.sca dos.seq 2 se remove_aniso=false build=false clean_up=False mask_cycles=1 minor_cycles=1
if ( $status )then
  echo "FAILED"
  exit 1
endif

phenix.autosol dos.sca dos.seq 2 se remove_aniso=true build=false clean_up=False mask_cycles=1 minor_cycles=1
if ( $status )then
  echo "FAILED"
  exit 1
endif

phenix.autosol dos2.sca dos.seq 2 se remove_aniso=true build=false clean_up=False mask_cycles=1 minor_cycles=1
if ( $status )then
  echo "FAILED"
  exit 1
endif

phenix.autosol unix2.sca dos.seq 2 se remove_aniso=true build=false clean_up=False mask_cycles=1 minor_cycles=1
if ( $status )then
  echo "FAILED"
  exit 1
endif

