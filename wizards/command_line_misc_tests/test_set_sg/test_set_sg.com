#!/bin/csh -ef
#$ -cwd
echo "" > tmp.dat

echo "ha_inv.pdb coords.pdb"
phenix.autosol peak.sca seq.dat build=false quick=true have_hand=true resolution=4 skip_xtriage=true remove_aniso=false thorough_denmod=false check_only=true debug=true partpdb_file=coords.pdb sites_file=ha_inv.pdb 

echo "ha_inv.pdb coords_inv.pdb"
phenix.autosol peak.sca seq.dat build=false quick=true have_hand=true resolution=4 skip_xtriage=true remove_aniso=false thorough_denmod=false check_only=true debug=true partpdb_file=coords_inv.pdb sites_file=ha_inv.pdb

echo "ha_inv.pdb None"
phenix.autosol peak.sca seq.dat build=false quick=true have_hand=true resolution=4 skip_xtriage=true remove_aniso=false thorough_denmod=false check_only=true debug=true partpdb_file=None sites_file=ha_inv.pdb 


echo "ha.pdb coords.pdb"
phenix.autosol peak.sca seq.dat build=false quick=true have_hand=true resolution=4 skip_xtriage=true remove_aniso=false thorough_denmod=false check_only=true debug=true partpdb_file=coords.pdb sites_file=ha.pdb 

echo "ha.pdb coords_inv.pdb"
phenix.autosol peak.sca seq.dat build=false quick=true have_hand=true resolution=4 skip_xtriage=true remove_aniso=false thorough_denmod=false check_only=true debug=true partpdb_file=coords_inv.pdb sites_file=ha.pdb

echo "ha.pdb None"
phenix.autosol peak.sca seq.dat build=false quick=true have_hand=true resolution=4 skip_xtriage=true remove_aniso=false thorough_denmod=false check_only=true debug=true partpdb_file=None sites_file=ha.pdb 


echo "None coords.pdb"
phenix.autosol peak.sca seq.dat build=false quick=true have_hand=true resolution=4 skip_xtriage=true remove_aniso=false thorough_denmod=false check_only=true debug=true partpdb_file=coords.pdb sites_file=None 

echo "None coords_inv.pdb"
phenix.autosol peak.sca seq.dat build=false quick=true have_hand=true resolution=4 skip_xtriage=true remove_aniso=false thorough_denmod=false check_only=true debug=true partpdb_file=coords_inv.pdb sites_file=None

echo "None None"
phenix.autosol peak.sca seq.dat build=false quick=true have_hand=true resolution=4 skip_xtriage=true remove_aniso=false thorough_denmod=false check_only=true debug=true partpdb_file=None sites_file=None 

