merge_program_labels=['FP', 'SIGFP', 'FreeR_flag', 'PHIB', 'FOM', 'HLA', 'HLB', 'HLC', 'HLD']
merge_input_labels=['F(+)', 'SIGF(+)', 'R-free-flags(+)', 'PHIB', 'FOM', 'HLA', 'HLB', 'HLC', 'HLD']
refinement_labels_use=['F(+)', 'SIGF(+)', 'R-free-flags(+)']
phased_labels_use=['PHIB', 'FOM', 'HLA', 'HLB', 'HLC', 'HLD']
input_refinement_file='not_unique.mtz'
phased='phased.mtz'
sg='P 21 21 21'
cell=[70.049, 73.796, 123.014, 90.0, 90.0, 90.0]
from cctbx import crystal
from cctbx import sgtbx
from cctbx import uctbx
SpaceGroup=sgtbx.space_group_info(symbol=sg)
UnitCell=uctbx.unit_cell(cell)
crystal_symmetry=crystal.symmetry(
     unit_cell=UnitCell,space_group_info=SpaceGroup)
from phenix.autosol.merge_mtz import merge_mtz
merge_mtz=merge_mtz(file_list=
           [input_refinement_file,phased],
                arrays_list=
             [[refinement_labels_use],[phased_labels_use]],
                output_file='temp.mtz',
           crystal_symmetry=crystal_symmetry)
merge_mtz.select_arrays()
output_labels_guess=merge_mtz.write_arrays()
