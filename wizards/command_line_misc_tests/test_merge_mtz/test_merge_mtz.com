#!/bin/csh -ef
#$ -cwd
phenix.python run.py
if ( $status )then
  echo "FAILED"
  exit 1
endif
phenix.python run_2.py
if ( $status )then
  echo "FAILED"
  exit 1
endif
phenix.remove_aniso refmac.mtz
if ( $status )then
  echo "FAILED"
  exit 1
endif
phenix.mtz.dump remove_aniso.mtz
