input_refinement_file='exp.mtz'
phased='unmerged.mtz'
sg='P622'
cell=[49.7495, 49.7495, 116.928, 90, 90, 120]
from cctbx import crystal
from cctbx import sgtbx
from cctbx import uctbx
SpaceGroup=sgtbx.space_group_info(symbol=sg)
UnitCell=uctbx.unit_cell(cell)
crystal_symmetry=crystal.symmetry(
     unit_cell=UnitCell,space_group_info=SpaceGroup)
from phenix.autosol.merge_mtz import merge_mtz
merge_mtz=merge_mtz(file_list=
           [input_refinement_file,phased],
                arrays_list=[['ALL'],['ALL']],
                output_file='temp.mtz',
           crystal_symmetry=crystal_symmetry)
merge_mtz.select_arrays()
output_labels_guess=merge_mtz.write_arrays()
