#!/bin/csh -ef
#$ -cwd
phenix.import_and_add_free native.sca
if ( $status )then
  echo "FAILED"
  exit 1
endif
phenix.mtz.dump -c working_data_file.mtz 

