from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):

  
  
  # Auto-generated script prepared by create_py.py
  #
  import os,sys
  print("Test script for test_sad_data_from_pdb")
  print("")
  log_file=os.path.join(os.getcwd(),"test_sad_data_from_pdb.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this
  
  
  
  from phenix.command_line.sad_data_from_pdb  import run
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''2g42''')
  run(args=args,out=log)
  
  log.close()
  

  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_sad_data_from_pdb',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')


  
if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['command_line_misc_tests','test_sad_data_from_pdb']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")

