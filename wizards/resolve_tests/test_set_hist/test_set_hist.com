#!/bin/csh  -ef
#$ -cwd
phenix.resolve <<EOD
hklin random.mtz
labin FP=FP PHIB=PHIM
resolution 200 4.5
mask_cycles 1
minor_cycles 1
no_build
solvent_content 0.4
hklperfect perfect.mtz
labperfect FP=FP PHIB=PHIC
use_prob
database_cutoff 6.0
n_refine_rot_trans 4
n_xyz   24  24  24
EOD








