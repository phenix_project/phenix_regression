from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_mask_image")
  print("")
  log_file=os.path.join(os.getcwd(),"test_mask_image.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  cmds='''hklin exptl_fobs_phases_freeR_flags.mtz
labin FP=FP PHIB=PHIM FOM=FOMM
solvent_content 0.6
no_build
resolution 3.0 500.0
ha_file NONE
composite_all
dump_composite
composite_pdb refine.pdb_
composite_pdb_first 1
composite_pdb_last 1
use_wang
no_ha
n_xyz 36 36 36
database 5
no_optimize_ncs
spg_name_use P 1 21 1
start_chain 1   92
group_ca_length 4
group_length 2
n_random_frag 0
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.resolve', cmds,'phenix.resolve.log',None,None,None)
  print(open('phenix.resolve.log').read(), file=log)

  cmds='''hklin exptl_fobs_phases_freeR_flags.mtz
labin FP=FP SIGFP=SIGFP PHIB=PHIM FOM=FOMM
labin HLA=HLAM HLB=HLBM HLC=HLCM HLD=HLDM
labin FreeR_flag=FreeR_flag
hklout image.mtz
solvent_content 0.6
no_build
resolution 3.0 500.0
ha_file NONE
pattern_phase
cc_map_file dump.map
use_wang
no_ha
n_xyz 36 36 36
database 5
no_optimize_ncs
spg_name_use P 1 21 1
start_chain 1   92
group_ca_length 4
group_length 2
n_random_frag 0
get_cc_mask_image
image_only
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.resolve', cmds,'phenix.resolve.log',None,None,None)
  print(open('phenix.resolve.log').read(), file=log)

  cmds='''hklin exptl_fobs_phases_freeR_flags.mtz
labin FP=FP SIGFP=SIGFP PHIB=PHIM FOM=FOMM
labin HLA=HLAM HLB=HLBM HLC=HLCM HLD=HLDM
labin FreeR_flag=FreeR_flag
hklout image.mtz
solvent_content 0.6
no_build
resolution 3.0 500.0
ha_file NONE
pattern_phase
cc_map_file dump.map
use_wang
no_ha
n_xyz 36 36 36
database 5
no_optimize_ncs
spg_name_use P 1 21 1
start_chain 1   92
group_ca_length 4
group_length 2
n_random_frag 0
get_cc_mask
image_only
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.resolve', cmds,'phenix.resolve.log',None,None,None)
  print(open('phenix.resolve.log').read(), file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_mask_image',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['resolve_tests','test_mask_image']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
