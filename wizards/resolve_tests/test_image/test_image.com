#!/bin/csh  -ef
#$ -cwd
phenix.resolve <<EOD
hklin perfect.mtz
labin FP=FP PHIB=PHIC
resolution 200 3.
build_only
superquick_build
seq_file seq.dat
build_image
n_refine_rot_trans 4
no_refine_image
 n_xyz   36  36  36
EOD
head -50 dump.map
cp dump.map combine.map
phenix.resolve<<EOD |grep '[a-z]' |grep -v 'Overall average'
hklin exptl_fobs_phases_freeR_flags.mtz    
labin FP=FP SIGFP=SIGFP FreeR_flag=FreeR_flag   
hklout test.mtz    
solvent_content 0.6    
no_build    
resolution 3.0 500.0     
ha_file NONE   
pattern_phase     
cc_map_file combine.map     
use_wang    
no_ha     
n_xyz 36 36 36      
database 5     
no_optimize_ncs     
spg_name_use P 1 21 1    
start_chain 1   92     
group_ca_length 4   
group_length 2      
n_random_frag 0     
n_refine_rot_trans 4
 n_xyz   36  36  36
EOD
phenix.resolve<<EOD |grep '[a-z]'
hklin test.mtz
labin FP=FP PHIB=PHIM FOM=FOMM
hklperfect image.mtz
labperfect FP=FP PHIB=PHIM FOM=FOMM
no_build
hkl_offset_file offset.mtz
keep_f_mag
noscale
database 5
 n_xyz   36  36  36
EOD







