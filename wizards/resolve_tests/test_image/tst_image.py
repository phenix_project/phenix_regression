from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_image")
  print("")
  log_file=os.path.join(os.getcwd(),"test_image.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  cmds='''hklin perfect.mtz
labin FP=FP PHIB=PHIC
resolution 200 3.
build_only
superquick_build
seq_file seq.dat
build_image
n_refine_rot_trans 4
no_refine_image
n_xyz   36  36  36
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.resolve', cmds,'phenix.resolve.log',None,None,None)
  print(open('phenix.resolve.log').read(), file=log)

  for line in open('dump.map').readlines()[:50]:
    print(line, file=log)

  import shutil
  shutil.copyfile('dump.map','combine.map')

  cmds='''hklin exptl_fobs_phases_freeR_flags.mtz
labin FP=FP SIGFP=SIGFP FreeR_flag=FreeR_flag
hklout test.mtz
solvent_content 0.6
no_build
resolution 3.0 500.0
ha_file NONE
pattern_phase
cc_map_file combine.map
use_wang
no_ha
n_xyz 36 36 36
database 5
no_optimize_ncs
spg_name_use P 1 21 1
start_chain 1   92
group_ca_length 4
group_length 2
n_random_frag 0
n_refine_rot_trans 4
n_xyz   36  36  36
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.resolve', cmds,'phenix.resolve.log',None,None,None)
  print(open('phenix.resolve.log').read(), file=log)

  cmds='''hklin test.mtz
labin FP=FP PHIB=PHIM FOM=FOMM
hklperfect image.mtz
labperfect FP=FP PHIB=PHIM FOM=FOMM
no_build
hkl_offset_file offset.mtz
keep_f_mag
noscale
database 5
n_xyz   36  36  36
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.resolve', cmds,'phenix.resolve.log',None,None,None)
  print(open('phenix.resolve.log').read(), file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_image',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['resolve_tests','test_image']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
