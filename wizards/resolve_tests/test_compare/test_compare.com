#!/bin/csh  -ef
#$ -cwd
phenix.resolve <<EOD
hklin 1BPI.pdb.mtz
labin FP=FMODEL PHIB=PHIFMODEL
resolution 200 4
pdb_in all.pdb
compare_pdb
no_sort
seq_file NONE
compare_file 1BPI.pdb
n_xyz  60  24  24
EOD
phenix.resolve<<EOD
hklin 1BPI.pdb.mtz
labin FP=FMODEL PHIB=PHIFMODEL
resolution 200 4
pdb_in all.pdb
no_sort
compare_file 1BPI.pdb
compare_pdb
n_xyz  60  24  24
EOD
phenix.resolve<<EOD
hklin 1BPI.pdb.mtz
labin FP=FMODEL PHIB=PHIFMODEL
resolution 200 4
pdb_in a.pdb
no_sort
compare_file b.pdb
compare_pdb_flip
n_xyz  60  24  24
EOD
phenix.resolve<<EOD
hklin 1BPI.pdb.mtz
labin FP=FMODEL PHIB=PHIFMODEL
resolution 200 4
pdb_in a.pdb
no_sort
compare_file a.pdb
compare_pdb_flip
n_xyz  60  24  24
EOD

