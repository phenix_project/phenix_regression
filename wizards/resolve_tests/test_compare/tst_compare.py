from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_compare")
  print("")
  log_file=os.path.join(os.getcwd(),"test_compare.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  cmds='''hklin 1BPI.pdb.mtz
labin FP=FMODEL PHIB=PHIFMODEL
resolution 200 4
pdb_in all.pdb
compare_pdb
no_sort
seq_file NONE
compare_file 1BPI.pdb
n_xyz  60  24  24
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.resolve', cmds,'phenix.resolve.log',None,None,None)
  print(open('phenix.resolve.log').read(), file=log)

  cmds='''hklin 1BPI.pdb.mtz
labin FP=FMODEL PHIB=PHIFMODEL
resolution 200 4
pdb_in all.pdb
no_sort
compare_file 1BPI.pdb
compare_pdb
n_xyz  60  24  24
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.resolve', cmds,'phenix.resolve.log',None,None,None)
  print(open('phenix.resolve.log').read(), file=log)

  cmds='''hklin 1BPI.pdb.mtz
labin FP=FMODEL PHIB=PHIFMODEL
resolution 200 4
pdb_in a.pdb
no_sort
compare_file b.pdb
compare_pdb_flip
n_xyz  60  24  24
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.resolve', cmds,'phenix.resolve.log',None,None,None)
  print(open('phenix.resolve.log').read(), file=log)

  cmds='''hklin 1BPI.pdb.mtz
labin FP=FMODEL PHIB=PHIFMODEL
resolution 200 4
pdb_in a.pdb
no_sort
compare_file a.pdb
compare_pdb_flip
n_xyz  60  24  24
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.resolve', cmds,'phenix.resolve.log',None,None,None)
  print(open('phenix.resolve.log').read(), file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_compare',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['resolve_tests','test_compare']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
