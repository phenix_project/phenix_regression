#!/bin/csh  -ef 
#$ -cwd
phenix.resolve --multi<<EOD |grep '[a-z]'   # eliminate raw numbers
hklin phaser_1_offset_edited.mtz
labin FP=FP SIGFP=SIGFP PHIB=PHIM FOM=FOMM HLA=HLAM 
LABIN HLB=HLBM HLC=HLCM HLD=HLDM
hklout denmod_cycle_1_xl_0.mtz

solvent_content 0.43
no_build
mask_cycles 1



ha_file NONE




new_ncs_group

rota_matrix    1.0000    0.0000    0.0000
rota_matrix    0.0000    1.0000    0.0000
rota_matrix    0.0000    0.0000    1.0000
tran_orth     0.0000    0.0000    0.0000
center_orth    8.2211    7.3029    7.9156
crystal_number 0

rota_matrix    0.2625    0.4428    0.8573
rota_matrix    0.8666   -0.4989   -0.0076
rota_matrix    0.4243    0.7450   -0.5147
tran_orth   -22.3655  -24.5075  -16.1695
center_orth   45.8159   15.6179   13.5839
crystal_number 1
rota_matrix   -0.5633   -0.2836   -0.7761
rota_matrix    0.7345   -0.6020   -0.3131
rota_matrix   -0.3784   -0.7465    0.5474
tran_orth    32.1702  -15.8041   14.9179
center_orth   33.1123   -1.8922    7.5181
crystal_number 1


crystal_map dummy_map_cycle_1_xl_0.mtz
labin_asis  labin FP=FP PHIB=PHIM FOM=FOMM












use_wang
no_ha


hklstart resolve_1_offset_edited.mtz
labstart FP=FP PHIB=PHIM FOM=FOMM 





database 5


no_optimize_ncs


n_refine_rot_trans 4
EOD








