#!/bin/csh -ef
#$ -cwd
phenix.resolve<<EOD
hklin image_only_dm.mtz     
labin FP=FP PHIB=PHIM FOM=FOMM   
no_build    
ha_file NONE   
seq_file seq.dat    
morph morph.pdb     
rad_morph 7.0     
use_wang    
no_ha     
n_xyz 36 36 36      
database 5     
no_optimize_ncs     
spg_name_use P 1    
start_chain 1   92     
group_ca_length 4   
group_length 2      
n_random_frag 0     
EOD
cat resolve.pdb

