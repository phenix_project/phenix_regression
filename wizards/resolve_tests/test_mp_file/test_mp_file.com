#!/bin/csh -ef
#$ -cwd
phenix.resolve<<EOD
hklin resolve_work.mtz   
labin FP=FP PHIB=PHIM FOM=FOMM   
extend_only    
resolution 1000 3.0    
ha_file NONE   
seq_file seq.dat    
ok_molp_score -0.4     
i_ran_seed 201653   
dist_close 0.5      
scale_molp_score 4.0     
mp_file_extra refine_1.pdb.quality    
mp_file composite_model.pdb.quality   
pdb_in composite_model.pdb    
group_ca_length 4   
cross     
zero_incomplete     
pdb_in_extra refine_1.pdb   
skip_hetatm    
use_wang    
no_ha     
n_xyz 36 36 36      
database 5     
use_any_side   
no_merge_ncs_copies    
no_optimize_ncs     
nohl      
spg_name_use P 1 21 1    
start_chain 1   92     
group_length 2      
n_random_frag 0     
EOD

