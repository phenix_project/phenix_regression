#!/bin/csh -ef
#$ -cwd
phenix.resolve<<EOD
hklin resolve_map.mtz    
labin FP=FP PHIB=PHIM FOM=FOMM   
no_build    
ha_file NONE   
model all.pdb     
start_rot 350 60 334     
use_wang    
no_ha     
delta_phi_ligand 40.0    
ligand_file side6.pdb   
n_indiv_tries_min 3
n_indiv_tries_max 3
n_group_search 2
group_search 0      
fit_phi_inc 20.0    
n_keep_plac 100     
ligand_resno 1      
n_template_atom 6
fit_phi_range -180.0 180.0    
search_center 0.0 0.0 0.0   
database 5     
no_optimize_ncs     
spg_name_use P 1 21 1    
n_xyz   36  36  36
EOD
phenix.resolve<<EOD
hklin resolve_map.mtz    
labin FP=FP PHIB=PHIM FOM=FOMM   
no_build    
RESOLUTION 1000.0 3.0    
ha_file NONE   
model ligand_fit.pdb    
evaluate_ligand     
use_wang    
no_ha     
database 5     
no_optimize_ncs     
spg_name_use P 1 21 1    
n_xyz   36  36  36
EOD
phenix.resolve<<EOD
hklin resolve_map.mtz    
labin FP=FP PHIB=PHIM FOM=FOMM   
no_build    
RESOLUTION 1000.0 3.0    
ha_file NONE   
model ligand_fit.pdb    
evaluate_model
use_wang    
no_ha     
database 5     
no_optimize_ncs     
spg_name_use P 1 21 1    
n_xyz   36  36  36
EOD

