from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_composite_pdb")
  print("")
  log_file=os.path.join(os.getcwd(),"test_composite_pdb.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  cmds='''hklin exptl_phases_and_amplitudes_for_density_modification.mtz
labin FP=FP SIGFP=SIGFP PHIB=PHIM FOM=FOMM
labin HLA=HLAM HLB=HLBM HLC=HLCM HLD=HLDM
labin FreeR_flag=FreeR_flag
hklout resolve_work.mtz
solvent_content 0.46
no_build
mask_cycles 0
resolution 3.5 500.0
ha_file NONE
seq_file sequence.dat
image
composite_all
scale_refl 1.0
composite_pdb refine.pdb_
composite_pdb_first 4
composite_pdb_last 6
pdb_in refine.pdb_6
use_hist_prob
no_ha
hklstart image_only_dm.mtz
labstart FP=FP PHIB=PHIM FOM=FOMM
n_xyz 96 36 60
database 5
use_any_side
no_optimize_ncs
nohl
spg_name_use C 1 2 1
start_chain 1 1
group_ca_length 4
group_length 2
n_random_frag 0
composite_unique
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.resolve', cmds,'phenix.resolve.log',None,None,None)
  print(open('phenix.resolve.log').read(), file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_composite_pdb',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['resolve_tests','test_composite_pdb']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
