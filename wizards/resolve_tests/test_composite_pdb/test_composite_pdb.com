#!/bin/csh -ef
#$ -cwd
phenix.resolve<<EOD |grep -v ' 3.5 '
hklin exptl_phases_and_amplitudes_for_density_modification.mtz    
labin FP=FP SIGFP=SIGFP PHIB=PHIM FOM=FOMM    
labin HLA=HLAM HLB=HLBM HLC=HLCM HLD=HLDM  
labin FreeR_flag=FreeR_flag   
hklout resolve_work.mtz     
solvent_content 0.46     
no_build    
mask_cycles 0     
resolution 3.5 500.0     
ha_file NONE   
seq_file sequence.dat    
image     
composite_all     
scale_refl 1.0      
composite_pdb refine.pdb_   
composite_pdb_first 4    
composite_pdb_last 6     
pdb_in refine.pdb_6    
use_hist_prob     
no_ha     
hklstart image_only_dm.mtz    
labstart FP=FP PHIB=PHIM FOM=FOMM     
n_xyz 96 36 60      
database 5     
use_any_side   
no_optimize_ncs     
nohl      
spg_name_use C 1 2 1     
start_chain 1 1     
group_ca_length 4   
group_length 2      
n_random_frag 0     
composite_unique
EOD

