#!/bin/csh -ef
#$ -cwd
phenix.resolve<<EOD|grep '[a-z]' # eliminates some numbers that vary a bit
hklin solve_1.mtz
labin FP=FP SIGFP=SIGFP PHIB=PHIB FOM=FOM 
dump_mask
no_build
solvent_content 0.5
database 5
mask_cycles 1
minor_cycles 1
highest_mask 0.50 3.0
EOD

