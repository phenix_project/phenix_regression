#!/bin/csh -ef
#$ -cwd
phenix.resolve<<EOD
!hklin resolve_work.mtz   
!labin FP=FP PHIB=PHIM FOM=FOMM   
hklin all.mtz
labin FP=FWT PHIB=PHIFWT
extend_only    
resolution 1000 3
ha_file NONE   
seq_file seq_from_pdb.dat   
skip_hetatm    
pdb_in short.pdb      
group_ca_length 3
!cut_up_model   
use_wang    
no_ha     
database 5     
use_any_side   
no_merge_ncs_copies    
no_optimize_ncs     
nohl      
spg_name_use P 1    
start_chain 1 2     
start_chain 2   22     
build_rna      
compare_all    
group_length 5      
rho_min_main_low -0.0   
start_segment 5     
dist_ca_approach 4.0     
max_segment 5     
r_match 2.5    
use_any_side   
rho_min_side_seg -1.0   
group_ca_length 3   
rho_min_side -1.0     
rho_min_main_base 1.0     
dist_cut_assemble 1.0    
rho_min_main_seg -1.0   
rms_random_frag 0.5    
d_cut_fragment 0.01 0.01    
!trim      
n_random_frag 0     
dump_frags 2
EOD

