#!/bin/csh  -ef
#$ -cwd
phenix.resolve --multi_huge <<EOD
hklin multi.pdb_AU.pdb_f_data_1_ed_aniso.mtz
labin FP=FP_aniso SIGFP=SIGFP_aniso
hklout denmod_cycle_1_xl_1.mtz
solvent_content 0.5
no_build
n_xyz 108 108  36
no_free
mask_cycles 1
minor_cycles 1
ncs_mask_file ncs_mask_file.ccp4
fix_ncs
resolution 200 2.5
ha_file NONE
new_ncs_group
ncs_domain_pdb multi_A_1.pdb_group_1.pdb
rota_matrix 1.0000 0.0000 0.0000
rota_matrix 0.0000 1.0000 0.0000
rota_matrix 0.0000 0.0000 1.0000
tran_orth  0.0000 0.0000 0.0000
center_orth   45.8159   15.6179   13.5839
crystal_number 0
rota_matrix 0.2625 0.8666 0.4243
rota_matrix 0.4428   -0.4989 0.7450
rota_matrix 0.8573   -0.0076   -0.5147
tran_orth 33.9703 9.7237   10.6655
center_orth 8.2211 7.3029 7.9156
crystal_number 1
fill
res_fill   2.50
keep_missing
crystal_map gene-5_I.pdb_map_coeffs_2_ed.mtz
labin_asis  labin FP=FP PHIB=PHIM
force_ncs_boundary
overlap_min 0.001
fraction_ncs_min 0.001
use_wang
no_ha
hklstart multi.pdb_AU.pdb_map_coeffs_1_ed.mtz
labstart FP=FP PHIB=PHIM
database 5
no_optimize_ncs
no_create_free
use_all_for_test
EOD

