from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_resolve_multi_c")
  print("")
  log_file=os.path.join(os.getcwd(),"test_resolve_multi_c.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  cmds='''hklin multi.pdb_AU.pdb_f_data_1_ed_aniso.mtz
labin FP=FP_aniso SIGFP=SIGFP_aniso
hklout denmod_cycle_1_xl_1.mtz
solvent_content 0.5
no_build
n_xyz 108 108  36
no_free
mask_cycles 1
minor_cycles 1
ncs_mask_file ncs_mask_file.ccp4
fix_ncs
resolution 200 2.5
ha_file NONE
new_ncs_group
ncs_domain_pdb multi_A_1.pdb_group_1.pdb
rota_matrix 1.0000 0.0000 0.0000
rota_matrix 0.0000 1.0000 0.0000
rota_matrix 0.0000 0.0000 1.0000
tran_orth  0.0000 0.0000 0.0000
center_orth   45.8159   15.6179   13.5839
crystal_number 0
rota_matrix 0.2625 0.8666 0.4243
rota_matrix 0.4428   -0.4989 0.7450
rota_matrix 0.8573   -0.0076   -0.5147
tran_orth 33.9703 9.7237   10.6655
center_orth 8.2211 7.3029 7.9156
crystal_number 1
fill
res_fill   2.50
keep_missing
crystal_map gene-5_I.pdb_map_coeffs_2_ed.mtz
labin_asis  labin FP=FP PHIB=PHIM
force_ncs_boundary
overlap_min 0.001
fraction_ncs_min 0.001
use_wang
no_ha
hklstart multi.pdb_AU.pdb_map_coeffs_1_ed.mtz
labstart FP=FP PHIB=PHIM
database 5
no_optimize_ncs
no_create_free
use_all_for_test
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.resolve', cmds,'phenix.resolve.log',None,None,None)
  print(open('phenix.resolve.log').read(), file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_resolve_multi_c',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['resolve_tests','test_resolve_multi_c']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
