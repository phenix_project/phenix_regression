#!/bin/csh -f
phenix.resolve<<EOD
freer_if_present
hklin TEMP.mtz
labin FP=FP FC=FMODEL PHIC=PH2FOFCWT FWT=2FOFCWT
hklout prime_and_switch.mtz
solvent_content 0.52
no_build
RESOLUTION 1000.0 2.3002
ha_file NONE
prime_and_switch
mask_cycles 1
minor_cycles 3
use_wang
no_ha
n_xyz 96 54 32
database 5
no_optimize_ncs
spg_name_use P 21 21 21
min_z_value_rho -3.0
no_create_free
use_all_for_test
delta_phi   20.00
dist_cut_base 3.0
free_id 0
group_ca_length 4
group_length 2
n_random_frag 0
nohl
EOD
