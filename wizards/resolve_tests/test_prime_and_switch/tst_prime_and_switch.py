from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_prime_and_switch")
  print("")
  log_file=os.path.join(os.getcwd(),"test_prime_and_switch.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  cmds='''hklin TEMP.mtz
labin FP=FP FC=FMODEL PHIC=PH2FOFCWT FWT=2FOFCWT
hklout prime_and_switch.mtz
solvent_content 0.6
no_build
ha_file NONE
prime_and_switch
mask_cycles 2
minor_cycles 4
use_wang
no_ha
n_xyz 36 36 36
database 5
no_optimize_ncs
spg_name_use P 1
start_chain 1   92
group_ca_length 4
group_length 2
n_random_frag 0
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.resolve', cmds,'phenix.resolve.log',None,None,None)
  print(open('phenix.resolve.log').read(), file=log)

  cmds='''hklin prime_and_switch.mtz
labin FP=FP PHIB=PHIM FOM=FOMM
hklperfect prime_and_switch_std.mtz
labperfect FP=FP PHIB=PHIM FOM=FOMM
no_build
hkl_offset_file offset.mtz
keep_f_mag
noscale
database 5
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.resolve', cmds,'phenix.resolve.log',None,None,None)
  print(open('phenix.resolve.log').read(), file=log)

  cmds='''freer_if_present
hklin TEMP.mtz
labin FP=FP FC=FMODEL PHIC=PH2FOFCWT FWT=2FOFCWT
hklout prime_and_switch.mtz
solvent_content 0.52
no_build
RESOLUTION 1000.0 2.3002
ha_file NONE
prime_and_switch
mask_cycles 1
minor_cycles 3
use_wang
no_ha
n_xyz 96 54 32
database 5
no_optimize_ncs
spg_name_use P 21 21 21
min_z_value_rho -3.0
no_create_free
use_all_for_test
delta_phi   20.00
dist_cut_base 3.0
free_id 0
group_ca_length 4
group_length 2
n_random_frag 0
nohl
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.resolve', cmds,'phenix.resolve.log',None,None,None)
  print(open('phenix.resolve.log').read(), file=log)

  cmds='''hklin prime_and_switch.mtz
labin FP=FP PHIB=PHIM FOM=FOMM
hklperfect prime_and_switch_std2.mtz
labperfect FP=FP PHIB=PHIM FOM=FOMM
no_build
hkl_offset_file offset.mtz
keep_f_mag
noscale
database 5
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.resolve', cmds,'phenix.resolve.log',None,None,None)
  print(open('phenix.resolve.log').read(), file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_prime_and_switch',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['resolve_tests','test_prime_and_switch']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
