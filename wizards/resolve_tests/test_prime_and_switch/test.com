#!/bin/csh -ef
#$ -cwd
phenix.resolve<<EOD|grep -v 'Protein:'|grep '[a-z]' # only numbers with chars
hklin TEMP.mtz      
labin FP=FP FC=FMODEL PHIC=PH2FOFCWT FWT=2FOFCWT   
hklout prime_and_switch.mtz   
solvent_content 0.6    
no_build    
ha_file NONE   
prime_and_switch    
mask_cycles 2
minor_cycles 4
use_wang    
no_ha     
n_xyz 36 36 36      
database 5     
no_optimize_ncs     
spg_name_use P 1    
start_chain 1   92     
group_ca_length 4   
group_length 2      
n_random_frag 0     
EOD
phenix.resolve<<EOD
hklin prime_and_switch.mtz
labin FP=FP PHIB=PHIM FOM=FOMM
hklperfect prime_and_switch_std.mtz
labperfect FP=FP PHIB=PHIM FOM=FOMM
no_build
hkl_offset_file offset.mtz
keep_f_mag
noscale
database 5
EOD

echo "testing prime and switch on toxd where scaling of fwt matters"
./ps.csh

phenix.resolve<<EOD
hklin prime_and_switch.mtz
labin FP=FP PHIB=PHIM FOM=FOMM
hklperfect prime_and_switch_std2.mtz
labperfect FP=FP PHIB=PHIM FOM=FOMM
no_build
hkl_offset_file offset.mtz
keep_f_mag
noscale
database 5

