from __future__ import division
import sys

if (__name__ == "__main__") :
  input_file=sys.argv[1]
  if len(sys.argv)>2:
    resolve_size=int(sys.argv[2])
  else:
    resolve_size=18
  inputs=open(sys.argv[1]).read()
  cerr = sys.stdout
  cout = sys.stdout
  from solve_resolve import resolve
  from boost_adaptbx.boost.python import streambuf

  from solve_resolve.resolve import helper
  resolve_libs=helper.resolve_libs() # empty libs, filled on call to resolve

  cmn=resolve.call(
    inputs,
    err_streambuf=streambuf(cerr, buffer_size=1),
    out_streambuf=streambuf(cout, buffer_size=1),
    isizeit=resolve_size,
    n_crystal_map_max=1,
    resolve_libs=resolve_libs)
