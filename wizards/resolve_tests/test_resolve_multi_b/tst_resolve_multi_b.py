from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_resolve_multi_b")
  print("")
  log_file=os.path.join(os.getcwd(),"test_resolve_multi_b.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  cmds='''hklin multi.pdb_edited.mtz
labin FP=FP
hklout denmod_cycle_1_xl_1.mtz
solvent_content 0.50
no_build
mask_cycles 1
minor_cycles 1
resolution 200 3.5
ha_file NONE
new_ncs_group
rota_matrix 1.0000 0.0000 0.0000
rota_matrix 0.0000 1.0000 0.0000
rota_matrix 0.0000 0.0000 1.0000
tran_orth  0.0000 0.0000 0.0000
center_orth   45.8159   15.6179   13.5839
crystal_number 0
rota_matrix 0.3282   -0.9129   -0.2428
rota_matrix   -0.8978   -0.3813 0.2203
rota_matrix   -0.2937 0.1457   -0.9447
tran_orth 35.0476   42.9681   30.6875
center_orth   33.1123   -1.8922 7.5181
crystal_number 0
rota_matrix 0.2625 0.8666 0.4243
rota_matrix 0.4428   -0.4989 0.7450
rota_matrix 0.8573   -0.0076   -0.5147
tran_orth 33.9703 9.7237   10.6655
center_orth 8.2211 7.3029 7.9156
crystal_number 1
crystal_map denmod_cycle_1_xl_0.mtz
labin_asis  labin FP=FP PHIB=PHIM FOM=FOMM
use_wang
no_ha
hklstart cycle_1_xl_1_dump_pattern_phase.mtz
labstart FP=FP PHIB=PHIM FOM=FOMM
database 5
no_optimize_ncs
n_refine_rot_trans 4
n_xyz   72  36  48
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.resolve', cmds,'phenix.resolve.log',None,None,None)
  print(open('phenix.resolve.log').read(), file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_resolve_multi_b',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['resolve_tests','test_resolve_multi_b']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
