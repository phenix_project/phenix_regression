#!/bin/csh -ef
#$ -cwd
phenix.resolve<<EOD
hklin exptl_fobs_phases_freeR_flags.mtz    
labin FP=FP SIGFP=SIGFP FreeR_flag=FreeR_flag   
solvent_content 0.77     
no_build    
mask_cycles 0     
minor_cycles 0      
resolution 3.5 500.0     
ha_file NONE   
seq_file seq_from_pdb.dat   
pdb_in TMP.pdb      
no_sort
find_omit_region    
omit_mask_file omit_region.mtz   
no_reuse_model      
mask_as_mtz    
offset_boundary 2.0    
use_wang    
no_ha     
n_xyz 192 192 72    
database 5     
no_optimize_ncs     
spg_name_use P 6    
start_chain 1  593     
group_ca_length 1   
group_length 2      
n_random_frag 0     
EOD

