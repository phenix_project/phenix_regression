from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_omit_box_id")
  print("")
  log_file=os.path.join(os.getcwd(),"test_omit_box_id.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  cmds='''hklin exptl_fobs_phases_freeR_flags.mtz
labin FP=FP SIGFP=SIGFP FreeR_flag=FreeR_flag
solvent_content 0.77
no_build
mask_cycles 0
minor_cycles 0
resolution 3.5 500.0
ha_file NONE
seq_file seq_from_pdb.dat
pdb_in TMP.pdb
no_sort
find_omit_region
omit_mask_file omit_region.mtz
no_reuse_model
mask_as_mtz
offset_boundary 2.0
use_wang
no_ha
n_xyz 192 192 72
database 5
no_optimize_ncs
spg_name_use P 6
start_chain 1  593
group_ca_length 1
group_length 2
n_random_frag 0
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.resolve', cmds,'phenix.resolve.log',None,None,None)
  print(open('phenix.resolve.log').read(), file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_omit_box_id',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['resolve_tests','test_omit_box_id']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
