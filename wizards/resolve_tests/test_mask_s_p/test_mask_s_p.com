#!/bin/csh -ef
#$ -cwd
phenix.resolve<<EOD
hklin exptl_fobs_phases_freeR_flags.mtz    
labin FP=FP SIGFP=SIGFP PHIB=PHIM FOM=FOMM    
labin HLA=HLAM HLB=HLBM HLC=HLCM HLD=HLDM  
solvent_content 0.6    
no_build    
resolution 3.0 500.0     
ha_file NONE   
use_wang    
no_ha     
n_xyz 36 36 36      
database 5     
no_optimize_ncs     
spg_name_use P 1 21 1    
start_chain 1   92     
group_ca_length 4   
group_length 2      
n_random_frag 0     
use_new_prob
test_extra
mask_cycles 1 
minor_cycles 0
EOD

