#!/bin/csh  -ef
#$ -cwd
phenix.resolve <<EOD
hklin perfect.mtz
labin FP=FP PHIB=PHIC
resolution 200 3.0
demo_special
n_refine_rot_trans 4
n_xyz   36  36  36
EOD
phenix.resolve<<EOD |grep CC
hklin special.mtz
labin FP=FP PHIB=PHIM FOM=FOMM
hklperfect special_std.mtz
labperfect FP=FP PHIB=PHIM FOM=FOMM
no_build
hkl_offset_file offset.mtz
keep_f_mag
noscale
database 5
n_xyz   36  36  36
EOD


