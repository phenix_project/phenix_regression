#!/bin/csh -ef
#$ -cwd
phenix.resolve<<EOD
hklin perfect.mtz
labin FP=FP PHIB=PHIC
build_only
n_build_repeat 2
n_xyz   36  36  36
EOD

