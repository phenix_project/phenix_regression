#!/bin/csh  -ef
#$ -cwd
phenix.resolve <<EOD
hklin 1BPI.pdb.mtz
labin FP=FMODEL PHIB=PHIFMODEL
pdb_in all.pdb
extend_only
macro_cycles 1
n_cycle_build 1
seq_file NONE
pdb_in_extra coords.pdb
n_refine_rot_trans 4
n_xyz   72  24  36
EOD








