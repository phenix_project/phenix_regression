#!/bin/csh -ef
#$ -cwd
phenix.resolve<<EOD
hklin TEMP_perfect.mtz   
labin FP=FP PHIB=PHIC    
no_build    
ha_file NONE   
pdb_in TEMP_coords.pdb   
copy_pdb
omit
omit_box 3
n_xyz   36  36  36
n_box_target 12
EOD
grep '0\.00' resolve.pdb

