from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_fit")
  print("")
  log_file=os.path.join(os.getcwd(),"test_fit.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  cmds='''hklin resolve_map.mtz
labin FP=FP PHIB=PHIM FOM=FOMM
no_build
ha_file NONE
model all.pdb
start_rot 239 348 290
use_wang
no_ha
delta_phi_ligand 40.0
ligand_file side.pdb
n_indiv_tries_min 5
n_indiv_tries_max 10
n_group_search 1
group_search 0
fit_phi_inc 20.0
n_keep_plac 100
ligand_resno 1
n_template_atom 4
fit_phi_range -180.0 180.0
search_center 0.0 0.0 0.0
database 5
no_optimize_ncs
spg_name_use P 1 21 1
n_refine_rot_trans 4
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.resolve', cmds,'phenix.resolve.log',None,None,None)
  print(open('phenix.resolve.log').read(), file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_fit',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['resolve_tests','test_fit']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
