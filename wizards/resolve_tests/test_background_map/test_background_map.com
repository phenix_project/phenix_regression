#!/bin/csh -ef
#$ -cwd
phenix.resolve<<EOD
hklin exptl_fobs_phases_freeR_flags.mtz    
labin FP=FP SIGFP=SIGFP PHIB=PHIM
solvent_content 0.77     
no_build    
resolution 3.5 500.0     
ha_file NONE   
omit_box 1     
no_complete_omit    
no_protein_target   
omit_offset 40 70 48 82 48 64    
combine_map overall_best_denmod_map_coeffs.mtz_OMIT_REGION_1 
background_map 
background_offset 0.5
scale_background 0.7
omit      
use_hist_prob     
no_ha     
n_xyz 192 192 72    
database 5     
no_optimize_ncs     
spg_name_use P 6    
start_chain 1  593     
group_ca_length 1   
group_length 2      
n_random_frag 0     
EOD
phenix.resolve<<EOD
hklin resolve_composite_map.mtz
labin FP=FP PHIB=PHIM FOM=FOMM
hklperfect resolve_composite_map_std.mtz
labperfect FP=FP PHIB=PHIM FOM=FOMM
no_build
hkl_offset_file offset.mtz
keep_f_mag
noscale
database 5
EOD


