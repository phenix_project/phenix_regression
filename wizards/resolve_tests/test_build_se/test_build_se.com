#!/bin/csh  -ef
#$ -cwd
phenix.resolve <<EOD
hklin perfect.mtz
labin FP=FP PHIB=PHIC
resolution 200 3.
build_only
superquick_build
macro_cycles 0
n_cycle_build 0
seq_file seq.dat
se_file ha.pdb
n_refine_rot_trans 4
n_xyz   36  36  36
EOD








