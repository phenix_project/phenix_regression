#!/bin/csh  -ef
#$ -cwd
phenix.resolve <<EOD|grep '[a-z]' # focus on numbers with chars in same line
hklin random.mtz
labin FP=FP PHIB=PHIM
resolution 200 4.5
mask_cycles 1
minor_cycles 1
no_build
database 5
hklperfect perfect.mtz
labperfect FP=FP PHIB=PHIC
use_prob
n_refine_rot_trans 4
verbose
 n_xyz   24  24  24
EOD








