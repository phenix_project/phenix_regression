#!/bin/csh  -ef
#$ -cwd
phenix.resolve<<EOD
hklin resolve_work.mtz   
labin FP=FP PHIB=PHIM FOM=FOMM   
extend_only    
resolution 1000 3.0    
ha_file NONE   
seq_file seq.dat    
richardson_rotamers True    
replace_existing True    
i_ran_seed 215322   
no_merge_ncs_copies True    
n_try_rebuild 1     
keep_main True      
skip_hetatm True    
rebuild_in_place True    
pdb_in Build_composite_refined_3.pdb  
skip_hetatm    
omit_box 1     
no_complete_omit    
no_protein_target   
omit_offset 0 0 0 0 0 0     
use_wang    
no_ha     
n_xyz 36 36 36      
database 5     
no_merge_ncs_copies    
no_optimize_ncs     
nohl      
spg_name_use P 1 21 1    
start_chain 1   92     
group_ca_length 4   
group_length 2      
n_random_frag 0     
ha_file ha.pdb
n_refine_rot_trans 4
n_random_side 4
n_box_target 12
EOD








