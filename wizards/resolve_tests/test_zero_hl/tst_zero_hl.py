from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_zero_hl")
  print("")
  log_file=os.path.join(os.getcwd(),"test_zero_hl.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  cmds='''freer_if_present
hklin phaser_1.mtz
labin FP=FP PHIB=PHIB FOM=FOM HLA=HLanomA HLB=HLanomB HLC=HLanomC
LABIN HLD=HLanomD
hklout resolve_1.mtz
solvent_content 0.85
no_build
mask_cycles 1
minor_cycles 1
RESOLUTION 1000.0 2.499
ha_file NONE
seq_file seq.dat
hklstart phaser_1_fwt.mtz
labstart FP=FP PHIB=PHIM FOM=FOMM
phases_from_solve
modify
rad_mask 5.0
model ha_1.pdb_formatted.pdb
use_hist_prob
no_ha
database 5
no_optimize_ncs
nohl
spg_name_use P 1 21 1
no_create_free
use_all_for_test
group_ca_length 4
group_length 2
n_random_frag 0
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.resolve', cmds,'phenix.resolve.log',None,None,None)
  print(open('phenix.resolve.log').read(), file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_zero_hl',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['resolve_tests','test_zero_hl']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
