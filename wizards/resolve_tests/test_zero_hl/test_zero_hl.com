#!/bin/csh  -ef
#$ -cwd
phenix.resolve<<EOD
freer_if_present
hklin phaser_1.mtz
labin FP=FP PHIB=PHIB FOM=FOM HLA=HLanomA HLB=HLanomB HLC=HLanomC
LABIN HLD=HLanomD
hklout resolve_1.mtz
solvent_content 0.85
no_build
mask_cycles 1
minor_cycles 1
RESOLUTION 1000.0 2.499
ha_file NONE
seq_file seq.dat
hklstart phaser_1_fwt.mtz
labstart FP=FP PHIB=PHIM FOM=FOMM
phases_from_solve
modify
rad_mask 5.0
model ha_1.pdb_formatted.pdb
use_hist_prob
no_ha
database 5
no_optimize_ncs
nohl
spg_name_use P 1 21 1
no_create_free
use_all_for_test
group_ca_length 4
group_length 2
n_random_frag 0
EOD
