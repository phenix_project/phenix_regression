#!/bin/csh -ef
#$ -cwd
phenix.resolve<<EOD
hklin TEMP_a2u-sigmaa.mtz   
labin FP=FWT PHIB=PHIC   
no_build    
ha_file NONE   
i_ran_seed 7077     
resolution 1000 7.0    
find_helices   
rho_min_main_low -100    
rho_min_side_seg -100    
mask_cycles 1     
minor_cycles 0      
solvent_content 0.5    
use_wang    
no_ha     
database 5     
no_optimize_ncs     
EOD
cat helix_loc.dat

