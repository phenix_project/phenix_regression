#!/bin/csh -ef
#$ -cwd
phenix.resolve <<EOD
hklin solve_1.mtz
resolution 200 3.
dump_map
EOD
cp dump.map solve_1.map
phenix.resolve <<EOD
hklin solve_2.mtz
resolution 200 3.
dump_map
EOD
cp dump.map solve_2.map
phenix.resolve<<EOD
resolution 200 3.
hklin solve_1.mtz
labin FP=FP SIGFP=SIGFP PHIB=PHIB FOM=FOM 
combine_map solve_1.map
combine_map solve_2.map
database 5
no_build
EOD



