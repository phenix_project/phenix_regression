#!/bin/csh -ef
#$ -cwd
phenix.resolve<<EOD
hklin resolve_map.mtz    
labin FP=FP PHIB=PHIM FOM=FOMM   
no_build    
ha_file NONE   
model all.pdb     
start_rot 239 343 288    
use_wang    
no_ha     
delta_phi_ligand 40.0    
ligand_file side.pdb     
n_indiv_tries_min 1
n_indiv_tries_max 1
n_group_search 4    
group_search 0      
fit_phi_inc 20.0    
n_keep_plac 100     
ligand_resno 1      
n_template_atom 4   
fit_phi_range -180.0 180.0    
search_center 0.0 0.0 0.0   
database 5     
no_optimize_ncs     
spg_name_use P 2 2 2     
use_distance
EOD


