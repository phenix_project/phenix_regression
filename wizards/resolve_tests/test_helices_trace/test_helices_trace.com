#!/bin/csh -ef
#$ -cwd
phenix.resolve<<EOD
hklin TEMP_a2u-sigmaa.mtz   
labin FP=FWT PHIB=PHIC   
no_build    
ha_file NONE   
resolution 1000 2.4
trace_helices     
max_segment 1     
i_start_helix 1     
i_end_helix 1     
rho_min_main_low -1.     
rho_min_side_seg -100    
mask_cycles 1     
minor_cycles 0      
solvent_content 0.5    
cut_1 0.2      
cut_2 0.0      
use_wang    
no_ha     
database 5     
no_optimize_ncs     
EOD

