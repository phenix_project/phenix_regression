from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_align_bases")
  print("")
  log_file=os.path.join(os.getcwd(),"test_align_bases.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  cmds='''!hklin resolve_work.mtz
!labin FP=FP PHIB=PHIM FOM=FOMM
hklin all.mtz
labin FP=FWT PHIB=PHIFWT
extend_only
resolution 1000 3
ha_file NONE
!seq_file seq_from_pdb.dat
skip_hetatm
pdb_in all.pdb
group_ca_length 3
!cut_up_model
use_wang
no_ha
database 5
use_any_side
no_merge_ncs_copies
no_optimize_ncs
nohl
spg_name_use P 1
start_chain 1 2
start_chain 2   22
build_rna
compare_all
group_length 5
rho_min_main_low -0.0
start_segment 5
dist_ca_approach 4.0
max_segment 5
r_match 2.5
use_any_side
rho_min_side_seg -1.0
group_ca_length 3
rho_min_side -1.0
rho_min_main_base 1.0
dist_cut_assemble 1.0
rho_min_main_seg -1.0
rms_random_frag 0.5
d_cut_fragment 0.01 0.01
!trim
n_random_frag 0
dump_frags 2
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.resolve', cmds,'phenix.resolve.log',None,None,None)
  print(open('phenix.resolve.log').read(), file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_align_bases',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['resolve_tests','test_align_bases']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
