from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_find_strands")
  print("")
  log_file=os.path.join(os.getcwd(),"test_find_strands.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  cmds='''hklin TEMP_gene-5-perfect.mtz
labin FP=FP PHIB=PHIC
no_build
ha_file NONE
resolution 1000 3.0
find_sheets
rho_min_main_low -100
rho_min_side_seg -100
mask_cycles 1
minor_cycles 0
solvent_content 0.5
use_wang
no_ha
database 5
no_optimize_ncs
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.resolve', cmds,'phenix.resolve.log',None,None,None)
  print(open('phenix.resolve.log').read(), file=log)

  for line in open('sheets.pdb').readlines()[:50]:
    print(line, file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_find_strands',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['resolve_tests','test_find_strands']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
