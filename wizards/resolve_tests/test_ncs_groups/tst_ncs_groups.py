from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_ncs_groups")
  print("")
  log_file=os.path.join(os.getcwd(),"test_ncs_groups.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  cmds='''hklin a2u-sigmaa.mtz
labin FP=FWT PHIB=PHIC
resolution 200 8
no_refine_ncs
mask_cycles 1
minor_cycles 1
solvent_content 0.5
database 5
no_build
new_ncs_group
rota_matrix      1.0000     0.0000     0.0000
rota_matrix      0.0000     1.0000     0.0000
rota_matrix      0.0000     0.0000     1.0000
tran_orth       0.0000     0.0000     0.0000
center_orth     27.9638    -9.7513   -13.0742
rota_matrix     -0.9552    -0.2479    -0.1615
rota_matrix     -0.2571     0.4255     0.8677
rota_matrix     -0.1464     0.8703    -0.4702
tran_orth      43.2942     4.6358     2.0025
center_orth     10.9978   -17.9223    -4.5330
new_ncs_group
rota_matrix      1.0000     0.0000     0.0000
rota_matrix      0.0000     1.0000     0.0000
rota_matrix      0.0000     0.0000     1.0000
tran_orth       0.0000     0.0000     0.0000
center_orth     27.9638    -9.7513   -13.0742
rota_matrix     -0.9943     0.0655    -0.0845
rota_matrix      0.0466    -0.4463    -0.8937
rota_matrix     -0.0963    -0.8925     0.4407
tran_orth      38.0383   -24.3301   -12.6676
center_orth     10.7350    -6.8036   -12.3560
new_ncs_group
rota_matrix      1.0000     0.0000     0.0000
rota_matrix      0.0000     1.0000     0.0000
rota_matrix      0.0000     0.0000     1.0000
tran_orth       0.0000     0.0000     0.0000
center_orth     27.9638    -9.7513   -13.0742
rota_matrix      0.9475     0.2220     0.2302
rota_matrix      0.2254    -0.9742     0.0118
rota_matrix      0.2269     0.0407    -0.9731
tran_orth       5.3305   -27.2396   -18.1719
center_orth     26.5426   -11.8055     0.4550
n_xyz   48  36  48
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.resolve', cmds,'phenix.resolve.log',None,None,None)
  print(open('phenix.resolve.log').read(), file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_ncs_groups',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['resolve_tests','test_ncs_groups']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
