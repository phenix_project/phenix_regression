from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_cc")
  print("")
  log_file=os.path.join(os.getcwd(),"test_cc.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  cmds='''freer_if_present
hklin TEMP1_bad.mtz
labin FP=FP PHIB=PHIM FOM=FOMM
no_build
ha_file NONE
hkl_offset_file offset.mtz
keep_f_mag
noscale
fill_ratio 1.0
fill
res_fill 0.0
use_wang
no_ha
hklperfect TEMP2_prime_and_switch.mtz
labperfect FP=FP PHIB=PHIM FOM=FOMM
database 5
no_optimize_ncs
no_create_free
use_all_for_test
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.resolve', cmds,'phenix.resolve.log',None,None,None)
  print(open('phenix.resolve.log').read(), file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_cc',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['resolve_tests','test_cc']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
