#!/bin/csh -ef
phenix.resolve<<EOD
freer_if_present
hklin TEMP1_bad.mtz
labin FP=FP PHIB=PHIM FOM=FOMM 
no_build
ha_file NONE
hkl_offset_file offset.mtz
keep_f_mag
noscale
fill_ratio 1.0 
fill 
res_fill 0.0
use_wang
no_ha
hklperfect TEMP2_prime_and_switch.mtz
labperfect FP=FP PHIB=PHIM FOM=FOMM 

database 5
no_optimize_ncs
no_create_free 
use_all_for_test
EOD
