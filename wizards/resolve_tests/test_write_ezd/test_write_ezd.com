#!/bin/csh -ef
#$ -cwd
phenix.resolve<<EOD
hklin solve_1.mtz
labin FP=FP SIGFP=SIGFP PHIB=PHIB FOM=FOM 
map_ezd_file map.ezd
no_build
solvent_content 0.5
database 5
mask_cycles 1
minor_cycles 1
EOD

