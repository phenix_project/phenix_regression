from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_ligand_group")
  print("")
  log_file=os.path.join(os.getcwd(),"test_ligand_group.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  cmds='''hklin ATP.pdb.mtz
labin FP=FMODEL PHIB=PHIFMODEL
no_build
ha_file NONE
n_indiv_tries_min 100
n_indiv_tries_max 100
database 5
ligand_start start.pdb
ligand_file ATP.pdb
test_extra
n_xyz   36  48  36
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.resolve', cmds,'phenix.resolve.log',None,None,None)
  print(open('phenix.resolve.log').read(), file=log)

  cmds='''hklin ATP.pdb.mtz
labin FP=FMODEL PHIB=PHIFMODEL
no_build
ha_file NONE
n_indiv_tries_min 100
n_indiv_tries_max 100
database 5
ligand_start start.pdb
ligand_file ATP.pdb
n_xyz   36  48  36
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.resolve', cmds,'phenix.resolve.log',None,None,None)
  print(open('phenix.resolve.log').read(), file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_ligand_group',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['resolve_tests','test_ligand_group']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
