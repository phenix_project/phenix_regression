#!/bin/csh  -ef
#$ -cwd
phenix.resolve<<EOD
hklin random.mtz    
labin FP=FP PHIB=PHIM FOM=FOMM   
hklout resolve.mtz     
solvent_content 0.85     
no_build    
mask_cycles 1     
minor_cycles 1      
RESOLUTION 1000.0 2.5    
ha_file ha_2.pdb    
rad_mask 5.0   
model ha_2.pdb      
prior_weight 0.0    
res_start 2.5     
score_only     
no_free   
modify    
use_wang    
no_ha     
hklperfect random.mtz    
labperfect FP=FP PHIB=PHIM FOM=FOMM   
database 5     
no_optimize_ncs     
nohl      
spg_name_use P 1 21 1    
group_ca_length 4   
group_length 2      
n_random_frag 0     
n_refine_rot_trans 4
n_xyz   36  36  36
EOD








