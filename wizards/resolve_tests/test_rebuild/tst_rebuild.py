from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_rebuild")
  print("")
  log_file=os.path.join(os.getcwd(),"test_rebuild.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  cmds='''hklin resolve_work.mtz
labin FP=FP PHIB=PHIM FOM=FOMM
extend_only
resolution 1000 3.0
ha_file NONE
seq_file seq.dat
replace_existing True
n_try_rebuild 2
rho_min_main_low 0.75
n_random_loop 3
cut_2 0.2
rho_min_main_base 0.75
use_any_side Yes
i_ran_seed 447628
rms_random_loop 0.2
seq_prob_min 0.95
richardson_rotamers True
dist_close 0.8
cut_1 0.2
rebuild_in_place Yes
rho_min_side 0.2
skip_hetatm True
no_merge_ncs_copies True
pdb_in starting_model.pdb
group_ca_length 4
use_wang
no_ha
n_xyz 36 36 36
database 5
use_any_side
no_optimize_ncs
nohl
spg_name_use P 1 21 1
start_chain 1   92
group_length 2
n_random_frag 0
n_refine_rot_trans 4
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.resolve', cmds,'phenix.resolve.log',None,None,None)
  print(open('phenix.resolve.log').read(), file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_rebuild',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['resolve_tests','test_rebuild']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
