#!/bin/csh  -ef
#$ -cwd
phenix.resolve --trace<<EOD |grep peak
hklin 1BPI.mtz
labin FP=FP PHIB=PHIM
trace_chain
fill
res_fill 3.0
resolution 200 3.0
solvent_content 0.4
fill_ratio 10.
analyze_trace
pdb_in start.pdb
n_xyz   84  36  36
EOD








