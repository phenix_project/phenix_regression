from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_ligand_rot_trans")
  print("")
  log_file=os.path.join(os.getcwd(),"test_ligand_rot_trans.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  cmds='''hklin resolve_map.mtz
labin FP=FP PHIB=PHIM FOM=FOMM
no_build
ha_file NONE
start_rot 239 343 288
use_wang
no_ha
delta_phi_ligand 400.0
ligand_file several.pdb
n_indiv_tries_min 1
n_indiv_tries_max 11
n_group_search 4
group_search 0
fit_phi_inc 200.0
n_keep_plac 1
ligand_resno 1
n_template_atom 5
fit_phi_range -10.0 10.0
search_center 0.0 0.0 0.0
database 5
no_optimize_ncs
spg_name_use P 2 2 2
ligand_require one.pdb
density_offset 100.
n_xyz   36  36  36
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.resolve', cmds,'phenix.resolve.log',None,None,None)
  print(open('phenix.resolve.log').read(), file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_ligand_rot_trans',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['resolve_tests','test_ligand_rot_trans']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
