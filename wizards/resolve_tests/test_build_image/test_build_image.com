#!/bin/csh  -ef
#$ -cwd
phenix.resolve <<EOD
hklin perfect.mtz
labin FP=FP PHIB=PHIC
resolution 200 3.
build_only
superquick_build
seq_file seq.dat
build_image
n_refine_rot_trans 4
EOD
head -20 dump.map

phenix.resolve <<EOD
hklin perfect.mtz
labin FP=FP PHIB=PHIC
resolution 200 3.
build_only
superquick_build
seq_file seq.dat
build_image
tubes
n_refine_rot_trans 4
EOD
head -20 dump.map







