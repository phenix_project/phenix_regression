#!/bin/csh -ef
#$ -cwd
phenix.resolve<<EOD|grep '[a-z]'|grep -v 'done with'
hklin exptl_phases_and_amplitudes_for_density_modification.mtz    
labin FP=FP SIGFP=SIGFP PHIB=PHIM FOM=FOMM    
solvent_content 0.46     
mask_cycles 0     
resolution 3.5 500.0     
ha_file NONE   
seq_file sequence.dat    
model refine.pdb_6
pdb_in refine.pdb_6    
extend_only
n_xyz 96 36 60      
database 5     
spg_name_use C 1 2 1     
main_map 1
EOD

