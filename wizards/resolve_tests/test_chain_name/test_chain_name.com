#!/bin/csh  -ef
#$ -cwd
phenix.resolve<<EOD
verbose
pdb_in chain_name.pdb
hklin cycle_2.mtz
labin FP=FP PHIB=PHIM
solvent_content 0.4
mask_cycles 1
minor_cycles 1
extend_only
skip_hetatm
keep_main
skip_cc
rebuild_in_place
n_random_loop 0
n_try_rebuild 1
group_ca_length 4
no_merge_ncs_copies
replace_existing
EOD
