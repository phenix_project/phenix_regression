#!/bin/csh -ef
#$ -cwd
phenix.resolve --trace<<EOD
hklin mlt_gap.pdb.mtz
labin FP=FMODEL PHIB=PHIFMODEL
trace_chain
fill
res_fill 3.0
resolution 200 3.0
solvent_content 0.8
 n_xyz   64  64 64 
EOD

