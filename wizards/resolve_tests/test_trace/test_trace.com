#!/bin/csh  -ef
#$ -cwd
phenix.resolve --trace<<EOD
hklin 1BPI.mtz
labin FP=FP PHIB=PHIM
trace_chain
fill
res_fill 3.0
resolution 200 3.0
solvent_content 0.4
fill_ratio 10.
!rad_mask_trace 1.1
!rad_mask_trace 2.0
!w_density 6.
!n_sift_nona 1
!rat_pair_min 0.7
!a_cut_min 1.0
!target_ratio 3.0
!n_overlap 4
n_refine_rot_trans 4
n_xyz   84  36  36
EOD








