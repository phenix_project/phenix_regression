#!/bin/csh  -ef
#$ -cwd
echo "NOTE: local_hist is not finished...do not really use yet"
phenix.resolve <<EOD 
hklin coords.pdb.mtz
labin FP=FMODEL PHIB=PHIFMODEL
resolution 200 4.
mask_cycles 1
minor_cycles 1
no_build
solvent_content 0.5
database 5
n_refine_rot_trans 4
model coords.pdb
wang_radius_start 4.
local_hist
test_extra
 n_xyz   24  24  24
EOD

