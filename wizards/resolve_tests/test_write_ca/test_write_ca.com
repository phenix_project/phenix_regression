#!/bin/csh  -ef
#$ -cwd
phenix.resolve <<EOD  |grep -v ' A  '
hklin a2u-sigmaa.mtz
resolution 200 6
labin FP=FWT PHIB=PHIC
pdb_in start.pdb
extend_only
seq_file a2u.seq
n_xyz   60  36  60
EOD

