from __future__ import division
from __future__ import print_function

# script to check results of wizard test

import os,sys
from libtbx.utils import Sorry,null_out
from libtbx.test_utils import approx_equal

def trim_lines(longer_lines,shorter_lines,swap=None):
  original_longer_lines=longer_lines
  new_longer_lines=[]
  for s in shorter_lines:
    ss=s.lstrip().split()[0]
    for l in longer_lines:
      longer_lines=longer_lines[1:]
      ll=l.lstrip().split()[0]
      if ll==ss:
        new_longer_lines.append(l)
        break
  if len(new_longer_lines)==len(shorter_lines):
    return new_longer_lines
  else:
    return original_longer_lines


def try_to_match_lines(lines_a,lines_b):
  if len(lines_a)==len(lines_b):
    pass # keep it

  # try to trim off lines from longer one (does not catch case where both change)
  elif len(lines_a)>len(lines_b):
    lines_a=trim_lines(longer_lines=lines_a,shorter_lines=lines_b)
  else:
    lines_b=trim_lines(longer_lines=lines_b,shorter_lines=lines_a)

  return " ".join(lines_a)," ".join(lines_b)


def find_diff_numbers(lines_a,lines_b,eps=None):

  a,b=try_to_match_lines(lines_a,lines_b)

  spl_a=a.replace(":",": ").split()
  spl_b=b.replace(":",": ").split()
  for aa,bb in zip(spl_a,spl_b):
    if aa.lower()=='nan': aa=-999.
    if bb.lower()=='nan': bb=-999.
    try:
      aa1=float(aa)
      bb1=float(bb)
    except Exception as e:
      continue # just trying (wasn't a number)
    # special case for 180 -180
    if int(0.5+abs(aa1))==180 and int(0.5+abs(bb1))==180:
        aa1='180'
        bb1='180'
    eps_use = max(eps, eps * max(abs(aa1),abs(bb1)))
    if type(aa1) == type(1):
      eps_use = max(eps, 1)
    if not approx_equal(aa1,bb1,eps=eps_use,out=null_out()):
      line="FAILED...numbers %s and %s differ" %(aa,bb)
      raise Sorry(line)

def check_for_bad_things(lines):
  ignore_things=['source_line','column type different','Error est','chain_type was not defined']
  bad_things=[
   'UNIT TEST FAILED',
   'not defined',
   'Input failed',
   'Sorry:',
   'raise Sorry',
   'Error',
   'Failed to carry out',
   'Sorry, no model built',
   'seems not to exist?'
   'Sorry, cannot interpret the file']
  for a in lines:
    ignore_this_line=False
    for ignore_thing in ignore_things:
      if a.find(ignore_thing)>-1:
        ignore_this_line=True
    if not ignore_this_line:
      for bad_thing in bad_things:
        if a.find(bad_thing)>-1:
          line="FAILED...found %s in %s" %(bad_thing,a)
          raise Sorry(line)

def is_similar(line1,line2):
  # checking for ignore_lines here just could differ in blanks
  if line1.strip()==line2:
    return True
  else:
    return False

always_ignore_phrases=['pid','run','jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec','status','save_time','instance','malloc','ccp4','cpu','time','seconds','date','calls','check','secs','source_line']

always_keep_phrases=['remark']

def is_all_numbers(line):
  line=line.lower().replace("e","").replace("+","").replace("-","").replace(".","").replace(" ","")
  for x in line:
    if not x in "0123456789":
      return False
  return True

def remove_ignore_lines(expected_output,ignored_output=None):
    new_lines=[]
    if not ignored_output: ignored_output=[]
    if not expected_output: expected_output=[]
    for line in expected_output:
      to_ignore=False
      if not to_ignore:
        if is_all_numbers(line):
          to_ignore=True
      if not to_ignore:
        for ignore_line in ignored_output:
          if is_similar(ignore_line,line):
             to_ignore=True
             break
      if not to_ignore:
        always_keep=False
        for a in always_keep_phrases:
          if line.lower().find(a)>-1:
            always_keep=True
            break

        if not always_keep:
          for a in always_ignore_phrases:
            if line.lower().find(a)>-1:
              to_ignore=True
              break
      if not to_ignore:
        new_lines.append(line)

    return new_lines

def any_overlap(lines1,lines2):
  #return True if any of lines1 matches lines2
  for line in lines1:
    if line in lines2: return True
  return False

def has_numbers(line):
  for x in line.replace(":"," ").split():
    try:
      a=float(x)
      return True
    except Exception as e:
      pass # was fine
  return False

def check_results(test="",lines=None,
    expected_output=None, expected_output_file=None,
    list_useful_lines=None,
    ignored_output=None,ignored_output_file=None,eps=1.e-3,
    max_lines = 1000,
    update_ignore_file=None,write_expected_file=None,out=sys.stdout):
  # Note: works when run in the directory of the test
  lines=run_strip(lines)

  # lines: working lines
  # expected_output: standard lines
  # ignored_output: standard lines to ignore

  # Check for bad things anywhere
  check_for_bad_things(lines)

  if test and (not write_expected_file) and (write_expected_file is not None):  # write to standard file:
    write_expected_file=os.path.join(
      os.getcwd(),"%s.draft_expected_output" %test)
  if write_expected_file: # write output to expected
    f=open(write_expected_file,'w')
    for x in lines[-max_lines:]:
      print(x, file=f)
    f.close()
    print("Wrote expected output to %s" %(write_expected_file), file=out)

  if not expected_output:
    if not expected_output_file:
      expected_output_file=os.path.join(
       os.getcwd(),"%s.expected_output" %test)
    # get expected lines
    if os.path.isfile(expected_output_file):
      expected_output=open(expected_output_file).readlines()
      expected_output=run_strip(expected_output)
    else:
      expected_output=None

  if not ignored_output:
    if not ignored_output_file:
      ignored_output_file="%s.ignored_output" %test
    # get ignored lines
    if os.path.isfile(ignored_output_file):
      ignored_output=open(ignored_output_file).readlines()
      ignored_output=run_strip(ignored_output)
    else:
      ignored_output=None


  # remove ignore_lines
  expected_output=remove_ignore_lines(expected_output)
  lines=remove_ignore_lines(lines)

  if list_useful_lines:
    print("\nUSEFUL LINES:\n", file=out)

  count=0
  for line in expected_output:
   if (not ignored_output) or \
       ( ignored_output and not any_overlap([line],ignored_output)):
     if has_numbers(line):
       if list_useful_lines:
         print(line.strip(), file=out)
       count+=1
  print("\nTOTAL USEFUL LINES for test %s: %d" %(test,count))

  import difflib
  if expected_output:
    try:
      deltas=list(difflib.ndiff(expected_output,lines))
    except Exception as e:
      expected_output = []  # didn't work
      deltas=list(difflib.ndiff(expected_output,lines))

    deltas=list(difflib.ndiff(expected_output,lines))
    failed_minus_lines=[]
    failed_minus_and_plus_lines=[]
    error_messages=[]
    minus_lines=[]
    plus_lines=[]
    for i in range(len(deltas)):
      line=deltas[i]
      if not line: continue
      x=line[0:2]
      if x == '- ': #minus line
        minus_lines.append(line[2:])
      elif x == '+ ': #plus line
        plus_lines.append(line[2:])
      elif x=='  ' or i+1==len(deltas):
        if not minus_lines: # nothing missing...skip it
          pass
        else: # something changed from expected...
          # compare minus and plus lines for numbers
          try:
            find_diff_numbers(minus_lines,plus_lines,eps=eps)
          except Exception as e:  # found a difference
            # ignore the difference if minus_lines contains any ignore_lines
            if (not ignored_output) or \
              ( ignored_output and not any_overlap(minus_lines,ignored_output)):
              failed_minus_and_plus_lines+=[
                80*"=","EXPECTED (%s):" %(str(expected_output_file)) ,
                80*"=",""]+minus_lines+\
                ["\n",80*"=","FOUND (e.g., %s.output):" %(
                   os.path.join(os.getcwd(),str(test))),80*"=",
                ""]+plus_lines+["",80*"="]

              failed_minus_lines+=minus_lines
              error_messages.append(str(e))
            else:
              pass # ignore the differences
          plus_lines=[]
          minus_lines=[]

    if error_messages:
      print("\nERROR: Test %s FAILED" %(str(test)), file=out)
      print("Lines not matched:\n","\n".join(failed_minus_and_plus_lines),"\n", file=out)
      if not ignored_output: ignored_output=[]
      if test and not update_ignore_file:
         update_ignore_file=os.path.join(
          os.getcwd(),"%s.draft_ignored_output" %test)

      if update_ignore_file:
        lines_added=0
        for line in failed_minus_lines:
          if not line in ignored_output:
            ignored_output.append(line)
            lines_added+=1
        f=open(update_ignore_file,'w')
        for x in ignored_output:
          print(x, file=f)
        f.close()
        test_group=os.getcwd().split(os.path.sep)[-2]
        print("Added %d new ignore lines to %s\n\n" %(lines_added,update_ignore_file) + \
         "To ignore all errors found ALL of these wizard tests in the \n"+\
         "future run (in %s ):\n\n" %(
           os.path.sep.join(os.getcwd().split(os.path.sep)[:-2]))+\
         "  phenix_regression.wizards.update_wizard_tests all "+\
         "copy_ignored_output allow_missing\n\n"+\
         "to copy all ignore files to the regression "+\
          "directory, then commit these new or updated ignore files.\n\n"+\
         "To save ALL current output as expected, use (in %s):\n\n" %(
           os.path.sep.join(os.getcwd().split(os.path.sep)[:-2]))+\
         "  phenix_regression.wizards.update_wizard_tests all "+\
         "copy_expected_output allow_missing\n\n"+\
         "To ignore all errors found JUST THIS ONE wizard test in the \n"+\
         "future run (in %s ):\n\n" %(
           os.path.sep.join(os.getcwd().split(os.path.sep)[:-2]))+\
         "  phenix_regression.wizards.update_wizard_tests %s %s " % (
          test_group,test) +\
         "copy_ignored_output allow_missing\n\n"+\
         "to copy all ignore files to the regression "+\
          "directory, then commit these new or updated ignore files.\n\n"+\
         "To save current output from JUST THIS ONE test as expected, use "+\
            "(in %s):\n\n" %(
           os.path.sep.join(os.getcwd().split(os.path.sep)[:-2]))+\
         "  phenix_regression.wizards.update_wizard_tests %s %s " %(
           test_group,test)+\
         "copy_expected_output allow_missing\n\n"+\
         "To rerun just this test, use:\n\n"+\
         "  phenix_regression.wizards.run_wizard_test  %s %s \n\n" %(
          test_group,test), file=out)
      raise Sorry("\n\n".join(error_messages)+"\nTest %s FAILED" %(test))
    else:
      print("Test %s OK" %(test), file=out)
  else:
    print("No expected output...OK", file=out)

def run_strip(lines):
  new_lines=[]
  for line in lines:
    new_lines.append(line.rstrip())
  return new_lines

def run(args):
  if not args:
    print("Use: phenix.python check_results.py output.dat expected.dat ignore.dat 1.e-3 <update_ignore_file=ignore.dat> <write_expected_file=expected.dat> <test=test_xxx> <list_useful_lines>")

  else:
    update_ignore_file=False
    write_expected_file=False
    list_useful_lines=False
    test=None
    new_args=[]
    for arg in args:
      if arg.find("=")>-1 and arg.split("=")[0]=='update_ignore_file':
        update_ignore_file=arg.split("=")[1]
      elif arg.find("=")>-1 and arg.split("=")[0]=='write_expected_file':
        write_expected_file=arg.split("=")[1]
      elif arg.find("=")>-1 and arg.split("=")[0]=='test':
        test=arg.split("=")[1]
      elif arg=='list_useful_lines':
        list_useful_lines=True
      else:
        new_args.append(arg)
    args=new_args
    if test:
      lines=open("%s.output" %(test))
    else:
      lines=open(args[0]).readlines()
    lines=run_strip(lines)
    if len(args)>1:
      expected_output_file=args[1]
    else:
      expected_output_file=None
    if len(args)>2:
      ignored_output_file=args[2]
    else:
      ignored_output_file=None
    if len(args)>3:
      eps=float(args[3])
    else:
      eps=0.3

    check_results(test=test,
      lines=lines,expected_output_file=expected_output_file,
      ignored_output_file=ignored_output_file,eps=eps,
        update_ignore_file=update_ignore_file,
        list_useful_lines=list_useful_lines,
        write_expected_file=write_expected_file)

def tst_01():

  text="""
  test 1 here expected output
  test 1 here expected output line 2
  test 1 here expected output line 3
  """

  expected_output_text="""
  test 1 here expected output
  test 1 here expected output line 2
  test 1 here expected output line 3
  """

  ignored_output_text="""
  """

  from copy import deepcopy
  original_text=deepcopy(text)

  # original test with identical lines...
  eps=1.e-3
  check_results(test="tst_01",lines=text.splitlines(),
      expected_output=expected_output_text.splitlines(),
      ignored_output=ignored_output_text.splitlines(),eps=eps,out=null_out())
  print("tst_01 original... OK\n")

  # check finding a bad line
  text=original_text
  text+="\nFailed to carry out"  # a bad line
  try:
    check_results(test="tst_01a",lines=text.splitlines(),
      expected_output=expected_output_text.splitlines(),
      ignored_output=ignored_output_text.splitlines(),eps=eps,out=null_out())
    line="tst01a failed to recognize bad line: %s" %(text)
    raise Sorry(line)
  except Exception as e:
    pass # expected result
  print("tst_01a found fail line...OK\n")

  # Check different numbers
  text=original_text
  text=text.replace("output line 2","output line 7")
  try:
    check_results(test="tst_01b",lines=text.splitlines(),
      expected_output=expected_output_text.splitlines(),
      ignored_output=ignored_output_text.splitlines(),eps=eps,out=null_out())
    line="tst01b failed to recognize difference between 2 and 7:"
    raise Sorry(line)
  except Exception as e:
    pass # expected result
  print("tst_01b found difference between 2 and 7...OK\n")

  # Check slightly different numbers
  text=original_text
  text=text.replace("output line 2","output line 2.0001")
  try:
    check_results(test="tst_01c",lines=text.splitlines(),
      expected_output=expected_output_text.splitlines(),
      ignored_output=ignored_output_text.splitlines(),eps=eps,out=null_out())
  except Exception as e:
    line="tst01c failed: found difference between 2 and 2.0001 eps=1.e-3"
    raise Sorry(line)
  print("tst_01c found no difference between 2 and 2.0001...OK")

  # Check slightly different numbers but bigger than eps
  text=original_text
  text=text.replace("output line 2","output line 2.0001")
  eps=1.e-5
  try:
    check_results(test="tst_01d",lines=text.splitlines(),
      expected_output=expected_output_text.splitlines(),
      ignored_output=ignored_output_text.splitlines(),eps=eps,out=null_out())
    line="tst01d failed...found no difference between 2 and 2.0001 eps=1.e-5:"
    raise Sorry(line)
  except Exception as e:
    line="tst01d OK found difference between 2 and 2.0001 eps=1.e-5:"
  print("tst_01d found difference between 2 and 2.0001 eps 1.e-5...OK")

  # Check ignored difference is properly ignored with slightly different numbers
  text=original_text
  text=text.replace("output line 2","output line 2.01")
  ignored_text="  test 1 here expected output line 2"
  try:
    check_results(test="tst_01e",lines=text.splitlines(),
      expected_output=expected_output_text.splitlines(),
      ignored_output=ignored_text.splitlines(),eps=eps,out=null_out())
  except Exception as e:
    line="tst01e failed: found difference between 2 and 2.0001 eps=1.e-3"
    raise Sorry(line)
  print("tst_01e found no difference between 2 and 2.01 with ignore line...OK")


if __name__=="__main__":
  args=sys.argv[1:]
  if not 'test' in args:
    run(args)
  else:
    tst_01()
