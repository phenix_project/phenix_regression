from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os,sys
  print ("Test script for test_unmerged_mir")
  print ("")
  log_file=os.path.join(os.getcwd(),"test_unmerged_mir.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  local_log_file='test_unmerged_mir.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autosol  import run_autosol
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''trace_chain=False chain_type=PROTEIN expt_type=sir native.data=unmerged.mtz deriv.data=unmerged.mtz space_group=p622 debug=True resolution=7 sites_file=ha_perfect.pdb build=False have_hand=True ncs_copies=1 residues=200 solvent_fraction=0.50 quick=true remove_aniso=False''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autosol(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)



  local_log_file='test_unmerged_mir.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autosol  import run_autosol
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''trace_chain=False chain_type=PROTEIN expt_type=sir native.data=unmerged.mtz deriv.data=unmerged.mtz space_group=p622 debug=True resolution=7 sites_file=ha_perfect.pdb build=False have_hand=True ncs_copies=1 residues=200 solvent_fraction=0.50 quick=true remove_aniso=True''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autosol(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)



  local_log_file='test_unmerged_mir.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autosol  import run_autosol
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''trace_chain=False chain_type=PROTEIN expt_type=sad data=unmerged.mtz space_group=p622 debug=True resolution=7 sites_file=ha_perfect.pdb build=False have_hand=True ncs_copies=1 residues=200 solvent_fraction=0.50 quick=true remove_aniso=False''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autosol(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)



  local_log_file='test_unmerged_mir.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autosol  import run_autosol
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''trace_chain=False chain_type=PROTEIN expt_type=sad data=unmerged.mtz space_group=p622 debug=True resolution=7 sites_file=ha_perfect.pdb build=False have_hand=True ncs_copies=1 residues=200 solvent_fraction=0.50 quick=true remove_aniso=True''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autosol(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)


  log.close()


  print ()
  print ('Done with test...running checks on output')
  print ()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_unmerged_mir',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print ()
  print ('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['command_line_tests','test_unmerged_mir']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
