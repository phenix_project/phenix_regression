#!/bin/csh -ef
#$ -cwd
phenix.find_helices_strands 1BPI.mtz trace_chain=True debug=true
if ( $status )then
  echo "FAILED"
  exit 1
endif
cat 1BPI.mtz_helices_strands.log
