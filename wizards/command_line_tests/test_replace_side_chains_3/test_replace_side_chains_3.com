#!/bin/csh -f
#$ -cwd


echo "Testing replace_side_chains with RNA..."
phenix.replace_side_chains use_any_side=False chain_type=RNA rna.seq rna.pdb cycle_2.mtz debug=true remove_clashes=True

wc replace_side_chains.pdb
