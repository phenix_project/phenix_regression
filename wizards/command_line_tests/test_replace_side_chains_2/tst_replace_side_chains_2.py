from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print ("Test script for test_replace_side_chains_2")
  print ("")
  log_file=os.path.join(os.getcwd(),"test_replace_side_chains_2.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  from phenix.command_line.replace_side_chains  import replace_side_chains
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''sequence_from_density=True sequence_autosol.dat arr_1.pdb cycle_2.mtz debug=true remove_clashes=False pdb_out=sequence_from_density.pdb''')
  replace_side_chains(args=args,out=log)

  for line in open('sequence_from_density.pdb').readlines()[:100]:
    print (line, file = log)

  log.close()


  print ()
  print ('Done with test...running checks on output')
  print ()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_replace_side_chains_2',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print ()
  print ('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['command_line_tests','test_replace_side_chains_2']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
