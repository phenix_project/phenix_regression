#!/bin/csh -ef
#$ -cwd
phenix.assign_sequence  test.eff
if ( $status )then
  echo "FAILED"
  exit 1
endif
echo "OK"
