#!/bin/csh -ef
#$ -cwd
set phenix_regression="`libtbx.find_in_repositories phenix_regression`"
foreach key ("" check show_ignore help)
echo "CHECKING $key"
phenix.list $key >output_$key.list
if ( $status )then
  echo "FAILED"
  exit 1
endif
end

phenix.python $phenix_regression/wizards/compare_quick.py output_check.list output_check.std 
if ( $status )then
  echo "test_list FAILED in `pwd`"
  exit 1
endif
