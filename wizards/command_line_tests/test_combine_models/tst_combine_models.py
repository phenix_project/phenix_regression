from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print ("Test script for test_combine_models")
  print ("")
  log_file=os.path.join(os.getcwd(),"test_combine_models.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  from iotbx.cli_parser import run_program
  from phenix.programs import combine_models as run
  from phenix.programs import get_cc_mtz_pdb as get_cc

  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''working.ccp4 merge_by_segment_correlation=True merge_remainder=False pdb_out=out.pdb working.pdb ncs_file=working.ncs dump_remainder=true verbose=true in_cell_only=False resolution=2.85''')
  run_program(program_class=run.Program, args = args, logger = log)


  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''target.mtz out.pdb''')
  run_program(program_class=get_cc.Program, args = args, logger = log)


  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''working_box.ccp4 merge_by_segment_correlation=True merge_remainder=False pdb_out=out.pdb working_box.pdb ncs_file=working_box.ncs_spec dump_remainder=true verbose=true in_cell_only=False resolution=2.85''')
  run_program(program_class=run.Program, args = args, logger = log)


  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''target.mtz out.pdb''')
  run_program(program_class=get_cc.Program, args = args, logger = log)


  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''working.ccp4 merge_by_segment_correlation=True merge_remainder=False pdb_out=out.pdb working_ncs.pdb ncs_file=working.ncs dump_remainder=true verbose=true in_cell_only=False resolution=2.85''')
  run_program(program_class=run.Program, args = args, logger = log)


  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''target.mtz out.pdb''')
  run_program(program_class=get_cc.Program, args = args, logger = log)


  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''working_box.ccp4 merge_by_segment_correlation=True merge_remainder=False pdb_out=out.pdb working_box_ncs.pdb ncs_file=working_box.ncs_spec dump_remainder=true verbose=true in_cell_only=False resolution=2.85''')
  run_program(program_class=run.Program, args = args, logger = log)


  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''working.ccp4 merge_by_segment_correlation=False merge_remainder=False pdb_out=out.pdb working_ncs.pdb ncs_file=working.ncs dump_remainder=true verbose=true in_cell_only=False resolution=2.85''')
  run_program(program_class=run.Program, args = args, logger = log)


  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''target.mtz out.pdb''')
  run_program(program_class=get_cc.Program, args = args, logger = log)


  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''working_box.ccp4 merge_by_segment_correlation=False merge_remainder=False pdb_out=out.pdb working_box_ncs.pdb ncs_file=working_box.ncs_spec dump_remainder=true verbose=true in_cell_only=False resolution=2.85''')
  run_program(program_class=run.Program, args = args, logger = log)


  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''working.mtz merge_by_segment_correlation=True working.pdb''')
  run_program(program_class=run.Program, args = args, logger = log)

  input_text=open('combine_models.pdb').read()
  chars=len(input_text)
  lines=len(input_text.splitlines())
  print (chars,lines, file = log)


  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''combine_models.eff''')
  run_program(program_class=run.Program, args = args, logger = log)

  input_text=open('combine_models.pdb').read()
  chars=len(input_text)
  lines=len(input_text.splitlines())
  print (chars,lines, file = log)


  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''combine_rna_protein.eff''')
  run_program(program_class=run.Program, args = args, logger = log)

  input_text=open('combine_models.pdb').read()
  chars=len(input_text)
  lines=len(input_text.splitlines())
  print (chars,lines, file = log)


  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''working.mtz merge_by_segment_correlation=True merge_remainder=False pdb_out=out.pdb working.pdb ncs_file=working.ncs dump_remainder=true verbose=true in_cell_only=False''')
  run_program(program_class=run.Program, args = args, logger = log)


  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''target.mtz out.pdb''')
  run_program(program_class=get_cc.Program, args = args, logger = log)


  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''working.mtz merge_by_segment_correlation=True merge_remainder=False pdb_out=out.pdb working.pdb ncs_file=working.ncs dump_remainder=true verbose=true in_cell_only=True''')
  run_program(program_class=run.Program, args = args, logger = log)


  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''target.mtz out.pdb''')
  run_program(program_class=get_cc.Program, args = args, logger = log)

  log.close()


  print ()
  print ('Done with test...running checks on output')
  print ()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_combine_models',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print ()
  print ('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['command_line_tests','test_combine_models']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
