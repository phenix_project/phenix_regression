#!/bin/csh -ef
#$ -cwd

echo "Testing for test_flag_value"

phenix.autobuild delete_runs=1-10 > dummy.log
phenix.autobuild ccp4.mtz seq.dat delete_bad_residues_only=true ncycle_refine=0 coords.pdb number_of_parallel_models=1 find_ncs=false remove_aniso=false skip_xtriage=true resolve_command_list="'mask_cycles 1' 'minor_cycles 1'" test_flag_value=3 > autobuild.log
if ( $status )then
  echo "FAILED"
  exit 1
endif


phenix.python<<EOD
from __future__ import division
from __future__ import print_function
for line in open(os.path.join('AutoBuild_run_1_','TEMP0','refined_pdb_in.pdb')).readlines():
  if line.startswith("REMARK   test_flag_value"):
    test_flag_value=int(line.rstrip().split()[-1])
    if test_flag_value != 3:
      print ("FAILED flag value:  got %d expected 3" %(test_flag_value))
      raise AssertionError, "FAILED flag value:  got %d expected 3" %(test_flag_value)
    else:
      print ("OK flag value:  got %d expected 3" %(test_flag_value))
    break
EOD

