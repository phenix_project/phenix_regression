#!/bin/csh -f
#$ -cwd

echo "Testing reassign_sequence..."

phenix.replace_side_chains reassign_sequence=True sequence_autosol.dat arr_1.pdb cycle_2.mtz debug=true remove_clashes=False pdb_out=reassign.pdb 
cat reassign.pdb

