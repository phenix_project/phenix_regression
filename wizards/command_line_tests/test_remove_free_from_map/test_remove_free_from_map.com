#!/bin/csh -ef
#$ -cwd
phenix.remove_free_from_map free_in=working_data_file.mtz map_coeffs=cycle_2.mtz labin_map_coeffs="FP=FP PHIB=PHIM FOM=FOMM" debug=true> test_remove_free_from_map_current.log
if ( $status )then
  echo "FAILED"
  exit 1
endif
phenix.mtz.dump map_coeffs_without_freer_set.mtz -c | tail -20 >>test_remove_free_from_map_current.log
if ( $status )then
  echo "mr_rescoring FAILED"
  exit 1
endif
