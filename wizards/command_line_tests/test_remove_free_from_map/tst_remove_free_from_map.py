from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os,sys
  print("Test script for test_remove_free_from_map")
  print("")
  log_file=os.path.join(os.getcwd(),"test_remove_free_from_map.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  local_log_file='test_remove_free_from_map.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.remove_free_from_map  import remove_free_from_map
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''free_in=working_data_file.mtz map_coeffs=cycle_2.mtz labin_map_coeffs="FP=FP PHIB=PHIM FOM=FOMM" debug=true''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  remove_free_from_map(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)


  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.mtz.dump map_coeffs_without_freer_set.mtz -c')
  for line in result.stdout_lines:
    print(line, file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_remove_free_from_map',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['command_line_tests','test_remove_free_from_map']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
