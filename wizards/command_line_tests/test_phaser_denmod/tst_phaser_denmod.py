from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os,sys
  print("Test script for test_phaser_denmod")
  print("")
  log_file=os.path.join(os.getcwd(),"test_phaser_denmod.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  local_log_file='test_phaser_denmod.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autosol  import run_autosol
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''seq_file="hewl.seq" unit_cell="78.239 78.239 37.281 90 90 90" space_group="P 43 21 2" data="lyso2001.sca" atom_type="S" lambda=1.5418 input_partpdb_file="MR.1.pdb" partpdb_rms=1 build=False sites_then_phase=False ha_iteration=False model_ha_iteration=False add_classic_denmod=False''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autosol(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)



  local_log_file='test_phaser_denmod.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''maps_only=True data=AutoSol_run_1_/phaser_2.mtz input_map_file=AutoSol_run_1_/phaser_2.mtz input_map_labels="FWT PHIFWT" seq_file=hewl.seq map_file_fom=0.45 ha_file=AutoSol_run_1_/ha_2.pdb_formatted.pdb''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)



  from phenix.command_line.get_cc_mtz_mtz  import get_cc_mtz_mtz
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''AutoSol_run_1_/overall_best_denmod_map_coeffs.mtz AutoBuild_run_1_/cycle_best_1.mtz''')
  get_cc_mtz_mtz(args=args,out=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_phaser_denmod',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['command_line_tests','test_phaser_denmod']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
