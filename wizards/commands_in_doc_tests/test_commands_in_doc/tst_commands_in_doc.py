from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_commands_in_doc")
  print("")
  log_file=os.path.join(os.getcwd(),"test_commands_in_doc.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.python extract_phenix.py `libtbx.find_in_repositories phenix_html`/rst_files/reference/cut_out_density.txt')
  for line in result.stdout_lines:
    print(line, file=log)

  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.python extract_phenix.py `libtbx.find_in_repositories phenix_html`/rst_files/reference/mr_rosetta.txt')
  for line in result.stdout_lines:
    print(line, file=log)

  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.python extract_phenix.py `libtbx.find_in_repositories phenix_html`/rst_files/reference/remove_free_from_map.txt')
  for line in result.stdout_lines:
    print(line, file=log)

  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.python extract_phenix.py `libtbx.find_in_repositories phenix_html`/rst_files/reference/replace_side_chains.txt')
  for line in result.stdout_lines:
    print(line, file=log)

  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.python extract_phenix.py `libtbx.find_in_repositories phenix_html`/rst_files/reference/build_one_model.txt')
  for line in result.stdout_lines:
    print(line, file=log)

  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.python extract_phenix.py `libtbx.find_in_repositories phenix_html`/rst_files/reference/phase_and_build.txt')
  for line in result.stdout_lines:
    print(line, file=log)

  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.python extract_phenix.py `libtbx.find_in_repositories phenix_html`/rst_files/reference/assign_sequence.txt')
  for line in result.stdout_lines:
    print(line, file=log)

  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.python extract_phenix.py `libtbx.find_in_repositories phenix_html`/rst_files/reference/apply_ncs.txt')
  for line in result.stdout_lines:
    print(line, file=log)

  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.python extract_phenix.py `libtbx.find_in_repositories phenix_html`/rst_files/reference/find_ncs_from_density.txt')
  for line in result.stdout_lines:
    print(line, file=log)

  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.python extract_phenix.py `libtbx.find_in_repositories phenix_html`/rst_files/reference/fit_loops.txt')
  for line in result.stdout_lines:
    print(line, file=log)

  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.python extract_phenix.py `libtbx.find_in_repositories phenix_html`/rst_files/reference/find_helices_strands.txt')
  for line in result.stdout_lines:
    print(line, file=log)

  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.python extract_phenix.py `libtbx.find_in_repositories phenix_html`/rst_files/reference/find_ncs.txt')
  for line in result.stdout_lines:
    print(line, file=log)

  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.python extract_phenix.py `libtbx.find_in_repositories phenix_html`/rst_files/reference/simple_ncs_from_pdb.txt')
  for line in result.stdout_lines:
    print(line, file=log)

  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.python extract_phenix.py `libtbx.find_in_repositories phenix_html`/rst_files/reference/map_to_object.txt')
  for line in result.stdout_lines:
    print(line, file=log)

  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.python extract_phenix.py `libtbx.find_in_repositories phenix_html`/rst_files/reference/get_cc_mtz_pdb.txt')
  for line in result.stdout_lines:
    print(line, file=log)

  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.python extract_phenix.py `libtbx.find_in_repositories phenix_html`/rst_files/reference/get_cc_mtz_mtz.txt')
  for line in result.stdout_lines:
    print(line, file=log)

  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.python extract_phenix.py `libtbx.find_in_repositories phenix_html`/rst_files/reference/multi_crystal_average.txt')
  for line in result.stdout_lines:
    print(line, file=log)

  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.python extract_phenix.py `libtbx.find_in_repositories phenix_html`/rst_files/reference/find_all_ligands.txt')
  for line in result.stdout_lines:
    print(line, file=log)

  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.python extract_phenix.py `libtbx.find_in_repositories phenix_html`/rst_files/reference/autosol.txt')
  for line in result.stdout_lines:
    print(line, file=log)

  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.python extract_phenix.py `libtbx.find_in_repositories phenix_html`/rst_files/reference/autobuild.txt')
  for line in result.stdout_lines:
    print(line, file=log)

  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.python extract_phenix.py `libtbx.find_in_repositories phenix_html`/rst_files/reference/automr.txt')
  for line in result.stdout_lines:
    print(line, file=log)

  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.python extract_phenix.py `libtbx.find_in_repositories phenix_html`/rst_files/reference/ligandfit.txt')
  for line in result.stdout_lines:
    print(line, file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  for line in lines:
    if line.lower().find ("failed") > -1:
      print("FAILED: %s" %(line))
      return
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['commands_in_doc_tests','test_commands_in_doc']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
