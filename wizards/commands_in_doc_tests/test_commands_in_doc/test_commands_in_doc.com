#!/bin/csh  -f
#$ -cwd
#
setenv LIBTBX_FULL_TESTING
set phenix_html="`libtbx.find_in_repositories phenix_html`"
if (! -d $phenix_html/rst_files/reference)then
  echo "Skipping test..."
  exit 0
endif
#
if ($#argv > 0) then
  if ($argv[1] == "--help") then
    echo "test all commands in wizard documentation"
    exit 0
  endif
endif

echo " "
echo "----------------------------------------------------- "
echo "test all commands in wizard documentation"
#
#
set logname = "test_doc.list"
#
phenix.python extract_phenix.py $phenix_html/rst_files/reference/cut_out_density.txt
phenix.python extract_phenix.py $phenix_html/rst_files/reference/mr_rosetta.txt
phenix.python extract_phenix.py $phenix_html/rst_files/reference/remove_free_from_map.txt
phenix.python extract_phenix.py $phenix_html/rst_files/reference/replace_side_chains.txt
phenix.python extract_phenix.py $phenix_html/rst_files/reference/build_one_model.txt
phenix.python extract_phenix.py $phenix_html/rst_files/reference/phase_and_build.txt
phenix.python extract_phenix.py $phenix_html/rst_files/reference/assign_sequence.txt
phenix.python extract_phenix.py $phenix_html/rst_files/reference/apply_ncs.txt
phenix.python extract_phenix.py $phenix_html/rst_files/reference/find_ncs_from_density.txt
phenix.python extract_phenix.py $phenix_html/rst_files/reference/fit_loops.txt
phenix.python extract_phenix.py $phenix_html/rst_files/reference/find_helices_strands.txt
phenix.python extract_phenix.py $phenix_html/rst_files/reference/find_ncs.txt
phenix.python extract_phenix.py $phenix_html/rst_files/reference/simple_ncs_from_pdb.txt
phenix.python extract_phenix.py $phenix_html/rst_files/reference/map_to_object.txt
phenix.python extract_phenix.py $phenix_html/rst_files/reference/get_cc_mtz_pdb.txt
phenix.python extract_phenix.py $phenix_html/rst_files/reference/get_cc_mtz_mtz.txt
phenix.python extract_phenix.py $phenix_html/rst_files/reference/multi_crystal_average.txt
phenix.python extract_phenix.py $phenix_html/rst_files/reference/find_all_ligands.txt
phenix.python extract_phenix.py $phenix_html/rst_files/reference/autosol.txt
phenix.python extract_phenix.py $phenix_html/rst_files/reference/autobuild.txt
phenix.python extract_phenix.py $phenix_html/rst_files/reference/automr.txt
phenix.python extract_phenix.py $phenix_html/rst_files/reference/ligandfit.txt
