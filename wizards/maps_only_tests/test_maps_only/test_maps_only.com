#!/bin/csh  -f
#$ -cwd
#
setenv LIBTBX_FULL_TESTING
#
if ($#argv == 0) then
else
  if ($argv[1] == "--help") then
    echo "test_maps_only. Use: test_maps_only.csh "
    exit 0
  endif
endif

echo " "
echo "----------------------------------------------------- "
echo " test_maps_only "
#
phenix.autobuild input_pdb_file=coords.pdb data=perfect.mtz   \
ps_in_rebuild=True maps_only=True input_labels="FP SIGFP PHIC FOM"
