from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os,sys
  print ("Test script for test_resno")
  print ("")
  log_file=os.path.join(os.getcwd(),"test_resno.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  local_log_file='test_resno.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''gene-5-za.pdb gene-5.mtz ncycle_refine=1 rebuild_res_start_list=-37 rebuild_res_end_list=-37 rebuild_chain_list=ZB negative_residues=True n_cycle_build=1 super_quick=True resolution=4.5 semet=False n_cycle_rebuild_max=1 number_of_parallel_models=1''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)

  print ( open('AutoBuild_run_1_/seq_from_pdb.dat').read(), file = log)
  print ( open('AutoBuild_run_1_/edited_pdb.pdb').read(), file = log)
  print ( open('AutoBuild_run_1_/TEMP0/LIG_LIGANDS_ELBOW.cif').read(), file = log)
  print ( open('AutoBuild_run_1_/overall_best.log_eval').read(), file = log)
  print ( open('AutoBuild_run_1_/overall_best.pdb').read(), file = log)

  log.close()


  print ()
  print ('Done with test...running checks on output')
  print ()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_resno',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print ()
  print ('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['resno_tests','test_resno']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
