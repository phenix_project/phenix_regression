#!/bin/csh  -f
#$ -cwd
#
setenv LIBTBX_FULL_TESTING
#
if ($#argv > 0) then
  if ($argv[1] == "--help") then
    echo "test rebuild-in-place with complicated residue numbers"
    exit 0
  endif
endif

echo " "
echo "----------------------------------------------------- "
echo "test_resno: test rebuild-in-place with complicated residue numbers"
#
phenix.autobuild super_quick=True gene-5-za.pdb gene-5.mtz \
ncycle_refine=1 rebuild_res_start_list=-37 rebuild_res_end_list=-37 \
rebuild_chain_list=ZB negative_residues=True \
n_cycle_build=1 resolution=4.5 \
semet=False \
n_cycle_rebuild_max=1  number_of_parallel_models=1 

cat AutoBuild_run_1_/seq_from_pdb.dat 
cat AutoBuild_run_1_/edited_pdb.cif
cat AutoBuild_run_1_/TEMP0/LIG_LIGANDS_ELBOW.cif
cat AutoBuild_run_1_/overall_best.log_eval 
cat AutoBuild_run_1_/overall_best_pdb_file.pdb

#
goto finished
bad:

    echo "*****************************************************"
    echo "Error:test_resno failed to run"
    echo "*****************************************************"
    exit 1
  goto finished
endif
finished:
echo "OK"
echo "------------------------------------------------------------"
echo " "
exit 0
