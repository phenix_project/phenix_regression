#!/bin/csh  -ef
#$ -cwd
phenix.resolve <<EOD
hklin 1BPI.pdb.mtz
labin FP=FMODEL PHIB=PHIFMODEL
resolution 200 4
pdb_in all.pdb
compare_pdb
seq_file NONE
compare_file 1BPI.pdb
EOD
phenix.resolve<<EOD
hklin 1BPI.pdb.mtz
labin FP=FMODEL PHIB=PHIFMODEL
resolution 200 4
pdb_in all.pdb
compare_file 1BPI.pdb
compare_pdb
EOD
phenix.resolve<<EOD
hklin 1BPI.pdb.mtz
labin FP=FMODEL PHIB=PHIFMODEL
resolution 200 4
pdb_in a.pdb
compare_file b.pdb
compare_pdb_flip
EOD
phenix.resolve<<EOD
hklin 1BPI.pdb.mtz
labin FP=FMODEL PHIB=PHIFMODEL
resolution 200 4
pdb_in a.pdb
compare_file a.pdb
compare_pdb_flip
EOD

