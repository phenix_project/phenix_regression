#!/bin/csh  -ef
#$ -cwd
phenix.resolve <<EOD
hklin perfect.mtz
labin FP=FP PHIB=PHIC
resolution 200 4.5
fom_target 0.5
hklout random.mtz
mask_cycles 0
minor_cycles 0
n_refine_rot_trans 4
EOD
phenix.resolve <<EOD
hklin random.mtz
labin FP=FP PHIB=PHIM
resolution 200 4.5
mask_cycles 1
minor_cycles 1
no_build
solvent_content 0.5
database 5
hklperfect perfect.mtz
labperfect FP=FP PHIB=PHIC
use_prob
n_refine_rot_trans 4
EOD








