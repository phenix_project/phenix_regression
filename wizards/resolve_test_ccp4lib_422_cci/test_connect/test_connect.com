#!/bin/csh  -ef
#$ -cwd
phenix.resolve<<EOD
hklin resolve_work.mtz   
labin FP=FP PHIB=PHIM FOM=FOMM   
extend_only    
RESOLUTION 1000.0 2.0    
ha_file NONE   
seq_file seq_from_pdb.dat   
pdb_in mask.pdb     
pdb_in_extra unplaced.pdb   
link_extra     
skip_hetatm    
no_trim   
use_wang    
no_ha     
n_xyz 120 48 72     
database 5     
no_optimize_ncs     
spg_name_use C 1 2 1     
start_chain 1 1     
group_ca_length 4   
group_length 2      
n_random_frag 0     
n_refine_rot_trans 4
EOD
cat edit_model_assigned.pdb








