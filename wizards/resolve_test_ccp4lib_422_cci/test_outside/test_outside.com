#!/bin/csh  -ef
#$ -cwd
phenix.resolve<<EOD
hklin resolve_work.mtz   
labin FP=FP PHIB=PHIM FOM=FOMM   
build_only     
RESOLUTION 1000.0 2.5    
ha_file NONE   
seq_file seq_from_pdb.dat   
model mask.pdb      
build_outside_model    
skip_hetatm    
superquick_build    
delta_phi 30.0      
use_wang    
no_ha     
n_xyz 36 36 36      
database 5     
no_optimize_ncs     
spg_name_use P 1 21 1    
start_chain 1   95     
group_ca_length 4   
group_length 2      
n_random_frag 0     
n_refine_rot_trans 4
EOD
cat resolve.pdb








