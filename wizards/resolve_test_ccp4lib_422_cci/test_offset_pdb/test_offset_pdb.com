#!/bin/csh -ef
#$ -cwd
phenix.resolve<<EOD
hklin TEMP_perfect.mtz   
labin FP=FP PHIB=PHIC    
no_build    
ha_file NONE   
hkl_offset_file offset.mtz    
pdb_in TEMP_coords.pdb   
use_wang    
no_ha     
hklperfect resolve.mtz   
labperfect FP=FP PHIB=PHIM FOM=FOMM   
database 5     
no_optimize_ncs     
EOD

