#!/bin/csh  -ef 
#$ -cwd
phenix.resolve<<EOD
hklin exptl_fobs_phases_freeR_flags.mtz    
labin FP=FP SIGFP=SIGFP FreeR_flag=FreeR_flag   
solvent_content 0.6    
no_build    
mask_cycles 0     
minor_cycles 0      
resolution 3.0 500.0     
ha_file NONE   
seq_file seq.dat    
pdb_in TMP.pdb      
find_omit_region    
omit_mask_file omit_region.mtz   
no_reuse_model      
mask_as_mtz    
offset_boundary 2.0    
use_wang    
no_ha     
n_xyz 36 36 36      
database 5     
no_optimize_ncs     
spg_name_use P 1 21 1    
start_chain 1   92     
group_ca_length 4   
group_length 2      
n_random_frag 0     
n_refine_rot_trans 4
EOD
phenix.resolve<<EOD
hklin omit_region.mtz
labin FP=FP PHIB=PHIM FOM=FOMM
hklperfect std_omit_region.mtz
labperfect FP=FP PHIB=PHIM FOM=FOMM
no_build
hkl_offset_file offset.mtz
keep_f_mag
noscale
database 5
EOD








