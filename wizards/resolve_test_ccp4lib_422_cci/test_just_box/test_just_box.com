#!/bin/csh -ef
#$ -cwd
phenix.resolve<<EOD
hklin solve_1.mtz
labin FP=FP SIGFP=SIGFP PHIB=PHIB FOM=FOM 
no_build
resolution 200 10
solvent_content 0.5
database 5
mask_cycles 1
minor_cycles 1
just_box
model coords.pdb
use_model_mask
EOD

