#!/bin/csh  -ef
#$ -cwd
phenix.resolve<<EOD
hklin resolve_work.mtz   
labin FP=FP PHIB=PHIM FOM=FOMM   
extend_only    
resolution 1000 3.0    
ha_file NONE   
seq_file seq.dat    
replace_existing True    
n_try_rebuild 2     
rho_min_main_low 0.75    
n_random_loop 3     
cut_2 0.2      
rho_min_main_base 0.75   
use_any_side Yes    
i_ran_seed 447628   
rms_random_loop 0.2    
seq_prob_min 0.95   
richardson_rotamers True    
dist_close 0.8      
cut_1 0.2      
rebuild_in_place Yes     
rho_min_side 0.2    
skip_hetatm True    
no_merge_ncs_copies True    
pdb_in starting_model.pdb   
group_ca_length 4   
use_wang    
no_ha     
n_xyz 36 36 36      
database 5     
use_any_side   
no_optimize_ncs     
nohl      
spg_name_use P 1 21 1    
start_chain 1   92     
group_length 2      
n_random_frag 0     
n_refine_rot_trans 4
EOD








