#!/bin/csh  -ef
#$ -cwd
phenix.resolve <<EOD 
hklin a2u-sigmaa.mtz
resolution 200 6
labin FP=FWT PHIB=PHIC
pdb_in start.pdb
extend_only
seq_file a2u.seq
EOD

