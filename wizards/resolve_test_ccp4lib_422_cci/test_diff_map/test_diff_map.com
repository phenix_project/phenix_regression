#!/bin/csh  -ef
#$ -cwd
phenix.resolve <<EOD
hklin protein_mask.mtz
labin FP=FP PHIB=PHIM FOM=FOMM
hklperfect ncs_mask.mtz
labperfect FP=FP PHIB=PHIM FOM=FOMM
difference_map
resolution 200 4.5
mask_cycles 1
minor_cycles 1
no_build
solvent_content 0.5
database 5
EOD
phenix.resolve<<EOD
hklin resolve.mtz
labin FP=FP PHIB=PHIM FOM=FOMM
hklperfect diff_1_std.mtz
labperfect FP=FP PHIB=PHIM FOM=FOMM
no_build
hkl_offset_file offset.mtz
keep_f_mag
noscale
database 5
EOD
phenix.resolve <<EOD
hklin protein_mask.mtz
labin FP=FP PHIB=PHIM FOM=FOMM
hklperfect ncs_mask.mtz
labperfect FP=FP PHIB=PHIM FOM=FOMM
difference_map
phase_with_perf
resolution 200 4.5
mask_cycles 1
minor_cycles 1
no_build
solvent_content 0.5
database 5
EOD
phenix.resolve<<EOD
hklin resolve.mtz
labin FP=FP PHIB=PHIM FOM=FOMM
hklperfect diff_1_std.mtz
labperfect FP=FP PHIB=PHIM FOM=FOMM
no_build
hkl_offset_file offset.mtz
keep_f_mag
noscale
database 5
EOD

