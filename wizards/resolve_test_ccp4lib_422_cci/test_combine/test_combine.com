#!/bin/csh  -ef
#$ -cwd
phenix.resolve<<EOD
hklin exptl_fobs_phases_freeR_flags.mtz    
labin FP=FP SIGFP=SIGFP FreeR_flag=FreeR_flag   
solvent_content 0.6    
no_build    
ha_file NONE   
combine_map overall_best_denmod_map_coeffs.mtz_OMIT_REGION_1 
combine_map overall_best_denmod_map_coeffs.mtz_OMIT_REGION_2 
combine_map overall_best_denmod_map_coeffs.mtz_OMIT_REGION_3 
combine_map overall_best_denmod_map_coeffs.mtz_OMIT_REGION_4 
combine_map overall_best_denmod_map_coeffs.mtz_OMIT_REGION_5 
combine_map overall_best_denmod_map_coeffs.mtz_OMIT_REGION_6 
combine_map overall_best_denmod_map_coeffs.mtz_OMIT_REGION_7 
combine_map overall_best_denmod_map_coeffs.mtz_OMIT_REGION_8 
combine_map overall_best_denmod_map_coeffs.mtz_OMIT_REGION_9 
combine_map overall_best_denmod_map_coeffs.mtz_OMIT_REGION_10   
combine_map overall_best_denmod_map_coeffs.mtz_OMIT_REGION_11   
combine_map overall_best_denmod_map_coeffs.mtz_OMIT_REGION_12   
omit      
use_hist_prob     
no_ha     
n_xyz 36 36 36      
database 5     
no_optimize_ncs     
spg_name_use P 1 21 1    
start_chain 1   92     
group_ca_length 4   
group_length 2      
n_random_frag 0     
n_refine_rot_trans 4
EOD
phenix.resolve<<EOD
hklin resolve_composite_map.mtz
labin FP=FP PHIB=PHIM FOM=FOMM
hklperfect resolve_composite_map_std.mtz
labperfect FP=FP PHIB=PHIM FOM=FOMM
no_build
hkl_offset_file offset.mtz
keep_f_mag
noscale
database 5
EOD






