#!/bin/csh -ef
#$ -cwd
phenix.resolve<<EOD
hklin exptl_fobs_phases_freeR_flags.mtz    
labin FP=FP SIGFP=SIGFP PHIB=PHIM
solvent_content 0.77     
no_build    
resolution 3.5 500.0     
pdb_in side.pdb
find_omit_region
background_map 
background_offset 0.5
scale_background 0.7
mask_cycles 1
minor_cycles 1
omit      
omit_mask_file omit_mask.mtz
mask_as_mtz
EOD
phenix.resolve<<EOD
hklin omit_mask.mtz
labin FP=FP PHIB=PHIM FOM=FOMM
hklperfect omit_mask_std.mtz
labperfect FP=FP PHIB=PHIM FOM=FOMM
no_build
hkl_offset_file offset.mtz
keep_f_mag
noscale
database 5
EOD



