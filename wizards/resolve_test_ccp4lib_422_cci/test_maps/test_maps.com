#!/bin/csh  -ef
#$ -cwd
phenix.resolve <<EOD
hklin a2u-sigmaa.mtz
labin FP=FWT PHIB=PHIC
resolution 200 4.5
mask_cycles 1
minor_cycles 1
no_build
solvent_content 0.5
database 5
ncs_mask_file test2.mtz
mask_as_mtz
 new_ncs_group

rota_matrix      1.0000     0.0000     0.0000
rota_matrix      0.0000     1.0000     0.0000
rota_matrix      0.0000     0.0000     1.0000
tran_orth       0.0000     0.0000     0.0000
center_orth     27.9638    -9.7513   -13.0742


rota_matrix     -0.9552    -0.2479    -0.1615
rota_matrix     -0.2571     0.4255     0.8677
rota_matrix     -0.1464     0.8703    -0.4702
tran_orth      33.2942     4.6358     2.0025
center_orth     10.9978   -17.9223    -4.5330


rota_matrix     -0.9943     0.0655    -0.0845
rota_matrix      0.0466    -0.4463    -0.8937
rota_matrix     -0.0963    -0.8925     0.4407
tran_orth      38.0383   -24.3301   -12.6676
center_orth     10.7350    -6.8036   -12.3560


rota_matrix      0.9475     0.2220     0.2302
rota_matrix      0.2254    -0.9742     0.0118
rota_matrix      0.2269     0.0407    -0.9731
tran_orth       5.3305   -27.2396   -18.1719
center_orth     26.5426   -11.8055     0.4550

n_refine_rot_trans 4
EOD
phenix.resolve<<EOD
hklin test2.mtz
labin FP=FP PHIB=PHIM FOM=FOMM
hklperfect ncs_mask.mtz
labperfect FP=FP PHIB=PHIM FOM=FOMM
no_build
hkl_offset_file offset.mtz
keep_f_mag
noscale
database 5
EOD


phenix.resolve <<EOD
hklin random.mtz
labin FP=FP PHIB=PHIM
resolution 200 4.5
mask_cycles 1
minor_cycles 1
no_build
solvent_content 0.5
database 5
protein_mask_file test1.mtz
mask_as_mtz
n_refine_rot_trans 4
EOD
phenix.resolve<<EOD
hklin test1.mtz
labin FP=FP PHIB=PHIM FOM=FOMM
hklperfect protein_mask.mtz
labperfect FP=FP PHIB=PHIM FOM=FOMM
no_build
hkl_offset_file offset.mtz
keep_f_mag
noscale
database 5
EOD


phenix.resolve <<EOD
hklin random.mtz
labin FP=FP PHIB=PHIM
resolution 200 4.5
mask_cycles 1
minor_cycles 1
no_build
solvent_content 0.5
database 5
hklout test.mtz
n_refine_rot_trans 4
EOD
phenix.resolve<<EOD
hklin test.mtz
labin FP=FP PHIB=PHIM FOM=FOMM
hklperfect random_denmod.mtz
labperfect FP=FP PHIB=PHIM FOM=FOMM
no_build
hkl_offset_file offset.mtz
keep_f_mag
noscale
database 5
EOD



phenix.resolve <<EOD
hklin random.mtz
labin FP=FP PHIB=PHIM
resolution 200 4.5
mask_cycles 1
minor_cycles 1
no_build
solvent_content 0.5
database 5
ccp4_map_file map.map
n_refine_rot_trans 4
EOD









