#!/bin/csh  -ef
#$ -cwd
phenix.resolve <<EOD
hklin random.mtz
labin FP=FP PHIB=PHIM
resolution 200 4.5
mask_cycles 2
minor_cycles 1
no_build
database 5
hklperfect perfect.mtz
labperfect FP=FP PHIB=PHIC
use_prob
n_refine_rot_trans 4
solvent_content 0.5
solvent_prob_min 0.04

EOD
phenix.resolve<<EOD
hklin resolve.mtz
labin FP=FP PHIB=PHIM FOM=FOMM
hklperfect resolve_std.mtz
labperfect FP=FP PHIB=PHIM FOM=FOMM
no_build
hkl_offset_file offset.mtz
keep_f_mag
noscale
database 5
EOD

