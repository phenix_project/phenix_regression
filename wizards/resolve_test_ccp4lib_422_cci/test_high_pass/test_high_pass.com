#!/bin/csh  -ef
#$ -cwd
phenix.resolve <<EOD|grep '[a-z]'|grep -v 'Cell:'  # gets rid of numbers
hklin a2u-sigmaa.mtz
labin FP=FWT PHIB=PHIC
ncs_only
ha_file ca.pdb
score_only
n_refine_rot_trans 4
res_cut_ncs 3.5
EOD

