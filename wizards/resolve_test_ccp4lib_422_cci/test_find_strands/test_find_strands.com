#!/bin/csh -ef
#$ -cwd
phenix.resolve<<EOD
hklin TEMP_gene-5-perfect.mtz    
labin FP=FP PHIB=PHIC    
no_build    
ha_file NONE   
resolution 1000 3.0    
find_sheets    
rho_min_main_low -100    
rho_min_side_seg -100    
mask_cycles 1     
minor_cycles 0      
solvent_content 0.5    
use_wang    
no_ha     
database 5     
no_optimize_ncs     
EOD
head -50 sheets.pdb

