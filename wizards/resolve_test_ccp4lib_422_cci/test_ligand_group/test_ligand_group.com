#!/bin/csh -ef
#$ -cwd
phenix.resolve<<EOD
hklin ATP.pdb.mtz    
labin FP=FMODEL PHIB=PHIFMODEL
no_build    
ha_file NONE   
n_indiv_tries_min 100    
n_indiv_tries_max 100    
database 5     
ligand_start start.pdb
ligand_file ATP.pdb
test_extra
EOD
phenix.resolve<<EOD
hklin ATP.pdb.mtz    
labin FP=FMODEL PHIB=PHIFMODEL
no_build    
ha_file NONE   
n_indiv_tries_min 100    
n_indiv_tries_max 100    
database 5     
ligand_start start.pdb
ligand_file ATP.pdb
EOD


