#!/bin/csh -ef
#$ -cwd
phenix.resolve<<EOD
hklin perfect.mtz
labin FP=FP PHIB=PHIC
build_only
noget_peaks
superquick_build
track_libs
EOD

