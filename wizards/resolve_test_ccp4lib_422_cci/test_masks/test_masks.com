#!/bin/csh  -ef
#$ -cwd
phenix.resolve <<EOD
hklin perfect.mtz
labin FP=FP PHIB=PHIC
resolution 200 4.5
fom_target 0.5
hklout random.mtz
mask_cycles 0
minor_cycles 0
n_refine_rot_trans 4
EOD
foreach mask_type (use_prob use_wang use_new_prob use_hist_prob)
echo "MASK TYPE: $mask_type"
phenix.resolve <<EOD
hklin random.mtz
labin FP=FP PHIB=PHIM
resolution 200 4.5
mask_cycles 1
minor_cycles 1
no_build
solvent_content 0.5
database 5
hklperfect perfect.mtz
labperfect FP=FP PHIB=PHIC
$mask_type
n_refine_rot_trans 4
EOD
end








