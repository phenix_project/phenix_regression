#!/bin/csh -ef
#$ -cwd
phenix.resolve<<EOD
hklin solve_1.mtz
labin FP=FP SIGFP=SIGFP PHIB=PHIB FOM=FOM HLA=HLA HLB=HLB HLC=HLC HLD=HLD
hklperfect solve_2.mtz
labperfect FP=FP SIGFP=SIGFP PHIB=PHIB FOM=FOM HLA=HLA HLB=HLB HLC=HLC HLD=HLD
no_build
hkl_merge_file test.mtz
keep_f_mag
noscale
database 5
EOD

