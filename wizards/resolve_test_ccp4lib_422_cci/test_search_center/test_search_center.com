#!/bin/csh -ef
#$ -cwd
phenix.resolve<<EOD
hklin resolve_map.mtz    
labin FP=FP PHIB=PHIM FOM=FOMM   
no_build    
RESOLUTION 1000.0 3.0    
ha_file NONE   
model all.pdb     
res_ligand_near 94
use_wang    
no_ha     
ligand_file side.pdb     
n_indiv_tries_min 2
n_indiv_tries_max 2
n_template_atom 4   
database 5     
no_optimize_ncs     
spg_name_use P 1 21 1    
EOD

