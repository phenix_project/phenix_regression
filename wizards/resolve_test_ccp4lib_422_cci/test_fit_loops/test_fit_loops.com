#!/bin/csh  -ef
#$ -cwd
phenix.resolve<<EOD
hklin resolve_work.mtz   
labin FP=FP PHIB=PHIM FOM=FOMM   
extend_only    
RESOLUTION 1000.0 2.0    
ha_file NONE   
rad_mask 1.5   
seq_file seq_from_pdb.dat   
model mask.pdb      
loop_only      
build_outside_model    
no_sub_segments     
pdb_in in.pdb     
skip_hetatm    
n_random_loop 20    
loop_length 7     
rms_random_loop 0.3    
rho_min_main_low 1.0     
rho_min_main_base 1.0    
i_ran_seed 672913   
use_wang    
no_ha     
n_xyz 120 48 72     
database 5     
no_optimize_ncs     
spg_name_use C 1 2 1     
start_chain 1 1     
group_ca_length 4   
group_length 2      
n_random_frag 0     
n_refine_rot_trans 4
EOD
cat resolve_unassigned.pdb








