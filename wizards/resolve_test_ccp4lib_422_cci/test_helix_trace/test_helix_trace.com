#!/bin/csh -ef
#$ -cwd
phenix.resolve --trace<<EOD
hklin helix.pdb.mtz
labin FP=FMODEL PHIB=PHIFMODEL
trace_chain
fill
res_fill 3.0
resolution 200 3.0
solvent_content 0.4
EOD

