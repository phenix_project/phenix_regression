#!/bin/csh -ef
#$ -cwd
phenix.resolve<<EOD
hklin TEMP_perfect.mtz   
labin FP=FP PHIB=PHIC    
no_build    
ha_file NONE   
pdb_in TEMP_coords.pdb   
copy_pdb
omit
omit_box 3
EOD
echo "omitted atoms: `cat resolve.pdb|grep ' 0\.00 '|wc`"

