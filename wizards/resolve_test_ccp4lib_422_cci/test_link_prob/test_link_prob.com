#!/bin/csh -ef
#$ -cwd
phenix.resolve<<EOD
hklin partial.pdb.mtz
labin FP=FMODEL PHIB=PHIFMODEL
pdb_in partial.pdb
seq_file gene-5.seq
extend_only
EOD

