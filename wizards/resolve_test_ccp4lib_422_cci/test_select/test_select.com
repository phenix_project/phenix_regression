#!/bin/csh  -ef
#$ -cwd
phenix.resolve <<EOD
hklin random.mtz
labin FP=FP PHIB=PHIM
resolution 200 4.5
mask_cycles 1
minor_cycles 1
no_build
solvent_content 0.5
database 5
select_refl 50
EOD

