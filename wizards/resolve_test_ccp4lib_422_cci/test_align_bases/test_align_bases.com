#!/bin/csh -ef
#$ -cwd
phenix.resolve<<EOD
hklin resolve_work.mtz   
labin FP=FP PHIB=PHIM FOM=FOMM   
extend_only    
resolution 1000 2.0    
ha_file NONE   
seq_file seq_from_pdb.dat   
skip_hetatm    
pdb_in all.pdb      
group_ca_length 4   
cut_up_model   
use_wang    
no_ha     
n_xyz 36 36 36      
database 5     
use_any_side   
no_merge_ncs_copies    
no_optimize_ncs     
nohl      
spg_name_use P 1    
start_chain 1 2     
start_chain 2   22     
build_rna      
compare_all    
group_length 5      
rho_min_main_low -10.0   
start_segment 5     
dist_ca_approach 5.0     
max_segment 5     
r_match 2.5    
use_any_side   
rho_min_side_seg -10.0   
group_ca_length 3   
rho_min_side -10.0     
rho_min_main_base -10.0     
dist_cut_assemble 1.0    
rho_min_main_seg -10.0   
rms_random_frag 0.5    
d_cut_fragment 0.01 0.01    
trim      
n_random_frag 0     
EOD

