#!/bin/csh -ef
#$ -cwd
phenix.resolve<<EOD
hklin exptl_fobs_phases_freeR_flags.mtz    
labin FP=FP PHIB=PHIM FOM=FOMM   
solvent_content 0.6    
no_build    
resolution 3.0 500.0     
ha_file NONE   
composite_all     
dump_composite      
composite_pdb refine.pdb_   
composite_pdb_first 1    
composite_pdb_last 1     
use_wang    
no_ha     
n_xyz 36 36 36      
database 5     
no_optimize_ncs     
spg_name_use P 1 21 1    
start_chain 1   92     
group_ca_length 4   
group_length 2      
n_random_frag 0     
EOD
phenix.resolve<<EOD
hklin exptl_fobs_phases_freeR_flags.mtz    
labin FP=FP SIGFP=SIGFP PHIB=PHIM FOM=FOMM    
labin HLA=HLAM HLB=HLBM HLC=HLCM HLD=HLDM  
labin FreeR_flag=FreeR_flag   
hklout image.mtz    
solvent_content 0.6    
no_build    
resolution 3.0 500.0     
ha_file NONE   
pattern_phase     
cc_map_file dump.map     
use_wang    
no_ha     
n_xyz 36 36 36      
database 5     
no_optimize_ncs     
spg_name_use P 1 21 1    
start_chain 1   92     
group_ca_length 4   
group_length 2      
n_random_frag 0     
get_cc_mask_image
image_only
EOD
phenix.resolve<<EOD
hklin exptl_fobs_phases_freeR_flags.mtz    
labin FP=FP SIGFP=SIGFP PHIB=PHIM FOM=FOMM    
labin HLA=HLAM HLB=HLBM HLC=HLCM HLD=HLDM  
labin FreeR_flag=FreeR_flag   
hklout image.mtz    
solvent_content 0.6    
no_build    
resolution 3.0 500.0     
ha_file NONE   
pattern_phase     
cc_map_file dump.map     
use_wang    
no_ha     
n_xyz 36 36 36      
database 5     
no_optimize_ncs     
spg_name_use P 1 21 1    
start_chain 1   92     
group_ca_length 4   
group_length 2      
n_random_frag 0     
get_cc_mask
image_only
EOD

