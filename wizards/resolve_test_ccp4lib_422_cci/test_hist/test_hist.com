#!/bin/csh -ef
#$ -cwd
phenix.resolve<<EOD|grep -v "Maximum at"|grep -v 'done with apply_hist'
hklin solve_1.mtz
labin FP=FP SIGFP=SIGFP PHIB=PHIB FOM=FOM 
hklperfect solve_2.mtz
labperfect FP=FP SIGFP=SIGFP PHIB=PHIB FOM=FOM 
analyze_perf
database 5
n_try_hist 2
n_inv 1
EOD

