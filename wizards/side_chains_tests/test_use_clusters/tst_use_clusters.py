from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_use_clusters")
  print("")
  log_file=os.path.join(os.getcwd(),"test_use_clusters.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  cmds='''0,2,     !1=get clusters; 0=rna 1=dna 2=prot
0.0,    ! additional B-factor for all atoms
0.8,3,0.0    ! max rmsd for a group, min_examples, rmsd_ok
0.10    !   fraction allowed to be in misc group
1
nsf-d2_reference.pdb
  '''
  from phenix.autosol.run_program import run_program
  run_program('solve_resolve.side_chains', cmds,'solve_resolve.side_chains.log',None,None,None)
  print(open('solve_resolve.side_chains.log').read(), file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_use_clusters',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['side_chains_tests','test_use_clusters']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
