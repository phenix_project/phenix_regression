#!/bin/csh
setenv SOLVETMPDIR /var/tmp
#
setenv pdb_lib ../pdb_lib/  # make the path short or it won't work
#  put name of PDB model with all source models here:
setenv input_model NONE
#
#
echo "Working in `pwd`"
if (-d segments )rm -rf segments
mkdir segments
#
rm -rf side_chains_best.dat rho_map_new.dat side_chains_rho.dat
echo "getting clusters..."
solve_resolve.side_chains<<EOD > side_chains_clusters.log
1,2,     !1=get clusters; 0=rna 1=dna 2=prot
0.8    ! max rmsd for a group
0.10    !   fraction allowed to be in misc group
574
$pdb_lib/pdb119l.ent
$pdb_lib/pdb153l.ent
$pdb_lib/pdb16pk.ent
$pdb_lib/pdb1a12.ent
$pdb_lib/pdb1a2p.ent
$pdb_lib/pdb1a2z.ent
$pdb_lib/pdb1a3a.ent
$pdb_lib/pdb1a4i.ent
$pdb_lib/pdb1a68.ent
$pdb_lib/pdb1a6m.ent
$pdb_lib/pdb1a8d.ent
$pdb_lib/pdb1a8e.ent
$pdb_lib/pdb1a8i.ent
$pdb_lib/pdb1a9x.ent
$pdb_lib/pdb1aba.ent
$pdb_lib/pdb1ads.ent
$pdb_lib/pdb1afw.ent
$pdb_lib/pdb1agj.ent
$pdb_lib/pdb1aho.ent
$pdb_lib/pdb1aie.ent
$pdb_lib/pdb1ajs.ent
$pdb_lib/pdb1ako.ent
$pdb_lib/pdb1al3.ent
$pdb_lib/pdb1amm.ent
$pdb_lib/pdb1amt.ent
$pdb_lib/pdb1aoh.ent
$pdb_lib/pdb1aop.ent
$pdb_lib/pdb1aqb.ent
$pdb_lib/pdb1arb.ent
$pdb_lib/pdb1aru.ent
$pdb_lib/pdb1atg.ent
$pdb_lib/pdb1atl.ent
$pdb_lib/pdb1atz.ent
$pdb_lib/pdb1avw.ent
$pdb_lib/pdb1axn.ent
$pdb_lib/pdb1ay7.ent
$pdb_lib/pdb1ayl.ent
$pdb_lib/pdb1b0b.ent
$pdb_lib/pdb1b0u.ent
$pdb_lib/pdb1b0y.ent
$pdb_lib/pdb1b16.ent
$pdb_lib/pdb1b2p.ent
$pdb_lib/pdb1b3a.ent
$pdb_lib/pdb1b4k.ent
$pdb_lib/pdb1b4v.ent
$pdb_lib/pdb1b5e.ent
$pdb_lib/pdb1b67.ent
$pdb_lib/pdb1b6a.ent
$pdb_lib/pdb1b6g.ent
$pdb_lib/pdb1b8o.ent
$pdb_lib/pdb1bd8.ent
$pdb_lib/pdb1bdm.ent
$pdb_lib/pdb1bdo.ent
$pdb_lib/pdb1bfd.ent
$pdb_lib/pdb1bgf.ent
$pdb_lib/pdb1bk7.ent
$pdb_lib/pdb1bkf.ent
$pdb_lib/pdb1bkr.ent
$pdb_lib/pdb1bm8.ent
$pdb_lib/pdb1bs0.ent
$pdb_lib/pdb1bte.ent
$pdb_lib/pdb1bu7.ent
$pdb_lib/pdb1bue.ent
$pdb_lib/pdb1bup.ent
$pdb_lib/pdb1bur.ent
$pdb_lib/pdb1bx4.ent
$pdb_lib/pdb1bx7.ent
$pdb_lib/pdb1bxa.ent
$pdb_lib/pdb1bxo.ent
$pdb_lib/pdb1byi.ent
$pdb_lib/pdb1byq.ent
$pdb_lib/pdb1c0p.ent
$pdb_lib/pdb1c1d.ent
$pdb_lib/pdb1c1k.ent
$pdb_lib/pdb1c24.ent
$pdb_lib/pdb1c3c.ent
$pdb_lib/pdb1c3p.ent
$pdb_lib/pdb1c3w.ent
$pdb_lib/pdb1c44.ent
$pdb_lib/pdb1c4q.ent
$pdb_lib/pdb1c52.ent
$pdb_lib/pdb1c5e.ent
$pdb_lib/pdb1c5y.ent
$pdb_lib/pdb1c75.ent
$pdb_lib/pdb1c7k.ent
$pdb_lib/pdb1c7s.ent
$pdb_lib/pdb1c8c.ent
$pdb_lib/pdb1c9o.ent
$pdb_lib/pdb1cc8.ent
$pdb_lib/pdb1ccw.ent
$pdb_lib/pdb1cem.ent
$pdb_lib/pdb1cex.ent
$pdb_lib/pdb1cg5.ent
$pdb_lib/pdb1chd.ent
$pdb_lib/pdb1cjc.ent
$pdb_lib/pdb1cjw.ent
$pdb_lib/pdb1cmb.ent
$pdb_lib/pdb1cnv.ent
$pdb_lib/pdb1cnz.ent
$pdb_lib/pdb1cru.ent
$pdb_lib/pdb1cs1.ent
$pdb_lib/pdb1cse.ent
$pdb_lib/pdb1csh.ent
$pdb_lib/pdb1ctf.ent
$pdb_lib/pdb1ctj.ent
$pdb_lib/pdb1ctq.ent
$pdb_lib/pdb1cuo.ent
$pdb_lib/pdb1cv8.ent
$pdb_lib/pdb1cxp.ent
$pdb_lib/pdb1cxq.ent
$pdb_lib/pdb1cy5.ent
$pdb_lib/pdb1cyo.ent
$pdb_lib/pdb1czf.ent
$pdb_lib/pdb1czp.ent
$pdb_lib/pdb1d02.ent
$pdb_lib/pdb1d0d.ent
$pdb_lib/pdb1d0i.ent
$pdb_lib/pdb1d1q.ent
$pdb_lib/pdb1d3g.ent
$pdb_lib/pdb1d3v.ent
$pdb_lib/pdb1d4o.ent
$pdb_lib/pdb1d4x.ent
$pdb_lib/pdb1d5t.ent
$pdb_lib/pdb1d8w.ent
$pdb_lib/pdb1dbf.ent
$pdb_lib/pdb1dbg.ent
$pdb_lib/pdb1dbx.ent
$pdb_lib/pdb1dc1.ent
$pdb_lib/pdb1dci.ent
$pdb_lib/pdb1dcs.ent
$pdb_lib/pdb1deo.ent
$pdb_lib/pdb1df4.ent
$pdb_lib/pdb1dfm.ent
$pdb_lib/pdb1dg6.ent
$pdb_lib/pdb1dgw.ent
$pdb_lib/pdb1dhn.ent
$pdb_lib/pdb1di6.ent
$pdb_lib/pdb1din.ent
$pdb_lib/pdb1dj0.ent
$pdb_lib/pdb1dk0.ent
$pdb_lib/pdb1dk8.ent
$pdb_lib/pdb1dl5.ent
$pdb_lib/pdb1dlf.ent
$pdb_lib/pdb1dlw.ent
$pdb_lib/pdb1dmh.ent
$pdb_lib/pdb1dos.ent
$pdb_lib/pdb1dow.ent
$pdb_lib/pdb1doz.ent
$pdb_lib/pdb1dp0.ent
$pdb_lib/pdb1dp7.ent
$pdb_lib/pdb1dpj.ent
$pdb_lib/pdb1dps.ent
$pdb_lib/pdb1dqg.ent
$pdb_lib/pdb1dqs.ent
$pdb_lib/pdb1dqz.ent
$pdb_lib/pdb1ds1.ent
$pdb_lib/pdb1dsz.ent
$pdb_lib/pdb1dtd.ent
$pdb_lib/pdb1dus.ent
$pdb_lib/pdb1duv.ent
$pdb_lib/pdb1dvj.ent
$pdb_lib/pdb1dwk.ent
$pdb_lib/pdb1dxe.ent
$pdb_lib/pdb1dxg.ent
$pdb_lib/pdb1dy5.ent
$pdb_lib/pdb1dym.ent
$pdb_lib/pdb1dyp.ent
$pdb_lib/pdb1dys.ent
$pdb_lib/pdb1dzo.ent
$pdb_lib/pdb1e19.ent
$pdb_lib/pdb1e1a.ent
$pdb_lib/pdb1e29.ent
$pdb_lib/pdb1e2g.ent
$pdb_lib/pdb1e2u.ent
$pdb_lib/pdb1e30.ent
$pdb_lib/pdb1e39.ent
$pdb_lib/pdb1e3a.ent
$pdb_lib/pdb1e3d.ent
$pdb_lib/pdb1e3u.ent
$pdb_lib/pdb1e4c.ent
$pdb_lib/pdb1e4m.ent
$pdb_lib/pdb1e58.ent
$pdb_lib/pdb1e5k.ent
$pdb_lib/pdb1e5m.ent
$pdb_lib/pdb1e5p.ent
$pdb_lib/pdb1e6b.ent
$pdb_lib/pdb1e6c.ent
$pdb_lib/pdb1e6u.ent
$pdb_lib/pdb1e7l.ent
$pdb_lib/pdb1e85.ent
$pdb_lib/pdb1e9g.ent
$pdb_lib/pdb1eaj.ent
$pdb_lib/pdb1ecs.ent
$pdb_lib/pdb1ed8.ent
$pdb_lib/pdb1edg.ent
$pdb_lib/pdb1edm.ent
$pdb_lib/pdb1edq.ent
$pdb_lib/pdb1eex.ent
$pdb_lib/pdb1eg9.ent
$pdb_lib/pdb1egu.ent
$pdb_lib/pdb1ej0.ent
$pdb_lib/pdb1ej8.ent
$pdb_lib/pdb1ejg.ent
$pdb_lib/pdb1el4.ent
$pdb_lib/pdb1el5.ent
$pdb_lib/pdb1elk.ent
$pdb_lib/pdb1elw.ent
$pdb_lib/pdb1en2.ent
$pdb_lib/pdb1eok.ent
$pdb_lib/pdb1ep0.ent
$pdb_lib/pdb1epx.ent
$pdb_lib/pdb1eqo.ent
$pdb_lib/pdb1erx.ent
$pdb_lib/pdb1erz.ent
$pdb_lib/pdb1es9.ent
$pdb_lib/pdb1esi.ent
$pdb_lib/pdb1et1.ent
$pdb_lib/pdb1eu1.ent
$pdb_lib/pdb1euv.ent
$pdb_lib/pdb1euw.ent
$pdb_lib/pdb1evy.ent
$pdb_lib/pdb1ew4.ent
$pdb_lib/pdb1ew6.ent
$pdb_lib/pdb1ewf.ent
$pdb_lib/pdb1eye.ent
$pdb_lib/pdb1eyv.ent
$pdb_lib/pdb1eyz.ent
$pdb_lib/pdb1ezg.ent
$pdb_lib/pdb1ezm.ent
$pdb_lib/pdb1ezw.ent
$pdb_lib/pdb1f0i.ent
$pdb_lib/pdb1f0l.ent
$pdb_lib/pdb1f24.ent
$pdb_lib/pdb1f2t.ent
$pdb_lib/pdb1f46.ent
$pdb_lib/pdb1f4p.ent
$pdb_lib/pdb1f5f.ent
$pdb_lib/pdb1f60.ent
$pdb_lib/pdb1f74.ent
$pdb_lib/pdb1f7l.ent
$pdb_lib/pdb1f86.ent
$pdb_lib/pdb1f8e.ent
$pdb_lib/pdb1f94.ent
$pdb_lib/pdb1fa8.ent
$pdb_lib/pdb1faz.ent
$pdb_lib/pdb1fcy.ent
$pdb_lib/pdb1fd3.ent
$pdb_lib/pdb1fe6.ent
$pdb_lib/pdb1fgl.ent
$pdb_lib/pdb1fhu.ent
$pdb_lib/pdb1fi2.ent
$pdb_lib/pdb1fiu.ent
$pdb_lib/pdb1fj2.ent
$pdb_lib/pdb1fjh.ent
$pdb_lib/pdb1fjj.ent
$pdb_lib/pdb1fk5.ent
$pdb_lib/pdb1flm.ent
$pdb_lib/pdb1flt.ent
$pdb_lib/pdb1fm0.ent
$pdb_lib/pdb1fn8.ent
$pdb_lib/pdb1fn9.ent
$pdb_lib/pdb1fna.ent
$pdb_lib/pdb1fnc.ent
$pdb_lib/pdb1fo8.ent
$pdb_lib/pdb1fqt.ent
$pdb_lib/pdb1fr3.ent
$pdb_lib/pdb1fs7.ent
$pdb_lib/pdb1fsg.ent
$pdb_lib/pdb1ft5.ent
$pdb_lib/pdb1ftr.ent
$pdb_lib/pdb1fvg.ent
$pdb_lib/pdb1fvk.ent
$pdb_lib/pdb1fw9.ent
$pdb_lib/pdb1fx2.ent
$pdb_lib/pdb1fxm.ent
$pdb_lib/pdb1fxo.ent
$pdb_lib/pdb1fye.ent
$pdb_lib/pdb1fzo.ent
$pdb_lib/pdb1g2b.ent
$pdb_lib/pdb1g2y.ent
$pdb_lib/pdb1g3p.ent
$pdb_lib/pdb1g4i.ent
$pdb_lib/pdb1g57.ent
$pdb_lib/pdb1g5t.ent
$pdb_lib/pdb1g61.ent
$pdb_lib/pdb1g66.ent
$pdb_lib/pdb1g6s.ent
$pdb_lib/pdb1g6u.ent
$pdb_lib/pdb1g6x.ent
$pdb_lib/pdb1g7a.ent
$pdb_lib/pdb1g7f.ent
$pdb_lib/pdb1g8k.ent
$pdb_lib/pdb1g8m.ent
$pdb_lib/pdb1g8q.ent
$pdb_lib/pdb1g9o.ent
$pdb_lib/pdb1ga6.ent
$pdb_lib/pdb1gai.ent
$pdb_lib/pdb1gbg.ent
$pdb_lib/pdb1gci.ent
$pdb_lib/pdb1gd0.ent
$pdb_lib/pdb1gd1.ent
$pdb_lib/pdb1gdo.ent
$pdb_lib/pdb1gof.ent
$pdb_lib/pdb1gpe.ent
$pdb_lib/pdb1guq.ent
$pdb_lib/pdb1h4x.ent
$pdb_lib/pdb1h61.ent
$pdb_lib/pdb1h70.ent
$pdb_lib/pdb1h75.ent
$pdb_lib/pdb1h8d.ent
$pdb_lib/pdb1h96.ent
$pdb_lib/pdb1h97.ent
$pdb_lib/pdb1ha1.ent
$pdb_lib/pdb1hbn.ent
$pdb_lib/pdb1hbz.ent
$pdb_lib/pdb1hdo.ent
$pdb_lib/pdb1het.ent
$pdb_lib/pdb1hfe.ent
$pdb_lib/pdb1hg7.ent
$pdb_lib/pdb1hh8.ent
$pdb_lib/pdb1hnj.ent
$pdb_lib/pdb1hq1.ent
$pdb_lib/pdb1htr.ent
$pdb_lib/pdb1hvb.ent
$pdb_lib/pdb1hw1.ent
$pdb_lib/pdb1hx0.ent
$pdb_lib/pdb1hx6.ent
$pdb_lib/pdb1hxi.ent
$pdb_lib/pdb1hxn.ent
$pdb_lib/pdb1hyo.ent
$pdb_lib/pdb1hyp.ent
$pdb_lib/pdb1hz6.ent
$pdb_lib/pdb1hzt.ent
$pdb_lib/pdb1hzy.ent
$pdb_lib/pdb1i0h.ent
$pdb_lib/pdb1i0s.ent
$pdb_lib/pdb1i0v.ent
$pdb_lib/pdb1i19.ent
$pdb_lib/pdb1i27.ent
$pdb_lib/pdb1i2t.ent
$pdb_lib/pdb1i45.ent
$pdb_lib/pdb1i4f.ent
$pdb_lib/pdb1i5g.ent
$pdb_lib/pdb1i6w.ent
$pdb_lib/pdb1i71.ent
$pdb_lib/pdb1i76.ent
$pdb_lib/pdb1i8o.ent
$pdb_lib/pdb1i9s.ent
$pdb_lib/pdb1i9z.ent
$pdb_lib/pdb1iab.ent
$pdb_lib/pdb1iat.ent
$pdb_lib/pdb1iby.ent
$pdb_lib/pdb1ida.ent
$pdb_lib/pdb1ifc.ent
$pdb_lib/pdb1iho.ent
$pdb_lib/pdb1ihr.ent
$pdb_lib/pdb1iib.ent
$pdb_lib/pdb1isu.ent
$pdb_lib/pdb1ixh.ent
$pdb_lib/pdb1j6z.ent
$pdb_lib/pdb1j79.ent
$pdb_lib/pdb1j97.ent
$pdb_lib/pdb1j98.ent
$pdb_lib/pdb1j9q.ent
$pdb_lib/pdb1jb3.ent
$pdb_lib/pdb1jbe.ent
$pdb_lib/pdb1jd0.ent
$pdb_lib/pdb1jer.ent
$pdb_lib/pdb1jhg.ent
$pdb_lib/pdb1jiw.ent
$pdb_lib/pdb1jix.ent
$pdb_lib/pdb1jjt.ent
$pdb_lib/pdb1jp3.ent
$pdb_lib/pdb1jp4.ent
$pdb_lib/pdb1jqc.ent
$pdb_lib/pdb1kap.ent
$pdb_lib/pdb1kid.ent
$pdb_lib/pdb1koe.ent
$pdb_lib/pdb1kp6.ent
$pdb_lib/pdb1kpe.ent
$pdb_lib/pdb1kpt.ent
$pdb_lib/pdb1kve.ent
$pdb_lib/pdb1lam.ent
$pdb_lib/pdb1lbu.ent
$pdb_lib/pdb1lcl.ent
$pdb_lib/pdb1ldg.ent
$pdb_lib/pdb1lkk.ent
$pdb_lib/pdb1lmb.ent
$pdb_lib/pdb1lst.ent
$pdb_lib/pdb1luc.ent
$pdb_lib/pdb1mfm.ent
$pdb_lib/pdb1mgt.ent
$pdb_lib/pdb1mla.ent
$pdb_lib/pdb1mml.ent
$pdb_lib/pdb1mof.ent
$pdb_lib/pdb1mol.ent
$pdb_lib/pdb1moq.ent
$pdb_lib/pdb1mpg.ent
$pdb_lib/pdb1mrj.ent
$pdb_lib/pdb1mrp.ent
$pdb_lib/pdb1msk.ent
$pdb_lib/pdb1mty.ent
$pdb_lib/pdb1mug.ent
$pdb_lib/pdb1mun.ent
$pdb_lib/pdb1nbc.ent
$pdb_lib/pdb1nkd.ent
$pdb_lib/pdb1nls.ent
$pdb_lib/pdb1nox.ent
$pdb_lib/pdb1npk.ent
$pdb_lib/pdb1nps.ent
$pdb_lib/pdb1nul.ent
$pdb_lib/pdb1oaa.ent
$pdb_lib/pdb1one.ent
$pdb_lib/pdb1opd.ent
$pdb_lib/pdb1orc.ent
$pdb_lib/pdb1pa2.ent
$pdb_lib/pdb1pcf.ent
$pdb_lib/pdb1pda.ent
$pdb_lib/pdb1pdo.ent
$pdb_lib/pdb1pgs.ent
$pdb_lib/pdb1pmi.ent
$pdb_lib/pdb1pot.ent
$pdb_lib/pdb1ppn.ent
$pdb_lib/pdb1psr.ent
$pdb_lib/pdb1pym.ent
$pdb_lib/pdb1qb7.ent
$pdb_lib/pdb1qcx.ent
$pdb_lib/pdb1qcz.ent
$pdb_lib/pdb1qd1.ent
$pdb_lib/pdb1qdd.ent
$pdb_lib/pdb1qf8.ent
$pdb_lib/pdb1qfm.ent
$pdb_lib/pdb1qft.ent
$pdb_lib/pdb1qg8.ent
$pdb_lib/pdb1qgi.ent
$pdb_lib/pdb1qgw.ent
$pdb_lib/pdb1qgx.ent
$pdb_lib/pdb1qh4.ent
$pdb_lib/pdb1qh8.ent
$pdb_lib/pdb1qhv.ent
$pdb_lib/pdb1qj4.ent
$pdb_lib/pdb1qj5.ent
$pdb_lib/pdb1qjp.ent
$pdb_lib/pdb1qkr.ent
$pdb_lib/pdb1qks.ent
$pdb_lib/pdb1ql0.ent
$pdb_lib/pdb1qlw.ent
$pdb_lib/pdb1qmg.ent
$pdb_lib/pdb1qmv.ent
$pdb_lib/pdb1qna.ent
$pdb_lib/pdb1qnf.ent
$pdb_lib/pdb1qnr.ent
$pdb_lib/pdb1qop.ent
$pdb_lib/pdb1qow.ent
$pdb_lib/pdb1qpc.ent
$pdb_lib/pdb1qq7.ent
$pdb_lib/pdb1qq9.ent
$pdb_lib/pdb1qqf.ent
$pdb_lib/pdb1qre.ent
$pdb_lib/pdb1qrr.ent
$pdb_lib/pdb1qsa.ent
$pdb_lib/pdb1qsu.ent
$pdb_lib/pdb1qtn.ent
$pdb_lib/pdb1qto.ent
$pdb_lib/pdb1qts.ent
$pdb_lib/pdb1qtw.ent
$pdb_lib/pdb1qu9.ent
$pdb_lib/pdb1qus.ent
$pdb_lib/pdb1ra9.ent
$pdb_lib/pdb1rb9.ent
$pdb_lib/pdb1rge.ent
$pdb_lib/pdb1rhs.ent
$pdb_lib/pdb1rie.ent
$pdb_lib/pdb1sbp.ent
$pdb_lib/pdb1sbw.ent
$pdb_lib/pdb1sgp.ent
$pdb_lib/pdb1slu.ent
$pdb_lib/pdb1sml.ent
$pdb_lib/pdb1svf.ent
$pdb_lib/pdb1svy.ent
$pdb_lib/pdb1swu.ent
$pdb_lib/pdb1tag.ent
$pdb_lib/pdb1tca.ent
$pdb_lib/pdb1thf.ent
$pdb_lib/pdb1thg.ent
$pdb_lib/pdb1thv.ent
$pdb_lib/pdb1tif.ent
$pdb_lib/pdb1toa.ent
$pdb_lib/pdb1tvx.ent
$pdb_lib/pdb1tx4.ent
$pdb_lib/pdb1tyv.ent
$pdb_lib/pdb1ubi.ent
$pdb_lib/pdb1unk.ent
$pdb_lib/pdb1uro.ent
$pdb_lib/pdb1ush.ent
$pdb_lib/pdb1vfy.ent
$pdb_lib/pdb1vhh.ent
$pdb_lib/pdb1vie.ent
$pdb_lib/pdb1vjs.ent
$pdb_lib/pdb1vns.ent
$pdb_lib/pdb1vqb.ent
$pdb_lib/pdb1vsr.ent
$pdb_lib/pdb1wap.ent
$pdb_lib/pdb1wfb.ent
$pdb_lib/pdb1whi.ent
$pdb_lib/pdb1xnb.ent
$pdb_lib/pdb1yac.ent
$pdb_lib/pdb1yge.ent
$pdb_lib/pdb1zin.ent
$pdb_lib/pdb256b.ent
$pdb_lib/pdb2a0b.ent
$pdb_lib/pdb2acy.ent
$pdb_lib/pdb2ahj.ent
$pdb_lib/pdb2arc.ent
$pdb_lib/pdb2baa.ent
$pdb_lib/pdb2bbk.ent
$pdb_lib/pdb2btc.ent
$pdb_lib/pdb2cpl.ent
$pdb_lib/pdb2ctc.ent
$pdb_lib/pdb2dri.ent
$pdb_lib/pdb2end.ent
$pdb_lib/pdb2eng.ent
$pdb_lib/pdb2erl.ent
$pdb_lib/pdb2fcb.ent
$pdb_lib/pdb2fdn.ent
$pdb_lib/pdb2gdm.ent
$pdb_lib/pdb2hmq.ent
$pdb_lib/pdb2igd.ent
$pdb_lib/pdb2ilk.ent
$pdb_lib/pdb2lis.ent
$pdb_lib/pdb2mcm.ent
$pdb_lib/pdb2nac.ent
$pdb_lib/pdb2nlr.ent
$pdb_lib/pdb2olb.ent
$pdb_lib/pdb2por.ent
$pdb_lib/pdb2pth.ent
$pdb_lib/pdb2pvb.ent
$pdb_lib/pdb2pvi.ent
$pdb_lib/pdb2rn2.ent
$pdb_lib/pdb2sak.ent
$pdb_lib/pdb2sga.ent
$pdb_lib/pdb2sic.ent
$pdb_lib/pdb2sns.ent
$pdb_lib/pdb2tgi.ent
$pdb_lib/pdb2tps.ent
$pdb_lib/pdb2trx.ent
$pdb_lib/pdb2utg.ent
$pdb_lib/pdb2zta.ent
$pdb_lib/pdb3cao.ent
$pdb_lib/pdb3chb.ent
$pdb_lib/pdb3cla.ent
$pdb_lib/pdb3cyr.ent
$pdb_lib/pdb3ebx.ent
$pdb_lib/pdb3ezm.ent
$pdb_lib/pdb3grs.ent
$pdb_lib/pdb3lzt.ent
$pdb_lib/pdb3nul.ent
$pdb_lib/pdb3pyp.ent
$pdb_lib/pdb3seb.ent
$pdb_lib/pdb3sil.ent
$pdb_lib/pdb3std.ent
$pdb_lib/pdb3vub.ent
$pdb_lib/pdb3xis.ent
$pdb_lib/pdb4eug.ent
$pdb_lib/pdb4fgf.ent
$pdb_lib/pdb4pga.ent
$pdb_lib/pdb4uag.ent
$pdb_lib/pdb4ubp.ent
$pdb_lib/pdb5rub.ent
$pdb_lib/pdb6gsv.ent
$pdb_lib/pdb6rlx.ent
$pdb_lib/pdb7a3h.ent
$pdb_lib/pdb7odc.ent
$pdb_lib/pdb8abp.ent
EOD
#
echo "done with clusters"
echo "mapping #1 through 500..."
solve_resolve.side_chains<<EOD > side_chains_mapping.log
0,2,     !1=get clusters; 0=rna 1=dna 2=prot
0.0,    ! additional B-factor for all atoms
0.8    ! max rmsd for a group
0.10    !   fraction allowed to be in misc group
500
$pdb_lib/pdb119l.ent
$pdb_lib/pdb153l.ent
$pdb_lib/pdb16pk.ent
$pdb_lib/pdb1a12.ent
$pdb_lib/pdb1a2p.ent
$pdb_lib/pdb1a2z.ent
$pdb_lib/pdb1a3a.ent
$pdb_lib/pdb1a4i.ent
$pdb_lib/pdb1a68.ent
$pdb_lib/pdb1a6m.ent
$pdb_lib/pdb1a8d.ent
$pdb_lib/pdb1a8e.ent
$pdb_lib/pdb1a8i.ent
$pdb_lib/pdb1a9x.ent
$pdb_lib/pdb1aba.ent
$pdb_lib/pdb1ads.ent
$pdb_lib/pdb1afw.ent
$pdb_lib/pdb1agj.ent
$pdb_lib/pdb1aho.ent
$pdb_lib/pdb1aie.ent
$pdb_lib/pdb1ajs.ent
$pdb_lib/pdb1ako.ent
$pdb_lib/pdb1al3.ent
$pdb_lib/pdb1amm.ent
$pdb_lib/pdb1amt.ent
$pdb_lib/pdb1aoh.ent
$pdb_lib/pdb1aop.ent
$pdb_lib/pdb1aqb.ent
$pdb_lib/pdb1arb.ent
$pdb_lib/pdb1aru.ent
$pdb_lib/pdb1atg.ent
$pdb_lib/pdb1atl.ent
$pdb_lib/pdb1atz.ent
$pdb_lib/pdb1avw.ent
$pdb_lib/pdb1axn.ent
$pdb_lib/pdb1ay7.ent
$pdb_lib/pdb1ayl.ent
$pdb_lib/pdb1b0b.ent
$pdb_lib/pdb1b0u.ent
$pdb_lib/pdb1b0y.ent
$pdb_lib/pdb1b16.ent
$pdb_lib/pdb1b2p.ent
$pdb_lib/pdb1b3a.ent
$pdb_lib/pdb1b4k.ent
$pdb_lib/pdb1b4v.ent
$pdb_lib/pdb1b5e.ent
$pdb_lib/pdb1b67.ent
$pdb_lib/pdb1b6a.ent
$pdb_lib/pdb1b6g.ent
$pdb_lib/pdb1b8o.ent
$pdb_lib/pdb1bd8.ent
$pdb_lib/pdb1bdm.ent
$pdb_lib/pdb1bdo.ent
$pdb_lib/pdb1bfd.ent
$pdb_lib/pdb1bgf.ent
$pdb_lib/pdb1bk7.ent
$pdb_lib/pdb1bkf.ent
$pdb_lib/pdb1bkr.ent
$pdb_lib/pdb1bm8.ent
$pdb_lib/pdb1bs0.ent
$pdb_lib/pdb1bte.ent
$pdb_lib/pdb1bu7.ent
$pdb_lib/pdb1bue.ent
$pdb_lib/pdb1bup.ent
$pdb_lib/pdb1bur.ent
$pdb_lib/pdb1bx4.ent
$pdb_lib/pdb1bx7.ent
$pdb_lib/pdb1bxa.ent
$pdb_lib/pdb1bxo.ent
$pdb_lib/pdb1byi.ent
$pdb_lib/pdb1byq.ent
$pdb_lib/pdb1c0p.ent
$pdb_lib/pdb1c1d.ent
$pdb_lib/pdb1c1k.ent
$pdb_lib/pdb1c24.ent
$pdb_lib/pdb1c3c.ent
$pdb_lib/pdb1c3p.ent
$pdb_lib/pdb1c3w.ent
$pdb_lib/pdb1c44.ent
$pdb_lib/pdb1c4q.ent
$pdb_lib/pdb1c52.ent
$pdb_lib/pdb1c5e.ent
$pdb_lib/pdb1c5y.ent
$pdb_lib/pdb1c75.ent
$pdb_lib/pdb1c7k.ent
$pdb_lib/pdb1c7s.ent
$pdb_lib/pdb1c8c.ent
$pdb_lib/pdb1c9o.ent
$pdb_lib/pdb1cc8.ent
$pdb_lib/pdb1ccw.ent
$pdb_lib/pdb1cem.ent
$pdb_lib/pdb1cex.ent
$pdb_lib/pdb1cg5.ent
$pdb_lib/pdb1chd.ent
$pdb_lib/pdb1cjc.ent
$pdb_lib/pdb1cjw.ent
$pdb_lib/pdb1cmb.ent
$pdb_lib/pdb1cnv.ent
$pdb_lib/pdb1cnz.ent
$pdb_lib/pdb1cru.ent
$pdb_lib/pdb1cs1.ent
$pdb_lib/pdb1cse.ent
$pdb_lib/pdb1csh.ent
$pdb_lib/pdb1ctf.ent
$pdb_lib/pdb1ctj.ent
$pdb_lib/pdb1ctq.ent
$pdb_lib/pdb1cuo.ent
$pdb_lib/pdb1cv8.ent
$pdb_lib/pdb1cxp.ent
$pdb_lib/pdb1cxq.ent
$pdb_lib/pdb1cy5.ent
$pdb_lib/pdb1cyo.ent
$pdb_lib/pdb1czf.ent
$pdb_lib/pdb1czp.ent
$pdb_lib/pdb1d02.ent
$pdb_lib/pdb1d0d.ent
$pdb_lib/pdb1d0i.ent
$pdb_lib/pdb1d1q.ent
$pdb_lib/pdb1d3g.ent
$pdb_lib/pdb1d3v.ent
$pdb_lib/pdb1d4o.ent
$pdb_lib/pdb1d4x.ent
$pdb_lib/pdb1d5t.ent
$pdb_lib/pdb1d8w.ent
$pdb_lib/pdb1dbf.ent
$pdb_lib/pdb1dbg.ent
$pdb_lib/pdb1dbx.ent
$pdb_lib/pdb1dc1.ent
$pdb_lib/pdb1dci.ent
$pdb_lib/pdb1dcs.ent
$pdb_lib/pdb1deo.ent
$pdb_lib/pdb1df4.ent
$pdb_lib/pdb1dfm.ent
$pdb_lib/pdb1dg6.ent
$pdb_lib/pdb1dgw.ent
$pdb_lib/pdb1dhn.ent
$pdb_lib/pdb1di6.ent
$pdb_lib/pdb1din.ent
$pdb_lib/pdb1dj0.ent
$pdb_lib/pdb1dk0.ent
$pdb_lib/pdb1dk8.ent
$pdb_lib/pdb1dl5.ent
$pdb_lib/pdb1dlf.ent
$pdb_lib/pdb1dlw.ent
$pdb_lib/pdb1dmh.ent
$pdb_lib/pdb1dos.ent
$pdb_lib/pdb1dow.ent
$pdb_lib/pdb1doz.ent
$pdb_lib/pdb1dp0.ent
$pdb_lib/pdb1dp7.ent
$pdb_lib/pdb1dpj.ent
$pdb_lib/pdb1dps.ent
$pdb_lib/pdb1dqg.ent
$pdb_lib/pdb1dqs.ent
$pdb_lib/pdb1dqz.ent
$pdb_lib/pdb1ds1.ent
$pdb_lib/pdb1dsz.ent
$pdb_lib/pdb1dtd.ent
$pdb_lib/pdb1dus.ent
$pdb_lib/pdb1duv.ent
$pdb_lib/pdb1dvj.ent
$pdb_lib/pdb1dwk.ent
$pdb_lib/pdb1dxe.ent
$pdb_lib/pdb1dxg.ent
$pdb_lib/pdb1dy5.ent
$pdb_lib/pdb1dym.ent
$pdb_lib/pdb1dyp.ent
$pdb_lib/pdb1dys.ent
$pdb_lib/pdb1dzo.ent
$pdb_lib/pdb1e19.ent
$pdb_lib/pdb1e1a.ent
$pdb_lib/pdb1e29.ent
$pdb_lib/pdb1e2g.ent
$pdb_lib/pdb1e2u.ent
$pdb_lib/pdb1e30.ent
$pdb_lib/pdb1e39.ent
$pdb_lib/pdb1e3a.ent
$pdb_lib/pdb1e3d.ent
$pdb_lib/pdb1e3u.ent
$pdb_lib/pdb1e4c.ent
$pdb_lib/pdb1e4m.ent
$pdb_lib/pdb1e58.ent
$pdb_lib/pdb1e5k.ent
$pdb_lib/pdb1e5m.ent
$pdb_lib/pdb1e5p.ent
$pdb_lib/pdb1e6b.ent
$pdb_lib/pdb1e6c.ent
$pdb_lib/pdb1e6u.ent
$pdb_lib/pdb1e7l.ent
$pdb_lib/pdb1e85.ent
$pdb_lib/pdb1e9g.ent
$pdb_lib/pdb1eaj.ent
$pdb_lib/pdb1ecs.ent
$pdb_lib/pdb1ed8.ent
$pdb_lib/pdb1edg.ent
$pdb_lib/pdb1edm.ent
$pdb_lib/pdb1edq.ent
$pdb_lib/pdb1eex.ent
$pdb_lib/pdb1eg9.ent
$pdb_lib/pdb1egu.ent
$pdb_lib/pdb1ej0.ent
$pdb_lib/pdb1ej8.ent
$pdb_lib/pdb1ejg.ent
$pdb_lib/pdb1el4.ent
$pdb_lib/pdb1el5.ent
$pdb_lib/pdb1elk.ent
$pdb_lib/pdb1elw.ent
$pdb_lib/pdb1en2.ent
$pdb_lib/pdb1eok.ent
$pdb_lib/pdb1ep0.ent
$pdb_lib/pdb1epx.ent
$pdb_lib/pdb1eqo.ent
$pdb_lib/pdb1erx.ent
$pdb_lib/pdb1erz.ent
$pdb_lib/pdb1es9.ent
$pdb_lib/pdb1esi.ent
$pdb_lib/pdb1et1.ent
$pdb_lib/pdb1eu1.ent
$pdb_lib/pdb1euv.ent
$pdb_lib/pdb1euw.ent
$pdb_lib/pdb1evy.ent
$pdb_lib/pdb1ew4.ent
$pdb_lib/pdb1ew6.ent
$pdb_lib/pdb1ewf.ent
$pdb_lib/pdb1eye.ent
$pdb_lib/pdb1eyv.ent
$pdb_lib/pdb1eyz.ent
$pdb_lib/pdb1ezg.ent
$pdb_lib/pdb1ezm.ent
$pdb_lib/pdb1ezw.ent
$pdb_lib/pdb1f0i.ent
$pdb_lib/pdb1f0l.ent
$pdb_lib/pdb1f24.ent
$pdb_lib/pdb1f2t.ent
$pdb_lib/pdb1f46.ent
$pdb_lib/pdb1f4p.ent
$pdb_lib/pdb1f5f.ent
$pdb_lib/pdb1f60.ent
$pdb_lib/pdb1f74.ent
$pdb_lib/pdb1f7l.ent
$pdb_lib/pdb1f86.ent
$pdb_lib/pdb1f8e.ent
$pdb_lib/pdb1f94.ent
$pdb_lib/pdb1fa8.ent
$pdb_lib/pdb1faz.ent
$pdb_lib/pdb1fcy.ent
$pdb_lib/pdb1fd3.ent
$pdb_lib/pdb1fe6.ent
$pdb_lib/pdb1fgl.ent
$pdb_lib/pdb1fhu.ent
$pdb_lib/pdb1fi2.ent
$pdb_lib/pdb1fiu.ent
$pdb_lib/pdb1fj2.ent
$pdb_lib/pdb1fjh.ent
$pdb_lib/pdb1fjj.ent
$pdb_lib/pdb1fk5.ent
$pdb_lib/pdb1flm.ent
$pdb_lib/pdb1flt.ent
$pdb_lib/pdb1fm0.ent
$pdb_lib/pdb1fn8.ent
$pdb_lib/pdb1fn9.ent
$pdb_lib/pdb1fna.ent
$pdb_lib/pdb1fnc.ent
$pdb_lib/pdb1fo8.ent
$pdb_lib/pdb1fqt.ent
$pdb_lib/pdb1fr3.ent
$pdb_lib/pdb1fs7.ent
$pdb_lib/pdb1fsg.ent
$pdb_lib/pdb1ft5.ent
$pdb_lib/pdb1ftr.ent
$pdb_lib/pdb1fvg.ent
$pdb_lib/pdb1fvk.ent
$pdb_lib/pdb1fw9.ent
$pdb_lib/pdb1fx2.ent
$pdb_lib/pdb1fxm.ent
$pdb_lib/pdb1fxo.ent
$pdb_lib/pdb1fye.ent
$pdb_lib/pdb1fzo.ent
$pdb_lib/pdb1g2b.ent
$pdb_lib/pdb1g2y.ent
$pdb_lib/pdb1g3p.ent
$pdb_lib/pdb1g4i.ent
$pdb_lib/pdb1g57.ent
$pdb_lib/pdb1g5t.ent
$pdb_lib/pdb1g61.ent
$pdb_lib/pdb1g66.ent
$pdb_lib/pdb1g6s.ent
$pdb_lib/pdb1g6u.ent
$pdb_lib/pdb1g6x.ent
$pdb_lib/pdb1g7a.ent
$pdb_lib/pdb1g7f.ent
$pdb_lib/pdb1g8k.ent
$pdb_lib/pdb1g8m.ent
$pdb_lib/pdb1g8q.ent
$pdb_lib/pdb1g9o.ent
$pdb_lib/pdb1ga6.ent
$pdb_lib/pdb1gai.ent
$pdb_lib/pdb1gbg.ent
$pdb_lib/pdb1gci.ent
$pdb_lib/pdb1gd0.ent
$pdb_lib/pdb1gd1.ent
$pdb_lib/pdb1gdo.ent
$pdb_lib/pdb1gof.ent
$pdb_lib/pdb1gpe.ent
$pdb_lib/pdb1guq.ent
$pdb_lib/pdb1h4x.ent
$pdb_lib/pdb1h61.ent
$pdb_lib/pdb1h70.ent
$pdb_lib/pdb1h75.ent
$pdb_lib/pdb1h8d.ent
$pdb_lib/pdb1h96.ent
$pdb_lib/pdb1h97.ent
$pdb_lib/pdb1ha1.ent
$pdb_lib/pdb1hbn.ent
$pdb_lib/pdb1hbz.ent
$pdb_lib/pdb1hdo.ent
$pdb_lib/pdb1het.ent
$pdb_lib/pdb1hfe.ent
$pdb_lib/pdb1hg7.ent
$pdb_lib/pdb1hh8.ent
$pdb_lib/pdb1hnj.ent
$pdb_lib/pdb1hq1.ent
$pdb_lib/pdb1htr.ent
$pdb_lib/pdb1hvb.ent
$pdb_lib/pdb1hw1.ent
$pdb_lib/pdb1hx0.ent
$pdb_lib/pdb1hx6.ent
$pdb_lib/pdb1hxi.ent
$pdb_lib/pdb1hxn.ent
$pdb_lib/pdb1hyo.ent
$pdb_lib/pdb1hyp.ent
$pdb_lib/pdb1hz6.ent
$pdb_lib/pdb1hzt.ent
$pdb_lib/pdb1hzy.ent
$pdb_lib/pdb1i0h.ent
$pdb_lib/pdb1i0s.ent
$pdb_lib/pdb1i0v.ent
$pdb_lib/pdb1i19.ent
$pdb_lib/pdb1i27.ent
$pdb_lib/pdb1i2t.ent
$pdb_lib/pdb1i45.ent
$pdb_lib/pdb1i4f.ent
$pdb_lib/pdb1i5g.ent
$pdb_lib/pdb1i6w.ent
$pdb_lib/pdb1i71.ent
$pdb_lib/pdb1i76.ent
$pdb_lib/pdb1i8o.ent
$pdb_lib/pdb1i9s.ent
$pdb_lib/pdb1i9z.ent
$pdb_lib/pdb1iab.ent
$pdb_lib/pdb1iat.ent
$pdb_lib/pdb1iby.ent
$pdb_lib/pdb1ida.ent
$pdb_lib/pdb1ifc.ent
$pdb_lib/pdb1iho.ent
$pdb_lib/pdb1ihr.ent
$pdb_lib/pdb1iib.ent
$pdb_lib/pdb1isu.ent
$pdb_lib/pdb1ixh.ent
$pdb_lib/pdb1j6z.ent
$pdb_lib/pdb1j79.ent
$pdb_lib/pdb1j97.ent
$pdb_lib/pdb1j98.ent
$pdb_lib/pdb1j9q.ent
$pdb_lib/pdb1jb3.ent
$pdb_lib/pdb1jbe.ent
$pdb_lib/pdb1jd0.ent
$pdb_lib/pdb1jer.ent
$pdb_lib/pdb1jhg.ent
$pdb_lib/pdb1jiw.ent
$pdb_lib/pdb1jix.ent
$pdb_lib/pdb1jjt.ent
$pdb_lib/pdb1jp3.ent
$pdb_lib/pdb1jp4.ent
$pdb_lib/pdb1jqc.ent
$pdb_lib/pdb1kap.ent
$pdb_lib/pdb1kid.ent
$pdb_lib/pdb1koe.ent
$pdb_lib/pdb1kp6.ent
$pdb_lib/pdb1kpe.ent
$pdb_lib/pdb1kpt.ent
$pdb_lib/pdb1kve.ent
$pdb_lib/pdb1lam.ent
$pdb_lib/pdb1lbu.ent
$pdb_lib/pdb1lcl.ent
$pdb_lib/pdb1ldg.ent
$pdb_lib/pdb1lkk.ent
$pdb_lib/pdb1lmb.ent
$pdb_lib/pdb1lst.ent
$pdb_lib/pdb1luc.ent
$pdb_lib/pdb1mfm.ent
$pdb_lib/pdb1mgt.ent
$pdb_lib/pdb1mla.ent
$pdb_lib/pdb1mml.ent
$pdb_lib/pdb1mof.ent
$pdb_lib/pdb1mol.ent
$pdb_lib/pdb1moq.ent
$pdb_lib/pdb1mpg.ent
$pdb_lib/pdb1mrj.ent
$pdb_lib/pdb1mrp.ent
$pdb_lib/pdb1msk.ent
$pdb_lib/pdb1mty.ent
$pdb_lib/pdb1mug.ent
$pdb_lib/pdb1mun.ent
$pdb_lib/pdb1nbc.ent
$pdb_lib/pdb1nkd.ent
$pdb_lib/pdb1nls.ent
$pdb_lib/pdb1nox.ent
$pdb_lib/pdb1npk.ent
$pdb_lib/pdb1nps.ent
$pdb_lib/pdb1nul.ent
$pdb_lib/pdb1oaa.ent
$pdb_lib/pdb1one.ent
$pdb_lib/pdb1opd.ent
$pdb_lib/pdb1orc.ent
$pdb_lib/pdb1pa2.ent
$pdb_lib/pdb1pcf.ent
$pdb_lib/pdb1pda.ent
$pdb_lib/pdb1pdo.ent
$pdb_lib/pdb1pgs.ent
$pdb_lib/pdb1pmi.ent
$pdb_lib/pdb1pot.ent
$pdb_lib/pdb1ppn.ent
$pdb_lib/pdb1psr.ent
$pdb_lib/pdb1pym.ent
$pdb_lib/pdb1qb7.ent
$pdb_lib/pdb1qcx.ent
$pdb_lib/pdb1qcz.ent
$pdb_lib/pdb1qd1.ent
$pdb_lib/pdb1qdd.ent
$pdb_lib/pdb1qf8.ent
$pdb_lib/pdb1qfm.ent
$pdb_lib/pdb1qft.ent
$pdb_lib/pdb1qg8.ent
$pdb_lib/pdb1qgi.ent
$pdb_lib/pdb1qgw.ent
$pdb_lib/pdb1qgx.ent
$pdb_lib/pdb1qh4.ent
$pdb_lib/pdb1qh8.ent
$pdb_lib/pdb1qhv.ent
$pdb_lib/pdb1qj4.ent
$pdb_lib/pdb1qj5.ent
$pdb_lib/pdb1qjp.ent
$pdb_lib/pdb1qkr.ent
$pdb_lib/pdb1qks.ent
$pdb_lib/pdb1ql0.ent
$pdb_lib/pdb1qlw.ent
$pdb_lib/pdb1qmg.ent
$pdb_lib/pdb1qmv.ent
$pdb_lib/pdb1qna.ent
$pdb_lib/pdb1qnf.ent
$pdb_lib/pdb1qnr.ent
$pdb_lib/pdb1qop.ent
$pdb_lib/pdb1qow.ent
$pdb_lib/pdb1qpc.ent
$pdb_lib/pdb1qq7.ent
$pdb_lib/pdb1qq9.ent
$pdb_lib/pdb1qqf.ent
$pdb_lib/pdb1qre.ent
$pdb_lib/pdb1qrr.ent
$pdb_lib/pdb1qsa.ent
$pdb_lib/pdb1qsu.ent
$pdb_lib/pdb1qtn.ent
$pdb_lib/pdb1qto.ent
$pdb_lib/pdb1qts.ent
$pdb_lib/pdb1qtw.ent
$pdb_lib/pdb1qu9.ent
$pdb_lib/pdb1qus.ent
$pdb_lib/pdb1ra9.ent
$pdb_lib/pdb1rb9.ent
$pdb_lib/pdb1rge.ent
$pdb_lib/pdb1rhs.ent
$pdb_lib/pdb1rie.ent
$pdb_lib/pdb1sbp.ent
$pdb_lib/pdb1sbw.ent
$pdb_lib/pdb1sgp.ent
$pdb_lib/pdb1slu.ent
$pdb_lib/pdb1sml.ent
$pdb_lib/pdb1svf.ent
$pdb_lib/pdb1svy.ent
$pdb_lib/pdb1swu.ent
$pdb_lib/pdb1tag.ent
$pdb_lib/pdb1tca.ent
$pdb_lib/pdb1thf.ent
$pdb_lib/pdb1thg.ent
$pdb_lib/pdb1thv.ent
$pdb_lib/pdb1tif.ent
$pdb_lib/pdb1toa.ent
$pdb_lib/pdb1tvx.ent
$pdb_lib/pdb1tx4.ent
$pdb_lib/pdb1tyv.ent
$pdb_lib/pdb1ubi.ent
$pdb_lib/pdb1unk.ent
$pdb_lib/pdb1uro.ent
$pdb_lib/pdb1ush.ent
$pdb_lib/pdb1vfy.ent
$pdb_lib/pdb1vhh.ent
$pdb_lib/pdb1vie.ent
$pdb_lib/pdb1vjs.ent
$pdb_lib/pdb1vns.ent
EOD
#
echo "done with mapping"
echo " updating..."
solve_resolve.side_chains_update<<EOD>side_chains_update.log
-1,2,     ! -1=update ; 0=rna 1=dna 2=prot
EOD
#
cp -p rho_map_new.dat side_chains_rho.dat
cp -p side_chains_best.dat  side_chains_rho.dat segments/

#
rm -rf side_chains_best.dat rho_map_new.dat side_chains_rho.dat
echo "Results are in `cd segments; pwd ; cd ..`"
