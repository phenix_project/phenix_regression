#!/bin/csh -ef
#$ -cwd
setenv SOLVETMPDIR /var/tmp
#
solve_resolve.side_chains<<EOD 
1,2,     !1=get clusters; 0=rna 1=dna 2=prot
0.8,1,0.0    ! max rmsd for a group, min in group, rmsd_ok
0.10    !   fraction allowed to be in misc group
1
nsf-d2_reference.pdb
EOD
