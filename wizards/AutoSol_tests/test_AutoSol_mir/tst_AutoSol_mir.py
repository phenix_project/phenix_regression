from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os,sys
  print("Test script for test_AutoSol_mir")
  print("")
  log_file=os.path.join(os.getcwd(),"test_AutoSol_mir.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  local_log_file='test_AutoSol_mir.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autosol  import run_autosol
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''mir.eff''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autosol(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)


  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  # Edited here to skip the log file and make sure it got the expected files

  lines=open(log_file).readlines()
  assert "Acceptable build found\n" in lines
  fom = None
  for line in lines:
    if fom: continue
    if len(line.split()) == 12 and \
       line.startswith("Solution #") and line.find("BAYES-CC")> -1 and \
        line.find("FOM:") >-1:
      fom = float(line.split()[-1])
  assert fom
  assert fom > 0.6
      
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['AutoSol_tests','test_AutoSol_mir']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
