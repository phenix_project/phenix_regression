from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os,sys
  print ("Test script for test_AutoSol_rna")
  print ("")
  log_file=os.path.join(os.getcwd(),"test_AutoSol_rna.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  local_log_file='test_AutoSol_rna.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autosol  import run_autosol
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''w1_rna.sca input_phase_file=perfect_rna.mtz seq_file=rna.seq chain_type=RNA thoroughness=quick n_random_frag=4 mad_ha_n=2 build_type=RESOLVE phasing_method=SOLVE model_building.helices_strands_only=True refine=False refine_b=False ncycle_refine=1 number_of_models=1 add_classic_denmod=False ''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autosol(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print (open(local_log_file).read(), file = log)


  log.close()


  print ()
  print ('Done with test...running checks on output')
  print ()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_AutoSol_rna',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print ()
  print ('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['AutoSol_tests','test_AutoSol_rna']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
