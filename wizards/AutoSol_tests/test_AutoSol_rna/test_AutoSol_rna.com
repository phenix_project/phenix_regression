#!/bin/csh -f
phenix.autosol w1_rna.sca input_phase_file=perfect_rna.mtz seq_file=rna.seq chain_type=RNA thoroughness=quick  n_random_frag=4  mad_ha_n=2  build_type=RESOLVE phasing_method=PHASER model_building.helices_strands_only=True refine=False
