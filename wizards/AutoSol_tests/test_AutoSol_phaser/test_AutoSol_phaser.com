#!/bin/csh -f
phenix.autosol w1.sca seq_file=seq.dat quick=True ncs_copies=1 phasing_method=PHASER atom_type=Au build_type=RESOLVE model_building.helices_strands_only=False f_prime=-8. f_double_prime=10. refine=False
