#!/bin/csh -f
setenv LIBTBX_FULL_TESTING
phenix.autosol w1.sca seq_file=seq.dat quick=True ncs_copies=1 phasing_method=SOLVE build_type=RESOLVE refine=False

foreach f (overall_best.pdb overall_best_denmod_map_coeffs.mtz )
  if (! -f AutoSol_run_1_/$f) then
    echo "FAILED TO PRODUCE AutoSol_run_1_/$f"
    echo "Deleting  PDS so the test will now fail"
    rm -f PDS
    exit 1
  endif
end
