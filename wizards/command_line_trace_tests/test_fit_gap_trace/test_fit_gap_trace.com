#!/bin/csh -ef
#$ -cwd
phenix.autobuild data=random_0.7_mlt.mtz seq_file=seq_mlt.dat model=mlt_gap.pdb ncycle_refine=0 rebuild_in_place=False resolution=3.0 number_of_models=0 build_outside=False n_cycle_build=1 n_cycle_rebuild_max=1 input_map_file=mlt.mtz skip_combine_extend=True refine=false number_of_parallel_models=1 trace_loops=true standard_loops=false loop_lib=false loop_cc_min=0.3
if ( $status )then
  echo "FAILED"
  exit 1
endif
