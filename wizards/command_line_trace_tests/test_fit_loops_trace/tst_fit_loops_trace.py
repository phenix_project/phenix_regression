from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print ("Test script for test_fit_loops_trace")
  print ("")
  log_file=os.path.join(os.getcwd(),"test_fit_loops_trace.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  from phenix.programs  import fit_loops as run

  from iotbx.cli_parser import run_program
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''pdb_in=nsf_gap.pdb mtz_in=resolve_0.mtz seq_file=nsf.seq start=37 end=43 chain_id=None debug=True trace_loops=True standard_loops=False loop_backup_residues=1''')
  run_program(program_class=run.Program,args=args,logger=log)

  log.close()


  print ()
  print ('Done with test...running checks on output')
  print ()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_fit_loops_trace',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print ()
  print ('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['command_line_trace_tests','test_fit_loops_trace']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
