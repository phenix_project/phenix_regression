#!/bin/csh -ef
#$ -cwd
phenix.multi_crystal_average multi_I_AU_remove.eff write_ncs_domain_pdb=true
echo "HA 1:  "
cat temp_dir/TEMP_HA_1.pdb
echo "HA 2:  "
cat temp_dir/TEMP_HA_2.pdb
echo "CC HA 1 crystal_2 pattern: "
phenix.get_cc_mtz_pdb temp_dir/cycle_1_xl_2_dump_pattern_phase.mtz temp_dir/TEMP_HA_1.pdb
echo "CC HA 2 crystal_2 pattern: "
phenix.get_cc_mtz_pdb temp_dir/cycle_1_xl_2_dump_pattern_phase.mtz temp_dir/TEMP_HA_2.pdb
echo "CC HA 1 crystal_1 denmod: "
phenix.get_cc_mtz_pdb temp_dir/denmod_cycle_1_xl_1.mtz temp_dir/TEMP_HA_1.pdb
echo "CC HA 1 crystal_2 denmod: "
phenix.get_cc_mtz_pdb temp_dir/denmod_cycle_1_xl_2.mtz temp_dir/TEMP_HA_1.pdb
echo "CC HA 2 crystal_1 denmod: "
phenix.get_cc_mtz_pdb temp_dir/denmod_cycle_1_xl_1.mtz temp_dir/TEMP_HA_2.pdb
echo "CC HA 2 crystal_2 denmod: "
phenix.get_cc_mtz_pdb temp_dir/denmod_cycle_1_xl_2.mtz temp_dir/TEMP_HA_2.pdb

