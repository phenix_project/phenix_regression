from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_multi_ha")
  print("")
  log_file=os.path.join(os.getcwd(),"test_multi_ha.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  from phenix.command_line.multi_crystal_average  import multi_crystal_average
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''multi_I_AU_remove.eff averaging.write_ncs_domain_pdb=true''')
  multi_crystal_average(args=args,out=log)
  print(open('temp_dir/TEMP_HA_1.pdb').read(), file=log)
  print(open('temp_dir/TEMP_HA_2.pdb').read(), file=log)


  from phenix.command_line.get_cc_mtz_pdb  import get_cc_mtz_pdb
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''temp_dir/cycle_1_xl_2_dump_pattern_phase.mtz temp_dir/TEMP_HA_1.pdb''')
  get_cc_mtz_pdb(args=args,out=log)


  from phenix.command_line.get_cc_mtz_pdb  import get_cc_mtz_pdb
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''temp_dir/cycle_1_xl_2_dump_pattern_phase.mtz temp_dir/TEMP_HA_2.pdb''')
  get_cc_mtz_pdb(args=args,out=log)


  from phenix.command_line.get_cc_mtz_pdb  import get_cc_mtz_pdb
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''temp_dir/denmod_cycle_1_xl_1.mtz temp_dir/TEMP_HA_1.pdb''')
  get_cc_mtz_pdb(args=args,out=log)


  from phenix.command_line.get_cc_mtz_pdb  import get_cc_mtz_pdb
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''temp_dir/denmod_cycle_1_xl_2.mtz temp_dir/TEMP_HA_1.pdb''')
  get_cc_mtz_pdb(args=args,out=log)


  from phenix.command_line.get_cc_mtz_pdb  import get_cc_mtz_pdb
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''temp_dir/denmod_cycle_1_xl_1.mtz temp_dir/TEMP_HA_2.pdb''')
  get_cc_mtz_pdb(args=args,out=log)


  from phenix.command_line.get_cc_mtz_pdb  import get_cc_mtz_pdb
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''temp_dir/denmod_cycle_1_xl_2.mtz temp_dir/TEMP_HA_2.pdb''')
  get_cc_mtz_pdb(args=args,out=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_multi_ha',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['command_line_ncs_tests','test_multi_ha']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
