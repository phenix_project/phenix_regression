Parameters taken from: multi_I_AU_remove.eff


#                       multi
#
# Apply multi-crystal averaging with statistical density
# modification

# Type phenix.doc for help
Values of all params:
multi {
  job_title = None
  crystal {
    ncs_domain_file = None
    pdb_file = "gene-5.pdb"
    map_coeffs = "gene-5_I.pdb_f.mtz"
    map_coeffs_labin = None
    datafile = "gene-5_I.pdb_f.mtz"
    datafile_labin = "FP=FP SIGFP=SIGFP"
    solvent_content = 0.43
    ha_file = "gene-5_ha.pdb"
    perfect_map_coeffs = "gene-5_I.pdb.mtz"
    perfect_map_coeffs_labin = None
    update_map = True
  }
  crystal {
    ncs_domain_file = None
    pdb_file = "multi.pdb"
    map_coeffs = None
    map_coeffs_labin = None
    datafile = "multi.pdb_AU.pdb_f.mtz"
    datafile_labin = "FP=FP SIGFP=SIGFP"
    solvent_content = 0.5
    ha_file = "multi_ha.pdb"
    perfect_map_coeffs = "multi.pdb_AU.pdb.mtz"
    perfect_map_coeffs_labin = None
    update_map = True
  }
  averaging {
    cycles = 1
    resolution = None
    fill = True
    res_fill = None
    temp_dir = "temp_dir"
    output_dir = ""
    write_target_map_and_sigma = False
    use_model_mask = False
    sharpen = False
    equal_ncs_weight = False
    weight_ncs = None
    write_ncs_domain_pdb = True
    mask_cycles = 1
    map_phasing_ab = False
    use_FP_PHIM = False
    no_recombination = False
  }
  aniso {
    remove_aniso = True
    b_iso = None
    max_b_iso = 40
    target_b_ratio = 10
  }
  control {
    verbose = True
    debug = False
    raise_sorry = False
    n_xyz = None
    coarse_grid = False
    resolve_size = "12"
    resolve_command_list = None
    dry_run = False
    base_gui_dir = None
  }
}
simple_ncs_from_pdb {
  pdb_in = None
  temp_dir = ""
  ncs_domain_pdb_stem = None
  write_ncs_domain_pdb = False
  write_ncs_phil = False
  show_summary = False
  write_spec_files = False
  ncs_search {
    enabled = False
    exclude_selection = element H or element D or water
    chain_similarity_threshold = 0.85
    chain_max_rmsd = 2
    residue_match_radius = 4
    try_shortcuts = False
    minimum_number_of_atoms_in_copy = 3
    validate_user_supplied_groups = True
  }
  ncs_group {
    reference = None
    selection = None
  }
}

datafile files:
gene-5_I.pdb_f.mtz  LABIN:  FP=FP SIGFP=SIGFP
...copied to:  temp_dir/gene-5_I.pdb_f_data_1_ed.mtz  LABIN:  FP=FP SIGFP=SIGFP

Removing anisotropy from gene-5_I.pdb_f_data_1_ed.mtz to yield gene-5_I.pdb_f_data_1_ed_aniso.mtz


#                       remove_aniso
#
# Correct columns in MTZ file for anisotropy


# Type phenix.doc for help
Values of all params:
remove_aniso {
  input_files {
    mtz_in = "temp_dir/gene-5_I.pdb_f_data_1_ed.mtz"
    obs_labels = None
  }
  output_files {
    mtz_out = "temp_dir/gene-5_I.pdb_f_data_1_ed_aniso.mtz"
    log = "remove_aniso.log"
    extension = "_aniso"
    params_out = "remove_aniso_params.eff"
  }
  directories {
    temp_dir = "temp_dir"
    output_dir = ""
  }
  crystal_info {
    resolution = None
  }
  aniso {
    b_iso = 25.0001591
  }
  control {
    verbose = False
    raise_sorry = False
    debug = False
    dry_run = False
    resolve_command_list = None
  }
}

Correction for anisotropy

Arrays in input file temp_dir/gene-5_I.pdb_f_data_1_ed.mtz:
FILE  dummy  LABELS:  ['ALL']
['FP', 'SIGFP'] ['PHIM'] ['FOMM'] ['HLAM', 'HLBM', 'HLCM', 'HLDM'] ['FWT', 'PHWT']
Columns for anisotropy correction:  FP,SIGFP

Trace of aniso B:   31.20    33.33    27.54 A**2.
Minimum:   27.54    Mean:   30.69

Applying aniso correction now with target B set with b_iso=  25.00
Offset in B will be:    -5.69 (negative is sharpening)


Citations for remove_aniso:

Liebschner D, Afonine PV, Baker ML, Bunkóczi G, Chen VB, Croll TI, Hintze B,
Hung LW, Jain S, McCoy AJ, Moriarty NW, Oeffner RD, Poon BK, Prisant MG, Read
RJ, Richardson JS, Richardson DC, Sammito MD, Sobolev OV, Stockwell DH,
Terwilliger TC, Urzhumtsev AG, Videau LL, Williams CJ, Adams PD. (2019)
Macromolecular structure determination using X-rays, neutrons and electrons:
recent developments in Phenix. Acta Cryst. D75:861-877.

multi.pdb_AU.pdb_f.mtz  LABIN:  FP=FP SIGFP=SIGFP
...copied to:  temp_dir/multi.pdb_AU.pdb_f_data_2_ed.mtz  LABIN:  FP=FP SIGFP=SIGFP

Removing anisotropy from multi.pdb_AU.pdb_f_data_2_ed.mtz to yield multi.pdb_AU.pdb_f_data_2_ed_aniso.mtz


#                       remove_aniso
#
# Correct columns in MTZ file for anisotropy


# Type phenix.doc for help
Values of all params:
remove_aniso {
  input_files {
    mtz_in = "temp_dir/multi.pdb_AU.pdb_f_data_2_ed.mtz"
    obs_labels = None
  }
  output_files {
    mtz_out = "temp_dir/multi.pdb_AU.pdb_f_data_2_ed_aniso.mtz"
    log = "remove_aniso.log"
    extension = "_aniso"
    params_out = "remove_aniso_params.eff"
  }
  directories {
    temp_dir = "temp_dir"
    output_dir = ""
  }
  crystal_info {
    resolution = None
  }
  aniso {
    b_iso = 25.0001591
  }
  control {
    verbose = False
    raise_sorry = False
    debug = False
    dry_run = False
    resolve_command_list = None
  }
}

Correction for anisotropy

Arrays in input file temp_dir/multi.pdb_AU.pdb_f_data_2_ed.mtz:
FILE  dummy  LABELS:  ['ALL']
['FP', 'SIGFP'] ['PHIM'] ['FOMM'] ['HLAM', 'HLBM', 'HLCM', 'HLDM'] ['FWT', 'PHWT']
Columns for anisotropy correction:  FP,SIGFP

Trace of aniso B:   29.08    28.62    32.27 A**2.
Minimum:   28.62    Mean:   29.99

Applying aniso correction now with target B set with b_iso=  25.00
Offset in B will be:    -4.99 (negative is sharpening)


Citations for remove_aniso:

Liebschner D, Afonine PV, Baker ML, Bunkóczi G, Chen VB, Croll TI, Hintze B,
Hung LW, Jain S, McCoy AJ, Moriarty NW, Oeffner RD, Poon BK, Prisant MG, Read
RJ, Richardson JS, Richardson DC, Sammito MD, Sobolev OV, Stockwell DH,
Terwilliger TC, Urzhumtsev AG, Videau LL, Williams CJ, Adams PD. (2019)
Macromolecular structure determination using X-rays, neutrons and electrons:
recent developments in Phenix. Acta Cryst. D75:861-877.


High resolution for datafiles:    2.50 A

map_coeffs files:
gene-5_I.pdb_f.mtz  LABIN:  FP=FWT PHIB=PHWT
...copied to:  temp_dir/gene-5_I.pdb_f_map_coeffs_1_ed.mtz  LABIN:  FP=FP PHIB=PHIM

High resolution for map coeffs:    2.50 A

perfect_map_coeffs files:
gene-5_I.pdb.mtz  LABIN:  FP=FMODEL PHIB=PHIFMODEL
...copied to:  temp_dir/gene-5_I.pdb_perfect_1_ed.mtz  LABIN:  FP=FP PHIB=PHIM
multi.pdb_AU.pdb.mtz  LABIN:  FP=FMODEL PHIB=PHIFMODEL
...copied to:  temp_dir/multi.pdb_AU.pdb_perfect_2_ed.mtz  LABIN:  FP=FP PHIB=PHIM

High resolution for perfect data:    2.50 A

List of PDB files for NCS ['gene-5.pdb', 'multi.pdb']

Copied list of PDB files for NCS ['temp_dir/gene-5_1.cif', 'temp_dir/multi_2.cif']

Copied list of ncs_domain files [None, None]

Reflections will be filled to resolution of    2.50 A


--------------------------------------------------------
Finding NCS groups for crystal temp_dir/gene-5_1.cif by itself
NCS groups in  temp_dir/gene-5_1.cif : 0
Unique chains from temp_dir/gene-5_1.cif renamed to: ['--']

List of unique chains for crystal temp_dir/gene-5_1.cif:['A']
List of renamed chains:  ['--']

Finding NCS to map each crystal to temp_dir/gene-5_1.cif...


Finding NCS to map temp_dir/gene-5_1.cif on to temp_dir/gene-5_1.cif


GROUP 1
Summary of NCS group with 2 operators:
ID of chain/residue where these apply: [['--', 'A'], [[[1, 86]], [[1, 86]]]]
RMSD (A) from chain --:  0.0  0.0
Number of residues matching chain --:[86, 86]
NCS domains represented by: gene-5_1.cif__group_1.pdb

OPERATOR 1
CENTER:    8.0489    7.2040    7.9938

ROTA 1:    1.0000    0.0000    0.0000
ROTA 2:    0.0000    1.0000    0.0000
ROTA 3:    0.0000    0.0000    1.0000
TRANS:     0.0000    0.0000    0.0000

OPERATOR 2
CENTER:    8.0489    7.2040    7.9938

ROTA 1:    1.0000    0.0000    0.0000
ROTA 2:    0.0000    1.0000    0.0000
ROTA 3:    0.0000    0.0000    1.0000
TRANS:     0.0000    0.0000    0.0000



Finding NCS to map temp_dir/multi_2.cif on to temp_dir/gene-5_1.cif


GROUP 1
Summary of NCS group with 3 operators:
ID of chain/residue where these apply: [['--', 'A', 'B'], [[[1, 86]], [[1, 86]], [[1, 86]]]]
RMSD (A) from chain --:  0.0  0.0  0.0
Number of residues matching chain --:[86, 86, 86]

OPERATOR 1
CENTER:    8.0489    7.2040    7.9938

ROTA 1:    1.0000    0.0000    0.0000
ROTA 2:    0.0000    1.0000    0.0000
ROTA 3:    0.0000    0.0000    1.0000
TRANS:     0.0000    0.0000    0.0000

OPERATOR 2
CENTER:   45.7182   15.6492   13.3967

ROTA 1:    0.2625    0.4428    0.8573
ROTA 2:    0.8666   -0.4989   -0.0076
ROTA 3:    0.4243    0.7450   -0.5147
TRANS:   -22.3654  -24.5076  -16.1694

OPERATOR 3
CENTER:   33.1070   -1.8422    7.7256

ROTA 1:   -0.5633   -0.2836   -0.7761
ROTA 2:    0.7345   -0.6020   -0.3131
ROTA 3:   -0.3784   -0.7465    0.5474
TRANS:    32.1702  -15.8041   14.9178


Number of NCS groups is  1

--------------------------------------------------------
Finding NCS groups for crystal temp_dir/multi_2.cif by itself
NCS groups in  temp_dir/multi_2.cif : 1
Chains in NCS group:  [['A', 'B'], [[[1, 86]], [[1, 86]]]]
Unique chains from temp_dir/multi_2.cif renamed to: ['--']

List of unique chains for crystal temp_dir/multi_2.cif:['A']
List of renamed chains:  ['--']

Finding NCS to map each crystal to temp_dir/multi_2.cif...


Finding NCS to map temp_dir/multi_2.cif on to temp_dir/multi_2.cif


GROUP 1
Summary of NCS group with 3 operators:
ID of chain/residue where these apply: [['--', 'A', 'B'], [[[1, 86]], [[1, 86]], [[1, 86]]]]
RMSD (A) from chain --:  0.0  0.0  0.0
Number of residues matching chain --:[86, 86, 86]
NCS domains represented by: multi_2.cif__group_1.pdb

OPERATOR 1
CENTER:   45.7182   15.6492   13.3967

ROTA 1:    1.0000    0.0000    0.0000
ROTA 2:    0.0000    1.0000    0.0000
ROTA 3:    0.0000    0.0000    1.0000
TRANS:     0.0000    0.0000    0.0000

OPERATOR 2
CENTER:   45.7182   15.6492   13.3967

ROTA 1:    1.0000    0.0000    0.0000
ROTA 2:    0.0000    1.0000    0.0000
ROTA 3:    0.0000    0.0000    1.0000
TRANS:     0.0000    0.0000    0.0000

OPERATOR 3
CENTER:   33.1070   -1.8422    7.7256

ROTA 1:    0.3282   -0.9129   -0.2428
ROTA 2:   -0.8978   -0.3813    0.2203
ROTA 3:   -0.2937    0.1457   -0.9447
TRANS:    35.0475   42.9680   30.6875



Finding NCS to map temp_dir/gene-5_1.cif on to temp_dir/multi_2.cif


GROUP 1
Summary of NCS group with 2 operators:
ID of chain/residue where these apply: [['--', 'A'], [[[1, 86]], [[1, 86]]]]
RMSD (A) from chain --:  0.0  0.0
Number of residues matching chain --:[86, 86]

OPERATOR 1
CENTER:   45.7182   15.6492   13.3967

ROTA 1:    1.0000    0.0000    0.0000
ROTA 2:    0.0000    1.0000    0.0000
ROTA 3:    0.0000    0.0000    1.0000
TRANS:     0.0000    0.0000    0.0000

OPERATOR 2
CENTER:    8.0489    7.2040    7.9938

ROTA 1:    0.2625    0.8666    0.4243
ROTA 2:    0.4428   -0.4989    0.7450
ROTA 3:    0.8573   -0.0076   -0.5147
TRANS:    33.9704    9.7236   10.6654


Number of NCS groups is  1

****** Ready to run multi-crystal NCS density modification****


Starting CC to perfect maps:

Cycle  1
Running density modification on crystal  1 gene-5_I.pdb_f_data_1_ed_aniso.mtz
NOTE: no information available yet for other crystals.
New map for crystal  1 : denmod_cycle_1_xl_1.mtz
Running density modification on crystal  2 multi.pdb_AU.pdb_f_data_2_ed_aniso.mtz
New map for crystal  2 : denmod_cycle_1_xl_2.mtz

Current CC values to perfect maps:
Map denmod_cycle_1_xl_1.mtz   CC= 0.96
Map denmod_cycle_1_xl_2.mtz   CC= 0.91
Current best map coeffs for gene-5_I.pdb_f_data_1_ed_aniso.mtz are in denmod_cycle_1_xl_1.mtz
Current best map coeffs for multi.pdb_AU.pdb_f_data_2_ed_aniso.mtz are in denmod_cycle_1_xl_2.mtz
CRYST1    1.000    1.000    1.000  90.00  90.00  90.00 P 1
HETATM    1  I     I A  13      19.414   5.675  14.737  1.00 24.12           I
HETATM    1 AU    AU A  26      14.591  -8.480   8.951  1.00 23.21          AU
HETATM    1 AU    AU B  26      14.591  -8.481   8.951  1.00 23.21          AU

CRYST1    1.000    1.000    1.000  90.00  90.00  90.00 P 1
HETATM    1 AU    AU A  26      34.249  27.084  18.632  1.00 23.21          AU
HETATM    1 AU    AU B  26      34.249  27.084  18.632  1.00 23.21          AU
HETATM    1  I     I A  13      50.237  26.469  19.681  1.00 24.12           I



#                       get_cc_mtz_pdb
#
# Get correlation between atoms in a PDB file and map
# offsetting the PDB file by allowed origin shifts

# Type phenix.doc for help
Values of all params:
get_cc_mtz_pdb {
  pdb_in = "temp_dir/TEMP_HA_1.pdb"
  output_files {
    target_output_format = None *pdb mmcif
  }
  atom_selection = None
  mtz_in = "temp_dir/cycle_1_xl_2_dump_pattern_phase.mtz"
  map_in = None
  labin = ""
  offset_pdb_suffix = "offset"
  resolution = None
  use_only_refl_present_in_mtz = False
  scale = False
  split_conformers = False
  use_cc_mask = None
  fix_xyz = False
  fix_rad_max = False
  rad_max = None
  any_offset = False
  chain_type = *PROTEIN DNA RNA
  temp_dir = "temp_dir"
  output_dir = ""
  gui_output_dir = None
  verbose = True
  quick = False
  raise_sorry = False
  debug = False
  dry_run = False
  resolve_command_list = None
  job_title = None
}
Get_cc_mtz_pdb: correlation of map and model allowing origin offsets
Copied as PDB/mmcif temp_dir/TEMP_TEMP_HA_1.cif
Map from:  TEMP_cycle_1_xl_2_dump_pattern_phase.mtz  using labin  FP=FWT PHIB=PHWT
Model from:  TEMP_TEMP_HA_1.cif
LABIN LINE:  FP=FWT PHIB=PHWT
Offsetting  TEMP_TEMP_HA_1.cif  to match  TEMP_cycle_1_xl_2_dump_pattern_phase.mtz
Getting FC from self.pdb...
FC is in  temp_dir/resolve.mtz
Offset pdb file is in  HA_1_offset.cif
Getting CC of  TEMP_TEMP_HA_1.cif  (offset to  HA_1_offset.cif ) to match  TEMP_cycle_1_xl_2_dump_pattern_phase.mtz
Detailed analysis of correlation of map and model are in:  cc.log
Offset PDB file is in HA_1_offset.cif

Correlation in region of model:  0.333 ...overall:  0.023
Translation applied: (0.00, 0.00, 0.00) A
overall CC:  0.023
local CC:  0.333


#                       get_cc_mtz_pdb
#
# Get correlation between atoms in a PDB file and map
# offsetting the PDB file by allowed origin shifts

# Type phenix.doc for help
Values of all params:
get_cc_mtz_pdb {
  pdb_in = "temp_dir/TEMP_HA_2.pdb"
  output_files {
    target_output_format = None *pdb mmcif
  }
  atom_selection = None
  mtz_in = "temp_dir/cycle_1_xl_2_dump_pattern_phase.mtz"
  map_in = None
  labin = ""
  offset_pdb_suffix = "offset"
  resolution = None
  use_only_refl_present_in_mtz = False
  scale = False
  split_conformers = False
  use_cc_mask = None
  fix_xyz = False
  fix_rad_max = False
  rad_max = None
  any_offset = False
  chain_type = *PROTEIN DNA RNA
  temp_dir = "temp_dir"
  output_dir = ""
  gui_output_dir = None
  verbose = True
  quick = False
  raise_sorry = False
  debug = False
  dry_run = False
  resolve_command_list = None
  job_title = None
}
Get_cc_mtz_pdb: correlation of map and model allowing origin offsets
Copied as PDB/mmcif temp_dir/TEMP_TEMP_HA_2.cif
Map from:  TEMP_cycle_1_xl_2_dump_pattern_phase.mtz  using labin  FP=FWT PHIB=PHWT
Model from:  TEMP_TEMP_HA_2.cif
LABIN LINE:  FP=FWT PHIB=PHWT
Offsetting  TEMP_TEMP_HA_2.cif  to match  TEMP_cycle_1_xl_2_dump_pattern_phase.mtz
Getting FC from self.pdb...
FC is in  temp_dir/resolve.mtz
Offset pdb file is in  HA_2_offset.cif
Getting CC of  TEMP_TEMP_HA_2.cif  (offset to  HA_2_offset.cif ) to match  TEMP_cycle_1_xl_2_dump_pattern_phase.mtz
Detailed analysis of correlation of map and model are in:  cc.log
Offset PDB file is in HA_2_offset.cif

Correlation in region of model:  0.094 ...overall:  0.003
Translation applied: (0.00, 0.00, 0.00) A
overall CC:  0.003
local CC:  0.094


#                       get_cc_mtz_pdb
#
# Get correlation between atoms in a PDB file and map
# offsetting the PDB file by allowed origin shifts

# Type phenix.doc for help
Values of all params:
get_cc_mtz_pdb {
  pdb_in = "temp_dir/TEMP_HA_1.pdb"
  output_files {
    target_output_format = None *pdb mmcif
  }
  atom_selection = None
  mtz_in = "temp_dir/denmod_cycle_1_xl_1.mtz"
  map_in = None
  labin = ""
  offset_pdb_suffix = "offset"
  resolution = None
  use_only_refl_present_in_mtz = False
  scale = False
  split_conformers = False
  use_cc_mask = None
  fix_xyz = False
  fix_rad_max = False
  rad_max = None
  any_offset = False
  chain_type = *PROTEIN DNA RNA
  temp_dir = "temp_dir"
  output_dir = ""
  gui_output_dir = None
  verbose = True
  quick = False
  raise_sorry = False
  debug = False
  dry_run = False
  resolve_command_list = None
  job_title = None
}
Get_cc_mtz_pdb: correlation of map and model allowing origin offsets
Copied as PDB/mmcif temp_dir/TEMP_TEMP_HA_1.cif
Map from:  TEMP_denmod_cycle_1_xl_1.mtz  using labin  FP=FWT PHIB=PHWT
Model from:  TEMP_TEMP_HA_1.cif
LABIN LINE:  FP=FWT PHIB=PHWT
Offsetting  TEMP_TEMP_HA_1.cif  to match  TEMP_denmod_cycle_1_xl_1.mtz
Getting FC from self.pdb...
FC is in  temp_dir/resolve.mtz
Offset pdb file is in  HA_1_offset.cif
Getting CC of  TEMP_TEMP_HA_1.cif  (offset to  HA_1_offset.cif ) to match  TEMP_denmod_cycle_1_xl_1.mtz
Detailed analysis of correlation of map and model are in:  cc.log
Offset PDB file is in HA_1_offset.cif

Correlation in region of model:  0.286 ...overall:  0.108
Translation applied: (0.00, 0.00, 0.00) A
overall CC:  0.108
local CC:  0.286


#                       get_cc_mtz_pdb
#
# Get correlation between atoms in a PDB file and map
# offsetting the PDB file by allowed origin shifts

# Type phenix.doc for help
Values of all params:
get_cc_mtz_pdb {
  pdb_in = "temp_dir/TEMP_HA_1.pdb"
  output_files {
    target_output_format = None *pdb mmcif
  }
  atom_selection = None
  mtz_in = "temp_dir/denmod_cycle_1_xl_2.mtz"
  map_in = None
  labin = ""
  offset_pdb_suffix = "offset"
  resolution = None
  use_only_refl_present_in_mtz = False
  scale = False
  split_conformers = False
  use_cc_mask = None
  fix_xyz = False
  fix_rad_max = False
  rad_max = None
  any_offset = False
  chain_type = *PROTEIN DNA RNA
  temp_dir = "temp_dir"
  output_dir = ""
  gui_output_dir = None
  verbose = True
  quick = False
  raise_sorry = False
  debug = False
  dry_run = False
  resolve_command_list = None
  job_title = None
}
Get_cc_mtz_pdb: correlation of map and model allowing origin offsets
Copied as PDB/mmcif temp_dir/TEMP_TEMP_HA_1.cif
Map from:  TEMP_denmod_cycle_1_xl_2.mtz  using labin  FP=FWT PHIB=PHWT
Model from:  TEMP_TEMP_HA_1.cif
LABIN LINE:  FP=FWT PHIB=PHWT
Offsetting  TEMP_TEMP_HA_1.cif  to match  TEMP_denmod_cycle_1_xl_2.mtz
Getting FC from self.pdb...
FC is in  temp_dir/resolve.mtz
Offset pdb file is in  HA_1_offset.cif
Getting CC of  TEMP_TEMP_HA_1.cif  (offset to  HA_1_offset.cif ) to match  TEMP_denmod_cycle_1_xl_2.mtz
Detailed analysis of correlation of map and model are in:  cc.log
Offset PDB file is in HA_1_offset.cif

Correlation in region of model:  0.28 ...overall:  0.024
Translation applied: (0.00, 0.00, 0.00) A
overall CC:  0.024
local CC:  0.28


#                       get_cc_mtz_pdb
#
# Get correlation between atoms in a PDB file and map
# offsetting the PDB file by allowed origin shifts

# Type phenix.doc for help
Values of all params:
get_cc_mtz_pdb {
  pdb_in = "temp_dir/TEMP_HA_2.pdb"
  output_files {
    target_output_format = None *pdb mmcif
  }
  atom_selection = None
  mtz_in = "temp_dir/denmod_cycle_1_xl_1.mtz"
  map_in = None
  labin = ""
  offset_pdb_suffix = "offset"
  resolution = None
  use_only_refl_present_in_mtz = False
  scale = False
  split_conformers = False
  use_cc_mask = None
  fix_xyz = False
  fix_rad_max = False
  rad_max = None
  any_offset = False
  chain_type = *PROTEIN DNA RNA
  temp_dir = "temp_dir"
  output_dir = ""
  gui_output_dir = None
  verbose = True
  quick = False
  raise_sorry = False
  debug = False
  dry_run = False
  resolve_command_list = None
  job_title = None
}
Get_cc_mtz_pdb: correlation of map and model allowing origin offsets
Copied as PDB/mmcif temp_dir/TEMP_TEMP_HA_2.cif
Map from:  TEMP_denmod_cycle_1_xl_1.mtz  using labin  FP=FWT PHIB=PHWT
Model from:  TEMP_TEMP_HA_2.cif
LABIN LINE:  FP=FWT PHIB=PHWT
Offsetting  TEMP_TEMP_HA_2.cif  to match  TEMP_denmod_cycle_1_xl_1.mtz
Getting FC from self.pdb...
FC is in  temp_dir/resolve.mtz
Offset pdb file is in  HA_2_offset.cif
Getting CC of  TEMP_TEMP_HA_2.cif  (offset to  HA_2_offset.cif ) to match  TEMP_denmod_cycle_1_xl_1.mtz
Detailed analysis of correlation of map and model are in:  cc.log
Offset PDB file is in HA_2_offset.cif

Correlation in region of model:  0.255 ...overall:  0.021
Translation applied: (0.00, 0.00, 0.00) A
overall CC:  0.021
local CC:  0.255


#                       get_cc_mtz_pdb
#
# Get correlation between atoms in a PDB file and map
# offsetting the PDB file by allowed origin shifts

# Type phenix.doc for help
Values of all params:
get_cc_mtz_pdb {
  pdb_in = "temp_dir/TEMP_HA_2.pdb"
  output_files {
    target_output_format = None *pdb mmcif
  }
  atom_selection = None
  mtz_in = "temp_dir/denmod_cycle_1_xl_2.mtz"
  map_in = None
  labin = ""
  offset_pdb_suffix = "offset"
  resolution = None
  use_only_refl_present_in_mtz = False
  scale = False
  split_conformers = False
  use_cc_mask = None
  fix_xyz = False
  fix_rad_max = False
  rad_max = None
  any_offset = False
  chain_type = *PROTEIN DNA RNA
  temp_dir = "temp_dir"
  output_dir = ""
  gui_output_dir = None
  verbose = True
  quick = False
  raise_sorry = False
  debug = False
  dry_run = False
  resolve_command_list = None
  job_title = None
}
Get_cc_mtz_pdb: correlation of map and model allowing origin offsets
Copied as PDB/mmcif temp_dir/TEMP_TEMP_HA_2.cif
Map from:  TEMP_denmod_cycle_1_xl_2.mtz  using labin  FP=FWT PHIB=PHWT
Model from:  TEMP_TEMP_HA_2.cif
LABIN LINE:  FP=FWT PHIB=PHWT
Offsetting  TEMP_TEMP_HA_2.cif  to match  TEMP_denmod_cycle_1_xl_2.mtz
Getting FC from self.pdb...
FC is in  temp_dir/resolve.mtz
Offset pdb file is in  HA_2_offset.cif
Getting CC of  TEMP_TEMP_HA_2.cif  (offset to  HA_2_offset.cif ) to match  TEMP_denmod_cycle_1_xl_2.mtz
Detailed analysis of correlation of map and model are in:  cc.log
Offset PDB file is in HA_2_offset.cif

Correlation in region of model:  0.891 ...overall:  0.239
Translation applied: (0.00, 0.00, 0.00) A
overall CC:  0.239
local CC:  0.891
