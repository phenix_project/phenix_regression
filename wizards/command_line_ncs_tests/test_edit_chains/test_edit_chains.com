#!/bin/csh -ef
#$ -cwd

phenix.python<<EOD
from phenix.autosol.edit_chains import   copy_and_edit_pdb
copy_and_edit_pdb("pdb_file.pdb","edited.pdb")
EOD
if ( $status )then
  echo "FAILED"
  exit 1
endif
cat edited.pdb 
