#!/bin/csh -ef
#$ -cwd

echo "Generating helical ncs"
phenix.find_ncs helix_trans_along_z=1 helix_theta=20
if ( $status )then
  echo "FAILED at find_ncs with helical parameters"
  exit 1
endif

echo "Finding ncs with mtz"
phenix.find_ncs coords_tet.pdb coords_tet.mtz resolution=3
if ( $status )then
  echo "FAILED at find_ncs with mtz"
  exit 1
endif

echo "Finding ncs with map"
phenix.find_ncs coords_tet.pdb coords_tet.ccp4 resolution=3
if ( $status )then
  echo "FAILED at find_ncs with map"
  exit 1
endif

phenix.find_ncs ncs_in=short.ncs_spec coordinate_offset="10 10 10"
if ( $status )then
  echo "FAILED at coordinate_offset"
  exit 1
endif


phenix.simple_ncs_from_pdb veryshort_ncs.pdb write_ncs_domain_pdb=true write_spec_files=True
if ( $status )then
  echo "FAILED at phenix.simple_ncs_from_pdb veryshort_ncs.pdb write_ncs_domain_pdb=true write_spec_files=True"
  exit 1
endif

echo "DIFFERENCES FROM EXPECTED:"
cat veryshort_ncs_simple_ncs_from_pdb.ncs_spec |tail -130 
phenix.python compare.py group_1.pdb group_1_expected.pdb
phenix.python compare.py group_4.pdb group_4_expected.pdb

echo "END OF DIFFERENCES FROM EXPECTED:"

echo "RUNNING TEST with change of ordering of NCS groups"

phenix.python test.py
if ( $status )then
  echo "FAILED at phenix.simple_ncs_from_pdb veryshort_ncs.pdb write_ncs_domain_pdb=true"
  exit 1
endif

phenix.find_ncs helical.ncs_spec helix_extend_range=10
if ( $status )then
  echo "FAILED at phenix.find_ncs helical.ncs_spec helix_extend_range=10"
  exit 1
endif

exit 0
