from __future__ import print_function

from cctbx.array_family import flex
import iotbx.pdb
from phenix.command_line.simple_ncs_from_pdb import simple_ncs_from_pdb
file_name='veryshort_ncs_order.pdb'

pdb_str = open(file_name).read()
raw_records = flex.std_string()
raw_records.extend(flex.split_lines(pdb_str))
pdb_inp = iotbx.pdb.input(source_info=None, lines=raw_records)
hierarchy=pdb_inp.construct_hierarchy()
from phenix.command_line import simple_ncs_from_pdb
simple_ncs=simple_ncs_from_pdb.run(args=[file_name,'write_ncs_domain_pdb=true'])
ncs_obj=simple_ncs.get_ncs_object()
from six.moves import cStringIO as StringIO
f=StringIO()
ncs_obj.display_all(log=f)
orig=f.getvalue()

new_obj=ncs_obj.deep_copy(hierarchy_to_match_order=hierarchy)
f=StringIO()
new_obj.display_all(log=f)
new=f.getvalue()
diff_a=[]
diff_b=[]
for a,b in zip(orig.splitlines(),new.splitlines()):
  if a!=b:
    diff_a.append(a)
    diff_b.append(b)
print("DIFFS:")
print("ORIG:\n")
print("\n".join(diff_a))
print("\nNEW:\n")
print("\n".join(diff_b))
