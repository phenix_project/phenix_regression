from __future__ import print_function
import sys
args=sys.argv[1:]


from mmtbx.ncs.ncs import ncs
ncs_object=ncs()
ncs_object.read_ncs(args[0],source_info=args[0])
ncs_object.display_all()

from phenix.autosol.get_pdb_inp import get_pdb_inp
pdb_input=get_pdb_inp(file_name=args[1])
crystal_symmetry=pdb_input.crystal_symmetry_from_cryst1()
unit_cell=crystal_symmetry.unit_cell()

from mmtbx.ncs.ncs import euler_frac_to_rot_trans
new_ncs=ncs()
i=0
text=""
for ncs_group in ncs_object._ncs_groups:
  euler_list,translation_list=\
    ncs_group.rotations_translations_forward_euler()
  for one_euler, translation in zip (euler_list,translation_list):

    frac=unit_cell.fractionalize(translation)
    center=[0.,0.,0.]
    print("REMARK EULER %8.2f %8.2f %8.2f  " %(tuple(one_euler)) + \
         "FRAC  %8.2f %8.2F %8.2f" %(tuple(frac)))

      # Now convert these back to RT from one_euler
    euler_values=list(one_euler)
    ncs_rota_matr,trans_orth=euler_frac_to_rot_trans(
      euler_values,frac,unit_cell)


    i+=1
    for j in range(3):
      text+="\nrota_matrix "+" %8.4f  %8.4f  %8.4f" %tuple(
          ncs_rota_matr[j*3:j*3+3])
    text+="\ntran_orth  "+" %8.4f  %8.4f  %8.4f" %tuple(trans_orth)
    text+="\n"
    text+="\ncenter_orth "+" %8.4f  %8.4f  %8.4f\n" %tuple(center)
    print(text)


    from scitbx.math import euler_angles_as_matrix, euler_angles
    new_angles=euler_angles.zyz_angles(ncs_rota_matr.inverse())
    print("Euler from matrix: %8.2f %8.2f %8.2f  " %(tuple(new_angles)))
