from __future__ import print_function
print("phenix.python apply_offset.py find_ncs.ncs_spec new_ncs.ncs_spec ab.pdb")
from mmtbx.ncs.ncs import ncs
import sys
args=sys.argv[1:]
file=args[0]
new_file=args[1]
ncs_object=ncs()
ncs_object.read_ncs(file,source_info=file)
ncs_object.display_all()
pdb_for_sym=args[2]
import iotbx
import iotbx.pdb
pdb_input = iotbx.pdb.input(file_name=pdb_for_sym)
crystal_symmetry=pdb_input.crystal_symmetry_from_cryst1()
unit_cell=crystal_symmetry.unit_cell()
coordinate_offset=(25.,35.,45.)
print("\n\nAfter offset:")
new_object=ncs_object.coordinate_offset(coordinate_offset=coordinate_offset)
new_object.display_all()
new_object.format_all_for_group_specification(file_name=new_file)

