#!/bin/csh -ef
#$ -cwd

phenix.apply_ncs ab.ncs_spec b.pdb debug=True> test_apply_ncs_current.log
if ( $status )then
  echo "FAILED"
  exit 1
endif
phenix.superpose_pdbs apply_ncs.pdb ab.pdb >> test_apply_ncs_current.log
if ( $status )then
  echo "FAILED"
  exit 1
endif

