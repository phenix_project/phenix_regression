from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_ncs_change_of_basis")
  print("")
  log_file=os.path.join(os.getcwd(),"test_ncs_change_of_basis.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  from phenix.command_line.find_ncs  import find_ncs
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''ab_coord_zero.pdb ncs_out=ab_coord_zero.ncs_spec''')
  find_ncs(args=args,out=log)


  from phenix.command_line.find_ncs  import find_ncs
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''ab_coord_offset.pdb ncs_out=ab_coord_offset.ncs_spec''')
  find_ncs(args=args,out=log)

  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.python apply_offset.py ab_coord_zero.ncs_spec ab_coord_zero_apply_offset.ncs_spec ab_coord_zero.pdb')
  for line in result.stdout_lines:
    print(line, file=log)
  print(open('ab_coord_offset.ncs_spec').read(), file=log)
  print(open('ab_coord_zero_apply_offset.ncs_spec').read(), file=log)

  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.python pdb_to_pdb.py ab.pdb ab_new_basis.pdb')
  for line in result.stdout_lines:
    print(line, file=log)


  from phenix.command_line.find_ncs  import find_ncs
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''ab.pdb ncs_out=ab.ncs_spec''')
  find_ncs(args=args,out=log)


  from phenix.command_line.find_ncs  import find_ncs
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''ab_new_basis.pdb ncs_out=ab_new_basis.ncs_spec''')
  find_ncs(args=args,out=log)
  print(open('ab_new_basis.ncs_spec').read(), file=log)

  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.python read_ncs.py ab.ncs_spec ab_transformed.ncs_spec ab.pdb')
  for line in result.stdout_lines:
    print(line, file=log)
  print(open('ab_transformed.ncs_spec').read(), file=log)

  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.python pdb_to_pdb.py ab_p1.pdb ab_p1_new_basis.pdb')
  for line in result.stdout_lines:
    print(line, file=log)


  from phenix.command_line.find_ncs  import find_ncs
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''ab_p1.pdb ncs_out=ab_p1.ncs_spec''')
  find_ncs(args=args,out=log)


  from phenix.command_line.find_ncs  import find_ncs
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''ab_p1_new_basis.pdb ncs_out=ab_p1_new_basis.ncs_spec''')
  find_ncs(args=args,out=log)
  print(open('ab_p1_new_basis.ncs_spec').read(), file=log)

  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.python read_ncs.py ab_p1.ncs_spec ab_p1_transformed.ncs_spec ab_p1.pdb')
  for line in result.stdout_lines:
    print(line, file=log)
  print(open('ab_p1_transformed.ncs_spec').read(), file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_ncs_change_of_basis',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['command_line_ncs_tests','test_ncs_change_of_basis']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
