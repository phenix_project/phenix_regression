#!/bin/csh -ef
# -cwd

echo ""
echo "Comparing NCS from ab_coord_zero.pdb then applying coordinate_offset"
echo "to ncs.ncs_spec, with NCS from ab_coord_offset.pdb"

echo "Finding ncs in original coordinate system -> ab_coord_zero.ncs_spec"
  phenix.find_ncs  ab_coord_zero.pdb ncs_out=ab_coord_zero.ncs_spec > ab_coord_zero_ncs_spec.log

echo "Finding ncs in new coordinate system -> ab_coord_offset.ncs_spec"
  phenix.find_ncs  ab_coord_offset.pdb ncs_out=ab_coord_offset.ncs_spec > ab_coord_offset_ncs_spec.log

echo "Applying coordinate offset to ab_coord_zero.ncs_spec"
phenix.python apply_offset.py ab_coord_zero.ncs_spec ab_coord_zero_apply_offset.ncs_spec ab_coord_zero.pdb > ab_coord_zero_apply_offset.log

echo "ab_coord_offset.ncs_spec:"
cat ab_coord_offset.ncs_spec 
echo "ab_coord_zero_apply_offset.ncs_spec:"
cat ab_coord_zero_apply_offset.ncs_spec


echo ""
echo "Comparing NCS from ab.pdb after change-of-basis with application"
echo " of change-of-basis directly to NCS from ab.pdb"
echo ""
echo "Creating duplicate of ab.pdb in new coordinate system"
phenix.python pdb_to_pdb.py ab.pdb ab_new_basis.pdb > ab_new_basis.log

echo "Finding ncs in original coordinate system -> ab.ncs_spec"
  phenix.find_ncs  ab.pdb ncs_out=ab.ncs_spec > ab_ncs_spec.log

echo "Finding ncs in new coordinate system -> new_ab.ncs_spec"
  phenix.find_ncs  ab_new_basis.pdb ncs_out=ab_new_basis.ncs_spec >ab_new_basis_ncs_spec.log

# have to edit these so that rota_matrix, tran_orth and center_orth are not ignored in the diff.dat that comes up when this test is run
cat  ab_new_basis.ncs_spec 

echo "Transforming ncs object ab.ncs_spec to new coordinate system directly"
  phenix.python read_ncs.py ab.ncs_spec ab_transformed.ncs_spec ab.pdb > ab_transformed.log
cat ab_transformed.ncs_spec 


echo ""
echo "Comparing NCS from ab_p1.pdb after change-of-basis with application"
echo " of change-of-basis directly to NCS from ab_p1.pdb"
echo ""
echo "Creating duplicate of ab_p1.pdb in new coordinate system"
phenix.python pdb_to_pdb.py ab_p1.pdb ab_p1_new_basis.pdb > ab_p1_new_basis.log

echo "Finding ncs in original coordinate system -> ab.ncs_spec"
  phenix.find_ncs  ab_p1.pdb ncs_out=ab_p1.ncs_spec > ab_p1_ncs_spec.log

echo "Finding ncs in new coordinate system -> new_ab.ncs_spec"
  phenix.find_ncs  ab_p1_new_basis.pdb ncs_out=ab_p1_new_basis.ncs_spec >ab_p1_new_basis_ncs_spec.log

# have to edit these so that rota_matrix, tran_orth and center_orth are not ignored in the diff.dat that comes up when this test is run
cat  ab_p1_new_basis.ncs_spec 

echo "Transforming ncs object ab_p1.ncs_spec to new coordinate system directly"
  phenix.python read_ncs.py ab_p1.ncs_spec ab_p1_transformed.ncs_spec ab_p1.pdb > ab_p1_transformed.log
cat ab_p1_transformed.ncs_spec 




finish:



