from __future__ import print_function
## pdb_to_pdb.py
## edit this file to use as a jiffy to edit pdb files
import os, shutil, string, re, sys, time
from mmtbx import monomer_library
import mmtbx.monomer_library.pdb_interpretation
import mmtbx.monomer_library.server
from iotbx import pdb
import iotbx
from iotbx.pdb import resseq_encode, resseq_decode
from phenix.autosol.resname import resname
from phenix.autosol import iotbx_pdb_v0
from scitbx.math import  matrix
def copy_and_edit_pdb(file,work_file):
    print("Changing basis ...")
   
    out=open(work_file,'w')
    pdb_input = iotbx.pdb.input(file_name=file)

    crystal_symmetry=pdb_input.crystal_symmetry_from_cryst1()
    unit_cell=crystal_symmetry.unit_cell()
    cob=unit_cell.change_of_basis_op_to_niggli_cell()

    new_unit_cell = unit_cell.change_basis(cob)
    print("NEW CELL: ",new_unit_cell)

    from cctbx import crystal
    from cctbx import sgtbx
    new_space_group=sgtbx.space_group_info(symbol="P1") # XXX just for now
    new_crystal_symmetry=crystal.symmetry(
      space_group_info=new_space_group,unit_cell=new_unit_cell)
    cryst1_record=pdb.format_cryst1_record(crystal_symmetry=new_crystal_symmetry)
    print(cryst1_record, file=out)


    hierarchy = pdb_input.construct_hierarchy()
    serial=0
    for model in hierarchy.models():
      chains = model.chains()
      for chain in chains:
        conformers = chain.conformers()
        for conformer in conformers:
          residues = conformer.residues()
          for residue in residues:
            residue_resname = residue.resname
            residue_resseq = residue.resseq
            residue_icode = residue.icode
            residue_link_to_previous = residue.link_to_previous
            atoms = list(residue.atoms())
            resseq_int = residue.resseq_as_int()
            for atom in atoms:
              serial += 1
              frac=unit_cell.fractionalize(atom.xyz)
              new_frac = cob.c() * frac
              new_xyz=new_unit_cell.orthogonalize(new_frac)
              print(iotbx_pdb_v0.format_atom_record(
                serial=serial,
                name=atom.name,
                altLoc=" ",
                resName=residue.resname,
                chainID=chain.id,
                resSeq=resseq_encode(resseq_int),
                iCode=residue_icode,
                site=new_xyz, #atom.xyz,
                occupancy=atom.occ,
                tempFactor=atom.b,
                segID=atom.segid,
                element=atom.element,
                charge=atom.charge), file=out)

    out.close()
if __name__=="__main__":
  copy_and_edit_pdb(sys.argv[1],sys.argv[2])
