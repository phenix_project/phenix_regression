#!/bin/csh -ef
#$ -cwd

echo ""
echo "Comparing NCS from ab_coord_zero.pdb then applying coordinate_offset"
echo "to ncs.ncs_spec, with NCS from ab_coord_offset.pdb"

echo "Finding ncs in original coordinate system -> ab_coord_zero.ncs_spec"
  phenix.find_ncs  ab_coord_zero.pdb ncs_out=ab_coord_zero.ncs_spec > ab_coord_zero_ncs_spec.log

echo "Finding ncs in new coordinate system -> ab_coord_offset.ncs_spec"
  phenix.find_ncs  ab_coord_offset.pdb ncs_out=ab_coord_offset.ncs_spec > ab_coord_offset_ncs_spec.log

echo "Applying coordinate offset to ab_coord_zero.ncs_spec"
phenix.python apply_offset.py ab_coord_zero.ncs_spec ab_coord_zero_apply_offset.ncs_spec ab_coord_zero.pdb > ab_coord_zero_apply_offset.log

echo "ab_coord_offset.ncs_spec:"
cat ab_coord_offset.ncs_spec 
echo "ab_coord_zero_apply_offset.ncs_spec:"
cat ab_coord_zero_apply_offset.ncs_spec


