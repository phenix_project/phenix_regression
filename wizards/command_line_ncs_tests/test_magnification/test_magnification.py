import sys
args=sys.argv[1:]

from mmtbx.ncs.ncs import ncs
ncs_object=ncs()
ncs_object.read_ncs(args[0],source_info=args[0])
ncs_object.display_all()

#
new_ncs_object=ncs_object.magnification(scale_factor=1.05)
new_ncs_object.display_all()


