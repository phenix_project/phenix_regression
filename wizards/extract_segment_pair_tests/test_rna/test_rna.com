#!/bin/csh -ef
#$ -cwd
setenv SOLVETMPDIR /var/tmp
solve_resolve.extract_segment_pair<<EOD 
5,0,    ! 5=type, 0=rna 1=dna 2=prot
RNA
360.
-1     ! antiparallel
1.0,0.0,
1T0E_3-6_edited.pdb
1T0E_10-13_edited.pdb
all_edited_053007_order.pdb
solve_30.mtz
labin FP=FP FOM=FOM PHIB=PHIB
template_30_mean_5.ezd
template_30_corr_5.ezd
template_30_mask_5.ezd
EOD

