#!/bin/csh -ef
#$ -cwd
setenv SOLVETMPDIR /var/tmp
#
solve_resolve.extract_segment_pair<<EOD
3,2,    ! 5=type, 0=rna 1=dna 2=prot
PARALLEL BETA
360.
1     ! parallel
1.0,0.0,
1bav_191_194.pdb
1bav_265_268.pdb
1bav_chaina.pdb
solve_30.mtz
labin FP=FP FOM=FOM PHIB=PHIB
template_30_mean_3.ezd
template_30_corr_3.ezd
template_30_mask_3.ezd
EOD
