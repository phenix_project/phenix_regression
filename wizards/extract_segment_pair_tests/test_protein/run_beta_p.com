#!/bin/csh
setenv SOLVETMPDIR /var/tmp

#
#  put name of PDB model with all source models here:
setenv input_model  1bav_chaina.pdb
#
#
echo "Working in `pwd`"
if (-d segments )rm -rf segments
mkdir segments
#
echo "starting template generation "
rm -rf segment_*_3.pdb *.ezd template_map_coeffs.mtz
#
solve_resolve.extract_segment_pair<<EOD >extract_segment_pair.log
3,2,    ! 5=type, 0=rna 1=dna 2=prot
PARALLEL BETA
360.
1     ! parallel
1.0
1bav_191_194.pdb
1bav_265_268.pdb
${input_model}
solve_30.mtz
labin FP=FP FOM=FOM PHIB=PHIB
template_30_mean_3.ezd
template_30_corr_3.ezd
template_30_mask_3.ezd
EOD
cp -p segment_*_3.pdb *.ezd template_map_coeffs.mtz segments/
rm -rf segment_*_3.pdb *.ezd template_map_coeffs.mtz
#
echo "ready..."
echo "Results are in `cd segments; pwd ; cd ..`"
