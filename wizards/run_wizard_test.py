from __future__ import division
from __future__ import print_function

import sys
# run_wizard_test.py # run resno tests

import sys,os
import libtbx.load_env
import shutil
from libtbx.utils import Sorry

def replace_pair_of_quotes(line):
  #  replace ""   "" with  "  "
  return line.replace('""','"').replace("''","'")

def replace_multiple_rhs(line):
  #  replace "seq_file=a b"  with "seq_file='a b'"
  spl=line.split("=")
  rhs=spl[-1]
  if len(rhs.split())>1 and spl[0]=='resolve_command_list':
     return spl[0]+"="+"'%s'" %(rhs)

  else:
    return line

def split_except_quotes(line):
  # split at spaces but not those within quotes
  #  if end up with ""  "" or '' ''  replace the inside ones
  assert line.find("|")<0  # our replacement
  new_line=""
  line=replace_pair_of_quotes(line)
  started_single=False
  started_double=False
  for c in line:
    if c=="'":
      if started_single:
        started_single=False
      else:
        started_single=True

    elif c=='"':
      if started_double:
        started_double=False
      else:
        started_double=True
    if (started_single or started_double) and c==" ":
       new_line+="|"
    else:
       new_line+=c
  new_split=[]
  for s in new_line.split():
    new_s=s.replace("|"," ")
    new_s=new_s.replace("'","").replace('"',"")
    new_s=replace_multiple_rhs(new_s)
    new_split.append(new_s)
  return new_split

def get_test_dict(group_name=None):
  base_path = libtbx.env.under_dist(
    module_name="phenix_regression",
    path='wizards',
    test=os.path.isdir)
  assert base_path is not None

  # identify what test groups to work with
  if group_name:
    if not group_name.endswith("_tests"):
      group_name="%s_tests" %(group_name)

    group_data_dir=os.path.join(base_path,group_name)
    if not os.path.isdir(group_data_dir):
      raise Sorry("Cannot find the directory %s" %(group_data_dir))
    group_name_list=[group_name]
  else:
    group_name_list=[]
    for dd in os.listdir(base_path):
      if not dd.endswith("_tests"): continue
      group_name_list.append(dd)

  # Now get all the tests in all these dirs
  test_dict={}
  for group_name in group_name_list:
    group_data_dir=os.path.join(base_path,group_name)
    test_dict[group_name]=[]
    for xx in os.listdir(group_data_dir):
      if xx.startswith("test_") and os.path.isdir(os.path.join(group_data_dir,xx)):
        if not xx in test_dict[group_name]:
          test_dict[group_name].append(xx)
  return test_dict

def find_test(test_name=None,group_name=None):
  assert test_name
  base_path = libtbx.env.under_dist(
    module_name="phenix_regression",
    path='wizards',
    test=os.path.isdir)
  assert base_path is not None

  if not test_name.startswith("test_"):
    test_name="test_%s" %(test_name)

  if group_name:
    if not group_name.endswith("_tests"):
      group_name="%s_tests" %(group_name)

    group_data_dir=os.path.join(base_path,group_name)
    if not os.path.isdir(group_data_dir):
      raise Sorry("Cannot find the directory %s" %(group_data_dir))
  else:
    for dd in os.listdir(base_path):
      if os.path.isdir(os.path.join(base_path,dd)) \
         and os.path.isdir(os.path.join(base_path,dd,test_name)):
        if dd.find("ccp4lib_422_cci")>-1: continue # not a real test
        group_name=dd
        break
    if not group_name:
      raise Sorry("Cannot find the test %s" %(test_name))
  return group_name,test_name

def set_up_wizard_test(args,out=sys.stdout):
  if not args:
    print("phenix_regression.run_wizard_test [resno_tests] test_resno", file=out)
    print("phenix_regression.run_wizard_test resno", file=out)
    sys.exit(0)

  if len(args)==1:
    test_name=args[0]
    group_name=None
  else:
    test_name=args[1]
    group_name=args[0]

  group_name,test_name=find_test(test_name=test_name,group_name=group_name)

  print("\nRunning test %s in group %s\n" %(test_name,group_name), file=out)

  data_dir = libtbx.env.under_dist(
    module_name="phenix_regression",
    path=os.path.join('wizards',group_name,test_name),
    test=os.path.isdir)

  if not os.path.isdir(group_name):
    print("Making local directory %s" %(group_name), file=out)
    try:
      os.mkdir(group_name)
    except Exception as e:
      pass # probably had a conflict
  local_path=os.path.join(group_name,test_name)
  if not os.path.isdir(local_path):
    print("Making local directory %s" %(local_path), file=out)
    os.mkdir(local_path)
  print("Copying files from %s" %(data_dir), file=out)
  assert data_dir is not None
  for file_name in os.listdir(data_dir):
    if os.path.isfile(os.path.join(data_dir,file_name)):
      shutil.copyfile(os.path.join(data_dir,file_name),
        os.path.join(local_path,file_name))
    elif os.path.isdir(os.path.join(data_dir,file_name)):
      sub_dir_name=file_name
      sub_dir=os.path.join(data_dir,sub_dir_name)
      if not os.path.isdir(os.path.join(local_path,sub_dir_name)):
        os.mkdir(os.path.join(local_path,sub_dir_name))
      for local_file_name in os.listdir(sub_dir):
        if os.path.isfile(os.path.join(sub_dir,local_file_name)):
          shutil.copyfile(os.path.join(sub_dir,local_file_name),
                          os.path.join(local_path,sub_dir_name,local_file_name))

  print("\nFiles in local directory:", file=out)
  for x in os.listdir(local_path):
    print(x)

  # change to working directory
  os.chdir(local_path)
  print("\nWorking directory: %s\n" %(os.getcwd()), file=out)

if __name__=="__main__":
  run_wizard_test(sys.argv[1:])
