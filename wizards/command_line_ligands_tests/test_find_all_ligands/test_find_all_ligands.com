#!/bin/csh -ef
#$ -cwd

phenix.find_all_ligands data=perfect.mtz model=partial.pdb quick=True resolution=3.0 n_group_search=1 ligand_cc_min=0.5 input_labels="FP PHIC FOM" quick=true ligand_list="side.pdb sideb.pdb" number_of_ligands=2 debug=true
if ( $status )then
  echo "FAILED"
  exit 1
endif
