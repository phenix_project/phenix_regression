

#                       find_all_ligands
#
# Run ligandfit with several ligands to
# identify which fits the map the best
# Type phenix.doc for help
find_all_ligands {
  output_files {
    target_output_format = None *pdb mmcif
  }
  number_of_ligands = 2
  cc_min = 0.5
  ligand_list = side.pdb sideb.pdb
  nproc = 1
  background = True
  run_command = "sh "
  verbose = False
  raise_sorry = False
  debug = True
  temp_dir = Auto
  output_dir = ""
  dry_run = False
  check_wait_time = 1
  wait_between_submit_time = 1
}
ligandfit {
  data = "perfect.mtz"
  ligand = None
  model = "partial.pdb"
  quick = True
  crystal_info {
    unit_cell = None
    resolution = 3.5
    space_group = None
  }
  file_info {
    file_or_file_list = *single_file file_with_list_of_files
    input_labels = FP PHIC FOM
    lig_map_type = fo-fc_difference_map fobs_map *pre_calculated_map_coeffs
    ligand_format = *PDB SMILES
  }
  output_files {
    target_output_format = *None pdb mmcif
  }
  input_files {
    existing_ligand_file_list = None
    ligand_start = None
    ncs_in = None
    input_ligand_compare_file = None
    cif_def_file_list = None
    refinement_file = None
    fobs_labels = None
    r_free_label = None
    map_in = None
  }
  search_parameters {
    fixed_ligand = False
    conformers = 1
    fitting {
      delta_phi_ligand = 40
      fit_phi_inc = 20
      fit_phi_range = -180 180
    }
    group_search = 0
    ligand_cc_min = 0.5
    ligand_completeness_min = 1
    local_search = True
    search_dist = 10
    use_cc_local = False
    ligands_from_ncs = False
    max_ligands_from_ncs = 1
    n_group_search = 1
    n_indiv_tries_max = 10
    n_indiv_tries_min = 5
    number_of_ligands = 1
    offsets_list = 7 53 29
    refine_ligand = False
    ligand_occupancy = 1
    real_space_target_weight = 10
  }
  search_target {
    ligand_near_chain = None
    ligand_near_res = None
    ligand_near_pdb = None
    name_of_ligand_near_pdb = None
    search_center = 0 0 0
  }
  general {
    extend_try_list = True
    ligand_id = None
    nbatch = 5
    nproc = 1
    resolve_command_list = None
    coot_name = "coot"
    i_ran_seed = 72432
    raise_sorry = False
    background = True
    check_wait_time = 1
    max_wait_time = 1
    wait_between_submit_time = 1
    cache_resolve_libs = True
    resolve_size = "12"
    check_run_command = False
    run_command = "sh "
    queue_commands = None
    condor_universe = "vanilla"
    add_double_quotes_in_condor = True
    condor = None
    last_process_is_local = True
    skip_r_factor = False
    test_flag_value = Auto
    skip_xtriage = False
    base_path = None
    temp_dir = None
    local_temp_directory = None
    clean_up = None
    print_citations = True
    solution_output_pickle_file = None
    job_title = None
    top_output_dir = None
    wizard_directory_number = None
    verbose = False
    extra_verbose = False
    debug = False
    require_nonzero = True
    remove_path_word_list = None
    fill = False
    res_fill = None
    check_only = False
    keep_files = "ligandfit*.pdb"
  }
  display {
    number_of_solutions_to_display = None
    solution_to_display = 1
  }
  run_control {
    ignore_blanks = None
    stop = None
    display_facts = None
    display_summary = None
    carry_on = None
    run = None
    copy_run = None
    display_runs = None
    delete_runs = None
    display_labels = None
    dry_run = False
    params_only = False
    display_all = False
    coot = None
  }
  special_keywords {
    write_run_directory_to_file = None
  }
  non_user_parameters {
    gui_output_dir = None
    sg = None
    get_lig_volume = False
    input_data_file = None
    input_lig_file = None
    ligand_code = None
    input_partial_model_file = None
    cif_already_generated = False
  }
}

Running find_all_ligands to find all copies of ligand
Maximum number of ligand sites to find: 2
Minimum correlation of ligands to map to keep:  0.5
List of ligands to test: ['side.pdb', 'sideb.pdb']


Finding ligands one at a time...

Ligand center for site 1 : [11.3, 22.9, 1.0]
Estimated volume:  76.0

Searching for ligand 1

Number of processors for each run: 1
Ligand_list:  ['side.pdb', 'sideb.pdb']


SOLUTION
...files are relative path starting from:
/net/anaconda/raid1/terwill/misc/junk1/command_line_ligands_tests/test_find_all_ligands/TEMP0
All model list for this solution with CC= 0.743 :
['LigandFit_run_1_/ligand_fit_1.pdb']
Model  LigandFit_run_1_/ligand_fit_1.pdb copied to  /net/anaconda/raid1/terwill/misc/junk1/command_line_ligands_tests/test_find_all_ligands/TEMP0/SITE_1_side.pdb

New best ligand for site  1  is  /net/anaconda/raid1/terwill/misc/junk1/command_line_ligands_tests/test_find_all_ligands/TEMP0/SITE_1_side.pdb


SOLUTION
...files are relative path starting from:
/net/anaconda/raid1/terwill/misc/junk1/command_line_ligands_tests/test_find_all_ligands/TEMP0
All model list for this solution with CC= 0.743 :
['LigandFit_run_2_/ligand_fit_1.pdb']
Model  LigandFit_run_2_/ligand_fit_1.pdb copied to  /net/anaconda/raid1/terwill/misc/junk1/command_line_ligands_tests/test_find_all_ligands/TEMP0/SITE_1_sideb.pdb

Done with group of wizards.......
Ligand center for site 2 : [8.2, 1.0, 1.0]
Estimated volume:  57.0

Searching for ligand 2
based on  ['/net/anaconda/raid1/terwill/misc/junk1/command_line_ligands_tests/test_find_all_ligands/TEMP0/SITE_1_side.pdb']
Number of processors for each run: 1
Ligand_list:  ['side.pdb', 'sideb.pdb']


SOLUTION
...files are relative path starting from:
/net/anaconda/raid1/terwill/misc/junk1/command_line_ligands_tests/test_find_all_ligands/TEMP0
All model list for this solution with CC= 0.597 :
['LigandFit_run_3_/ligand_fit_1.pdb']
Model  LigandFit_run_3_/ligand_fit_1.pdb copied to  /net/anaconda/raid1/terwill/misc/junk1/command_line_ligands_tests/test_find_all_ligands/TEMP0/SITE_2_side.pdb

New best ligand for site  2  is  /net/anaconda/raid1/terwill/misc/junk1/command_line_ligands_tests/test_find_all_ligands/TEMP0/SITE_2_side.pdb


SOLUTION
...files are relative path starting from:
/net/anaconda/raid1/terwill/misc/junk1/command_line_ligands_tests/test_find_all_ligands/TEMP0
All model list for this solution with CC= 0.595 :
['LigandFit_run_4_/ligand_fit_1.pdb']
Model  LigandFit_run_4_/ligand_fit_1.pdb copied to  /net/anaconda/raid1/terwill/misc/junk1/command_line_ligands_tests/test_find_all_ligands/TEMP0/SITE_2_sideb.pdb

Done with group of wizards.......

Ending search cycles as number of ligands placed ( 2 ) has reached the
target of  2

Final list of ligands:  ['/net/anaconda/raid1/terwill/misc/junk1/command_line_ligands_tests/test_find_all_ligands/TEMP0/SITE_1_side.pdb', '/net/anaconda/raid1/terwill/misc/junk1/command_line_ligands_tests/test_find_all_ligands/TEMP0/SITE_2_side.pdb']
Final list of cc values:    0.74   0.60
Final list of estimated volumes:      76.0     57.0

Final list of all ligands and cc values at all sites:
SITE:  1  ESTIMATED VOLUME:     76.0
     CC            LIGAND
    0.74      /net/anaconda/raid1/terwill/misc/junk1/command_line_ligands_tests/test_find_all_ligands/TEMP0/SITE_1_sideb.pdb
    0.74      /net/anaconda/raid1/terwill/misc/junk1/command_line_ligands_tests/test_find_all_ligands/TEMP0/SITE_1_side.pdb

SITE:  2  ESTIMATED VOLUME:     57.0
     CC            LIGAND
    0.60      /net/anaconda/raid1/terwill/misc/junk1/command_line_ligands_tests/test_find_all_ligands/TEMP0/SITE_2_side.pdb
    0.59      /net/anaconda/raid1/terwill/misc/junk1/command_line_ligands_tests/test_find_all_ligands/TEMP0/SITE_2_sideb.pdb

Model with final set of ligands is in  composite.pdb

DONE


Citations for find_all_ligands:

Liebschner D, Afonine PV, Baker ML, Bunkóczi G, Chen VB, Croll TI, Hintze B,
Hung LW, Jain S, McCoy AJ, Moriarty NW, Oeffner RD, Poon BK, Prisant MG, Read
RJ, Richardson JS, Richardson DC, Sammito MD, Sobolev OV, Stockwell DH,
Terwilliger TC, Urzhumtsev AG, Videau LL, Williams CJ, Adams PD. (2019)
Macromolecular structure determination using X-rays, neutrons and electrons:
recent developments in Phenix. Acta Cryst. D75:861-877.

Terwilliger TC, Adams PD, Moriarty NW, Cohn JD. (2006) Ligand identification
using electron-density map correlations. Acta Crystallogr D Biol Crystallogr
63:101-7.

Terwilliger TC, Klei H, Adams PD, Moriarty NW, Cohn JD. (2006) Automated ligand
fitting by core-fragment fitting and extension into density. Acta Crystallogr D
Biol Crystallogr 62:915-22.

