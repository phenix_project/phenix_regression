from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os,sys
  print("Test script for test_ligand_refine")
  print("")
  log_file=os.path.join(os.getcwd(),"test_ligand_refine.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  local_log_file='test_ligand_refine.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.ligandfit  import run_ligandfit
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''nsf-d2.mtz ligand=lig.pdb model=nsf-d2_noligand.pdb cif_def_file_list=lig.cif refinement_file=fobs.mtz fobs_labels=F-obs,SIGF-obs r_free_label=R-free-flags quick=True resolution=3.5 lig_map_type=fo-fc_difference_map debug=true n_group_search=1 n_indiv_tries_max=1 n_indiv_tries_min=1''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_ligandfit(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)


  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_ligand_refine',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['command_line_ligands_tests','test_ligand_refine']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
