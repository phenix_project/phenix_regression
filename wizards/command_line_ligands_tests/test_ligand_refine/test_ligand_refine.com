#!/bin/csh -ef
#$ -cwd

phenix.ligandfit nsf-d2.mtz \
 ligand=lig.pdb \
 model=nsf-d2_noligand.pdb \
 cif_def_file_list=lig.cif  \
 refinement_file=fobs.mtz \
 fobs_labels=F-obs,SIGF-obs \
 r_free_label=R-free-flags \
 quick=True \
 quick=True resolution=3.0 \
 lig_map_type=fo-fc_difference_map \
 debug=true
if ( $status )then
  echo "FAILED"
  exit 1
endif
