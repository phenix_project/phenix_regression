#!/bin/csh -ef
#$ -cwd

phenix.ligandfit nsf-d2.mtz \
model=nsf-d2_noligand.pdb \
lig_map_type=fo-fc_difference_map \
cif_def_file_list=ATP_ELBOW.cif \
quick=true \
ligand_code=atp \
 debug=true

if ( $status )then
  echo "FAILED"
  exit 1
endif
