#!/bin/csh -ef
#$ -cwd

echo "Running ligandfit with ncs"
phenix.ligandfit model=partial.pdb ligand=lig.pdb data=map_coeffs.mtz number_of_ligands=4 input_labels='2FOFCWT PH2FOFCWT' nbatch=1 quick=True ligands_from_ncs=True ncs_in=ncs.ncs_spec max_ligands_from_ncs=1
 if ( $status == 1 )then
    echo "*****************************************************"
    echo "Error: ligandfit with ncs failed "
    echo "*****************************************************"
    exit 1
 endif
