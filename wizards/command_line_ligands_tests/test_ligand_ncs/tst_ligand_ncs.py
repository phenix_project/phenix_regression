from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os,sys
  print("Test script for test_ligand_ncs")
  print("")
  log_file=os.path.join(os.getcwd(),"test_ligand_ncs.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  local_log_file='test_ligand_ncs.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.ligandfit  import run_ligandfit
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''model=partial.pdb ligand=lig.pdb data=map_coeffs.mtz number_of_ligands=2 refine_ligand=False  input_labels='2FOFCWT PH2FOFCWT' nbatch=1 quick=True ligands_from_ncs=True ncs_in=ncs.ncs_spec max_ligands_from_ncs=1''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_ligandfit(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)


  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_ligand_ncs',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['command_line_ligands_tests','test_ligand_ncs']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
