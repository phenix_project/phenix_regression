#!/bin/csh -ef
#$ -cwd

phenix.python run_fit.py

if ( $status )then
  echo "FAILED"
  exit 1
endif
