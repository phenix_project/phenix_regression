from __future__ import print_function
#
# script to run ligand fitting
# tt 2013-02-15

print("Getting map from ligand.mtz")
from iotbx import reflection_file_reader
from solve_resolve.resolve_python.resolve_utils import get_map_mask_sg_cell
from libtbx.utils import null_out
map_db,mask_map_db,space_group,unit_cell=\
      get_map_mask_sg_cell(map_coeff_file='ligand.mtz',
      solvent_content=0.5,
      resolution=2.5,out=null_out())
map=map_db.map


print("Getting ligand from lig.pdb (can supply n conformations if desired)")
import iotbx.pdb
ligand_pdb_inp=iotbx.pdb.input('lig.pdb')

import sys
out=sys.stdout

print("HERE IS WHERE YOU START WITH MAP and ligand_pdb_inp:")

input_text="""
ligand_file MEMORY
no_local_search

delta_phi_ligand 40.0
n_indiv_tries_min 5
n_indiv_tries_max 10
n_group_search 3
fit_phi_inc 20.0
n_keep_plac 100
fit_phi_range -180.0 180.0

"""

from solve_resolve.resolve_python import resolve_in_memory
result_obj=resolve_in_memory.run(map=map,
      space_group=space_group,
      unit_cell=unit_cell,
      input_text=input_text,
      pdb_inp=ligand_pdb_inp,
      wang_radius=5.,
      out=out)
cmn=result_obj.results


fitted_ligand=cmn.atom_db.pdb_out_as_string
print(fitted_ligand)




