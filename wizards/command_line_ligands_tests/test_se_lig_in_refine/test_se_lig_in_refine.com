#!/bin/csh -ef
#$ -cwd

if (! -d temp_dir) mkdir temp_dir

echo "Running phenix.refine with c_no_se.pdb and ligs_no_se.pdb"
cp c_no_se.pdb coords.pdb
cp ligs_no_se.pdb ligands.pdb

phenix.python<<EOD
from phenix.autosol.run_phenix_refine import run_test
run_test
EOD
 if ( $status == 1 )then
    echo "*****************************************************"
    echo "Error: run_phenix_refine failed with c_no_se.pdb and ligs_no_se.pdb" 
    echo "*****************************************************"
    exit 1
 endif

echo "Running phenix.refine with c_no_se.pdb and ligs.pdb"
cp c_no_se.pdb coords.pdb
cp ligs.pdb ligands.pdb
phenix.python<<EOD
from phenix.autosol.run_phenix_refine import run_test
run_test
EOD
 if ( $status == 1 )then
    echo "*****************************************************"
    echo "Error: run_phenix_refine failed with c_no_se.pdb and ligs.pdb" 
    echo "*****************************************************"
    exit 1
 endif


echo "Running phenix.refine with c.pdb and ligs_no_se.pdb"
cp c.pdb coords.pdb
cp ligs_no_se.pdb ligands.pdb
phenix.python<<EOD
from phenix.autosol.run_phenix_refine import run_test
run_test
EOD
 if ( $status == 1 )then
    echo "*****************************************************"
    echo "Error: run_phenix_refine failed with c.pdb and ligs_no_se.pdb" 
    echo "*****************************************************"
    exit 1
 endif

echo "Running phenix.refine with c.pdb and ligs.pdb"
cp c.pdb coords.pdb
cp ligs.pdb ligands.pdb
phenix.python<<EOD
from phenix.autosol.run_phenix_refine import run_test
run_test
EOD
 if ( $status == 1 )then
    echo "*****************************************************"
    echo "Error: run_phenix_refine failed with c.pdb and ligs.pdb" 
    echo "*****************************************************"
    exit 1
 endif


