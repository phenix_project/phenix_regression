from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_se_lig_in_refine")
  print("")
  log_file=os.path.join(os.getcwd(),"test_se_lig_in_refine.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  import shutil
  shutil.copyfile('c_no_se.pdb','coords.pdb')

  import shutil
  shutil.copyfile('ligs_no_se.pdb','ligands.pdb')

  cmds='''from phenix.autosol.run_phenix_refine import run_test
run_test
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print(open('phenix.python.log').read(), file=log)

  import shutil
  shutil.copyfile('c_no_se.pdb','coords.pdb')

  import shutil
  shutil.copyfile('ligs.pdb','ligands.pdb')

  cmds='''from phenix.autosol.run_phenix_refine import run_test
run_test
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print(open('phenix.python.log').read(), file=log)

  import shutil
  shutil.copyfile('c.pdb','coords.pdb')

  import shutil
  shutil.copyfile('ligs_no_se.pdb','ligands.pdb')

  cmds='''from phenix.autosol.run_phenix_refine import run_test
run_test
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print(open('phenix.python.log').read(), file=log)

  import shutil
  shutil.copyfile('c.pdb','coords.pdb')

  import shutil
  shutil.copyfile('ligs.pdb','ligands.pdb')

  cmds='''from phenix.autosol.run_phenix_refine import run_test
run_test
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print(open('phenix.python.log').read(), file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_se_lig_in_refine',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['command_line_ligands_tests','test_se_lig_in_refine']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
