

#                       mr_rosetta
#
# Run automr/autobuild/rosetta together


# Type phenix.doc for help

usage: phenix.mr_rosetta mtzfile.mtz sequence.dat [pdbfile.pdb] [labin='FP=FP PHIB=PHIM FOM=FOMM']

Values of all params:
mr_rosetta {
  input_files {
    data = "fobs.mtz"
    data_labels = None
    free_r_data = None
    free_r_labels = None
    remove_free = True
    labin = None
    seq_file = "seq.dat"
    search_models = "coords1.pdb"
    copies_in_search_models = None
    hhr_files = None
    alignment_files = None
    model_info_file = None
    mr_rosetta_solutions = None
    ids_to_load = None
    map_coeffs = None
    labin_map_coeffs = None
    map = None
    refinement_params = None
    display_solutions = False
    fragment_files = None
    fragment_files_chain_list = None
    fragment_files_9_mer_by_chain = None
    fragment_files_3_mer_by_chain = None
    use_dummy_fragment_files = False
    sort_fragment_files = True
  }
  output_files {
    log = "mr_rosetta.log"
    sort_score_type = None
    params_out = "mr_rosetta_params.eff"
  }
  directories {
    temp_dir = ""
    workdir = ""
    output_dir = ""
    gui_output_dir = None
    top_output_dir = None
    rosetta_path = ""
    rosetta_binary_dir = "rosetta_source/bin"
    rosetta_binary_name = "mr_protocols.default"
    rosetta_script_dir = "rosetta_source/src/apps/public/electron_density"
    rosetta_database_dir = "rosetta_database"
  }
  read_hhpred {
    number_of_models = 1
    number_of_models_to_skip = 0
    copies_to_extract = None
    only_extract_proper_symmetry = False
  }
  place_model {
    run_place_model = True
    prerefine {
      run_prerefine = True
      number_of_prerefine_models = 1
      number_of_models_in_ensemble = 1
    }
    model_already_placed = True
    model_already_aligned = False
    force_alignment = False
    number_of_output_models = 5
    align_with_sculptor = True
    identity = None
    identity_for_scoring_only = 25
    use_all_plausible_sg = True
    overlap_allowed = 10
    selection_criteria_rot_value = 75
    fast_search_mode = True
    peak_rota_down = 25
    mr_resolution = None
    refine_after_mr = False
    denmod_after_refine = True
    ps_in_rebuild = False
    find_ncs_after_mr = True
    fixed_model = None
    fixed_model_identity = None
    sufficient_number_finished = None
    fixed_ensembles {
      fixed_ensembleID_list = None
      fixed_euler_list = 0 0 0
      fixed_frac_list = 0 0 0
      fixed_frac_list_is_fractional = True
    }
    copies_of_search_model_to_place = None
  }
  rescore_mr {
    run_rescore_mr = True
    nstruct = 5
    relax = False
    include_unrelaxed_in_scoring = False
    align = True
    edit_model = False
    stage_to_rescore = "mr_solution"
  }
  rosetta_rebuild {
    run_rosetta_rebuild = True
    stage_to_rebuild = "rescored_mr_solution"
    max_solutions_to_rebuild = 5
    min_solutions_to_rebuild = 1
    llg_percent_of_max_to_keep = 50
    rosetta_models = 100
    chunk_size = 1
    edit_model = True
    superpose_model = False
  }
  rosetta_rescore {
    run_rosetta_rescore = True
    percentage_to_rescore = 20
    min_solutions_to_rescore = 2
  }
  similarity {
    run_similarity = False
    required_cc = 0.2
    number_of_required_cc = 5
  }
  refine_top_models {
    run_refine_top_models = True
    stage_to_refine = None
    sort_score_type = None
    percent_to_refine = 20
    denmod_after_refine = True
    remove_clashing_residues = None
    clash_cutoff = 1.5
  }
  average_density_top_models {
    run_average_density_top_models = True
    percent_to_average = 100
  }
  relax_top_models {
    run_relax_top_models = True
    stage_to_relax = None
    number_to_relax = 2
    nstruct = 5
    sort_score_type = None
  }
  autobuild_top_models {
    run_autobuild_top_models = True
    number_to_autobuild = 2
    quick = False
    phase_and_build = False
    macro_cycles = None
    remove_residues_on_special_positions = True
    morph = False
    edit_model = True
    use_map_coeffs = True
  }
  setup_repeat_mr_rosetta {
    run_setup_repeat_mr_rosetta = True
    repeats = 1
    template_repeats = 0
    morph_repeats = 0
    number_to_repeat = 1
    acceptable_r = 0.25
    minimum_delta_r = None
  }
  repeat_mr_rosetta {
    run_repeat_mr_rosetta = True
    copies_in_new_search_group = 1
    update_map_coeffs_with_autobuild = True
  }
  rosetta_modeling {
    map_resolution = 3
    map_grid_spacing = 1.5
    map_weight = 1
    map_window = 5
    include_solvation_energy = True
    weights_file = None
  }
  crystal_info {
    resolution = 0
    space_group = None
    chain_type = *PROTEIN DNA RNA
    ncs_copies = Auto
  }
  control {
    job_title = None
    verbose = False
    debug = True
    raise_sorry = False
    dry_run = False
    nproc = 1
    group_run_command = "sh "
    queue_commands = None
    condor_universe = "vanilla"
    add_double_quotes_in_condor = True
    condor = None
    one_subprocess_level = None
    single_run_command = "sh "
    last_process_is_local = True
    background = None
    ignore_errors_in_subprocess = True
    check_run_command = False
    max_wait_time = 1
    check_wait_time = 10
    wait_between_submit_time = 1
    wizard_directory_number = None
    n_dir_max = 100000
    number_to_print = 5
    write_run_directory_to_file = None
    rosetta_command = None
    rosetta_3_6_or_later = None
    fast = None
    generate_fragment_files = True
    resolve_command_list = None
    start_point = *place_model rescore_mr rosetta_rebuild rosetta_rescore \
                  similarity refine_top_models average_density_top_models \
                  relax_top_models autobuild_top_models \
                  setup_repeat_mr_rosetta repeat_mr_rosetta
    stop_point = *place_model rescore_mr rosetta_rebuild rosetta_rescore \
                 similarity refine_top_models average_density_top_models \
                 relax_top_models autobuild_top_models \
                 setup_repeat_mr_rosetta repeat_mr_rosetta
    clean_up = True
    add_id = True
    test_flag_value = None
    real_space_optimize = None
    set_real_space_optimize = None
  }
  non_user_params {
    file_base = None
    print_citations = True
    highest_id = 0
    is_sub_process = True
    dummy_autobuild = False
    dummy_refinement = False
    dummy_rosetta = False
    prerefine_only = False
    skip_clash_guard = True
    correct_special_position_tolerance = None
    ncs_in_refinement = *torsion cartesian None
    comparison_mtz = None
    labin_comparison_mtz = None
    write_local_files = False
    rosetta_fixed_seed = 1571
  }
}

Starting mr_rosetta
Date: Sat Mar  2 12:17:17 2019
Directory: /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine

HOSTNAME: anaconda.lbl.gov
Log file will be mr_rosetta.log
Splitting output to  mr_rosetta.log
Checking rosetta paths:
  rosetta binary: /net/anaconda/raid1/terwill/misc/ROSETTA/rosetta_src_2018.33.60351_bundle/main/source/bin/mr_protocols.python.linuxgccrelease
  database_dir: /net/anaconda/raid1/terwill/misc/ROSETTA/rosetta_src_2018.33.60351_bundle/main/database
  script_dir: /net/anaconda/raid1/terwill/misc/ROSETTA/rosetta_src_2018.33.60351_bundle/main/source/src/apps/public/electron_density

Running mr_rosetta with inputs for Rosetta 3.6 or later

================================================================================
Setting up reflection file and labels
================================================================================
LABIN LINE TO BE USED: FP=FP SIGFP=SIGFP FreeR_flag=FreeR_flag

Identifying plausible values of ncs_copies

Number of residues in unique chains in seq file: 8
Cell volume:  15387.62
Equivalent positions:2

Maximum number of NCS copies based on cell contents:  5
Best guess of number of NCS copies: 4
Estimate of solvent fraction: 0.44
Choosing value of ncs copies leading to solvent content closest to 50%
Possible NCS copies to test: [4]

Methods to be run:

place_model : True
rescore_mr : False
rosetta_rebuild : False
rosetta_rescore : False
similarity : False
refine_top_models : False
average_density_top_models : False
relax_top_models : False
autobuild_top_models : False
setup_repeat_mr_rosetta : False
repeat_mr_rosetta : False


Note: fragment files will be generated by Rosetta
(Requires version 2013wk35 or later of Rosetta)

No alignment file supplied...using search model as is

MODEL IS NOT ALIGNED TO SEQUENCE FILE

================================================================================
Reading model from /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/coords1.pdb
================================================================================

SETTING CRYSTAL SYMMETRY FROM /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/coords1.pdb
CRYST1   25.000   25.000   25.000  90.00 100.00  90.00 P 1 21 1
Wrote dummy alignment to /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/alignment.ali
ALIGNMENT LINES:
## TARGET coord
# hhsearch
scores_from_program: 0 1.00
0 AQLMDMRD
0 AQLMDMRD
--

Trying to adjust the PDB sequence from alignment file:
AQLMDMRD
to match the actual PDB sequence:
QLMDMRD
New alignment line 4 for PDB:
0 -QLMDMRD


Start position of target sequence in sequence file:  0


Fully edited alignment file: /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/edited_align.ali
Matched sequences:  ['AQLMDMRD']
Trying to adjust the PDB sequence from alignment file:
AQLMDMRD
to match the actual PDB sequence:
QLMDMRD

original pdb_file_alignment: AQLMDMRD
new alignment              : -QLMDMRD
NOTE: alignment has been edited

Identity is: 100 percent

Creating information file

Mean B-value for this structure was   20.7

================================================================================
    RUNNING ROSETTA TO PRE-REFINE MODEL

================================================================================

Running prerefine with /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/coords1.pdb as a starting model

No NCS found for /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/coords1.pdb
Log file is /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/coords1_ncs.log

CRYST1   25.000   25.000   25.000  90.00 100.00  90.00 P 1 21 1


ID: 1 : /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/coords1.pdb None None  Space group: 'P 1 21 1 '
Rebuilding MR model (ID:1) by generating 1 rosetta models and choosing
the best using rosetta scoring


#                       mr_rosetta_rebuild
#
# Run automr mr_rosetta_rebuild with rosetta


# Type phenix.doc for help

usage: phenix.mr_rosetta_rebuild mtzfile.mtz sequence.dat [pdbfile.pdb] [labin='FP=FP PHIB=PHIM FOM=FOMM']

Values of all params:
mr_rosetta_rebuild {
  input_files {
    model = "/net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/coords1.pdb"
    map = None
    seq_file = "/net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/seq.dat"
    hhr_files = None
    alignment_files = "/net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/edited_align.ali"
    model_info_file = ""
    fragment_files = None
    fragment_files_chain_list = None
    fragment_files_9_mer_by_chain = None
    fragment_files_3_mer_by_chain = None
    use_dummy_fragment_files = False
    sort_fragment_files = True
  }
  output_files {
    log = "mr_rosetta_rebuild.log"
    params_out = "mr_rosetta_rebuild_params.eff"
  }
  directories {
    temp_dir = "/net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2"
    workdir = "/net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2"
    output_dir = "/net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2"
    gui_output_dir = None
    top_output_dir = "/net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine"
    rosetta_path = "/net/anaconda/raid1/terwill/misc/ROSETTA/rosetta_src_2018.33.60351_bundle"
    rosetta_binary_dir = "main/source/bin"
    rosetta_binary_name = "mr_protocols.python.linuxgccrelease"
    rosetta_script_dir = "main/source/src/apps/public/electron_density"
    rosetta_database_dir = "main/database"
  }
  rosetta_rebuild {
    run_rosetta_rebuild = True
    stage_to_rebuild = "dummy_solution"
    max_solutions_to_rebuild = 5
    min_solutions_to_rebuild = 1
    llg_percent_of_max_to_keep = 50
    rosetta_models = 1
    chunk_size = 1
    edit_model = True
    superpose_model = True
  }
  rosetta_modeling {
    map_resolution = 3
    map_grid_spacing = 1.5
    map_weight = 1
    map_window = 5
    include_solvation_energy = True
    weights_file = ""
  }
  crystal_info {
    resolution = 0
    space_group = "P 1 21 1"
    chain_type = *PROTEIN DNA RNA
    ncs_copies = 4
  }
  control {
    job_title = None
    verbose = False
    debug = True
    raise_sorry = False
    dry_run = False
    nproc = 1
    group_run_command = "sh "
    queue_commands = None
    condor_universe = "vanilla"
    add_double_quotes_in_condor = True
    condor = None
    one_subprocess_level = None
    single_run_command = "sh "
    last_process_is_local = True
    background = None
    ignore_errors_in_subprocess = True
    check_run_command = False
    max_wait_time = 1
    check_wait_time = 10
    wait_between_submit_time = 1
    wizard_directory_number = None
    n_dir_max = 100000
    number_to_print = 5
    write_run_directory_to_file = None
    rosetta_command = None
    rosetta_3_6_or_later = True
    fast = None
    generate_fragment_files = True
    resolve_command_list = None
    start_point = place_model rescore_mr rosetta_rebuild rosetta_rescore \
                  similarity refine_top_models average_density_top_models \
                  relax_top_models autobuild_top_models \
                  setup_repeat_mr_rosetta repeat_mr_rosetta
    stop_point = place_model rescore_mr rosetta_rebuild rosetta_rescore \
                 similarity refine_top_models average_density_top_models \
                 relax_top_models autobuild_top_models \
                 setup_repeat_mr_rosetta repeat_mr_rosetta
    clean_up = True
    add_id = True
    test_flag_value = None
    real_space_optimize = None
    set_real_space_optimize = None
  }
  non_user_params {
    file_base = "coords1"
    print_citations = False
    highest_id = 0
    is_sub_process = True
    dummy_autobuild = False
    dummy_refinement = False
    dummy_rosetta = False
    prerefine_only = False
    skip_clash_guard = True
    correct_special_position_tolerance = None
    ncs_in_refinement = *torsion cartesian None
    comparison_mtz = ""
    labin_comparison_mtz = None
    write_local_files = False
    rosetta_fixed_seed = 1571
  }
}

Starting mr_rosetta_rebuild
Date: Sat Mar  2 12:17:18 2019
Directory: /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine

HOST = anaconda.lbl.gov
HOSTTYPE = x86_64-linux
USER = terwill
PID = 136186
Checking rosetta paths:
  rosetta binary: /net/anaconda/raid1/terwill/misc/ROSETTA/rosetta_src_2018.33.60351_bundle/main/source/bin/mr_protocols.python.linuxgccrelease
  database_dir: /net/anaconda/raid1/terwill/misc/ROSETTA/rosetta_src_2018.33.60351_bundle/main/database
  script_dir: /net/anaconda/raid1/terwill/misc/ROSETTA/rosetta_src_2018.33.60351_bundle/main/source/src/apps/public/electron_density

Running mr_rosetta with inputs for Rosetta 3.6 or later
Sequence rewritten to /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/EDITED_seq.dat :
 > sequence information
AQLMDMRD


Note: fragment files will be generated by Rosetta
(Requires version 2013wk35 or later of Rosetta)

Changing to work directory: /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2


Rebuilding the model /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/coords1.pdb
with rosetta
Running mr_rosetta_rebuild without map information

Generating 1 rebuilt models
================================================================================
Reading model from /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/coords1.pdb
================================================================================
ALIGNMENT LINES:
## TARGET coord
# hhsearch
scores_from_program: 0 1.00
0 AQLMDMRD
0 -QLMDMRD
--

Alignment file matches PDB file
Start position of target sequence in sequence file:  0

Aligned model for comparison: /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/WORK_1/aligned_model.pdb
Log for rebuilding: /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/WORK_1/rebuild.log
Superposing /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/WORK_1/S_COORD_0001.pdb on /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/WORK_1/aligned_model.pdb to yield /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/WORK_1/coords1_S_COORD_0001_su.pdb

HOST = anaconda.lbl.gov
HOSTTYPE = x86_64-linux
USER = terwill
PID = 136186
Date 2019-03-02 Time 12:17:35 PST -0800 (1551557855.11 s)

-------------------------------------------------------------------------------
  phenix.superpose_pdbs: superpose two PDB models.
-------------------------------------------------------------------------------


========================== Complete set of parameters =========================

input {
  pdb_file_name_fixed = /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/WORK_1/aligned_model.pdb
  pdb_file_name_moving = /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/WORK_1/S_COORD_0001.pdb
}
output {
  file_name = /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/WORK_1/coords1_S_COORD_0001_su.pdb
  job_title = None
}
selection_fixed = None
selection_moving = None
selection_default_moving = pepnames and (name ca or name n or name c) and \
                           'altloc " "'
selection_default_fixed = pepnames and (name ca or name n or name c) and \
                          'altloc " "'
reciprocal_matching = False
alignment {
  alignment_style = local *global
  gap_opening_penalty = 1
  gap_extension_penalty = 1
  similarity_matrix = blosum50 dayhoff *identity
}

=========================== Selections and alignment ==========================

Initial number of atoms in fixed set:  60
Initial number of atoms in moving set: 131
Selected number of atoms in fixed set:  21
Selected number of atoms in moving set: 24
Number of matches after alignment:  7


The alignment used in the superposition is shown below.

The sequence identity (fraction of | symbols) is 100.0%
of the aligned length of the fixed molecule sequence.

The sequence similarity (fraction of | and * symbols) is 100.0%
of the aligned length of the fixed molecule sequence.

              12345678901234567890123456789012345678901234567890

fixed         -QLMDMRD
               |||||||
moving        AQLMDMRD


================================== LS fitting =================================

Number of atoms for LS fitting =  9
RMSD between fixed and moving atoms (start): 7.239
Percentile-based spread (start): 7.282
RMSD between fixed and moving atoms (final): 0.827
Percentile-based spread (final): 0.735
Rotation:
r={{ 0.57254,  0.80034, -0.17792},
   { 0.77241, -0.45376,  0.44439},
   { 0.27493, -0.39186, -0.87799}}
Translation:
t={{-10.24335}, { 7.11165}, {65.32042}}

================= Apply transformations and write output model ================

RMSD (all matching atoms) (start): 7.053 (number of atoms: 24)
RMSD (all matching atoms) (final): 3.389 (number of atoms: 24)
Output file name:  /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/WORK_1/coords1_S_COORD_0001_su.pdb

===================================== Done ====================================

Sample rebuilt model:  /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/WORK_1/coords1_S_COORD_0001_su.pdb  SCORE:  6.358
Done with generating rebuilt models
Number of rebuilt models: 1

Writing solutions as csv to /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/results.csv

Saved overall mr_rosetta results in /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/results.pkl

To see details of these results type
    phenix.mr_rosetta mr_rosetta_solutions=/net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/results.pkl  display_solutions=True

Changing to starting directory: /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine

Editing /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/WORK_1/coords1_S_COORD_0001_su.pdb and writing to /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/WORK_1/coords1_S_COORD_0001_su_2_ed.pdb

CRYSTAL SYMMETRY:  CRYST1   25.000   25.000   25.000  90.00 100.00  90.00 P 1 21 1
Total of 37 atoms matched to info_file and 29 not
Rosetta rebuilt solutions: 1
RESULTS OF PREREFINEMENT:
Total models obtained: 1

ID: 2  Model: /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/WORK_1/coords1_S_COORD_0001_su_2_ed.pdb Single chain: /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/WORK_1/coords1_S_COORD_0001_su_2_ed.pdb
Stage: prerefined_rosetta_solution  ROSETTA SCORE:     6.36
Group: None
CRYST1   25.000   25.000   25.000  90.00 100.00  90.00 P 1 21 1
map_coeffs: None labin:None
map: None
Placed model: /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/coords1.pdb
component_solutions: None

Total prerefined rosetta solutions obtained: 1

Rerunning PDB read with prerefined model


MODEL IS ALREADY ALIGNED TO SEQUENCE FILE

================================================================================
Reading model from /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/WORK_1/coords1_S_COORD_0001_su_2_ed.pdb
================================================================================

SETTING CRYSTAL SYMMETRY FROM /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/WORK_1/coords1_S_COORD_0001_su_2_ed.pdb
CRYST1   25.000   25.000   25.000  90.00 100.00  90.00 P 1 21 1
Wrote dummy alignment to /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/alignment.ali
ALIGNMENT LINES:
## TARGET coord
# hhsearch
scores_from_program: 0 1.00
0 AQLMDMRD
0 AQLMDMRD
--

Alignment file matches PDB file
Start position of target sequence in sequence file:  0


Fully edited alignment file: /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/edited_align.ali
Matched sequences:  ['AQLMDMRD']

Creating information file

Mean B-value for this structure was   21.0

================================================================================
    USING SEARCH MODEL AS INPUT (NO MR)
================================================================================

Using search model /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/WORK_1/coords1_S_COORD_0001_su_2_ed.pdb as is

No NCS found for /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/WORK_1/coords1_S_COORD_0001_su_2_ed.pdb
Log file is /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/coords1_S_COORD_0001_su_2_ed_ncs.log


Getting map files by refinement and optional density modification


Running refinement with /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/WORK_1/coords1_S_COORD_0001_su_2_ed.pdb
Refinement results for: /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/coords1_S_COORD_0001_su_2_ed_ref_001.pdb
R:   0.57  Rfree:   0.56
Refinement log file is in /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/coords1_S_COORD_0001_su_2_ed_ref_001.log
Copying data from /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/fobs.mtz to /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/coords1_S_COORD_0001_su_2_ed_data.mtz and setting space_group= P 1 21 1
Selecting arrays from /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/fobs.mtz : ['FP', 'SIGFP'] ['PHIM'] ['FOMM'] ['FreeR_flag'] ['FC']
WARNING: skipping column ['PHIM'] as it could not be merged
Output space group: P 1 21 1 (No. 4)
Columns of data in /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/coords1_S_COORD_0001_su_2_ed_data.mtz:
H K L FP SIGFP FOMM FreeR_flag FC
H H H F Q W I F

Converting mtz to map files


Removing free reflection data from /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/coords1_S_COORD_0001_su_2_ed_ref_001.mtz to yield /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/coords1_S_COORD_0001_su_2_ed_ref_001_nf.mtz
Log file is /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/coords1_S_COORD_0001_su_2_ed_ref_001_nf.log
Converted /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/coords1_S_COORD_0001_su_2_ed_ref_001.mtz to map using labels FP=2FOFCWT PHIB=PH2FOFCWT

================================================================================
  INITIAL SCORING OF MODELS WITH PHASER LLG

================================================================================
Working directory for scoring: /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/SCORE_MR_MODELS_2

Scoring MR model /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/WORK_1/coords1_S_COORD_0001_su_2_ed.pdb
with mtz /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/fobs.mtz

Scoring 1 models
Working directory for scoring: /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/SCORE_MR_MODELS_2

Scoring /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/WORK_1/coords1_S_COORD_0001_su_2_ed.pdb
Score for /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/WORK_1/coords1_S_COORD_0001_su_2_ed.pdb is   4.84

LLG scores:       4.84
Done with scoring this model . Best score =  4.84

List of MR models obtained:

ID: 4 : /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/WORK_1/coords1_S_COORD_0001_su_2_ed.pdb /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/coords1_S_COORD_0001_su_2_ed_ref_001.mtz /net/anaconda/raid1/terwill/misc/atest/command_line_rosetta_quick_tests/test_prerefine/WORK_2/coords1_S_COORD_0001_su_2_ed_ref_001_nf.map  Score:     4.84  Space group: 'P 1 21 1 '


Writing solutions as csv to results.csv

Saved overall mr_rosetta results in results.pkl

To see details of these results type
    phenix.mr_rosetta mr_rosetta_solutions=results.pkl  display_solutions=True


Finishing up mr_rosetta...


Citations for mr_rosetta:

Adams PD, Afonine PV, Bunkoczi G, Chen VB, Davis IW, Echols N, Headd JJ, Hung
LW, Kapral GJ, Grosse-Kunstleve RW, McCoy AJ, Moriarty NW, Oeffner R, Read RJ,
Richardson DC, Richardson JS, Terwilliger TC, Zwart PH. (2010) PHENIX: a
comprehensive Python-based system for macromolecular structure solution. Acta
Cryst. D66:213-221.

DiMaio F, Terwilliger TC, Read RJ, Wlodawer A, Oberdorfer G, Wagner U, Valkov
E, Alon A, Fass D, Axelrod HL, Das D, Vorobiev SM, Iwaï H, Pokkuluri PR, Baker
D. (2011) Improved molecular replacement by density- and energy-guided protein
structure optimization. Nature 473:540-3.

Söding J. (2005) Protein homology detection by HMM-HMM comparison.
Bioinformatics 21:951-60.

All done
