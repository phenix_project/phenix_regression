#!/bin/csh -ef
#$ -cwd
phenix.mr_rosetta seq_file=seq.dat search_models=coords_dimer1.pdb stop=rosetta_rebuild data=fobs.mtz  debug=True is_sub_process=True ncs_copies=2 fixed_model= coords_dimer2.pdb fixed_model_identity=70 rosetta_rebuild.rosetta_models=1
if ( $status )then
  echo "mr_rosetta test_set_up FAILED"
  exit 1
endif

