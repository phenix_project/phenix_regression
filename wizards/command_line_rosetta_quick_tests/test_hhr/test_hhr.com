#!/bin/csh -ef
#$ -cwd
phenix.mr_rosetta data=1dfn.mtz seq_file=1dfn.seq hhr_files=1dfn.hhr number_of_models=1 number_of_models_to_skip=2 start_point=place_model stop_point=place_model run_place_model=False debug=True max_wait_time=1
if ( $status )then
  echo "mr_rosetta test_hhr FAILED"
  exit 1
endif
diff 1zmm_1_chain.pdb MR_ROSETTA_1/WORK_1/1dfn_1//1zmm_1_chain.pdb

