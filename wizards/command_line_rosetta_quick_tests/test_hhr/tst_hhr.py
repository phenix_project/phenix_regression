from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_hhr")
  print("")
  if (not "PHENIX_ROSETTA_PATH" in os.environ) :
     print("No Rosetta installation...skipping this test...OK")
     return

  log_file=os.path.join(os.getcwd(),"test_hhr.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.mr_rosetta data=1dfn.mtz seq_file=1dfn.seq hhr_files=1dfn.hhr number_of_models=1 number_of_models_to_skip=2 start_point=place_model stop_point=place_model run_place_model=False debug=True max_wait_time=1')
  for line in result.stdout_lines:
    print(line, file=log)

  import difflib
  expected=open('1zmm_1_chain.pdb').readlines()
  found=open('MR_ROSETTA_1/WORK_1/1dfn_1//1zmm_1_chain.pdb').readlines()
  for line in difflib.ndiff(expected,found):
    print(line, file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_hhr',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['command_line_rosetta_quick_tests','test_hhr']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
