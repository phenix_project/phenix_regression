#!/bin/csh -ef
#$ -cwd
phenix.mr_rosetta seq_file=seq.dat search_models=coords_tet.pdb is_sub_process=True data=fobs.mtz start=refine_top_models stop=refine_top_models mr_rosetta_solutions=rescored_rebuild.pkl refine_top_models.denmod_after_refine=False  debug=True max_wait_time=1
if ( $status )then
  echo "mr_rosetta test_refine_top_models FAILED"
  exit 1
endif

