

#                       mr_rosetta
#
# Run automr/autobuild/rosetta together


# Type phenix.doc for help

usage: phenix.mr_rosetta mtzfile.mtz sequence.dat [pdbfile.pdb] [labin='FP=FP PHIB=PHIM FOM=FOMM']

Values of all params:
mr_rosetta {
  input_files {
    data = "fobs.mtz"
    data_labels = None
    free_r_data = None
    free_r_labels = None
    remove_free = True
    labin = None
    seq_file = "seq.dat"
    search_models = "coords1.pdb"
    copies_in_search_models = None
    hhr_files = None
    alignment_files = None
    model_info_file = None
    mr_rosetta_solutions = "relax.pkl"
    ids_to_load = None
    map_coeffs = None
    labin_map_coeffs = None
    map = None
    refinement_params = None
    display_solutions = False
    fragment_files = None
    fragment_files_chain_list = None
    fragment_files_9_mer_by_chain = None
    fragment_files_3_mer_by_chain = None
    use_dummy_fragment_files = False
    sort_fragment_files = True
  }
  output_files {
    log = "mr_rosetta.log"
    sort_score_type = None
    params_out = "mr_rosetta_params.eff"
  }
  directories {
    temp_dir = ""
    workdir = ""
    output_dir = ""
    gui_output_dir = None
    top_output_dir = None
    rosetta_path = ""
    rosetta_binary_dir = "rosetta_source/bin"
    rosetta_binary_name = "mr_protocols.default"
    rosetta_script_dir = "rosetta_source/src/apps/public/electron_density"
    rosetta_database_dir = "rosetta_database"
  }
  read_hhpred {
    number_of_models = 1
    number_of_models_to_skip = 0
    copies_to_extract = None
    only_extract_proper_symmetry = False
  }
  place_model {
    run_place_model = True
    prerefine {
      run_prerefine = False
      number_of_prerefine_models = 1000
      number_of_models_in_ensemble = 1
    }
    model_already_placed = False
    model_already_aligned = False
    force_alignment = False
    number_of_output_models = 5
    align_with_sculptor = True
    identity = None
    identity_for_scoring_only = 25
    use_all_plausible_sg = True
    overlap_allowed = 10
    selection_criteria_rot_value = 75
    fast_search_mode = True
    peak_rota_down = 25
    mr_resolution = None
    refine_after_mr = True
    denmod_after_refine = True
    ps_in_rebuild = False
    find_ncs_after_mr = True
    fixed_model = None
    fixed_model_identity = None
    sufficient_number_finished = None
    fixed_ensembles {
      fixed_ensembleID_list = None
      fixed_euler_list = 0 0 0
      fixed_frac_list = 0 0 0
      fixed_frac_list_is_fractional = True
    }
    copies_of_search_model_to_place = None
  }
  rescore_mr {
    run_rescore_mr = True
    nstruct = 5
    relax = False
    include_unrelaxed_in_scoring = False
    align = True
    edit_model = False
    stage_to_rescore = "mr_solution"
  }
  rosetta_rebuild {
    run_rosetta_rebuild = True
    stage_to_rebuild = "rescored_mr_solution"
    max_solutions_to_rebuild = 5
    min_solutions_to_rebuild = 1
    llg_percent_of_max_to_keep = 50
    rosetta_models = 100
    chunk_size = 1
    edit_model = True
    superpose_model = False
  }
  rosetta_rescore {
    run_rosetta_rescore = True
    percentage_to_rescore = 20
    min_solutions_to_rescore = 2
  }
  similarity {
    run_similarity = False
    required_cc = 0.2
    number_of_required_cc = 5
  }
  refine_top_models {
    run_refine_top_models = True
    stage_to_refine = None
    sort_score_type = None
    percent_to_refine = 20
    denmod_after_refine = True
    remove_clashing_residues = None
    clash_cutoff = 1.5
  }
  average_density_top_models {
    run_average_density_top_models = True
    percent_to_average = 100
  }
  relax_top_models {
    run_relax_top_models = True
    stage_to_relax = None
    number_to_relax = 2
    nstruct = 5
    sort_score_type = None
  }
  autobuild_top_models {
    run_autobuild_top_models = True
    number_to_autobuild = 1
    quick = True
    phase_and_build = False
    macro_cycles = 1
    remove_residues_on_special_positions = True
    morph = False
    edit_model = True
    use_map_coeffs = True
  }
  setup_repeat_mr_rosetta {
    run_setup_repeat_mr_rosetta = True
    repeats = 1
    template_repeats = 0
    morph_repeats = 0
    number_to_repeat = 1
    acceptable_r = 0.25
    minimum_delta_r = None
  }
  repeat_mr_rosetta {
    run_repeat_mr_rosetta = True
    copies_in_new_search_group = 1
    update_map_coeffs_with_autobuild = True
  }
  rosetta_modeling {
    map_resolution = 3
    map_grid_spacing = 1.5
    map_weight = 1
    map_window = 5
    include_solvation_energy = True
    weights_file = None
  }
  crystal_info {
    resolution = 0
    space_group = None
    chain_type = *PROTEIN DNA RNA
    ncs_copies = Auto
  }
  control {
    job_title = None
    verbose = False
    debug = True
    raise_sorry = False
    dry_run = False
    nproc = 1
    group_run_command = "sh "
    queue_commands = None
    condor_universe = "vanilla"
    add_double_quotes_in_condor = True
    condor = None
    one_subprocess_level = None
    single_run_command = "sh "
    last_process_is_local = True
    background = None
    ignore_errors_in_subprocess = True
    check_run_command = False
    max_wait_time = 1
    check_wait_time = 10
    wait_between_submit_time = 1
    wizard_directory_number = None
    n_dir_max = 100000
    number_to_print = 5
    write_run_directory_to_file = None
    rosetta_command = None
    rosetta_3_6_or_later = None
    fast = None
    generate_fragment_files = True
    resolve_command_list = None
    start_point = place_model rescore_mr rosetta_rebuild rosetta_rescore \
                  similarity refine_top_models average_density_top_models \
                  relax_top_models *autobuild_top_models \
                  setup_repeat_mr_rosetta repeat_mr_rosetta
    stop_point = place_model rescore_mr rosetta_rebuild rosetta_rescore \
                 similarity refine_top_models average_density_top_models \
                 relax_top_models *autobuild_top_models \
                 setup_repeat_mr_rosetta repeat_mr_rosetta
    clean_up = True
    add_id = True
    test_flag_value = None
    real_space_optimize = None
    set_real_space_optimize = None
  }
  non_user_params {
    file_base = None
    print_citations = True
    highest_id = 0
    is_sub_process = True
    dummy_autobuild = False
    dummy_refinement = False
    dummy_rosetta = False
    prerefine_only = False
    skip_clash_guard = True
    correct_special_position_tolerance = None
    ncs_in_refinement = *torsion cartesian None
    comparison_mtz = None
    labin_comparison_mtz = None
    write_local_files = False
    rosetta_fixed_seed = None
  }
}

Starting mr_rosetta
Date: Sun Feb  9 18:54:11 2020
Directory: /net/anaconda/raid1/terwill/misc/etest/command_line_rosetta_quick_tests/test_autobuild

HOSTNAME: anaconda.lbl.gov
Log file will be mr_rosetta.log
Splitting output to  mr_rosetta.log
Checking rosetta paths:
  rosetta binary: /net/anaconda/raid1/terwill/misc/ROSETTA/rosetta_src_2018.33.60351_bundle/main/source/bin/mr_protocols.python.linuxgccrelease
  database_dir: /net/anaconda/raid1/terwill/misc/ROSETTA/rosetta_src_2018.33.60351_bundle/main/database
  script_dir: /net/anaconda/raid1/terwill/misc/ROSETTA/rosetta_src_2018.33.60351_bundle/main/source/src/apps/public/electron_density

Running mr_rosetta with inputs for Rosetta 3.6 or later

================================================================================
Setting up reflection file and labels
================================================================================
LABIN LINE TO BE USED: FP=FP SIGFP=SIGFP FreeR_flag=FreeR_flag

Identifying plausible values of ncs_copies

Number of residues in unique chains in seq file: 8
Cell volume:  15387.62
Equivalent positions:2

Maximum number of NCS copies based on cell contents:  5
Best guess of number of NCS copies: 4
Estimate of solvent fraction: 0.44
Choosing value of ncs copies leading to solvent content closest to 50%
Possible NCS copies to test: [4]

================================================================================
 LOADING EXISTING SOLUTIONS
================================================================================
Loading solutions from /net/anaconda/raid1/terwill/misc/etest/command_line_rosetta_quick_tests/test_autobuild/relax.pkl
RESULTS:  [
ID: 14  Model: S_COORD_0001_ed_0005_ed.pdb Single chain: S_COORD_0001_ed_0005_ed.pdb
Stage: relaxed_rosetta_solution  MR_LLG:     6.78
Group: None
CRYST1   25.000   25.000   25.000  90.00 100.00  90.00 P 1 21 1
map_coeffs: averaged_map_coeffs.mtz labin:FP=FAVG PHIB=PHIFAVG
map: averaged_map_coeffs_nf.map
Placed model: coords1.pdb
component_solutions: None]
S_COORD_0001_ed_0005_ed.pdb (name) set to /net/anaconda/raid1/terwill/misc/etest/command_line_rosetta_quick_tests/test_autobuild/S_COORD_0001_ed_0005_ed.pdb
S_COORD_0001_ed_0005_ed.pdb (model) set to /net/anaconda/raid1/terwill/misc/etest/command_line_rosetta_quick_tests/test_autobuild/S_COORD_0001_ed_0005_ed.pdb
coords1.pdb (search_model) set to /net/anaconda/raid1/terwill/misc/etest/command_line_rosetta_quick_tests/test_autobuild/coords1.pdb
coords1.pdb (placed_model) set to /net/anaconda/raid1/terwill/misc/etest/command_line_rosetta_quick_tests/test_autobuild/coords1.pdb
averaged_map_coeffs.mtz (map_coeffs) set to /net/anaconda/raid1/terwill/misc/etest/command_line_rosetta_quick_tests/test_autobuild/averaged_map_coeffs.mtz
seq.dat (seq_file) set to /net/anaconda/raid1/terwill/misc/etest/command_line_rosetta_quick_tests/test_autobuild/seq.dat
coords1_data.mtz (data) set to /net/anaconda/raid1/terwill/misc/etest/command_line_rosetta_quick_tests/test_autobuild/coords1_data.mtz
S_COORD_0001_ed_0005_ed.pdb (model_one_copy) set to /net/anaconda/raid1/terwill/misc/etest/command_line_rosetta_quick_tests/test_autobuild/S_COORD_0001_ed_0005_ed.pdb

Loaded 1 previous solutions:
(list is in /net/anaconda/raid1/terwill/misc/etest/command_line_rosetta_quick_tests/test_autobuild/WORK_1/solutions_loaded.log)
SET CRYSTAL SYMMETRY FROM INPUT SOLUTION:  CRYST1   25.000   25.000   25.000  90.00 100.00  90.00 P 1 21 1

Methods to be run:

place_model : False
rescore_mr : False
rosetta_rebuild : False
rosetta_rescore : False
similarity : False
refine_top_models : False
average_density_top_models : False
relax_top_models : False
autobuild_top_models : True
setup_repeat_mr_rosetta : False
repeat_mr_rosetta : False


Note: fragment files will be generated by Rosetta
(Requires version 2013wk35 or later of Rosetta)


================================================================================
   AUTOBUILDING TOP SOLUTIONS

================================================================================
Taking top 1 solutions to autobuild
Candidate: ID: 14  SCORE :   6.78 STAGE: relaxed_rosetta_solution

================================================================================
    RUNNING AUTOBUILDING TO REBUILD A SOLUTION

================================================================================

Running autobuilding with
ID: 14  Model: /net/anaconda/raid1/terwill/misc/etest/command_line_rosetta_quick_tests/test_autobuild/S_COORD_0001_ed_0005_ed.pdb Single chain: /net/anaconda/raid1/terwill/misc/etest/command_line_rosetta_quick_tests/test_autobuild/S_COORD_0001_ed_0005_ed.pdb
Stage: relaxed_rosetta_solution  MR_LLG:     6.78
Group: None
CRYST1   25.000   25.000   25.000  90.00 100.00  90.00 P 1 21 1
map_coeffs: /net/anaconda/raid1/terwill/misc/etest/command_line_rosetta_quick_tests/test_autobuild/averaged_map_coeffs.mtz labin:FP=FAVG PHIB=PHIFAVG
map: averaged_map_coeffs_nf.map
Placed model: /net/anaconda/raid1/terwill/misc/etest/command_line_rosetta_quick_tests/test_autobuild/coords1.pdb
component_solutions: None as a starting model
Using phenix.autobuild for autobuilding
NCS copies obtained from parameters: 4

TEMP directory will be /net/anaconda/raid1/terwill/misc/etest/command_line_rosetta_quick_tests/test_autobuild/WORK_1
Log file for autobuilding will be:  /net/anaconda/raid1/terwill/misc/etest/command_line_rosetta_quick_tests/test_autobuild/WORK_1/autobuild.log
Copied  /net/anaconda/raid1/terwill/misc/etest/command_line_rosetta_quick_tests/test_autobuild/S_COORD_0001_ed_0005_ed.pdb  with editing to working file:  AutoBuild_run_1_/S_COORD_0001_ed_0005_ed_ed.pdb












Summary of output files for Solution 1 from rebuild cycle 2

---  Model (PDB file)  ---
pdb_file: /net/anaconda/raid1/terwill/misc/etest/command_line_rosetta_quick_tests/test_autobuild/WORK_1/AutoBuild_run_1_/overall_best.pdb

---  Model (CIF file)  ---
cif_pdb_file: /net/anaconda/raid1/terwill/misc/etest/command_line_rosetta_quick_tests/test_autobuild/WORK_1/AutoBuild_run_1_/overall_best.cif

---  Refinement log file ---
log_refine: /net/anaconda/raid1/terwill/misc/etest/command_line_rosetta_quick_tests/test_autobuild/WORK_1/AutoBuild_run_1_/overall_best.log_refine

---  Model-building log file ---
log: /net/anaconda/raid1/terwill/misc/etest/command_line_rosetta_quick_tests/test_autobuild/WORK_1/AutoBuild_run_1_/overall_best.log

---  Model-map correlation log file ---
log_eval: /net/anaconda/raid1/terwill/misc/etest/command_line_rosetta_quick_tests/test_autobuild/WORK_1/AutoBuild_run_1_/overall_best.log_eval

---  2FoFc and FoFc map coefficients from refinement 2FOFCWT PH2FOFCWT FOFCWT PHFOFCWT ---
refine_map_coeffs: /net/anaconda/raid1/terwill/misc/etest/command_line_rosetta_quick_tests/test_autobuild/WORK_1/AutoBuild_run_1_/overall_best_refine_map_coeffs.mtz

---  Data for refinement FP SIGFP PHIM FOMM HLAM HLBM HLCM HLDM FreeR_flag ---
refine_data: /net/anaconda/raid1/terwill/misc/etest/command_line_rosetta_quick_tests/test_autobuild/WORK_1/AutoBuild_run_1_/overall_best_refine_data.mtz

---  Density-modified map coefficients FWT PHWT ---
denmod_map_coeffs: /net/anaconda/raid1/terwill/misc/etest/command_line_rosetta_quick_tests/test_autobuild/WORK_1/AutoBuild_run_1_/overall_best_denmod_map_coeffs.mtz
Autobuild model:  /net/anaconda/raid1/terwill/misc/etest/command_line_rosetta_quick_tests/test_autobuild/WORK_1/AutoBuild_run_1_/overall_best.pdb
R= 0.43
map coeffs:  /net/anaconda/raid1/terwill/misc/etest/command_line_rosetta_quick_tests/test_autobuild/WORK_1/AutoBuild_run_1_/overall_best_denmod_map_coeffs.mtz

================================================================================
RESULTS OF AUTOBUILDING:
================================================================================

ID: 15
R/Rfree:   0.43 /   0.40
MODEL: /net/anaconda/raid1/terwill/misc/etest/command_line_rosetta_quick_tests/test_autobuild/WORK_1/AutoBuild_run_1_/overall_best.pdb
MAP COEFFS /net/anaconda/raid1/terwill/misc/etest/command_line_rosetta_quick_tests/test_autobuild/WORK_1/AutoBuild_run_1_/overall_best_denmod_map_coeffs.mtz

Writing solutions as csv to results.csv

Saved overall mr_rosetta results in results.pkl

To see details of these results type
    phenix.mr_rosetta mr_rosetta_solutions=results.pkl  display_solutions=True


Best solution: ------------------------

ID: 15
R/Rfree:   0.43 /   0.40
MODEL: /net/anaconda/raid1/terwill/misc/etest/command_line_rosetta_quick_tests/test_autobuild/WORK_1/AutoBuild_run_1_/overall_best.pdb
MAP COEFFS /net/anaconda/raid1/terwill/misc/etest/command_line_rosetta_quick_tests/test_autobuild/WORK_1/AutoBuild_run_1_/overall_best_denmod_map_coeffs.mtz

---------------------------------------

Finishing up mr_rosetta...


Citations for mr_rosetta:

DiMaio F, Terwilliger TC, Read RJ, Wlodawer A, Oberdorfer G, Wagner U, Valkov
E, Alon A, Fass D, Axelrod HL, Das D, Vorobiev SM, Iwaï H, Pokkuluri PR, Baker
D. (2011) Improved molecular replacement by density- and energy-guided protein
structure optimization. Nature 473:540-3.

Liebschner D, Afonine PV, Baker ML, Bunkóczi G, Chen VB, Croll TI, Hintze B,
Hung LW, Jain S, McCoy AJ, Moriarty NW, Oeffner RD, Poon BK, Prisant MG, Read
RJ, Richardson JS, Richardson DC, Sammito MD, Sobolev OV, Stockwell DH,
Terwilliger TC, Urzhumtsev AG, Videau LL, Williams CJ, Adams PD. (2019)
Macromolecular structure determination using X-rays, neutrons and electrons:
recent developments in Phenix. Acta Cryst. D75:861-877.

Söding J. (2005) Protein homology detection by HMM-HMM comparison.
Bioinformatics 21:951-60.

All done
