from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_autobuild")
  print("")
  if (not "PHENIX_ROSETTA_PATH" in os.environ) :
     print("No Rosetta installation...skipping this test...OK")
     return

  log_file=os.path.join(os.getcwd(),"test_autobuild.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.mr_rosetta seq_file=seq.dat search_models=coords1.pdb is_sub_process=True data=fobs.mtz start=autobuild_top_models stop=autobuild_top_models mr_rosetta_solutions=relax.pkl autobuild_top_models.macro_cycles=1 autobuild_top_models.number_to_autobuild=1 autobuild_top_models.quick=true debug=True max_wait_time=1')
  for line in result.stdout_lines:
    print(line, file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_autobuild',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['command_line_rosetta_quick_tests','test_autobuild']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
