#!/bin/csh -ef
#$ -cwd
phenix.mr_rosetta seq_file=seq.dat search_models=coords1.pdb is_sub_process=True data=fobs.mtz start=autobuild_top_models stop=autobuild_top_models mr_rosetta_solutions=relax.pkl autobuild_top_models.macro_cycles=1 autobuild_top_models.number_to_autobuild=1 autobuild_top_models.quick=true debug=True max_wait_time=1
if ( $status )then
  echo "mr_rosetta test_autobuild FAILED"
  exit 1
endif

