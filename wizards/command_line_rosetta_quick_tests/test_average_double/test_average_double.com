#!/bin/csh -ef
#$ -cwd
phenix.mr_rosetta seq_file=seq.dat search_models=coords_tet.pdb is_sub_process=True data=fobs.mtz start=average_density_top_models stop=average_density_top_models mr_rosetta_solutions=refined_top_models.pkl   debug=True max_wait_time=1
if ( $status )then
  echo "mr_rosetta test_average FAILED"
  exit 1
endif

