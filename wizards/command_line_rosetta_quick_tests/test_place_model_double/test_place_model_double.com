#!/bin/csh -ef
#$ -cwd
phenix.mr_rosetta alignment_files=coords.ali seq_file=seq.dat search_models=coords_tet.pdb start=place_model stop=place_model data=fobs.mtz is_sub_process=True already_placed=true refine_after_mr=False  debug=True max_wait_time=1
if ( $status )then
  echo "mr_rosetta test_place_model FAILED"
  exit 1
endif

