#!/bin/csh -ef
#$ -cwd
phenix.mr_rosetta seq_file=seq.dat search_models=coords1.pdb start=place_model stop=place_model data=fobs.mtz ps_in_rebuild=true is_sub_process=True debug=True max_wait_time=1 ncs_copies=1
if ( $status )then
  echo "mr_rosetta test_place_model_ps FAILED"
  exit 1
endif
phenix.get_cc_mtz_pdb AutoMR_run_1_/coord_mr.1.pdb WORK_1/AutoBuild_run_1_/overall_best_denmod_map_coeffs.mtz
if ( $status )then
  echo "mr_rosetta test_place_model_ps FAILED"
  exit 1
endif

