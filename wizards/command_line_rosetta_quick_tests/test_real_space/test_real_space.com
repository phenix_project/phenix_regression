#!/bin/csh -ef
#$ -cwd
phenix.mr_rosetta set_real_space_optimize=True map_coeffs=chainI.mtz search_models=short.pdb seq_file=seq.dat labin_map_coeffs='FP=FWT PHIB=PHWT' rosetta_rebuild.rosetta_models=1 relax_top_models.nstruct=1 rosetta_fixed_seed=771001
if ( $status )then
  echo "mr_rosetta test_real_space FAILED"
  exit 1
endif

