#!/bin/csh -f
phenix.mr_rosetta set_real_space_optimize=True map_coeffs=chainI.mtz search_models=short.pdb seq_file=model_iterative_ss_assign.seq labin_map_coeffs='FP=FWT PHIB=PHWT' rosetta_rebuild.rosetta_models=1 relax_top_models.nstruct=1 rosetta_fixed_seed=771001
