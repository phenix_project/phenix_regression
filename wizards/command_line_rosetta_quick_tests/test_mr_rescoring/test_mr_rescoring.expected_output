

#                       mr_rosetta
#
# Run automr/autobuild/rosetta together


# Type phenix.doc for help

usage: phenix.mr_rosetta mtzfile.mtz sequence.dat [pdbfile.pdb] [labin='FP=FP PHIB=PHIM FOM=FOMM']

Values of all params:
mr_rosetta {
  input_files {
    data = "fobs.mtz"
    data_labels = None
    free_r_data = None
    free_r_labels = None
    remove_free = True
    labin = None
    seq_file = "seq.dat"
    search_models = "coords1.pdb"
    copies_in_search_models = None
    hhr_files = None
    alignment_files = None
    model_info_file = None
    mr_rosetta_solutions = "place_model.pkl"
    ids_to_load = None
    map_coeffs = None
    labin_map_coeffs = None
    map = None
    refinement_params = None
    display_solutions = False
    fragment_files = None
    fragment_files_chain_list = None
    fragment_files_9_mer_by_chain = None
    fragment_files_3_mer_by_chain = None
    use_dummy_fragment_files = False
    sort_fragment_files = True
  }
  output_files {
    log = "mr_rosetta.log"
    sort_score_type = None
    params_out = "mr_rosetta_params.eff"
  }
  directories {
    temp_dir = ""
    workdir = ""
    output_dir = ""
    gui_output_dir = None
    top_output_dir = None
    rosetta_path = ""
    rosetta_binary_dir = "rosetta_source/bin"
    rosetta_binary_name = "mr_protocols.default"
    rosetta_script_dir = "rosetta_source/src/apps/public/electron_density"
    rosetta_database_dir = "rosetta_database"
  }
  read_hhpred {
    number_of_models = 1
    number_of_models_to_skip = 0
    copies_to_extract = None
    only_extract_proper_symmetry = False
  }
  place_model {
    run_place_model = True
    prerefine {
      run_prerefine = False
      number_of_prerefine_models = 1000
      number_of_models_in_ensemble = 1
    }
    model_already_placed = False
    model_already_aligned = False
    force_alignment = False
    number_of_output_models = 5
    align_with_sculptor = True
    identity = None
    identity_for_scoring_only = 25
    use_all_plausible_sg = True
    overlap_allowed = 10
    selection_criteria_rot_value = 75
    fast_search_mode = True
    peak_rota_down = 25
    mr_resolution = None
    refine_after_mr = True
    denmod_after_refine = True
    ps_in_rebuild = False
    find_ncs_after_mr = True
    fixed_model = None
    fixed_model_identity = None
    sufficient_number_finished = None
    fixed_ensembles {
      fixed_ensembleID_list = None
      fixed_euler_list = 0 0 0
      fixed_frac_list = 0 0 0
      fixed_frac_list_is_fractional = True
    }
    copies_of_search_model_to_place = None
  }
  rescore_mr {
    run_rescore_mr = True
    nstruct = 5
    relax = False
    include_unrelaxed_in_scoring = False
    align = True
    edit_model = False
    stage_to_rescore = "mr_solution"
  }
  rosetta_rebuild {
    run_rosetta_rebuild = True
    stage_to_rebuild = "rescored_mr_solution"
    max_solutions_to_rebuild = 5
    min_solutions_to_rebuild = 1
    llg_percent_of_max_to_keep = 50
    rosetta_models = 100
    chunk_size = 1
    edit_model = True
    superpose_model = False
  }
  rosetta_rescore {
    run_rosetta_rescore = True
    percentage_to_rescore = 20
    min_solutions_to_rescore = 2
  }
  similarity {
    run_similarity = False
    required_cc = 0.2
    number_of_required_cc = 5
  }
  refine_top_models {
    run_refine_top_models = True
    stage_to_refine = None
    sort_score_type = None
    percent_to_refine = 20
    denmod_after_refine = True
    remove_clashing_residues = None
    clash_cutoff = 1.5
  }
  average_density_top_models {
    run_average_density_top_models = True
    percent_to_average = 100
  }
  relax_top_models {
    run_relax_top_models = True
    stage_to_relax = None
    number_to_relax = 2
    nstruct = 5
    sort_score_type = None
  }
  autobuild_top_models {
    run_autobuild_top_models = True
    number_to_autobuild = 2
    quick = False
    phase_and_build = False
    macro_cycles = None
    remove_residues_on_special_positions = True
    morph = False
    edit_model = True
    use_map_coeffs = True
  }
  setup_repeat_mr_rosetta {
    run_setup_repeat_mr_rosetta = True
    repeats = 1
    template_repeats = 0
    morph_repeats = 0
    number_to_repeat = 1
    acceptable_r = 0.25
    minimum_delta_r = None
  }
  repeat_mr_rosetta {
    run_repeat_mr_rosetta = True
    copies_in_new_search_group = 1
    update_map_coeffs_with_autobuild = True
  }
  rosetta_modeling {
    map_resolution = 3
    map_grid_spacing = 1.5
    map_weight = 1
    map_window = 5
    include_solvation_energy = True
    weights_file = None
  }
  crystal_info {
    resolution = 0
    space_group = None
    chain_type = *PROTEIN DNA RNA
    ncs_copies = Auto
  }
  control {
    job_title = None
    verbose = False
    debug = True
    raise_sorry = False
    dry_run = False
    nproc = 1
    group_run_command = "sh "
    queue_commands = None
    condor_universe = "vanilla"
    add_double_quotes_in_condor = True
    condor = None
    one_subprocess_level = None
    single_run_command = "sh "
    last_process_is_local = True
    background = None
    ignore_errors_in_subprocess = True
    check_run_command = False
    max_wait_time = 1
    check_wait_time = 10
    wait_between_submit_time = 1
    wizard_directory_number = None
    n_dir_max = 100000
    number_to_print = 5
    write_run_directory_to_file = None
    rosetta_command = None
    rosetta_3_6_or_later = None
    fast = None
    generate_fragment_files = True
    resolve_command_list = None
    start_point = place_model *rescore_mr rosetta_rebuild rosetta_rescore \
                  similarity refine_top_models average_density_top_models \
                  relax_top_models autobuild_top_models \
                  setup_repeat_mr_rosetta repeat_mr_rosetta
    stop_point = place_model *rescore_mr rosetta_rebuild rosetta_rescore \
                 similarity refine_top_models average_density_top_models \
                 relax_top_models autobuild_top_models \
                 setup_repeat_mr_rosetta repeat_mr_rosetta
    clean_up = True
    add_id = True
    test_flag_value = None
    real_space_optimize = None
    set_real_space_optimize = None
  }
  non_user_params {
    file_base = None
    print_citations = True
    highest_id = 0
    is_sub_process = True
    dummy_autobuild = False
    dummy_refinement = False
    dummy_rosetta = False
    prerefine_only = False
    skip_clash_guard = True
    correct_special_position_tolerance = None
    ncs_in_refinement = *torsion cartesian None
    comparison_mtz = None
    labin_comparison_mtz = None
    write_local_files = False
    rosetta_fixed_seed = None
  }
}

Starting mr_rosetta
Date: Fri Dec  1 19:31:56 2017
Directory: /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring

HOSTNAME: anaconda.lbl.gov
Log file will be mr_rosetta.log
Splitting output to  mr_rosetta.log
Checking rosetta paths:
  rosetta binary: /net/chevy/raid1/terwill/rosetta/rosetta_bin_linux_2015.22.57859_bundle/main/source/bin/mr_protocols.linuxgccrelease
  database_dir: /net/chevy/raid1/terwill/rosetta/rosetta_bin_linux_2015.22.57859_bundle/main/database
  script_dir: /net/chevy/raid1/terwill/rosetta/rosetta_bin_linux_2015.22.57859_bundle/main/source/src/apps/public/electron_density

Running mr_rosetta with inputs for Rosetta 3.6 or later

================================================================================
Setting up reflection file and labels
================================================================================
LABIN LINE TO BE USED: FP=FP SIGFP=SIGFP FreeR_flag=FreeR_flag

Identifying plausible values of ncs_copies

Number of residues in unique chains in seq file: 8
Cell volume:  15387.62
Equivalent positions:2

Maximum number of NCS copies based on cell contents:  5
Best guess of number of NCS copies: 4
Estimate of solvent fraction: 0.44
Choosing value of ncs copies leading to solvent content closest to 50%
Possible NCS copies to test: [4]

================================================================================
 LOADING EXISTING SOLUTIONS
================================================================================
Loading solutions from /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/place_model.pkl
RESULTS:  [
ID: 1  Model: coords1.pdb Single chain: coords1.pdb
Stage: scored_solution  MR_LLG:    12.84
Group: None
CRYST1   25.000   25.000   25.000  90.00 100.00  90.00 P 1 21 1
map_coeffs: None labin:FP=FWT PHIB=PHWT
map: None
Placed model: None
component_solutions: None,
ID: 2  Model: coords1.pdb Single chain: coords1.pdb
Stage: mr_solution  MR_LLG:    12.84
Group: None
CRYST1   25.000   25.000   25.000  90.00 100.00  90.00 P 1 21 1
map_coeffs: coords1_ref_001_map_coeffs.mtz labin:FP=2FOFCWT PHIB=PH2FOFCWT
map: coords1_ref_001_map_coeffs_nf.map
Placed model: coords1.pdb
component_solutions: None]
coords1.pdb (name) set to /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/coords1.pdb
coords1.pdb (model) set to /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/coords1.pdb
coords1.pdb (model_one_copy) set to /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/coords1.pdb
coords1.pdb (name) set to /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/coords1.pdb
coords1.pdb (model) set to /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/coords1.pdb
coords1.pdb (search_model) set to /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/coords1.pdb
coords1.pdb (placed_model) set to /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/coords1.pdb
coords1_ref_001_map_coeffs_nf.map (map) set to /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/coords1_ref_001_map_coeffs_nf.map
coords1_ref_001_map_coeffs.mtz (map_coeffs) set to /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/coords1_ref_001_map_coeffs.mtz
edited_align.ali (alignment_file) set to /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/edited_align.ali
seq.dat (seq_file) set to /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/seq.dat
coords1_data.mtz (data) set to /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/coords1_data.mtz
coords1.pdb (model_one_copy) set to /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/coords1.pdb

Loaded 2 previous solutions:
(list is in /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/WORK_1/solutions_loaded.log)
SET CRYSTAL SYMMETRY FROM INPUT SOLUTION:  CRYST1   25.000   25.000   25.000  90.00 100.00  90.00 P 1 21 1

Methods to be run:

place_model : False
rescore_mr : True
rosetta_rebuild : False
rosetta_rescore : False
similarity : False
refine_top_models : False
average_density_top_models : False
relax_top_models : False
autobuild_top_models : False
setup_repeat_mr_rosetta : False
repeat_mr_rosetta : False


Note: fragment files will be generated by Rosetta
(Requires version 2013wk35 or later of Rosetta)


================================================================================
   RESCORING WITH LLG RECALCULATION

================================================================================
Rescoring one model with LLG

Model to rescore:
ID: 2  Model: /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/coords1.pdb Single chain: /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/coords1.pdb
Stage: mr_solution  MR_LLG:    12.84
Group: None
CRYST1   25.000   25.000   25.000  90.00 100.00  90.00 P 1 21 1
map_coeffs: /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/coords1_ref_001_map_coeffs.mtz labin:FP=2FOFCWT PHIB=PH2FOFCWT
map: /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/coords1_ref_001_map_coeffs_nf.map
Placed model: /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/coords1.pdb
component_solutions: None


#                       mr_rescoring
#
# Run automr mr_rescoring with optional rosetta rebuilding


# Type phenix.doc for help

usage: phenix.mr_rescoring mtzfile.mtz sequence.dat [pdbfile.pdb] [labin='FP=FP PHIB=PHIM FOM=FOMM']

Values of all params:
mr_rescoring {
  input_files {
    model = None
    map = ""
    mtz_in = "/net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/fobs.mtz"
    labin = "FP=FP SIGFP=SIGFP"
    mr_rosetta_solutions = "/net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/RESCORE_MR_1/solution_for_rescore.pkl"
    seq_file = "/net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/seq.dat"
    hhr_files = None
    alignment_files = None
    model_info_file = None
  }
  output_files {
    log = "mr_rescoring.log"
    params_out = "mr_rescoring_params.eff"
  }
  directories {
    temp_dir = "/net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/RESCORE_MR_1"
    workdir = "/net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/RESCORE_MR_1"
    output_dir = "/net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/RESCORE_MR_1"
    gui_output_dir = None
    top_output_dir = "/net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring"
    rosetta_path = "/net/chevy/raid1/terwill/rosetta/rosetta_bin_linux_2015.22.57859_bundle"
    rosetta_binary_dir = "main/source/bin"
    rosetta_binary_name = "mr_protocols.linuxgccrelease"
    rosetta_script_dir = "main/source/src/apps/public/electron_density"
    rosetta_database_dir = "main/database"
  }
  place_model {
    run_place_model = True
    prerefine {
      run_prerefine = False
      number_of_prerefine_models = 1000
      number_of_models_in_ensemble = 1
    }
    model_already_placed = False
    model_already_aligned = False
    force_alignment = False
    number_of_output_models = 5
    align_with_sculptor = True
    identity = None
    identity_for_scoring_only = 25
    use_all_plausible_sg = True
    overlap_allowed = 10
    selection_criteria_rot_value = 75
    fast_search_mode = True
    peak_rota_down = 25
    mr_resolution = None
    refine_after_mr = True
    denmod_after_refine = True
    ps_in_rebuild = False
    find_ncs_after_mr = True
    fixed_model = ""
    fixed_model_identity = None
    sufficient_number_finished = None
    fixed_ensembles {
      fixed_ensembleID_list = None
      fixed_euler_list = 0 0 0
      fixed_frac_list = 0 0 0
      fixed_frac_list_is_fractional = True
    }
    copies_of_search_model_to_place = None
  }
  rescore_mr {
    run_rescore_mr = True
    nstruct = 1
    relax = False
    include_unrelaxed_in_scoring = False
    align = True
    edit_model = False
    stage_to_rescore = "mr_solution"
  }
  rosetta_modeling {
    map_resolution = 3
    map_grid_spacing = 1.5
    map_weight = 1
    map_window = 5
    include_solvation_energy = True
    weights_file = ""
  }
  crystal_info {
    resolution = 0
    space_group = "P 1 21 1"
    chain_type = *PROTEIN DNA RNA
    ncs_copies = 4
  }
  control {
    job_title = None
    verbose = False
    debug = True
    raise_sorry = False
    dry_run = False
    nproc = 1
    group_run_command = "sh "
    queue_commands = None
    condor_universe = "vanilla"
    add_double_quotes_in_condor = True
    condor = None
    one_subprocess_level = None
    single_run_command = "sh "
    last_process_is_local = True
    background = None
    ignore_errors_in_subprocess = True
    check_run_command = False
    max_wait_time = 1
    check_wait_time = 10
    wait_between_submit_time = 1
    wizard_directory_number = None
    n_dir_max = 100000
    number_to_print = 5
    write_run_directory_to_file = None
    rosetta_command = None
    rosetta_3_6_or_later = True
    fast = None
    generate_fragment_files = True
    resolve_command_list = None
    start_point = place_model rescore_mr rosetta_rebuild rosetta_rescore \
                  similarity refine_top_models average_density_top_models \
                  relax_top_models autobuild_top_models \
                  setup_repeat_mr_rosetta repeat_mr_rosetta
    stop_point = place_model rescore_mr rosetta_rebuild rosetta_rescore \
                 similarity refine_top_models average_density_top_models \
                 relax_top_models autobuild_top_models \
                 setup_repeat_mr_rosetta repeat_mr_rosetta
    clean_up = True
    add_id = True
    test_flag_value = None
    real_space_optimize = None
    set_real_space_optimize = None
  }
  non_user_params {
    file_base = None
    print_citations = False
    highest_id = 2
    is_sub_process = True
    dummy_autobuild = False
    dummy_refinement = False
    dummy_rosetta = False
    prerefine_only = False
    skip_clash_guard = True
    correct_special_position_tolerance = None
    ncs_in_refinement = *torsion cartesian None
    comparison_mtz = ""
    labin_comparison_mtz = None
    write_local_files = False
    rosetta_fixed_seed = None
  }
}

Starting mr_rescoring
Date: Fri Dec  1 19:31:57 2017
Directory: /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring

HOST = anaconda.lbl.gov
HOSTTYPE = x86_64-linux
USER = terwill
PID = 60145
Checking rosetta paths:
  rosetta binary: /net/chevy/raid1/terwill/rosetta/rosetta_bin_linux_2015.22.57859_bundle/main/source/bin/mr_protocols.linuxgccrelease
  database_dir: /net/chevy/raid1/terwill/rosetta/rosetta_bin_linux_2015.22.57859_bundle/main/database
  script_dir: /net/chevy/raid1/terwill/rosetta/rosetta_bin_linux_2015.22.57859_bundle/main/source/src/apps/public/electron_density

Running mr_rosetta with inputs for Rosetta 3.6 or later
Sequence rewritten to /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/RESCORE_MR_1/EDITED_seq.dat :
 > sequence information
AQLMDMRD


================================================================================
 LOADING EXISTING SOLUTIONS
================================================================================
Loading solutions from /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/RESCORE_MR_1/solution_for_rescore.pkl
RESULTS:
ID: 2  Model: /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/coords1.pdb Single chain: /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/coords1.pdb
Stage: mr_solution  MR_LLG:    12.84
Group: None
CRYST1   25.000   25.000   25.000  90.00 100.00  90.00 P 1 21 1
map_coeffs: /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/coords1_ref_001_map_coeffs.mtz labin:FP=2FOFCWT PHIB=PH2FOFCWT
map: /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/coords1_ref_001_map_coeffs_nf.map
Placed model: /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/coords1.pdb
component_solutions: None

Loaded 1 previous solutions:
(list is in /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/RESCORE_MR_1/solutions_loaded.log)
SET CRYSTAL SYMMETRY FROM INPUT SOLUTION:  CRYST1   25.000   25.000   25.000  90.00 100.00  90.00 P 1 21 1
Changing to work directory: /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/RESCORE_MR_1


Using information from saved model
ID: 2  Model: /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/coords1.pdb Single chain: /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/coords1.pdb
Stage: mr_solution  MR_LLG:    12.84
Group: None
CRYST1   25.000   25.000   25.000  90.00 100.00  90.00 P 1 21 1
map_coeffs: /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/coords1_ref_001_map_coeffs.mtz labin:FP=2FOFCWT PHIB=PH2FOFCWT
map: /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/coords1_ref_001_map_coeffs_nf.map
Placed model: /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/coords1.pdb
component_solutions: None
SET CRYSTAL SYMMETRY FROM INPUT SOLUTION:  CRYST1   25.000   25.000   25.000  90.00 100.00  90.00 P 1 21 1

Scoring the model /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/coords1.pdb
Not relaxing solution...just mr_rescoring input model

Scoring 1 models
Working directory for scoring: /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/RESCORE_MR_1/WORK_1

Scoring /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/coords1.pdb
Score for /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/coords1.pdb is  73.00

LLG scores:      73.00
Done with scoring this model . Best score = 73.00

Writing solutions as csv to /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/RESCORE_MR_1/results.csv

Saved overall mr_rosetta results in /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/RESCORE_MR_1/results.pkl

To see details of these results type
    phenix.mr_rosetta mr_rosetta_solutions=/net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/RESCORE_MR_1/results.pkl  display_solutions=True

Changing to starting directory: /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring

Added rescored solution to results:
ID: 4  Model: /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/coords1.pdb Single chain: /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/coords1.pdb
Stage: rescored_mr_solution  MR_LLG:    73.00
Group: None
CRYST1   25.000   25.000   25.000  90.00 100.00  90.00 P 1 21 1
map_coeffs: /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/coords1_ref_001_map_coeffs.mtz labin:FP=2FOFCWT PHIB=PH2FOFCWT
map: /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/coords1_ref_001_map_coeffs_nf.map
Placed model: /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_mr_rescoring/coords1.pdb
component_solutions: None

Writing solutions as csv to results.csv

Saved overall mr_rosetta results in results.pkl

To see details of these results type
    phenix.mr_rosetta mr_rosetta_solutions=results.pkl  display_solutions=True


Finishing up mr_rosetta...


Citations for mr_rosetta:

Adams PD, Afonine PV, Bunkoczi G, Chen VB, Davis IW, Echols N, Headd JJ, Hung
LW, Kapral GJ, Grosse-Kunstleve RW, McCoy AJ, Moriarty NW, Oeffner R, Read RJ,
Richardson DC, Richardson JS, Terwilliger TC, Zwart PH. (2010) PHENIX: a
comprehensive Python-based system for macromolecular structure solution. Acta
Cryst. D66:213-221.

DiMaio F, Terwilliger TC, Read RJ, Wlodawer A, Oberdorfer G, Wagner U, Valkov
E, Alon A, Fass D, Axelrod HL, Das D, Vorobiev SM, Iwaï H, Pokkuluri PR, Baker
D. (2011) Improved molecular replacement by density- and energy-guided protein
structure optimization. Nature 473:540-3.

Söding J. (2005) Protein homology detection by HMM-HMM comparison.
Bioinformatics 21:951-60.

All done
