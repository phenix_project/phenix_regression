

#                       mr_rosetta
#
# Run automr/autobuild/rosetta together


# Type phenix.doc for help

usage: phenix.mr_rosetta mtzfile.mtz sequence.dat [pdbfile.pdb] [labin='FP=FP PHIB=PHIM FOM=FOMM']

Values of all params:
mr_rosetta {
  input_files {
    data = "fobs.mtz"
    data_labels = None
    free_r_data = None
    free_r_labels = None
    remove_free = True
    labin = None
    seq_file = "seq.dat"
    search_models = "coords1.pdb"
    copies_in_search_models = None
    hhr_files = None
    alignment_files = None
    model_info_file = None
    mr_rosetta_solutions = None
    ids_to_load = None
    map_coeffs = None
    labin_map_coeffs = None
    map = None
    refinement_params = None
    display_solutions = False
    fragment_files = None
    fragment_files_chain_list = None
    fragment_files_9_mer_by_chain = None
    fragment_files_3_mer_by_chain = None
    use_dummy_fragment_files = False
    sort_fragment_files = True
  }
  output_files {
    log = "mr_rosetta.log"
    sort_score_type = None
    params_out = "mr_rosetta_params.eff"
  }
  directories {
    temp_dir = ""
    workdir = ""
    output_dir = ""
    gui_output_dir = None
    top_output_dir = None
    rosetta_path = ""
    rosetta_binary_dir = "rosetta_source/bin"
    rosetta_binary_name = "mr_protocols.default"
    rosetta_script_dir = "rosetta_source/src/apps/public/electron_density"
    rosetta_database_dir = "rosetta_database"
  }
  read_hhpred {
    number_of_models = 1
    number_of_models_to_skip = 0
    copies_to_extract = None
    only_extract_proper_symmetry = False
  }
  place_model {
    run_place_model = True
    prerefine {
      run_prerefine = False
      number_of_prerefine_models = 1000
      number_of_models_in_ensemble = 1
    }
    model_already_placed = True
    model_already_aligned = False
    force_alignment = False
    number_of_output_models = 5
    align_with_sculptor = True
    identity = None
    identity_for_scoring_only = 25
    use_all_plausible_sg = True
    overlap_allowed = 10
    selection_criteria_rot_value = 75
    fast_search_mode = True
    peak_rota_down = 25
    mr_resolution = None
    refine_after_mr = False
    denmod_after_refine = True
    ps_in_rebuild = False
    find_ncs_after_mr = True
    fixed_model = None
    fixed_model_identity = None
    sufficient_number_finished = None
    fixed_ensembles {
      fixed_ensembleID_list = None
      fixed_euler_list = 0 0 0
      fixed_frac_list = 0 0 0
      fixed_frac_list_is_fractional = True
    }
    copies_of_search_model_to_place = None
  }
  rescore_mr {
    run_rescore_mr = True
    nstruct = 5
    relax = False
    include_unrelaxed_in_scoring = False
    align = True
    edit_model = False
    stage_to_rescore = "mr_solution"
  }
  rosetta_rebuild {
    run_rosetta_rebuild = True
    stage_to_rebuild = "rescored_mr_solution"
    max_solutions_to_rebuild = 5
    min_solutions_to_rebuild = 1
    llg_percent_of_max_to_keep = 50
    rosetta_models = 100
    chunk_size = 1
    edit_model = True
    superpose_model = False
  }
  rosetta_rescore {
    run_rosetta_rescore = True
    percentage_to_rescore = 20
    min_solutions_to_rescore = 2
  }
  similarity {
    run_similarity = False
    required_cc = 0.2
    number_of_required_cc = 5
  }
  refine_top_models {
    run_refine_top_models = True
    stage_to_refine = None
    sort_score_type = None
    percent_to_refine = 20
    denmod_after_refine = True
    remove_clashing_residues = None
    clash_cutoff = 1.5
  }
  average_density_top_models {
    run_average_density_top_models = True
    percent_to_average = 100
  }
  relax_top_models {
    run_relax_top_models = True
    stage_to_relax = None
    number_to_relax = 2
    nstruct = 5
    sort_score_type = None
  }
  autobuild_top_models {
    run_autobuild_top_models = True
    number_to_autobuild = 2
    quick = False
    phase_and_build = False
    macro_cycles = None
    remove_residues_on_special_positions = True
    morph = False
    edit_model = True
    use_map_coeffs = True
  }
  setup_repeat_mr_rosetta {
    run_setup_repeat_mr_rosetta = True
    repeats = 1
    template_repeats = 0
    morph_repeats = 0
    number_to_repeat = 1
    acceptable_r = 0.25
    minimum_delta_r = None
  }
  repeat_mr_rosetta {
    run_repeat_mr_rosetta = True
    copies_in_new_search_group = 1
    update_map_coeffs_with_autobuild = True
  }
  rosetta_modeling {
    map_resolution = 3
    map_grid_spacing = 1.5
    map_weight = 1
    map_window = 5
    include_solvation_energy = True
    weights_file = None
  }
  crystal_info {
    resolution = 0
    space_group = None
    chain_type = *PROTEIN DNA RNA
    ncs_copies = 1
  }
  control {
    job_title = None
    verbose = False
    debug = True
    raise_sorry = False
    dry_run = False
    nproc = 1
    group_run_command = "sh "
    queue_commands = None
    condor_universe = "vanilla"
    add_double_quotes_in_condor = True
    condor = None
    one_subprocess_level = None
    single_run_command = "sh "
    last_process_is_local = True
    background = None
    ignore_errors_in_subprocess = True
    check_run_command = False
    max_wait_time = 1
    check_wait_time = 10
    wait_between_submit_time = 1
    wizard_directory_number = None
    n_dir_max = 100000
    number_to_print = 5
    write_run_directory_to_file = None
    rosetta_command = None
    rosetta_3_6_or_later = None
    fast = None
    generate_fragment_files = True
    resolve_command_list = None
    start_point = *place_model rescore_mr rosetta_rebuild rosetta_rescore \
                  similarity refine_top_models average_density_top_models \
                  relax_top_models autobuild_top_models \
                  setup_repeat_mr_rosetta repeat_mr_rosetta
    stop_point = *place_model rescore_mr rosetta_rebuild rosetta_rescore \
                 similarity refine_top_models average_density_top_models \
                 relax_top_models autobuild_top_models \
                 setup_repeat_mr_rosetta repeat_mr_rosetta
    clean_up = True
    add_id = True
    test_flag_value = None
    real_space_optimize = None
    set_real_space_optimize = None
  }
  non_user_params {
    file_base = None
    print_citations = True
    highest_id = 0
    is_sub_process = True
    dummy_autobuild = False
    dummy_refinement = False
    dummy_rosetta = False
    prerefine_only = False
    skip_clash_guard = True
    correct_special_position_tolerance = None
    ncs_in_refinement = *torsion cartesian None
    comparison_mtz = None
    labin_comparison_mtz = None
    write_local_files = False
    rosetta_fixed_seed = None
  }
}

Starting mr_rosetta
Date: Fri Dec  1 19:32:23 2017
Directory: /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_place_model

HOSTNAME: anaconda.lbl.gov
Log file will be mr_rosetta.log
Splitting output to  mr_rosetta.log
Checking rosetta paths:
  rosetta binary: /net/chevy/raid1/terwill/rosetta/rosetta_bin_linux_2015.22.57859_bundle/main/source/bin/mr_protocols.linuxgccrelease
  database_dir: /net/chevy/raid1/terwill/rosetta/rosetta_bin_linux_2015.22.57859_bundle/main/database
  script_dir: /net/chevy/raid1/terwill/rosetta/rosetta_bin_linux_2015.22.57859_bundle/main/source/src/apps/public/electron_density

Running mr_rosetta with inputs for Rosetta 3.6 or later

================================================================================
Setting up reflection file and labels
================================================================================
LABIN LINE TO BE USED: FP=FP SIGFP=SIGFP FreeR_flag=FreeR_flag

Methods to be run:

place_model : True
rescore_mr : False
rosetta_rebuild : False
rosetta_rescore : False
similarity : False
refine_top_models : False
average_density_top_models : False
relax_top_models : False
autobuild_top_models : False
setup_repeat_mr_rosetta : False
repeat_mr_rosetta : False


Note: fragment files will be generated by Rosetta
(Requires version 2013wk35 or later of Rosetta)

No alignment file supplied...using search model as is

MODEL IS NOT ALIGNED TO SEQUENCE FILE

================================================================================
Reading model from /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_place_model/coords1.pdb
================================================================================

SETTING CRYSTAL SYMMETRY FROM /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_place_model/coords1.pdb
CRYST1   25.000   25.000   25.000  90.00 100.00  90.00 P 1 21 1
Wrote dummy alignment to /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_place_model/WORK_1/alignment.ali
ALIGNMENT LINES:
## TARGET coord
# hhsearch
scores_from_program: 0 1.00
0 AQLMDMRD
0 AQLMDMRD
--

Trying to adjust the PDB sequence from alignment file:
AQLMDMRD
to match the actual PDB sequence:
QLMDMRD
New alignment line 4 for PDB:
0 -QLMDMRD


Start position of target sequence in sequence file:  0


Fully edited alignment file: /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_place_model/WORK_1/edited_align.ali
Matched sequences:  ['AQLMDMRD']
Trying to adjust the PDB sequence from alignment file:
AQLMDMRD
to match the actual PDB sequence:
QLMDMRD

original pdb_file_alignment: AQLMDMRD
new alignment              : -QLMDMRD
NOTE: alignment has been edited

Identity is: 100 percent

Creating information file

Mean B-value for this structure was   20.7

================================================================================
    USING SEARCH MODEL AS INPUT (NO MR)
================================================================================

Using search model /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_place_model/coords1.pdb as is

No NCS found for /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_place_model/coords1.pdb
Log file is /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_place_model/WORK_1/coords1_ncs.log


Getting map files by refinement and optional density modification


Running refinement with /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_place_model/coords1.pdb
Refinement results for: /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_place_model/WORK_1/coords1_ref_001.pdb
R:   0.36  Rfree:   0.29
Refinement log file is in /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_place_model/WORK_1/coords1_ref_001.log
Copying data from /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_place_model/fobs.mtz to /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_place_model/WORK_1/coords1_data.mtz and setting space_group= P 1 21 1
Selecting arrays from /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_place_model/fobs.mtz : ['FP', 'SIGFP'] ['PHIM'] ['FOMM'] ['FreeR_flag'] ['FC']
WARNING: skipping column ['PHIM'] as it could not be merged
Output space group: P 1 21 1 (No. 4)
Columns of data in /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_place_model/WORK_1/coords1_data.mtz:
H K L FP SIGFP FOMM FreeR_flag FC
H H H F Q W I F

Converting mtz to map files


Removing free reflection data from /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_place_model/WORK_1/coords1_ref_001.mtz to yield /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_place_model/WORK_1/coords1_ref_001_nf.mtz
Log file is /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_place_model/WORK_1/coords1_ref_001_nf.log
Converted /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_place_model/WORK_1/coords1_ref_001.mtz to map using labels FP=2FOFCWT PHIB=PH2FOFCWT

================================================================================
  INITIAL SCORING OF MODELS WITH PHASER LLG

================================================================================
Working directory for scoring: /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_place_model/SCORE_MR_MODELS_1

Scoring MR model /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_place_model/coords1.pdb
with mtz /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_place_model/fobs.mtz

Scoring 1 models
Working directory for scoring: /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_place_model/SCORE_MR_MODELS_1

Scoring /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_place_model/coords1.pdb
Score for /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_place_model/coords1.pdb is  73.00

LLG scores:      73.00
Done with scoring this model . Best score = 73.00

List of MR models obtained:

ID: 2 : /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_place_model/coords1.pdb /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_place_model/WORK_1/coords1_ref_001.mtz /net/anaconda/raid1/terwill/misc/itest/command_line_rosetta_quick_tests/test_place_model/WORK_1/coords1_ref_001_nf.map  Score:    73.00  Space group: 'P 1 21 1 '


Writing solutions as csv to results.csv

Saved overall mr_rosetta results in results.pkl

To see details of these results type
    phenix.mr_rosetta mr_rosetta_solutions=results.pkl  display_solutions=True


Finishing up mr_rosetta...


Citations for mr_rosetta:

Adams PD, Afonine PV, Bunkoczi G, Chen VB, Davis IW, Echols N, Headd JJ, Hung
LW, Kapral GJ, Grosse-Kunstleve RW, McCoy AJ, Moriarty NW, Oeffner R, Read RJ,
Richardson DC, Richardson JS, Terwilliger TC, Zwart PH. (2010) PHENIX: a
comprehensive Python-based system for macromolecular structure solution. Acta
Cryst. D66:213-221.

DiMaio F, Terwilliger TC, Read RJ, Wlodawer A, Oberdorfer G, Wagner U, Valkov
E, Alon A, Fass D, Axelrod HL, Das D, Vorobiev SM, Iwaï H, Pokkuluri PR, Baker
D. (2011) Improved molecular replacement by density- and energy-guided protein
structure optimization. Nature 473:540-3.

Söding J. (2005) Protein homology detection by HMM-HMM comparison.
Bioinformatics 21:951-60.

All done
