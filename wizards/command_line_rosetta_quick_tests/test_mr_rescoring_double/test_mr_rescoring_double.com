#!/bin/csh -ef
#$ -cwd
phenix.mr_rosetta seq_file=seq.dat search_models=coords_tet.pdb start=rescore_mr stop=rescore_mr mr_rosetta_solutions=place_model.pkl is_sub_process=True data=fobs.mtz  debug=True max_wait_time=1
if ( $status )then
  echo "mr_rosetta test_mr_rescoring FAILED"
  exit 1
endif

