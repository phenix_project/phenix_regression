#!/bin/csh -f
phenix.autobuild generate_hl_if_missing=true ncs_file=sites_mlt.pdb data=random_0.7_mlt.mtz seq_file=seq_mlt.dat model=part_mlt.pdb ncycle_refine=1 rebuild_in_place=False resolution=3.0 n_cycle_rebuild_max=1 build_outside=False number_of_models=0 n_cycle_build=1 refine_with_ncs=True number_of_parallel_models=1 start_chains_list=92
