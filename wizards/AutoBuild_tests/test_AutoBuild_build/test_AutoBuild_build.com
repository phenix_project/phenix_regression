#!/bin/csh -f
phenix.autobuild super_quick=True perfect.mtz input_labels="FP SIGFP PHIC FOM" map_file=perfect.mtz input_map_labels="FP PHIC FOM" seq.dat resolution=3.0 solvent_fraction=.6  n_cycle_rebuild_max=2 n_cycle_build_max=2 start_chains_list=92
