#!/bin/csh -f
phenix.autobuild super_quick=True perfect.mtz input_labels="FP SIGFP" map_file=perfect.mtz input_map_labels="FP PHIC FOM" seq.dat super_quick=True resolution=5.0 solvent_fraction=.6 model=coords.pdb rebuild_in_place=True n_cycle_rebuild_min=1 n_cycle_rebuild_max=1 multiple_models=True   multiple_models_number=1 multiple_models_group_number=1 highest_resno=200
