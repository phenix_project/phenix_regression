from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os,sys
  print("Test script for test_AutoBuild_truncate")
  print("")
  log_file=os.path.join(os.getcwd(),"test_AutoBuild_truncate.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  local_log_file='test_AutoBuild_truncate.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''data.mtz coords.pdb number_of_parallel_models=1 refine=false quick=true find_ncs=false resolve_command_list="'side_avg_min 0.5'"''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)


  for file_name in ['AutoBuild_run_1_/TEMP0/cycle_best.log']:
    for line in open(file_name).readlines():
      if line.lower().find("truncate")>-1:
        print(line, file=log)


  local_log_file='test_AutoBuild_truncate.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''data.mtz coords.pdb number_of_parallel_models=1 refine=false quick=true find_ncs=false truncate_missing_side_chains=True resolve_command_list="'side_avg_min 0.5'"''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)


  for file_name in ['AutoBuild_run_2_/TEMP0/cycle_best.log']:
    for line in open(file_name).readlines():
      if line.lower().find("truncate")>-1:
        print(line, file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_AutoBuild_truncate',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['AutoBuild_tests','test_AutoBuild_truncate']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
