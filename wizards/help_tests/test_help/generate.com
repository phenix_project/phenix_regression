#!/bin/csh
#
setenv SOLVETMPDIR /var/tmp
#
#
#  generate 1 mad dataset
#
phenix.solve <<EOD

!  GENERATE 1 MAD DATASET:
verbose
@solve.setup
percent_error 0.5
coordinatefile coords.pdb
iranseed -199753
logfile generate.logfile
solvefile generate.prt

mad_atom se                              ! define the scattering factors...
!
lambda 1
label set 1 with 2 se atoms, lambda 1
wavelength 0.9782             ! wavelength value
fprimv_mad  -6              ! f' value at this wavelength
fprprv_mad  5
atomname se
xyz      2.0486      1.4955      1.6212
occ 1.
bvalue 20.
atomname se
xyz      1.9121      1.6770      1.5176
occ 1.
bvalue 20.
!
!
GENERATE_MAD                            ! generate the MAD dataset now.
!
EOD
