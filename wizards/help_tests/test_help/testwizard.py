from __future__ import division
from __future__ import print_function
# testwizard.py
import os,sys,string
import imp
from phenix.foundation.PhenixUtils import GetWizardParentDirs
import libtbx.load_env
args=sys.argv[1:]

"""
NOTE: for case where command_line test is run, the run of the Wizard
already was done in testwizard.csh (and most of the next code is
never used). For running from a script, it is
run here.
"""

directories = GetWizardParentDirs()
wizards = []
import time
time.sleep(1)
#
for dd in directories:
    if not os.path.exists(dd): continue
    for wiz in os.listdir(dd):
      if wiz.endswith('.py') and wiz.find('__'):
        wizards.append(wiz[:-3])
module = args[0]
name = args[0]

phenix_regression = libtbx.env.find_in_repositories("phenix_regression")
if phenix_regression is None:
  raise AssertionError ('Sorry, cannot find phenix_regression')

Facts_file='Facts.list'
if not os.path.isfile(Facts_file):
  raise AssertionError ('Sorry, cannot find the file: '+str(Facts_file))
input=None
for line in open(Facts_file).readlines():  # get name of original Facts file
  if line and string.split(line) and len(string.split(line))>1:
    input=string.split(line)[1]
    break
if not input: raise AssertionError('Need a Facts file for testwizard...')

File=os.path.join(phenix_regression,'wizards',str(input)+'.results')
CurrentFile=str(input)+'.current'

try:
    file, path, desc = imp.find_module(module, directories)
    module = imp.load_source(module, path, file)
except ImportError as e:
    print("ImportError:", e)
    print("usage:  python <path>/runWizard.py <wizard-name> ")
    outl = "choices:"
    for wiz in wizards:
      outl += "\n\t%s" % wiz
    print(outl)
    sys.exit(1)

if 'restart' in args or 'start_all_over' in args:
    restart=True
else:
    restart=False

if not 'command_line' in args:
  print("RUNNING ",module," with file: ",input)
  File=os.path.join(phenix_regression,'wizards',str(input)+'.results')
  if not os.path.isfile(File):
    raise AssertionError('Need a comparison results file : '+str(File))
  from phenix.command_line.runWizard import runWizard
  myrunWizard=runWizard(name,module,restart=restart,Facts_file=Facts_file)
  ok=myrunWizard.run()
  if not ok:
    print("Sorry, the try of running ",module," couldn't start the PDS server")
    sys.exit(2)
  print("DONE")
  Facts=myrunWizard.Wizard.Facts  # now Facts has all results!
else:
  File=os.path.join(phenix_regression,'wizards',
    string.replace(str(input),"csh","inp")+'.results')
  if not os.path.isfile(File):
    raise AssertionError('Need a comparison results file : '+str(File))
  import pickle,os
  if name=='AutoMR':  # special case (AutoMR goes on into AutoBuild)
   print("Reading AutoMR Facts from AutoBuild...")
   name='AutoBuild'
  facts_file="PDS/"+str(name)+"_run_1_/"+str(name)+"_Facts_...dict"
  print("Reading Facts from PDS...",facts_file)
  if os.path.isfile(facts_file):
    Facts=pickle.load(open(facts_file))
  else:
    print("Sorry,...no Facts file found in ",facts_file)
    sys.exit(1)

print("\nSUMMARY:\n")
if 'top_solution' in Facts:   # top_solution may have some info
      same=True
      failed=False
      similar=True
      good_results={}
      try:
       for line in open(File).readlines():
        try:
         good_results[string.split(line)[0]  ]=string.split(line)[1]
        except KeyboardInterrupt: raise
        except Exception:
          pass
      except KeyboardInterrupt: raise
      except Exception:
        print("No file with standard results...")
      print("good results would be: ",good_results)
      top_solution=Facts['top_solution']
      f=open(CurrentFile,'w')
      f1=open('summary.dat','w')
      for key in top_solution.keys():
       if not key in ['ha_sites','solution_number','starting_cc',
         'chains']:
        line=str(key)+": "+str(top_solution[key])
        f.write(str(key)+" "+str(top_solution[key])+"\n")
        if key in good_results.keys():
          line+=" standard: "+str(good_results[key])
          if str(top_solution[key])!=str(good_results[key]):
            same=False
          if str(top_solution[key])=='None' and str(good_results[key])!='None':
            similar=False
            failed=True
          else:
            try:
              good=string.atof(good_results[key])
              top=string.atof(top_solution[key])
              if top < 0.75 * good:
                similar=False
              if top < 0.01 * good and not key in ['residues_placed','placed','cc_overall']:
                similar=False
                failed=True
            except KeyboardInterrupt: raise
            except Exception:
              pass
        else:
          pass # 070508
        print(line)
        f1.write(line+"\n")
      if failed:
        line="Failed run...different from the ones in the standard file"
        print(line)
        sys.exit(1)
      elif same:
        line="These results are identical to the ones in the standard file"
      elif similar:
        line="These results are similar to the ones in the standard file"
      else:
        line="These results are different than the ones in the standard file"
      f1.write(line+"\n")
      print(line)
elif 'composite_omit_mtz' in Facts.keys():
      f=open(CurrentFile,'w')
      f1=open('summary.dat','w')
      for key in ['composite_omit_mtz']:
        line=key+": "+str(Facts[key])+"\n"
        f1.write(line)
      line="This composite omit run finished ok..."
      f1.write(line+"\n")
elif 'final_mult_model_list' in Facts.keys():
      f=open(CurrentFile,'w')
      f1=open('summary.dat','w')
      for key in ['final_mult_model_list']:
        line=key+": "+str(Facts[key])+"\n"
        f1.write(line)
      line="This multiple-model run finished ok..."
      f1.write(line+"\n")
else:
  print("No solution in Facts...failed")
  sys.exit(1)
