from __future__ import division
from __future__ import print_function
import sys, os

skip=['resolve_tests','resolve_pattern_tests','solve_tests']
def related(args,key):
  r=False
  for arg in args:
    if key.find(arg)>-1:
      r=True
  return r
def run(args):
  import libtbx.load_env
  phenix_regression = libtbx.env.find_in_repositories("phenix_regression")

  dd=os.path.join(phenix_regression,'wizards')
  all=os.listdir(dd)
  dirs=[]
  for x in all:
    xx=os.path.join(phenix_regression,'wizards',x)
    if os.path.isdir(xx):
      if x.find('tests')>-1 and (not x in skip):
         dirs.append(x)

  all_test_dict={}
  for x in dirs:
    tgz_list=[]
    xx=os.path.join(phenix_regression,'wizards',x)
    files=os.listdir(xx)
    for file in files:
      if file.endswith(".tgz"):
        all_test_dict[file.replace(".tgz","")]='test_'+x.replace('_tests','')
  keys=all_test_dict.keys()
  keys.sort()
  for key in keys:
    if (args and (related(args,key) or related(args,all_test_dict[key]))) \
       or (not args):
      print("phenix_regression.wizards.%s %s" %(all_test_dict[key],key))

if (__name__ == "__main__"):
  print("\nphenix_regression.wizards.list "+\
     "[chars_to_find]")
  print("\nFind a wizard regression test. Available tests:\n")
  run(args=sys.argv[1:])
