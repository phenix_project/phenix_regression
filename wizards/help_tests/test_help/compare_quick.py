from __future__ import division
import sys
args=sys.argv[1:]

def get_warnings(lines):
  warnings=[]
  for line in lines:
    if line.startswith("WARNING") or line.startswith("UNKNOWN"):
      warnings.append(line)
  return "".join(warnings)

if len(args)<2:
  raise AssertionError("Need 2 files for compare_quick.py")
if len(args)>2:
  fraction=args[2]
else:
  fraction=0.25

lines0=open(args[0]).readlines()
lines1=open(args[1]).readlines()
bigger=len(lines0)
if len(lines1)>bigger: bigger=len(lines1)
text=get_warnings(lines0)
if text or float(len(lines0)-len(lines1) )> fraction * float(bigger):
  raise AssertionError("\n"+text+\
     "Lines in %s (%d) differ from %s (%d) "%(
     args[0],len(lines0),args[1],len(lines1)))

missing1=0
for line in lines0:
  if not line in lines1:
    missing1+=1
missing0=0
for line in lines1:
  if not line in lines0:
     missing0+=1
if float(missing1+missing0) > fraction * float(bigger):
  raise AssertionError("\n"+text+\
    "\n\nToo many changed lines in %s  compared to %s (%d) "%(
     args[0],args[1],missing0+missing1))
