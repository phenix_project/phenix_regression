from __future__ import division
from __future__ import print_function
import sys
if len(sys.argv)<3:
  print("compare_file_lengths.py requires 2 file names")
  sys.exit(1)
if len(sys.argv)>3:
  ratio_max=float(sys.argv[3])
else:
  ratio_max=0.2
text1=open(sys.argv[1]).read().replace(" ","")
text2=open(sys.argv[2]).read().replace(" ","")
l1=len(text1)
l2=len(text2)
dd=l1-l2
if dd<0: dd=-dd
ratio=float(dd)/(0.5*float(l1+l2))
if ratio > ratio_max and dd > 200:
  raise AssertionError("FILE LENGTHS DIFFER: "+" ".join(sys.argv[1:]))
