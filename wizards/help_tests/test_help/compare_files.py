from __future__ import division
from __future__ import print_function

#  purpose: identify lines with numbers that are not similar in log files
#  use: run diff -b log1 log2 > diff.dat and run this on diff.dat
#  TCT 013109

import difflib
import sys
from six.moves import cStringIO as StringIO
from copy import deepcopy
diff_function = getattr(difflib, "unified_diff", difflib.ndiff)
from libtbx.test_utils import approx_equal
from libtbx.utils import null_out


# Hardwire special cases for solve/resolve tests START
base_eps=0.01  # anything under 0.01 is ok always
small_eps_value=1.  # any delta under small_eps is ok if abs(value)<small_eps_value
small_eps=0.05
base_ratio_eps=0.001 # any delta under 0.001 of the value is ok

#   general testing defaults
if 'tst' in sys.argv:
  ignore_list=[ ]
  one_percent_list=[]
  two_percent_list=[]
  ten_percent_list=[]
  twenty_percent_list=[]
  thirty_percent_list=[]
  one_integer_list=[]

# ----------- resolve defaults ---------------------
elif len(sys.argv)<3 or sys.argv[2]=='resolve':
 ignore_list=[
    "          size    ",
    "u+s u s : ", # just a timing
    "source_line",
    "Mon ","Tue ","Wed ","Thu ","Fri ","Sat ","Sun ",
    "SOLVE_RESOLVE_MALLOC_MAX",
    "-prime ",
    "object 1 and NC symmetry",
    "and as input by user is,",
    "WITH SITES:",
    "From ",
    "of atoms in au",  # changed printout; ignore it
    "ignored as more than",  # from trace_chain
    "NOTE: secondary sort",  # from trace_chain
    "version",
    "date",
    "to apply to Fc",
    "Maximum at ",  # test_offset can differ by 0.50
    "Applying phase shifts",  # test_offset can differ by 0.50
    "Dataset ", # test_main_map these differ
    "   X  ",  # get_histograms the values differ
    "! a1 a2 a3", # get_histograms the values differ
      "Date",
      "PHENIX",
    "in secondary structure",
    "alpha-helix residues",
    " SCORE  ", # trace_analyze
    " OK density  ", # trace_analyze
    "File name ", #test_ligand_volume
    "    Atoms placed    ", # test_ligand_rot_trans
    "   499   ", # test_planar_rings
    'Model assembly cycle', # test_build_se
    ' offset is ', # test_highest_mask
    "malloc",
    "GLN     92", # test_ligand_large
    "LEU     93", # test_ligand_large
    "    N    ", # test_ligand_large
    "     92   ", # test_ligand_relat
    "0.132  0.261  0.192 ", # test_multi_b
    "CC for fragment from atoms ", #reuse_chain
    "Max prob", # test_loop_lib
    "Testing cluster", # test_loop_lib
    "rotation about X",  # changed values of rotations 2010-05-10
    "|   1.00 |   ", # 2010-05-14
    "Ligand center for site", # find_all_ligands
    "Current residues built", #2010-06-06 build_model
    "CONNECT", # assign_sequence 2010-06-27
    "/phenix", # 2010-07-07 may or may not contain "-" in path name
    "Placed : ", # phase_and_build
    "max_wait_time", # all
    "MR_ROSETTA_1/WORK_1/WORK_1/S_COORDS_0001_ed_ed.pdb", # mr_rosetta
    "rescored_rosetta_solution", #mr_rosetta
    "mr_llg : 11.04", #mr_rosetta
     "WORK_1/WORK_1/S_COORDS_0001_ed.pdb is", # mr_rosetta
     "1         2      0.21        0.26", # change_sg
     "S_COORDS_0001.pdb  SCORE :", # mr_rosetta
     "Sample relaxed model with score of", # mr_rosetta
     "S_MORP_0001_ed.pdb is", # mr_rosetta
     " 1         2      0.", # change_sg
     "SCORE :  9.69251283211  SCORE TYPE :  MR_LLG",# mr_rosetta_ncs
     "R/Rfree :   0.47 /   0.63", # mr_rosetta_ncs
     "Total of 14 residues+ligand+waters written out and 0 skipped",# change_sg
     "Stage : relaxed_rosetta_solution  MR_LLG :     9.69  NCS copies : 2", #ncs
     "mr_llg : 9.69251283211",# ncs
     "PID ", # all
     "and at position 1 in PDB file  residue Q 92", # mr_rosetta
     "ID of chain/residue where these apply :   'R'  'A'      92  98  ",#mr_rosetta
     "SOLU 6DIM ENSE ensemble_1 EULER", # change_sg
     "#1  ensemble_1 -6.3143",# change_sg
     "ID : ",#test_mr_rosetta_double_pre
     " rosetta_score :",#test_mr_rosetta_double_pre
     "SCORE :  1.789",#test_mr_rosetta_double_pre
     " mr_llg : 1.789",#test_mr_rosetta_double_pre
     "SCORE :  4.35",#test_mr_rosetta_double_pre
     " mr_llg : 4.35",#test_mr_rosetta_double_pre
     "SCORE :  5.11007957464",#test_mr_rosetta_double_pre
     "mr_llg : 5.11",#test_mr_rosetta_double_pre
     "mr_llg : 3.58",#test_mr_rosetta_double_pre
     "SCORE :  3.58",#test_mr_rosetta_double_pre
     "SCORE :  6.44450815102  SCORE TYPE :  MR_LLG", # test_mr_rosetta
     "7.1    0.09   0.70   0.71     0.33", # memory
     " 7.1    0.09   0.70   0.71     0.22", # memory
     "  3.1    0.34   0.58   0.62     0.31", # memory
     "  7.1    0.79    0.81      32   0.22    0.22", #memory
     "xyz  fractional coords      0.1784 -0.1856 -0.0646", # high_pass
     "ENSE ensemble_1 - EULER    9.414    2.454  350.396 - FRAC   -0.001   -0.001   -0.002", # test_automr_remark
     "0.0    0.01    0.01     543  -0.06   -0.03     102   0.00   -0.00     645", #test_denmod_pdb_memory
     "0.0    0.01    0.00     543  -0.08   -0.03     102  -0.00   -0.00     645"
, #test_denmod_pdb_memory
     "0.0    0.88    0.90     948   0.55    0.62     141   0.84    0.88    1089", # test_resolve_memory
     "5.4    0.04    0.12      70  -0.09   -0.11      22   0.01    0.05      92",# test_resolve_denmod_pdb_memory
     "7.1    0.09   0.71   0.71     0.33       0.10        0.71         18", # test_resolve_memory
     "3.8   -0.03   -0.01      96   0.12   -0.07      16  -0.01   -0.02     112", # test_resolve_denmod_pdb_memory
     "90      0.63    0.73        13      0.60    0.73         3.01",# test_resolve_denmod_pdb_memory
     "33      0.86    0.79         9      0.00    0.00         4.02",# test_resolve_denmod_pdb_memory
     "3.0    0.11   -0.01     115  -0.47   -0.38      15   0.05   -0.07     130",# test_resolve_denmod_pdb_memory
     "3.0    0.09   0.35   0.36     0.20       0.20        0.36         15",# test_resolve_denmod_pdb_memory
     "3.2    0.79    0.91     160   0.74    0.97      23   0.78    0.92     183",# test_resolve_denmod_pdb_memory
     "5.4    0.06    0.12      70  -0.09   -0.12      22   0.02    0.06      92",# test_resolve_denmod_pdb_memory
     "4.3   -0.09   -0.07      84  -0.06    0.04      17  -0.09   -0.05     101",# test_resolve_denmod_pdb_memory
     "4.3   -0.07   -0.05      84  -0.06    0.09      17  -0.07   -0.03     101", # test_resolve_denmod_pdb_memory
     "3.0    0.13    0.01     115  -0.33   -0.36      15   0.08   -0.06     130", # test_resolve_denmod_pdb_memory
     "5.4    0.05    0.12      70  -0.09   -0.11      22   0.01    0.05      92", # test_resolve_denmod_pdb_memory
     "0.0    0.01    0.00     543  -0.10   -0.03     102  -0.00   -0.00     645", # test_resolve_denmod_pdb_memory
     "4.5    0.84    0.86     127   0.21    0.38      28   0.73    0.80     155", # test_resolve_memory

     "3.0    0.08   0.33   0.34     0.19       0.20        0.34         16", # test_resolve_denmod_pdb_memory
     "3.0    0.63    0.78     115   0.50    0.88      16   0.61    0.79     131", # test_resolve_denmod_pdb_memory
     "3.2    0.03    0.02     160   0.08    0.13      24   0.03    0.04     184", # test_resolve_denmod_pdb_memory
     "5.4    0.04    0.11      70  -0.18   -0.11      22  -0.01    0.05      92", # test_resolve_denmod_pdb_memory
     "33      0.88    0.80         9      0.00    0.00         4.02", # test_resolve_denmod_pdb_memory
  "10.27    0.99    1.03        14", #test_resolve_denmod_pdb_memory
  " 63      0.70    0.74        10      0.34    0.74         3.52", # test_zero_hl
  "4.5    0.90   0.00   0.90     0.00       0.98        0.32         36",# test_masks
  "4.3   -0.09   -0.07      84  -0.11    0.04      18  -0.10   -0.05     102", # test_resolve_denmod_pdb_memory
  "2.7    0.89    0.92     299   0.76    0.96      34   0.88    0.92     333",# test_resolve_memory
     "*     BIAS RATIO :  4.26                                              *", # test_masks
     "ALL :    0.90   0.02   0.90    -0.02       0.99       -0.97         47",# test_masks
    "ALL :    0.90   0.01   0.90     0.02       0.98        0.90        153",# test_masks
     "6.4    0.90   0.01   0.90     0.05       0.98        0.90         27",# test_masks
     "*     BIAS RATIO :  2.69                                              *",#test_masks
     "63      0.69    0.69        10      0.54    0.69         3.52",# test_resolve_memory
      "3.6    0.81    0.83     149   0.64    0.72      22   0.79    0.82     171",# test_zero_hl
      "ENSE ensemble_1 - EULER  179.865    0.000    0.000 - FRAC    1.002    0.997   -1.001",# test_change_sg
      "ALL :    0.90   0.01   0.90     0.09       0.99        0.90         47",# test_masks
      "ALL :    0.90   0.01   0.90     0.03       0.98        0.90        200",# test_masks
      "6.4    0.90   0.01   0.90    -0.02       0.98       -1.00         32", # test_masks
      "*     CORRECTED OVERALL FIGURE OF MERIT OF PHASING :   0.90",# test_masks
    "Convergence before iteration limit  50 ",#test_change_sg
     "Rotational Deviation of NCS related structure",#test_change_sg
    "#   3  +0.7 +2.4 -1.4  +7.5 +17.7 -1.1  19.3  +0.501 +0.478 -0.04",#test_change_sg
    "#   2  +0.7 -1.8 -1.4  +11.5 -17.9 -0.7  -21.2",#test_change_sg
    "Range  Resolution 17.321 - 2.500  :",#test_change_sg
    "-7161.866             -6718.875               442.992",#test_change_sg
     "with final rotation     20.0 -7.4 2.0",#test_change_sg
    "-7249.690             -6718.903               530.787",#test_change_sg
    "Maximum EpsFac   1.5",#test_change_sg
   " 0.5  *  6", #test_change_sg
    "3.8   -0.03   -0.01      96   0.12   -0.05      16  -0.01   -0.02     112",#test_resolve_denmod_memory
    "-7189.327             -6717.559               471.768", # test_change_sg
    "center_orth     27.9638    -9.7513   -13.0742", # test_find_ncs
    "xyz         -0.452          -0.333          -0.885       1.00", # test_dos
    "xyz          0.412           0.002           0.016       0.88", # test_dos
    "xyz          0.452           0.333           0.885       1.00", # test_dos
    "xyz         -0.412          -0.002          -0.016       0.88", # test_dos
    "FINAL SITES :  -0.45195466670057816  -0.33329708266078584  -0.8849362187777485", # test_dos
    "Wilson B-factor      9.66", # test_dos
    "90      0.61    0.72        13      0.65    0.73         3.01", # resolve_denmod_pdb_memory
    "5.4    0.02    0.12      70  -0.09   -0.10      22  -0.01    0.06      92",# resolve_denmode_pdb_memory
    "4.3   -0.07   -0.06      84  -0.22    0.11      18  -0.09   -0.03     102",# resolve_denmode_pdb_memory
    "4.3   -0.07   -0.06      84  -0.22    0.07      18  -0.10   -0.04     102", # resolve_denmod_pdb_memory
    "0.0    0.02    0.00     543  -0.09   -0.03     105  -0.00   -0.00     648",# resolve_denmod_pdb_memory
    "8.6   -0.09   -0.07      18  -0.11    0.14       9  -0.10   -0.01      27", #resolve_denmod_pdb_memory
     " 5.4    0.04    0.12      70  -0.09   -0.11      22   0.01    0.06      92",# resolve_denmod_pdb_memory
     "Solution annotation  history  :  RFZ 9.6 TFZ 15.4",# test_change_sg
     "1     3       46.0    3.1  318.9  +29.38   12.52   0.0      3  98.7/100.0", # test_autobuild-remark
     "1     3       46.0    3.1  318.9  +29.38   12.52   0.0      5  98.7/100.0", # test_autobuild_remark
     "2 PRO         0.57    1.87      1.57      0.48    2.35", # test_assign_sequence
     "MEAN :           0.65    0.78      1.20        10",# test_assign_sequence
     "47 ILE         0.73    1.52      1.01     -0.18    2.73",# test_assign_seq uence
     "51 GLU         0.48    0.06      0.41     -0.00    0.80",# test_assign_seq uence
     "MEAN :           0.61    1.09      0.98        10",# test_assign_seq uence
     "mr_llg : ", # many places
     "   2     3      200.7    6.0  156.9 -210.33    9.99  10.6      1  89.3/ 89.3", # automr_remark
     "3.2    0.01    0.01     160   0.17    0.14      24   0.03    0.04     184", # test_resolve_denmod_pdb_memory
      "overlap_allowed ", # mr_rosetta tests; changed to 10 from 300

            ] # text starts with one of these
 one_percent_list=[
    "unique mappings",
    "elements covers",
    "N:",
    "ALL:",
    "free reflections",
    "HIST :",  # in local_hist can differ by 1%
    "Mean LLG used for purge", # test_change_sg
    "Mean Score  Sigma  :",#test_change_sg
            ] # text starts with one of these
 two_percent_list= [
    "(plus the",  # from modify_ncs
    "elements covers", # test_rebuild
    "orthogonal", # test_multi_b
    "Asymmetric unit of NCS", #test_multi_b
    "Score :   ",  # 2010-05-10,
    "Best score up to now :",  # 2010-05-10
    "CENTER :  ", # 2010-06-13 find_ncs_from_density
     "tran_orth", # test_multi_b,find_ncs_from_density
     "center_orth", # find_ncs_from_density
     "rota_matrix", # find_ncs_from_density
    " : : ", # 2010-06-24 find_ncs_from_density
    "Overall score : 99.3306", # test_ligand_library
    "Segment     1 with    17 CA atoms", # test_helix_trace 2010-12-06
    "   1         -66",# test_automr_remark
    "Mean density in cutout region  no density offset applied        0.320 for    19694 points", # test_cutout
    "Segment     2 with     5 CA atoms.   Score     8.41", # test_helix_trace
    "Maximum   1.0196e+06", # test_automr_remark
    "Overall R-factor for FC vs FP :",#test_sharpen


]
 ten_percent_list=[
    "Saved at write out",  # 2010-05-10
    "Fraction protein",  # these are up to about 3 percent
    "Fraction solvent",
    "Atoms placed",   #  in build
    "! a",  # get_histograms numbers are about 1% off
    "    X   ",  # get_histograms numbers are about 1% off
    "12.9    0.90",   # in maps and values are more than 3*prec off
    "Atoms placed",
    "gvp model data",  # in set_hist values are about 2% off
    "fragments", # build_repeat differs
    " respectively", # test_sharpen
    "Regions in top", # test_modify_hist
    "Adjusted template by mean of", # test_dump_relat
    "HIST :", # test_local_hist
    "connection", #in test_loop_lib_split
    "100 x EST OF CC", #  2010-05-10
    "Principal components", #  2010-05-13
    "BAYES-CC", # 2010-05-14
    "mean bin z_score", # 2010-05-16 non-standard-sg
    "ARRANGEMENT SUMMARY", # 2010-06-06 assign_seq
    "Writing out PDB with", # 2010-06-06 assign_seq
    "PLACE SCORES", #2010-06-06 assign_sequence
   "Sample rebuilt model :", # mr_rosetta_rebuild
   "S_COORD_0001_ed_ed.pdb is   6.90", #test_rosetta_rescore
   "Wilson B-factor     1", # test_non_standard_sg
    "#1  ensemble_1",# test_automr_remark
    "Mean overlap <rho1*rho2> where rms of map   1 :    1.029158", #high_pass
    "Mean overlap <rho1*rho2> where rms of map   1 :    1.25144", # find_ncs
    "Resolution of Patterson  Number  :      10.00 - 5.00  115", # test_automr_remark
    "Resolution of Patterson  Number  :      10.00 - 5.00  134", # test-change_sg
    "Min B aniso from datafiles is   18.24 A**2",# test_non_standard_sg
    "b_iso   18.24322363",#test_non_standard_sg
    "Trace of aniso B :   18.24    28.97    22.74 A**2. Minimum :   18.24",#test_non_standard_sg,
     "Mean E^2  E^4 for  centric  expect 1 3  : 1.03141 3.01525", # test_change_sg
     "Mean E^2  E^4 for  centric  expect 1 3  : 1.02323 2.91377", # test_change_sg
     "Mean E^2  E^4 for  centric  expect 1 3  : 1.02678 2.97556", # test_change_sg
     "Z-score :     2.607540", # test_trace
     "Best dir :           -1    2.607540        23.33334", # test_trace
            ] # text starts with one of these

 twenty_percent_list=[
    "where beta",  # can vary depending on <cr>
    "Assembling model from",  # build_repeat varies by 20%
    "Skipped due to overlap with other segments", # build_repeat differs
    " atoms is ", # test_ligand_large
    "CA placed :", # test_find_helices_strands
    "R and R-free : 0", #test_non_standard_sg
    "ESTIMATED VOLUME :   ", # test_find_all_ligands
    "Estimated volume : ", # test_find_all_ligands
    "SITE_2_side.pdb", # test_find_all_ligands
    "Final list of cc values : ", # test_find_all_ligands
    "Final list of estimated volumes : ", # test_find_all_ligands
    "Real-space-refined model with map correlation", # 2010-06-06 build_model
    "SE     ", # test_inv_sg
    "1      99.331      0.840        1    31 /   31    ligand_fit_1_1.pdb", # test_ligand_library
    "R/Rfree :   0.44 /   0.46",# mr_rosetta_iter
    "RESCORE_MR_1/WORK_1/S_COORDS_0001_ed.pdb is", # mr_rosetta
    "Best match of copy is  1  with dist ", # mr_rosetta_ncs
    "ENSE ensemble_1 - EULER    8",#test_automr_remark
    "Highest Score  Z-score", # test_automr_remark
    "Number of peaks   27",#test_change_sg
    "#   2  +0.7 -1.8 -1.4  +11.5 -17.9 -0.7  -21.2  +0.502 +0.483 -0.046      -6719.0",#test-change_sg
    "**********",#test_change_sg
    "|    0         116       232       348       464       580 ",#test_change_sg
    "Mean E^2  E^4 for  centric  expect 1 3  : 1.03062 3.01206", # test_change_sg
    "with final rotation     20.0000 -7.4424 1.9757", # test_change_sg
    "Pseudo-translational NCS rotation angle 20 -7.44244 1.97571", # test_change_sg
    "2     4      200.7    6.0  156.9  +28.69   12.25  11.5      1  93.7/ 93.7", # test_automr_remark
    "2     4      200.7    6.0  156.9  +28.69   12.25  11.5      2  93.7/ 93.7", # test_automr_remark
  ]
 thirty_percent_list=[
    "Assembling model from", # in build
    "LOG FILE LENGTH",  # can vary depending on <cr>
    "ATOM ",  # B-factor can vary a lot
    "READY WITH LOOP",  # loop cc can vary
    " 93 ", # test_planar_rings
    "  Atoms placed ", #test_planar_rings
    "   92   ", # test_dump_relat
    "BIAS RATIO",  # in test_highest_mask about 5% off
    "      all       ",  #test_ligand_large
    "    main-chain      ",  #test_ligand_large
    "    side-chain      ",  #test_ligand_large
    " Rfree :   0", # phase_and_build
    "with R/Rfree", # phase_and_build
    "Model-map CC :   0.", # phase_and_build
    "refine_001.pdb  R/Rfree", # phase_and_build
    "CC-EST (BAYES-CC) CORR_RMS  :", #phase_and_build
    "CORR_RMS", #test_non_standard_sg
    "ESTIMATED MAP CC x 100 :", #test_non_standard_sg
    "RFACTOR :", #test_non_standard_sg
    "resolve_1.log (R=0.", #test_non_standard_sg
    "100 x EST OF CC :", #test_non_standard_sg
    "Density modification logfile", # test_non_standard_sg
    "    0.90   0", # test_ncs_groups
    "  Rfree  :   0", # test_inv_sg
    "Estimated ligand volume",#find_all_ligands
    "SITE_5_side", # find_all_ligands
    "None  None     0.00      0.00",#find_all_ligands
     "2.7    0.81    0.85     298   0.39    0.78      33   0.77    0.84     331", # test_zero_hl
     "mr_llg : 13.3516027187",# mr_Rosetta
    "test_mr_rosetta/MR_ROSETTA_1/WORK_1/WORK_1/S_COORDS_0001.pdb  SCORE :", # mr_rosetta
    "test_mr_rosetta/MR_ROSETTA_1/WORK_1/WORK_1/S_COORDS_0001_ed.pdb", # mr_rosetta
    "MR_ROSETTA_1/WORK_1/WORK_1/S_COORDS_0001_ed_ed.pdb", # mr_rosetta
    "Sample relaxed model with score of", # mr_rosetta
    "test_mr_rosetta/MR_ROSETTA_1/WORK_1/RUN_1/", # mr_rosetta
    "test_mr_rosetta/MR_ROSETTA_1/AutoMR_run_1_/coor.1.map", # mr_Rosetta
    "test_mr_rosetta_ncs/MR_ROSETTA_1/AutoMR_run_1_/coor.1.pdb", # mr_rosetta_ncs
    "test_mr_rosetta_ncs/MR_ROSETTA_1/AutoMR_run_1_/coor.1.map", # mr_rosetta_ncs
    "Stage : mr_solution  MR_LLG :", # mr_rosetta_ncs
    "test_mr_rosetta_ncs/MR_ROSETTA_1/RESCORE_MR_1/WORK_1/S_COORDS_0001_ed.pdb", # mr_rosetta_ncs
    "Stage : rescored_mr_solution  MR_LLG :", # mr_rosetta_ncs
    "WORK_1/S_COORDS_0001_mono_ed.pdb", # mr_rosetta_ncs
     " mr_llg :", # mr_rosetta_ncs
     "Stage : relaxed_rosetta_solution  MR_LLG :",#  mr_rosetta_ncs
     "ID : 3  SCORE :", # mr_rosetta_ncs
     "coor.1_refine_001_map_coeffs.map  Score :", # mr_rosetta_iter
     "ID : 1  ", # mr_rosetta_iter
     "SCORE TYPE :  MR_LLG", #mr_rosetta_ncs
     "Model correlation is   0.57  overall  and   0.62  in region of model", # mr_rosetta
     "Model correlation is   0.61  overall  and   0.69  in region of model", # mr_rosetta
     "Stage : rosetta_solution  ROSETTA SCORE :", # mr_rosetta_ncs
     "S_COORDS_0001_one_ed.pdb is   9", #mr_rosetta_ncs
     "cc_to_comparison : 0.", # mr_rosetta
     "coor.1_ref_001_map_coeffs_nf.map  Score :", # mr_rosetta
     "coor.1_ed_ed_rs_ref_001_ed.pdb is", # mr_rosetta_iter
     "Model info file with mean B of", # mr_rosetta_iter
     "cycle_best_2_nf.map  Score :", # mr_rosetta_iter
     "Mean B-value for this structure was", # mr_rosetta_iter
     "coor.1_ed.pdb is",# mr_rosetta_iter
     "test_mr_rosetta_ncs/MR_ROSETTA_1/WORK_1/RUN_1/S_COORDS_0001_ed_one_0001_ed_ed_ref_001.pdb  R/Rfree", # mr_rosetta_ncs
     "    6.43    0.83    0.87        14", # test_align_bases
     "Built : 71", # phase_and_build
     "R/Rfree :   0.46 /   0.38", # test_mr_rosetta_iter
     "Stage : autobuild_model  R_FACTOR :     0.47  NCS copies : 2",# test_mr_rosetta_ncs
     "Map correlation is   0.77",#ncs
     "Model correlation is   0.41  overall  and   0.47  in region of model",#ncs
     "Model correlation is   0.58  overall  and   0.66", # mr_rosetta
     "R/Rfree :   0.46 /   0.47", # mr_rosetta
     "Stage : autobuild_model  R_FACTOR :     0.46", # mr_rosetta
     "Model correlation is   0.74  overall  and   0.77", #mr_rosetta
     "R/Rfree :   0.42 /   0.43", # mr_rosetta
     "Stage : autobuild_model  R_FACTOR :     0.42", #mr_Rosetta
     " rosetta_score :", # mr_rosetta
     "3.6    0.28   0.56   0.60     0.64",#memory
     "  7.1    0.79    0.81      32   0.33", #memory
     " 2.7    0.90    0.92     299   0.82    0.96",#memory
     "3.6    0.83    0.84     149   0.73    0.74      22   0.81  ", # zero_hl
     "3.0    0.06   -0.04     115  -0.47   -0.37      15  -0.00   -0.10     130", #denmod_pdb_memory
     "63      0.70    0.72         6      0.00    0.00         3.18", # denmod_pdb_memory
     "/net/sigma/raid1/scratch1/terwill/misc/junk/test_find_all_ligands/TEMP0/SITE_3_side.pdb", # find_all_ligands
     "This build was no better than previous best",# test_build_repeat
     "12      0.62    0.65         4      0.00    0.00         7.34",# test_masks
    "10      0.29    0.41         0      0.00    0.00         6.84",#test_masks
    "Mean overlap <rho1*rho2> where rms of map   1 :    1.251301",#test_find_ncs
    "Mean overlap <rho1*rho2> where rms of map   1 :    1.029047",#test_high_pass
    "Number of points with protein histograms included : 15329",#test_resolve_multi_b
  ]
 one_integer_list=[
  "Leaving out",
  "attempts to write duplicates",
  ]
# ----------- solve defaults ---------------------
elif sys.argv[2]=='solve':
 base_eps=0.02  # anything under 0.02 is ok always
 small_eps_value=2.  # any delta under small_eps is ok if abs(value)<small_eps_value
 small_eps=0.5
 base_ratio_eps=0.02 # any delta under 0.02 of the value is ok
 ignore_list=[
    "Mon ","Tue ","Wed ","Thu ","Fri ","Sat ","Sun ",
    "SOLVE_RESOLVE_MALLOC_MAX",
    "DATASET TITLE",
    "TIME ELAPSED",
    "STEP TIME",
    "SOLVE",
    "PID ",
    "Dumped files",
    "license",
    "TIME REQUIRED",
    "FOR RUN",
    "quality of this Patterson",
    "Skew of the map",
    "AutoSol Run",
      " 1 ",
      " 2 ",
      " 8 ",
      " 9 ",
      " 10 ",
      "# OF REFLECTIONS",
      "NatFourier CCx100",
      "Date",
      "PHENIX",
      "Residual for targets",
    "in secondary structure",
    "alpha-helix residues",
    "/phenix", # 2010-07-07 may or may not contain "-" in path name
     'New estimate of f" :',  # test_sites_from_phases
    "max_wait_time", # all
    "Wilson B-factor     14.27", # test_autosol_unmerged
            ] # text starts with one of these
 one_percent_list=[
    "CURRENT VALUES",
            ] # text starts with one of these
 two_percent_list= [
            ] # text starts with one of these

 ten_percent_list=[
      "REFLECTIONS",
      "quality",
      "Overall Z-score value",
      "Mean figure of meritx100",
      "WHOLE MAP",
      "points in input map",
      'f"',
      'EST OF CC',
      'Raw scores',
      'BAYES-CC',
      'CC x 100',
      'FPRPRV'
            ]
 twenty_percent_list=[
      "Avg normalized",
      "points in input map",
      'f"',
      "WHOLE MAP XBAR",
      "Fbar",
      "Iso",
      "Wilson B-factor     14.28", # test_autosol_unmerged
  ]

 thirty_percent_list=[
     "Ratio of occupancies to standard refinement :",  # test_sites_from_phases
  ]
 one_integer_list=[
  ]

# ----------resolve pattern defaults------------
elif len(sys.argv)<3 or sys.argv[2]=='resolve_pattern':
 ignore_list=[
    "Mon ","Tue ","Wed ","Thu ","Fri ","Sat ","Sun ",
    "SOLVE_RESOLVE_MALLOC_MAX",
    "Dumped files",
    "license",
    "TIME REQUIRED",
    "version",
    "date",
      "Date",
      "PHENIX",
      "20  groups :",
      "NAN",
            ] # text starts with one of these
 one_percent_list=[
    "unique mappings",
    "elements covers",
            ] # text starts with one of these
 two_percent_list= [
]
 ten_percent_list=[
    "Fraction protein",  # these are up to about 3 percent
    "Fraction solvent",
            ] # text starts with one of these
 twenty_percent_list=[
  ]
 thirty_percent_list=[
  ]
 one_integer_list=[
  ]
# ---------- extract_segment_pair defaults------------
elif len(sys.argv)<3 or sys.argv[2] in ['extract_segment_pair',
    'extract_segment','side_chains','side_chains_update','segment_library']:
 base_eps=0.02  # anything under 0.02 is ok always
 small_eps_value=3.  # any delta under small_eps is ok if abs(value)<small_eps_value
 small_eps=1.5
 base_ratio_eps=0.1 # any delta under 0.1 of the value is ok
 ignore_list=[
    "Mon ","Tue ","Wed ","Thu ","Fri ","Sat ","Sun ",
    "SOLVE_RESOLVE_MALLOC_MAX",
    "Dumped files",
    "license",
    "TIME REQUIRED",
    "version",
    "date",
    "q= ",
    " q ",
    " 8000.",
      "Date",
      "PHENIX",
            ] # text starts with one of these
 one_percent_list=[
    "unique mappings",
    "elements covers",
            ] # text starts with one of these
 two_percent_list= [
]
 ten_percent_list=[
    "Fraction protein",  # these are up to about 3 percent
    "Fraction solvent",
            ] # text starts with one of these
 twenty_percent_list=[
  ]
 thirty_percent_list=[
  ]
 one_integer_list=[
  ]
else:
  print("Unrecognized method ",sys.argv[2])
  sys.exit(1)

import os
ignore_file=os.path.sep.join(list(os.path.split(sys.argv[1])[:-2])+['ignore.dat'])
if os.path.isfile(ignore_file):
  for line in open(ignore_file).readlines():
    ignore_list.append(line.rstrip())

def get_space_words():
  space_words=[]
  space_words.append(['test space','testspace'])
  import os
  path=os.getcwd()
  path_list=path.split(os.path.sep)
  for p in path_list:
    if p.find(" ")>-1:
      space_words.append([p,p.replace(" ","")])
  return space_words

def special_cases(a):
  # add spaces between integers and text if not present. EXCEPT de and DE
  #  which are used as 1.0234e-23 etc
  # 2009-08-09 add spaces after ( and [ and before ) and ]
  # 2012-03-13 remove space between "test space"  so we can have this as a special case and consider it in a path
  # 2012-03-18 remove any spaces that are in the current path
  new_a=""
  if len(a)==0:
    return a
  for old,new in zip (
     ["(","[",")","]",","],
     [" "," ", " "," "," "]):
   a=a.replace(old,new)
  for i in range(len(a)):
    if len(a)>i+2 and a[i] in "0123456789" and \
      a[i+1] in "abcfghijklmnopqrstuvwxyzABCFGHIJKLMNOPQRSTUVWXYZ,:;":
     new_a+=a[i]+" "
    elif len(a)>i+2 and a[i] in "0123456789" and \
      a[i+1] in "abcfghijklmnopqrstuvwxyzABCFGHIJKLMNOPQRSTUVWXYZ,:;-":
     new_a+=a[i]+" "
    elif len(a)>i+2 and a[i+1] in ":":
     new_a+=a[i]+" "
    else:
     new_a+=a[i]
  new_a+=a[len(a)-1]
  new_a=new_a.replace("="," ")
  for [space_word,nospace_word] in get_space_words():
   new_a=new_a.replace(space_word,nospace_word) #  2012-03-14 so we can have pathnames with spaces
  return new_a
# Hardwire special cases for solve/resolve tests END


def max(a,b):
  if a>b: return a
  return b
def abs(a):
  if a>0.: return a
  return -1.*a
def get_precision(a): # try to figure out the scale of the last digit of a float
  if a.find(".")<0:
   return 1 # integer precision is 1
  rest=a[a.find(".")+1:]
  digits=len(rest)
  if digits<1: return 1.
  if digits>10: return 1.e-10
  return 1./10.**digits
def get_line_from_diff(x):
     value=x[2:].rstrip()
     if len(value)>1 and value[0]==' ': # skip leading blank
       value=value[1:]
     return value

def run_find_diff_numbers(a,b,text,a1,b1): #try it with a b and if fails, try
  # a1 b1, then write out results whatever they are
  for aa,bb in zip([a1,a1,a,a],[b1,b,b1,b]):
    text_hold=deepcopy(text)
    f=StringIO()
    ok=find_diff_numbers(aa,bb,text_hold,out=f)
    notes=f.getvalue()
    #print notes
    if ok: return True
  print(notes)
  return False

def find_diff_numbers(a,b,text,out=sys.stdout,pad_colon=True):
 if pad_colon:
   spl_a=a.replace(":",": ").split()
   spl_b=b.replace(":",": ").split()
 else:
   spl_a=a.split()
   spl_b=b.split()
 ok=True
 for aa,bb in zip(spl_a,spl_b):
  if str(aa).lower()=='nan' and str(bb).lower()=='nan': continue #special
  try:
    aa1=float(aa)
    bb1=float(bb)
  except Exception:
   continue
  try:
   eps_min=3.*max(get_precision(aa),get_precision(bb))
  except Exception:
   eps_min=0.
  try:
    # special case: let -180 be same as +180 (for phase angles)
    if int(0.5+abs(aa1))==180 and int(0.5+abs(bb1))==180:
      same=True
    # special case: small numbers are all the same:
    elif abs(aa1)<base_eps and abs(bb1)<base_eps:
      same=True
    elif abs(aa1)<small_eps_value and abs(bb1)<small_eps_value \
         and abs(aa1-bb1)<small_eps:
      same=True
    else:
      eps=base_ratio_eps*max(10.,max(abs(aa1),abs(bb1)))
      eps=max(eps_min,eps)
      same=approx_equal(aa1,bb1,out=null_out(),eps=eps)
  except Exception:
    same=False
  if not same:
    ignore=False
    for ig in ignore_list:
     if text.find(ig)>=0:
       ignore=True
       continue
    for lis,perc,allowed in zip(
     [one_percent_list,two_percent_list,ten_percent_list,
         one_integer_list,twenty_percent_list,thirty_percent_list],
     [0.01,0.02,0.10,0,0.20,0.30],
     [0.05,0.05,0.,1,1,1]):  # one_percent_list gets 1%+0.05 allowed
      found=False
      for te in lis:
       if text.find(te)>=0:
         found=True
      if found:
        try:
          eps=max(allowed,perc*max(1.,max(abs(aa1),abs(bb1))))
          same=approx_equal(aa1,bb1,out=null_out(),eps=eps)
          if same:
            ignore=True
        except Exception: pass
    if not ignore:
      print(60*"-", file=out)
      print("Numbers do not match:",aa,bb, file=out)
      print(text, file=out)
      print(60*"-", file=out)
      ok=False
      break
 return ok

def check_for_bad_things(a):  # return False if any bad lines
  ignore_things=['column type different','Error estimate']
  bad_things=[
   'UNIT TEST FAILED',
   'not defined',
   'Input failed',
   'Sorry:',
   'raise Sorry',
   'Error',
   'Failed to carry out',
   'Sorry, no model built',
   'seems not to exist?'
   'Sorry, cannot interpret the file']
  for ignore_thing in ignore_things:
    if a.find(ignore_thing)>-1:
       return True

  for bad_thing in bad_things:
   if a.find(bad_thing)>-1:
     print("FAILING BECAUSE ",bad_thing," IS PRESENT IN: ",a)
     return False
  return True
def run(a):
  overall_ok=True
  started=False
  text_minus_1=""
  text_plus_1=""
  text_minus=""
  text_plus=""
  text=""
  for x in a.splitlines(1):
    ok=check_for_bad_things(x)
    if not ok: overall_ok=False
    if x.find("> >")==0 or x.find("< >")==0:
      continue  # entry
    elif x.find("> ")==0:
      text_plus+=x[2:]
      text_plus_1+=get_line_from_diff(x)
      text+=x[2:]
    elif x.find("< ")==0:
      text_minus+=x[2:]
      text_minus_1+=get_line_from_diff(x)
      text+=x[2:]
    elif x.find("---")==0:
      text+="---\n"
    elif text_minus or text_plus:
      ok=run_find_diff_numbers(text_minus,text_plus,
            text,text_minus_1,text_plus_1)
      if not ok:
        overall_ok=False
      text_plus=""
      text_minus=""
      text_minus_1=""
      text_plus_1=""
      text=""
    else:
      pass
  if text_minus or text_plus:
      ok=run_find_diff_numbers(text_minus,text_plus,
         text,text_minus_1,text_plus_1)
      if not ok:
        overall_ok=False
  return overall_ok

def remove_blanks(a):
  len_last=len(a)
  while len(a)!=len_last:
    len_last=len(a)
    a=a.replace("  "," ")
  return a

def compare_files(file):
  a=open(file).read()
  a=special_cases(a)
  return run(a)

if __name__=="__main__":
  if len(sys.argv)<2:
    print("Use: python compare_files.py <diff.dat>")
    sys.exit(0)
  file=sys.argv[1]
  overall_ok=compare_files(file)
  if not overall_ok:
    print("FAILED")
    sys.exit(1)
  else:
    print("OK")
