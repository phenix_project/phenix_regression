from __future__ import division
from __future__ import print_function
from difflib import unified_diff
import sys, os
op = os.path

def simplify(stdin_lines):
  result = []
  for line in stdin_lines:
    if (line.startswith("binary=")):
      line = "binary=..."
    elif (set(line).issubset(set(" .ox"))):
      line = "[ .ox]*"
    else:
      flds = []
      for fld in line.split():
        try: f = float(fld)
        except ValueError: pass
        else: fld = "%.2g" % f
        flds.append(fld)
      line = " ".join(flds)
    result.append(line)
  return result

def run(args):
  assert len(args) == 2
  reference = args[0]
  other = args[1]
  for node in os.listdir(reference):
    reference_sub = op.join(reference, node)
    if (not op.isdir(reference_sub)): continue
    other_sub = op.join(other, node)
    if (not op.isdir(other_sub)):
      print("Missing sub-directory:", other_sub)
      continue
    for node in os.listdir(reference_sub):
      if (node.endswith(".mtz")): continue
      if (node.endswith(".map")): continue
      if (node.endswith(".dat")): continue
      reference_file = op.join(reference_sub, node)
      if (not op.isfile(reference_file)): continue
      other_file = op.join(other_sub, node)
      if (not op.isfile(other_file)):
        print("Missing file:", other_file)
        continue
      r = open(reference_file).read().splitlines()
      o = open(other_file).read().splitlines()
      if (r != o):
        r = simplify(r)
        o = simplify(o)
        if (r != o):
          print("diff", reference_file, other_file)
          print("\n".join(list(unified_diff(r, o))[:50]))
          print()

if (__name__ == "__main__"):
  run(sys.argv[1:])
