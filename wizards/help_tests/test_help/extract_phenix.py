from __future__ import division
from __future__ import print_function
# extract_phenix
print("extract_phenix... taking all the phenix.xxx commands and running them")
import sys,os
if len(sys.argv)<2:
  print("use: extract_phenix file.htm")
  sys.exit(1)
file=sys.argv[1]
print(60*"*","\nFILE: ",file)
lines=open(file).readlines()
continue_line=None
for line in lines:
  if continue_line:
    line=continue_line+line
  full_line=line

  # skip some things:
  if not line.startswith("  "): continue
  if line.find("\\_")>-1: continue  # no phenix.cut\_out\_density
  if line.find("/")>-1: continue  # no subdirectories
  if not line.lstrip().startswith("phenix."): continue

  skip=False
  for skip_thing in ['.ensembler','.refine','.elbow','-',' applications',
       'cd','tar','BioTools','.mr_model_preparation','.sculptor','.resolve']:
    i=line.lstrip().find("phenix"+skip_thing)
    if i==0: skip=True # was phenix.refine or elbow or -
  if skip: continue
  line=line.lstrip().rstrip()
  line=line.split("#")[0]
  skip=False
  if len(line.split())<2: # nothing but a command
    skip=True
  line=line+" dry_run=True debug=True"
  for skip_thing in ['{','braces.','is limited','using','PHENIX','Here','from','show_defaults','quot',' tar ','BioTools',' cd ','echo','phenix']:
    if line[5:].find(skip_thing)>-1:
      print("skip thing",skip_thing)
      skip=True
      break
  if skip: continue
  print(line)
  if full_line.rstrip().endswith("\\"):
    continue_line=full_line.rstrip()[:-1]
    continue
  continue_line=None
  print(line)
  os.system(line)
