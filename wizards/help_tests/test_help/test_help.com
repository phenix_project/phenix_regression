#!/bin/csh  -f
#$ -cwd
#

echo " "
echo "----------------------------------------------------- "
echo " test_help "
#
phenix.find_ncs --help
phenix.map_to_object --help
phenix.find_all_ligands --help
phenix.find_helices_strands --help
phenix.get_cc_mtz_mtz --help
phenix.get_cc_mtz_pdb --help
phenix.autosol --help
phenix.autobuild --help
phenix.ligandfit --help
phenix.automr --help
phenix.multi_crystal_average --help
