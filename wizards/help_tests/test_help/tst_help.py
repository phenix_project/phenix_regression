from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os,sys
  print("Test script for test_help")
  print("")
  if (sys.platform == "win32") :
     print("Windows installation...skipping this test...OK")
     return

  log_file=os.path.join(os.getcwd(),"test_help.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  from phenix.command_line.find_ncs  import find_ncs
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''--help''')
  find_ncs(args=args,out=log)


  from phenix.command_line.map_to_object  import map_to_object
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''--help''')
  map_to_object(args=args,out=log)


  from phenix.command_line.find_all_ligands  import find_all_ligands
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''--help''')
  find_all_ligands(args=args,out=log)


  local_log_file='test_help.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.find_helices_strands  import find_helices_strands
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''--help''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  find_helices_strands(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)



  from phenix.command_line.get_cc_mtz_mtz  import get_cc_mtz_mtz
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''--help''')
  get_cc_mtz_mtz(args=args,out=log)


  from phenix.command_line.get_cc_mtz_pdb  import get_cc_mtz_pdb
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''--help''')
  get_cc_mtz_pdb(args=args,out=log)


  local_log_file='test_help.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autosol  import run_autosol
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''--help''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autosol(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)



  local_log_file='test_help.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''--help''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)



  local_log_file='test_help.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.ligandfit  import run_ligandfit
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''--help''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_ligandfit(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)



  local_log_file='test_help.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.automr  import run_automr
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''--help''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_automr(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)



  from phenix.command_line.multi_crystal_average  import multi_crystal_average
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''--help''')
  multi_crystal_average(args=args,out=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_help',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['help_tests','test_help']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
