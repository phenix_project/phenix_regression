from __future__ import division
from __future__ import print_function
#
# find_test.py
# find a test  that will call the subroutine named in sys.argv[2]
import sys,os
import libtbx.load_env
if len(sys.argv)<3:
  print("use: python find_test.py <method> <name_of_subroutine><name_of_test><list_only>")
  sys.exit(1)
method =sys.argv[1]
file=method+"_subs_called_in_tests.list"
full_file= libtbx.env.find_in_repositories(
    relative_path=os.path.join("phenix_regression","wizards",method+"_tests",file),
    test=os.path.isfile)
if not full_file:
  print("Sorry, cannot find the file ",file,"?")
  sys.exit(1)
call_dict={}
all_subs=[]
working_c=None
for line in open(full_file).readlines():
  line=line.replace(" ","")
  if not line: continue
  if line.find(":")>0:
    c=line.split(":")[0].rstrip()
    if not c:
       print("Missing test name in line ",line)
       sys.exit(1)
    if c in call_dict.keys():
       print("Duplicate c: ",line)
       sys.exit(1)
    call_dict[c]=[]
    working_c=c
  elif working_c is None:
    print("Sorry, need a subroutine name before calls ",line)
    sys.exit(1)

  else:
    call_dict[c].append(line.rstrip())
    if not line.rstrip() in all_subs: all_subs.append(line.rstrip())

call_list=call_dict.keys()
call_list.sort()
if 'list_only' in sys.argv[2:]:
  remaining_args=[]
  for a in sys.argv[2:]:
     if a!='list_only': remaining_args.append(a)
  if len(remaining_args)==0:
    for c in call_list:
      print(c+":")
      for sub in call_dict[c]:
        print(sub)
    print()
  else:  # figure out if remaining_args[0] is a sub or a test:
   if remaining_args[0] in call_list:
     #print "Subroutines called by the test ",remaining_args[0],":"
     print()
     for s in call_dict[remaining_args[0]]:
       print(s, end=' ')
     print()
   elif remaining_args[0] in all_subs:
     #print "Tests using the subroutine ",remaining_args[0],":"
     print()
     for c in call_list:
       if remaining_args[0] in call_dict[c]:
         print(c, end=' ')
     print()
   else:
     print("Sorry, cannot find the item ",remaining_args[0])
     sys.exit(1)
  sys.exit(0)

s=sys.argv[2]

if s in call_list:  # it is the name of a test
  print(s)
  sys.exit(0)

for c in call_list:
  if s in call_dict[c]:  # it is a subroutine that has a test
    print(c)
    sys.exit(0)

print("No test for subroutine",s,"available")
sys.exit(1)
