#!/bin/csh -ef
#$ -cwd
setenv SOLVETMPDIR /var/tmp
#
solve_resolve.side_chains_update<<EOD
-1,2,     ! -1=update ; 0=rna 1=dna 2=prot
EOD
