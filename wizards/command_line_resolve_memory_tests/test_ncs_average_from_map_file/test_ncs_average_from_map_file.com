#!/bin/csh -ef
#$ -cwd

echo ""
echo "RUNNING test_ncs_average with a map file"
phenix.ncs_average map_file=ncs_map.ccp4 ncs.ncs_spec 
if ( $status ) then
  echo "FAILED"
  exit 1
endif
