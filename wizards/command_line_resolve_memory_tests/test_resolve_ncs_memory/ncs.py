from __future__ import print_function
# demo run of ncs
# tt 2011-07-27

import sys
args=sys.argv[1:]
assert len(args) >= 3, "mtz_file, ncs file, solvent_content"
# get miller arrays from mtz file so we can pass to resolve
print("Running resolve in memory...")
print("Getting miller arrays from %s" %(args[0]))
input_text=""

from iotbx import reflection_file_reader
reflection_file=reflection_file_reader.any_reflection_file(args[0])
mtz_content=reflection_file.file_content()
miller_arrays=reflection_file.as_miller_arrays(merge_equivalents=True)

from solve_resolve.resolve_python import refl_db
array_dict=refl_db.assign_miller_arrays( miller_arrays=miller_arrays)

ncs_file=args[1]
print("Getting ncs from %s " %(ncs_file))

solvent_content=float(args[2])
print("Solvent content will be %7.3f" %(solvent_content))

from mmtbx.ncs.ncs import ncs
ncs_object=ncs()
ncs_object.read_ncs(ncs_file,log=sys.stdout)
if not ncs_object.ncs_read() or not len(ncs_object.ncs_groups())>0:
  ncs_object=None
  print("NO NCS INFORMATION??")
else: # all ok
  input_text+=ncs_object.format_all_for_resolve()

verbose=False
from solve_resolve.resolve_python.resolve_in_memory import run

input_text+="""
dump_ncs_full_map  ! write out the NCS-averaged map to memory
include_self  ! average all NCS copies
no_free  ! skip extra cycles of getting error estimates
no_optimize_ncs ! skip any reformulation of NCS
no_create_free  ! skip all free R info
use_all_for_test ! skip all free R info
"""

print("ARGS FOR RESOLVE: ")
print(input_text)
result_obj=run(array_dict=array_dict,
  solvent_content=solvent_content,verbose=verbose,out=sys.stdout,
	   input_text=input_text)
cmn=result_obj.results

print("DATA: ",cmn.map_db.map.size())
print("nx ny nz : ",cmn.map_db.nx,cmn.map_db.ny,cmn.map_db.nz)
print("NCS CC: ",cmn.map_db.quality)
print("NCS OPS: ",cmn.map_db.n_nc_oper)

# print out the array in cmn.ncs_cc_array::
print("CC VALUES : ", end=' ')
for i in range(cmn.map_db.n_nc_oper):
  print(" %6d " %(i+1), end=' ')
print()
for i in range(cmn.map_db.n_nc_oper):
  print("\n  %d          " %(i+1), end=' ')
  for j in range(cmn.map_db.n_nc_oper):
     print(" %6.2f " %(cmn.ncs_cc_array[i][j]), end=' ')
print()

print("DONE")
