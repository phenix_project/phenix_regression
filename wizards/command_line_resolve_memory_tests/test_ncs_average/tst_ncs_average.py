from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_ncs_average")
  print("")
  log_file=os.path.join(os.getcwd(),"test_ncs_average.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  from iotbx.cli_parser import run_program
  from phenix.programs import ncs_average as run
  from phenix.programs import map_correlations as map_cc


  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''ncs_map_coeffs.mtz ncs.pdb''')
  run_program(program_class=run.Program, args=args, logger = log)


  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''ncs_map_coeffs.mtz ncs.ncs_spec''')
  run_program(program_class=run.Program, args=args, logger = log)


  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''ncs_map_coeffs.mtz ncs_average.mtz''')
  run_program(program_class=map_cc.Program, args=args, logger = log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_ncs_average',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['command_line_resolve_memory_tests','test_ncs_average']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
