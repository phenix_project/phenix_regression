#!/bin/csh -ef
#$ -cwd
echo "standard morph"
phenix.python morph.py map_coeffs.mtz coords.pdb
if ( $status )then
  echo "FAILED"
  exit 1
endif
phenix.superpose_pdbs model.pdb coords_offset.pdb

echo "morph in box"
phenix.python morph.py m2_box.mtz m2_box.pdb "BoxMap,PHIBoxMap"
if ( $status )then
  echo "FAILED"
  exit 1
endif
phenix.superpose_pdbs model.pdb m2_box_offset.pdb

echo "morph as group "
phenix.python morph_group.py map_coeffs.mtz offset.pdb 
if ( $status )then
  echo "FAILED"
  exit 1
endif
phenix.superpose_pdbs model.pdb coords.pdb


