#!/bin/csh -ef
#$ -cwd


echo "RUNNING test_command_line_resolve_denmod_memory"
phenix.python<<EOD
import sys
args="phaser_1.mtz resolve_commands_file=inputs.dat output_mtz=denmod.mtz solvent_content=0.5".split()
from solve_resolve.resolve_python.density_modify_in_memory import get_files_and_run
get_files_and_run(args)
EOD
if ( $status ) then
  echo "FAILED"
  exit 1
endif
phenix.python<<EOD
import sys
args="phaser_1.mtz resolve_commands_file=inputs_no_denmod.dat solvent_content=0.5 output_mtz=no_denmod.mtz ".split()
from solve_resolve.resolve_python.density_modify_in_memory import get_files_and_run
get_files_and_run(args)
EOD

if ( $status ) then
  echo "FAILED"
  exit 1
endif
echo "CC denmod: `phenix.get_cc_mtz_mtz perfect.mtz denmod.mtz | grep offsett`"
echo "CC start:  `phenix.get_cc_mtz_mtz perfect.mtz no_denmod.mtz | grep offsett`"

if ( $status ) then
  echo "FAILED"
  exit 1
endif
