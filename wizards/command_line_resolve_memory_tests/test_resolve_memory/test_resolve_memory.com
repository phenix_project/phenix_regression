#!/bin/csh -ef
#$ -cwd

phenix.python <<EOD
from phenix_regression.phenix_masks.tst1 import run
run()
EOD

if ( $status ) then
  echo "FAILED phenix_regression/phenix_masks/tst1.py"
  exit 1
endif

foreach f ( averaged_map_coeffs.mtz fobs.mtz fobs_hl.mtz perfect.mtz reflections.mtz)

echo ""
echo "RUNNING WITH $f"
phenix.python<<EOD
import sys
args="$f".split()
from solve_resolve.resolve_python.density_modify_in_memory import get_files_and_run
get_files_and_run(args)
EOD

if ( $status ) then
  echo "FAILED"
  exit 1
endif
end
