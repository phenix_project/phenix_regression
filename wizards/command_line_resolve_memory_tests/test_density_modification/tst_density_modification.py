from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_density_modification")
  print("")
  log_file=os.path.join(os.getcwd(),"test_density_modification.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  from solve_resolve.resolve_python.density_modify_in_memory  import get_files_and_run
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''phaser_1.mtz mask_cycles=1 minor_cycles=1 resolution=3 output_mtz=denmod_std.mtz solvent_content=0.64 mask_type=histograms''')
  get_files_and_run(args=args,out=log)


  from phenix.command_line.get_cc_mtz_mtz  import get_cc_mtz_mtz
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''denmod_std.mtz denmod_std_expected.mtz''')
  get_cc_mtz_mtz(args=args,out=log)


  from solve_resolve.resolve_python.density_modify_in_memory  import get_files_and_run
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''fom_labels=SKIP phib_labels=SKIP phaser_1.mtz mask_cycles=1 minor_cycles=1 resolution=3 output_mtz=denmod_std_hl.mtz solvent_content=0.64 mask_type=histograms''')
  get_files_and_run(args=args,out=log)


  from phenix.command_line.get_cc_mtz_mtz  import get_cc_mtz_mtz
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''denmod_std_hl.mtz denmod_std_expected.mtz''')
  get_cc_mtz_mtz(args=args,out=log)


  from solve_resolve.resolve_python.density_modify_in_memory  import get_files_and_run
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''phaser_1.mtz mask_cycles=1 minor_cycles=1 resolution=3 output_mtz=denmod_no_aniso.mtz solvent_content=0.64 remove_aniso=False mask_type=histograms resolve_commands_file=resolve_commands.dat''')
  get_files_and_run(args=args,out=log)


  from phenix.command_line.get_cc_mtz_mtz  import get_cc_mtz_mtz
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''denmod_no_aniso.mtz denmod_no_aniso_expected.mtz''')
  get_cc_mtz_mtz(args=args,out=log)


  from solve_resolve.resolve_python.density_modify_in_memory  import get_files_and_run
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''phaser_1.mtz mask_cycles=1 minor_cycles=1 resolution=3 output_mtz=denmod_ha.mtz solvent_content=0.64 remove_aniso=True ha_file=ha.pdb rad_mask=3 mask_type=histograms''')
  get_files_and_run(args=args,out=log)


  from phenix.command_line.get_cc_mtz_mtz  import get_cc_mtz_mtz
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''denmod_ha.mtz denmod_ha_expected.mtz''')
  get_cc_mtz_mtz(args=args,out=log)


  from solve_resolve.resolve_python.density_modify_in_memory  import get_files_and_run
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''phaser_1.mtz mask_cycles=1 minor_cycles=1 resolution=3 output_mtz=denmod_model.mtz solvent_content=0.64 remove_aniso=True pdb_file=coords.pdb rad_mask=3 mask_type=histograms''')
  get_files_and_run(args=args,out=log)


  from phenix.command_line.get_cc_mtz_mtz  import get_cc_mtz_mtz
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''denmod_model.mtz denmod_model_expected.mtz''')
  get_cc_mtz_mtz(args=args,out=log)


  from solve_resolve.resolve_python.density_modify_in_memory  import get_files_and_run
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''phaser_1.mtz mask_cycles=1 minor_cycles=1 resolution=3 output_mtz=denmod_model_mask.mtz solvent_content=0.64 remove_aniso=True rad_mask=2 mask_type=histograms mask_from_pdb=mask.pdb''')
  get_files_and_run(args=args,out=log)


  from phenix.command_line.get_cc_mtz_mtz  import get_cc_mtz_mtz
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''denmod_model_mask.mtz denmod_model_mask_expected.mtz''')
  get_cc_mtz_mtz(args=args,out=log)


  from solve_resolve.resolve_python.density_modify_in_memory  import get_files_and_run
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''phaser_1.mtz mask_cycles=1 minor_cycles=1 resolution=3 output_mtz=denmod_model_mask_model.mtz solvent_content=0.64 remove_aniso=True rad_mask=2 mask_type=histograms mask_from_pdb=mask.pdb pdb_file=coords.pdb''')
  get_files_and_run(args=args,out=log)


  from phenix.command_line.get_cc_mtz_mtz  import get_cc_mtz_mtz
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''denmod_model_mask_model.mtz denmod_model_mask_model_expected.mtz''')
  get_cc_mtz_mtz(args=args,out=log)


  from solve_resolve.resolve_python.density_modify_in_memory  import get_files_and_run
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''ncs.mtz map_coeffs_file=phaser_1.mtz seq_file=seq.dat mask_cycles=1 minor_cycles=1 resolution=3 output_mtz=denmod_ncs.mtz solvent_content=0.50 remove_aniso=True mask_type=histograms ncs_file=ncs.ncs_spec pdb_file=ncs_one_chain_plus.pdb''')
  get_files_and_run(args=args,out=log)


  from phenix.command_line.get_cc_mtz_mtz  import get_cc_mtz_mtz
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''denmod_ncs.mtz denmod_ncs_expected.mtz''')
  get_cc_mtz_mtz(args=args,out=log)


  from solve_resolve.resolve_python.density_modify_in_memory  import get_files_and_run
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''ncs.mtz map_coeffs_file=phaser_1.mtz seq_file=seq.dat mask_cycles=1 minor_cycles=1 resolution=3 output_mtz=denmod_ncs_mask.mtz solvent_content=0.50 remove_aniso=True mask_type=histograms ncs_file=ncs.ncs_spec pdb_file=ncs_one_chain_plus.pdb ncs_domain_pdb=ncs_mask.pdb''')
  get_files_and_run(args=args,out=log)


  from phenix.command_line.get_cc_mtz_mtz  import get_cc_mtz_mtz
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''denmod_ncs_mask.mtz denmod_ncs_mask_expected.mtz''')
  get_cc_mtz_mtz(args=args,out=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_density_modification',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['command_line_resolve_memory_tests','test_density_modification']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
