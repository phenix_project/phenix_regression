#!/bin/csh -f
phenix.resolve<<EOD
hklin ncs_map_coeffs.mtz
labin FP=FWT PHIB=PHIFWT
solvent_content 0.34
no_build

new_ncs_group
rota_matrix    1.0000    0.0000    0.0000
rota_matrix    0.0000    1.0000    0.0000
rota_matrix    0.0000    0.0000    1.0000
tran_orth     0.0000    0.0000    0.0000

center_orth    8.5650    7.2765    9.2690

rota_matrix    0.4559   -0.7747    0.4382
rota_matrix    0.3152    0.6009    0.7345
rota_matrix   -0.8323   -0.1968    0.5182
tran_orth     6.8665   -5.8284   -2.1582

center_orth   -4.6060    4.3107   16.2912

rota_matrix    0.9070    0.1252    0.4020
rota_matrix    0.0964    0.8677   -0.4876
rota_matrix   -0.4099    0.4810    0.7750
tran_orth     7.6458   -0.2251    9.1974

center_orth    1.5272    6.6588   -3.2330

rota_matrix    0.1592   -0.4044    0.9006
rota_matrix    0.0427    0.9142    0.4029
rota_matrix   -0.9863   -0.0256    0.1628
tran_orth    14.5567    3.2018   -3.7124

center_orth  -13.5835    5.8152   -1.6410

no_free
mask_cycles 1
minor_cycles 1
include_self
dump_ncs_full_map
no_create_free
use_all_for_test
no_optimize_ncs
EOD
