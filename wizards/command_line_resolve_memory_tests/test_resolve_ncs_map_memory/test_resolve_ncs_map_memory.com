#!/bin/csh -ef
#$ -cwd

echo ""
echo "RUNNING test_resolve_ncs_in_memory"

phenix.python<<EOD
from solve_resolve.resolve_python.ncs_average import run
args="ncs_map_coeffs.mtz ncs.ncs_spec .60 3.".split()
run(args)
EOD

if ( $status ) then
  echo "FAILED"
  exit 1
endif
