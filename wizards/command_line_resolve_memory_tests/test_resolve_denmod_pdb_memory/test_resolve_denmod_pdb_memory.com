#!/bin/csh -ef
#$ -cwd


echo "RUNNING test_command_line_resolve_denmod_pdb_memory"
phenix.python<<EOD
import sys
args="exptl_fobs_phases_freeR_flags.mtz pdb_file=start.pdb solvent_content=0.6 resolve_commands_file=inputs.dat mask_cycles=1 minor_cycles=1 resolution=3.0".split()
from solve_resolve.resolve_python.density_modify_in_memory import get_files_and_run
get_files_and_run(args)
EOD

if ( $status ) then
  echo "FAILED"
  exit 1
endif

echo "RUNNING test_command_line_resolve_denmod_pdb_memory with mask as sites_cart"
phenix.python<<EOD
import sys
args="exptl_fobs_phases_freeR_flags.mtz pdb_file=start.pdb solvent_content=0.6 resolve_commands_file=inputs.dat mask_cycles=1 minor_cycles=1 resolution=3.0 mask_from_pdb=mask.pdb".split()
from solve_resolve.resolve_python.density_modify_in_memory import get_files_and_run
get_files_and_run(args)
EOD

if ( $status ) then
  echo "FAILED"
  exit 1
endif
