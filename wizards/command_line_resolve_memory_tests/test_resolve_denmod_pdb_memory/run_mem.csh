#!/bin/csh -f
phenix.python $PHENIX/solve_resolve/resolve_python/density_modify_in_memory.py exptl_fobs_phases_freeR_flags.mtz start.pdb solvent_content=0.6 denmod_with_model inputs.dat mask_cycles=1 minor_cycles=1
