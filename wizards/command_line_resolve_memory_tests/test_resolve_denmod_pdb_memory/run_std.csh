#!/bin/csh -f

echo "ZZ DUMP IMAGE"

phenix.resolve<<EOD
freer_if_present
hklin exptl_fobs_phases_freeR_flags.mtz
labin FP=FP 
solvent_content 0.6
no_build
resolution 3.0 500.0
ha_file NONE
composite_all
dump_composite
composite_pdb refine.pdb_
composite_pdb_first 1
composite_pdb_last 1
use_wang
no_ha
n_xyz 32 32 32
database 5
no_optimize_ncs
spg_name_use P 1 21 1
min_z_value_rho -3.0
start_chain 1   92
no_create_free
use_all_for_test
group_ca_length 4
group_length 2
n_random_frag 0
EOD

echo "ZZ IMAGE_ONLY"

phenix.resolve<<EOD
freer_if_present
hklin exptl_fobs_phases_freeR_flags.mtz
labin FP=FP SIGFP=SIGFP
hklout image.mtz
solvent_content 0.6
no_build
resolution 3.0 500.0
ha_file NONE
pattern_phase
cc_map_file combine.map
use_wang
no_ha
n_xyz 32 32 32
database 5
no_optimize_ncs
spg_name_use P 1 21 1
min_z_value_rho -3.0
start_chain 1   92
no_create_free
use_all_for_test
group_ca_length 4
group_length 2
n_random_frag 0
EOD

echo "ZZ DM IMAGE"

phenix.resolve<<EOD
freer_if_present
hklin image.mtz
labin FP=FP SIGFP=SIGFP PHIB=PHIM FOM=FOMM
labin HLA=HLAM HLB=HLBM HLC=HLCM HLD=HLDM
hklout image_only_dm.mtz
solvent_content 0.6
no_build
resolution 3.0 500.0
ha_file NONE
use_hist_prob
no_ha
n_xyz 32 32 32
database 5
no_optimize_ncs
spg_name_use P 1 21 1
min_z_value_rho -3.0
start_chain 1   92
no_create_free
use_all_for_test
group_ca_length 4
group_length 2
n_random_frag 0
mask_cycles 1  ! JUST XXX
minor_cycles 1 ! JUST XXX
!noscale
EOD

echo "ZZ DM IMAGE AND MODEL"

phenix.resolve<<EOD
freer_if_present
hklin exptl_fobs_phases_freeR_flags.mtz
labin FP=FP SIGFP=SIGFP
hklout resolve_work.mtz
solvent_content 0.6
no_build
mask_cycles 0
resolution 3.0 500.0
ha_file NONE
seq_file seq.dat
image
composite_all
scale_refl 0.2
composite_pdb refine.pdb_
composite_pdb_first 1
composite_pdb_last 1
pdb_in refine.pdb_1
use_hist_prob
no_ha
hklstart image_only_dm.mtz
labstart FP=FP PHIB=PHIM FOM=FOMM
n_xyz 32 32 32
database 5
use_any_side
no_optimize_ncs
spg_name_use P 1 21 1
min_z_value_rho -3.0
start_chain 1   92
no_create_free
use_all_for_test
group_ca_length 4
group_length 2
n_random_frag 0
minor_cycles 1 ! JUST XXX
EOD
