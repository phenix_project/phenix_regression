#!/bin/csh -ef
#$ -cwd

echo ""
echo "RUNNING cut_out_density"
phenix.cut_out_density coords1.pdb.mtz mtz_out=output.mtz padding=5. high_resolution=2.9999 cutout_dimensions=15,15,15 coords1.pdb cutout_type=model cutout_model_radius=5 cutout_subtract_mean=false pdb_out=output.pdb
if ( $status ) then
  echo "FAILED"
  exit 1
endif

phenix.get_cc_mtz_mtz output.mtz output_std.mtz
phenix.superpose_pdbs output.pdb cutout_std.pdb
if ( $status ) then
  echo "FAILED"
  exit 1
endif

echo "RUNNING cut_out_density with soft_mask"
phenix.cut_out_density coords1.pdb.mtz mtz_out=output1.mtz padding=5. high_resolution=2.9999 cutout_dimensions=15,15,15 coords1.pdb cutout_type=sphere cutout_model_radius=5 cutout_subtract_mean=false soft_mask=True pdb_out=output1.pdb
if ( $status ) then
  echo "FAILED"
  exit 1
endif

phenix.get_cc_mtz_mtz output1.mtz output_std.mtz
phenix.superpose_pdbs output1.pdb cutout_std.pdb
if ( $status ) then
  echo "FAILED"
  exit 1
endif

