#!/bin/csh -ef
#$ -cwd

echo ""
echo "RUNNING run_build (in memory)"
phenix.python<<EOD
from solve_resolve.resolve_python.run_build import run_build 
args="mtz_in=cycle_2.mtz pdb_out=pdb_out.pdb".split()
pdb_out = run_build(args)
import shutil
shutil.copyfile(pdb_out,'pdb_out_1.pdb')
EOD

wc pdb_out_1.pdb
if ( $status ) then
  echo "FAILED"
  exit 1
endif

echo "RUNNING build_in_memory"
phenix.python build_model.py map_coeffs.mtz seq.dat
if ( $status ) then
  echo "FAILED"
  exit 1
endif
echo ""
echo "Running from script now"
phenix.resolve<<EOD
hklin map_coeffs.mtz
labin FP=FWT PHIB=PHIFWT
solvent_content 0.4
seq_file seq.dat
mask_cycles 1
minor_cycles 1
build_only
superquick_build
EOD
if ( $status ) then
  echo "FAILED"
  exit 1
endif
