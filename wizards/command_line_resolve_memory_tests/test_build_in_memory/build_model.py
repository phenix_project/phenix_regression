from __future__ import print_function
# demo run density modification with any mtz file and start file and ncs
import sys
args=sys.argv[1:]
assert len(args) >= 1, "phenix.python build_model.py hklin "
if 'seq.dat' in args:
  seq_file_as_string=open('seq.dat').read()
else:
  seq_file_as_string=None

from iotbx import reflection_file_reader
reflection_file=reflection_file_reader.any_reflection_file(args[0])
miller_arrays=reflection_file.as_miller_arrays(merge_equivalents=True)
from solve_resolve.resolve_python import refl_db
array_dict=refl_db.assign_miller_arrays( miller_arrays=miller_arrays)
map_coeffs=array_dict.get('map_coeffs')
from solve_resolve.resolve_python import resolve_in_memory
input_text="""
build_only
superquick_build
"""
result_obj=resolve_in_memory.run(
        map_coeffs=map_coeffs, # map coefficients
        input_text=input_text, # any model-building commands
        seq_file_as_string=seq_file_as_string,
        build=True)
cmn=result_obj.results
f=open('model.pdb','w')
print(cmn.atom_db.pdb_out_as_string, file=f)
f.close()


