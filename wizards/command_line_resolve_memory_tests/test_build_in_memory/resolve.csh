#!/bin/csh -f
phenix.resolve<<EOD
hklin map_coeffs.mtz
labin FP=FWT PHIB=PHIFWT
solvent_content 0.4
seq_file seq.dat
mask_cycles 1
minor_cycles 1
build_only
superquick_build
EOD
