from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print ("Test script for test_build_in_memory")
  print ("")
  log_file=os.path.join(os.getcwd(),"test_build_in_memory.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  cmds='''from solve_resolve.resolve_python.run_build import run_build
args="mtz_in=cycle_2.mtz pdb_out=pdb_out.pdb".split()
pdb_out = run_build(args)
import shutil
shutil.copyfile(pdb_out,'pdb_out_1.pdb')
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print (open('phenix.python.log').read(), file = log)

  input_text=open('pdb_out_1.pdb').read()
  chars=len(input_text)
  lines=len(input_text.splitlines())
  print (chars,lines, file = log)

  from libtbx import easy_run
  result=easy_run.fully_buffered(command='phenix.python build_model.py map_coeffs.mtz seq.dat')
  for line in result.stdout_lines:
    print (line, file = log)

  cmds='''hklin map_coeffs.mtz
labin FP=FWT PHIB=PHIFWT
solvent_content 0.4
seq_file seq.dat
mask_cycles 1
minor_cycles 1
build_only
superquick_build
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.resolve', cmds,'phenix.resolve.log',None,None,None)
  print (open('phenix.resolve.log').read(), file = log)

  log.close()


  print ()
  print ('Done with test...running checks on output')
  print ()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_build_in_memory',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print ()
  print ('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['command_line_resolve_memory_tests','test_build_in_memory']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
