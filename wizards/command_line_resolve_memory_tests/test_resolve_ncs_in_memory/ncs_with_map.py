from __future__ import print_function
# demo run of ncs
# tt 2011-07-27

import sys
args=sys.argv[1:]
assert len(args) >= 4, "mtz_file, ncs file, solvent_content, map file"
# get map from map file and other info from mtz file so we can pass to resolve
print("Running resolve in memory...")
print("Getting miller arrays from %s" %(args[0]))
input_text=""

from iotbx import reflection_file_reader
reflection_file=reflection_file_reader.any_reflection_file(args[0])
mtz_content=reflection_file.file_content()
miller_arrays=reflection_file.as_miller_arrays(merge_equivalents=True)

from solve_resolve.resolve_python import refl_db
array_dict=refl_db.assign_miller_arrays( miller_arrays=miller_arrays)
refl_db=refl_db.get_refl_db(array_dict=array_dict)

ncs_file=args[1]
print("Getting ncs from %s " %(ncs_file))

solvent_content=float(args[2])
print("Solvent content will be %7.3f" %(solvent_content))

from libtbx import easy_pickle
map=easy_pickle.load(args[3])

from mmtbx.ncs.ncs import ncs
ncs_object=ncs()
ncs_object.read_ncs(ncs_file,log=sys.stdout)
if not ncs_object.ncs_read() or not len(ncs_object.ncs_groups())>0:
  ncs_object=None
  print("NO NCS INFORMATION??")
else: # all ok
  input_text+=ncs_object.format_all_for_resolve()

verbose=False
from solve_resolve.resolve_python.resolve_in_memory import run

input_text+="""
dump_ncs_full_map  ! write out the NCS-averaged map to memory
include_self  ! average all NCS copies
no_free  ! skip extra cycles of getting error estimates
no_optimize_ncs ! skip any reformulation of NCS
no_create_free  ! skip all free R info
use_all_for_test ! skip all free R info
resolution 200 2.5
res_start 2.5
"""


nx=40 
ny=48 
nz=40
wang_radius=5.5  # REQUIRED
rms_rho_prot=0.33 # made up

space_group=refl_db.space_group
unit_cell=refl_db.unit_cell
print("UNIT CELL",unit_cell)
print("SG ",space_group)

print("ARGS FOR RESOLVE: ")
print(input_text)
result_obj=run(
  solvent_content=solvent_content,verbose=verbose,out=sys.stdout,
	   input_text=input_text,rms_rho_prot=rms_rho_prot,
           map=map,nx=nx,ny=ny,nz=nz,quality=1.0,
           space_group=space_group,unit_cell=unit_cell,
wang_radius=wang_radius)
cmn=result_obj.results

print("DATA: ",cmn.map_db.map.size())
print("nx ny nz : ",cmn.map_db.nx,cmn.map_db.ny,cmn.map_db.nz)
print("NCS CC: ",cmn.map_db.quality)
print("NCS OPS: ",cmn.map_db.n_nc_oper)

