#!/bin/csh -ef
#$ -cwd

echo ""
echo "RUNNING test_resolve_ncs_in_memory"
phenix.python ncs_with_map.py ncs_map_coeffs.mtz ncs.ncs_spec 0.34 dump.pkl
if ( $status ) then
  echo "FAILED"
  exit 1
endif
