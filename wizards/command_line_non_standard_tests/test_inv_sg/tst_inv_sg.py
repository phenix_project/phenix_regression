from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os,sys
  print("Test script for test_inv_sg")
  print("")
  log_file=os.path.join(os.getcwd(),"test_inv_sg.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  local_log_file='test_inv_sg.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autosol  import run_autosol
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''sites=2 lam1_p31.sca build=False quick=true seq.dat remove_aniso=False skip_xtriage=true debug=true clean_up=False ha_iteration=False model_ha_iteration=False''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autosol(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)


  cmds='''from phenix.autosol.run_program import run_program
temp_dir=os.path.join('AutoSol_run_1_','TEMP0')
for x in os.listdir(temp_dir):
if x.endswith(".mtz"):
xx=os.path.join(temp_dir,x)
cmds="%s" %(xx)
run_program('phenix.mtz.dump', cmds,None,None,None,None)
elif x.endswith(".pdb"):
xx=os.path.join(temp_dir,x)
for line in open(xx).readlines():
if line.startswith("CRYST1"): print line
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print(open('phenix.python.log').read(), file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_inv_sg',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['command_line_non_standard_tests','test_inv_sg']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
