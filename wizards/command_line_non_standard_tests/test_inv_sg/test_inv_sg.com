#!/bin/csh -ef
#$ -cwd
phenix.autosol sites=2 lam1_p31.sca build=False quick=true seq.dat remove_aniso=False skip_xtriage=true debug=true clean_up=False ha_iteration=False model_ha_iteration=False
phenix.python<<EOD
from phenix.autosol.run_program import run_program
temp_dir=os.path.join('AutoSol_run_1_','TEMP0')
for x in os.listdir(temp_dir):
  if x.endswith(".mtz"):
    xx=os.path.join(temp_dir,x)
    cmds="%s" %(xx)
    run_program('phenix.mtz.dump', cmds,None,None,None,None)
  elif x.endswith(".pdb"):
    xx=os.path.join(temp_dir,x)
    for line in open(xx).readlines():
      if line.startswith("CRYST1"): print line
EOD
