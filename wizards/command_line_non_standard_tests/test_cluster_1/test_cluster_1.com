#!/bin/csh -ef
#$ -cwd
echo ""
echo "Clusters as TX"
phenix.autosol fom_for_extreme_dm=0.01 ncycle_refine=1 resolution=5 coords.pdb.mtz seq.dat build=false quick=true mask_cycles=1 minor_cycles=1 lambda=.9792 skip_xtriage=True remove_aniso=false atom_type=TX sites_file=ha_1.pdb have_hand=True ha_iteration=False add_classic_denmod=False build=False phase_only=True
if ( $status )then
  echo "FAILED"
  exit 1
endif
