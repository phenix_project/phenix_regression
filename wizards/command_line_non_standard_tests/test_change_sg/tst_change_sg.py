from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os,sys
  print("Test script for test_change_sg")
  print("")
  log_file=os.path.join(os.getcwd(),"test_change_sg.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  local_log_file='test_change_sg.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.automr  import run_automr
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''resolution=4 rebuild_in_place=true coords=coords.pdb data=native.mtz RMS=0.85 seq_file=seq.dat copies=1 start_chains_list=92 n_cycle_rebuild_max=1 nbatch=1 input_refinement_file=refine.mtz use_all_plausible_sg=True include_input_model=False rebuild_in_place=True use_tncs=False''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_automr(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)



  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_change_sg',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['command_line_non_standard_tests','test_change_sg']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
