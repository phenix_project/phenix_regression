#!/bin/csh -ef
#$ -cwd
phenix.automr coords=coords.pdb data=native.mtz RMS=0.85 \
seq_file=seq.dat copies=1 start_chains_list=92 \
n_cycle_rebuild_max=1 nbatch=1 \
input_refinement_file=refine.mtz \
use_all_plausible_sg=True \
include_input_model=False \
rebuild_in_place=True use_tncs=False
if ( $status )then
  echo "FAILED"
  exit 1
endif

phenix.python<<EOD
from phenix.autosol.run_program import run_program
temp_dir='AutoBuild_run_1_'
for x in ['aniso_data_PHX.mtz','cycle_best_2.mtz','cycle_best_refine_map_coeffs_2.mtz','exptl_fobs_phases_freeR_flags.mtz','exptl_phases_and_amplitudes_for_density_modification_aniso.mtz','overall_best_denmod_map_coeffs.mtz','overall_best_final_refine_001.mtz','overall_best_refine_data.mtz','overall_best_refine_map_coeffs.mtz','refinement_PHX.mtz','working_best_denmod_map_coeffs.mtz','working_best_refine_map_coeffs.mtz']:
  xx=os.path.join(temp_dir,x)
  cmds="%s" %(xx)
  run_program('phenix.mtz.dump', cmds,None,None,None,None)
for xx in ['native.mtz','refine.mtz']:
  cmds="%s" %(xx)
  run_program('phenix.mtz.dump', cmds,None,None,None,None)

for x in ['MR.1_ed.pdb','cycle_best_2.pdb','edited_pdb.pdb','overall_best.pdb','overall_best_final_refine_001.pdb','overall_best_placed.pdb','working_best.pdb','working_best_placed.pdb']:
  for line in open(os.path.join(temp_dir,x)).readlines():
    if line.startwith('CRYST1'): print line

for xx in ['coords.pdb']:
  for line in open(xx).readlines():
    if line.startwith('CRYST1'): print line
EOD

