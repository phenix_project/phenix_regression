from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os,sys
  print("Test script for test_autosol_unmerged")
  print("")
  log_file=os.path.join(os.getcwd(),"test_autosol_unmerged.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  local_log_file='test_autosol_unmerged.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autosol  import run_autosol
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''ha_iteration=false model_ha_iteration=false add_classic_denmod=False chain_type=PROTEIN p9_se_w2.sca debug=True quick=True resolution=3.5 sites_file=p9_ha.pdb build=false have_hand=True remove_aniso=False test_remove_aniso=False unit_cell="113.949 113.949 32.474 90.000 90.000 90.00" space_group=I4 residues=200 solvent_fraction=None ncs_copies=1 refine_b=False phase_improve_and_build=False ncycle_refine=1''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autosol(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)



  local_log_file='test_autosol_unmerged.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autosol  import run_autosol
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''ha_iteration=false model_ha_iteration=false add_classic_denmod=False chain_type=PROTEIN unmerged.mtz space_group=p622 debug=True quick=True resolution=3.5 sites_file=ha_perfect_2.pdb build=false have_hand=True remove_aniso=True test_remove_aniso=False ncs_copies=1 residues=200 solvent_fraction=None''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autosol(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)



  local_log_file='test_autosol_unmerged.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autosol  import run_autosol
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''ha_iteration=false model_ha_iteration=false add_classic_denmod=False chain_type=PROTEIN unmerged.mtz space_group=p622 debug=True quick=True resolution=3.5 sites_file=ha_perfect_2.pdb build=false have_hand=True remove_aniso=False test_remove_aniso=False ncs_copies=1 residues=200 solvent_fraction=None''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autosol(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)


  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_autosol_unmerged',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')
  return 0



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['autosol_unmerged_tests','test_autosol_unmerged']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
