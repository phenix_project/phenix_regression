from __future__ import division
from __future__ import print_function
import libtbx.load_env
import sys, os
import os.path
import glob
from libtbx.command_line import run_tests_parallel


REGRESSION = libtbx.env.find_in_repositories(relative_path='phenix_regression', test=os.path.exists)

cmdargs = [
  "directory=" + os.path.join(REGRESSION,"wizards"),
]

def run(args,
     python_keyword_text=None,
     max_tests=None,
     start_test=None,
     tests_to_skip=None):

  if python_keyword_text:
    print("Running with %s " %(python_keyword_text))
  if max_tests:
    print("Running only %s tests" %(max_tests))
  if start_test:
    print("Running starting with test %s " %(start_test))
  if tests_to_skip:
    print("Skipping tests containing these words: %s " %str(tests_to_skip))

  return_value=run_tests_parallel.run(cmdargs + args,
     python_keyword_text=python_keyword_text,
     max_tests=max_tests,
     start_test=start_test,
     tests_to_skip=tests_to_skip)
  if return_value != 0:
    raise AssertionError("AT LEAST ONE TEST FAILED")

if __name__ == '__main__':
  import sys
  run(sys.argv[1:])
