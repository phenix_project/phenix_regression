#!/bin/csh -ef
#$ -cwd
phenix.python<<EOD
from phenix.autosol.regression.tst_map_to_model import tst_00,tst_01,tst_02,tst_03,tst_04
tst_00()
tst_01()
tst_02() # requires tst_01 etc
tst_03() # requires tst_01 etc
tst_04() # requires tst_01 etc
EOD
if ( $status )then
  echo "FAILED"
  exit 1
endif

