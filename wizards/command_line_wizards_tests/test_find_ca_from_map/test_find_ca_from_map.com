#!/bin/csh -ef
#$ -cwd

phenix.python<<EOD
from phenix.autosol.regression.tst_find_ca_from_map import tst_01
tst_01()
EOD
if ( $status )then
  echo "FAILED"
  exit 1
endif

