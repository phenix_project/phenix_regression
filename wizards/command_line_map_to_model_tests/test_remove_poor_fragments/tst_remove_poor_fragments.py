from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_remove_poor_fragments")
  print("")
  log_file=os.path.join(os.getcwd(),"test_remove_poor_fragments.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  from phenix.autosol.remove_poor_fragments  import run
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''fragments_all.pdb fragments_missing_one.ccp4 resolution=4 cutoff_method=sigma_cutoff seq_file=seq.dat minimum_number_of_fragments=2 toss_sigma_cutoff_a=5''')
  run(args=args,log=log)


  from phenix.autosol.remove_poor_fragments  import run
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''fragments_shifted.pdb fragments_missing_one_shifted.ccp4 resolution=4 cutoff_method=sigma_cutoff seq_file=seq.dat minimum_number_of_fragments=2 toss_sigma_cutoff_a=5''')
  run(args=args,log=log)


  from phenix.autosol.remove_poor_fragments  import run
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''fragments.pdb fragments_in.ccp4 resolution=4 seq_file=seq.dat cutoff_method=sigma_cutoff''')
  run(args=args,log=log)


  from phenix.autosol.remove_poor_fragments  import run
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''fragments_in.pdb fragments_out.pdb fragments_in.ccp4 resolution=4 optimize_weights=True seq_file=seq.dat cutoff_method=sigma_cutoff''')
  run(args=args,log=log)


  from phenix.autosol.remove_poor_fragments  import run
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''complex.pdb fragments_in.ccp4 resolution=4 seq_file=seq_complex.dat minimum_number_of_fragments=3 cutoff_method=sigma_cutoff''')
  run(args=args,log=log)


  from phenix.autosol.remove_poor_fragments  import run
  from phenix_regression.wizards.run_wizard_test import split_except_quotes
  args=split_except_quotes('''complex.pdb fragments_in.ccp4 resolution=4 seq_file=seq_complex.dat minimum_number_of_fragments=3''')
  run(args=args,log=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_remove_poor_fragments',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['command_line_map_to_model_tests','test_remove_poor_fragments']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
