#!/bin/csh -ef
#$ -cwd

phenix.remove_poor_fragments fragments_all.pdb fragments_missing_one.ccp4 resolution=4 cutoff_method=sigma_cutoff seq_file=seq.dat minimum_number_of_fragments=2 toss_sigma_cutoff_a=5 
if ( $status )then
  echo "FAILED"
  exit 1
endif

phenix.remove_poor_fragments fragments_shifted.pdb fragments_missing_one_shifted.ccp4 resolution=4 cutoff_method=sigma_cutoff seq_file=seq.dat minimum_number_of_fragments=2 toss_sigma_cutoff_a=5 
if ( $status )then
  echo "FAILED"
  exit 1
endif


phenix.remove_poor_fragments fragments.pdb fragments_in.ccp4 resolution=4 seq_file=seq.dat cutoff_method=sigma_cutoff
if ( $status )then
  echo "FAILED"
  exit 1
endif


phenix.remove_poor_fragments fragments_in.pdb fragments_out.pdb fragments_in.ccp4 resolution=4 optimize_weights=True seq_file=seq.dat cutoff_method=sigma_cutoff
if ( $status )then
  echo "FAILED"
  exit 1
endif

phenix.remove_poor_fragments complex.pdb fragments_in.ccp4 resolution=4 seq_file=seq_complex.dat minimum_number_of_fragments=3 cutoff_method=sigma_cutoff
if ( $status )then
  echo "FAILED"
  exit 1
endif

phenix.remove_poor_fragments complex.pdb fragments_in.ccp4 resolution=4 seq_file=seq_complex.dat minimum_number_of_fragments=3
if ( $status )then
  echo "FAILED"
  exit 1
endif

