#!/bin/csh -ef
#$ -cwd
phenix.map_to_model asymmetric_map=True include_trace_and_build=False resolution=3 chain_type=RNA veryshort.ccp4 veryshort.seq segment=true thoroughness=quick number_of_macro_cycles=1 number_of_trials=1 number_of_sa_models=1 b_sharpen_low=0 thorough_resolve_model_building=False run_helices_strands_only=true rebuild_cycles=1 refine_cycles=1 ss_refine_ncycle=1 parallel_runs=1 include_phase_and_build=False include_helices_strands_only=True run_resolve_model_building=False build_in_regions=False refine=False rebuild=False iterative_ss=False auto_sharpen=False
if ( $status )then
  echo "FAILED"
  exit 1
endif
