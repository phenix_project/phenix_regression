#!/bin/csh -ef
#$ -cwd
phenix.build_rna_helices veryshort.mtz veryshort.seq 
if ( $status )then
  echo "FAILED"
  exit 1
endif
phenix.get_cc_mtz_pdb veryshort.mtz build_rna_helices.pdb
