from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os,sys
  print("Test script for test_labels")
  print("")
  log_file=os.path.join(os.getcwd(),"test_labels.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  local_log_file='test_labels.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''quick=True a2u_short.pdb data=a2u-data.mtz input_map_file=a2u-sigmaa.mtz hires_file=a2u.sca refinement_file=a2u-data.mtz debug=True number_of_parallel_models=1 n_cycle_rebuild_max=0 input_map_labels=FWT,PHIC''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)



  local_log_file='test_labels.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''show_facts''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)



  local_log_file='test_labels.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''a2u_short.pdb data=a2u-data.mtz input_map_file=a2u-sigmaa.mtz hires_file=a2u.sca refinement_file=a2u-data.mtz debug=True number_of_parallel_models=1 n_cycle_rebuild_max=0 input_map_labels=FWT,PHIC input_hires_labels=I,SIGI input_refinement_labels="F-obs SIGF-obs R-free-flags"''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)



  local_log_file='test_labels.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''show_facts''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)



  local_log_file='test_labels.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''a2u_short.pdb data=a2u-data.mtz input_map_file=a2u-sigmaa.mtz hires_file=a2u.sca refinement_file=a2u-data.mtz debug=True number_of_parallel_models=1 n_cycle_rebuild_max=0 input_map_labels=FWT,PHIC,FOM input_hires_labels=I,SIGI input_refinement_labels="F-obs SIGF-obs"''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)



  local_log_file='test_labels.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''show_facts''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)


  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_labels',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['labels_tests','test_labels']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
