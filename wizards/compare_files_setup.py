from __future__ import division
from __future__ import print_function
import sys,os
if len(sys.argv)<2:
  print("phenix.python compare_files.py diff.dat > diff.list")
  print("phenix.python compare_files_setup.py diff.list")
  print("appends to ignore.dat")
  sys.exit(0)
if os.path.isfile('ignore.dat'):
  previous=open('ignore.dat').read()
else:
  previous=""
f=open('ignore.dat','w')
if previous:
  print(previous, file=f)

ready=False
for line in open(sys.argv[1]).readlines():
  if ready:
    print(line.rstrip(), file=f)
    ready=False
  elif line.startswith('Numbers do not match:'):
    ready=True
  else:
    ready=False
