#!/bin/csh -f

setenv dd $argv[1]
setenv n $argv[2]
echo "Making $n new copies of $dd"

@ i = 0
while ($i < $n)
@ i = $i + 1
echo "Making copy $i"
cp -rp $dd  ${dd}_${i}
cd  ${dd}_${i}
foreach x (*.py *.com *.expected_output *.ignored_output)
  mv $x ${x:r}_${i}.${x:e}
end
cd ..
end

