from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os,sys
  print("Test script for test_short_seq")
  print("")
  log_file=os.path.join(os.getcwd(),"test_short_seq.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this



  local_log_file='test_short_seq.log'
  local_log=open(local_log_file,'w')
  from phenix.command_line.autobuild  import run_autobuild
  from phenix_regression.wizards.run_wizard_test import split_except_quotes

  args=split_except_quotes('''input_pdb_file=hipip.pdb min_seq_identity_percent=50.0 resolution=0.0 input_data_file=hipip_2.sca space_group="P 21 21 21" input_seq_file=hipip_shortseq.dat rebuild_in_place=true rebuild_res_start_list=1 rebuild_res_end_list=1 number_of_parallel_models=1 rebuild_chain_list=A refine=False maps_only=true skip_xtriage=true n_cycle_rebuild_max=0''')
  sys_stdout_sav=sys.stdout
  sys.stdout=local_log
  run_autobuild(args=args)
  sys.stdout=sys_stdout_sav
  local_log.close()
  print(open(local_log_file).read(), file=log)


  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_short_seq',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['short_seq_tests','test_short_seq']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
