#!/bin/csh -ef
#$ -cwd
phenix.fit_loops pdb_in=nsf_gap.pdb mtz_in=resolve_0.mtz seq_file=nsf.seq start=37 end=43 chain_id=None debug=True
if ( $status )then
  echo "FAILED"
  exit 1
endif

phenix.fit_loops pdb_in=nsf_nogap.pdb mtz_in=resolve_0.mtz seq_file=nsf.seq 
grep "is not a gap" test_fit_loops.output
if ( $status )then
  echo "FAILED"
  exit 1
endif


