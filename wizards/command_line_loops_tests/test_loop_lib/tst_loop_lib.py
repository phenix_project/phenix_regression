from __future__ import division
from __future__ import print_function
def run(args,
   write_expected_file=False,
   update_ignore_file=False):



  # Auto-generated script prepared by create_py.py
  #
  import os
  print("Test script for test_loop_lib")
  print("")
  log_file=os.path.join(os.getcwd(),"test_loop_lib.output")  # always use log_file where needed
  log=open(log_file,'w')  # the word log in the output script is going to be this


  os.mkdir('run_test_loop_lib')

  import shutil
  shutil.copyfile('gene-5.pdb','run_test_loop_lib/gene-5.pdb')

  import shutil
  shutil.copyfile('gene-5.pdb.mtz','run_test_loop_lib/gene-5.pdb.mtz')

  import shutil
  shutil.copyfile('gap_gvp.pdb','run_test_loop_lib/gap_gvp.pdb')

  os.chdir('run_test_loop_lib')

  cmds='''from phenix.loop_lib.clustering import clustering
c=clustering()
c.tst()
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print(open('phenix.python.log').read(), file=log)

  cmds='''from phenix.loop_lib.pattern_match import pattern_match
pm=pattern_match(database_dir="loop_lib")
pm.tst(verbose=False)
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print(open('phenix.python.log').read(), file=log)

  cmds='''from phenix.loop_lib.pdb_mining import run
run("--file_name=gene-5.pdb --set_up_cluster_from_file --bin_width=100 --loop_length=9 --rmsd=1.5 ".split())
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print(open('phenix.python.log').read(), file=log)

  cmds='''from phenix.loop_lib.pdb_mining import run
run("--file_name=gene-5.pdb --set_up_cluster_from_file --bin_width=100 --loop_length=9 --rmsd=1.5 --existing_database=clusters_0.pkl.gz".split())
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print(open('phenix.python.log').read(), file=log)
  print(open('database_0.log').read(), file=log)

  cmds='''from phenix.loop_lib.pattern_match import pattern_match
pm=pattern_match(database_dir="loop_lib")
pm.setup(file_name="train.list")
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print(open('phenix.python.log').read(), file=log)

  cmds='''from phenix.loop_lib.clustering import clustering
c=clustering()
c.read_write_clusters(" clusters_0.pkl.gz split_clusters_3.pkl.gz ".split())
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print(open('phenix.python.log').read(), file=log)

  cmds='''from phenix.loop_lib.clustering import clustering
c=clustering()
c.read_write_clusters_database("clusters_0.pkl.gz test.pkl.gz".split())
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print(open('phenix.python.log').read(), file=log)

  cmds='''from phenix.loop_lib.clustering import clustering
c=clustering()
c.read_dump_segments("dump.pdb".split())
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print(open('phenix.python.log').read(), file=log)

  cmds='''from phenix.loop_lib.clustering import clustering
c=clustering()
c.show_clusters()
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print(open('phenix.python.log').read(), file=log)

  cmds='''from phenix.loop_lib.clustering import clustering
c=clustering()
c.run_set_up_cluster(file_name="gvp.pkl.gz")
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print(open('phenix.python.log').read(), file=log)

  cmds='''from phenix.loop_lib.clustering import clustering
c=clustering()
c.run_set_up_cluster(file_name="aep.pkl.gz")
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print(open('phenix.python.log').read(), file=log)

  cmds='''from phenix.loop_lib.clustering import clustering
c=clustering()
c.merge_clusters(
cluster_file="gvp.pkl.gz",
cluster_file_merge_list=["aep.pkl.gz"])
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print(open('phenix.python.log').read(), file=log)

  cmds='''from phenix.loop_lib.clustering import clustering
c=clustering()
c.merge_clusters(
cluster_file="gvp.pkl.gz",
cluster_file_merge_list=open("merge.list").readlines())
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print(open('phenix.python.log').read(), file=log)

  cmds='''from phenix.loop_lib.clustering import clustering
c=clustering()
c.merge_clusters(
cluster_file="gvp.pkl.gz",
cluster_file_merge_list=open("merge.list").readlines(),
new_clusters_only=True)
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print(open('phenix.python.log').read(), file=log)

  cmds='''from phenix.loop_lib.clustering import clustering
c=clustering()
c.merge_clusters(
cluster_file="gvp.pkl.gz",
cluster_file_merge_list=open("merge.list").readlines(),
new_clusters_only=True,assume_unique=True)
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print(open('phenix.python.log').read(), file=log)

  cmds='''from phenix.loop_lib.clustering import clustering
cluster_file="loop_lib/clusters_0.pkl.gz"
database_dir=os.path.split(cluster_file)[0]
c=clustering(database_dir=database_dir)
print "Database dir will be: ",database_dir
c.apply_clusters(
mtz_file="gene-5.pdb.mtz",ends_as_pdb="gap_gvp.pdb",
cluster_file=cluster_file,database_dir=database_dir)
  '''
  from phenix.autosol.run_program import run_program
  run_program('phenix.python', cmds,'phenix.python.log',None,None,None)
  print(open('phenix.python.log').read(), file=log)

  log.close()


  print()
  print('Done with test...running checks on output')
  print()
  lines=open(log_file).readlines()
  from phenix_regression.wizards.check_results import check_results
  check_results(test='test_loop_lib',
      lines=lines,
      update_ignore_file=update_ignore_file,
      write_expected_file=write_expected_file)
  print()
  print('OK')



if __name__=="__main__":
  import sys
  if 'run' in sys.argv:
    run(sys.argv[1:])
  else:
    from phenix_regression.wizards.run_wizard_test import set_up_wizard_test
    args=['command_line_loops_tests','test_loop_lib']

    print("Setting up test")
    set_up_wizard_test(args)
    print("Running test")
    run(args=[])
    print("OK")
