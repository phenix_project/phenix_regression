#!/bin/csh -ef
#$ -cwd
phenix.fit_loops pdb_in=nsf_gap_offset.pdb mtz_in=resolve_0.mtz seq_file=nsf_offset.seq start=38 end=44 chain_id=None debug=True
if ( $status )then
  echo "FAILED"
  exit 1
endif
phenix.pdbtools connect.pdb keep=all

