#!/bin/csh -ef
#$ -cwd
phenix.fit_loops pdb_in=nsf_gap_abafter.pdb mtz_in=resolve_0.mtz seq_file=nsf.seq loop_lib=True debug=True
if ( $status )then
  echo "FAILED"
  exit 1
endif
