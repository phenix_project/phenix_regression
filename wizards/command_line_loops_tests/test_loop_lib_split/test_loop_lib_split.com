#!/bin/csh -ef
#$ -cwd
phenix.fit_loops split.pdb random.mtz connect=true seq_file=sequence.dat debug=True
if ( $status )then
  echo "FAILED"
  exit 1
endif
