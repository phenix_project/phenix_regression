#!/bin/csh -f
setenv LIBTBX_FULL_TESTING
setenv PATH /usr/local/dummy/nothing:$PATH

phenix.ligandfit data=perfect.mtz model=partial.pdb \
   ligand=side_H.pdb cif_def_file_list=side_H.cif \
    resolution=3.0 n_group_search=1 ligand_cc_min=0.5 \
   remove_path_word_list=dummy input_labels="FP PHIC FOM" quick=True

phenix.ligandfit data=perfect.mtz model=partial.pdb \
   ligand=side.pdb resolution=3.0 n_group_search=1 ligand_cc_min=0.5 \
   remove_path_word_list=dummy input_labels="FP PHIC FOM" quick=True

