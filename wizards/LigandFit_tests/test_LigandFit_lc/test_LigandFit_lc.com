#!/bin/csh -f
phenix.ligandfit data=perfect.mtz model=test_ncs_lc.pdb \
   ligand=side.pdb quick=True resolution=3.0 n_group_search=1 ligand_cc_min=0.5 \
   ligand_near_chain="a" ligand_near_res=16 input_labels="FP PHIC FOM"
 
