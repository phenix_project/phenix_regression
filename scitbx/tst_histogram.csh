#! /bin/csh -fe

set PHENIX_REGRESSION_DIR="`libtbx.find_in_repositories phenix_regression`"

scitbx.histogram "$PHENIX_REGRESSION_DIR/misc/rms_differences.txt" | tail -1 | libtbx.assert_stdin "3.05101 - 3.3661: 6"

scitbx.histogram "$PHENIX_REGRESSION_DIR/misc/rms_differences.txt" --slots=5 --format_cutoff="%5.2f" | tail -1 | libtbx.assert_stdin " 2.74 -  3.37: 8"

scitbx.histogram --slots=5 --min=0 --max=5 --format_cutoff="%5.2f" < "$PHENIX_REGRESSION_DIR/misc/rms_differences.txt" | tail -2 | head -1 | libtbx.assert_stdin " 3.00 -  4.00: 6"

echo "OK"
