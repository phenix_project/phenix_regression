from __future__ import print_function
from scitbx.math.gumbel import exercise,exercise_1, exercise_sample

def exercise_gumbel():
  exercise()
  exercise_1()
  exercise_sample()
  print("OK")

if (__name__ == "__main__"):
  exercise_gumbel()
