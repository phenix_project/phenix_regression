from __future__ import print_function
from libtbx.test_utils import approx_equal
import libtbx.load_env
from iotbx import reflection_file_reader
from iotbx import reflection_file_utils
import iotbx.pdb
import mmtbx.utils
from six.moves import cStringIO as StringIO
import os
import mmtbx.maps
import iotbx
import mmtbx.model
from iotbx import extract_xtal_data

exercise_00_map_params_str_1 ="""\
  map_coefficients {
    format = *mtz phs
    mtz_label_amplitudes = 2mFoDFc
    mtz_label_phases = PH2mFoDFc
    map_type = 2mFo-DFc
    fill_missing_f_obs = True
    isotropize = False
  }
  map_coefficients {
    format = *mtz phs
    mtz_label_amplitudes = mFoDFc
    mtz_label_phases = PHmFoDFc
    map_type = mFo-DFc
    fill_missing_f_obs = False
    isotropize = False
  }
  map_coefficients {
    format = *mtz phs
    mtz_label_amplitudes = ANOM
    mtz_label_phases = PHANOM
    map_type = anom
    fill_missing_f_obs = False
    isotropize = False
  }
  map
  {
    map_type = 2Fobs-Fcalc
    fill_missing_f_obs = False
    grid_resolution_factor = 1/4.
    region = *selection cell
    atom_selection = None
    scale = *sigma volume
    atom_selection_buffer = 3
    file_name = 2fofc.xplor
    isotropize = False
  }
  map
  {
    map_type = Fo-Fc
    fill_missing_f_obs = False
    grid_resolution_factor = 1/4.
    region = selection *cell
    atom_selection = None
    scale = sigma *volume
    atom_selection_buffer = 3
    file_name = fofc.xplor
    isotropize = False
  }
"""

exercise_00_map_params_str_2 ="""\
  map_coefficients {
    format = *mtz phs
    mtz_label_amplitudes = 3mFo2DFc
    mtz_label_phases = PH3mFo2DFc
    map_type = 3mFo-2DFc
    fill_missing_f_obs = False
    isotropize = False
  }
  map
  {
    map_type = 3Fo-2Fc
    fill_missing_f_obs = False
    grid_resolution_factor = 1/4.
    region = *selection cell
    atom_selection = resname BGM
    scale = *sigma volume
    atom_selection_buffer = 3
    file_name = 3fo2fc.xplor
    isotropize = False
  }
"""

def exercise_00():
  import iotbx
  import mmtbx
  params1 = iotbx.phil.parse(exercise_00_map_params_str_1, process_includes=False)
  params1, unused = mmtbx.maps.map_and_map_coeff_master_params().fetch(params1,
    track_unused_definitions=True)
  assert len(unused) == 0
  params2 = iotbx.phil.parse(exercise_00_map_params_str_2, process_includes=False)
  params2, unused = mmtbx.maps.map_and_map_coeff_master_params().fetch(params2,
    track_unused_definitions=True)
  assert len(unused) == 0
  if 0:
    params1.show()
    params2.show()
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/anom_diff_map/1mdg.pdb",
    test=os.path.isfile)
  hkl_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/anom_diff_map/1mdg.mtz",
    test=os.path.isfile)
  reflection_file = reflection_file_reader.any_reflection_file(
    file_name = hkl_file)
  reflection_file_server = reflection_file_utils.reflection_file_server(
    reflection_files = [reflection_file])
  data_and_flags = extract_xtal_data.run(
    reflection_file_server = reflection_file_server)
  pdb_inp = iotbx.pdb.input(file_name=pdb_file)
  model = mmtbx.model.manager(
      model_input = pdb_inp,
      stop_for_unknowns = False,
      log = StringIO(),
      )
  model.process()
  model.set_non_unit_occupancy_implies_min_distance_sym_equiv_zero(False)
  xray_structure = model.get_xray_structure()
  fmodel = mmtbx.utils.fmodel_simple(
    xray_structures = [xray_structure],
    scattering_table = "wk1995",
    r_free_flags   = data_and_flags.r_free_flags,
    f_obs          = data_and_flags.f_obs)
  fmodel = fmodel.resolution_filter(d_min=2.5)
  fmodel.update_all_scales(update_f_part1=False)
  assert approx_equal(fmodel.r_work(), 0.16, 0.01)
  assert approx_equal(fmodel.r_free(), 0.22, 0.01)
  pdb_inp = iotbx.pdb.input(file_name = pdb_file)
  pdb_hierarchy = pdb_inp.construct_hierarchy()
  pdb_atoms = pdb_hierarchy.atoms()
  pdb_atoms.reset_i_seq()
  atom_selection_manager = pdb_hierarchy.atom_selection_cache()
  for asm in [None, atom_selection_manager]:
    mmtbx.maps.compute_xplor_maps(fmodel, params = params1.extract().map,
      atom_selection_manager=asm)
    cmo1 = mmtbx.maps.compute_map_coefficients(
      fmodel = fmodel,
      params = params1.extract().map_coefficients)

    mmtbx.maps.compute_xplor_maps(fmodel, params = params2.extract().map,
      atom_selection_manager=asm)
    cmo2 = mmtbx.maps.compute_map_coefficients(
      fmodel      = fmodel,
      params      = params2.extract().map_coefficients,
      mtz_dataset = cmo1.mtz_dataset)
    cmo2.write_mtz_file(file_name = "map_coefficients.mtz")

    miller_arrays = reflection_file_reader.any_reflection_file(file_name =
      "map_coefficients.mtz").as_miller_arrays()
    expected_labels = [['2mFoDFc', 'PH2mFoDFc'],
                       ['mFoDFc', 'PHmFoDFc'],
                       ['ANOM', 'PHANOM'],
                       ['3mFo2DFc', 'PH3mFo2DFc']]
    assert len(miller_arrays) == 4
    for ma in miller_arrays:
      assert ma.info().labels in expected_labels

def run(args):
  assert len(args) == 0
  exercise_00()
  from libtbx.utils import format_cpu_times
  print(format_cpu_times())

if (__name__ == "__main__"):
  import sys
  run(args=sys.argv[1:])
