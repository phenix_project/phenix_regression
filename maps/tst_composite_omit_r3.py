
from __future__ import division
from phenix.command_line import composite_omit_map
import iotbx.pdb.hierarchy
from cctbx.development import random_structure
from cctbx import sgtbx
from scitbx.array_family import flex
from libtbx.utils import null_out
import random
import sys

def exercise (args) :
  flex.set_random_seed(12345)
  random.seed(12345)
  xs = random_structure.xray_structure(
    space_group_info = sgtbx.space_group_info("R3:R"),
    elements         = ["O"]*20,
    unit_cell        = (20, 20, 20, 110, 110, 110))
  fc = abs(xs.structure_factors(d_min=2.0).f_calc())
  flags = fc.generate_r_free_flags(fraction=0.1,
    use_lattice_symmetry=True)
  mtz = fc.as_mtz_dataset(column_root_label="F")
  mtz.add_miller_array(flags, column_root_label="FreeR_flag")
  mtz.mtz_object().write("tst_composite_omit_r3.mtz")
  root = iotbx.pdb.hierarchy.root()
  model = iotbx.pdb.hierarchy.model()
  root.append_model(model)
  chain = iotbx.pdb.hierarchy.chain(id="X")
  model.append_chain(chain)
  unit_cell = xs.unit_cell()
  u_isos = xs.extract_u_iso_or_u_equiv()
  for i_seq, sc in enumerate(xs.scatterers()) :
    rg = iotbx.pdb.hierarchy.residue_group(
      resseq="%4d" % (i_seq+1))
    ag = iotbx.pdb.hierarchy.atom_group(resname="HOH")
    atom = iotbx.pdb.hierarchy.atom()
    atom.xyz = unit_cell.orthogonalize(site_frac=sc.site)
    atom.name = " O  "
    atom.element = " O"
    atom.b = random.random() * 20
    atom.occ = 1.0
    ag.append_atom(atom)
    rg.append_atom_group(ag)
    chain.append_residue_group(rg)
  open("tst_composite_omit_r3.pdb", "w").write(
    root.as_pdb_string(crystal_symmetry=xs))
  args_omit = [
    "tst_composite_omit_r3.pdb",
    "tst_composite_omit_r3.mtz",
    "omit_type=None",
    "omit_fraction=0.1",
    "box_cushion_radius=0.5",
    "output.file_name=tst_composite_omit_r3_map.mtz",
  ]
  args_omit.extend(args)
  composite_omit_map.run(args=args_omit, out=null_out())
  if (len(args) == 0) :
    from iotbx import mtz
    ma = mtz.object("tst_composite_omit_r3_map.mtz").as_miller_arrays()
    fft_map = ma[0].fft_map()
    real_map = fft_map.apply_sigma_scaling().real_map_unpadded()
    unit_cell = ma[0].unit_cell()
    for sc in xs.scatterers() :
      value = real_map.eight_point_interpolation(sc.site)
      assert (value > 3.4), value # XXX was 4 set to 3.5 for python3.8. Lowered to 3.4.

if (__name__ == "__main__") :
  exercise(args=sys.argv[1:])
