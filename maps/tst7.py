
from libtbx import easy_run
import libtbx.load_env
import os

def exercise () :
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/anom_diff_map/1mdg.pdb",
    test=os.path.isfile)
  hkl_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/anom_diff_map/1mdg.mtz",
    test=os.path.isfile)
  cmd = ["phenix.maps", pdb_file, hkl_file, "output.prefix=maps_tst7"]
  assert not easy_run.call(" ".join(cmd))
  assert os.path.isfile("maps_tst7_map_coeffs.mtz")
  from iotbx import mtz
  hkl_server = mtz.object("maps_tst7_map_coeffs.mtz")
  labels = [ a.info().label_string() for a in hkl_server.as_miller_arrays() ]
  assert (labels == ['2FOFCWT,PH2FOFCWT', '2FOFCWT_fill,PH2FOFCWT_fill',
                     'FOFCWT,PHFOFCWT', 'ANOM,PHANOM'])

if (__name__ == "__main__") :
  exercise()
