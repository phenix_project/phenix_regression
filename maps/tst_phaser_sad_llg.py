from __future__ import print_function
from libtbx import easy_run
import libtbx.load_env
import iotbx.pdb
import iotbx.mtz
import os

params = """\
maps {
  input {
     pdb_file_name = %s
     reflection_data.file_name = %s
  }
  wavelength = 0.918
  output {
    prefix = "1mdg_sad_llg"
  }
  map_coefficients {
    map_type = llg
    mtz_label_amplitudes = "SADLLG"
    mtz_label_phases = "PHISADLLG"
  }
  map_coefficients {
    map_type = anom
    mtz_label_amplitudes = "ANOM"
    mtz_label_phases = "PHIANOM"
  }
}
"""

def exercise_00():
  global params
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/anom_diff_map/1mdg.pdb",
    test=os.path.isfile)
  hkl_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/refinement/anom_diff_map/1mdg.mtz",
    test=os.path.isfile)
  params=params%(pdb_file,hkl_file)
  open("phaser_llg_map.eff","w").write(params)
  assert not easy_run.call("phenix.maps phaser_llg_map.eff > phaser_llg_map.log")
  assert os.path.isfile("1mdg_sad_llg_map_coeffs.mtz")
  coeffs = iotbx.mtz.object("1mdg_sad_llg_map_coeffs.mtz").as_miller_arrays()
  labels = [ c.info().label_string() for c in coeffs ]
  assert (labels == ['SADLLG,PHISADLLG', 'ANOM,PHIANOM'])
  llg_fft_map = coeffs[0].fft_map(resolution_factor=0.25)
  anom_fft_map = coeffs[1].fft_map(resolution_factor=0.25)
  map = llg_fft_map.apply_sigma_scaling().real_map_unpadded()
  map2 = anom_fft_map.apply_sigma_scaling().real_map_unpadded()
  hierarchy = iotbx.pdb.input(pdb_file).construct_hierarchy()
  for i_seq, atom in enumerate(hierarchy.atoms()) :
    if (atom.element.upper() in ["BR", "CO"]) :
      site_frac = coeffs[0].unit_cell().fractionalize(site_cart=atom.xyz)
      map_value = map.tricubic_interpolation(site_frac)
      map_value_2 = map2.tricubic_interpolation(site_frac)
      # BR will still have a large LLG peak (probably because this was
      # collected at the BR edge), but CO will be flat in the LLG map
      if (atom.element.upper() == "BR") :
        print(map_value, "1")
        assert (map_value > 25)
      else :
        print(map_value, "2")
        assert (map_value < 1.03) and (map_value_2 > 3), map_value

if (__name__ == "__main__"):
  try:
    import phaser
  except ImportError:
    print("phaser is not available: skipping test.")
    import sys
    sys.exit(0)
  exercise_00()
  print("OK")
