from __future__ import print_function
from cctbx.array_family import flex
import libtbx.load_env
from iotbx import reflection_file_reader
from iotbx import reflection_file_utils
import iotbx.pdb
import os, time
from libtbx import easy_run

overall_prefix="maps_test8"

def exercise_00():
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/maps/test_maps8.pdb",
    test=os.path.isfile)
  hkl_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/maps/test_maps8.mtz",
    test=os.path.isfile)
  #
  cmd = " ".join([
    "phenix.refine",
    "%s"%pdb_file,
    "%s"%hkl_file,
    "strategy=None",
    "main.number_of_mac=1",
    "overwrite=true",
    "output.prefix=%s"%overall_prefix,
    "--quiet",
    "> %s.zlog"%overall_prefix
    ])
  print (cmd)
  assert not easy_run.call(cmd)
  assert os.path.getsize("%s.zlog"%overall_prefix) == 0
  #
  miller_arrays = reflection_file_reader.any_reflection_file(file_name =
    "%s_001.mtz"%overall_prefix).as_miller_arrays()
  for ma in miller_arrays:
    if(ma.info().labels == ['2FOFCWT', 'PH2FOFCWT']):
      ma1 = ma.deep_copy()
  #
  map1 = ma1.fft_map(resolution_factor=1./3).real_map_unpadded()
  #
  xrs = iotbx.pdb.input(
    file_name="%s_001.pdb"%overall_prefix).xray_structure_simple()
  fc = ma1.structure_factors_from_scatterers(xray_structure = xrs).f_calc()
  map2 = fc.fft_map(resolution_factor=1./3).real_map_unpadded()
  #
  map_cc = flex.linear_correlation(x=map1.as_1d(), y=map2.as_1d()).coefficient()
  assert map_cc > 0.93

if (__name__ == "__main__"):
  t0 = time.time()
  exercise_00()
  print("Time: %6.4f" % (time.time()-t0))
