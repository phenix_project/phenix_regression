from __future__ import print_function
from libtbx import easy_run
import libtbx.load_env
import os
from iotbx import reflection_file_reader
from scitbx.array_family import flex

def exercise_00():
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/maps/test_maps5.pdb",
    test=os.path.isfile)
  hkl_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/maps/test_maps5.mtz",
    test=os.path.isfile)
  cmd = " ".join([
    "phenix.refine %s %s "%(pdb_file,hkl_file),
    "output.prefix=maps5 strategy=none main.number_of_mac=1",
    "xray_data.high_res=3", "--quiet --overwrite"
  ])
  assert not easy_run.call(cmd)
  miller_arrays = reflection_file_reader.any_reflection_file(file_name =
    "maps5_001.mtz").as_miller_arrays()
  for ma in miller_arrays:
    if(ma.info().labels == ['2FOFCWT', 'PH2FOFCWT']):
      mc1 = ma.deep_copy()
    if(ma.info().labels == ['2FOFCWT_no_fill', 'PH2FOFCWT_no_fill']):
      mc2 = ma.deep_copy()
  fft_map_1 = mc1.fft_map(resolution_factor=0.25)
  fft_map_1.apply_sigma_scaling()
  map_1 = fft_map_1.real_map_unpadded()
  fft_map_2 = mc2.fft_map(resolution_factor=0.25)
  fft_map_2.apply_sigma_scaling()
  map_2 = fft_map_2.real_map_unpadded()
  #
  CC = flex.linear_correlation(x = flex.double(list(map_1)),
    y = flex.double(list(map_2))).coefficient()
  assert CC>0.97, CC
  sel1 = map_1 >= 2.0
  sel2 = map_2 >= 2.0
  map_1 = map_1.set_selected(sel1, 1.0)
  map_1 = map_1.set_selected(~sel1, 0.0)
  map_2 = map_2.set_selected(sel2, 1.0)
  map_2 = map_2.set_selected(~sel2, 0.0)
  CC = flex.linear_correlation(x = flex.double(list(map_1)),
    y = flex.double(list(map_2))).coefficient()
  assert CC>0.90, CC

if (__name__ == "__main__"):
  exercise_00()
  from libtbx.utils import format_cpu_times
  print(format_cpu_times())
