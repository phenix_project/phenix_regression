
# a test with P6 symmetry - this did not work properly with the older
# symmetry handling code

from __future__ import division
from __future__ import print_function
from libtbx import easy_run

def exercise_p61 () :
  # derived from 4e0k
  pdb_str = """\
CRYST1   62.370   62.370   11.730  90.00  90.00 120.00 P 61
SCALE1      0.016033  0.009257  0.000000        0.00000
SCALE2      0.000000  0.018514  0.000000        0.00000
SCALE3      0.000000  0.000000  0.085251        0.00000
ATOM      1  N   LYS A   1      -5.730  -8.653  19.483  1.00 16.20           N
ATOM      2  C   LYS A   1      -7.408  -9.738  18.018  1.00 13.25           C
ATOM      3  O   LYS A   1      -8.029  -8.672  17.909  1.00 16.06           O
ATOM      4  CA  LYS A   1      -6.373  -9.945  19.113  1.00 13.85           C
ATOM      5  CB  LYS A   1      -7.002 -10.620  20.335  1.00 12.96           C
ATOM      6  CG  LYS A   1      -6.033 -10.783  21.486  1.00 13.19           C
ATOM      7  CD  LYS A   1      -6.566 -11.722  22.550  1.00 13.37           C
ATOM      8  CE  LYS A   1      -5.562 -11.852  23.679  1.00 13.77           C
ATOM      9  NZ  LYS A   1      -5.947 -12.837  24.706  1.00 14.91           N
ATOM     10  N   ASP A   2      -7.577 -10.771  17.210  1.00 10.13           N
ATOM     11  C   ASP A   2      -8.970 -12.081  15.708  1.00  6.99           C
ATOM     12  O   ASP A   2      -8.423 -13.101  16.149  1.00  6.91           O
ATOM     13  CA  ASP A   2      -8.327 -10.724  15.956  1.00  9.94           C
ATOM     14  CB  ASP A   2      -7.369 -10.476  14.782  1.00 13.66           C
ATOM     15  CG  ASP A   2      -6.963  -9.018  14.623  1.00 17.97           C
ATOM     16  OD1 ASP A   2      -7.292  -8.172  15.481  1.00 20.28           O
ATOM     17  OD2 ASP A   2      -6.287  -8.718  13.615  1.00 19.52           O
ATOM     18  N   TRP A   3     -10.042 -12.099  14.919  1.00  6.19           N
ATOM     19  CA  TRP A   3     -10.627 -13.316  14.415  1.00  5.55           C
ATOM     20  C   TRP A   3      -9.918 -13.810  13.170  1.00  5.91           C
ATOM     21  O   TRP A   3      -9.568 -13.028  12.288  1.00  8.46           O
ATOM     22  CB  TRP A   3     -12.108 -13.115  14.067  1.00  5.93           C
ATOM     23  CG  TRP A   3     -12.985 -12.920  15.255  1.00  5.62           C
ATOM     24  CD1 TRP A   3     -13.461 -11.735  15.755  1.00  6.18           C
ATOM     25  CD2 TRP A   3     -13.502 -13.943  16.106  1.00  4.86           C
ATOM     26  NE1 TRP A   3     -14.235 -11.962  16.865  1.00  6.69           N
ATOM     27  CE2 TRP A   3     -14.268 -13.311  17.111  1.00  5.78           C
ATOM     28  CE3 TRP A   3     -13.392 -15.338  16.112  1.00  5.08           C
ATOM     29  CZ2 TRP A   3     -14.902 -14.036  18.121  1.00  6.29           C
ATOM     30  CZ3 TRP A   3     -14.038 -16.055  17.097  1.00  5.65           C
ATOM     31  CH2 TRP A   3     -14.787 -15.400  18.094  1.00  6.14           C
ATOM     32  N   SER A   4      -9.765 -15.140  13.091  1.00  5.23           N
ATOM     33  C   SER A   4     -10.167 -17.015  11.603  1.00  5.54           C
ATOM     34  O   SER A   4     -10.790 -17.566  12.505  1.00  6.39           O
ATOM     35  CA  SER A   4      -9.257 -15.835  11.911  1.00  5.45           C
ATOM     36  CB  SER A   4      -7.842 -16.384  12.127  1.00  6.64           C
ATOM     37  OG  SER A   4      -6.904 -15.366  12.410  1.00  8.19           O
ATOM     38  N   PHE A   5     -10.209 -17.411  10.334  1.00  5.11           N
ATOM     39  CA  PHE A   5     -10.986 -18.562   9.880  1.00  5.12           C
ATOM     40  C   PHE A   5     -10.102 -19.389   8.970  1.00  5.31           C
ATOM     41  O   PHE A   5      -9.657 -18.913   7.927  1.00  6.13           O
ATOM     42  CB  PHE A   5     -12.239 -18.084   9.117  1.00  5.00           C
ATOM     43  CG  PHE A   5     -13.166 -19.161   8.665  1.00  5.57           C
ATOM     44  CD1 PHE A   5     -13.968 -19.826   9.567  1.00  6.15           C
ATOM     45  CD2 PHE A   5     -13.299 -19.452   7.308  1.00  6.58           C
ATOM     46  CE1 PHE A   5     -14.889 -20.756   9.125  1.00  7.94           C
ATOM     47  CE2 PHE A   5     -14.201 -20.386   6.877  1.00  8.01           C
ATOM     48  CZ  PHE A   5     -14.989 -21.035   7.777  1.00  8.69           C
ATOM     49  N   TYR A   6      -9.826 -20.621   9.363  1.00  5.87           N
ATOM     50  CA  TYR A   6      -8.998 -21.499   8.564  1.00  7.09           C
ATOM     51  C   TYR A   6      -9.148 -22.938   8.999  1.00  7.12           C
ATOM     52  O   TYR A   6      -9.847 -23.198  10.004  1.00  7.21           O
ATOM     53  CB  TYR A   6      -7.516 -21.055   8.578  1.00  8.59           C
ATOM     54  CG  TYR A   6      -6.899 -20.820   9.948  1.00  8.87           C
ATOM     55  CD1 TYR A   6      -6.440 -19.550  10.300  1.00 11.18           C
ATOM     56  CD2 TYR A   6      -6.748 -21.845  10.873  1.00  8.07           C
ATOM     57  CE1 TYR A   6      -5.846 -19.307  11.530  1.00 11.82           C
ATOM     58  CE2 TYR A   6      -6.161 -21.604  12.111  1.00  9.00           C
ATOM     59  CZ  TYR A   6      -5.710 -20.344  12.425  1.00 11.11           C
ATOM     60  OH  TYR A   6      -5.121 -20.114  13.649  1.00 14.19           O
ATOM     61  OXT TYR A   6      -8.579 -23.812   8.316  1.00  8.72           O
TER
ATOM     62  N   LYS B   1     -13.001 -26.037   8.426  1.00 18.26           N
ATOM     63  CA  LYS B   1     -12.569 -24.642   8.716  1.00 14.26           C
ATOM     64  C   LYS B   1     -13.313 -24.140   9.919  1.00 11.49           C
ATOM     65  O   LYS B   1     -14.481 -24.439  10.085  1.00 13.51           O
ATOM     66  CB  LYS B   1     -12.835 -23.768   7.523  1.00 16.75           C
ATOM     67  CG  LYS B   1     -12.247 -24.370   6.288  1.00 17.65           C
ATOM     68  CD  LYS B   1     -11.278 -23.415   5.717  1.00 13.71           C
ATOM     69  CE  LYS B   1     -10.704 -23.913   4.429  1.00 13.27           C
ATOM     70  NZ  LYS B   1      -9.800 -22.904   3.925  1.00 12.80           N
ATOM     71  N   ASP B   2     -12.640 -23.367  10.751  1.00  8.53           N
ATOM     72  C   ASP B   2     -12.576 -21.605  12.422  1.00  6.07           C
ATOM     73  O   ASP B   2     -11.515 -21.220  11.920  1.00  6.59           O
ATOM     74  CA  ASP B   2     -13.224 -22.916  12.015  1.00  7.62           C
ATOM     75  CB  ASP B   2     -13.026 -23.935  13.157  1.00  9.74           C
ATOM     76  CG  ASP B   2     -13.498 -25.338  12.819  1.00 13.92           C
ATOM     77  OD1 ASP B   2     -14.711 -25.596  12.959  1.00 15.80           O
ATOM     78  OD2 ASP B   2     -12.656 -26.200  12.481  1.00 15.92           O
ATOM     79  N   TRP B   3     -13.226 -20.924  13.367  1.00  5.85           N
ATOM     80  CA  TRP B   3     -12.785 -19.638  13.856  1.00  5.49           C
ATOM     81  C   TRP B   3     -11.857 -19.756  15.056  1.00  5.74           C
ATOM     82  O   TRP B   3     -11.902 -20.705  15.838  1.00  8.10           O
ATOM     83  CB  TRP B   3     -13.997 -18.775  14.228  1.00  6.22           C
ATOM     84  CG  TRP B   3     -14.820 -18.413  13.042  1.00  7.01           C
ATOM     85  CD1 TRP B   3     -15.892 -19.094  12.546  1.00  7.69           C
ATOM     86  CD2 TRP B   3     -14.618 -17.297  12.168  1.00  7.20           C
ATOM     87  NE1 TRP B   3     -16.366 -18.468  11.422  1.00  8.04           N
ATOM     88  CE2 TRP B   3     -15.591 -17.375  11.151  1.00  7.92           C
ATOM     89  CE3 TRP B   3     -13.694 -16.248  12.140  1.00  7.56           C
ATOM     90  CZ2 TRP B   3     -15.679 -16.433  10.129  1.00  9.12           C
ATOM     91  CZ3 TRP B   3     -13.792 -15.315  11.147  1.00  8.74           C
ATOM     92  CH2 TRP B   3     -14.762 -15.428  10.138  1.00  9.61           C
ATOM     93  N   SER B   4     -11.023 -18.736  15.189  1.00  5.17           N
ATOM     94  C   SER B   4     -10.140 -17.044  16.668  1.00  4.76           C
ATOM     95  O   SER B   4     -10.258 -16.226  15.750  1.00  5.54           O
ATOM     96  CA  SER B   4     -10.171 -18.530  16.359  1.00  5.40           C
ATOM     97  CB  SER B   4      -8.713 -18.978  16.093  1.00  7.95           C
ATOM     98  OG  SER B   4      -8.621 -20.307  15.617  1.00  9.69           O
ATOM     99  N   PHE B   5      -9.920 -16.688  17.929  1.00  4.79           N
ATOM    100  CA  PHE B   5      -9.761 -15.298  18.346  1.00  5.18           C
ATOM    101  C   PHE B   5      -8.557 -15.234  19.279  1.00  5.44           C
ATOM    102  O   PHE B   5      -8.584 -15.778  20.376  1.00  6.35           O
ATOM    103  CB  PHE B   5     -11.009 -14.777  19.063  1.00  4.93           C
ATOM    104  CG  PHE B   5     -10.943 -13.313  19.398  1.00  6.27           C
ATOM    105  CD1 PHE B   5     -11.161 -12.351  18.434  1.00  7.32           C
ATOM    106  CD2 PHE B   5     -10.643 -12.900  20.679  1.00  8.01           C
ATOM    107  CE1 PHE B   5     -11.092 -11.022  18.738  1.00  9.99           C
ATOM    108  CE2 PHE B   5     -10.556 -11.558  21.000  1.00 11.06           C
ATOM    109  CZ  PHE B   5     -10.786 -10.613  20.029  1.00 12.45           C
ATOM    110  N   TYR B   6      -7.485 -14.599  18.826  1.00  6.37           N
ATOM    111  CA  TYR B   6      -6.257 -14.475  19.605  1.00  7.65           C
ATOM    112  C   TYR B   6      -5.345 -13.455  18.949  1.00  8.75           C
ATOM    113  O   TYR B   6      -5.692 -12.941  17.870  1.00 10.05           O
ATOM    114  CB  TYR B   6      -5.522 -15.817  19.777  1.00  9.32           C
ATOM    115  CG  TYR B   6      -5.102 -16.450  18.458  1.00 12.18           C
ATOM    116  CD1 TYR B   6      -3.970 -16.007  17.775  1.00 13.98           C
ATOM    117  CD2 TYR B   6      -5.837 -17.462  17.881  1.00 14.24           C
ATOM    118  CE1 TYR B   6      -3.595 -16.545  16.552  1.00 16.25           C
ATOM    119  CE2 TYR B   6      -5.468 -18.014  16.645  1.00 15.03           C
ATOM    120  CZ  TYR B   6      -4.346 -17.543  15.985  1.00 15.50           C
ATOM    121  OH  TYR B   6      -3.965 -18.069  14.772  1.00 18.95           O
ATOM    122  OXT TYR B   6      -4.283 -13.180  19.518  1.00 10.45           O
TER
ATOM    123  N   LYS C   1      -8.515 -26.665   9.096  1.00  8.36           N
ATOM    124  C   LYS C   1      -6.811 -26.077  10.775  1.00  5.43           C
ATOM    125  O   LYS C   1      -7.551 -25.160  11.142  1.00  6.90           O
ATOM    126  CA  LYS C   1      -7.123 -26.899   9.541  1.00  6.60           C
ATOM    127  CB  LYS C   1      -6.128 -26.555   8.434  1.00  6.53           C
ATOM    128  CG  LYS C   1      -6.155 -27.508   7.247  1.00  5.98           C
ATOM    129  CD  LYS C   1      -5.021 -27.231   6.284  1.00  5.72           C
ATOM    130  CE  LYS C   1      -5.354 -27.699   4.888  1.00  6.07           C
ATOM    131  NZ  LYS C   1      -5.583 -29.166   4.816  1.00  7.38           N
ATOM    132  N   ASP C   2      -5.720 -26.440  11.432  1.00  4.62           N
ATOM    133  C   ASP C   2      -3.821 -25.868  12.834  1.00  4.02           C
ATOM    134  O   ASP C   2      -3.180 -26.769  12.275  1.00  4.33           O
ATOM    135  CA  ASP C   2      -5.313 -25.701  12.604  1.00  5.02           C
ATOM    136  CB  ASP C   2      -6.113 -26.133  13.822  1.00  7.20           C
ATOM    137  CG  ASP C   2      -6.307 -25.011  14.835  1.00  9.13           C
ATOM    138  OD1 ASP C   2      -7.140 -25.213  15.737  1.00 12.10           O
ATOM    139  OD2 ASP C   2      -5.639 -23.945  14.761  1.00  9.45           O
ATOM    140  N   TRP C   3      -3.284 -24.979  13.649  1.00  4.37           N
ATOM    141  CA  TRP C   3      -1.912 -25.015  14.099  1.00  4.48           C
ATOM    142  C   TRP C   3      -1.780 -25.868  15.360  1.00  4.87           C
ATOM    143  O   TRP C   3      -2.599 -25.754  16.261  1.00  7.76           O
ATOM    144  CB  TRP C   3      -1.442 -23.583  14.403  1.00  5.53           C
ATOM    145  CG  TRP C   3      -1.165 -22.741  13.172  1.00  5.77           C
ATOM    146  CD1 TRP C   3      -1.951 -21.763  12.614  1.00  7.59           C
ATOM    147  CD2 TRP C   3       0.002 -22.821  12.343  1.00  5.36           C
ATOM    148  NE1 TRP C   3      -1.335 -21.248  11.480  1.00  7.94           N
ATOM    149  CE2 TRP C   3      -0.129 -21.876  11.306  1.00  6.98           C
ATOM    150  CE3 TRP C   3       1.155 -23.606  12.385  1.00  5.12           C
ATOM    151  CZ2 TRP C   3       0.859 -21.723  10.317  1.00  7.06           C
ATOM    152  CZ3 TRP C   3       2.127 -23.441  11.409  1.00  6.28           C
ATOM    153  CH2 TRP C   3       1.970 -22.500  10.390  1.00  7.13           C
ATOM    154  N   SER C   4      -0.705 -26.647  15.431  1.00  3.61           N
ATOM    155  C   SER C   4       1.127 -27.257  16.900  1.00  3.10           C
ATOM    156  O   SER C   4       1.922 -27.154  15.967  1.00  3.97           O
ATOM    157  CA  SER C   4      -0.345 -27.463  16.600  1.00  3.54           C
ATOM    158  CB  SER C   4      -0.509 -28.962  16.332  1.00  5.38           C
ATOM    159  OG  SER C   4      -1.817 -29.334  16.003  1.00  7.19           O
ATOM    160  N   PHE C   5       1.487 -27.243  18.177  1.00  3.13           N
ATOM    161  CA  PHE C   5       2.878 -27.067  18.621  1.00  3.24           C
ATOM    162  C   PHE C   5       3.224 -28.258  19.506  1.00  3.64           C
ATOM    163  O   PHE C   5       2.674 -28.415  20.596  1.00  4.27           O
ATOM    164  CB  PHE C   5       3.033 -25.758  19.381  1.00  3.63           C
ATOM    165  CG  PHE C   5       4.453 -25.379  19.709  1.00  4.20           C
ATOM    166  CD1 PHE C   5       5.292 -24.885  18.733  1.00  5.57           C
ATOM    167  CD2 PHE C   5       4.933 -25.451  21.000  1.00  6.17           C
ATOM    168  CE1 PHE C   5       6.581 -24.495  19.024  1.00  8.18           C
ATOM    169  CE2 PHE C   5       6.229 -25.053  21.293  1.00  8.28           C
ATOM    170  CZ  PHE C   5       7.040 -24.575  20.308  1.00  9.34           C
ATOM    171  N   TYR C   6       4.128 -29.100  19.039  1.00  4.41           N
ATOM    172  CA  TYR C   6       4.512 -30.303  19.762  1.00  5.94           C
ATOM    173  C   TYR C   6       5.824 -30.855  19.229  1.00  6.76           C
ATOM    174  O   TYR C   6       6.350 -30.316  18.249  1.00  7.41           O
ATOM    175  CB  TYR C   6       3.415 -31.383  19.719  1.00  6.65           C
ATOM    176  CG  TYR C   6       3.029 -31.841  18.321  1.00  7.49           C
ATOM    177  CD1 TYR C   6       1.802 -31.521  17.786  1.00  9.33           C
ATOM    178  CD2 TYR C   6       3.908 -32.589  17.534  1.00  8.57           C
ATOM    179  CE1 TYR C   6       1.447 -31.964  16.507  1.00 10.98           C
ATOM    180  CE2 TYR C   6       3.569 -33.024  16.266  1.00 10.14           C
ATOM    181  CZ  TYR C   6       2.348 -32.701  15.755  1.00 10.72           C
ATOM    182  OH  TYR C   6       2.008 -33.122  14.483  1.00 12.83           O
ATOM    183  OXT TYR C   6       6.303 -31.832  19.829  1.00  8.25           O
TER
ATOM    184  N   LYS D   1      10.426 -29.021  19.772  1.00 17.83           N
ATOM    185  CA  LYS D   1       9.020 -28.821  19.363  1.00 15.31           C
ATOM    186  C   LYS D   1       8.950 -27.867  18.193  1.00 14.65           C
ATOM    187  O   LYS D   1       9.875 -27.109  17.930  1.00 18.23           O
ATOM    188  CB  LYS D   1       8.167 -28.281  20.506  1.00 17.02           C
ATOM    189  CG  LYS D   1       7.900 -29.308  21.600  1.00 18.78           C
ATOM    190  CD  LYS D   1       7.094 -28.700  22.721  1.00 20.52           C
ATOM    191  CE  LYS D   1       6.462 -29.763  23.595  1.00 19.99           C
ATOM    192  NZ  LYS D   1       7.373 -30.901  23.846  1.00 16.58           N
ATOM    193  N   ASP D   2       7.831 -27.902  17.508  1.00  9.39           N
ATOM    194  C   ASP D   2       6.189 -26.986  15.988  1.00  5.90           C
ATOM    195  O   ASP D   2       5.359 -27.732  16.505  1.00  5.88           O
ATOM    196  CA  ASP D   2       7.668 -27.218  16.232  1.00  9.08           C
ATOM    197  CB  ASP D   2       8.226 -28.074  15.091  1.00 11.98           C
ATOM    198  CG  ASP D   2       9.718 -27.932  14.926  1.00 14.80           C
ATOM    199  OD1 ASP D   2      10.427 -28.871  15.318  1.00 16.08           O
ATOM    200  OD2 ASP D   2      10.174 -26.898  14.394  1.00 16.25           O
ATOM    201  N   TRP D   3       5.890 -26.026  15.128  1.00  5.15           N
ATOM    202  CA  TRP D   3       4.546 -25.796  14.664  1.00  4.30           C
ATOM    203  C   TRP D   3       4.271 -26.583  13.379  1.00  4.70           C
ATOM    204  O   TRP D   3       5.123 -26.669  12.500  1.00  6.34           O
ATOM    205  CB  TRP D   3       4.305 -24.302  14.365  1.00  4.55           C
ATOM    206  CG  TRP D   3       4.132 -23.446  15.569  1.00  4.62           C
ATOM    207  CD1 TRP D   3       5.042 -22.600  16.117  1.00  5.15           C
ATOM    208  CD2 TRP D   3       2.958 -23.373  16.400  1.00  4.01           C
ATOM    209  NE1 TRP D   3       4.516 -21.998  17.234  1.00  5.41           N
ATOM    210  CE2 TRP D   3       3.234 -22.451  17.430  1.00  4.53           C
ATOM    211  CE3 TRP D   3       1.705 -23.987  16.366  1.00  3.96           C
ATOM    212  CZ2 TRP D   3       2.299 -22.144  18.418  1.00  4.86           C
ATOM    213  CZ3 TRP D   3       0.776 -23.678  17.346  1.00  4.10           C
ATOM    214  CH2 TRP D   3       1.083 -22.769  18.362  1.00  4.57           C
ATOM    215  N   SER D   4       3.050 -27.091  13.294  1.00  3.87           N
ATOM    216  C   SER D   4       1.134 -27.157  11.827  1.00  3.09           C
ATOM    217  O   SER D   4       0.430 -26.777  12.757  1.00  4.32           O
ATOM    218  CA  SER D   4       2.494 -27.764  12.109  1.00  3.41           C
ATOM    219  CB  SER D   4       2.276 -29.264  12.353  1.00  4.81           C
ATOM    220  OG  SER D   4       3.487 -29.941  12.600  1.00  6.36           O
ATOM    221  N   PHE D   5       0.733 -27.155  10.553  1.00  3.23           N
ATOM    222  CA  PHE D   5      -0.573 -26.665  10.134  1.00  2.99           C
ATOM    223  C   PHE D   5      -1.186 -27.732   9.231  1.00  3.21           C
ATOM    224  O   PHE D   5      -0.681 -27.986   8.138  1.00  3.98           O
ATOM    225  CB  PHE D   5      -0.454 -25.333   9.390  1.00  3.39           C
ATOM    226  CG  PHE D   5      -1.782 -24.713   9.035  1.00  3.72           C
ATOM    227  CD1 PHE D   5      -2.548 -24.076  10.009  1.00  4.87           C
ATOM    228  CD2 PHE D   5      -2.270 -24.772   7.734  1.00  4.40           C
ATOM    229  CE1 PHE D   5      -3.748 -23.499   9.688  1.00  6.69           C
ATOM    230  CE2 PHE D   5      -3.481 -24.200   7.396  1.00  5.80           C
ATOM    231  CZ  PHE D   5      -4.228 -23.576   8.383  1.00  7.27           C
ATOM    232  N   TYR D   6      -2.280 -28.340   9.686  1.00  3.68           N
ATOM    233  CA  TYR D   6      -2.939 -29.404   8.934  1.00  4.59           C
ATOM    234  C   TYR D   6      -4.342 -29.641   9.453  1.00  4.97           C
ATOM    235  O   TYR D   6      -5.028 -30.491   8.857  1.00  6.86           O
ATOM    236  CB  TYR D   6      -2.126 -30.697   8.934  1.00  5.91           C
ATOM    237  CG  TYR D   6      -1.764 -31.244  10.278  1.00  6.51           C
ATOM    238  CD1 TYR D   6      -0.446 -31.232  10.686  1.00  9.11           C
ATOM    239  CD2 TYR D   6      -2.709 -31.813  11.128  1.00  7.40           C
ATOM    240  CE1 TYR D   6      -0.058 -31.759  11.893  1.00 10.29           C
ATOM    241  CE2 TYR D   6      -2.315 -32.338  12.358  1.00  9.25           C
ATOM    242  CZ  TYR D   6      -0.995 -32.297  12.726  1.00  9.86           C
ATOM    243  OH  TYR D   6      -0.586 -32.816  13.916  1.00 12.27           O
ATOM    244  OXT TYR D   6      -4.726 -28.993  10.444  1.00  5.68           O
TER
"""
  pdb_file = "tst_composite_omit_2.pdb"
  mtz_file = "tst_composite_omit_2.mtz"
  open(pdb_file, "w").write(pdb_str)
  args = [
    "phenix.fmodel",
    pdb_file,
    "high_resolution=2.5",
    "type=real",
    "output.file_name=%s" % mtz_file,
    "r_free_flags_fraction=0.1",
    "label=F",
    "random_seed=12345",
    "> tst_composite_omit_map_2.zlog",
  ]
  rc = easy_run.call(" ".join(args))
  assert (rc == 0)
  args = [
    "phenix.composite_omit_map",
    pdb_file,
    mtz_file,
    "omit_type=None",
    "map_type=2mFo-DFc",
    "map_type=mFo-DFc",
    "output.file_name=tst_composite_omit_2_out.mtz",
    "> tst_composite_omit_map_2.zlog",
  ]
  rc = easy_run.call(" ".join(args))
  assert (rc == 0)
  import iotbx.pdb
  from iotbx import mtz
  mtz_in = mtz.object("tst_composite_omit_2_out.mtz")
  fft_map = mtz_in.as_miller_arrays()[0].fft_map(
    resolution_factor=0.25).apply_sigma_scaling()
  real_map = fft_map.real_map_unpadded()
  pdb_in = iotbx.pdb.input(pdb_file)
  xrs = pdb_in.xray_structure_simple()
  sum = total = 0
  for site_frac in xrs.sites_frac() :
    sum += real_map.eight_point_interpolation(site_frac)
    total += 1
  map_sites_mean = sum / total
  assert (map_sites_mean > 2)

if (__name__ == "__main__") :
  exercise_p61()
  print("OK")
