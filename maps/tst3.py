from __future__ import print_function
from libtbx import easy_run
import libtbx.load_env
import os

params = """\
 maps {
   input {
     pdb_file_name = %s
     reflection_data {
       file_name = %s
       labels = F_INFL
       high_resolution = None
       low_resolution = None
       outliers_rejection = True
       sigma_fobs_rejection_criterion = 0.0
       sigma_iobs_rejection_criterion = 0.0
       r_free_flags {
         file_name = %s
         label = None
         test_flag_value = None
         ignore_r_free_flags = False
       }
     }
   }
   output {
     prefix = "init_ano"
     job_title = None
     fmodel_data_file_format = mtz cns
   }
   bulk_solvent_correction = False
   anisotropic_scaling = False
   skip_twin_detection = True
   map_coefficients {
     map_type = anomalous
     format = *mtz phs
     mtz_label_amplitudes = "ANOM"
     mtz_label_phases = "PHIANOM"
     fill_missing_f_obs = True
   }
   map {
     map_type = anomalous
     format = *xplor ccp4
     file_name = None
     fill_missing_f_obs = True
   }
   map {
     map_type = anomalous
     format = xplor *ccp4
     file_name = ccp4_anom_map.ccp4map
     fill_missing_f_obs = True
   }
 }
"""

def exercise_00():
  global params
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/maps/test_maps3.pdb",
    test=os.path.isfile)
  hkl_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/maps/test_maps3.cv",
    test=os.path.isfile)
  params=params%(pdb_file,hkl_file,hkl_file)
  open("maps3.eff","w").write(params)
  assert not easy_run.call("phenix.maps maps3.eff > maps3.log")
  assert os.path.isfile("ccp4_anom_map.ccp4map")
  assert os.path.isfile("init_ano_anomalous_map.xplor")
  assert os.path.isfile("init_ano_map_coeffs.mtz")

if (__name__ == "__main__"):
  exercise_00()
  from libtbx.utils import format_cpu_times
  print(format_cpu_times())
