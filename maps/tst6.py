from __future__ import print_function
from libtbx import easy_run
import libtbx.load_env
import os
import iotbx.pdb
import iotbx.xplor.map
from scitbx.array_family import flex
from iotbx import reflection_file_reader

params = """\
maps {
  input {
    pdb_file_name = %s
    reflection_data {
      file_name = %s
      labels = f-obs
    }
  }
  bulk_solvent_correction = False
  anisotropic_scaling = False
  skip_twin_detection = True
  map_coefficients {
    map_type = mFo-DFc
    format = *mtz phs
    mtz_label_amplitudes = mFoDFc
    mtz_label_phases = PmFoDFc
  }
  map {
    map_type = mFo-DFc
    format = *xplor ccp4
    file_name = None
  }
  omit {
    method = *simple
    selection = %s
  }
  output {
    prefix = maps6
  }
}
"""

def exercise_00():
  global params
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/maps/test_maps6.pdb",
    test=os.path.isfile)
  cmd = " ".join([
    "phenix.fmodel",
    "%s"%pdb_file,
    "high_res=1.0",
    "type=real",
    "label=f-obs",
    "output.file_name=data6.mtz > log.tmp"])
  assert not easy_run.call(cmd)
  pdb_inp = iotbx.pdb.input(file_name = pdb_file)
  pdb_hierarchy = pdb_inp.construct_hierarchy()
  pdb_atoms = pdb_hierarchy.atoms()
  pdb_atoms.reset_i_seq()
  xrs = pdb_inp.xray_structure_simple()
  sites_frac = xrs.sites_frac()
  sel_keep = pdb_hierarchy.atom_selection_cache().selection(string = "resseq 1")
  sel_omit = pdb_hierarchy.atom_selection_cache().selection(string = "resseq 2")
  cntr = 0
  for sel_str in ["None", "resseq 2"]:
    params_=params%(pdb_file, "data6.mtz", sel_str)
    open("map6.params","w").write(params_)
    assert not easy_run.call("phenix.maps map6.params > log.tmp")
    #
    mc = reflection_file_reader.any_reflection_file(file_name =
    "maps6_map_coeffs.mtz").as_miller_arrays()[0]
    fft_map = mc.fft_map(resolution_factor=0.25)
    fft_map.apply_volume_scaling()
    d2 = fft_map.real_map_unpadded()
    #
    mv2 = flex.double()
    for sf in sites_frac:
      mv2.append(d2.eight_point_interpolation(sf))
    k = flex.mean(mv2.select(sel_keep))
    o = flex.mean(mv2.select(sel_omit))
    #print "keep:", k
    #print "omit:", o
    if(sel_str == "None"):
      cntr += 1
      assert abs(k)<1.e-6
      assert abs(o)<1.e-6
    else:
      cntr += 1
      assert k<-0.02, k
      assert o>1.0, o
  assert cntr == 2


if (__name__ == "__main__"):
  exercise_00()
  from libtbx.utils import format_cpu_times
  print(format_cpu_times())
