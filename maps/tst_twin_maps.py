from __future__ import print_function

from mmtbx.utils import fmodel_manager
from iotbx import pdb, mtz
from scitbx.array_family import flex
from libtbx.test_utils import approx_equal
import libtbx.load_env
import os

def exercise () :
  mtz_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/maps/fobs.mtz",
    test=os.path.isfile)
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/maps/model.pdb",
    test=os.path.isfile)
  pdb_in = pdb.input(pdb_file)
  xrs = pdb_in.xray_structure_simple()
  mtz_in = mtz.object(mtz_file)
  ma = mtz_in.as_miller_arrays()
  f_obs = ma[0]
  flags = ma[1].generate_bijvoet_mates()
  flags = flags.customized_copy(data=flags.data()==1)
  fmodel = fmodel_manager(
    f_obs=f_obs,
    xray_structure=xrs,
    r_free_flags=flags,
    twin_law="l,-k,h")
  assert fmodel.is_twin_fmodel_manager()
  # anomalous difference map
  map_coeffs = fmodel.map_coefficients(map_type="anom")
  anom_map = map_coeffs.fft_map().apply_sigma_scaling().real_map_unpadded()
  map_vals = anom_map.as_1d()
  assert approx_equal(flex.max(anom_map.as_1d()), 8.445, eps=0.001)
  # Se sites
  site1 = (-0.4494, -0.6654, -0.8777)
  site2 = (-0.4104,  0.0152, -0.0174)
  val1 = anom_map.eight_point_interpolation(site1)
  val2 = anom_map.eight_point_interpolation(site2)
  assert approx_equal(val1, 7.962, eps=0.001)
  assert approx_equal(val2, 7.164, eps=0.001)
  # other map types
  map_coeffs = fmodel.map_coefficients(map_type="mFo")
  assert (type(map_coeffs.data()).__name__ == "complex_double")
  map_coeffs = fmodel.map_coefficients(map_type="DFc")
  assert (type(map_coeffs.data()).__name__ == "complex_double")
  print("OK")

if (__name__ == "__main__") :
  exercise()
