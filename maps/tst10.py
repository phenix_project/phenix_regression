from __future__ import print_function
import iotbx.pdb
import time
from libtbx import easy_run
from iotbx import ccp4_map
from scitbx.array_family import flex
from libtbx.test_utils import approx_equal

def exercise_00(prefix="tst_00_map_values_along_line"):
  """
  Basic test for phenix.map_values_along_line
  """
  pdb_str= """
CRYST1   10.000   10.000   10.000  90.00  90.00  90.00 P 1
HETATM    1  O   HOH A   1       1.000   2.000   3.000  1.00 10.00           O
HETATM    2  O   HOH A   2       4.000   5.000   6.000  1.00 20.00           O
END
"""
  import iotbx.pdb
  pdb_inp = iotbx.pdb.input(lines = pdb_str, source_info=None)
  xrs = pdb_inp.xray_structure_simple()
  cs = xrs.crystal_symmetry()
  fc = xrs.structure_factors(d_min=1).f_calc()
  fft_map = fc.fft_map(resolution_factor=1/4.)
  fft_map.apply_sigma_scaling()
  map_data = fft_map.real_map_unpadded()
  sites_frac = xrs.sites_frac()
  m1 = map_data.tricubic_interpolation(sites_frac[0])
  m2 = map_data.tricubic_interpolation(sites_frac[1])
  #
  map_file = "%s.mrc"%prefix
  ccp4_map.write_ccp4_map(
    file_name   = map_file,
    unit_cell   = cs.unit_cell(),
    space_group = cs.space_group(),
    map_data    = map_data,
    labels      = flex.std_string([""]))
  #
  cmd = " ".join([
    "phenix.map_values_along_line",
    "%s"%map_file,
    "output.file_name=%s.log" % prefix,
    "point_1='1.000   2.000   3.000' ",
    "point_2='4.000   5.000   6.000' ",
    "> %s.zlog"%prefix
    ])
  assert not easy_run.call(cmd)
  #
  mv1, mv2 = None,None
  with open("%s.log" % prefix, "r") as fo:
    for l in fo.readlines():
      if l.startswith(" 0.0000  "): mv1 = float(l.strip().split()[-1])
      if l.startswith(" 5.1962  "): mv2 = float(l.strip().split()[-1])
  assert approx_equal(round(mv1,0), round(35,0), eps=1)
  assert approx_equal(round(mv2,0), round(17,0), eps=1)

if (__name__ == "__main__"):
  t0 = time.time()
  exercise_00()
  print("Time: %6.4f" % (time.time()-t0))
