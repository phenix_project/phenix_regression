
from __future__ import division
from __future__ import print_function
from libtbx import easy_run
from libtbx.introspection import number_of_processors
import libtbx.load_env
import os.path
from scitbx.array_family import flex
import iotbx.mtz
import iotbx.pdb
import random

if (1):
  random.seed(0)
  flex.set_random_seed(0)

mtz_file = libtbx.env.find_in_repositories(
  relative_path="phenix_regression/reflection_files/1yjp.mtz",
  test=os.path.isfile)
pdb_file = libtbx.env.find_in_repositories(
  relative_path="phenix_regression/pdb/1yjp_h.pdb",
  test=os.path.isfile)

def check_map_values (file_name, map_index=0, min_value=1.5, sel_str ="not element H") :
  mtz_in = iotbx.mtz.object(file_name)
  omit_map = mtz_in.as_miller_arrays()[map_index].fft_map( # 2mFo-DFc
    resolution_factor=0.25).apply_sigma_scaling().real_map_unpadded()
  pdb_in = iotbx.pdb.input(pdb_file)
  hierarchy = pdb_in.construct_hierarchy()
  xrs = pdb_in.xray_structure_simple()
  isel = hierarchy.atom_selection_cache().selection(
    sel_str).iselection()
  sum = total = 0
  for i_seq in isel :
    sc = xrs.scatterers()[i_seq]
    site = xrs.sites_frac()[i_seq]
    m = omit_map.eight_point_interpolation(site)
    sum += m
    total += 1
  assert (sum / total > min_value)

def exercise_basic () : # omit_type=None
  if os.path.isfile("tst_composite_omit_map.mtz") :
    os.remove("tst_composite_omit_map.mtz")
  # standard (composite) mode, with refinement disabled
  args = [
    "phenix.composite_omit_map",
    "\"%s\"" % mtz_file,
    "\"%s\"" % pdb_file,
    "nproc=1",
    "omit_type=None",
    "map_type=2mFo-DFc",
    "map_type=3mFo-DFc",
    "box_buffer_radius=0.25",
    "output.file_name=tst_composite_omit_map.mtz",
  ]
  print("calculating composite omit map (omit_type=None)...")
  cmd = " ".join(args)
  print(cmd)
  rc = easy_run.fully_buffered(cmd)
  assert os.path.isfile("tst_composite_omit_map.mtz")
  check_map_values("tst_composite_omit_map.mtz")

def exercise_simple () : # omit_type=simple
  if os.path.isfile("tst_composite_omit_map.mtz") :
    os.remove("tst_composite_omit_map.mtz")
  # standard (composite) mode, with refinement disabled
  args = [
    "phenix.composite_omit_map",
    "\"%s\"" % mtz_file,
    "\"%s\"" % pdb_file,
    "nproc=1",
    "omit_type=simple",
    "map_type=2mFo-DFc",
    "map_type=3mFo-DFc",
    "box_buffer_radius=0.25",
    "output.file_name=tst_composite_omit_map.mtz",
  ]
  print("calculating composite omit map (omit_type=simple)...")
  rc = easy_run.fully_buffered(" ".join(args))
  assert os.path.isfile("tst_composite_omit_map.mtz")
  check_map_values("tst_composite_omit_map.mtz")

def exercise_single () :
  # now in single-region mode with an atom selection
  args = [
    "phenix.composite_omit_map",
    "\"%s\"" % mtz_file,
    "\"%s\"" % pdb_file,
    "nproc=1",
    "omit_type=None",
    "map_type=mFo-DFc",
    "output.file_name=tst_composite_omit_map2.mtz",
    "selection='resname TYR and not (name C or name N or name O or name CA)'",
    "--single",
  ]
  print("calculating single mFo-DFc omit map (omit_type=None)...")
  rc = easy_run.fully_buffered(" ".join(args))
  assert os.path.isfile("tst_composite_omit_map2.mtz")
  check_map_values(
      file_name="tst_composite_omit_map2.mtz",
      min_value=3.,
      sel_str="resname TYR and not (name C or name N or name O or name CA or element H)")

def exercise_refined (anneal=False) : # omit_type=refine|anneal
  if os.path.isfile("tst_composite_omit_map3.mtz") :
    os.remove("tst_composite_omit_map3.mtz")
  omit_type = "refine"
  if (anneal) : omit_type = "anneal"
  args = [
    "phenix.composite_omit_map",
    "\"%s\"" % mtz_file,
    "\"%s\"" % pdb_file,
    "nproc=%d" % min(24, number_of_processors(return_value_if_unknown=1)),
    "omit_type=%s" % omit_type,
    "map_type=2mFo-DFc",
    "map_type=3mFo-DFc",
    "box_buffer_radius=0.25",
    "output.file_name=tst_composite_omit_map3.mtz",
    "> tst_composite_omit_map_exercise_refined.zlog",
  ]
  print("calculating refined composite omit map (anneal=%s)..." % anneal)
  cmd = " ".join(args)
  print(cmd)
  rc = easy_run.call(cmd)
  assert (rc == 0)
  assert os.path.isfile("tst_composite_omit_map3.mtz")

if (__name__ == "__main__") :
  # XXX test multiprocessing!
  os.environ["LIBTBX_FORCE_PARALLEL"] = "1"
  exercise_basic()
  exercise_simple()
  exercise_single()
  exercise_refined()
  exercise_refined(anneal=True) # This one takes almost all the runtime (5 out of 5.5 minutes)
  print("OK")
