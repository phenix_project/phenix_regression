from __future__ import print_function
import libtbx.load_env
import iotbx.pdb
import os, time
from libtbx import easy_run

def exercise_00(prefix="maps_test9"):
  """
  Make sure it does not crash due to rounding differences in unit cell
  parameters between PDB and data files.
  """
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/maps/test_maps9.pdb",
    test=os.path.isfile)
  hkl_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/maps/test_maps9.cif",
    test=os.path.isfile)
  #
  cmd = " ".join([
    "phenix.maps",
    "%s"%pdb_file,
    "%s"%hkl_file,
    "output.prefix=%s" % prefix,
    "> %s.zlog"%prefix
    ])
  assert not easy_run.call(cmd)

if (__name__ == "__main__"):
  t0 = time.time()
  exercise_00()
  print("Time: %6.4f" % (time.time()-t0))
