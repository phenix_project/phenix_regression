from __future__ import print_function
from libtbx import easy_run
import libtbx.load_env
import os

params = """\
maps {
  input {
    pdb_file_name = %s
    reflection_data {
      file_name = %s
      labels = f-obs
      high_resolution = 1.8
         low_resolution = 29.876
      outliers_rejection = True
      sigma_fobs_rejection_criterion = 0.0
      sigma_iobs_rejection_criterion = 0.0
      r_free_flags {
        file_name = %s
        label = R-free-flags
        ignore_r_free_flags = True
      }
    }
  }
  output {
    directory = None
    prefix = maps4
    job_title = None
    fmodel_data_file_format = mtz
  }
  bulk_solvent_correction = False
  anisotropic_scaling = False
  skip_twin_detection = True

  map_coefficients {
    map_type = 2mFo-DFc
    format = *mtz phs
    mtz_label_amplitudes = 2mFoDFc
    mtz_label_phases = P2mFoDFc
    fill_missing_f_obs = True
    acentrics_scale = 2.0
    centrics_pre_scale = 1.0
    sharpening = %s
    sharpening_b_factor = None
    exclude_free_r_reflections = True
    isotropize = %s
  }
  map {
     map_type = 2mFo-DFc
     format = *xplor ccp4
     file_name = None
     fill_missing_f_obs = False
     grid_resolution_factor = 1/4.
     scale = *sigma volume
     region = *selection cell
     atom_selection = None
     atom_selection_buffer = 3
     acentrics_scale = 2.0
     centrics_pre_scale = 1.0
     sharpening = False
     sharpening_b_factor = None
    exclude_free_r_reflections = True
   }
}
"""

def exercise_00():
  global params
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/maps/test_maps4.pdb",
    test=os.path.isfile)
  hkl_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/maps/test_maps4.mtz",
    test=os.path.isfile)
  file_names = ["maps4.eff", "maps4.log", "maps4_2mFo-DFc_map.xplor",
    "maps4_fmodel.mtz", "maps4_map_coeffs.mtz"]
  for isotropize in [True, False]:
    for resharp in [True, False]:
      params_=params%(pdb_file,hkl_file,hkl_file, str(isotropize), str(resharp))
      open(file_names[0],"w").write(params_)
      assert not easy_run.call("phenix.maps %s > %s"%(file_names[0],file_names[1]))
      for i in [2,3,4]:
        assert os.path.isfile(file_names[i])
      assert not easy_run.call("rm -rf %s" % " ".join(file_names))

if (__name__ == "__main__"):
  exercise_00()
  from libtbx.utils import format_cpu_times
  print(format_cpu_times())
