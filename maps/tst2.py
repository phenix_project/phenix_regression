from __future__ import print_function
from libtbx import easy_run
import libtbx.load_env
import os

params = """\
maps {
   input {
     pdb_file_name = %s
     reflection_data {
       high_resolution = 4.0
       file_name = %s
       r_free_flags {
         file_name = %s
       }
     }
   }
   output {
     prefix = "tst_maps2"
     include_r_free_flags = True
   }
   bulk_solvent_correction = False
   anisotropic_scaling = False
   skip_twin_detection = True
   map_coefficients {
     map_type = 2mFo-DFc
     format = *mtz phs
     mtz_label_amplitudes = "2FOFCWT"
     mtz_label_phases = "PHI2FOFCWT"
     fill_missing_f_obs = True
     acentrics_scale = 2.000000
     centrics_pre_scale = 1.000000
     sharpening = False
     sharpening_b_factor = None
   }
   map_coefficients {
     map_type = mFo-DFc
     format = *mtz phs
     mtz_label_amplitudes = "FOFCWT"
     mtz_label_phases = "PHIFOFCWT"
     fill_missing_f_obs = False
     acentrics_scale = 2.000000
     centrics_pre_scale = 1.000000
     sharpening = False
     sharpening_b_factor = None
   }
}
"""

def exercise_00():
  global params
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/maps/test_map2.pdb",
    test=os.path.isfile)
  hkl_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/maps/test_map2.mtz",
    test=os.path.isfile)
  params=params%(pdb_file,hkl_file,hkl_file)
  open("maps2.eff","w").write(params)
  assert not easy_run.call("phenix.maps maps2.eff > maps2.log")
  counter = 0
  counter2 = 0
  for line in open("maps2.log").readlines():
    line = line.strip()
    if(line.count("Space group: P 21 21 21 (No. 19)")): counter += 1
    if(line.count("Space group:")): counter2 += 1
  assert counter == 1, counter
  assert counter == counter2
  from iotbx import mtz
  mtz_in = mtz.object("tst_maps2_map_coeffs.mtz")
  arrays = [ a.info().label_string() for a in mtz_in.as_miller_arrays() ]
  assert (arrays == ['2FOFCWT,PHI2FOFCWT', 'FOFCWT,PHIFOFCWT', 'FreeR_flag'])

if (__name__ == "__main__"):
  exercise_00()
  from libtbx.utils import format_cpu_times
  print(format_cpu_times())
