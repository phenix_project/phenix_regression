from __future__ import absolute_import, division, print_function
from libtbx import easy_run
import time
import iotbx.pdb

pdb_str = """\
CRYST1  136.780  136.780  430.110  90.00  90.00 120.00 H 3 2
ATOM     64  N   THR D 213     -43.079  -7.188 -29.453  1.00330.29           N
ATOM     65  CA  THR D 213     -43.179  -6.333 -28.274  1.00421.63           C
ATOM     66  C   THR D 213     -43.589  -4.924 -28.677  1.00278.83           C
ATOM     67  O   THR D 213     -42.957  -4.316 -29.543  1.00220.31           O
ATOM     68  CB  THR D 213     -41.844  -6.288 -27.525  1.001052.8           C
ATOM     69  OG1 THR D 213     -41.540  -7.586 -27.001  1.001167.7           O
ATOM     70  CG2 THR D 213     -41.895  -5.278 -26.386  1.00342.05           C
ATOM     71  H   THR D 213     -42.402  -7.007 -29.952  1.00397.80           H
ATOM     72  HA  THR D 213     -43.863  -6.689 -27.686  1.00507.41           H
ATOM     73  HB  THR D 213     -41.143  -6.015 -28.138  1.00999.99           H
ATOM     74  HG1 THR D 213     -42.124  -7.814 -26.441  1.00999.99           H
ATOM     75 HG21 THR D 213     -41.138  -5.409 -25.793  1.00411.91           H
ATOM     76 HG22 THR D 213     -41.865  -4.376 -26.741  1.00411.91           H
ATOM     77 HG23 THR D 213     -42.715  -5.391 -25.880  1.00411.91           H
TER
END
"""

def run(prefix="tst_phenix_reduce"):
  """
  Make sure Reduce does not output *** for Biso field of ATOM records.
  """
  #
  pdb_inp = iotbx.pdb.input(source_info=None, lines=pdb_str)
  ph = pdb_inp.construct_hierarchy()
  ph.write_pdb_file(file_name="%s.pdb"%prefix,
    crystal_symmetry=pdb_inp.crystal_symmetry())
  #
  cmd = " ".join([
    "phenix.reduce",
    "%s.pdb"%prefix,
    ">%s_output.pdb"%prefix])
  fb = easy_run.fully_buffered(cmd)
  assert fb.return_code == 0
  #
  xrs = iotbx.pdb.input(
    file_name="%s_output.pdb"%prefix).xray_structure_simple()

if (__name__ == "__main__"):
  t0=time.time()
  run()
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
