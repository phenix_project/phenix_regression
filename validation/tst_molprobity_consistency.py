from __future__ import division
from __future__ import print_function
from libtbx.test_utils import approx_equal
from libtbx import easy_run
import libtbx.load_env
import time
import os

pdb_str_1 = """\
ATOM   2599  N   GLU B 102      10.750  24.795  38.233  1.00160.20           N
ATOM   2600  CA  GLU B 102      10.127  23.473  38.272  1.00160.16           C
ATOM   2601  C   GLU B 102      10.760  22.538  39.301  1.00160.20           C
ATOM   2602  O   GLU B 102      10.247  22.353  40.409  1.00160.05           O
ATOM   2603  CB  GLU B 102      10.201  22.784  36.900  1.00160.01           C
ATOM   2604  CG  GLU B 102      10.015  23.667  35.672  1.00160.30           C
ATOM   2605  CD  GLU B 102      11.268  24.443  35.314  1.00160.90           C
ATOM   2606  OE1 GLU B 102      12.349  24.108  35.843  1.00160.79           O
ATOM   2607  OE2 GLU B 102      11.173  25.393  34.512  1.00161.20           O
ATOM   2608  N   TYR B 103      11.913  21.965  38.911  1.00160.46           N
ATOM   2609  CA  TYR B 103      12.696  20.881  39.510  1.00160.67           C
ATOM   2610  C   TYR B 103      11.911  19.567  39.489  1.00160.50           C
ATOM   2611  O   TYR B 103      10.675  19.583  39.558  1.00160.30           O
ATOM   2612  CB  TYR B 103      13.163  21.242  40.922  1.00160.90           C
ATOM   2613  CG  TYR B 103      14.611  21.693  40.937  1.00161.78           C
ATOM   2614  CD1 TYR B 103      15.573  20.999  40.220  1.00162.91           C
ATOM   2615  CD2 TYR B 103      15.017  22.799  41.661  1.00161.97           C
ATOM   2616  CE1 TYR B 103      16.892  21.390  40.218  1.00162.89           C
ATOM   2617  CE2 TYR B 103      16.345  23.199  41.665  1.00162.09           C
ATOM   2618  CZ  TYR B 103      17.275  22.488  40.940  1.00162.46           C
ATOM   2619  OH  TYR B 103      18.597  22.870  40.931  1.00162.27           O
ATOM   2620  N   PRO B 104      12.591  18.399  39.401  1.00160.56           N
ATOM   2621  CA  PRO B 104      11.862  17.139  39.188  1.00160.98           C
ATOM   2622  C   PRO B 104      11.534  16.339  40.442  1.00161.63           C
ATOM   2623  O   PRO B 104      11.946  16.686  41.553  1.00161.53           O
ATOM   2624  CB  PRO B 104      12.823  16.354  38.292  1.00160.78           C
ATOM   2625  CG  PRO B 104      14.168  16.757  38.812  1.00160.42           C
ATOM   2626  CD  PRO B 104      14.044  18.209  39.242  1.00160.38           C
ATOM   2627  N   VAL B 105      10.777  15.263  40.251  1.00162.56           N
ATOM   2628  CA  VAL B 105      10.585  14.214  41.255  1.00163.66           C
ATOM   2629  C   VAL B 105      11.676  13.147  41.177  1.00164.39           C
ATOM   2630  O   VAL B 105      12.040  12.543  42.187  1.00164.59           O
ATOM   2631  CB  VAL B 105       9.179  13.591  41.106  1.00163.63           C
ATOM   2632  CG1 VAL B 105       8.863  12.689  42.287  1.00163.77           C
ATOM   2633  CG2 VAL B 105       8.130  14.683  40.960  1.00163.85           C
"""

def exercise_consistency(prefix="molprobity_consistency"):
  """
  Exercise that all molprobity tools gives the same answer:
  molprobity, clashscore, ramalyze, rotalyze, cbetadev
  """
  pdb_path_1 = "%s_test_1.pdb" % prefix
  f = open(pdb_path_1, "w")
  f.write(pdb_str_1)
  f.close()
  pdb_path_2 = libtbx.env.find_in_repositories(
        relative_path="phenix_regression/pdb/3bdn.pdb",
        test=os.path.isfile)

  for path in [pdb_path_1, pdb_path_2]:
    print("Working with", path)
    cmd = " ".join([
      "phenix.molprobity",
      "%s" % path,
      "> %s_mol.log" % prefix,
      "2>%s_mol.err" % prefix,
      ])
    print(cmd)
    assert not easy_run.call(cmd)
    f = open("%s_mol.log" % prefix, "r")
    for l in f.readlines():
      if l.startswith("  Ramachandran outliers"):
        molpr_rama_out = float(l.split()[3])
      if l.startswith("                favored "):
        molpr_rama_fav = float(l.split()[2])
      if l.startswith("  Rotamer outliers"):
        molpr_rot_out = float(l.split()[3])
      if l.startswith("  C-beta deviations"):
        molpr_cbeta = int(l.split()[3])
      if l.startswith("  Clashscore     "):
        molpr_clash = float(l.split()[2])
    f.close()

    cmd = " ".join([
      "phenix.ramalyze",
      "%s" % path,
      "> %s_ram.log" % prefix,
      "2>%s_ram.err" % prefix,
      ])
    print(cmd)
    assert not easy_run.call(cmd)
    f = open("%s_ram.log" % prefix, "r")
    for l in f.readlines():
      if l.endswith(" outliers (Goal: < 0.2%)\n"):
        rama_out = float(l.split()[1][:-1])
      if l.endswith(" favored (Goal: > 98%)\n"):
        rama_fav = float(l.split()[1][:-1])
    f.close()
    assert approx_equal(molpr_rama_out, rama_out)
    assert approx_equal(molpr_rama_fav, rama_fav)
    print("ramalyze OK")

    cmd = " ".join([
      "phenix.rotalyze",
      "%s" % path,
      "> %s_rot.log" % prefix,
      "2>%s_rot.err" % prefix,
      ])
    print(cmd)
    assert not easy_run.call(cmd)
    f = open("%s_rot.log" % prefix, "r")
    for l in f.readlines():
      if l.endswith(" outliers (Goal: < 0.3%)\n"):
        rot_out = float(l.split()[1][:-1])
    f.close()
    assert approx_equal(molpr_rot_out, rot_out)
    print("rotalyze OK")

    cmd = " ".join([
      "phenix.cbetadev",
      "%s" % path,
      "> %s_cbeta.log" % prefix,
      "2>%s_cbeta.err" % prefix,
      ])
    print(cmd)
    assert not easy_run.call(cmd)
    f = open("%s_cbeta.log" % prefix, "r")
    for l in f.readlines():
      if l.startswith("SUMMARY:"):
        cbet_out = int(l.split()[1])
    f.close()
    assert approx_equal(molpr_cbeta, cbet_out)
    print("cbetadev OK")

    cmd = " ".join([
      "phenix.clashscore",
      "%s" % path,
      "> %s_clash.log" % prefix,
      "2>%s_clash.err" % prefix,
      ])
    print(cmd)
    assert not easy_run.call(cmd)
    f = open("%s_clash.log" % prefix, "r")
    for l in f.readlines():
      if l.startswith("clashscore ="):
        clash = float(l.split()[2])
    f.close()
    assert approx_equal(molpr_clash, clash)
    print("clashscore OK")

if (__name__ == "__main__"):
  t0 = time.time()
  exercise_consistency()
  print("OK. Time: %8.3f"%(time.time()-t0))
