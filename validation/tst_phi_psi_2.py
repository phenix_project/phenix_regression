from mmtbx.regression import model_1yjp
from phenix.programs import phi_psi_2

from iotbx.data_manager import DataManager

def main():
  dm = DataManager()
  dm.process_model_str('testing', model_1yjp)
  model = dm.get_model()
  results = phi_psi_2.results_manager(model=model)
  counts = results.get_motif_count()
  assert counts['GENERAL']==0
  assert counts['MOTIF']==1
  assert counts['MOTIF20']==2
  assert counts['MOTIF...']==1
  assert counts['CIS']==0
  assert counts['OUTLIER']==0
  assert counts['ERROR']==0
  histogram = results.get_histogram()
  len(histogram.keys())==1
  assert histogram['PB']==1
  results.get_output_lines()
  results.get_output_lines(outliers_only=True)
  results.get_output_lines(cis_only=True)
  results.get_output_lines(motif_name_only=True)
  results.get_sequence_alignments()
  results.get_motif_sequences()
  return 0

if __name__ == '__main__':
  rc = main()
  assert rc==0
