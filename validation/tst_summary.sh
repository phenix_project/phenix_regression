#!/bin/sh

PHENIX_REGRESSION_DIR=`libtbx.find_in_repositories phenix_regression`
mmtbx.validation_summary "${PHENIX_REGRESSION_DIR}"/pdb/1ywf.pdb > tmp_summary.log
grep "Ramachandran" tmp_summary.log | libtbx.assert_stdin_contains_strings "0.42 %"
grep "R-work" tmp_summary.log | libtbx.assert_stdin_contains_strings "0.1760"
echo "OK"
