
from __future__ import division
from __future__ import print_function
from mmtbx.kinemage import validation
from libtbx.test_utils import assert_lines_in_file
import libtbx.load_env
import os

test_hb_string = """
ATOM    249  N   HIS    31      38.506  15.896  16.721  1.00 19.64      
ATOM    250  CA  HIS    31      37.409  16.218  17.597  1.00 11.92      
ATOM    251  C   HIS    31      37.043  17.688  17.490  1.00 30.90      
ATOM    252  O   HIS    31      37.643  18.585  18.066  1.00 16.95      
ATOM    253  CB  HIS    31      37.690  15.864  19.036  1.00 11.25      
ATOM    254  CG  HIS    31      36.468  16.091  19.824  1.00 24.45      
ATOM    255  ND1 HIS    31      35.452  15.139  19.887  1.00 16.82      
ATOM    256  CD2 HIS    31      36.079  17.191  20.508  1.00 38.86      
ATOM    257  CE1 HIS    31      34.494  15.654  20.645  1.00 19.90      
ATOM    258  NE2 HIS    31      34.836  16.893  21.037  1.00 22.96      
ATOM    525  N   ASP    70      33.712   9.453  19.798  1.00 12.28      
ATOM    526  CA  ASP    70      33.315   9.903  18.497  1.00 12.70      
ATOM    527  C   ASP    70      33.103   8.789  17.475  1.00 21.50      
ATOM    528  O   ASP    70      32.254   8.944  16.586  1.00 18.20      
ATOM    529  CB  ASP    70      34.306  10.934  17.920  1.00 13.97      
ATOM    530  CG  ASP    70      34.349  12.210  18.707  1.00 33.33      
ATOM    531  OD1 ASP    70      33.574  12.474  19.582  1.00 19.55      
ATOM    532  OD2 ASP    70      35.356  12.959  18.391  1.00 22.23      
"""

test_file_name = os.path.splitext(os.path.basename(__file__))[0]

def exercise_kinemage_hb():
  with open(test_file_name+"_hb.pdb", "w") as test_pdb:
    test_pdb.write(test_hb_string)
  args=[]
  args.append(test_file_name+"_hb.pdb")
  args.append("out_file="+test_file_name+"_hb.kin")
  args.append('flip_sym=False')
  validation.run(args=args)
  assert_lines_in_file(test_file_name+"_hb.kin", "{ HD1 HIS  31   }greentint  'S' 35.580,14.198,18.480")
  assert_lines_in_file(test_file_name+"_hb.kin", "{ OD2 ASP  70   }greentint  'S' 34.967,14.291,18.578")

def run():
  if (not libtbx.env.has_module(name="probe")):
    print("Skipping kinemage test:" \
      " probe not available")
  elif (not libtbx.env.has_module(name="reduce")):
    print("Skipping kinemage test:" \
      " reduce not available")
  else:
    exercise_kinemage_hb()
  print("OK")

if (__name__ == "__main__"):
  run()
