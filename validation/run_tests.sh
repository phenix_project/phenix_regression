#!/bin/sh

VALIDATION_TESTS=`libtbx.find_in_repositories "phenix_regression/validation"`
echo "testing Molprobity protein validation..."
phenix.python $VALIDATION_TESTS/tst_molprobity_protein.py
echo "testing Molprobity RNA validation..."
phenix.python $VALIDATION_TESTS/tst_molprobity_rna.py
echo "testing geometry statistics..."
phenix.python $VALIDATION_TESTS/tst_geometry.py
echo "testing comprehensive validation driver..."
phenix.python $VALIDATION_TESTS/tst_comprehensive.py
echo "testing suitename..."
phenix.python $VALIDATION_TESTS/tst_suitename.py
echo "testing reduce..."
phenix.python $VALIDATION_TESTS/test_reduce.py
