from __future__ import division
from __future__ import print_function
from libtbx import easy_run
from iotbx.pdb.fetch import fetch_and_write
import os

#
# We are in phenix_regression, therefore I'm not testing for reduce
# availability.
#

# After closer examination, I found out that 5ucy already contains hydrogen atoms.
# When I remove them, reduce works.
# I don't see why reduce cannot do it itself (remove H atoms when they are present),
# especially having -trim option to do just that.

# Note from Pavel:
# Not all H are present, but only on some N and O.
# Also, if you run a round of geometry minimization then Reduce will add H to
# this model (no need to remove H first).
# This makes me suspect that Reduce doesn't like some particular geometries
# in this file.

def tst_reduce_empty():
  fetch_and_write(id="5ucy")
  assert os.path.isfile("5ucy.pdb")
  cmd = "phenix.reduce 5ucy.pdb"
  print(cmd)
  build_out = easy_run.fully_buffered(cmd)
  reduce_str = '\n'.join(build_out.stdout_lines)
  assert len(reduce_str) > 0, "For Duke group to fix: reduce fails to produce output model."

if (__name__ == "__main__"):
  tst_reduce_empty()
