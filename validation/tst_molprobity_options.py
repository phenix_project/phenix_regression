from __future__ import division
from __future__ import print_function
import libtbx.load_env
from libtbx import easy_run
import os

def exercise () :
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/1yjp_h.pdb",
    test=os.path.isfile)
  mtz_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/reflection_files/1yjp.mtz",
    test=os.path.isfile)
  assert (not None in [pdb_file, mtz_file])
  if not os.path.exists('molp_options'): os.mkdir('molp_options')
  os.chdir('molp_options')

  cmd = 'phenix.molprobity %s' % pdb_file
  print('\n ~> %s' % cmd)
  rc = easy_run.go(cmd)
  assert rc.return_code==0
  cmd += ' output.pickle=True'
  print('\n ~> %s' % cmd)
  rc = easy_run.go(cmd)
  rc.show_stdout()
  assert rc.return_code==0, 'return_code is %s' % rc.return_code

  cmd = 'phenix.molprobity %s %s' % (pdb_file, mtz_file)
  print('\n ~> %s' % cmd)
  rc = easy_run.go(cmd)
  assert rc.return_code==0
  cmd += ' output.pickle=True'
  print('\n ~> %s' % cmd)
  rc = easy_run.go(cmd)
  rc.show_stdout()
  assert rc.return_code==0, 'return_code is %s' % rc.return_code

if (__name__ == "__main__") :
  exercise()
  print("OK")
