from __future__ import print_function

from phenix.automation import structure_comparison
from iotbx import pdb
from libtbx import easy_pickle
from libtbx import easy_run
from libtbx.utils import Sorry
from libtbx.test_utils import Exception_expected
import libtbx.load_env
from six.moves import cStringIO as StringIO
import sys, os, time

def find_file (short_name) :
  file_name = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/validation/%s" % short_name,
    test=os.path.isfile)
  assert file_name is not None
  return file_name

def which(program):
  def is_exe(fpath):
    return os.path.exists(fpath) and os.access(fpath, os.X_OK)
  def ext_candidates(fpath):
    yield fpath
    for ext in os.environ.get("PATHEXT", "").split(os.pathsep):
      yield fpath + ext
  fpath, fname = os.path.split(program)
  if fpath:
    if is_exe(program):
      return program
  else:
    for path in os.environ["PATH"].split(os.pathsep):
      exe_file = os.path.join(path, program)
      for candidate in ext_candidates(exe_file):
        if is_exe(candidate):
          return candidate
  return None

def exercise_condensation (debug=False) :
  def switch(filename, line1, line2):
    f = open(filename, 'r')
    lines = f.read()
    f.close()
    assert lines.find(line1)>-1
    lines = lines.replace(line1, line2)
    f = open(filename, 'w')
    f.write(lines)
    f.close()
    ###
  def add_water_to_chain_A(filename):
    pdb_inp = pdb.input(filename)
    pdb_hierarchy = pdb_inp.construct_hierarchy()
    for chain in pdb_hierarchy.chains():
      chain.id = 'A'
    pdb_hierarchy.write_pdb_file(filename,
                                 crystal_symmetry=pdb_inp.crystal_symmetry(),
                                 )
    ###
  if not which('phenix.condensation'): return
  if debug :
    out = sys.stdout
  else :
    out = StringIO()
  old_pwd = os.getcwd()
  if (not os.path.exists("sc_tmp5")) :
    os.makedirs("sc_tmp5")
  os.chdir("sc_tmp5")
  tar_file = find_file("1yjp_compare.tar.gz")
  assert not easy_run.call("tar zxf %s > /dev/null" % tar_file)
  ids = ["1yjp", "1yjp-mod1", "1yjp-mod2", "1yjp-mod3"]
  pdb_files = [ "%s.pdb" % id for id in ids ]
  for pdb_file in pdb_files: add_water_to_chain_A(pdb_file)
  if 1:
    cmd = 'phenix.condensation %s' % (' '.join(pdb_files))
    print(cmd)
    rc = easy_run.call(cmd)
    assert rc==0
  change = 'O   HOH A   8      -6.471   5.227   7.124  1.00 22.62           O'
  questions = [
    'O   HOH A   8      -7.471   5.227   7.124  1.00 22.62           O',
    'O   HOH A   8     -17.471   5.227   7.124  1.00 22.62           O',
    'CA  CA  A   8      -6.471   5.227   7.124  1.00 22.62          CA',
    'CA  CA  A   8      26.471   5.227   7.124  1.00 22.62          CA',
    ]
  answers = [
    {'percent_condensed': {4: 100.0}},
    {'percent_condensed': {1: 12.5, 3: 12.5, 4: 75.0}},
    {'percent_condensed': {4: 100.0}},
    {'percent_condensed': {1: 12.5, 3: 12.5, 4: 75.0}},
    ]
  analyses = ['water']
  args = pdb_files + ["1yjp.fa"]
  args.append("analyses=%s" % "+".join(analyses))
  rc=None
  for i, c in enumerate(questions):
    switch('1yjp-mod1.pdb', change, c)
    _results = structure_comparison.run(args, log=out)
    results = easy_pickle.loads(easy_pickle.dumps(_results))
    rc = results.clusters.validate()
    assert rc==answers[i], "%s != %s" % (rc, answers[i])
    assert not easy_run.call("tar zxf %s > /dev/null" % tar_file)
    for pdb_file in pdb_files: add_water_to_chain_A(pdb_file)
  os.chdir(old_pwd)

def exercise_simple (nproc=1, debug=False) :
  if debug :
    out = sys.stdout
  else :
    out = StringIO()
  old_pwd = os.getcwd()
  if (not os.path.exists("sc_tmp1")) :
    os.makedirs("sc_tmp1")
  os.chdir("sc_tmp1")
  tar_file = find_file("1yjp_compare.tar.gz")
  assert not easy_run.call("tar zxf %s > /dev/null" % tar_file)
  ids = ["1yjp", "1yjp-mod1", "1yjp-mod2", "1yjp-mod3"]
  pdb_files = [ "%s.pdb" % id for id in ids ]
  mtz_files = [ "%s.mtz" % id for id in ids ]
  analyses = ["rotamers","ramachandran","secondary_structure","missing_atoms",
    "b_factor"]
  args = pdb_files + ["1yjp.fa"]
  args.append("analyses=%s" % "+".join(analyses))
  args.append("nproc=%d" % nproc)
  args.extend([ "xray_data.file_name=%s" % os.path.abspath(f)
                for f in mtz_files ])
  _results = structure_comparison.run(args, log=out)
  results = easy_pickle.loads(easy_pickle.dumps(_results))
  assert os.path.isfile("1yjp-mod2_A_2mFo-DFc.ccp4")
  assert os.path.isfile(".rotamers.pml")
  assert os.path.isfile(".files.pkl")
  # now without a sequence
  args = list(pdb_files)
  import libtbx.callbacks
  out2 = StringIO()
  libtbx.call_back.set_warning_log(out2)
  args.append("analyses=%s" % "+".join(analyses))
  args.append("nproc=%d" % nproc)
  args.extend([ "xray_data.file_name=%s" % os.path.abspath(f)
                for f in mtz_files ])
  results = structure_comparison.run(args, log=out)
  output = results['rotamers'].get_residue_data()
  assert (len(output) == 4)
  assert (out2.getvalue() == """
  WARNING: No sequence file provided - defaulting to first chain found in
  1yjp.pdb.

""")
  # now with superpositioning
  args.append("1yjp.fa")
  args.append("superpose_processed_structures=True")
  _results = structure_comparison.run(args, log=out)
  results = easy_pickle.loads(easy_pickle.dumps(_results))
  output = results['rotamers'].get_residue_data()
  assert (len(output) == 4)
  output = results['ramachandran'].get_residue_data()
  #print output
  assert (len(output) == 3)
  output = results['missing_atoms'].get_residue_data()
  assert (output[1][0] == "3 ASN"), 'output %s not "3 ASN"' % output[0][0]
  assert (output[2][0] == "7 TYR")
  assert os.path.isfile("1yjp-mod2_map_coeffs.mtz")
  assert os.path.isfile("1yjp-mod2_A_2mFo-DFc.ccp4")
  # PyMOL selection strings
  file_name, chain_id = results.file_names_and_chain_ids[1]
  pymol_sele = results['rotamers'].format_pymol_selection(file_name,chain_id)
  assert (pymol_sele == "chain 'A' and ((resi 3:4) or (resi 6:7))")
  file_name, chain_id = results.file_names_and_chain_ids[3]
  pymol_sele = results['rotamers'].format_pymol_selection(file_name,chain_id)
  #print pymol_sele
  #assert (pymol_sele == "chain 'A' and ((resi 4) or (resi 6))")
  # rebuild_edited_full_structures (part of Coot plugin)
  for file_name_old in results.processed_file_names :
    file_base, ext = os.path.splitext(file_name_old)
    file_name_new = file_base + "_coot.pdb"
    pdb_in = pdb.input(file_name_old)
    hierarchy = pdb_in.construct_hierarchy()
    chain = hierarchy.models()[0].chains()[0]
    i_seq = 0
    for residue_group in chain.residues() :
      if (residue_group.resseq_as_int() == 4) :
        chain.remove_residue_group(i_seq)
        break
      i_seq += 1
    f = open(file_name_new, "w")
    f.write(hierarchy.as_pdb_string())
    f.close()
  out = StringIO()
  rebuilt_files = results.rebuild_edited_full_structures(log=out)
  assert (out.getvalue().count("number of residues changed") == 4)
  assert os.path.isfile("1yjp-mod2_modified.pdb")
  for file_name in pdb_files :
    pdb_old = pdb.input(file_name)
    n_atoms_old = pdb_old.construct_hierarchy().atoms().size()
    new_file = os.path.splitext(file_name)[0] + "_modified.pdb"
    pdb_new = pdb.input(new_file)
    pdb_new_h = pdb_new.construct_hierarchy()
    assert (pdb_new.crystal_symmetry() is not None)
    n_atoms_new = pdb_new_h.atoms().size()
    assert ((n_atoms_old - n_atoms_new) == 9)
    resname_counts = pdb_new_h.overall_counts().resnames
    assert (resname_counts['HOH'] == 7)
  # with atom selection
  args.append("atom_selection=all")
  _results = structure_comparison.run(args, log=out)
  results = easy_pickle.loads(easy_pickle.dumps(_results))
  output = results['rotamers'].get_residue_data()
  assert (len(output) == 6)
  os.chdir(old_pwd)

def exercise_msa (nproc=1, debug=False) :
  if debug :
    out = sys.stdout
  else :
    out = StringIO()
  old_pwd = os.getcwd()
  if (not os.path.exists("sc_tmp2")) :
    os.makedirs("sc_tmp2")
  os.chdir("sc_tmp2")
  tar_file = find_file("1yjp_compare_msa.tar.gz")
  assert not easy_run.call("tar zxf %s > /dev/null" % tar_file)
  ids = ["1yjp", "1yjp-mod1", "1yjp-mod2", "1yjp-mod3"]
  pdb_files = [ "%s.pdb" % id for id in ids ]
  mtz_files = [ "%s.mtz" % id for id in ids ]
  analyses = ["rotamers","ramachandran","secondary_structure","missing_atoms",
    "b_factor"]
  args = pdb_files + ["1yjp.fa"]
  args.append("analyses=%s" % "+".join(analyses))
  args.append("superpose_processed_structures=True")
  args.append("nproc=%d" % nproc)
  args.append("assume_identical_chain_numbering=False")
  # XXX because peptides are so short, need to decrease the %ID cutoff
  args.append("min_acceptable_seq_identity=0.5")
  args.extend([ "xray_data.file_name=%s" % os.path.abspath(f)
                for f in mtz_files ])
  results = structure_comparison.run(args, log=out)
  output = results['rotamers'].get_residue_data()
  #print output
#  assert (len(output) == 4)
  os.chdir(old_pwd)

# test skipping insertion codes (thrombin from different mammals)
def exercise_icode (nproc=1, debug=False) :
  if debug :
    out = sys.stdout
  else :
    out = StringIO()
  old_pwd = os.getcwd()
  if (not os.path.exists("sc_tmp4")) :
    os.makedirs("sc_tmp4")
  os.chdir("sc_tmp4")
  tar_file = find_file("thrombin.tgz")
  assert not easy_run.call("tar zxf %s > /dev/null" % tar_file)
  ids = ["3edx", "3hk6", "3jz2"]
  seq_file = "3edx.fa"
  pdb_files = [ "%s.pdb" % id for id in ids ]
  args = [seq_file] + pdb_files
  analyses = ["rotamers","ramachandran","secondary_structure","missing_atoms",
    "b_factor"]
  args.append("analyses=%s" % "+".join(analyses))
  args.append("superpose_processed_structures=True")
  args.append("nproc=%d" % nproc)
  args.append("min_acceptable_seq_identity=0.95")
  try :
    results = structure_comparison.run(args=args, log=out)
  except Sorry as e :
    assert ("does not contain" in str(e))
  else :
    raise Exception_expected
  args[-1] = "min_acceptable_seq_identity=0.8"
  from libtbx.utils import multi_out
  out_str = StringIO()
  out2 = multi_out()
  out2.register("stdout", out)
  out2.register("str", out_str)
  results = structure_comparison.run(args=args, log=out2)
  log = out_str.getvalue()
  assert (log.count("**** WARNING: 28 residues with insertion code found")==5)
  assert (log.count("**** WARNING: 23 residues with insertion code found")==1)
  output = results['rotamers'].get_residue_data()
  #print len(output)
  assert (len(output) == 123)
  #print output[-10][0]
  #print output[-10][1]
  assert (output[-10][0] == "224 LYS")
  assert (output[-10][1] == ['mmtt', 'mmtt', 'mmtt', 'OUTLIER', 'mptp', 'mttm'])
  output = results['ramachandran'].get_residue_data()

def exercise_big (nproc=1, debug=False) :
  if debug :
    out = sys.stdout
  else :
    out = StringIO()
  old_pwd = os.getcwd()
  if (not os.path.exists("sc_tmp3")) :
    os.makedirs("sc_tmp3")
  os.chdir("sc_tmp3")
  tar_file = find_file("pka_compare.tar.gz")
  assert not easy_run.call("tar zxf %s > /dev/null" % tar_file)
  ids = ["1l3r","1syk","3dnd","3dne","3fhi","3fjq"]
  seq_file = "1l3r.fa"
  pdb_files = [ "%s.pdb" % id for id in ids ]
  mtz_files = [ "%s.mtz" % id for id in ids ]
  args = [seq_file] + pdb_files
  analyses = ["rotamers","ramachandran","secondary_structure","missing_atoms",
    "b_factor"]
  args.append("analyses=%s" % "+".join(analyses))
  args.append("superpose_processed_structures=True")
  args.append("nproc=%d" % nproc)
  args.extend([ "xray_data.file_name=%s" % os.path.abspath(f)
                for f in mtz_files ])
  t1 = time.time()
  _results = structure_comparison.run(args, log=out)
  t2 = time.time()
  print("%.2fs" % (t2 - t1))
  results = easy_pickle.loads(easy_pickle.dumps(_results))
  result = results['rotamers']
  output = result.get_residue_data()
  assert len(output) == 174
  assert output[-1][0] == "349 GLU(6)/None(1)"
  #rotalyze_result.show()
  result = results['ramachandran']
  output = result.get_residue_data()
  assert len(output) == 39
  assert output[10][0] == "129 PHE"
  assert "%d,%d" % tuple(output[9][1][0][0]) == "-142,-165"
  #ramalyze_result.show()
  result = results['secondary_structure']
  output = result.get_residue_data()
  assert len(output) == 15
  assert output[6][0] == "67 ASN"
  assert output[7][1] == ['---','alpha','---','alpha','---','alpha','---']
  result = results['missing_atoms']
  output = result.get_residue_data()
  assert len(output) == 49
  assert output[-3][0] == "345 LYS"
  assert output[-3][1] == [['CG','CD','CE','NZ'],[],[],[],[],['CG','CD','CE','NZ'],[]]
  files = [(f.pdb_file,f.f_map,f.df_map) for f in results.files_for_viewing()]
  for (f1, f2, f3) in files :
    assert os.path.isfile(f1) and os.path.isfile(f2) and os.path.isfile(f3)
  s = easy_pickle.dumps(results)
  os.chdir(old_pwd)

if (__name__ == "__main__") :
  debug = False
  if ("--debug" in sys.argv) :
    debug = True
  nproc = 1
  for arg in sys.argv :
    if (arg.startswith("--nproc=") or arg.startswith("nproc=")) :
      nproc = int(arg.split("=")[1])
  print("exercise_condensation()")
  exercise_condensation(debug=debug)
  print("exercise_simple()")
  exercise_simple(nproc=nproc, debug=debug)
  print("exercise_msa()")
  exercise_msa(nproc=nproc, debug=debug)
  print("exercise_icode()")
  exercise_icode(nproc=nproc, debug=debug)
  if ("PHENIX_COMPREHENSIVE_TESTS" in os.environ) or ("--full" in sys.argv) :
    print("exercise_big()")
    exercise_big(nproc=nproc, debug=debug)
  print("OK")
