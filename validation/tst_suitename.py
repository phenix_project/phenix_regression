from __future__ import division
from __future__ import print_function
from libtbx import easy_run

def exercise_suitename():
  sample_dangle_out = """
1u8d.pdb:1:A:  15: : :  G:__?__:106.935:95.975:74.054:103.458:67.043
1u8d.pdb:1:A:  16: : :  G:-166.96:179.334:62.242:82.926:-161.61:-66.34
1u8d.pdb:1:A:  17: : :  A:-66.703:-170.613:43.207:83.183:-170.628:-76.851
1u8d.pdb:1:A:  18: : :  C:147.758:-155.321:175.336:79.746:-134.418:-70.846
1u8d.pdb:1:A:  19: : :  A:-54.008:174.062:44.315:81.331:-152.902:-71.736
1u8d.pdb:1:A:  20: : :  U:-66.685:177.274:50.651:81.948:-145.811:-66.329
1u8d.pdb:1:A:  21: : :  A:-71.703:176.987:51.995:81.507:-116.046:-73.225
1u8d.pdb:1:A:  22: : :  U:155.751:-158.902:59.996:147.276:-77.626:92.56
1u8d.pdb:1:A:  23: : :  A:49.571:-161.55:167.211:88.961:__?__:__?__
"""
  suitename_command = "phenix.suitename_old -report -"
  suitename = easy_run.fully_buffered(suitename_command,
                         stdin_lines=sample_dangle_out)
  suitename_out = suitename.stdout_lines
  assert suitename_out[0] == '1u8d.pdb:1:A:  15: : :  G inc  __ 0.000'
  assert suitename_out[1] == '1u8d.pdb:1:A:  16: : :  G trig !! 0.000 epsilon-1'
  assert len(suitename_out) == 49

if (__name__ == "__main__"):
  exercise_suitename()
  print("OK")
