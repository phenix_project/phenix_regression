import os, sys
from libtbx import easy_run

pdb_1 = '''
CRYST1   78.073   78.073   37.115  90.00  90.00  90.00 P 43 21 2
ATOM   1085  N   LEU A 129      20.174 -16.248  12.922  1.00 28.62           N
ATOM   1086  CA  LEU A 129      19.944 -16.357  11.494  1.00 37.94           C
ATOM   1087  C   LEU A 129      19.886 -17.800  11.053  1.00 41.26           C
ATOM   1088  O   LEU A 129      20.739 -18.634  11.372  1.00 41.61           O
ATOM   1089  CB  LEU A 129      20.999 -15.584  10.706  1.00 30.17           C
ATOM   1090  CG  LEU A 129      20.861 -14.077  10.905  1.00 26.65           C
ATOM   1091  CD1 LEU A 129      22.094 -13.346  10.396  1.00 29.28           C
ATOM   1092  CD2 LEU A 129      19.589 -13.553  10.242  1.00 32.46           C
ATOM   1093  OXT LEU A 129      18.936 -18.130  10.362  1.00 38.12           O
TER
HETATM 1094 CL    CL A 130      31.510  -8.422  -7.509  1.00 15.45          Cl
HETATM 1095 CL    CL A 131      28.792 -11.458   6.747  1.00 22.11          Cl
HETATM 1096 CL    CL A 132      11.312  11.295 -14.033  1.00 19.10          Cl
HETATM 1097 CL    CL A 133       8.470   5.027 -15.620  1.00 31.37          Cl
HETATM 1098 CL    CL A 134      10.227  -6.819   5.206  1.00 25.51          Cl
HETATM 1099 CL    CL A 135      23.393  -0.761  14.642  1.00 31.81          Cl
HETATM 1100 NA    NA A 136      13.811   7.564 -12.807  1.00 15.61          Na
'''
pdb_2 = '''
CRYST1   78.102   78.102   36.967  90.00  90.00  90.00 P 43 21 2
ATOM   1079  N   LEU A 129      20.190 -16.131  12.924  1.00 27.33           N
ATOM   1080  CA  LEU A 129      19.909 -16.174  11.503  1.00 33.98           C
ATOM   1081  C   LEU A 129      19.783 -17.586  10.984  1.00 41.09           C
ATOM   1082  O   LEU A 129      20.353 -18.545  11.508  1.00 46.76           O
ATOM   1083  CB  LEU A 129      20.991 -15.429  10.725  1.00 33.13           C
ATOM   1084  CG  LEU A 129      20.950 -13.921  10.938  1.00 31.06           C
ATOM   1085  CD1 LEU A 129      22.208 -13.276  10.389  1.00 31.74           C
ATOM   1086  CD2 LEU A 129      19.703 -13.329  10.298  1.00 33.36           C
ATOM   1087  OXT LEU A 129      19.090 -17.759   9.996  1.00 35.40           O
TER
HETATM 1088  C1  GOL A 130      22.091   0.888 -16.384  1.00 40.76           C
HETATM 1089  C2  GOL A 130      23.523   1.435 -16.252  1.00 40.92           C
HETATM 1090  C3  GOL A 130      23.542   2.163 -14.898  1.00 37.14           C
HETATM 1091  O1  GOL A 130      21.935   0.520 -17.719  1.00 47.27           O
HETATM 1092  O2  GOL A 130      24.465   0.414 -16.323  1.00 38.19           O
HETATM 1093  O3  GOL A 130      24.788   2.777 -14.797  1.00 43.38           O
HETATM 1094 CL    CL A 131      11.319  11.387 -13.974  1.00 20.24          Cl
HETATM 1095 CL    CL A 132      31.428  -8.417  -7.624  1.00 17.43          Cl
HETATM 1096 CL    CL A 133      10.246  -6.857   5.124  1.00 29.48          Cl
HETATM 1097 CL    CL A 134      28.836 -11.362   6.694  1.00 23.99          Cl
HETATM 1098 CL    CL A 135       5.615   5.139 -11.181  1.00 28.20          Cl
HETATM 1099 CL    CL A 136      23.467  -0.648  14.556  1.00 26.85          Cl
'''

def main():
  f = open('pdb_1.pdb', 'w')
  f.write(pdb_1)
  f.close()
  f = open('pdb_2.pdb', 'w')
  f.write(pdb_2)
  f.close()
  cmd = 'phenix.condensation pdb_1.pdb pdb_2.pdb analyses=ligands' % locals()
  print(cmd)
  rc = easy_run.go(cmd)
  assert rc.return_code==0

if __name__ == '__main__':
  main()
