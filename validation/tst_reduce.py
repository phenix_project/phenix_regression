from __future__ import division
from __future__ import print_function
from libtbx.test_utils import approx_equal
from libtbx import easy_run
import libtbx.load_env
import time
from mmtbx.monomer_library import server, pdb_interpretation
from cctbx.array_family import flex
from cctbx import geometry_restraints

#protein
pdb_str_1 = """\
CRYST1   41.808   78.159  102.883  90.00  96.82  90.00 P 1 21 1
SCALE1      0.023919  0.000000  0.002861        0.00000
SCALE2      0.000000  0.012794  0.000000        0.00000
SCALE3      0.000000  0.000000  0.009789        0.00000
ATOM      1  N   CYS A   1     -16.102  18.112 -22.349  1.00 44.69           N
ATOM      2  CA  CYS A   1     -17.426  18.320 -22.923  1.00 44.75           C
ATOM      3  C   CYS A   1     -17.789  17.203 -23.896  1.00 43.03           C
ATOM      4  O   CYS A   1     -16.924  16.660 -24.583  1.00 43.06           O
ATOM      5  CB  CYS A   1     -17.497  19.676 -23.629  1.00 45.65           C
ATOM      6  SG  CYS A   1     -16.306  19.882 -24.974  1.00 50.78           S
ATOM      7  N   ILE A   2     -19.073  16.865 -23.948  1.00 30.00           N
ATOM      8  CA  ILE A   2     -19.554  15.812 -24.834  1.00 30.00           C
ATOM      9  C   ILE A   2     -19.986  16.389 -26.179  1.00 30.00           C
ATOM     10  O   ILE A   2     -20.748  17.355 -26.234  1.00 30.00           O
ATOM     11  CB  ILE A   2     -20.736  15.047 -24.206  1.00 30.00           C
ATOM     12  CG1 ILE A   2     -20.354  14.520 -22.821  1.00 30.00           C
ATOM     13  CD1 ILE A   2     -21.465  13.763 -22.128  1.00 30.00           C
ATOM     14  CG2 ILE A   2     -21.178  13.907 -25.112  1.00 30.00           C
ATOM     15  N   THR A   3     -19.493  15.793 -27.260  1.00 32.39           N
ATOM     16  CA  THR A   3     -19.827  16.242 -28.607  1.00 29.35           C
ATOM     17  C   THR A   3     -21.304  16.020 -28.916  1.00 27.93           C
ATOM     18  O   THR A   3     -21.776  14.884 -28.955  1.00 29.22           O
ATOM     19  CB  THR A   3     -18.976  15.521 -29.669  1.00 30.02           C
ATOM     20  OG1 THR A   3     -17.588  15.793 -29.438  1.00 28.44           O
ATOM     21  CG2 THR A   3     -19.355  15.992 -31.065  1.00 27.61           C
ATOM     22  N   SER A   4     -22.027  17.114 -29.136  1.00 25.18           N
ATOM     23  CA  SER A   4     -23.452  17.043 -29.437  1.00 23.10           C
ATOM     24  C   SER A   4     -23.694  16.514 -30.847  1.00 20.92           C
ATOM     25  O   SER A   4     -23.040  16.937 -31.800  1.00 20.48           O
ATOM     26  CB  SER A   4     -24.103  18.418 -29.273  1.00 24.08           C
ATOM     27  OG  SER A   4     -23.956  18.897 -27.948  1.00 28.62           O
ATOM     28  N   ALA A   5     -24.637  15.586 -30.970  1.00 30.00           N
ATOM     29  CA  ALA A   5     -24.964  14.989 -32.259  1.00 30.00           C
ATOM     30  C   ALA A   5     -26.410  14.503 -32.284  1.00 30.00           C
ATOM     31  O   ALA A   5     -26.909  13.978 -31.289  1.00 30.00           O
ATOM     32  CB  ALA A   5     -24.012  13.843 -32.567  1.00 30.00           C
ATOM     33  N   PRO A   6     -27.090  14.682 -33.429  1.00 17.09           N
ATOM     34  CA  PRO A   6     -28.480  14.246 -33.609  1.00 17.29           C
ATOM     35  C   PRO A   6     -28.630  12.728 -33.542  1.00 15.59           C
ATOM     36  O   PRO A   6     -27.651  12.016 -33.317  1.00 14.49           O
ATOM     37  CB  PRO A   6     -28.821  14.740 -35.019  1.00 18.25           C
ATOM     38  CG  PRO A   6     -27.850  15.840 -35.288  1.00 19.56           C
ATOM     39  CD  PRO A   6     -26.591  15.423 -34.599  1.00 17.35           C
ATOM     40  N   GLN A   7     -29.853  12.244 -33.738  1.00 30.00           N
ATOM     41  CA  GLN A   7     -30.127  10.811 -33.709  1.00 30.00           C
ATOM     42  C   GLN A   7     -29.392  10.087 -34.833  1.00 30.00           C
ATOM     43  O   GLN A   7     -29.204  10.641 -35.916  1.00 30.00           O
ATOM     44  CB  GLN A   7     -31.631  10.551 -33.811  1.00 30.00           C
ATOM     45  CG  GLN A   7     -32.446  11.162 -32.682  1.00 30.00           C
ATOM     46  CD  GLN A   7     -33.929  10.878 -32.814  1.00 30.00           C
ATOM     47  OE1 GLN A   7     -34.365  10.222 -33.760  1.00 30.00           O
ATOM     48  NE2 GLN A   7     -34.713  11.372 -31.863  1.00 30.00           N
ATOM     49  N   LEU A   8     -28.981   8.851 -34.559  1.00 15.34           N
ATOM     50  CA  LEU A   8     -28.232   8.032 -35.512  1.00 14.71           C
ATOM     51  C   LEU A   8     -26.928   8.697 -35.953  1.00 14.25           C
ATOM     52  O   LEU A   8     -26.495   8.542 -37.094  1.00 16.06           O
ATOM     53  CB  LEU A   8     -29.093   7.674 -36.728  1.00 15.70           C
ATOM     54  CG  LEU A   8     -30.369   6.879 -36.445  1.00 16.44           C
ATOM     55  CD1 LEU A   8     -31.124   6.598 -37.735  1.00 17.87           C
ATOM     56  CD2 LEU A   8     -30.044   5.584 -35.716  1.00 17.17           C
ATOM     57  N   TRP A   9     -26.310   9.437 -35.037  1.00 14.72           N
ATOM     58  CA  TRP A   9     -25.040  10.101 -35.308  1.00 14.23           C
ATOM     59  C   TRP A   9     -24.135  10.078 -34.080  1.00 14.66           C
ATOM     60  O   TRP A   9     -24.525  10.526 -33.002  1.00 13.72           O
ATOM     61  CB  TRP A   9     -25.269  11.545 -35.764  1.00 14.59           C
ATOM     62  CG  TRP A   9     -25.679  11.670 -37.200  1.00 13.40           C
ATOM     63  CD1 TRP A   9     -26.945  11.589 -37.702  1.00 13.38           C
ATOM     64  CD2 TRP A   9     -24.818  11.905 -38.321  1.00 12.10           C
ATOM     65  NE1 TRP A   9     -26.926  11.756 -39.066  1.00 13.17           N
ATOM     66  CE2 TRP A   9     -25.631  11.952 -39.470  1.00 13.81           C
ATOM     67  CE3 TRP A   9     -23.438  12.079 -38.464  1.00 12.42           C
ATOM     68  CZ2 TRP A   9     -25.111  12.166 -40.745  1.00 13.71           C
ATOM     69  CZ3 TRP A   9     -22.923  12.291 -39.731  1.00 14.69           C
ATOM     70  CH2 TRP A   9     -23.758  12.332 -40.854  1.00 15.17           C
ATOM     71  N   GLU A  10     -22.926   9.553 -34.251  1.00 30.00           N
ATOM     72  CA  GLU A  10     -21.962   9.478 -33.160  1.00 30.00           C
ATOM     73  C   GLU A  10     -20.992  10.654 -33.205  1.00 30.00           C
ATOM     74  O   GLU A  10     -20.575  11.086 -34.280  1.00 30.00           O
ATOM     75  CB  GLU A  10     -21.192   8.157 -33.214  1.00 30.00           C
ATOM     76  CG  GLU A  10     -22.071   6.922 -33.105  1.00 30.00           C
ATOM     77  CD  GLU A  10     -21.274   5.633 -33.165  1.00 30.00           C
ATOM     78  OE1 GLU A  10     -20.033   5.705 -33.286  1.00 30.00           O
ATOM     79  OE2 GLU A  10     -21.889   4.548 -33.091  1.00 30.00           O
ATOM     80  N   GLY A  11     -20.634  11.166 -32.032  1.00 14.65           N
ATOM     81  CA  GLY A  11     -19.729  12.297 -31.939  1.00 14.32           C
ATOM     82  C   GLY A  11     -18.336  11.901 -31.492  1.00 14.63           C
ATOM     83  O   GLY A  11     -18.171  11.034 -30.633  1.00 15.06           O
ATOM     84  N   TYR A  12     -17.329  12.541 -32.077  1.00 30.00           N
ATOM     85  CA  TYR A  12     -15.939  12.265 -31.734  1.00 30.00           C
ATOM     86  C   TYR A  12     -15.060  13.480 -32.010  1.00 30.00           C
ATOM     87  O   TYR A  12     -14.839  13.846 -33.165  1.00 30.00           O
ATOM     88  CB  TYR A  12     -15.425  11.052 -32.514  1.00 30.00           C
ATOM     89  CG  TYR A  12     -14.016  10.639 -32.152  1.00 30.00           C
ATOM     90  CD1 TYR A  12     -13.769   9.835 -31.047  1.00 30.00           C
ATOM     91  CD2 TYR A  12     -12.932  11.048 -32.919  1.00 30.00           C
ATOM     92  CE1 TYR A  12     -12.484   9.454 -30.712  1.00 30.00           C
ATOM     93  CE2 TYR A  12     -11.643  10.672 -32.592  1.00 30.00           C
ATOM     94  CZ  TYR A  12     -11.424   9.875 -31.489  1.00 30.00           C
ATOM     95  OH  TYR A  12     -10.143   9.497 -31.160  1.00 30.00           O
ATOM     96  N   ASN A  13     -14.565  14.096 -30.938  1.00 18.63           N
ATOM     97  CA  ASN A  13     -13.716  15.283 -31.026  1.00 21.10           C
ATOM     98  C   ASN A  13     -14.370  16.421 -31.808  1.00 21.26           C
ATOM     99  O   ASN A  13     -13.859  16.852 -32.843  1.00 21.87           O
ATOM    100  CB  ASN A  13     -12.347  14.932 -31.620  1.00 22.52           C
ATOM    101  CG  ASN A  13     -11.332  16.046 -31.448  1.00 29.80           C
ATOM    102  OD1 ASN A  13     -11.484  16.913 -30.586  1.00 32.26           O
ATOM    103  ND2 ASN A  13     -10.288  16.029 -32.269  1.00 35.88           N
ATOM    104  N   ASP A  14     -15.511  16.889 -31.304  1.00 30.00           N
ATOM    105  CA  ASP A  14     -16.262  17.987 -31.915  1.00 30.00           C
ATOM    106  C   ASP A  14     -16.655  17.715 -33.367  1.00 30.00           C
ATOM    107  O   ASP A  14     -16.859  18.645 -34.147  1.00 30.00           O
ATOM    108  CB  ASP A  14     -15.484  19.304 -31.811  1.00 30.00           C
ATOM    109  CG  ASP A  14     -15.197  19.700 -30.376  1.00 30.00           C
ATOM    110  OD1 ASP A  14     -16.006  19.355 -29.489  1.00 30.00           O
ATOM    111  OD2 ASP A  14     -14.162  20.355 -30.134  1.00 30.00           O
ATOM    112  N   LYS A  15     -16.761  16.438 -33.722  1.00 17.18           N
ATOM    113  CA  LYS A  15     -17.138  16.045 -35.075  1.00 15.74           C
ATOM    114  C   LYS A  15     -18.136  14.892 -35.051  1.00 14.71           C
ATOM    115  O   LYS A  15     -18.034  13.988 -34.222  1.00 14.36           O
ATOM    116  CB  LYS A  15     -15.903  15.648 -35.889  1.00 16.55           C
ATOM    117  CG  LYS A  15     -14.925  16.785 -36.144  1.00 19.43           C
ATOM    118  CD  LYS A  15     -13.739  16.319 -36.973  1.00 21.62           C
ATOM    119  CE  LYS A  15     -14.184  15.807 -38.333  1.00 22.39           C
ATOM    120  NZ  LYS A  15     -13.032  15.346 -39.157  1.00 20.09           N
ATOM    121  N   MET A  16     -19.101  14.929 -35.965  1.00 30.00           N
ATOM    122  CA  MET A  16     -20.110  13.880 -36.055  1.00 30.00           C
ATOM    123  C   MET A  16     -19.801  12.910 -37.191  1.00 30.00           C
ATOM    124  O   MET A  16     -19.285  13.306 -38.237  1.00 30.00           O
ATOM    125  CB  MET A  16     -21.503  14.487 -36.238  1.00 30.00           C
ATOM    126  CG  MET A  16     -21.637  15.388 -37.455  1.00 30.00           C
ATOM    127  SD  MET A  16     -23.293  16.080 -37.630  1.00 30.00           S
ATOM    128  CE  MET A  16     -23.446  16.986 -36.093  1.00 30.00           C
ATOM    129  N   PHE A  17     -20.116  11.636 -36.978  1.00 12.24           N
ATOM    130  CA  PHE A  17     -19.870  10.607 -37.983  1.00 12.38           C
ATOM    131  C   PHE A  17     -21.035   9.627 -38.073  1.00 12.53           C
ATOM    132  O   PHE A  17     -21.794   9.459 -37.118  1.00 12.73           O
ATOM    133  CB  PHE A  17     -18.575   9.852 -37.675  1.00 11.11           C
ATOM    134  CG  PHE A  17     -17.344  10.713 -37.718  1.00 12.88           C
ATOM    135  CD1 PHE A  17     -16.848  11.294 -36.563  1.00 13.24           C
ATOM    136  CD2 PHE A  17     -16.684  10.941 -38.914  1.00 13.56           C
ATOM    137  CE1 PHE A  17     -15.716  12.086 -36.599  1.00 17.37           C
ATOM    138  CE2 PHE A  17     -15.552  11.732 -38.957  1.00 13.87           C
ATOM    139  CZ  PHE A  17     -15.067  12.305 -37.798  1.00 14.67           C
ATOM    140  N   ARG A  18     -21.169   8.982 -39.228  1.00 12.95           N
ATOM    141  CA  ARG A  18     -22.230   8.006 -39.445  1.00 12.86           C
ATOM    142  C   ARG A  18     -21.849   7.014 -40.538  1.00 12.72           C
ATOM    143  O   ARG A  18     -21.506   7.406 -41.653  1.00 12.37           O
ATOM    144  CB  ARG A  18     -23.541   8.707 -39.808  1.00 13.40           C
ATOM    145  CG  ARG A  18     -24.703   7.759 -40.054  1.00 14.78           C
ATOM    146  CD  ARG A  18     -25.969   8.518 -40.419  1.00 15.56           C
ATOM    147  NE  ARG A  18     -27.097   7.621 -40.649  1.00 18.49           N
ATOM    148  CZ  ARG A  18     -28.319   8.027 -40.977  1.00 24.76           C
ATOM    149  NH1 ARG A  18     -28.576   9.320 -41.114  1.00 22.07           N
ATOM    150  NH2 ARG A  18     -29.286   7.139 -41.168  1.00 28.18           N
ATOM    151  N   VAL A  19     -21.912   5.727 -40.212  1.00 30.00           N
ATOM    152  CA  VAL A  19     -21.577   4.677 -41.167  1.00 30.00           C
ATOM    153  C   VAL A  19     -22.801   4.258 -41.974  1.00 30.00           C
ATOM    154  O   VAL A  19     -23.767   3.728 -41.425  1.00 30.00           O
ATOM    155  CB  VAL A  19     -20.989   3.440 -40.462  1.00 30.00           C
ATOM    156  CG1 VAL A  19     -20.702   2.340 -41.472  1.00 30.00           C
ATOM    157  CG2 VAL A  19     -19.728   3.814 -39.700  1.00 30.00           C
ATOM    158  N   HIS A  20     -22.754   4.498 -43.281  1.00 30.00           N
ATOM    159  CA  HIS A  20     -23.860   4.150 -44.164  1.00 30.00           C
ATOM    160  C   HIS A  20     -23.581   2.853 -44.916  1.00 30.00           C
ATOM    161  O   HIS A  20     -22.471   2.629 -45.397  1.00 30.00           O
ATOM    162  CB  HIS A  20     -24.131   5.284 -45.155  1.00 30.00           C
ATOM    163  CG  HIS A  20     -25.257   5.003 -46.102  1.00 30.00           C
ATOM    164  ND1 HIS A  20     -26.546   4.769 -45.676  1.00 30.00           N
ATOM    165  CD2 HIS A  20     -25.286   4.919 -47.453  1.00 30.00           C
ATOM    166  CE1 HIS A  20     -27.321   4.553 -46.724  1.00 30.00           C
ATOM    167  NE2 HIS A  20     -26.581   4.638 -47.814  1.00 30.00           N
ATOM    168  N   MSE A  21     -24.597   2.002 -45.014  1.00 15.69           N
ATOM    169  CA  MSE A  21     -24.465   0.727 -45.709  1.00 16.33           C
ATOM    170  C   MSE A  21     -24.981   0.823 -47.141  1.00 17.23           C
ATOM    171  O   MSE A  21     -24.915  -0.143 -47.901  1.00 18.55           O
ATOM    172  CB  MSE A  21     -25.211  -0.375 -44.954  1.00 20.00           C
ATOM    173  CG  MSE A  21     -24.714  -0.601 -43.535  1.00 20.00           C
ATOM    174 SE   MSE A  21     -22.831  -1.101 -43.468  1.00 20.00          SE
ATOM    175  CE  MSE A  21     -22.913  -2.735 -44.529  1.00 20.00           C
TER
END
"""

#RNA
pdb_str_2 = """
CRYST1  132.298   35.250   42.225  90.00  90.95  90.00 C 1 2 1
SCALE1      0.007559  0.000000  0.000125        0.00000
SCALE2      0.000000  0.028369  0.000000        0.00000
SCALE3      0.000000  0.000000  0.023686        0.00000
ATOM      4  O5'   U A   1      33.230  31.542   5.077  1.00  8.57           O
ATOM      5  C5'   U A   1      33.079  32.906   5.441  1.00  7.65           C
ATOM      6  C4'   U A   1      32.546  33.048   6.845  1.00  7.93           C
ATOM      7  O4'   U A   1      31.228  32.448   6.932  1.00  7.66           O
ATOM      8  C3'   U A   1      33.343  32.346   7.934  1.00  7.36           C
ATOM      9  O3'   U A   1      34.476  33.087   8.354  1.00  8.48           O
ATOM     10  C2'   U A   1      32.304  32.144   9.029  1.00  8.63           C
ATOM     11  O2'   U A   1      32.103  33.346   9.758  1.00  8.93           O
ATOM     12  C1'   U A   1      31.047  31.862   8.205  1.00  9.06           C
ATOM     13  N1    U A   1      30.805  30.412   8.030  1.00  8.03           N
ATOM     14  C2    U A   1      30.197  29.733   9.069  1.00  9.41           C
ATOM     15  O2    U A   1      29.860  30.274  10.108  1.00  9.53           O
ATOM     16  N3    U A   1      29.998  28.392   8.848  1.00  8.15           N
ATOM     17  C4    U A   1      30.336  27.676   7.718  1.00  7.87           C
ATOM     18  O4    U A   1      30.091  26.470   7.669  1.00  8.06           O
ATOM     19  C5    U A   1      30.959  28.449   6.688  1.00  6.19           C
ATOM     20  C6    U A   1      31.166  29.757   6.875  1.00  9.31           C
ATOM     21  P     G A   2      35.832  32.328   8.763  1.00 11.17           P
ATOM     22  OP1   G A   2      36.911  33.342   8.877  1.00 11.89           O
ATOM     23  OP2   G A   2      36.006  31.172   7.847  1.00 14.22           O
ATOM     24  O5'   G A   2      35.533  31.760  10.221  1.00 12.60           O
ATOM     25  C5'   G A   2      35.228  32.636  11.296  1.00 11.40           C
ATOM     26  C4'   G A   2      34.646  31.890  12.470  1.00 11.05           C
ATOM     27  O4'   G A   2      33.417  31.229  12.070  1.00 11.06           O
ATOM     28  C3'   G A   2      35.501  30.768  13.037  1.00 11.89           C
ATOM     29  O3'   G A   2      36.511  31.233  13.917  1.00 11.52           O
ATOM     30  C2'   G A   2      34.471  29.871  13.709  1.00 12.14           C
ATOM     31  O2'   G A   2      34.082  30.408  14.965  1.00 14.85           O
ATOM     32  C1'   G A   2      33.291  29.995  12.745  1.00 12.01           C
ATOM     33  N9    G A   2      33.280  28.913  11.743  1.00  9.31           N
ATOM     34  C8    G A   2      33.655  29.002  10.424  1.00  8.70           C
ATOM     35  N7    G A   2      33.537  27.873   9.781  1.00  9.68           N
ATOM     36  C5    G A   2      33.056  26.984  10.732  1.00  7.23           C
ATOM     37  C6    G A   2      32.732  25.606  10.622  1.00  7.98           C
ATOM     38  O6    G A   2      32.809  24.873   9.629  1.00  5.42           O
ATOM     39  N1    G A   2      32.277  25.091  11.831  1.00  6.14           N
ATOM     40  C2    G A   2      32.150  25.805  12.997  1.00 10.35           C
ATOM     41  N2    G A   2      31.692  25.127  14.060  1.00  7.45           N
ATOM     42  N3    G A   2      32.448  27.089  13.113  1.00  9.43           N
ATOM     43  C4    G A   2      32.892  27.610  11.949  1.00  9.46           C
ATOM     44  P     C A   3      37.944  30.507  13.960  1.00 12.74           P
ATOM     45  OP1   C A   3      38.868  31.362  14.746  1.00 14.11           O
ATOM     46  OP2   C A   3      38.305  30.119  12.573  1.00 12.26           O
ATOM     47  O5'   C A   3      37.678  29.174  14.791  1.00 12.62           O
ATOM     48  C5'   C A   3      37.217  29.232  16.133  1.00 10.29           C
ATOM     49  C4'   C A   3      36.724  27.888  16.607  1.00 10.54           C
ATOM     50  O4'   C A   3      35.624  27.446  15.770  1.00 12.36           O
ATOM     51  C3'   C A   3      37.726  26.746  16.533  1.00 12.35           C
ATOM     52  O3'   C A   3      38.629  26.731  17.625  1.00 14.68           O
ATOM     53  C2'   C A   3      36.827  25.518  16.466  1.00 11.24           C
ATOM     54  O2'   C A   3      36.335  25.185  17.755  1.00 13.27           O
ATOM     55  C1'   C A   3      35.661  26.042  15.625  1.00 10.96           C
ATOM     56  N1    C A   3      35.815  25.716  14.188  1.00 20.00           N
ATOM     57  C2    C A   3      35.452  24.441  13.743  1.00 20.00           C
ATOM     58  N3    C A   3      35.587  24.134  12.432  1.00 20.00           N
ATOM     59  C4    C A   3      36.061  25.042  11.578  1.00 20.00           C
ATOM     60  C5    C A   3      36.437  26.349  12.002  1.00 20.00           C
ATOM     61  C6    C A   3      36.298  26.640  13.302  1.00 20.00           C
ATOM     62  O2    C A   3      35.014  23.620  14.562  1.00 20.00           O
ATOM     63  N4    C A   3      36.177  24.694  10.295  1.00 20.00           N
ATOM     64  P     A A   4      40.094  26.095  17.454  1.00 15.61           P
ATOM     65  OP1   A A   4      40.942  26.585  18.570  1.00 19.56           O
ATOM     66  OP2   A A   4      40.529  26.314  16.051  1.00 17.09           O
ATOM     67  O5'   A A   4      39.863  24.532  17.659  1.00 14.10           O
ATOM     68  C5'   A A   4      39.330  24.027  18.875  1.00 14.10           C
ATOM     69  C4'   A A   4      39.108  22.537  18.806  1.00 13.63           C
ATOM     70  O4'   A A   4      38.139  22.232  17.770  1.00 13.30           O
ATOM     71  C3'   A A   4      40.322  21.699  18.434  1.00 13.78           C
ATOM     72  O3'   A A   4      41.184  21.458  19.532  1.00 13.31           O
ATOM     73  C2'   A A   4      39.691  20.436  17.863  1.00 12.75           C
ATOM     74  O2'   A A   4      39.240  19.585  18.906  1.00 12.98           O
ATOM     75  C1'   A A   4      38.469  21.009  17.145  1.00 12.61           C
ATOM     76  N9    A A   4      38.738  21.270  15.718  1.00 12.88           N
ATOM     77  C8    A A   4      38.959  22.482  15.112  1.00 11.71           C
ATOM     78  N7    A A   4      39.171  22.396  13.821  1.00 10.33           N
ATOM     79  C5    A A   4      39.084  21.035  13.559  1.00  9.80           C
ATOM     80  C6    A A   4      39.214  20.288  12.375  1.00 10.29           C
ATOM     81  N6    A A   4      39.470  20.829  11.182  1.00  9.60           N
ATOM     82  N1    A A   4      39.069  18.947  12.459  1.00  9.49           N
ATOM     83  C2    A A   4      38.812  18.403  13.655  1.00  9.84           C
ATOM     84  N3    A A   4      38.668  18.999  14.837  1.00 11.60           N
ATOM     85  C4    A A   4      38.817  20.330  14.719  1.00 11.27           C
TER
END
"""

pdb_str_3 = """
CRYST1   25.635   40.310   65.928  90.00  90.00  90.00 P 21 21 21
SCALE1      0.039009  0.000000  0.000000        0.00000
SCALE2      0.000000  0.024808  0.000000        0.00000
SCALE3      0.000000  0.000000  0.015168        0.00000
ATOM      1  P    DT A   1     -29.513 -26.800  43.755  1.00 12.51           P
ATOM      2  OP1  DT A   1     -30.415 -27.102  44.889  1.00 14.01           O
ATOM      3  OP2  DT A   1     -29.589 -27.604  42.514  1.00 13.09           O
ATOM      4  O5'  DT A   1     -29.675 -25.259  43.356  1.00 12.75           O
ATOM      5  C5'  DT A   1     -30.548 -24.418  44.100  1.00 14.05           C
ATOM      6  C4'  DT A   1     -30.322 -22.958  43.747  1.00 13.12           C
ATOM      7  O4'  DT A   1     -28.962 -22.584  44.079  1.00 12.75           O
ATOM      8  C3'  DT A   1     -30.466 -22.622  42.271  1.00 12.82           C
ATOM      9  O3'  DT A   1     -31.828 -22.350  41.965  1.00 14.84           O
ATOM     10  C2'  DT A   1     -29.604 -21.371  42.145  1.00 13.59           C
ATOM     11  C1'  DT A   1     -28.472 -21.653  43.132  1.00 12.45           C
ATOM     12  N1   DT A   1     -27.244 -22.219  42.498  1.00 11.24           N
ATOM     13  C2   DT A   1     -26.390 -21.381  41.812  1.00 11.45           C
ATOM     14  O2   DT A   1     -26.581 -20.185  41.687  1.00 12.28           O
ATOM     15  N3   DT A   1     -25.293 -22.002  41.271  1.00 11.55           N
ATOM     16  C4   DT A   1     -24.972 -23.346  41.347  1.00 11.51           C
ATOM     17  O4   DT A   1     -23.962 -23.810  40.827  1.00 11.91           O
ATOM     18  C5   DT A   1     -25.907 -24.163  42.079  1.00 11.36           C
ATOM     19  C7   DT A   1     -25.665 -25.636  42.229  1.00 13.07           C
ATOM     20  C6   DT A   1     -26.982 -23.569  42.612  1.00 11.50           C
ATOM     21  P    DA A   2     -32.385 -22.584  40.476  1.00 15.73           P
ATOM     22  OP1  DA A   2     -33.862 -22.622  40.552  1.00 18.81           O
ATOM     23  OP2  DA A   2     -31.648 -23.729  39.896  1.00 18.08           O
ATOM     24  O5'  DA A   2     -31.940 -21.266  39.686  1.00 14.67           O
ATOM     25  C5'  DA A   2     -32.400 -19.990  40.117  1.00 14.55           C
ATOM     26  C4'  DA A   2     -31.639 -18.870  39.428  1.00 14.03           C
ATOM     27  O4'  DA A   2     -30.214 -19.091  39.590  1.00 13.54           O
ATOM     28  C3'  DA A   2     -31.879 -18.741  37.927  1.00 15.02           C
ATOM     29  O3'  DA A   2     -31.933 -17.365  37.564  1.00 15.17           O
ATOM     30  C2'  DA A   2     -30.665 -19.432  37.316  1.00 15.06           C
ATOM     31  C1'  DA A   2     -29.579 -19.088  38.328  1.00 12.56           C
ATOM     32  N9   DA A   2     -28.487 -20.056  38.356  1.00 20.00           N
ATOM     33  C8   DA A   2     -28.540 -21.348  38.802  1.00 20.00           C
ATOM     34  N7   DA A   2     -27.399 -21.989  38.708  1.00 20.00           N
ATOM     35  C5   DA A   2     -26.536 -21.052  38.164  1.00 20.00           C
ATOM     36  C4   DA A   2     -27.190 -19.855  37.941  1.00 20.00           C
ATOM     37  N1   DA A   2     -24.615 -19.993  37.295  1.00 20.00           N
ATOM     38  C2   DA A   2     -25.375 -18.903  37.136  1.00 20.00           C
ATOM     39  N3   DA A   2     -26.664 -18.733  37.425  1.00 20.00           N
ATOM     40  C6   DA A   2     -25.172 -21.106  37.816  1.00 20.00           C
ATOM     41  N6   DA A   2     -24.414 -22.196  37.975  1.00 20.00           N
ATOM     42  P    DC A   3     -32.448 -16.932  36.104  1.00 16.58           P
ATOM     43  OP1  DC A   3     -33.299 -15.733  36.269  1.00 19.72           O
ATOM     44  OP2  DC A   3     -32.997 -18.136  35.441  1.00 18.17           O
ATOM     45  O5'  DC A   3     -31.109 -16.510  35.335  1.00 15.21           O
ATOM     46  C5'  DC A   3     -30.389 -15.358  35.764  1.00 14.88           C
ATOM     47  C4'  DC A   3     -29.029 -15.268  35.095  1.00 13.89           C
ATOM     48  O4'  DC A   3     -28.192 -16.365  35.536  1.00 13.59           O
ATOM     49  C3'  DC A   3     -29.040 -15.384  33.579  1.00 14.04           C
ATOM     50  O3'  DC A   3     -29.307 -14.114  32.996  1.00 15.07           O
ATOM     51  C2'  DC A   3     -27.617 -15.849  33.288  1.00 13.36           C
ATOM     52  C1'  DC A   3     -27.280 -16.706  34.508  1.00 12.45           C
ATOM     53  N1   DC A   3     -27.368 -18.175  34.254  1.00 11.68           N
ATOM     54  C2   DC A   3     -26.249 -18.854  33.760  1.00 11.34           C
ATOM     55  O2   DC A   3     -25.208 -18.222  33.542  1.00 11.46           O
ATOM     56  N3   DC A   3     -26.338 -20.189  33.534  1.00 11.10           N
ATOM     57  C4   DC A   3     -27.478 -20.834  33.782  1.00 10.74           C
ATOM     58  N4   DC A   3     -27.517 -22.150  33.543  1.00 11.36           N
ATOM     59  C5   DC A   3     -28.629 -20.160  34.286  1.00 12.50           C
ATOM     60  C6   DC A   3     -28.530 -18.844  34.506  1.00 12.44           C
ATOM     61  P    DG A   4     -30.093 -14.017  31.597  1.00 17.10           P
ATOM     62  OP1  DG A   4     -30.555 -12.620  31.437  1.00 20.41           O
ATOM     63  OP2  DG A   4     -31.073 -15.125  31.557  1.00 21.17           O
ATOM     64  O5'  DG A   4     -28.967 -14.319  30.502  1.00 15.81           O
ATOM     65  C5'  DG A   4     -27.859 -13.437  30.361  1.00 15.43           C
ATOM     66  C4'  DG A   4     -26.747 -14.082  29.554  1.00 15.28           C
ATOM     67  O4'  DG A   4     -26.228 -15.228  30.274  1.00 14.19           O
ATOM     68  C3'  DG A   4     -27.168 -14.624  28.195  1.00 15.90           C
ATOM     69  O3'  DG A   4     -27.068 -13.600  27.214  1.00 16.05           O
ATOM     70  C2'  DG A   4     -26.142 -15.727  27.959  1.00 15.77           C
ATOM     71  C1'  DG A   4     -25.924 -16.272  29.368  1.00 13.79           C
ATOM     72  N9   DG A   4     -26.762 -17.425  29.688  1.00 13.18           N
ATOM     73  C8   DG A   4     -28.052 -17.409  30.162  1.00 14.02           C
ATOM     74  N7   DG A   4     -28.550 -18.598  30.357  1.00 13.01           N
ATOM     75  C5   DG A   4     -27.525 -19.460  29.990  1.00 12.16           C
ATOM     76  C6   DG A   4     -27.481 -20.874  29.988  1.00 11.61           C
ATOM     77  O6   DG A   4     -28.369 -21.670  30.323  1.00 12.64           O
ATOM     78  N1   DG A   4     -26.250 -21.350  29.542  1.00 11.41           N
ATOM     79  C2   DG A   4     -25.197 -20.559  29.147  1.00 11.92           C
ATOM     80  N2   DG A   4     -24.089 -21.199  28.747  1.00 12.09           N
ATOM     81  N3   DG A   4     -25.226 -19.232  29.143  1.00 11.91           N
ATOM     82  C4   DG A   4     -26.418 -18.753  29.576  1.00 12.57           C
TER
END
"""

build = "phenix.reduce -oh -his -flip -pen9999 -keep -allalt -"
build_neutron = "phenix.reduce -nuclear -oh -his -flip "+\
                "-pen9999 -keep -allalt -"

def tst_protein():
  for i, cmd in enumerate([build, build_neutron]):
    build_out = easy_run.fully_buffered(cmd,
                  stdin_lines=pdb_str_1)
    reduce_str = '\n'.join(build_out.stdout_lines)
    if i == 0:
      use_neutron_distances = False
    else:
      use_neutron_distances = True
    restraints_loading_flags = {}
    restraints_loading_flags["use_neutron_distances"] = use_neutron_distances
    processed_pdb_file = pdb_interpretation.process(
      mon_lib_srv              = server.server(),
      ener_lib                 = server.ener_lib(),
      raw_records              = flex.std_string(reduce_str.splitlines()),
      strict_conflict_handling = True,
      restraints_loading_flags = restraints_loading_flags,
      force_symmetry           = True,
      log                      = None)
    grm = processed_pdb_file.geometry_restraints_manager()
    sites_cart = processed_pdb_file.all_chain_proxies.sites_cart
    awl = tuple(
            processed_pdb_file.all_chain_proxies.pdb_hierarchy.atoms_with_labels())
    grf = geometry_restraints.flags.flags(default=True)
    bond_proxies = grm.pair_proxies(flags=grf, sites_cart=sites_cart).bond_proxies
    for bp in bond_proxies.simple:
      i_seqs = bp.i_seqs
      for i_seq in i_seqs:
        if awl[i_seq].element_is_hydrogen():
          b = geometry_restraints.bond(
            sites_cart=sites_cart,
            proxy=bp)
          #print b.distance_ideal, b.distance_model, awl[i_seq].resname, awl[i_seq].name
          assert approx_equal(b.delta, 0.0, eps=0.001)

def tst_rna():
  build_out = easy_run.fully_buffered(build,
                stdin_lines=pdb_str_2)
  reduce_str = '\n'.join(build_out.stdout_lines)
  processed_pdb_file = pdb_interpretation.process(
    mon_lib_srv              = server.server(),
    ener_lib                 = server.ener_lib(),
    raw_records              = flex.std_string(reduce_str.splitlines()),
    strict_conflict_handling = True,
    force_symmetry           = True,
    log                      = None)
  grm = processed_pdb_file.geometry_restraints_manager()
  sites_cart = processed_pdb_file.all_chain_proxies.sites_cart
  awl = tuple(
          processed_pdb_file.all_chain_proxies.pdb_hierarchy.atoms_with_labels())
  grf = geometry_restraints.flags.flags(default=True)
  bond_proxies = grm.pair_proxies(flags=grf, sites_cart=sites_cart).bond_proxies
  for bp in bond_proxies.simple:
    i_seqs = bp.i_seqs
    for i_seq in i_seqs:
      if awl[i_seq].element_is_hydrogen():
        b = geometry_restraints.bond(
          sites_cart=sites_cart,
          proxy=bp)
        #print b.distance_ideal, b.distance_model, awl[i_seq].resname, awl[i_seq].name
        assert approx_equal(b.delta, 0.0, eps=0.001)

def tst_dna():
  build_out = easy_run.fully_buffered(build,
                stdin_lines=pdb_str_3)
  reduce_str = '\n'.join(build_out.stdout_lines)
  processed_pdb_file = pdb_interpretation.process(
    mon_lib_srv              = server.server(),
    ener_lib                 = server.ener_lib(),
    raw_records              = flex.std_string(reduce_str.splitlines()),
    strict_conflict_handling = True,
    force_symmetry           = True,
    log                      = None)
  grm = processed_pdb_file.geometry_restraints_manager()
  sites_cart = processed_pdb_file.all_chain_proxies.sites_cart
  awl = tuple(
          processed_pdb_file.all_chain_proxies.pdb_hierarchy.atoms_with_labels())
  grf = geometry_restraints.flags.flags(default=True)
  bond_proxies = grm.pair_proxies(flags=grf, sites_cart=sites_cart).bond_proxies
  for bp in bond_proxies.simple:
    i_seqs = bp.i_seqs
    for i_seq in i_seqs:
      if awl[i_seq].element_is_hydrogen():
        b = geometry_restraints.bond(
          sites_cart=sites_cart,
          proxy=bp)
        #print b.distance_ideal, b.distance_model, awl[i_seq].resname, awl[i_seq].name
        assert approx_equal(b.delta, 0.0, eps=0.001)

if (__name__ == "__main__"):
  t0 = time.time()
  if (not libtbx.env.has_module(name="reduce")):
    print("Skipping tst_reduce:" \
      "reduce not available")
  else:
    tst_protein()
    tst_rna()
    tst_dna()
  print("OK. Time: %8.3f"%(time.time()-t0))
