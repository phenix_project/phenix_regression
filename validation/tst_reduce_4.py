from __future__ import division
from __future__ import print_function
from libtbx import easy_run
from iotbx.pdb.fetch import fetch_and_write
import os

def tst_reduce_anisou():
  fetch_and_write(id="5a46")
  assert os.path.isfile("5a46.pdb")
  #cmd = "phenix.reduce 5a46.pdb > 5a46_h.pdb"
  cmd = "mmtbx.reduce2 5a46.pdb output.description_file_name=tst_reduce_4_anisou.txt"
  print(cmd)
  build_out = easy_run.fully_buffered(cmd)
  #cmd = 'phenix.pdb_interpretation 5a46_h.pdb'
  cmd = 'phenix.pdb_interpretation 5a46_reduced.cif'
  build_out = easy_run.fully_buffered(cmd)
  reduce_str = '\n'.join(build_out.stderr_lines)
  print(reduce_str)
  assert len(reduce_str) == 0, "For Duke group to fix: mmtbx.reduce2 fails to remove ANISOU."
  cmd = "phenix.reduce -trim 5a46.pdb > 5a46.trimmed.pdb"
  print(cmd)
  build_out = easy_run.fully_buffered(cmd)
  cmd = "phenix.reduce 5a46.trimmed.pdb > 5a46_h.pdb"
  build_out = easy_run.fully_buffered(cmd)
  cmd = 'phenix.pdb_interpretation 5a46_h.pdb'
  build_out = easy_run.fully_buffered(cmd)
  reduce_str = '\n'.join(build_out.stderr_lines)
  print(reduce_str)
  assert len(reduce_str) == 0, "For Duke group to fix: reduce trim and add fails to remove ANISOU."

if (__name__ == "__main__"):
  tst_reduce_anisou()
