from __future__ import division
from __future__ import print_function

from libtbx import easy_run
import libtbx.load_env

from time import time
import datetime
import os, sys
import abc

"""
This test intend to ensure the broad range of applications runs in
expected time. This test should fail if at some point applications get
much slower (the extent to be determined), or issue some kind of warning
when app is running faster (to adjust reference times in this test).
It would be good to keep history of runs.
It is essential that this test is run on the same machine, OS, compiler and
load (e.g. nothing else is run on the machine that could distort results).
"""

class test_case(object):
  def __init__(self,
      name,
      cmd_to_run,
      expected_time,
      time_eps_seconds):
    self.name = name
    self.cmd_to_run = cmd_to_run
    self.expected_time = expected_time
    self.time_eps_seconds = time_eps_seconds
    fname = self.name+".results"
    self.file_name_with_results = os.path.join(
        results_directory_prefix,
        fname)
    # print "self.file_name_with_results", self.file_name_with_results
    # STOP()
    self._run_ok = True
    self._delta = self.time_eps_seconds
    self.time_elapsed = 0

  def setup(self):
    # need to implement in derivatives
    pass
    # print self.expected_time

  def job_completed(self):
    return self._run_ok

  def job_too_fast(self):
    if self.job_completed():
      return self.time_elapsed < self.expected_time - self._delta
    return False

  def job_too_slow(self):
    if self.job_completed():
      return self.time_elapsed > self.expected_time + self._delta
    return False

  def run(self):
    self.setup()
    t0 = time()
    try:
      rc = easy_run.go('time -p %s' % self.cmd_to_run)
      assert not rc.return_code
    except AssertionError as e: self._run_ok = False
    if not self._run_ok: print('\n'.join(rc.stdout_lines))
    self.time_elapsed = time()-t0
    user = None
    for line in rc.stdout_lines:
      if line.find('user')>-1: user = line
    if user:
      tmp = user.split()
      if len(tmp)==2: user = float(tmp[-1])
      else: user = None
    if user: self.time_elapsed = user
    self.record_results()

  def record_results(self):
    f = open(self.file_name_with_results, 'a')
    infostr = ", ".join([
        datetime.datetime.now().strftime("%H:%M %m-%d-%Y"),
        "%s" % self.job_completed(),
        "%s" % self.time_elapsed,
        "%s" % self.job_too_fast(),
        "%s" % self.job_too_slow()])
    infostr += '\n'
    f.write(infostr)
    f.close()

  def average_results(self):
    f = file(self.file_name_with_results, 'rb')
    lines = f.read()
    f.close()
    average = 0
    minimum = 1e9
    maximum = -1e9
    for i, line in enumerate(lines.splitlines()):
      t = float(line.split(',')[2])
      average+=t
      minimum = min(minimum, t)
      maximum = max(maximum, t)
    average /= (i+1)
    print('Timings %s: %0.1f (n=%d) +%0.1f -%0.1f' % (
      os.path.basename(self.file_name_with_results),
      average,
      i+1,
      maximum-average,
      average-minimum,
    ))

  def timing_history(self):
    f = file(self.file_name_with_results, 'rb')
    lines = f.read()
    f.close()
    outl = ''
    for i, line in enumerate(lines.splitlines()):
      t = float(line.split(',')[2])
      date_string = line.split(',')[0]
      d = datetime.datetime.strptime(date_string, "%H:%M %m-%d-%Y")
      d = d.strftime("%Y-%m-%dT%H:%M")
      outl += ' %s %0.1f\n' % (d, t)
    return outl

# this directory should exist. Optionally with results from previous runs
results_directory_prefix = os.getcwd()
if os.getcwd().find('phenix-externals-regression-bot')>-1:
  results_directory_prefix = os.path.join('..','..')

# Inherit from test_case and define setup() if need to do some initial setup,
# e.g. get some files, calculate fake map... This will not be counted in runtime.
class fetch_5y9f_with_mtz(test_case):
  def setup(self):
    if os.path.isfile("5y9f.pdb") and os.path.isfile("5y9f.mtz"):
      return
    easy_run.call("phenix.fetch_pdb 5y9f --mtz")

# this is for test purposes
class fetch_1yjp_with_mtz(test_case):
  def setup(self):
    if os.path.isfile("1yjp.pdb") and os.path.isfile("1yjp.mtz"):
      return
    easy_run.call("phenix.fetch_pdb 1yjp --mtz")

# instantiate test cases using either inherited classes or test_case
# when no setup needed.
# All necessary inforamtion is here
phenix_refine = fetch_1yjp_with_mtz(
    name = "phenix_refine",
    cmd_to_run = " ".join([
        "phenix.refine",
        "1yjp.pdb",
        "1yjp.mtz",
        "main.number_of_macro_cycles=3",
        "--overwrite",
        "output.prefix=phenix_refine_speed_regression"]),
    expected_time = 72,
    time_eps_seconds = 10)

real_space_refine = fetch_1yjp_with_mtz(
  name = 'phenix.real_space_refine',
  cmd_to_run = ' '.join([
    'phenix.real_space_refine',
    '1yjp.pdb',
    'phenix_refine_speed_regression_001.mtz',
    'label="2FOFCWT,PH2FOFCWT"',]),
  expected_time = 0,
  time_eps_seconds = 10)

molprobity = fetch_5y9f_with_mtz(
    name = "molprobity",
    cmd_to_run = " ".join([
        "phenix.molprobity",
        "5y9f.pdb",
        ]),
    expected_time = 250,
    time_eps_seconds = 10)

pdb_interpretation = fetch_5y9f_with_mtz(
    name = "pdb_interpretation",
    cmd_to_run = " ".join([
        "phenix.pdb_interpretation",
        "5y9f.pdb",
        ]),
    expected_time = 72,
    time_eps_seconds = 5)

model_vs_data = fetch_5y9f_with_mtz(
    name = "model_vs_data",
    cmd_to_run = " ".join([
        "phenix.model_vs_data",
        "5y9f.pdb",
        "5y9f.mtz",
        "r_free_flags_label=R-free-flags",
        ]),
    expected_time = 89,
    time_eps_seconds = 5)

elbow_opt = test_case(
  name = 'elbow_opt',
  cmd_to_run = ' '.join(['phenix.elbow',
                         '--chemical_component',
                         'amp',
                         '--opt',
                         ]),
  expected_time=99,
  time_eps_seconds=5,
  )

# put tests you want to run here.
tests_to_run = [
  phenix_refine,
  molprobity,
  pdb_interpretation,
  model_vs_data,
  real_space_refine,
  elbow_opt,
  ]

def exercise():
  failures = []
  fast = []
  slow = []
  raise_assert = False
  xmgrace_outl = ''
  for test in tests_to_run:
    print('%s %s %s' % ('_'*20, test.name, '_'*20))
    test.setup()
    test.run()
    test.average_results()
    if not test.job_completed():
      failures.append(test.name)
    if test.job_too_fast():
      fast.append(test.name)
    if test.job_too_slow():
      slow.append(test.name)
    xmgrace_outl += '@type xy\n'
    xmgrace_outl += test.timing_history()
    xmgrace_outl += '&\n'

  for er, msg in zip([failures, fast, slow], ["fail", "fast", "slow"]):
    if len(er) > 0:
      print("Some tests failed (%s): %s" % (msg, er))
      raise_assert = True
  if raise_assert:
    print('FAILED Timings are suppressed')
  print(xmgrace_outl)
  f = open('timings.dat', 'wb')
  f.write(xmgrace_outl)
  del f

if (__name__ == "__main__"):
  t0 = time()
  if (not libtbx.env.has_module(name="probe")):
    print("Skipping: probe not configured")
  else:
    exercise()
  print("Time: %.2f" % (time() - t0))
  print("OK")
