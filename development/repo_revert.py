from __future__ import print_function
import os, sys
from datetime import datetime
from libtbx import easy_run

skip_repos = [
      'reduce',
      'ccp4io',
      'suitename',
      'king',
      'probe',
      # 'phenix',
      # 'cctbx_project',
      ]

'''
A set of functions (and script) to revert all repositories to an exact date and
time. Ideal for moving back in time of the Phenix developers setup.

  Usage:
    iotbx.python modules/phenix_regression/development/repo_revert.py <date-time>

  Arguments:
    A single argument for the date-time of the desired time stamp.

      2020-3-15         -> to 15 MAR 2020 at time 00:00
      "2020-3-15 18:00" -> set to 6pm on the same day
      master            -> the git to master branch "git checkout master" and
                                                    "svn update"
      now               -> the git to current "git pull" and "svn update"

  N.B. You must do the "master" or "now" to return the HEAD to a usable situation.
'''

def set_all_repo_to_date_time(date_time,
                              modules_directory='.',
                              commands=None,
                              only_module=None,
                              ):
  if commands is None:
    commands = {'git' : 'git checkout `git rev-list -n 1 --before="%s" master`' % date_time,
                'svn' : "svn update -r '{%s}'" % date_time
      }
  cwd = os.getcwd()
  os.chdir(modules_directory)
  for d in os.listdir('.'):
    if not os.path.isdir(d): continue
    if d in skip_repos: continue
    if only_module is not None and only_module!=d: continue
    os.chdir(d)
    print('---- repo: %s' % d)
    if os.path.exists('.git'):
      cmd = commands['git']
      print(cmd)
      rc = easy_run.go(cmd)
    elif os.path.exists('.svn'):
      cmd = commands['svn']
      print(cmd)
      rc = easy_run.go(cmd)
    os.chdir('..')
  os.chdir(cwd)

def parse_date_string(date_string):
  rc = None
  for format_string in [
    '%Y-%m-%d %H:%M',
    '%Y-%m-%d',
    ]:
    try:
      rc = datetime.strptime(date_string, format_string)
    except ValueError as e:
      rc = None
    if rc: break
  assert rc, 'Date string %s not converted' % date_string
  return rc

def main(date_string, only_module=None):
  cwd = os.getcwd()
  if os.path.exists('modules'): os.chdir('modules')
  if 'modules' in os.listdir('../..'): os.chdir('..')
  if date_string=='now':
    commands = {
      'git' : 'git checkout master; git pull',
      'svn' : 'svn update',
      }
    set_all_repo_to_date_time(None, commands=commands, only_module=only_module)
  elif date_string=='master':
    commands = {
      'git' : 'git checkout master',
      'svn' : 'svn update',
      }
    set_all_repo_to_date_time(None, commands=commands, only_module=only_module)
  else:
    print((date_string, parse_date_string(date_string)))
    dt = parse_date_string(date_string)
    ds = dt.strftime('%Y-%m-%d %H:%M')
    set_all_repo_to_date_time(ds, only_module=only_module)
  os.chdir(cwd)

if __name__ == '__main__':
  if len(sys.argv)>1:
    main(*tuple(sys.argv[1:]))
  else:
    for ds in [
      '2001-1-1',
      '2001-01-01',
      '2010-3-15 18:00',
      ]:
      main(ds)
