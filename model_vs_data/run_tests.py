from __future__ import division
from __future__ import print_function
import libtbx.load_env
from libtbx.test_utils import approx_equal, run_command
import os, random
from mmtbx import utils
from cctbx.array_family import flex
import iotbx.pdb
from libtbx import easy_run

if (1):
  random.seed(0)
  flex.set_random_seed(0)

pdb_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="pdb",
  test=os.path.isdir)

hkl_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="reflection_files",
  test=os.path.isdir)

def assert_no_unexpected_output_in_phenix_model_vs_data(file_name):
  lines = open(file_name, "r").readlines()
  res1 = lines[0].strip().startswith("Data: -----")
  if(not res1):
    raise RuntimeError(file_name)

def compute_f_obs(pdb_file_name,
                  high_resolution,
                  output_file_name,
                  verbose,
                  format,
                  r_free_flags_fraction=None,
                  k_sol=0,
                  b_sol=0,
                  b_cart="0 0 0 0 0 0",
                  scattering_table="n_gaussian"):
  logfile = "tmp_%d.log" % os.getpid()
  cmd = " ".join([
    'phenix.fmodel',
    '%s'%pdb_file_name,
    'high_resolution=%s'%high_resolution,
    'algorithm=fft',
    'label=FOBS',
    'type=real',
    'k_sol=%s'%str(k_sol),
    'b_sol=%s'%str(b_sol),
    'b_cart="%s"'%str(b_cart),
    'r_free_flags_fraction=%s'%str(r_free_flags_fraction),
    'output.file_name=%s'%output_file_name,
    'scattering_table=%s'%scattering_table,
    '> %s' % logfile])
  run_command(
    command=cmd,
    stdout_file_name=logfile,
    result_file_names=[output_file_name],
    verbose=verbose)

def exercise_00(verbose):
  # PDB file does not have or have partial CRYST1 record
  log = "model_vs_data_tmp00a.log"
  pdb_file = os.path.join(pdb_dir, "t.pdb")
  hkl_file = os.path.join(hkl_dir, "t.mtz")
  cmd = "phenix.model_vs_data %s %s > %s" % (pdb_file, hkl_file, log)
  run_command(command = cmd, stdout_file_name = log, verbose = verbose)
  assert_no_unexpected_output_in_phenix_model_vs_data(file_name = log)
  #
  log = "model_vs_data_tmp00b.log"
  pdb_file = os.path.join(pdb_dir, "tgg_partial_symmetry_info.pdb")
  hkl_file = os.path.join(hkl_dir, "ygg.mtz")
  cmd = "phenix.model_vs_data %s %s > %s" % (pdb_file, hkl_file, log)
  res = easy_run.go(command=cmd).stdout_lines#[0].split()
  assert res[0] == "Sorry: Crystal symmetry mismatch between different files."

def exercise_02(verbose):
  # phenix.refine then phenix.model_vs_data
  log = "model_vs_data_tmp02.log"
  log_refine = "model_vs_data_tmp02_refine.log"
  pdb_file = os.path.join(pdb_dir, "pdb1akg_very_well_phenix_refined_001_noH_iso.pdb")
  hkl_file = os.path.join(hkl_dir, "1akg.mtz")
  cmd = "phenix.model_vs_data %s %s > %s" % (pdb_file, hkl_file, log)
  print(cmd)
  run_command(command = cmd, stdout_file_name = log, verbose = verbose)
  assert_no_unexpected_output_in_phenix_model_vs_data(file_name = log)
  cmd_refine = " ".join([
    'phenix.refine %s %s'%(pdb_file, hkl_file),
    ' --overwrite strategy=none main.number_of_mac=1'
    '> %s'%log_refine])
  print(cmd_refine)
  run_command(command          = cmd_refine,
              stdout_file_name = log_refine,
              verbose          = verbose)
  #
  x1 = iotbx.pdb.input(file_name=pdb_file).xray_structure_simple()
  x2 = iotbx.pdb.input(
    file_name="pdb1akg_very_well_phenix_refined_001_noH_iso_refine_001.pdb"
    ).xray_structure_simple()
  utils.assert_xray_structures_equal(x1=x1, x2=x2)
  #
  r_work_refine, r_free_refine, k_sol_refine, b_sol_refine = [None,]*4
  for line in open(log_refine,"r").readlines():
    line = line.strip()
    if(line.startswith("Final R-work = 0.") and not line.endswith("scale)")):
      r_work_refine = float(line[15:21])
      r_free_refine = float(line[32:38])
  r_work_mvd, r_free_mvd = [None,]*2
  for line_ in open(log,"r").readlines():
    line = line_.strip()
    if(line_.count("  r_work:")): r_work_mvd = float(line.split()[1])
    if(line_.count("  r_free:")): r_free_mvd = float(line.split()[1])
  print("r_work_refine, r_work_mvd:", r_work_refine, r_work_mvd)
  print("r_free_refine, r_free_mvd:", r_free_refine, r_free_mvd)
  assert approx_equal(r_work_refine, r_work_mvd, 0.0001)
  assert approx_equal(r_free_refine, r_free_mvd, 0.0001)

def exercise_03(verbose):
  # ambiguous Fobs, free_r labels; need to define from the command line
  log = "model_vs_data_tmp03.log"
  pdb_file = os.path.join(pdb_dir, "l.pdb")
  hkl_file = os.path.join(hkl_dir, "l_weird_labels.hkl")
  cmd = " ".join([
    'phenix.model_vs_data %s %s'%(pdb_file, hkl_file),
    'f_obs_label=XXX r_free_flags_label=ABCD',
    '> %s'%log])
  run_command(command = cmd, stdout_file_name = log, verbose = verbose)
  assert_no_unexpected_output_in_phenix_model_vs_data(file_name = log)

def exercise_04(verbose):
  # no R-free flags and no sigmas
  log = "model_vs_data_tmp04.log"
  pdb_file = os.path.join(pdb_dir, "l.pdb")
  hkl_file = os.path.join(hkl_dir, "l_weird_labels_no_freeflags_no_sigma.hkl")
  cmd = "phenix.model_vs_data %s %s > %s" % (pdb_file, hkl_file, log)
  run_command(command = cmd, stdout_file_name = log, verbose = verbose)
  assert_no_unexpected_output_in_phenix_model_vs_data(file_name = log)
  r_work, r_free = [None,]*2
  for line in open(log,"r").readlines():
    if(line.count("  r_work:")): r_work = float(line.split()[1])
    if(line.count("  r_free:")): r_free = line.split()[1]
  assert r_free == "None"

def exercise_05(verbose):
  # neutron data, and Iobs and Fobs (cns and mtz files)
  log = "model_vs_data_tmp05.log"
  pdb_file = os.path.join(pdb_dir, "5rsa-sfNeutron.pdb")
  hkl_file1 = os.path.join(hkl_dir, "5rsa-sfNeutron.mtz")
  hkl_file2 = os.path.join(hkl_dir, "5rsa-sfNeutron_intensities.hkl")
  r_factors = flex.double()
  for i_seq, hkl_file in enumerate([hkl_file1, hkl_file2]):
    log_ = log+"%s"%str(i_seq)
    cmd = "phenix.model_vs_data %s %s scattering_table=neutron > %s" % (
      pdb_file, hkl_file, log_)
    run_command(command = cmd, stdout_file_name = log_, verbose = verbose)
    assert_no_unexpected_output_in_phenix_model_vs_data(file_name = log_)
    r_work, r_free = [None,]*2
    for line in open(log_,"r").readlines():
      if(line.startswith("  r_work:")):
        r_factors.append(float(line.split()[1]))
      if(line.startswith("  r_free:") and line.split()[1] != "None"):
        r_factors.append(float(line.split()[1]))
        break
  assert flex.max(r_factors) < 0.19
  assert flex.min(r_factors) < 0.19

def exercise_06(verbose):
  # Basic
  log = "model_vs_data_tmp06.log"
  pdb_file = os.path.join(pdb_dir, "phe_i.pdb")
  mtz_file = 'model_vs_data_tmp06.mtz'
  compute_f_obs(pdb_file_name         = pdb_file,
                high_resolution       = 1.4,
                output_file_name      = mtz_file,
                verbose               = verbose,
                format                = "mtz",
                r_free_flags_fraction = 0.01)
  cmd = "phenix.model_vs_data %s %s > %s" % (pdb_file, mtz_file, log)
  print(cmd)
  run_command(command = cmd, stdout_file_name = log, verbose = verbose)
  assert_no_unexpected_output_in_phenix_model_vs_data(file_name = log)
  r_work, r_free = [None,]*2
  for line in open(log,"r").readlines():
    if(line.startswith("  r_work:")): r_work = float(line.split()[1])
    if(line.startswith("  r_free:")): r_free = float(line.split()[1])
  assert approx_equal(r_work, 0), r_work
  assert approx_equal(r_free, 0)

def exercise_07(verbose):
  # multiple models separated with MODEL card in one single PDB file
  log = "model_vs_data_tmp07.log"
  pdb_file = os.path.join(pdb_dir, "pdb2q3p.ent")
  hkl_file = os.path.join(hkl_dir, "2q3p.mtz")
  cmd = "phenix.model_vs_data %s %s > %s" % (pdb_file, hkl_file, log)
  run_command(command = cmd, stdout_file_name = log, verbose = verbose)
  assert_no_unexpected_output_in_phenix_model_vs_data(file_name = log)
  r_work, r_free = None, None
  for line in open(log,"r").readlines():
    if(line.startswith("  r_work:")): r_work = float(line.split()[1])
    if(line.startswith("  r_free:")):
      r_free = float(line.split()[1])
      break
  assert approx_equal(r_work, 0.1822, 0.001)
  assert approx_equal(r_free, 0.2232, 0.001)

def exercise_08(verbose):
  # gzipped PDB file has unknown residue
  log = "model_vs_data_tmp08.log"
  pdb_file = os.path.join(pdb_dir, "tgg.pdb.gz")
  hkl_file = os.path.join(hkl_dir, "ygg.mtz")
  cif_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/model_vs_data/tgg.ligands.cif",
    test=os.path.isfile)
  cmd = "phenix.model_vs_data %s %s %s > %s" % (pdb_file, cif_file,hkl_file,log)
  run_command(command = cmd, stdout_file_name = log, verbose = verbose)
  assert_no_unexpected_output_in_phenix_model_vs_data(file_name = log)

def exercise_09(verbose):
  # Check for Sorry if incomplete input
  log = "model_vs_data_tmp09a.log"
  pdb_file = os.path.join(pdb_dir, "t.pdb")
  cmd = "phenix.model_vs_data %s > %s" % (pdb_file, log)
  result = run_command(command = cmd, stdout_file_name = log, verbose = verbose,
    sorry_expected = True, join_stdout_stderr=True)
  assert result.stdout_lines[0] == "Sorry: No reflection file found."
  #
  log = "model_vs_data_tmp09b.log"
  hkl_file = os.path.join(hkl_dir, "ygg.mtz")
  cmd = "phenix.model_vs_data %s > %s" % (hkl_file, log)
  result = run_command(command = cmd, stdout_file_name = log, verbose = verbose,
    sorry_expected = True, join_stdout_stderr = True)
  assert result.stdout_lines[0] == "Sorry: No PDB file found."

def exercise_11(verbose):
  # twinned data, no R-free flags
  log = "model_vs_data_tmp11.log"
  pdb_file = os.path.join(pdb_dir, "p1_twin_dummy_model.pdb")
  hkl_file = os.path.join(hkl_dir, "p1_twin_dummy_model.mtz")
  # twin provided
  cmd = "phenix.model_vs_data %s %s twin_law='l,-k,h' > %s" % (pdb_file, hkl_file, log)
  run_command(command = cmd, stdout_file_name = log, verbose = verbose)
  assert_no_unexpected_output_in_phenix_model_vs_data(file_name = log)
  cntr = 0
  for line in open(log, "r").readlines():
    if(line.startswith("  r_work: 0.0000")): cntr+=1
  assert cntr == 1
  # twin not used
  cmd = "phenix.model_vs_data %s %s > %s" % (pdb_file,
    hkl_file, log)
  run_command(command = cmd, stdout_file_name = log, verbose = verbose)
  assert_no_unexpected_output_in_phenix_model_vs_data(file_name = log)
  cntr = 0
  for line in open(log, "r").readlines():
    if(line.startswith("  r_work: 0.1475")): cntr+=1
  assert cntr == 1

def exercise_13(verbose):
  # importance of "if(not f_obs.sigmas_are_sensible()):" check in f_obs extraction
  log = "model_vs_data_tmp13.log"
  pdb_file = os.path.join(pdb_dir, "pdb1avd.ent.gz")
  hkl_file = os.path.join(hkl_dir, "1avd.mtz")
  cmd = "mmtbx.model_vs_data %s %s> %s 2>%s" % (pdb_file, hkl_file, log, log+".err")
  res = easy_run.call(cmd)
  err_file = open(log+".err", 'r')
  err_lines = err_file.readlines()
  # MTRIX record is not proper rotation matrix, but it has '1' in 60th column
  # indicating that coordinates are present in file. For these records we
  # disabled validation.
  assert len(err_lines) == 0
  # assert err_lines[0].find("Rotation matrix is not proper!") > 0

def exercise_14(verbose):
  # run ok with residue numbers as strings
  log = "model_vs_data_tmp14.log"
  hkl_file = os.path.join(hkl_dir, "1yjp.mtz")
  for i, pf in enumerate(["1yjp_h_1.pdb","1yjp_h.pdb"]):
    pdb_file = os.path.join(pdb_dir, pf)
    logi = log+str(i)
    cmd = "phenix.model_vs_data %s %s > %s" % (pdb_file, hkl_file, logi)
    run_command(command = cmd, stdout_file_name = logi, verbose = verbose)
    assert_no_unexpected_output_in_phenix_model_vs_data(file_name = logi)
  easy_run.call("diff model_vs_data_tmp14.log0 model_vs_data_tmp14.log1 > diff_exercise_14")
  assert os.path.getsize("diff_exercise_14") == 0

def exercise_16(verbose):
  # Basic, H present, Xray
  log = "model_vs_data_tmp16.log"
  pdb_file = os.path.join(pdb_dir, "phe_h_2.pdb")
  mtz_file = 'model_vs_data_tmp16.mtz'
  compute_f_obs(pdb_file_name         = pdb_file,
                high_resolution       = 1.0,
                output_file_name      = mtz_file,
                verbose               = verbose,
                format                = "mtz",
                r_free_flags_fraction = 0.01)
  cmd = "phenix.model_vs_data %s %s > %s" % (pdb_file, mtz_file, log)
  print(cmd)
  run_command(command = cmd, stdout_file_name = log, verbose = verbose)
  assert_no_unexpected_output_in_phenix_model_vs_data(file_name = log)
  r_work, r_free = [None,]*2
  for line in open(log,"r").readlines():
    if(line.startswith("  r_work:")): r_work = float(line.split()[1])
    if(line.startswith("  r_free:")): r_free = float(line.split()[1])
  assert approx_equal(r_work, 0), r_work
  assert approx_equal(r_free, 0), r_free

def exercise_17(verbose):
  # Basic, H present, Neutron
  log = "model_vs_data_tmp17.log"
  pdb_file = os.path.join(pdb_dir, "phe_h_2.pdb")
  mtz_file = 'model_vs_data_tmp17.mtz'
  compute_f_obs(pdb_file_name         = pdb_file,
                high_resolution       = 2.0,
                output_file_name      = mtz_file,
                verbose               = verbose,
                format                = "mtz",
                scattering_table      = "neutron",
                r_free_flags_fraction = 0.01)
  cmd = "phenix.model_vs_data %s %s scattering_table=neutron > %s" % (pdb_file,
    mtz_file, log)
  print(cmd)
  run_command(command = cmd, stdout_file_name = log, verbose = verbose)
  assert_no_unexpected_output_in_phenix_model_vs_data(file_name = log)
  r_work, r_free = [None,]*2
  for line in open(log,"r").readlines():
    if(line.startswith("  r_work:")): r_work = float(line.split()[1])
    if(line.startswith("  r_free:")): r_free = float(line.split()[1])
  assert approx_equal(r_work, 0), r_work
  assert approx_equal(r_free, 0)

def exercise_18(verbose) :
  # high-resolution cutoff
  pdb_file = os.path.join(pdb_dir, "t.pdb")
  hkl_file = os.path.join(hkl_dir, "t.mtz")
  log = "model_vs_data_tmp18.log"
  cmd = "phenix.model_vs_data %s %s high_resolution=4.0 > %s" % (pdb_file,
    hkl_file, log)
  run_command(command=cmd, stdout_file_name=log, verbose=verbose)
  assert_no_unexpected_output_in_phenix_model_vs_data(file_name=log)
  cntr=0
  for line in open(log, "r").readlines() :
    if(line.startswith("  Number of Miller indices: 276")):
      cntr+=1
    if(line.startswith("  Resolution range: 19.1511 4.01742")):
      cntr+=1
  assert cntr==2

def exercise_21(verbose):
  # automatic switching to neutron scattering table
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/model_vs_data/3ryg.pdb",
    test=os.path.isfile)
  mtz_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/model_vs_data/3ryg.mtz",
    test=os.path.isfile)
  log = "model_vs_data_tmp21.log"
  cmd = "phenix.model_vs_data %s %s > %s" % (pdb_file, mtz_file, log)
  run_command(command=cmd, stdout_file_name=log, verbose=verbose)
  assert_no_unexpected_output_in_phenix_model_vs_data(file_name=log)
  expected_output_lines = [
    "exptl_method    : NEUTRON DIFFRACTION",
    "r_work: 0.2184",
  ]
  counter = 0
  for line1 in open(log, "r").readlines():
    line1 = line1.strip()
    for line2 in expected_output_lines:
      line2 = line2.strip()
      if(line1 == line2): counter += 1
  assert counter == 2

if (__name__ == "__main__") :
  exercise_00(False)
  exercise_02(False)
  exercise_03(False)
  exercise_04(False)
  exercise_05(False)
  exercise_06(False)
  exercise_07(False)
  exercise_08(False)
  exercise_09(False)
  exercise_11(False)
  exercise_13(False)
  exercise_14(False)
  exercise_16(False)
  exercise_17(False)
  exercise_18(False)
  exercise_21(False)
