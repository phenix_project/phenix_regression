from __future__ import division
from __future__ import print_function
import iotbx.pdb
import time, os
from libtbx import easy_run
import libtbx.load_env

def check(prefix, line):
  found = 0
  for l in open("%s.zlog"%prefix,"r").readlines():
    if(l.count(line)>0):
      found+=1
  assert found == 1

def exercise(prefix="tst_01_model_vs_data"):
  pdb = libtbx.env.find_in_repositories(
        relative_path="phenix_regression/model_vs_data/1q09.pdb",
        test=os.path.isfile)
  hkl = libtbx.env.find_in_repositories(
        relative_path="phenix_regression/model_vs_data/1q09.mtz",
        test=os.path.isfile)
  ### CASE 1
  cmd = " ".join([
    "mmtbx.model_vs_data",
    "%s"%pdb,
    "%s"%hkl,
    "f_obs_label='F-obs-filtered'",
    "high_resolution=8",
    "> %s.zlog"%prefix])
  assert not easy_run.call(cmd)
  check(prefix=prefix,
    line = "F-obs-filtered(+),SIGF-obs-filtered(+),F-obs-filtered(-),SIGF-obs-filtered(-)")
  ### CASE 2
  cmd = " ".join([
    "mmtbx.model_vs_data",
    "%s"%pdb,
    "%s"%hkl,
    "high_resolution=8",
    "f_obs_label='F-obs('",
    "> %s.zlog"%prefix])
  assert not easy_run.call(cmd)
  check(prefix=prefix,
    line = "F-obs(+),SIGF-obs(+),F-obs(-),SIGF-obs(-)")
  ### CASE 3
  cmd = " ".join([
    "mmtbx.model_vs_data",
    "%s"%pdb,
    "%s"%hkl,
    "high_resolution=8",
    "> %s.zlog"%prefix])
  assert not easy_run.call(cmd)
  check(prefix=prefix,
    line = "F-obs(+),SIGF-obs(+),F-obs(-),SIGF-obs(-)")

if (__name__ == "__main__"):
  t0 = time.time()
  exercise()
  print("Total time: %-8.4f"%(time.time()-t0))
  print("OK")
