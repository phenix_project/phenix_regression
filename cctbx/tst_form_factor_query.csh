#! /bin/csh -fe

phenix.form_factor_query se 1.0 | grep fdp | libtbx.assert_stdin "fdp: 0.5185"
phenix.form_factor_query element=se wavelength=1.0 table=henke | grep fdp | libtbx.assert_stdin "fdp: 0.545248"
echo "OK"
