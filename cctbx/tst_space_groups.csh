#! /bin/csh -fe

cctbx.space_group_info | libtbx.assert_stdin_contains_strings 'Convert'
cctbx.space_group_info all | grep '^Space group: ' | libtbx.assert_line_count 230
cctbx.space_group_info --primitive 230 | libtbx.assert_stdin_contains_strings 'Lattice centering operations: 1'
cctbx.space_group_info --symops 230 | libtbx.assert_stdin_contains_strings 'List of symmetry operations:'

cctbx.subgroups p41 | libtbx.assert_stdin_contains_strings 'number of subgroups: 3'
echo "OK"
