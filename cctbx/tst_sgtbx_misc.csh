#! /bin/csh -fe

cctbx.sys_abs_equiv_space_groups | libtbx.assert_stdin "0 2 1  81 83 89 99 111 115 123 0"

cctbx.python "`libtbx.show_dist_paths cctbx`"/sgtbx/direct_space_asu/check_redundancies.py 12 181

cctbx.python "`libtbx.show_dist_paths cctbx`"/sgtbx/direct_space_asu/check_redundancies.py --plane_group 12 17

echo "OK"
