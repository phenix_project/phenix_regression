#! /bin/csh -fe

set PHENIX_REGRESSION_DIR="`libtbx.find_in_repositories phenix_regression`"

set nl09="`libtbx.find_in_repositories compcomm/newsletter09`"

cctbx.python "$nl09"/sf_times.py unit_test
cctbx.python "$nl09"/sf_times.py quick
cctbx.structure_factor_timings "$PHENIX_REGRESSION_DIR"/cctbx/1ab1.ent 4
cctbx.structure_factor_timings "$PHENIX_REGRESSION_DIR"/cctbx/1ab1.ent --fft_only 3

echo "OK"
