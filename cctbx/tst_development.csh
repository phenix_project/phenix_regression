#! /bin/csh -fe

set PHENIX_REGRESSION_DIR="`libtbx.find_in_repositories phenix_regression`"

cctbx.python "`libtbx.show_dist_paths cctbx`"/development/electron_density_sampling.py "$PHENIX_REGRESSION_DIR"/cctbx/1ab1.ent

echo "OK"
