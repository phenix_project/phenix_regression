#! /bin/csh -fe

set PHENIX_REGRESSION_DIR="`libtbx.find_in_repositories phenix_regression`"

cctbx.python "`libtbx.show_dist_paths cctbx`"/maptbx/boost_python/time_grid_indices_around_sites.py "$PHENIX_REGRESSION_DIR"/pdb/rebuild03_ckpt04.pdb | grep grid_indices.size | libtbx.assert_stdin 'grid_indices.size(): 1084233'

cctbx.python "`libtbx.show_dist_paths cctbx`"/crystal/burzlaff_zimmermann_dewolff.py | grep max_penalty | tail -1 | libtbx.assert_stdin "44: aP      triclinic, matrix    (1, 0, 0, 0, 1, 0, 0, 0, 1) max_penalty=0.00%"

echo "OK"
