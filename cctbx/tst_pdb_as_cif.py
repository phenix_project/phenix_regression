from __future__ import print_function
import iotbx.pdb
from libtbx import easy_run
import shutil
import os

pdb_str_1ywf_from = """\n
CRYST1  113.068  113.068   53.292  90.00  90.00  90.00 I 41          8
ORIGX1      1.000000  0.000000  0.000000        0.00000
ORIGX2      0.000000  1.000000  0.000000        0.00000
ORIGX3      0.000000  0.000000  1.000000        0.00000
SCALE1      0.008844  0.000000  0.000000        0.00000
SCALE2      0.000000  0.008844  0.000000        0.00000
SCALE3      0.000000  0.000000  0.018765        0.00000
ATOM      1  N   ARG A   4       7.511  46.981  14.258  1.00 46.91           N
ATOM      2  CA  ARG A   4       7.057  47.032  12.837  1.00 47.14           C
ATOM      3  C   ARG A   4       7.149  48.454  12.261  1.00 44.77           C
ATOM      4  O   ARG A   4       7.798  48.643  11.247  1.00 45.20           O
ATOM      5  CB  ARG A   4       5.613  46.509  12.682  1.00 47.77           C
ATOM      6  CG  ARG A   4       5.282  45.235  13.472  1.00 52.79           C
ATOM      7  CD  ARG A   4       4.921  43.958  12.663  1.00 58.16           C
ATOM      8  NE  ARG A   4       4.877  44.193  11.216  1.00 63.64           N
ATOM      9  CZ  ARG A   4       5.947  44.408  10.439  1.00 67.41           C
ATOM     10  NH1 ARG A   4       7.176  44.420  10.954  1.00 68.79           N
ATOM     11  NH2 ARG A   4       5.785  44.627   9.133  1.00 68.95           N
END
"""

def run():
  with open("tst_pdb_as_cif_symmetry.pdb","w") as fo:
    fo.write(pdb_str_1ywf_from)
  er = easy_run.call
  assert not er("phenix.pdb_as_cif tst_pdb_as_cif_symmetry.pdb")
  assert not er("mv tst_pdb_as_cif_symmetry.cif tst_pdb_as_cif_symmetry.pdb.cif")
  assert not er("phenix.cif_as_pdb tst_pdb_as_cif_symmetry.pdb.cif")
  assert os.path.isfile("tst_pdb_as_cif_symmetry.pdb.pdb")
  p1 = iotbx.pdb.input(source_info=None, lines=pdb_str_1ywf_from)
  p2 = iotbx.pdb.input(file_name="tst_pdb_as_cif_symmetry.pdb.pdb")
  assert p1.crystal_symmetry().is_identical_symmetry(p2.crystal_symmetry())


if __name__ == "__main__" :
  run()
