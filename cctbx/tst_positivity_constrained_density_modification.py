from __future__ import print_function
from scitbx.array_family import flex
from cctbx import maptbx
import iotbx.pdb
import cctbx.maptbx.mem as mem
from cctbx import miller
import math
from libtbx.test_utils import approx_equal

def map_1d(xrs, map_data, n_steps=300, step_size=0.01, tc=False):
  atom_center = xrs.scatterers()[0].site
  a_cell = xrs.unit_cell().parameters()[0]
  dist = flex.double()
  rho = flex.double()
  x=0
  while x<=10:
    x += step_size
    point = x/a_cell, 0, 0
    density_value_at_point_e = map_data.eight_point_interpolation(point)
    dist.append(x)
    rho.append(density_value_at_point_e)
  return dist, rho

def scale(x, y):
  assert x.size() == y.size()
  x = flex.abs(x)
  y = flex.abs(y)
  return flex.sum(x*y)/flex.sum(y*y)

def run_sr(map_coeffs):
  input_text="""
fom_target  0.3
mask_cycles 1
minor_cycles 0
solvent_content 0.5
database 5
no_build
fix_fom
"""#%(str(math.acos(20*math.pi/180)))
  import solve_resolve, sys
  from solve_resolve.resolve_python.resolve_in_memory import run
  result_obj=run(map_coeffs=map_coeffs, out=sys.stdout, input_text=input_text)
  cmn=result_obj.results
  from solve_resolve.resolve_python.resolve_utils import get_map_coeffs_from_resolve_refl_db
  randomized_map_coeffs=get_map_coeffs_from_resolve_refl_db(cmn.output_refl_db)
  return randomized_map_coeffs

def exercise_00(d_min=1.5, n_cycles=100):
  pdb_str="""
CRYST1   10.000   10.000   10.000  90.00  90.00  90.00 P 1
HETATM    1  C    C      1       4.271   0.000   0.000  1.00  5.00           C
HETATM    2  N    N      2       5.729   0.000   0.000  1.00  5.00           N
END
"""
  pdb_inp = iotbx.pdb.input(source_info=None, lines=pdb_str)
  ph = pdb_inp.construct_hierarchy()
  xrs = pdb_inp.xray_structure_simple()
  f_000 = xrs.f_000()
  f_full = xrs.structure_factors(d_min = 1.0, algorithm = "direct").f_calc()
  fft_map_full = f_full.fft_map(resolution_factor = 0.15, f_000=f_000)
  #
  fft_map_full.apply_volume_scaling()
  map = fft_map_full.real_map_unpadded()
  r, rho = map_1d(xrs=xrs, map_data=map)
  rhos=[rho]
  legs=["d_min=%s"%str(1)]
  #
  f = f_full.resolution_filter(d_min=d_min)
  f = f.sort()
  n = int(0.3*f.data().size())
  f = f.customized_copy(data = f.data()[n:], indices = f.indices()[n:])
  #
  fft_map = miller.fft_map(
    crystal_gridding = fft_map_full, fourier_coefficients = f, f_000=f_000)
  fft_map.apply_volume_scaling()
  map = fft_map.real_map_unpadded()
  r, rho = map_1d(xrs=xrs, map_data=map)
  rhos.append(rho)
  legs.append("d_min=%s_inc"%str(d_min))
  #
  fr = run_sr(f)
  assert approx_equal(scale(abs(f).data(), abs(fr).data()), 1)
  print("Introduced mean_phase_shake:", f.mean_phase_error(fr))
  fft_map = miller.fft_map(
    crystal_gridding = fft_map_full, fourier_coefficients = fr, f_000=f_000)
  fft_map.apply_volume_scaling()
  map = fft_map.real_map_unpadded()
  r, rho = map_1d(xrs=xrs, map_data=map)
  rhos.append(rho)
  legs.append("rand")
  #
  o = maptbx.positivity_constrained_density_modification(f = fr, f_000=f_000,
    n_cycles=100, resolution_factor=0.15, d_min=1.0)
  map = o.map
  r, rho = map_1d(xrs=xrs, map_data=map)
  rhos.append(rho)
  legs.append("DM")
  ##### gnuplot output
  ofn = "cn_syn"
  of = open(ofn,"w")
  for i in range(rhos[0].size()):
    print("%12.8f"%r[i] + "".join(["%12.8f"%j[i] for j in rhos]), file=of)
  of.close()
  #
  pl = []
  for i in range(len(rhos)):
    pl.append(""" "%s" using 1:%d with lines lw 3 title "%s" """%(ofn, i+2, legs[i]))
  print("plot " + ",".join(pl))
  #

if __name__ == "__main__" :
  try:
    import solve_resolve
    from solve_resolve.resolve_python.resolve_in_memory import run
    from solve_resolve import resolve
  except ImportError:
    print("solve_resolve not available: skipping test.")
    import sys
    sys.exit(0)
  exercise_00()
