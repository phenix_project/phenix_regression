from __future__ import print_function
import os
import libtbx.load_env
import iotbx.pdb
from scitbx.array_family import flex
import iotbx.xplor.map
from scitbx.math import superpose
from cctbx import maptbx
from libtbx.math_utils import ifloor, iceil
from libtbx import easy_run
import mmtbx.utils

def lsq_fit(fixed_sites, moving_sites):
  rmsd1 = fixed_sites.rms_difference(moving_sites)
  lsq_fit_obj = superpose.least_squares_fit(reference_sites = fixed_sites,
                                            other_sites     = moving_sites)
  moving_sites = lsq_fit_obj.other_sites_best_fit()
  rmsd2 = fixed_sites.rms_difference(lsq_fit_obj.other_sites_best_fit())
  return lsq_fit_obj, rmsd1, rmsd2

def apply_shift(rt, site_cart):
  new_site =  rt.r.elems * flex.vec3_double([site_cart]) + rt.t.elems
  return new_site[0]

def write_xplor_map(sites_cart, unit_cell, map_data, n_real, file_name):
  frac_min, frac_max = unit_cell.box_frac_around_sites(
    sites_cart = sites_cart, buffer = 10.)
  gridding_first=[ifloor(f*n) for f,n in zip(frac_min,n_real)]
  gridding_last=[iceil(f*n) for f,n in zip(frac_max,n_real)]
  gridding = iotbx.xplor.map.gridding(n     = map_data.focus(),
                                      first = gridding_first,
                                      last  = gridding_last)
  iotbx.xplor.map.writer(
    file_name          = file_name,
    is_p1_cell         = True,
    title_lines        = [' None',],
    unit_cell          = unit_cell,
    gridding           = gridding,
    data               = map_data,
    average            = -1,
    standard_deviation = -1)

def exercise():
  pdb_file_name_1 = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/phe.pdb",
    test=os.path.isfile)
  print(pdb_file_name_1)
  pdb_inp = iotbx.pdb.input(file_name = pdb_file_name_1)
  xrs1 = pdb_inp.construct_hierarchy().extract_xray_structure(
    crystal_symmetry = pdb_inp.crystal_symmetry())
  ofn = "phe_rt.pdb"
  cmd = " ".join([
    "phenix.pdbtools",
    "%s"%pdb_file_name_1,
    "rotate='90 10 20' translate='3 3 3'",
    "suffix=none",
    "prefix=phe_rt"])
  assert not easy_run.call(cmd)
  xrs2 = iotbx.pdb.input(file_name = ofn).xray_structure_simple()
  mmtbx.utils.assert_xray_structures_equal(
    x1 = xrs1,
    x2 = xrs2,
    sites = False,
    adp = True,
    occupancies = True,
    elements = True,
    scattering_types = True,
    eps = 1.e-6)
  fft_map_1 = xrs1.structure_factors(d_min=1.5).f_calc().fft_map(
    resolution_factor = 1./7)
  fft_map_1.apply_sigma_scaling()
  map_data_1 = fft_map_1.real_map_unpadded()
  if 1:
    write_xplor_map(sites_cart = xrs1.sites_cart(),
                    unit_cell  = xrs1.unit_cell(),
                    map_data   = map_data_1,
                    n_real     = fft_map_1.n_real(),
                    file_name  = "phe.xplor")
  rt, rmsd1, rmsd2 = lsq_fit(fixed_sites = xrs1.sites_cart(),
    moving_sites = xrs2.sites_cart())
  assert rmsd1 > 5.
  assert rmsd2 < 1.e-3, rmsd2
  map_data_2 = maptbx.rotate_translate_map(
    unit_cell          = xrs1.unit_cell(),
    map_data           = fft_map_1.real_map_unpadded(),
    rotation_matrix    = rt.r.elems,
    translation_vector = rt.t.elems)
  if 1:
          write_xplor_map(sites_cart = xrs2.sites_cart(),
                    unit_cell  = xrs2.unit_cell(),
                    map_data   = map_data_2,
                    n_real     = fft_map_1.n_real(),
                    file_name  = "phe_rt.xplor")
  for sf1, sf2 in zip(xrs1.sites_frac(), xrs2.sites_frac()):
    e1 = map_data_1.eight_point_interpolation(sf1)
    e2 = map_data_2.eight_point_interpolation(sf2)
    assert abs(e1-e2) < 1.
  print("OK")

def exercise2 () :
  pdb_file_name_1 = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/misc/1l3r_no_ligand.pdb",
    test=os.path.isfile)
  xrs1 = iotbx.pdb.input(file_name = pdb_file_name_1).xray_structure_simple()
  ofn = "1l3r_rt.pdb"
  cmd = " ".join([
    "phenix.pdbtools",
    "%s"%pdb_file_name_1,
    "rotate='90 10 20' translate='10 10 10'",
    "output.prefix=1l3r_rt",
    "suffix=none"])
  assert not easy_run.call(cmd)
  xrs2 = iotbx.pdb.input(file_name = ofn).xray_structure_simple()
  fft_map_1 = xrs1.structure_factors(d_min=1.5).f_calc().fft_map(
    resolution_factor = 1./3)
  fft_map_1.apply_sigma_scaling()
  map_data_1 = fft_map_1.real_map_unpadded()
  if 1:
    write_xplor_map(sites_cart = xrs1.sites_cart(),
                    unit_cell  = xrs1.unit_cell(),
                    map_data   = map_data_1,
                    n_real     = fft_map_1.n_real(),
                    file_name  = "1l3r.xplor")
  rt, rmsd1, rmsd2 = lsq_fit(fixed_sites = xrs1.sites_cart(),
    moving_sites = xrs2.sites_cart())
  assert rmsd1 > 5.
  assert rmsd2 < 1.e-3
  map_data_2 = maptbx.rotate_translate_map(
    unit_cell          = xrs1.unit_cell(),
    map_data           = fft_map_1.real_map_unpadded(),
    rotation_matrix    = rt.r.elems,
    translation_vector = rt.t.elems)
  if 1:
    write_xplor_map(sites_cart = xrs2.sites_cart(),
                    unit_cell  = xrs2.unit_cell(),
                    map_data   = map_data_2,
                    n_real     = fft_map_1.n_real(),
                    file_name  = "1l3r_rt.xplor")
  for sf1, sf2 in zip(xrs1.sites_frac(), xrs2.sites_frac()):
    e1 = map_data_1.eight_point_interpolation(sf1)
    e2 = map_data_2.eight_point_interpolation(sf2)
    assert abs(e1-e2) < 1.
  print("OK")

if (__name__ == "__main__"):
  exercise()
