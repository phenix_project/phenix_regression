#! /bin/csh -fe

set PHENIX_REGRESSION_DIR="`libtbx.find_in_repositories phenix_regression`"

cctbx.python "`libtbx.show_dist_paths cctbx`"/run_examples.py --Quick

cctbx.python "`libtbx.show_dist_paths cctbx`"/examples/symops_530.py | libtbx.assert_line_count 23224

set ccp4_symop_lib="`libtbx.show_dist_paths ccp4io`"/libccp4/data/symop.lib

cctbx.python "`libtbx.show_dist_paths cctbx`"/examples/convert_ccp4_symop_lib.py < "$ccp4_symop_lib" > convert_ccp4_symop_py_out
grep "Fd-3m" convert_ccp4_symop_py_out | libtbx.assert_stdin "227 192 48 Fd-3m PGm3barm CUBIC 'F 41/d -3 2/m' 'F d -3 m'"

cctbx.convert_ccp4_symop_lib < "$ccp4_symop_lib" > convert_ccp4_symop_cpp_out

diff convert_ccp4_symop_py_out convert_ccp4_symop_cpp_out | libtbx.assert_stdin ""

echo "OK"
