#! /bin/csh -fe

set PHENIX_REGRESSION_DIR="`libtbx.find_in_repositories phenix_regression`"

cctbx.python "`libtbx.show_dist_paths cctbx`"/web/asu_gallery/jv_asu.py 213

cctbx.python "`libtbx.show_dist_paths cctbx`"/web/replay.py "$PHENIX_REGRESSION_DIR"/cctbx/web/[a-z]* | grep -i error


echo "OK"
