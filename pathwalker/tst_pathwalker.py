# -*- coding: utf-8 -*-
from __future__ import division, print_function

import time, sys, os
from libtbx.test_utils import approx_equal
import libtbx.load_env

def tst_01():

  # Identify where the data are to come from
  data_dir = libtbx.env.under_dist(
    module_name="phenix_regression",
    path="pathwalker",
    test=os.path.isdir)

  # Identify the map and model files
  map_file=os.path.join(data_dir,"41test_path_map.mrc")
  offset_map_file=os.path.join(data_dir,"41test_path_map_offset.mrc")

  # Set the resolution
  threshold = 0.23456
  # Set the number of pseudoatoms
  pseudoatoms = 41

  # set defaults
  from libtbx import group_args
  defaults = group_args(
    group_args_type = 'defaults',
    seq_file=None,
    pa_file=None,
    verbose=False,
    tsp='ortools',
    pa_type=str('kmeans'),
    noise=float(0),
    map_weight=False,
    filter=False,
    filter_resolution=float(4.5),
    local_search_strategy='GUIDED_LOCAL_SEARCH',
    refine_cycles=int(5),
    or_time=int(30),
    prob_model=False,
    all_atom=False,
    bracket=[],
    reverse=False,
    refine_resolution=float(0),
    tsp_runs=[0],
    start_pt=int(0),
    end_pt=int(0),
    average_model=False,
    )

  # Import and test the program pathwalker.py:
  from phenix_pathwalker.tools.pathwalker import run_pathwalker
  from copy import deepcopy

  result = run_pathwalker(
    map_name = map_file,
    map_thr = threshold,
    nres = pseudoatoms,
    strategy = deepcopy(defaults))
  # print ("Result was: %.3f" %(result))
  std_path_length = result.path_length_info.path_length

  # Make sure we got the right answer. Fails if result.path_distance does not match up
  assert abs(result.path_length_info.path_length - 164) < 100

  # Now run with offset map
  result = run_pathwalker(
    map_name = map_file,
    map_thr = threshold,
    nres = pseudoatoms,
    strategy = deepcopy(defaults))

  offset_path_length = result.path_length_info.path_length

  assert approx_equal(std_path_length, offset_path_length, eps = 100)


  # We are done...if we had the wrong answer it would have crashed above

if __name__=="__main__":
  try:
    import ortools
  except ImportError:
    print("ortools is not available, skipping test")
    sys.exit()

  t0 = time.time()
  tst_01()
  print('')
  print("Time: %6.4f"%(time.time()-t0))
  print('')
  print("Test Pass - Pathwalker runs correctly!")
