#! /bin/csh -f

set PHENIX_REGRESSION_DIR="`libtbx.find_in_repositories phenix_regression`"
set tst_tardy_pdb="`libtbx.show_dist_paths phenix_regression`"/tardy_action/tardy_pdb.py
set tst_tardy_comprehensive="`libtbx.show_dist_paths phenix_regression`"/tardy_action/tardy_comprehensive.py

# set verbose

mmtbx.python "$tst_tardy_pdb" random_seed=0 "$PHENIX_REGRESSION_DIR"/tardy_action/gly_gly_box.pdb

mmtbx.python "$tst_tardy_pdb" random_seed=0 "$PHENIX_REGRESSION_DIR"/tardy_action/loop_edge_i_gt_j.pdb | libtbx.assert_stdin_contains_strings 'number of lbfgs restarts: '

mmtbx.python "$tst_tardy_pdb" random_seed=0 "$PHENIX_REGRESSION_DIR"/tardy_action/gly_gly_box.pdb "$PHENIX_REGRESSION_DIR"/tardy_action/gly_gly_box_mod.pdb

mmtbx.python "$tst_tardy_pdb" random_seed=0 "$PHENIX_REGRESSION_DIR"/tardy_action/gly_gly_box.pdb tardy_displacements=Auto

mmtbx.python "$tst_tardy_pdb" random_seed=0 "$PHENIX_REGRESSION_DIR"/tardy_action/gly_gly_box.pdb tardy_displacements=Auto tardy_displacements_auto.parameterization=cartesian

mmtbx.python "$tst_tardy_pdb" random_seed=0 "$PHENIX_REGRESSION_DIR"/tardy_action/gly_gly_box.pdb tardy_displacements=1.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,1.0,1.0

mmtbx.python "$tst_tardy_pdb" random_seed=0 "$PHENIX_REGRESSION_DIR"/tardy_action/gly_gly_box.pdb tardy_displacements=1.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,1.0,1.0,9 | & tail -n 1 | cut -d' ' -f2- | libtbx.assert_stdin 'Incompatible tardy_displacements.'

mmtbx.python "$tst_tardy_pdb" random_seed=0 "$PHENIX_REGRESSION_DIR"/tardy_action/gly_gly_box.pdb "$PHENIX_REGRESSION_DIR"/tardy_action/gly_gly_box_mod.pdb tardy_displacements=1.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,1.0,1.0

mmtbx.python "$tst_tardy_pdb" random_seed=0 "$PHENIX_REGRESSION_DIR"/tardy_action/gly_gly_box.pdb "$PHENIX_REGRESSION_DIR"/tardy_action/gly_gly_box_mod.pdb tardy_displacements=1.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,1.0,1.0 emulate_cartesian=True

mmtbx.python "$tst_tardy_comprehensive" pdb_file="$PHENIX_REGRESSION_DIR"/tardy_action/gly_gly_box.pdb algorithm=minimization hot=False

mmtbx.python "$tst_tardy_comprehensive" pdb_file="$PHENIX_REGRESSION_DIR"/tardy_action/gly_gly_box.pdb algorithm=minimization chunk=72,0 number_of_random_trials=1 hot=True

mmtbx.python "$tst_tardy_comprehensive" pdb_file="$PHENIX_REGRESSION_DIR"/tardy_action/gly_gly_box.pdb algorithm=annealing hot=False

mmtbx.python "$tst_tardy_comprehensive" pdb_file="$PHENIX_REGRESSION_DIR"/tardy_action/gly_gly_box.pdb algorithm=annealing chunk=288,0 number_of_random_trials=1 hot=True
