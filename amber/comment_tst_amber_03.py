import os, sys
from phenix.command_line import amber_run_tests
rc = amber_run_tests.main(only_i=3)
assert not rc, 'Amber errors: %s' % rc
