import os, sys

from phenix.command_line import amber_run_tests

def main(only_i=0):
  return amber_run_tests.main(only_i=only_i)

if __name__ == '__main__':
  rc = main(*tuple(sys.argv[1:]))
  assert not rc, 'Amber errors: %s' % rc
