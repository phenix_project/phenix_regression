from __future__ import print_function
import libtbx.load_env
import os, time
from libtbx import easy_run
from libtbx.test_utils import run_command, approx_equal

PREFIX = "tst_real_space_correlation"

def check(o, t):
  t = t.strip().split()
  found = False
  for l in o:
    l = l.strip().split()
    if(len(l)>0 and t[0]==l[0] and t[1]==l[1] and t[2]==l[2]):
      t3,l3 = float(t[3]), float(l[3])
      t4,l4 = float(t[4]), float(l[4])
      t5,l5 = float(t[5]), float(l[5])
      t6,l6 = float(t[6]), float(l[6])
      t7,l7 = float(t[7]), float(l[7])
      if(abs(t3-l3)<0.01 and abs(t4-l4)<0.01 and abs(t5-l5)<0.0001 and
         abs(t6-l6)<0.5 and abs(t7-l7)<0.5):
        found=True
        break
  return found

def exercise_1(prefix=PREFIX+"_1"):
  # Basics: x-ray data
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/real_space_correlation/m1.pdb",
    test=os.path.isfile)
  hkl_file = prefix+".mtz"
  cmd = "phenix.fmodel %s high_res=1.0 type=real output.file_name=%s > zlog"%(
    pdb_file, hkl_file)
  assert not easy_run.call(cmd)
  #
  base = "phenix.real_space_correlation %s %s"%(pdb_file,hkl_file)
  #
  cmd = "%s"%base
  o = easy_run.fully_buffered(cmd).raise_if_errors().stdout_lines
  assert "Rho1 = Fc, Rho2 = 2mFo-DFc" in o
  assert check(t="     HOH    1   O  0.70    5.00  1.0000  47.61  47.68", o=o)
  assert check(t="     HOH    1   H1 0.90   10.00  1.0000   4.82   4.69", o=o)
  assert check(t="     HOH    1   H2 1.00   15.00  1.0000   4.68   4.65", o=o)
  #
  cmd = "%s detail=atom"%base
  o = easy_run.fully_buffered(cmd).raise_if_errors().stdout_lines
  assert "Rho1 = Fc, Rho2 = 2mFo-DFc" in o
  assert check(t="     HOH    1   O  0.70    5.00  1.0000  47.61  47.68", o=o)
  assert check(t="     HOH    1   H1 0.90   10.00  1.0000   4.82   4.69", o=o)
  assert check(t="     HOH    1   H2 1.00   15.00  1.0000   4.68   4.65", o=o)
  #
  cmd = "%s detail=atom use_hydrogens=false"%base
  o = easy_run.fully_buffered(cmd).raise_if_errors().stdout_lines
  assert "Rho1 = Fc, Rho2 = 2mFo-DFc" in o
  assert     check(t="     HOH    1   O  0.70    5.00  1.0000  47.61  47.68",o=o)
  assert not check(t="     HOH    1   H1 0.90   10.00  1.0000   4.82   4.69",o=o)
  assert not check(t="     HOH    1   H2 1.00   15.00  1.0000   4.68   4.65",o=o)
  #
  cmd = "%s map_1.type=Fo map_2.type=Fc"%base
  o = easy_run.fully_buffered(cmd).raise_if_errors().stdout_lines
  assert "Rho1 = Fo, Rho2 = Fc" in o
  assert check(t="     HOH    1   O  0.70    5.00  1.0000  47.66  47.61",o=o)
  assert check(t="     HOH    1   H1 0.90   10.00  1.0000   4.72   4.82",o=o)
  assert check(t="     HOH    1   H2 1.00   15.00  1.0000   4.67   4.68",o=o)

def exercise_2(prefix=PREFIX+"_2"):
  # Basics: neutron data
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/real_space_correlation/m1.pdb",
    test=os.path.isfile)
  hkl_file = prefix+".mtz"
  cmd = "phenix.fmodel %s high_res=1.0 type=real output.file_name=%s scattering_table=neutron > zlog"%(
    pdb_file, hkl_file)
  assert not easy_run.call(cmd)
  #
  base = "phenix.real_space_correlation %s %s scattering_table=neutron"%(
    pdb_file,hkl_file)
  #
  cmd = "%s"%base
  o = easy_run.fully_buffered(cmd).raise_if_errors().stdout_lines
  assert check(t="     HOH    1   O  0.70    5.00  0.9999  47.78  47.80",o=o)
  assert check(t="     HOH    1   H1 0.90   10.00  0.9999 -25.95 -26.02",o=o)
  assert check(t="     HOH    1   H2 1.00   15.00  0.9999 -17.78 -17.86",o=o)
  #
  cmd = "%s detail=atom"%base
  o = easy_run.fully_buffered(cmd).raise_if_errors().stdout_lines
  assert check(t="     HOH    1   O  0.70    5.00  0.9999  47.78  47.80",o=o)
  assert check(t="     HOH    1   H1 0.90   10.00  0.9999 -25.95 -26.02",o=o)
  assert check(t="     HOH    1   H2 1.00   15.00  0.9999 -17.78 -17.86",o=o)
  #
  cmd = "%s detail=atom use_hydrogens=false"%base
  o = easy_run.fully_buffered(cmd).raise_if_errors().stdout_lines
  assert     check(t="     HOH    1   O  0.70    5.00  0.9999  47.78  47.80",o=o)
  assert not check(t="     HOH    1   H1 0.90   10.00  0.9999 -25.95 -26.02",o=o)
  assert not check(t="     HOH    1   H2 1.00   15.00  0.9999 -17.78 -17.86",o=o)
  #
  cmd = "%s detail=residue"%base
  o = easy_run.fully_buffered(cmd).raise_if_errors().stdout_lines
  assert     "     HOH    1   0.87   10.00  0.9999   1.35   1.31" in o

def exercise_3(prefix=PREFIX+"_3"):
  # Use data labels, stop if bad parameter
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/real_space_correlation/m1.pdb",
    test=os.path.isfile)
  hkl_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/real_space_correlation/m.mtz",
    test=os.path.isfile)
  #
  base = "phenix.real_space_correlation %s %s"%(pdb_file,hkl_file)
  #
  cmd = "%s data_labels=FX"%base
  o = easy_run.fully_buffered(cmd).raise_if_errors().stdout_lines
  assert "Rho1 = Fc, Rho2 = 2mFo-DFc" in o
  assert " <----id string---->  occ     ADP      CC   Rho1   Rho2" in o
  assert check(t="     HOH    1   O  0.70    5.00  1.0000  47.61  47.68",o=o)
  assert check(t="     HOH    1   H1 0.90   10.00  1.0000   4.82   4.69",o=o)
  assert check(t="     HOH    1   H2 1.00   15.00  1.0000   4.68   4.65",o=o)
  #
  cmd = "%s data_labels=FN scattering_table=neutron"%base
  o = easy_run.fully_buffered(cmd).raise_if_errors().stdout_lines
  assert "Rho1 = Fc, Rho2 = 2mFo-DFc" in o
  assert " <----id string---->  occ     ADP      CC   Rho1   Rho2" in o
  assert check(t="     HOH    1   O  0.70    5.00  0.9999  47.78  47.80",o=o)
  assert check(t="     HOH    1   H1 0.90   10.00  0.9999 -25.95 -26.02",o=o)
  assert check(t="     HOH    1   H2 1.00   15.00  0.9999 -17.78 -17.86",o=o)
  #
  cmd = "%s data_labels=FN XXX=neutron > %s.log"%(base,prefix)
  o = easy_run.fully_buffered(cmd).stderr_lines
  assert "Sorry: Unused parameters: see above." in o

def exercise_4(prefix=PREFIX+"_4"):
  # Use parameter file
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/real_space_correlation/m1.pdb",
    test=os.path.isfile)
  hkl_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/real_space_correlation/m.mtz",
    test=os.path.isfile)
  #
  par_str = """\
  scattering_table = n_gaussian wk1995 it1992 *neutron
  detail = atom residue *automatic
  map_1 {
    type = Fo
  }
  map_2 {
    type = 2Fo-Fc
  }
  pdb_file_name = %s
  reflection_file_name = %s
  data_labels = FN
  """%(pdb_file, hkl_file)
  par_file_name = "%s.params"%prefix
  f = open(par_file_name, "w")
  f.write(par_str)
  f.close()
  #
  base = "phenix.real_space_correlation %s"%par_file_name
  #
  cmd = "%s "%base
  o1 = easy_run.fully_buffered(cmd).raise_if_errors().stdout_lines
  #
  cmd = "phenix.real_space_correlation %s %s data_labels=FN scattering_table=neutron map_1.type=Fo map_2.type=2Fo-Fc"%(
    pdb_file, hkl_file)
  o2 = easy_run.fully_buffered(cmd).raise_if_errors().stdout_lines
  for o in [o1, o2]:
    assert "Rho1 = Fo, Rho2 = 2Fo-Fc" in o1
    assert " <----id string---->  occ     ADP      CC   Rho1   Rho2" in o
    assert check(t="     HOH    1   O  0.70    5.00  1.0000  47.80  47.80",o=o)
    assert check(t="     HOH    1   H1 0.90   10.00  1.0000 -26.02 -26.02",o=o)
    assert check(t="     HOH    1   H2 1.00   15.00  1.0000 -17.86 -17.86",o=o)

if (__name__ == "__main__"):
  t0 = time.time()
  exercise_1()
  exercise_2()
  exercise_3()
  exercise_4()
  print("Time: %8.3f" % (time.time() - t0))
