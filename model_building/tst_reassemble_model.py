from __future__ import division
from __future__ import print_function
import os , time
import libtbx.load_env






data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path=os.path.join("model_building"),
  test=os.path.isdir)

def tst_01(data_dir):

  try:
    from iotbx.cli_parser import run_program
    from phenix.programs import reassemble_model_cryo_em as run
  except Exception as e:
    print("reassemble_model_cryo_em not available...skipping")
    return

  args = "resolution=4 refine_cycles=1 model=%s full_map=%s seq_file=%s thoroughness=quick nproc=1" %(
      os.path.join(data_dir,"5376_small_split.pdb"),
      os.path.join(data_dir,"5376_small.ccp4"),
      os.path.join(data_dir,"5376_small.seq"))
  args = args.split()

  new_model = run_program(program_class=run.Program,args=args)

if __name__=="__main__":

  t0 = time.time()
  tst_01(data_dir)

  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
