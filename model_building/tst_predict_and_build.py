from __future__ import division
from __future__ import print_function
import sys,os
import libtbx.load_env
from libtbx.test_utils import approx_equal
import random
from scitbx.array_family import flex
from iotbx.map_model_manager import map_model_manager as MapModelManager
from iotbx.data_manager import DataManager
from phenix.model_building import local_model_building

import time,sys

data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="model_building",
  test=os.path.isdir)

def extract_files():
  import tarfile
  f = tarfile.open(os.path.join(data_dir,'7n8i_24237_L_predict_and_build.tgz'))
  f.extractall('.')
  f.close()

def tst_01():

  print("Testing predict_and_build with pre-run files")

  # Set up a data_manager to read and write files
  dm=DataManager()

  # Read in a cut_out map and a model and refit a loop

  try:
    from iotbx.cli_parser import run_program
    from phenix.programs import predict_and_build as run
  except Exception as e:
    print("predict_and_build not available...skipping")
    return

  if sys.version_info.major != 3:
   print("predict_and_build requires version 3 of python")
   return

  dd = 'tst_predict_and_build'
  if not os.path.isdir(dd):
    os.mkdir(dd)
  os.chdir(dd)
  extract_files()
  os.chdir('7n8i_24237_L')

  args = """full_map=7n8i_24237_box.ccp4 crystal_info.resolution=3 seq_file=7n8i_24237.seq carry_on=true jobname=7n8i_24237_set_s5 cycles=10 carry_on_directory=7n8i_24237_set_s5 include_templates_from_pdb=false get_msa_with_mmseqs2=false""".split()

  print("phenix.predict_and_build %s" %(" ".join(args)))

  dr = run_program(program_class=run.Program,args=args)
  assert dr.best_final_model.first_resseq_as_int() == 1
  assert dr.best_final_model.last_resseq_as_int() == 106

def tst_02():

  print("Testing predict_and_build with pre-run files and trmming predict only")

  # Set up a data_manager to read and write files
  dm=DataManager()

  # Read in a cut_out map and a model and refit a loop

  try:
    from iotbx.cli_parser import run_program
    from phenix.programs import predict_and_build as run
  except Exception as e:
    print("predict_and_build not available...skipping")
    return

  if sys.version_info.major != 3:
   print("predict_and_build requires version 3 of python")
   return

  dd = 'tst_predict_and_build'
  if not os.path.isdir(dd):
    os.mkdir(dd)
  os.chdir(dd)
  extract_files()
  os.chdir('7n8i_24237_L')


  args = """full_map=7n8i_24237_box.ccp4 crystal_info.resolution=3 predicted_model=7n8i_24237_set_s5_predicted_1_cycle_1.pdb  carry_on=true jobname=7n8i_24237_set_s5 cycles=10 include_templates_from_pdb=False carry_on_directory=7n8i_24237_set_s5 prediction_server=LocalServer include_side_in_templates=False stop_after_predict=True crystal_info.sequence=%s""" %( "SLSASVGDRVTITCQASQDIRFYLNWYQQKPGKAPKLLISDASNMETGVPSRFSGSGSGTDFTFTISSLQPEDIATYYCQQYDNLPFTFGP")
  args = args.split()
  print("phenix.predict_and_build %s" %(" ".join(args)))

  dr = run_program(program_class=run.Program,args=args)
  assert dr.best_final_model.first_resseq_as_int() == 10
  assert dr.best_final_model.last_resseq_as_int() == 100

if __name__=="__main__":

  t0 = time.time()
  tst_01()
  tst_02()

  print("Time: %6.4f"%(time.time()-t0))
  print("OK")

