from __future__ import division, print_function
import sys, os, time


def tst_01():
  from iotbx.map_model_manager import map_model_manager as MapModelManager
  mmm = MapModelManager()
  mmm.generate_map()
  build = mmm.model_building()
  build.set_defaults(nproc=1)
  build.set_log(sys.stdout)
  build.set_defaults(debug=True)
  model_info = build.structure_search(sequence_list = ['XAREFSLEKTRNIGIMAHIDAGKTTTTERILYYTGRIHXXXXXXXXXXXXXXXXXXXXXXXXXTSAATTAAWEGHRVNIIDTPGHVDFTVEVERSLRVLDGAVTVLDAQSGVEPQTETVWRQATTYGVPRIVFVNKMDKLGANFEYSVSTLHDRLQANAAPIQLPIGAEDEFEAIIDLVEMKCFKYTNDLGTEIEEIEIPEDHLDRAEEARASLIEAVAETSDELMEKYLGDEEISVSELKEAIRQATTNVEFYPVLCGTAFKNKGVQLMLDAVIDYLPSPLDVKPIIGHRASNPEEEVIAKADDSAEFAALAFKVMTDPYVGKLTFFRVYSGTMTSGSYVKNSTKGKRERVGRLLQMHANSRQEIDTVYSGDIAAAVGLKDTGTGDTLCGEKNDIILESMEFPEPVIHLSVEPKSKADQDKMTQALVKLQEEDPTFHAHTXXXTGQVIIGGMGELHLDILVDRMKKEFNVECNVGAPMVSYRETFKSSAQVQGKFSRQSGGRGQYGDVHIEFTPNETGAGFEFENAIVGGVVPREYIPSVEAGLKDAMENGVLAGYPLIDVKAKLYDGSYHDVDSSEMAFKIAASLALKEAAKKCDPVILEPMMKVTIEMPEEYMGDIMGDVTSRRGRVDGMEPRGNAQVVNAYVPLSEMFGYATSLRSNTQGRGTYTMYFDHYAEVPKSIAEDIIKKNKGE'],
    number_of_models = 1)
  assert len(model_info.model_list) == 1

def tst_02():
  from iotbx.map_model_manager import map_model_manager as MapModelManager
  mmm = MapModelManager()
  mmm.generate_map()
  build = mmm.model_building()
  build.set_defaults(nproc=1)
  model_info = build.structure_search(pdb_id = '2xex',
    number_of_models = 1)
  assert len(model_info.model_list) == 1

if __name__ == "__main__":

  t0 = time.time()
  tst_01()
  tst_02()

  print("Time: %6.4f"%(time.time()-t0))
  print("OK")

