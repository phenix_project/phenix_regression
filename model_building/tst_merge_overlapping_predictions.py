from __future__ import division, print_function
import sys, os, time

import libtbx.load_env
from libtbx.test_utils import approx_equal
from iotbx.data_manager import DataManager

from phenix.model_building.merge_overlapping_predictions import  \
  merge_overlapping_predictions

data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="model_building",
  test=os.path.isdir)

model_fn_list = []
for fn in 'a_1-50_ca.pdb b_25-75_ca.pdb c_50-100_ca.pdb'.split():
  model_fn_list.append(os.path.join(data_dir,fn))

fa_fn = os.path.join(data_dir,"a_1-100_ca.fa")

def tst_01(log = sys.stdout):

  dm = DataManager()
  model_list = []
  for fn in model_fn_list:
    model_list.append(dm.get_model(fn))
  sequence = ""
  for s in dm.get_sequence(fa_fn):
     sequence +="%s" %(s.sequence.strip())

  result = merge_overlapping_predictions(model_list = model_list,
    sequence = sequence)

if __name__ == "__main__":

  t0 = time.time()
  tst_01()

  print("Time: %6.4f"%(time.time()-t0))
  print("OK")

