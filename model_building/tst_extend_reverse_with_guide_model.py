from __future__ import division, print_function

from libtbx import group_args
import sys, time, os
from scitbx.array_family import flex
from scitbx.matrix import col
import libtbx.load_env
data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="model_building",
  test=os.path.isdir)

map_file=os.path.join(data_dir,"c_term_box.ccp4")
model_file = os.path.join(data_dir,"c_term_stub.pdb")
guide_model_file = os.path.join(data_dir, "n_term_guide_model.pdb")


def tst_01():

  # get a target model and an insertion model
  from iotbx.data_manager import DataManager
  dm = DataManager()
  dm.set_overwrite(True)

  mmm = dm.get_map_model_manager(map_files=map_file,
     model_file=model_file,
     )

  guide_model = dm.get_model(guide_model_file)
  guide_model.add_crystal_symmetry_if_necessary(
     crystal_symmetry= mmm.model().crystal_symmetry())
  mmm.add_model_by_id(model=guide_model, model_id = 'guide model')


  mmm.set_log(sys.stdout)
  mmm_sav = mmm.deep_copy()
  sequence="""CLWDLQNKAERQNDILV"""
  build = mmm.model_building()
  build.set_defaults(sequence=sequence, debug=True)
  print("\nModel file: \n", build.model().model_as_pdb())
  print("\nGuide model file: \n",guide_model.model_as_pdb())
  extended = build.extend_reverse(extend_methods=['trace_through_density'],
    refine_cycles = 0, target_number_of_residues_to_build=16,
        guide_model = guide_model)
  print("\nInsert with residue on end:\n",extended.model_as_pdb())

  
  sequence_found = extended.as_sequence(as_string = True)
  assert sequence_found.find("CLWDLQNKAERQNDILV") > -1

if __name__ == "__main__":

  t0 = time.time()
  tst_01()

  print("Time: %6.4f"%(time.time()-t0))
  print("OK")

