from __future__ import division
from __future__ import print_function
import sys,os
import libtbx.load_env
from libtbx.test_utils import approx_equal
import random
from scitbx.array_family import flex
from iotbx.map_model_manager import map_model_manager as MapModelManager
from iotbx.data_manager import DataManager
from phenix.model_building import local_model_building

import time,sys

data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="model_building",
  test=os.path.isdir)

map_file=os.path.join(data_dir,"dummy_af.ccp4")
model_file=os.path.join(data_dir,"dummy_af.pdb")
docked_model_file=os.path.join(data_dir,"dummy_af_docked.pdb")

def tst_01(map_file, model_file):

  print(
    "Testing rebuild_predicted_model with small map and dummy alphafold model.")

  # Set up a data_manager to read and write files
  dm=DataManager()

  try:
    from iotbx.cli_parser import run_program
    from phenix.programs import rebuild_predicted_model as run
  except Exception as e:
    print("rebuild_predicted_model not available...skipping")
    return

  args = "docked_model_file=%s model=%s full_map=%s resolution=3 rigid_body_refinement=False rigid_body_refine_cycles=0 refine_cycles=0 run_trace_loops_through_density=False  run_iterative_resolution_refine=False run_extend=False" %(docked_model_file, model_file, map_file)
  args = args.split()

  dr = run_program(program_class=run.Program,args=args)



if __name__=="__main__":

  t0 = time.time()
  tst_01(map_file, model_file)

  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
