from __future__ import division, print_function
import time


def tst_01():
  from iotbx.map_model_manager import map_model_manager as MapModelManager
  from phenix.model_building.compact_hierarchy import compact_hierarchy
  from phenix.model_building.compact_hierarchy import ph_as_json_string
  from phenix.model_building.compact_hierarchy import json_string_as_ph

  mmm = MapModelManager()
  mmm.generate_map()
  ph = mmm.model().get_hierarchy()
  ch = compact_hierarchy(ph)
  text_1 = str(ch)
  assert len(str(ch).splitlines()) == 51,len(str(ch).splitlines())
  state =  ch.__getstate__()
  new_ch = compact_hierarchy()
  new_ch.__setstate__(state)
  text_2 = str(new_ch)
  assert len(str(new_ch).splitlines()) ==51

  # all atoms

  ph = mmm.model().get_hierarchy()
  json_string = ph_as_json_string(ph = ph,
    include_all=True)
  ph = json_string_as_ph(json_string)

  # Now verify we can go back and forth:

  json_string = ph_as_json_string(ph = ph,
    include_all = True)

  new_ph = json_string_as_ph(json_string)
  assert ph.as_pdb_string().strip() == new_ph.as_pdb_string().strip()

if __name__ == "__main__":

  t0 = time.time()
  tst_01()

  print("Time: %6.4f"%(time.time()-t0))
  print("OK")

