from __future__ import division, print_function
import sys, os, time

import libtbx.load_env
from libtbx.test_utils import approx_equal

from phenix.model_building.morph_info import morph_info \
    as morph_info
from phenix.model_building.morph_info import \
    get_lsq_fit_from_model_pair, unit_lsq_fit

data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="model_building",
  test=os.path.isdir)

model_file=os.path.join(data_dir,"2DY1_short.pdb")

def tst_01(log = sys.stdout):

 
  working_directory = "replace_with_fragments_from_pdb_01"
  if os.path.isdir(working_directory):
    print ("please remove %s before running this test" %(working_directory))
    assert not os.path.isdir(working_directory)
  else:
    os.mkdir(working_directory)
  os.chdir(working_directory)
  print ("Working in %s" %(os.getcwd()))

  # Make a data file

  try:
    from iotbx.cli_parser import run_program
    from phenix.programs import replace_with_fragments_from_pdb as run
  except Exception as e:
    print("replace_with_fragments_from_pdb not available...skipping")
    return

  args = "nproc=1 %s quick=True refine_cycles=0 " %(model_file) 
  args = args.split()

  rebuild_model = run_program(program_class=run.Program,args=args)
  
if __name__ == "__main__":

  t0 = time.time()
  tst_01()

  print("Time: %6.4f"%(time.time()-t0))
  print("OK")

