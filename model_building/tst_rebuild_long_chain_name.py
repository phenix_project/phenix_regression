from __future__ import division
from __future__ import print_function
import os , time
import libtbx.load_env
from libtbx.test_utils import approx_equal

from iotbx.data_manager import DataManager
from phenix.model_building import local_model_building


data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="model_building",
  test=os.path.isdir)

model_file=os.path.join(data_dir,"box_mtm.pdb")
target_file = os.path.join(data_dir,"box.pdb")
map_file = os.path.join(data_dir,"box.ccp4")

def tst_01(rebuild_methods = None, dummy_run = None,
   test_id = 0,
   expected_cc = None):

  print("Testing rebuilding")

  working_directory = "segment_replacement_%s" %(test_id)
  if not os.path.isdir(working_directory):
    os.mkdir(working_directory)
  os.chdir(working_directory)
  print ("Working in %s" %(os.getcwd()))


  # Make a data file
  from iotbx.data_manager import DataManager
  dm = DataManager()
  mmm=dm.get_map_model_manager(model_file=model_file,map_files=map_file)
  target_model=dm.get_model(target_file)
  mmm.add_model_by_id(model=target_model,model_id='target_model')

  hierarchy = mmm.model().get_hierarchy()
  for m in hierarchy.models():
      for chain in m.chains():
        chain.id = "ABCDEF"
  model = mmm.model()
  print(model.model_as_pdb())
  print(model.model_as_mmcif())

  mmm.set_resolution(3)
  mmm.set_experiment_type('cryo_em')

  sequence=mmm.model().as_sequence(as_string = True)


  # Ready with small map and model that is inside the map region

  # Set up local model building
  mmm.write_model('model.pdb')
  mmm.write_map('map.ccp4')
  build=local_model_building(
     map_model_manager=mmm, # map_model manager
     nproc= 1,
    )

  # set any defaults
  build.set_defaults(
     thoroughness='quick',
     debug=True,
     )

  # run rebuild with sequence
  build.rebuild(
    segment_length = 15,
    segment_length_variability = 0,
    sequence = None,
    selection_string = None,
    refine_cycles = 0,                       # Refinement cycles for a segment
    macro_cycles = 1,
    base_methods = rebuild_methods,
    dummy_run = dummy_run,
     )
  # insert the loop
  mam=build.as_map_model_manager()
  mam.write_model('rebuilt_model.pdb')
  mam.write_map('rebuilt_model.ccp4')

  cc=mam.map_model_cc(resolution=3)
  assert approx_equal(cc,expected_cc,eps=0.05)


if __name__=="__main__":

  t0 = time.time()
  tst_01(rebuild_methods = ['refine'],
    dummy_run = False,
    test_id = 1,
    expected_cc = 0.48)

  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
