from __future__ import division, print_function
import sys, os, time

import libtbx.load_env
from libtbx.test_utils import approx_equal
from iotbx.data_manager import DataManager

from phenix.model_building.fragment_search import get_ca_sites_from_hierarchy
from phenix.model_building.fragment_search import crystal_symmetry_from_sites
from phenix.model_building.fragment_search import model_from_sites
from phenix.model_building.fragment_search import get_rmsd_with_indel
from phenix.model_building.fragment_search import superpose_sites_with_indel
from phenix.model_building.fragment_search import morph_with_segment_matching

data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="model_building",
  test=os.path.isdir)

model_file=os.path.join(data_dir,"box_mtm.pdb")
target_file = os.path.join(data_dir,"box.pdb")

def tst_01():
  # Run using model_building object

  dm = DataManager()
  m = dm.get_model(model_file)
  m2 = dm.get_model(target_file)

  build = m.as_map_model_manager().model_building()

  # Test get_ca_sites
  m_ca = build.get_ca_sites(m)
  m2_ca = build.get_ca_sites(m2)

  # Test get_rmsd_with_indel
  rmsd = build.get_rmsd_with_indel(m_ca, m2_ca).rmsd_value
  assert approx_equal(rmsd, 1.59, eps= 0.01)

  # Test superpose_sites_with_indel
  superposed_sc = build.superpose_sites_with_indel(
        sites_cart = m_ca,
        target_sites = m2_ca,
        sites_to_apply_superposition_to = m_ca)

  # Test get_rmsd_with_indel
  rmsd = build.get_rmsd_with_indel(m2_ca, superposed_sc).rmsd_value
  assert approx_equal(rmsd, 1.28, eps= 0.01)

  # Test morph_with_segment_matching
  replacement_model = build.morph_to_match_model(
    fixed_model = m2,
    moving_model = m,
    )

  new_segment = replacement_model
  original_segment = m2

  rmsd = get_rmsd_with_indel(get_ca_sites_from_hierarchy
   (new_segment.get_hierarchy()) , superposed_sc).rmsd_value

  assert approx_equal(rmsd, 1.23, eps= 0.01)


def tst_02():
  dm = DataManager()
  m = dm.get_model(model_file)
  m2 = dm.get_model(target_file)


  ph = m.get_hierarchy()
  # Test get_ca_sites_from_hierarchy
  sc = get_ca_sites_from_hierarchy(ph)

  # Test crystal_symmetry_from_sites
  cs = crystal_symmetry_from_sites(sc)
  new_m = model_from_sites(sc)

  # Test superpose_sites_with_indel
  other_sc = get_ca_sites_from_hierarchy(m2.get_hierarchy())
  superposed_sc = superpose_sites_with_indel(
        sites_cart = sc,
        target_sites = other_sc,
        sites_to_apply_superposition_to = sc)

  # Test get_rmsd_with_indel
  rmsd = get_rmsd_with_indel(other_sc, superposed_sc).rmsd_value

  # Test morph_with_segment_matching
  replacement_info = morph_with_segment_matching(
    model = m,
    match_model = m2,
    return_replacement_fragments_only = True,
    )

  assert len(replacement_info.replacement_info_list) == 1
  replacement = replacement_info.replacement_info_list[0]
  new_segment = replacement.replacement_segment
  original_segment = m2

  rmsd = get_rmsd_with_indel(get_ca_sites_from_hierarchy
   (new_segment.get_hierarchy()) , superposed_sc).rmsd_value

  assert approx_equal(rmsd, 1.23, eps= 0.01)


if __name__ == "__main__":

  t0 = time.time()
  tst_01()
  tst_02()

  print("Time: %6.4f"%(time.time()-t0))
  print("OK")

