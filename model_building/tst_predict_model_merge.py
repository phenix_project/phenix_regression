from __future__ import division, print_function
import sys, os, time

import libtbx.load_env
from libtbx.test_utils import approx_equal
from iotbx.data_manager import DataManager


data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="model_building",
  test=os.path.isdir)

seq_file=os.path.join(data_dir,"7mjs_23883_H.fa")

def tst_01(log = sys.stdout, max_time = 60):

  command = """phenix.predict_and_build stop_after_predict=True seq_file=%s predict_and_build.overlap_size=51 overlap_match_size=50 predict_and_build.chunk_size=100 predict_and_build.break_into_chunks_if_length_is=103""" %(seq_file)

  import time
  from libtbx import easy_run
  t0 = time.time()
  result = easy_run.fully_buffered_subprocess(command, timeout = max_time)
  t1 = time.time()
  dt = t1 - t0
  if dt >= max_time:  # timed out
    print("Command timed out...not running test")
    return

  if "\n".join(result.stdout_lines).find("Wrote model with 132 residues to:") > -1:
    print("OK")
  else:
    print ("ERRORS:", "\n".join(result.stderr_lines))
    print ("RESULT:", "\n".join(result.stdout_lines))
    print("FAILED")
    assert 0,"Failed to find expected output"
  
if __name__ == "__main__":

  t0 = time.time()
  tst_01()

  print("Time: %6.4f"%(time.time()-t0))
  print("OK")

