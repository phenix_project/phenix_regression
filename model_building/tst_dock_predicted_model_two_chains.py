from __future__ import division
from __future__ import print_function
import sys,os
import libtbx.load_env
from libtbx.test_utils import approx_equal
import random
from scitbx.array_family import flex
from iotbx.map_model_manager import map_model_manager as MapModelManager
from iotbx.data_manager import DataManager
from phenix.model_building import local_model_building

import time,sys

data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="model_building",
  test=os.path.isdir)

map_file=os.path.join(data_dir,"model_A30_B30.ccp4")
model_file=os.path.join(data_dir,"A30_90.pdb")
model_file_2=os.path.join(data_dir,"A30_90b.pdb")

def tst_01(map_file, model_file):

  print(
    "Testing dock_predicted_model with small map and dummy alphafold model.")

  # Set up a data_manager to read and write files
  dm=DataManager()

  try:
    from iotbx.cli_parser import run_program
    from phenix.programs import dock_predicted_model as run
  except Exception as e:
    print("dock_predicted_model not available...skipping")
    return

  args = "predicted_model=%s predicted_model=%s full_map=%s crystal_info.resolution=3 rigid_body_refinement=true rigid_body_refine_cycles=1 build.refine_cycles=0 search.refine_cycles=0" %(model_file, model_file_2, map_file)
  args = args.split()
  print("phenix.dock_predicted_model %s" %(" ".join(args)))
  dr = run_program(program_class=run.Program,args=args)
  assert dr.rebuilt_model.overall_counts().n_residues == 14

if __name__=="__main__":

  t0 = time.time()
  tst_01(map_file, model_file)

  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
