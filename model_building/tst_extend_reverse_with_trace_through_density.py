from __future__ import division, print_function

from libtbx import group_args
import sys, time, os
from scitbx.array_family import flex
from scitbx.matrix import col
import libtbx.load_env
data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="model_building",
  test=os.path.isdir)

map_file=os.path.join(data_dir,"c_term_box.ccp4")
model_file = os.path.join(data_dir,"c_term_stub.pdb")


def tst_01():

  # get a target model and an insertion model
  from iotbx.data_manager import DataManager
  dm = DataManager()
  dm.set_overwrite(True)

  mmm = dm.get_map_model_manager(map_files=map_file,
     model_file=model_file,
     )

  mmm.set_log(sys.stdout)
  mmm_sav = mmm.deep_copy()
  sequence="""CLWDLQNKAERQNDILV"""
  build = mmm.model_building()
  build.set_defaults(sequence=sequence, debug=True)

  extended = build.extend_reverse(extend_methods=['trace_through_density'],
    refine_cycles = 0, target_number_of_residues_to_build=16)

  
  sequence_found = extended.as_sequence(as_string = True)
  assert sequence_found.find("CLWDLQNKAERQNDILV") > -1
  print(extended.model_as_pdb())

if __name__ == "__main__":

  t0 = time.time()
  tst_01()

  print("Time: %6.4f"%(time.time()-t0))
  print("OK")

