from __future__ import division
from __future__ import print_function
import os
import libtbx.load_env
from libtbx.test_utils import approx_equal


from iotbx.map_model_manager import map_model_manager as MapModelManager
from iotbx.data_manager import DataManager

import time

data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="model_building",
  test=os.path.isdir)

map_file=os.path.join(data_dir,"boxed_ligand_map.ccp4")
model_file=os.path.join(data_dir,"boxed_ligand_map.pdb")

def tst_01(map_file, model_file, ):

  print("Testing ligand_fitting with small map containing atp.")

  # Read in the map and model
  dm=DataManager()
  map_manager=dm.get_real_map(map_file)
  model=dm.get_model(model_file)

  # make a map-model manager and remove the model outside the map
  mmm=MapModelManager(map_manager=map_manager, model=model)

  mmm.set_resolution(3)
  mmm.set_experiment_type('cryo_em')

  mmm.remove_model_outside_map(boundary=1.5)

  # Ready with small map and model that is inside the map region and a ligand to fit

  # Set up local model building
  from phenix.model_building import local_model_building
  build=local_model_building(
     map_model_manager=mmm, # map_model manager


     nproc=1,   # more processors mean more tries also
    )

  # set any defaults
  build.set_defaults(
     scattering_table='electron',  # for a cryo-em map
     thoroughness='quick',  # quick/medium/thorough/extra_thorough
     debug = True,
     )

  # run ligand fitting (NOTE: you can run again and get a different answer each time)
  fitted_ligand_model=build.fit_ligand(
     ligand_code = 'ATP',
     good_enough_score=0.75,           # stop looking if this is achieved
     refine_cycles = 1,
    )

  # write out the ligand
  dm.write_model_file(fitted_ligand_model,'fitted_ligand.pdb',overwrite=True)

  dm=DataManager()
  read_in_fitted_ligand=dm.get_model('fitted_ligand.cif')
  read_in_map_manager=dm.get_real_map(map_file)

  mam=MapModelManager(model=read_in_fitted_ligand,map_manager=read_in_map_manager)
  cc=mam.map_model_cc(resolution=3)
  assert approx_equal(cc,0.85,eps=0.25)

if __name__=="__main__":

  t0 = time.time()
  tst_01(map_file, model_file)

  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
