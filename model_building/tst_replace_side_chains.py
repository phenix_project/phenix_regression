from __future__ import division
from __future__ import print_function
import os
import libtbx.load_env
from libtbx.test_utils import approx_equal


from iotbx.map_model_manager import map_model_manager as MapModelManager
from iotbx.data_manager import DataManager
from phenix.model_building import local_model_building

import time

data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="model_building",
  test=os.path.isdir)

model_file=os.path.join(data_dir,"helix_1.pdb")
map_file=os.path.join(data_dir,"helix_1.ccp4")
seq_file=os.path.join(data_dir,"seq.dat")

def tst_01(model_file, map_file,  seq_file):

  print("Testing chain building with replace_side_chains and small map .")

  # Set up a data_manager to read and write files
  dm=DataManager()

  # Read in a cut_out map and build into the map

  # Read in the map and model
  map_manager=dm.get_real_map(map_file)
  model=dm.get_model(model_file)

  # Get  sequence of what is there
  sequence=model.as_sequence(as_string = True)

  # make a map-model manager, but skip the model this time
  mmm=MapModelManager(map_manager=map_manager, model=model)

  mmm.set_resolution(3)
  mmm.set_experiment_type('cryo_em')

  # Box and mask
  mmm.box_all_maps_around_model_and_shift_origin()
  mmm.mask_all_maps_around_atoms()

  # Ready with small map and no model

  # Set up local model building
  build=local_model_building(
     map_model_manager=mmm, # map_model manager
     nproc= 1,
     normalize=True,
    )

  # set any defaults
  build.set_defaults(
     scattering_table='electron',  # for a cryo-em map
     thoroughness='quick',
     debug=True,
     build_methods=['replace_side_chains'],
     random_seed = 785877,
     )

  # run
  build_model=build.build(
     refine_cycles=0,
     sequence=sequence,
     chain_type='PROTEIN',
     return_model = True,
    )

  cc=mmm.map_model_cc(model = build_model, resolution=3)

  assert approx_equal(cc,.8,eps=0.2)


if __name__=="__main__":

  t0 = time.time()
  tst_01(model_file, map_file,  seq_file)

  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
