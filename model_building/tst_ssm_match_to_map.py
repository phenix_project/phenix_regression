from __future__ import division
from __future__ import print_function
import sys,os
import libtbx.load_env
from libtbx.test_utils import approx_equal


from iotbx.map_model_manager import map_model_manager as MapModelManager

from phenix.model_building import local_model_building

import time,sys

data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="model_building",
  test=os.path.isdir)

model_file=os.path.join(data_dir,"structure_search_target.pdb")

def tst_01( model_file, ):

  print("Testing segment replacement with structure_search.")

  working_directory = "ssm_match_to_map_1"
  if not os.path.isdir(working_directory):
    os.mkdir(working_directory)
  os.chdir(working_directory)
  print ("Working in %s" %(os.getcwd()))


  # Make a data file
  mmm=MapModelManager()
  mmm.generate_map(file_name=model_file, d_min=3)
  mmm.set_resolution(3)
  mmm.set_experiment_type('cryo_em')


  # Ready with small map and model that is inside the map region

  # Set up local model building
  build=local_model_building(
     map_model_manager=mmm, # map_model manager
     nproc= 1,
    )
  model = build.model().deep_copy()

  # set any defaults
  build.set_defaults(
     thoroughness='quick',
     debug=True,
     )
  build.set_log(sys.stdout)
  docked_model = build.ssm_match_to_map(model = model)

  if docked_model:
    print("Docked model found")
  else:
    print("No docked model found")
  result = build.get_results()[0]
  f = open('ss_model.pdb', 'w')
  print(result.fixed_model.model_as_pdb(), file = f)
  f.close()
  f = open('moving_model.pdb', 'w')
  print(result.moving_model.model_as_pdb(), file = f)
  f.close()

  if docked_model:
    f = open('docked_model.pdb', 'w')
    print(result.superposed_model.model_as_pdb(), file = f)
    f.close()
    mam=build.as_map_model_manager()
    cc=mam.map_model_cc(model = docked_model, resolution=3)
    assert approx_equal(cc,0.5,eps=0.15)

  assert docked_model


if __name__=="__main__":

  t0 = time.time()
  tst_01( model_file, )

  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
