from __future__ import division, print_function
import sys, os, time

import libtbx.load_env
from libtbx.test_utils import approx_equal
from iotbx.data_manager import DataManager


data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="model_building",
  test=os.path.isdir)

keep_file=os.path.join(data_dir,"keep.pdb")
insert_file=os.path.join(data_dir,"insert.pdb")

def tst_01(log = sys.stdout):


  try:
    from iotbx.cli_parser import run_program
    from phenix.programs import trim_overlapping as run
  except Exception as e:
    print("fragment_search not available...skipping")
    return

  args = "model=%s model_to_avoid=%s" %(insert_file, keep_file)
  args = args.split()

  model_list_info = run_program(program_class=run.Program,args=args)
  n =  model_list_info.trimmed_model.get_hierarchy().overall_counts().n_residues
  print("Residues found:", n)
  assert n == 49

if __name__ == "__main__":

  t0 = time.time()
  tst_01()

  print("Time: %6.4f"%(time.time()-t0))
  print("OK")

