from __future__ import division
from __future__ import print_function
import sys,os
import libtbx.load_env
import time

if __name__=="__main__":
  '''
    Quick test of all methods in rebuild with dummy run
  '''
  from phenix_regression.model_building.tst_rebuild import tst_01
  t0 = time.time()
  tst_01(rebuild_methods = ['regularize_model'],
    dummy_run = True,
    test_id = 2,
    expected_cc = 0.48)

  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
