from __future__ import division
from __future__ import print_function
import os
import libtbx.load_env




from iotbx.data_manager import DataManager


import time

data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="model_building",
  test=os.path.isdir)

map_file=os.path.join(data_dir,"dummy_af.ccp4")
model_file=os.path.join(data_dir,"dummy_af.pdb")
processed_model_file=os.path.join(data_dir,"dummy_af_processed.pdb")

def tst_01(map_file, model_file):

  print(
    "Testing dock_predicted_model with small map and dummy alphafold model.")

  # Set up a data_manager to read and write files
  dm=DataManager()

  try:
    from iotbx.cli_parser import run_program
    from phenix.programs import dock_predicted_model as run
  except Exception as e:
    print("dock_predicted_model not available...skipping")
    return

  args = 'rebuild_strategy=Quick control.nproc=1 processed_model_file=%s model=%s full_map=%s crystal_info.resolution=3 rigid_body_refinement=False rigid_body_refine_cycles=0 build.refine_cycles=0 search.refine_cycles=0' %(processed_model_file, model_file, map_file)
  args = args.split()

  dr = run_program(program_class=run.Program,args=args)



if __name__=="__main__":

  t0 = time.time()
  tst_01(map_file, model_file)

  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
