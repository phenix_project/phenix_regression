from __future__ import division
from __future__ import print_function
import sys,os
import libtbx.load_env
from libtbx.test_utils import approx_equal
import random
from scitbx.array_family import flex
from iotbx.map_model_manager import map_model_manager as MapModelManager
from iotbx.data_manager import DataManager
import mmtbx.utils

import time,sys

data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="model_building",
  test=os.path.isdir)

map_file=os.path.join(data_dir,"iterative_morph.ccp4")
model_file=os.path.join(data_dir,"iterative_morph.pdb")

def tst_01(map_file, model_file,):

  print("Testing iterative morphing with small map .")

  # Set up a data_manager to read and write files
  dm=DataManager()

  # Read in a cut_out map and build into the map

  # Read in the map and model
  map_manager=dm.get_real_map(map_file)
  model=dm.get_model(model_file)


  # make a map-model manager

  mmm=MapModelManager(map_manager=map_manager, model=model)

  mmm.set_resolution(3)
  mmm.set_experiment_type('cryo_em')
  starting_cc = mmm.map_model_cc(model = model)

  # Set up model building
  build=mmm.model_building(
     nproc= 1,
    )
  morphed_model = build.morph(iterative_morph = True,)

  cc = mmm.map_model_cc(model = morphed_model)
  print("CC value %.2f (starting: %.2f)" %( cc, starting_cc))
  assert cc >= 0.36
  assert cc > starting_cc

if __name__=="__main__":
  t0 = time.time()
  tst_01(map_file, model_file, )
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")

