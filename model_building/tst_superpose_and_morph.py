from __future__ import division, print_function
import sys, os, time

import libtbx.load_env
from libtbx.test_utils import approx_equal
from iotbx.data_manager import DataManager

from phenix.model_building.morph_info import morph_info \
    as morph_info
from phenix.model_building.morph_info import \
    get_lsq_fit_from_model_pair, unit_lsq_fit

data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="model_building",
  test=os.path.isdir)

model_file=os.path.join(data_dir,"ssm_model.pdb")
other_model_file=os.path.join(data_dir,"ssm_other_model.pdb")
superposed_model_file=os.path.join(data_dir,"superposed_model.pdb")

morph_fixed_file = os.path.join(data_dir,"morph_fixed.pdb")
morph_moving_file = os.path.join(data_dir,"morph_moving.pdb")
morph_moving_offset_file = os.path.join(data_dir,"morph_moving_offset.pdb")

def tst_01(log = sys.stdout):

 
  working_directory = "superpose_and_morph_01"
  if os.path.isdir(working_directory):
    print ("please remove %s before running this test" %(working_directory))
    assert not os.path.isdir(working_directory)
  os.mkdir(working_directory)
  os.chdir(working_directory)
  print ("Working in %s" %(os.getcwd()))

  try:
    from iotbx.cli_parser import run_program
    from phenix.programs import superpose_and_morph as run
  except Exception as e:
    print("superpose_and_morph not available...skipping")
    return

  args = "fixed_model=%s moving_model=%s" %(
     model_file,other_model_file)
  args = args.split()

  superposed_model_info = run_program(program_class=run.Program,args=args)
  superposed_model = superposed_model_info.superposed_model

  rmsd_info = superposed_model.info().rmsd_info
  print("RMSD, N",rmsd_info.rmsd,rmsd_info.rms_n)
  assert approx_equal(rmsd_info.rmsd,1.7,eps = 0.2)
  assert approx_equal(rmsd_info.rms_n,82,eps = 10)
 

def tst_02():
  from iotbx.data_manager import DataManager
  dm = DataManager()
  morph_fixed = dm.get_model(morph_fixed_file)
  morph_moving = dm.get_model(morph_moving_file)
  morph_moving_offset = dm.get_model(morph_moving_offset_file)
  build = morph_fixed.as_map_model_manager().model_building()
  m = build.superpose_and_morph(
    fixed_model = morph_fixed,
    moving_model = morph_moving,)
  info = m.info().rmsd_info
  assert approx_equal(info.rmsd, 0.23, eps = 0.05)
  assert approx_equal(info.all_n, 233, 5)

  m = build.superpose_and_morph(
    fixed_model = morph_fixed,
    moving_model = morph_moving_offset,)
  info = m.info().rmsd_info
  assert approx_equal(info.rmsd, 0.23, eps = 0.05)
  assert approx_equal(info.all_n, 233, 5)



   
if __name__ == "__main__":

  t0 = time.time()
  tst_01()
  tst_02()

  print("Time: %6.4f"%(time.time()-t0))
  print("OK")

