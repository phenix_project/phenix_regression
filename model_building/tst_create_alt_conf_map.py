from __future__ import division
from __future__ import print_function
import sys,os
import libtbx.load_env
from libtbx.test_utils import approx_equal
import random
from scitbx.array_family import flex
from iotbx.map_model_manager import map_model_manager as MapModelManager
from iotbx.data_manager import DataManager
from phenix.model_building import local_model_building

import time,sys

data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="model_building",
  test=os.path.isdir)

def tst_01():

  print("Testing create_alt_conf with one-residue model")

  # Set up a data_manager to read and write files
  dm=DataManager()

  # Read in a cut_out map and a model and refit a loop

  try:
    from iotbx.cli_parser import run_program
    from phenix.programs import create_alt_conf as run
  except Exception as e:
    print("create_alt_conf not available...skipping")
    return

  dd = 'tst_create_alt_conf_map'
  if not os.path.isdir(dd):
    os.mkdir(dd)
  os.chdir(dd)
  model_file = os.path.join(data_dir,'short_alt_conf_map.pdb')
  data_file = os.path.join(data_dir,'short_alt_conf_map.ccp4')

  args = ['model=%s' %(model_file),
          'full_map=%s' %(data_file),
          'chain_type=PROTEIN',
          'max_rotamers_to_consider=2',
          'water_and_hydrogens=never', 
          'rsr_macro_cycles=1',
          'cycles=1',
          'macro_cycles=1',
          'resolution=2',
          'min_rotamer_rmsd=.05' ]

  print("phenix.create_alt_conf %s" %(" ".join(args)))

  results = run_program(program_class=run.Program,args=args)
  model = results.multi_conformer_model
  assert model.get_hierarchy().altlocs_present() == ['A','B']

if __name__=="__main__":

  t0 = time.time()
  tst_01()

  print("Time: %6.4f"%(time.time()-t0))
  print("OK")

