from __future__ import division
from __future__ import print_function
import sys,os
import libtbx.load_env
from libtbx.test_utils import approx_equal
import random
from scitbx.array_family import flex
from iotbx.map_model_manager import map_model_manager as MapModelManager
from iotbx.data_manager import DataManager
from phenix.model_building import local_model_building

import time,sys

data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="model_building",
  test=os.path.isdir)

def extract_files():
  import tarfile
  f = tarfile.open(os.path.join(data_dir,'7n8i_24237_L_predict_and_build_as_xray.tgz'))
  f.extractall('.')
  f.close()

def tst_01():

  print("Testing predict_and_build (xray-data) with pre-run files")

  # Set up a data_manager to read and write files
  dm=DataManager()

  # Read in a cut_out map and a model and refit a loop

  try:
    from iotbx.cli_parser import run_program
    from phenix.programs import predict_and_build as run
  except Exception as e:
    print("predict_and_build not available...skipping")
    return

  if sys.version_info.major != 3:
   print("predict_and_build requires version 3 of python")
   return

  dd = 'tst_predict_and_build_as_xray'
  if not os.path.isdir(dd):
    os.mkdir(dd)
  os.chdir(dd)
  extract_files()
  os.chdir('7n8i_24237_xray')


  args = "get_msa_with_mmseqs2=false carry_on=True include_templates_from_pdb=False seq_file=7n8i_24237.seq jobname=7n8i_24237_xray xray_data_file=7n8i_24237_L_box_fobs.mtz crystal_info.resolution=3 cycles=2 include_templates_from_pdb=False carry_on_directory=7n8i_24237_xray include_side_in_templates=False"

  args = args.split()
  print("phenix.predict_and_build %s" %(" ".join(args)))

  dr = run_program(program_class=run.Program,args=args)

if __name__=="__main__":

  t0 = time.time()
  tst_01()

  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
