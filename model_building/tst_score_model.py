from __future__ import division
from __future__ import print_function
import sys,os,time
import libtbx.load_env
from libtbx.test_utils import approx_equal
from iotbx.map_model_manager import map_model_manager as MapModelManager
from iotbx.data_manager import DataManager
from phenix.model_building import local_model_building


data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="model_building",
  test=os.path.isdir)

model_file=os.path.join(data_dir,"box_mtm.pdb")
target_file = os.path.join(data_dir,"box.pdb")
map_file = os.path.join(data_dir,"box.ccp4")

def tst_01( test_id = 0):

  print("Testing score_model") 

  working_directory = "score_model_%s" %(test_id)
  if os.path.isdir(working_directory):
    print ("please remove %s before running this test" %(working_directory))
    assert not os.path.isdir(working_directory)
  os.mkdir(working_directory)
  os.chdir(working_directory)
  print ("Working in %s" %(os.getcwd()))


  # Make a data file
  from iotbx.data_manager import DataManager
  dm = DataManager()
  mmm=dm.get_map_model_manager(model_file=model_file,map_files=map_file)
  target_model=dm.get_model(target_file)
  mmm.add_model_by_id(model=target_model,model_id='target_model')

  mmm.set_resolution(3)
  mmm.set_experiment_type('cryo_em')

  
  sequence=mmm.model().as_sequence(as_string = True)


  # Ready with small map and model that is inside the map region

  # Set up local model building
  build=local_model_building(
     map_model_manager=mmm, # map_model manager
     nproc= 1,
    )

  # set any defaults
  build.set_defaults(
     thoroughness='quick',
     debug=True,
     )
  
  # run score_model with sequence
  score_info = build.score_model(
   sequence = build.model().as_sequence(as_string = True))

  assert approx_equal(score_info.score,9.5, eps=5)


if __name__=="__main__":

  t0 = time.time()
  tst_01( test_id = 1)

  print("Time: %6.4f"%(time.time()-t0))
  print("OK")
