from __future__ import division
from __future__ import print_function
import sys,os
import libtbx.load_env
from libtbx.test_utils import approx_equal
import random
from scitbx.array_family import flex
from iotbx.map_model_manager import map_model_manager as MapModelManager
from iotbx.data_manager import DataManager
import mmtbx.utils

import time,sys

data_dir = libtbx.env.under_dist(
  module_name="phenix_regression",
  path="model_building",
  test=os.path.isdir)

map_file=os.path.join(data_dir,"boxed_ligand_map.ccp4")
model_file=os.path.join(data_dir,"protein_ligand.pdb")

def tst_01(map_file, model_file,):

  print("Testing ligand + protein morphing with small map .")

  # Set up a data_manager to read and write files
  dm=DataManager()

  # Read in a cut_out map and build into the map

  # Read in the map and model
  map_manager=dm.get_real_map(map_file)
  model=dm.get_model(model_file)


  # make a map-model manager

  mmm=MapModelManager(map_manager=map_manager, model=model)

  mmm.set_resolution(3)
  mmm.set_experiment_type('cryo_em')

  starting_coords_chain_A = model.select(
      model.selection("chain A")).get_sites_cart()
  starting_coords_chain_L = model.select(
      model.selection("chain L")).get_sites_cart()

  # Set up model building
  build=mmm.model_building(
     nproc= 1,
    )

  build.set_defaults(debug=True)


  build.morph(default_selection_method='by_chain',
    skip_hetero=False)

  final_coords_chain_A = model.select(
      model.selection("chain A")).get_sites_cart()
  final_coords_chain_L = model.select(
      model.selection("chain L")).get_sites_cart()

  rms_A = final_coords_chain_A.rms_difference(starting_coords_chain_A)
  rms_L = final_coords_chain_L.rms_difference(starting_coords_chain_L)
  assert approx_equal( (rms_A, rms_L), (0.533037531744,0.126849279651), eps=0.2)



if __name__=="__main__":
  t0 = time.time()
  tst_01(map_file, model_file, )
  print("Time: %6.4f"%(time.time()-t0))
  print("OK")

