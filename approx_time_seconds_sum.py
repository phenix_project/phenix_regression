from __future__ import print_function
import fileinput

def run():
  pattern = "# APPROX TIME SECONDS".split()
  sum_seconds = 0
  for line in fileinput.input():
    flds = line.split()
    if (len(flds) != 5): continue
    if (flds[:4] == pattern):
      print(line.rstrip())
      seconds = float(flds[-1])
      sum_seconds += seconds
  print("sum seconds: %.6g" % sum_seconds)

if (__name__ == "__main__"):
  run()
