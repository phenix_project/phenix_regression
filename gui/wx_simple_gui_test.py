from __future__ import print_function

import sys, os
from phenix import validation
import libtbx.load_env
from libtbx.test_utils import approx_equal
import cPickle

def exercise_molprobity_plots () :
  from wxGUI2 import App
  from wxGUI2.Programs.Validation import MolprobityPlots
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/2hr0.pdb", test=os.path.isfile)
  open("/var/tmp/2hr0.pdb", "w").write(open(pdb_file).read())
  result = validation.molprobity_validation(pdb_file="/var/tmp/2hr0.pdb",
                                            coot_dir="/var/tmp",
                                            out_dir="/var/tmp",
                                            quiet=False)
  result.run()
  app = App.PhenixApp(["--auto_run"])
  frame1 = MolprobityPlots.RotamerFrame(None, "Chi1-Chi2 plot for 2hr0",
    result)
  frame1.Show()
  frame2 = MolprobityPlots.RamachandranFrame(None, "Ramachandran plot for 2hr0",
    result)
  frame2.Show()
  frame1.Close()
  #frame2.Close()
  app.MainLoop()

def exercise_reflection_file_info () :
  import wx
  from wxGUI2 import Reflections
  file_name = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/wizards/map_coeffs.mtz",
    test=os.path.isfile)
  a = wx.App(0)
  stats_window = Reflections.ReflectionFileStats(file_name, None)
  stats_window.Show()
  stats_window.Close()
#  a.Exit()

def run () :
  exercise_reflection_file_info()
  exercise_molprobity_plots()
  print("OK")

def exercise () : # placeholder for run_tests.py
  print("No non-GUI tests here.")

if __name__ == "__main__" :
  run()
