from __future__ import print_function

# TODO: more tests!

from phenix.refinement import runtime
import libtbx.load_env
from libtbx import thread_utils, easy_run, easy_pickle, runtime_utils
import sys, os

def exercise_callback () :
  class callback_handler (object) :
    def __init__ (self) :
      self._nprint = 0
      self._nstatus = 0
      self._result = 0

    def OnUpdate (self, data) :
      self._nstatus += 1
      if self._nstatus < 2 :
        callback = data
        if type(callback.data).__name__ == "refinement_status" :
          callback.data.write_pdb_file(os.getcwd())
          callback.data.write_map_coeffs(os.getcwd())
          #callback.data.write_xplor_maps(os.getcwd())
      s = easy_pickle.dumps(data)

    def OnPrint (self, data) :
      self._nprint += 1

    def OnComplete (self, data) :
      s = easy_pickle.dumps(data)
      self._result += 1
      self._result_data = data
      assert (data.validation is not None)

    def OnError (self, data, traceback) :
      print(data, traceback)
      assert False

    def check_stats (self) :
      # uncomment asserts once callbacks are fixed
      # assert self._nstatus > 10, self._nstatus
      assert self._result == 1
      # assert self._nprint > 20

  cb = callback_handler()
  write_inputs()
  pdb = "tst_refine_gui_in.pdb"
  mtz = "tst_refine_gui.mtz"
  strategies = ["individual_sites", "occupancies", "individual_adp",
                "rigid_body"]
  config_file = "tst_runtime.eff"
  f = open(config_file, "w")
  f.write("""
refinement.gui.migration.refinement.input.pdb.file_name = tst_refine_gui_in.pdb
refinement.gui.migration.refinement.input.xray_data.file_name = tst_refine_gui.mtz
refinement.gui.migration.refinement.input.xray_data.r_free_flags.file_name = tst_refine_gui.mtz
refinement.pdb_interpretation.restraints_library.cdl=False
refinement.refine.strategy = *individual_sites *occupancies *individual_adp *rigid_body
output.prefix = tst_refine_gui
refinement.main {
  number_of_macro_cycles = 1
  simulated_annealing = True
  target=ls
}
""")
  f.close()
  args = [config_file, "--overwrite"]
  run_phenix_refine = runtime.run_phenix_refine(
    args=args,
    file_name="refine_gui.pkl",
    output_dir=os.getcwd(),
    log_file="refine_gui.log")
  target = runtime_utils.detached_process_driver_mp(run_phenix_refine)
  p = thread_utils.process_with_callbacks(
    target = target,
    callback_stdout = cb.OnPrint,
    callback_final = cb.OnComplete,
    callback_other = cb.OnUpdate,
    callback_err = cb.OnError)
  p.start()
  while p.is_alive() :
    pass
  cb.check_stats()
  (r_work, r_free) = runtime.extract_phenix_refine_r_factors(
    file_name="tst_refine_gui_001.pdb")
  print('r_work',r_work)
  print('r_free',r_free)
  assert r_work.startswith("0.0") and r_free.startswith("0.0")

def write_inputs () :
  pdb_in = """\
CRYST1   24.937    8.866   25.477  90.00 107.08  90.00 P 1 21 1
SCALE1      0.040101  0.000000  0.012321        0.00000
SCALE2      0.000000  0.112790  0.000000        0.00000
SCALE3      0.000000  0.000000  0.041062        0.00000
ATOM      1  N   GLY A   1       8.992   0.474  -6.096  1.00 16.23           N
ATOM      2  CA  GLY A   1       9.033   0.047  -4.707  1.00 16.20           C
ATOM      3  C   GLY A   1       7.998  -1.029  -4.448  1.00 15.91           C
ATOM      4  O   GLY A   1       7.548  -1.689  -5.385  1.00 16.11           O
ATOM      5  N   ASN A   2       7.625  -1.218  -3.185  1.00 15.02           N
ATOM      6  CA  ASN A   2       6.523  -2.113  -2.848  1.00 13.92           C
ATOM      7  C   ASN A   2       5.220  -1.618  -3.428  1.00 12.24           C
ATOM      8  O   ASN A   2       4.955  -0.418  -3.432  1.00 11.42           O
ATOM      9  CB  ASN A   2       6.376  -2.261  -1.340  1.00 14.42           C
ATOM     10  CG  ASN A   2       7.620  -2.786  -0.697  1.00 13.92           C
ATOM     11  OD1 ASN A   2       8.042  -3.915  -0.978  1.00 14.39           O
ATOM     12  ND2 ASN A   2       8.232  -1.975   0.168  1.00 12.78           N
ATOM     13  N   ASN A   3       4.406  -2.553  -3.904  1.00 12.20           N
ATOM     14  CA  ASN A   3       3.164  -2.226  -4.594  1.00 11.81           C
ATOM     15  C   ASN A   3       1.925  -2.790  -3.910  1.00 10.59           C
ATOM     16  O   ASN A   3       1.838  -3.991  -3.653  1.00 10.32           O
ATOM     17  CB  ASN A   3       3.231  -2.727  -6.046  1.00 12.51           C
ATOM     18  CG  ASN A   3       1.973  -2.405  -6.848  1.00 12.59           C
ATOM     19  OD1 ASN A   3       1.662  -1.239  -7.106  1.00 13.64           O
ATOM     20  ND2 ASN A   3       1.260  -3.443  -7.268  1.00 12.39           N
ATOM     21  N   GLN A   4       0.973  -1.913  -3.608  1.00 10.34           N
ATOM     22  CA  GLN A   4      -0.366  -2.335  -3.208  1.00 10.00           C
ATOM     23  C   GLN A   4      -1.402  -1.637  -4.085  1.00 10.21           C
ATOM     24  O   GLN A   4      -1.514  -0.414  -4.070  1.00  8.99           O
ATOM     25  CB  GLN A   4      -0.656  -2.027  -1.736  1.00 10.00           C
ATOM     26  CG  GLN A   4      -1.927  -2.705  -1.229  1.00 10.50           C
ATOM     27  CD  GLN A   4      -2.482  -2.102   0.060  1.00 11.36           C
ATOM     28  OE1 GLN A   4      -2.744  -0.900   0.151  1.00 12.29           O
ATOM     29  NE2 GLN A   4      -2.684  -2.951   1.055  1.00 10.43           N
ATOM     30  N   GLN A   5      -2.154  -2.406  -4.857  1.00 10.48           N
ATOM     31  CA  GLN A   5      -3.247  -1.829  -5.630  1.00 11.24           C
ATOM     32  C   GLN A   5      -4.591  -2.382  -5.178  1.00 11.40           C
ATOM     33  O   GLN A   5      -4.789  -3.599  -5.092  1.00 11.94           O
ATOM     34  CB  GLN A   5      -3.024  -2.023  -7.129  1.00 11.14           C
ATOM     35  CG  GLN A   5      -1.852  -1.222  -7.653  1.00 10.65           C
ATOM     36  CD  GLN A   5      -1.338  -1.748  -8.965  1.00 10.73           C
ATOM     37  OE1 GLN A   5      -0.794  -2.845  -9.028  1.00 10.14           O
ATOM     38  NE2 GLN A   5      -1.511  -0.968 -10.027  1.00 11.31           N
ATOM     39  N   ASN A   6      -5.504  -1.471  -4.872  1.00 11.56           N
ATOM     40  CA  ASN A   6      -6.809  -1.838  -4.359  1.00 12.07           C
ATOM     41  C   ASN A   6      -7.856  -1.407  -5.353  1.00 13.18           C
ATOM     42  O   ASN A   6      -8.257  -0.251  -5.362  1.00 13.64           O
ATOM     43  CB  ASN A   6      -7.053  -1.149  -3.017  1.00 12.12           C
ATOM     44  CG  ASN A   6      -5.966  -1.446  -1.998  1.00 12.31           C
ATOM     45  OD1 ASN A   6      -5.833  -2.579  -1.517  1.00 13.43           O
ATOM     46  ND2 ASN A   6      -5.198  -0.423  -1.645  1.00 11.88           N
ATOM     47  N   TYR A   7      -8.298  -2.332  -6.193  1.00 14.34           N
ATOM     48  CA  TYR A   7      -9.162  -1.980  -7.317  1.00 15.00           C
ATOM     49  C   TYR A   7     -10.603  -1.792  -6.893  1.00 15.64           C
ATOM     50  O   TYR A   7     -11.013  -2.278  -5.838  1.00 15.68           O
ATOM     51  CB  TYR A   7      -9.064  -3.041  -8.412  1.00 15.31           C
ATOM     52  CG  TYR A   7      -7.657  -3.197  -8.931  1.00 15.06           C
ATOM     53  CD1 TYR A   7      -6.785  -4.118  -8.368  1.00 15.24           C
ATOM     54  CD2 TYR A   7      -7.193  -2.400  -9.960  1.00 14.96           C
ATOM     55  CE1 TYR A   7      -5.489  -4.253  -8.830  1.00 14.94           C
ATOM     56  CE2 TYR A   7      -5.905  -2.526 -10.429  1.00 15.13           C
ATOM     57  CZ  TYR A   7      -5.055  -3.451  -9.861  1.00 14.97           C
ATOM     58  OH  TYR A   7      -3.768  -3.572 -10.335  1.00 14.93           O
ATOM     59  OXT TYR A   7     -11.378  -1.149  -7.601  1.00 15.89           O
TER
"""
  with open("tst_refine_gui_in.pdb", "w") as f:
    f.write(pdb_in)
  if os.path.isfile('tst_refine_gui.mtz'):
    os.remove('tst_refine_gui.mtz')
  args = [
    "phenix.fmodel",
    "tst_refine_gui_in.pdb",
    "high_resolution=1.5",
    "r_free_flags_fraction=0.1",
    "type=real",
    "output.label=F",
    "output.file_name=tst_refine_gui.mtz",
    "add_sigmas=True",
    "random_seed=12345",
  ]
  assert (easy_run.fully_buffered(" ".join(args)
    ).raise_if_errors().return_code == 0)

def exercise () :
  exercise_callback()
  print("OK")

if __name__ == "__main__" :
  exercise()
