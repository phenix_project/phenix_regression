#!/bin/bash -f
#### SIMPLE INSTALLATION SCRIPT FOR GUI_TESTS ####


if [[ $# -ne 1 ]];
then
  echo "Use: install_gui_tests.csh <place_to_put_GUI_TESTS>"
  exit 0
fi

p=`which phenix`
if [[ $p == "" ]]; then
 echo "Please source your phenix installation so that phenix is defined."
 exit 0
fi

t=$1
here=`pwd`
echo "Installing GUI_TESTS from $here/GUI_TESTS.tgz in $t"
if [ -d "${t}/GUI_TESTS" ] ; then
  echo "The directory ${t}/GUI_TESTS exists...please remove it first"
  exit 0
fi

cd $t
echo "Working in `pwd`..."
if [ ! -f ${here}/GUI_TESTS.tgz ]; then 
    echo "The ${here}/GUI_TESTS.tgz file is missing..."
    exit 0
fi 
echo "Unpacking ${here}/GUI_TESTS.tgz in `pwd`"
tar xzf ${here}/GUI_TESTS.tgz
ls -tlr GUI_TESTS

cd GUI_TESTS
echo "Unpacking data files in `pwd`"
for dd in $(find . -maxdepth 1 -type d )
do
  if [[ "${dd}" != "./DATA_FILES" ]]; then
    if [[ "${dd}" != "./scripts" ]]; then
      if [[ "${dd}" != "." ]]; then
      cd $dd
      echo "Unpacking in `pwd` $dd"
      ls -tlr ../DATA_FILES/data.tgz
      tar xzf ../DATA_FILES/data.tgz
      cd ..
      fi
    fi
  fi
done

echo "Setting paths..."
phenix.python scripts/edit_paths.py

echo "All done... In Phenix GUI use Import Project to load these projects:"

for dd in $(find . -maxdepth 1 -type d )
do
  if [[ "${dd}" != "./DATA_FILES" ]]; then
    if [[ "${dd}" != "./scripts" ]]; then
      if [[ "${dd}" != "." ]]; then
        echo "`pwd`/${dd}"
      fi
    fi
  fi
done

echo "FINISHED"
exit 0
