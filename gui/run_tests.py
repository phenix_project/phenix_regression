from __future__ import print_function
import sys, os
import wxGUI2
import libtbx.load_env

def run_gui_tests () :
  tst_dir = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/gui", test=os.path.isdir)
  tst_files = os.listdir(tst_dir)
  for file_name in tst_files :
    base, ext = os.path.splitext(file_name)
    if base.startswith("tst_") and ext == ".py" :
      print("%s:" % base, end=' ')
      f = wxGUI2.import_object("phenix_regression.gui.%s" % base, "exercise")
      f()

if __name__ == "__main__" :
  run_gui_tests()
