from __future__ import print_function


def exercise () :
  from phenix.command_line import hyss
  from libtbx.utils import null_out
  import libtbx.load_env
  import os
  import sys
  fn = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/wizards/data/peak.sca",
    test=os.path.isfile)
  f = open("tst_hyss_gui.eff", "w")
  f.write("""\
data = %s
scattering_type = Se
n_sites = 2
space_group = P 1 21 1
unit_cell = 25.000    25.000    25.000    90.000   100.000    90.000
root = tst_hyss_gui_%d
""" % (fn, os.getpid()))
  f.close()
  _stdout = sys.stdout
  sys.stdout = null_out()
  launcher = hyss.launcher(
    args=[os.path.join(os.getcwd(), "tst_hyss_gui.eff")],
    file_name="tst_hyss_gui.log",
    output_dir=os.path.join(os.getcwd(), "tmp_tst_hyss_gui"))
  result = launcher()
  sys.stdout = _stdout
  output_files, stats = hyss.finish_job(result)
  #assert (os.path.basename(result).startswith("tst_hyss_%d" % os.getpid()))
  #assert os.path.isfile(result)
  assert (len(output_files) == 2)
  assert (stats[0][1] == 2)
  assert (result.cc > 0.4)

if (__name__ == "__main__") :
  exercise()
  print("OK")
