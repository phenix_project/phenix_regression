from __future__ import print_function

# XXX VERY IMPORTANT - the eff_files directory that gets created by this
# script exists to exercise backwards compatibility.  Please do not modify
# any of the files involved.

import iotbx.phil
import libtbx.load_env
from libtbx import easy_run
from libtbx.utils import import_python_object
import libtbx.phil.interface
import os

def exercise_any (scope_list, master_phil_path, phil_file=None) :
  if phil_file == 'refine_15.eff':
    from iotbx.data_manager import DataManager
    from phenix.refinement import master_params
    from phenix.programs.phenix_refine import Program
    data_manager_phil = DataManager(Program.datatypes).export_phil_scope()
    required_output_phil = iotbx.phil.parse(Program.output_phil_str)
    master_phil = master_params()
    master_phil.adopt_scope(data_manager_phil)
    master_phil.adopt_scope(required_output_phil)
  else:
    master_phil = import_python_object(
      import_path=master_phil_path,
      error_prefix="",
      target_must_be="",
      where_str="").object
  if isinstance(master_phil, str) :
    master_phil = iotbx.phil.parse(master_phil, process_includes=True)
  elif hasattr(master_phil, "__call__") :
    master_phil = master_phil()
  working_phil = master_phil.fetch()
  index = libtbx.phil.interface.index(
    working_phil=working_phil,
    master_phil=master_phil)
  bad_scopes = index.check_scopes(scope_list)
  if (len(bad_scopes) > 0) :
    raise RuntimeError("""\
One or more named scopes could not be found in the phil parameters specified by:
%s

Missing parameters/scopes:

%s

These parameters are used to render the Phenix GUI; their absence will prevent
use of the program in the GUI.""" % (master_phil_path, "\n".join(bad_scopes)))
  if (phil_file is not None) :
    file_name = os.path.join("eff_files", phil_file)
    file_phil = iotbx.phil.parse(file_name=file_name)
    working_phil = master_phil.fetch(source=file_phil,
      skip_incompatible_objects=True)
    params = working_phil.extract()

# phenix.phase_and_build
def exercise_phase_and_build () :
  scope_list = [
      "input_files.data",
      "input_files.seq_file",
      "input_files.pdb_in",
      "input_files.map_coeffs",
      "directories.base_gui_dir",
      "output_files.job_title",
      "input_files",
      "crystal_info.ncs_copies",
      "crystal_info.resolution",
      "cycles.ncycle",
      "crystal_info.chain_type",
      "cycles.nmodels",
      "crystal_info.solvent_fraction",
      "model_building.quick",
      "refinement.refine",
      "crystal_info.semet",
      "ncs.find_ncs",
      "density_modification",
      "model_building",
      "ncs",
      "refinement",
      "input_files.aniso_corrected_data",
      "input_files.ha_file",
      "input_files.ncs_info_file",
      "input_files.labin",
      "input_files.labin_map_coeffs",
      "input_files.labin_aniso_corrected_data",
      "output_files.mtz_out",
      "output_files.pdb_out",
      "output_files.log",
      "output_files.params_out",
      "directories.base_gui_dir",]
  exercise_any(scope_list,
    "phenix.command_line.phase_and_build.master_params",
    "phase_and_build_10.eff")

# phenix.fit_loops
def exercise_fit_loops () :
  scope_list = [
    "directories.temp_dir",
    "output_files.pdb_out",
    "gui.result_file",
    "input_files.mtz_in",
    "input_files.labin",
    "input_files.pdb_in",
    "input_files.seq_file",
    "gui.job_title",
    "output_files.pdb_out",
    "fitting.chain_id",
    "crystal_info.chain_type",
    "fitting.start",
    "fitting.end",
    "fitting.loop_cc_min",
    "crystal_info.resolution",
    "fitting.min_acceptable_prob",
    "fitting.score_min",
    "fitting.connect_all_segments",
    "fitting.refine_loops",
    "fitting.remove_loops",
    "fitting.loop_lib",
    "input_files.seq_prob_file",
    "fitting.aggressive",
    "control.verbose",
    "control.debug",
    "control.quick",]
  exercise_any(scope_list, "phenix.programs.fit_loops.master_params")

# phenix.find_helices_strands
def exercise_find_helices_strands () :
  scope_list = [
    "input_files.mtz_in",
    "input_files.pdb_in",
    "seq_file",
    "output_files.gui_output_dir",
    "job_title",
    "input_files.map_coeff_labels",
    "trace_chain",
    "crystal_info.resolution",
    "pulchra",
    "crystal_info.chain_type",
    "quick",
    "output_files.output_model",
    "output_files.output_log",
    "temp_dir",
    "raise_sorry",
  ]
  exercise_any(scope_list, "phenix.command_line.find_helices_strands.master_params")

# phenix.ligand_identification
def exercise_ligand_identification () :
  scope_list = [
    "ligand_identification.mtz_in",
    "ligand_identification.input_labels",
    "ligand_identification.mtz_type",
    "ligand_identification.model",
    "ligand_identification.output_dir",
    "ligand_identification.ligand_dir",
    "ligand_identification.ligand_list",
    "ligand_identification.job_title",
    "ligand_identification.high_resolution",
    "ligand_identification.ncpu",
    "ligand_identification.EC",
    "ligand_identification.function",]
  exercise_any(scope_list, "phenix.command_line.ligand_identification.master_params")

# phenix.superpose_pdbs
def exercise_superpose_pdbs () :
  scope_list = [
    "input.pdb_file_name_fixed",
    "input.pdb_file_name_moving",
    "output.file_name",
    "selection_fixed",
    "selection_moving",
    "output.job_title",
    "alignment.alignment_style",
    "alignment.similarity_matrix",
    "alignment.gap_opening_penalty",
    "alignment.gap_extension_penalty",]
  exercise_any(scope_list, "phenix.command_line.superpose_pdbs.master_params")

def exercise_geometry_minimization () :
  scope_list = [
      "file_name",
      "restraints",
      "selection",
      "job_title",
      "directory",
      "output_file_name_prefix",
      "minimization.max_iterations",
      "minimization.macro_cycles",
      "pdb_interpretation",
      "stop_for_unknowns",
      "pdb_interpretation.use_neutron_distances",
      "minimization",
      "fix_rotamer_outliers",
      "minimization.move",]
  exercise_any(scope_list,
    "mmtbx.command_line.geometry_minimization.master_params")

# phenix.xtriage
def exercise_xtriage () :
  scope_list = [
    "scaling.input.xray_data.file_name",
    "scaling.input.xray_data.reference.data.file_name",
    "scaling.input.parameters.reporting.verbose",
    "scaling.input.parameters.reporting.log",
    "scaling.gui.job_title",
    "scaling.input.xray_data.unit_cell",
    "scaling.input.xray_data.space_group",
    "scaling.input.asu_contents",
    "scaling.input.parameters.misc_twin_parameters",]
  exercise_any(scope_list, "mmtbx.scaling.xtriage.master_params")

def exercise_fmodel () :
  scope_list = [
    "output.file_name",
    "output.job_title",
    "high_resolution",
    "low_resolution",
    "scattering_table",
    "output.format",
    "output.label",
    "output.type",
    "r_free_flags_fraction",
    "add_sigmas",
    "anomalous_scatterers"
  ]
  exercise_any(scope_list,
    "mmtbx.programs.fmodel.master_phil")

def exercise_glr () :
  scope_list = [
    "guided_ligand_replacement.superpose_pdbs",
    "guided_ligand_replacement.output.job_title",
    "guided_ligand_replacement.input.guide_pdb_file_name",
    "guided_ligand_replacement.input.protein_pdb_file_name",
    "guided_ligand_replacement.input.ligand_input_file_name",
    "guided_ligand_replacement.input.data_file_name",
    "guided_ligand_replacement.input.map_coeffs_file_name"
  ]
  exercise_any(scope_list,
    "phenix.command_line.guided_ligand_replacement.master_phil")

# phenix.maps
def exercise_maps () :
  scope_list = [
    "maps.input.reflection_data.file_name",
    "maps.input.reflection_data.r_free_flags.file_name",
    "maps.output.directory",
    "maps.output.prefix",
    "maps.input.pdb_file_name",
    "maps.output.job_title",
    "maps.input.reflection_data.labels",
    "maps.input.reflection_data.r_free_flags.label",
    "maps.input.reflection_data.r_free_flags.test_flag_value",
    "maps.input.reflection_data.high_resolution",
    "maps.input.reflection_data.low_resolution",
    "maps.bulk_solvent_correction",
    "maps.anisotropic_scaling",
    "maps.skip_twin_detection",
    "maps.map_coefficients",
    "maps.map",]
  exercise_any(scope_list, "mmtbx.maps.master_params", "maps_21.eff")

def exercise_cut_out_density () :
  scope_list = [
    "control.raise_sorry",
    "output_files.mtz_out",
    "output_files.pdb_out",
    "output_files.params_out",
    "directories.output_dir",
    # "directories.temp_dir",
    "input_files.mtz_in",
    "input_files.pdb_in",
    "cutout_region.atom_selection",
    "output_files.mtz_out",
    "output_files.pdb_out",
    "input_files.map_coeff_labels",
    "cutout_region.cutout_type",
    "cutout_region.high_resolution",
    "cutout_region.padding",
    "cutout_region.cutout_center",
    "cutout_region.cutout_dimensions",
    "cutout_region.cutout_sphere_radius",
    "cutout_region.cutout_model_radius",
    "cutout_region.cutout_subtract_mean",
    "control.verbose",
    "control.debug",
    "control.resolve_command_list",
    "cutout_region.box_scale",
    "input_files.pdb_to_restore_position",
    "output_files.pdb_restored_in_position",
  ]
  exercise_any(scope_list, "phenix.command_line.cut_out_density.master_params")

def exercise_elbow () :
  scope_list = [
    "elbow.input.chemical_string_type",
    "elbow.input.chemical_string",
    "elbow.input.chemical_file_name_type",
    "elbow.input.chemical_file_name",
    "elbow.input.geometry_file_name_usage",
    "elbow.input.geometry_file_name",
    "elbow.input.template_file_name",
    "elbow.defaults.id",
    "elbow.output.output_dir",
    "elbow.output.output",
    "elbow.output.job_title",
    "elbow.input.optimisation_choice",
    "elbow.special.regno"
  ]
  exercise_any(scope_list, "elbow.utilities.phil_utils.master_phil")

# phenix.refine
def exercise_refine () :
  scope_list = [
    "refinement.crystal_symmetry.space_group",
    "refinement.crystal_symmetry.unit_cell",
    "refinement.gui.migration.refinement.input.pdb.file_name",
    "refinement.gui.migration.refinement.input.xray_data.labels",
    "refinement.gui.migration.refinement.input.xray_data.r_free_flags.label",
    "refinement.gui.migration.refinement.input.xray_data.file_name",
    "refinement.gui.migration.refinement.input.xray_data.r_free_flags.file_name",
    "refinement.gui.migration.refinement.input.neutron_data.file_name",
    "refinement.gui.migration.refinement.input.neutron_data.r_free_flags.label",
    "refinement.gui.migration.refinement.input.experimental_phases.file_name",
    "refinement.gui.migration.refinement.input.experimental_phases.labels",
    "refinement.output.job_title",
    "output.prefix",
    "output.serial",
    "output.serial_format",
    "refinement.output.write_maps",
    "refinement.output.write_map_coefficients",
    "refinement.electron_density_maps.map",
    "refinement.refine.strategy",
    "refinement.refine.adp.tls",
    "refinement.refine.adp.group_adp_refinement_mode",
    "refinement.refine.adp.group",
    "refinement.refine.adp.individual.isotropic",
    "refinement.refine.adp.individual.anisotropic",
    "refinement.refine.sites.individual",
    "refinement.refine.sites.rigid_body",
    "refinement.main.max_number_of_resolution_bins",
    "refinement.main.scattering_table",
    "refinement.main.number_of_macro_cycles",
    "refinement.pdb_interpretation.ncs_search.enabled",
    "refinement.main.ordered_solvent",
    "refinement.main.simulated_annealing",
    "refinement.main.simulated_annealing_torsion",
    "refinement.main.nqh_flips",
    "refinement.main.ias",
    "refinement.main.target",
    "refinement.reference_model.enabled",
    "refinement.pdb_interpretation.peptide_link.ramachandran_restraints",
    "refinement.main.use_experimental_phases",
    "refinement.target_weights.optimize_xyz_weight",
    "refinement.target_weights.optimize_adp_weight",
    "data_manager.fmodel.xray_data.twin_law",
    "data_manager.fmodel.neutron_data.twin_law",
    "refinement.gui.add_hydrogens",
    "refinement.gui.base_output_dir",
    "refinement.gui.send_notification",
    "refinement.gui.notify_email",
    # in dialog windows
    "refinement.refine.adp.group",
    "refinement.refine.adp.individual",
    "refinement.refine.adp.group_adp_refinement_mode",
    "refinement.refine.adp.individual.isotropic",
    "refinement.refine.adp.individual.anisotropic",
    "refinement.refine.sites.individual",
    "refinement.refine.sites.rigid_body",
    "refinement.twinning",
    "refinement.refine.occupancies.individual",
    "refinement.refine.occupancies.remove_selection",
    "refinement.refine.occupancies.constrained_group",
    "refinement.pdb_interpretation.secondary_structure",
  ]
  from phenix.refinement import master_params
  exercise_any(scope_list, master_params,
    "refine_15.eff")

def exercise_phaser () :
  if (not libtbx.env.has_module(name="phaser")):
    print("Skipping exercise_phaser(): phaser module not available.")
    return
  scope_list = [
    "phaser.hklin",
    "phaser.crystal_symmetry.unit_cell",
    "phaser.crystal_symmetry.space_group",
    "phaser.labin",
    "phaser.keywords.resolution.high",
    "phaser.keywords.resolution.auto_high",
    "phaser.solution",
    "phaser.keywords.sgalternative.select",
    "phaser.ensemble",
    "phaser.ensemble.coordinates",
    "phaser.ensemble.coordinates.pdb",
    "phaser.ensemble.coordinates.rmsd",
    "phaser.ensemble.coordinates.identity",
    "phaser.ensemble.coordinates.read_variance_from_pdb_remarks",
    "phaser.ensemble.model_id",
    "phaser.ensemble.disable_check",
    "phaser.ensemble.solution_at_origin",
    "phaser.ensemble.map_hklin",
    "phaser.ensemble.map_labels",
    "phaser.ensemble.map_rms",
    "phaser.ensemble.map_centre",
    "phaser.ensemble.map_extent",
    "phaser.ensemble.map_protein_mw",
    "phaser.ensemble.map_nucleic_mw",
    "phaser.composition.chain",
    "phaser.search",
    # EP
    "phaser.crystal.dataset.hklin",
    "phaser.keywords.partial.pdb",
    "phaser.crystal.pdb_file",
    "phaser.crystal_symmetry.unit_cell",
    "phaser.keywords.hand",
    "phaser.keywords.partial.variance",
    "phaser.keywords.partial.deviation",
    "phaser.crystal.dataset.wavelength",
    "phaser.crystal.dataset.energy",
    "phaser.keywords.llgcompletion.complete",
    "phaser.sad_mode",
    "phaser.keywords.partial.pdb",
    "phaser.keywords.partial.deviation",
    "phaser.keywords.partial.variance",
    "phaser.crystal.dataset.labin",
    "phaser.keywords.general.root",]
  exercise_any(scope_list, "phaser.phenix_interface.master_phil",
   "phaser_mr_1.eff")

if (__name__ == "__main__") :
  eff_tar = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/gui/eff_files.tar.gz",
    test=os.path.isfile)
  assert not easy_run.call("tar zxf %s" % eff_tar)
  exercise_phase_and_build()
  exercise_fit_loops()
  exercise_find_helices_strands()
  exercise_superpose_pdbs()
  exercise_ligand_identification()
  exercise_geometry_minimization()
  exercise_xtriage()
  exercise_maps()
  exercise_fmodel()
  exercise_elbow()
  exercise_cut_out_density()
  exercise_glr()
  exercise_refine()
  exercise_phaser()
  print("OK")
