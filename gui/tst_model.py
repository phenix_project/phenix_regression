from __future__ import print_function

import sys, os
import libtbx.load_env
from wxGUI2 import Model
from iotbx import file_reader

def exercise () :
  #
  # note that 2hr0 has a THC residue that is changed to NWM
  #
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/2hr0.pdb", test=os.path.isfile)
  cif_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/cif_files/2hr0.ligands.cif",
    test=os.path.isfile)
  pdb_in = file_reader.any_file(pdb_file)
  xyz_min, xyz_max = Model.get_pdb_extent(pdb_in.file_object.hierarchy)
  assert ("%.2f %.2f %.2f" % xyz_min) == "-24.35 -47.16 -69.92"
  assert ("%.2f %.2f %.2f" % xyz_max) == "78.45 49.99 100.82"

  pdb_hierarchy = pdb_in.file_object.construct_hierarchy()
  # (unknown1, counts) = Model.identify_unknown_residues(pdb_hierarchy, [])
  # assert unknown1 == ['Q2K']
  cif_object = file_reader.any_file(cif_file).file_object
  (unknown2, counts) = Model.identify_unknown_residues(pdb_hierarchy,
    [cif_object])
  assert unknown2 == []

  for elem in ["se","Se","SE","S","FE","Xe","hg"] :
    assert Model.is_element_name(elem)
  print("OK")

if __name__ == "__main__" :
  exercise()
