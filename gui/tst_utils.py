from __future__ import print_function

# XXX: do not run libtbx.clean_clutter - extra whitespace is for testing

import os, sys, math
from wxGUI2 import utils
from cctbx import uctbx
import iotbx.phil
import libtbx.load_env
from libtbx.utils import Sorry

def exercise () :
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/pdb/2ERL.pdb", test=os.path.isfile)

  extensions = ['mtz','map','py','cif','hkl','phil','pdb','seq','any']
  assert utils.is_list(extensions)
  assert not utils.is_list((None, None))

  # STRING UTILITIES
  #assert (utils.reformat_phil_name("simulated_annealing") ==
  #  utils.reformat_phil_full_name("refinement.main.simulated_annealing") ==
  #  "Simulated annealing")
  master_phil = iotbx.phil.parse("""
refinement {
  main {
    simulated_annealing = True
      .type = bool
    ncs = True
      .type = bool
      .short_caption = NCS restraints
    target = *ml mlhl ml_sad ls
      .type = choice
  }
}
""")
  #phil_object = master_phil.objects[0].objects[0].objects[0]
  #assert utils.get_standard_phil_label(phil_object) == "Simulated annealing"
  #phil_object = master_phil.objects[0].objects[0].objects[1]
  #assert utils.get_standard_phil_label(phil_object) == "NCS restraints"
  assert (utils.words_as_string(["chain", "'A'", "and", "resseq", "1:100"]) ==
          "chain 'A' and resseq 1:100")
  assert utils.words_as_int(["1000"]) == 1000
  assert utils.words_as_float(["1.0"]) == 1.0
  assert utils.get_float_value("1.0") == 1.0
  assert utils.get_float_value("A") is None
  assert utils.words_as_bool(["True"]) == True
  assert utils.words_as_bool(["False"]) == False
  assert utils.words_as_tribool(["Yes"]) == True
  assert utils.words_as_tribool(["False"]) == False
  assert utils.words_as_tribool(["None"]) is None
  assert (utils.clean_string("This fixes &quot;quotes&quot;.") ==
          'This fixes "quotes".')

  assert (utils.get_md5_sum(pdb_file) ==
    b'\xc8\x92P\x9c\xb1\xb1\x83QCM\r\xbeA\x0c"\xd4')

  # Crystallographic utility functions
  unit_cell = uctbx.unit_cell((3,4,5))
  assert (utils.format_unit_cell(unit_cell) == "3 4 5 90 90 90")
  (sg, uc) = utils.extract_symmetry(pdb_file)
  assert (sg, uc) == ("C 1 2 1", "53.91 23.1 23.1 90 110.4 90")
  assert (utils.reformat_unit_cell(uc) == "53.91,23.1,23.1,90,110.4,90")
  assert (utils.compare_conformer("", "A") ==
          utils.compare_conformer("A", "") ==
          utils.compare_conformer("A", "A") ==
          utils.compare_conformer("B", "B") == True)
  assert utils.compare_conformer("A", "B") == False

  assert utils.color_string_converter("(0.0, 1.0, 1.0)") == (0.0, 1.0, 1.0)
  #assert utils.time_converter(1243898180) == "Jun 01 2009 04:16"
  assert utils.time_converter(None) == "unknown"
  assert utils.round(math.pi, 5) == 3.14159
  print("OK")

if __name__ == "__main__" :
  exercise()
