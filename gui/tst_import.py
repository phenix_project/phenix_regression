from __future__ import print_function

import os
import warnings
from libtbx.utils import import_python_object
import libtbx.load_env

ignore_modules = ["Coot", "PyMOL", "Reel"]

def exercise () :
  base_module_dir = libtbx.env.find_in_repositories(
    relative_path="phenix/wxGUI2",
    test=os.path.isdir)
  for file_name in os.listdir(base_module_dir) :
    attempt_import(file_name)
  program_module_dir = base_module_dir + "/Programs"
  for file_name in os.listdir(program_module_dir) :
    attempt_import(file_name, "Programs")

def attempt_import (file_name, module=None) :
  # disable DeprecationWarning
  warnings.filterwarnings("ignore", category=DeprecationWarning)

  base, ext = os.path.splitext(file_name)
  if ext == ".py" and base not in ignore_modules :
    if module is not None :
      path = "wxGUI2.%s.%s" % (module, base)
    else :
      path = "wxGUI2.%s" % base
    py_module = __import__(path)
    return True
  return None

if __name__ == "__main__" :
  try :
    import wx
  except ImportError :
    print("wxPython not available, skipping GUI import check")
  else :
    exercise()
    print("OK")
