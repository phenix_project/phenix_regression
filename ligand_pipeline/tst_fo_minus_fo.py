
from __future__ import division
from __future__ import print_function
from phenix_regression.ligand_pipeline import make_inputs
from iotbx import mtz
from cctbx import sgtbx
from libtbx import easy_run
import shutil
import os
import sys

#
# UNSTABLE!
#
def exercise (verbose=False) :
  if (os.path.isdir("tst_ligand_fo_minus_fo")) :
    shutil.rmtree(os.path.join(os.getcwd(), "tst_ligand_fo_minus_fo"))
  os.mkdir("tst_ligand_fo_minus_fo")
  os.chdir("tst_ligand_fo_minus_fo")
  make_inputs(prefix="tst_ligand_fo_minus_fo", make_apo=True)
  mtz_in = mtz.object("tst_ligand_fo_minus_fo_in.mtz")
  arrays = []
  for array in mtz_in.as_miller_arrays():
    arrays.append(array.customized_copy(
      space_group_info=sgtbx.space_group_info("P2")))
  mtz_new = arrays[0].as_mtz_dataset(column_root_label="F")
  mtz_new.add_miller_array(arrays[1], column_root_label="FreeR_flag")
  mtz_new.mtz_object().write("tst_ligand_fo_minus_fo_in2.mtz")
  input_files = [
    "tst_ligand_fo_minus_fo_start.pdb",
    "tst_ligand_fo_minus_fo_in2.mtz",
    "tst_ligand_fo_minus_fo_seq.fa",
  ]
  assert ([ os.path.isfile(fn) for fn in input_files ] == [True] * 3)
  args = ["phenix.ligand_pipeline"] + input_files + [
    "build=False",
    "nproc=1",
    "ligand_copies=1",
    "ligand_smiles='CC([O-])=O'",
    "ligand_id=ACT",
    "ligandfit.quick=True",
    "mr=False",
    "--optimize",
    "make_subdir=False",
    "ligandfit.map_type=fo_minus_fo",
    "--update_waters",
    "--prune",
    "--refine_ligand_occupancy",
    "--quick_refine",
    "reference_structure=tst_ligand_fo_minus_fo_in.pdb",
    "reference_data.file_name=tst_ligand_fo_minus_fo_apo.mtz",
    "template_pdb=Auto",
  ]
  if (verbose) :
    print(" ".join(args))
    assert (easy_run.call(" ".join(args)) == 0)
  else :
    print(' '.join(args))
    result = easy_run.fully_buffered(" ".join(args)).raise_if_errors()
    assert (result.return_code == 0)
  assert os.path.isfile("ACT_final.pdb")
  from phenix.refinement.benchmark import extract_phenix_refine_r_factors
  r_work, r_free = extract_phenix_refine_r_factors("ACT_final.pdb", True)
  assert (r_free < 0.2)
  lines = open("ACT_final.pdb").readlines()
  have_ligand = False
  for line in lines :
    if line.startswith("HETATM") and (" ACT " in line) :
      have_ligand = True
      break
  assert have_ligand
  lines = open("pipeline.log").readlines()
  rbr = False
  for line in lines :
    assert (not "6_adp" in line) # make sure we run fewer cycles
    if ("FINAL SUMMARY" in line) : # check for rigid-body refinement
      rbr = True
  assert rbr
  print("OK")

if (__name__ == "__main__") :
  try:
    import phaser
  except ImportError:
    print("phaser is not available: skipping test.")
    sys.exit(0)
  exercise(verbose=("--verbose" in sys.argv))
