
from __future__ import division
from __future__ import print_function
from phenix.automation.protocols import import_data
from phenix.automation.protocols import xtriage
import iotbx.phil
from cctbx import uctbx
from cctbx import sgtbx
from libtbx.test_utils import approx_equal
from libtbx.utils import null_out
import libtbx.load_env
import os.path

def exercise_resolution_cutoff () :
  hkl_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/wizards/data/p9_se_w2.sca",
    test=os.path.isfile)
  params = import_data.master_phil.extract()
  params.file_name = hkl_file
  params.space_group = sgtbx.space_group_info("I4")
  params.unit_cell = uctbx.unit_cell((113.949,113.949,32.474,90,90,90))
  result = import_data.import_driver(
    params=params,
    prefix="tst_pipeline",
    verbose=False,
    out=null_out()).run(out=null_out())
  assert params.unmerged_data is not None
  params2 = iotbx.phil.parse(xtriage.xtriage_phil_str).extract()
  params2.min_i_over_sigma = 10
  params2.max_r_merge = 0.2
  result2 = xtriage.run_xtriage(
    mtz_file=result.data_file,
    params=params2,
    prefix="tst_pipeline",
    unmerged_data=params.unmerged_data,
    unmerged_labels=params.unmerged_labels,
    out=null_out()).run(out=null_out())
  assert approx_equal(result2.get_statistic("d_min_cut"), 2.0, eps=0.1)
  params2 = iotbx.phil.parse(xtriage.xtriage_phil_str).extract()
  # it would be nice to try this with data that actually extend well beyond
  # the cutoffs...
  result2 = xtriage.run_xtriage(
    mtz_file=result.data_file,
    params=params2,
    prefix="tst_pipeline",
    unmerged_data=params.unmerged_data,
    unmerged_labels=params.unmerged_labels,
    out=null_out()).run(out=null_out())
  assert (result2.get_statistic("d_min_cut") is None)
  assert approx_equal(result2.get_statistic("d_min_start"), 1.744343)
  assert approx_equal(result2.get_statistic("d_min_end"), 1.744343)

if (__name__ == "__main__") :
  exercise_resolution_cutoff()
  print("OK")
