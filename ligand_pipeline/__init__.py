
from __future__ import division
from libtbx import easy_run
import shutil
import os
import sys

def make_inputs (prefix="tst_ligand_pipeline", make_apo=False, use_chained_ligand=False) :
  pdb_in = """\
CRYST1   25.000   10.000   25.477  90.00 107.08  90.00 P 1 21 1
SCALE1      0.040101  0.000000  0.012321        0.00000
SCALE2      0.000000  0.112790  0.000000        0.00000
SCALE3      0.000000  0.000000  0.041062        0.00000
ATOM      1  N   GLY A   1       8.992   0.474  -6.096  1.00 16.23           N
ATOM      2  CA  GLY A   1       9.033   0.047  -4.707  1.00 16.20           C
ATOM      3  C   GLY A   1       7.998  -1.029  -4.448  1.00 15.91           C
ATOM      4  O   GLY A   1       7.548  -1.689  -5.385  1.00 16.11           O
ATOM      5  N   ASN A   2       7.625  -1.218  -3.185  1.00 15.02           N
ATOM      6  CA  ASN A   2       6.523  -2.113  -2.848  1.00 13.92           C
ATOM      7  C   ASN A   2       5.220  -1.618  -3.428  1.00 12.24           C
ATOM      8  O   ASN A   2       4.955  -0.418  -3.432  1.00 11.42           O
ATOM      9  CB  ASN A   2       6.376  -2.261  -1.340  1.00 14.42           C
ATOM     10  CG  ASN A   2       7.620  -2.786  -0.697  1.00 13.92           C
ATOM     11  OD1 ASN A   2       8.042  -3.915  -0.978  1.00 14.39           O
ATOM     12  ND2 ASN A   2       8.232  -1.975   0.168  1.00 12.78           N
ATOM     13  N   ASN A   3       4.406  -2.553  -3.904  1.00 12.20           N
ATOM     14  CA  ASN A   3       3.164  -2.226  -4.594  1.00 11.81           C
ATOM     15  C   ASN A   3       1.925  -2.790  -3.910  1.00 10.59           C
ATOM     16  O   ASN A   3       1.838  -3.991  -3.653  1.00 10.32           O
ATOM     17  CB  ASN A   3       3.231  -2.727  -6.046  1.00 12.51           C
ATOM     18  CG  ASN A   3       1.973  -2.405  -6.848  1.00 12.59           C
ATOM     19  OD1 ASN A   3       1.662  -1.239  -7.106  1.00 13.64           O
ATOM     20  ND2 ASN A   3       1.260  -3.443  -7.268  1.00 12.39           N
ATOM     21  N   GLN A   4       0.973  -1.913  -3.608  1.00 10.34           N
ATOM     22  CA  GLN A   4      -0.366  -2.335  -3.208  1.00 10.00           C
ATOM     23  C   GLN A   4      -1.402  -1.637  -4.085  1.00 10.21           C
ATOM     24  O   GLN A   4      -1.514  -0.414  -4.070  1.00  8.99           O
ATOM     25  CB  GLN A   4      -0.656  -2.027  -1.736  1.00 10.00           C
ATOM     26  CG  GLN A   4      -1.927  -2.705  -1.229  1.00 10.50           C
ATOM     27  CD  GLN A   4      -2.482  -2.102   0.060  1.00 11.36           C
ATOM     28  OE1 GLN A   4      -2.744  -0.900   0.151  1.00 12.29           O
ATOM     29  NE2 GLN A   4      -2.684  -2.951   1.055  1.00 10.43           N
ATOM     30  N   GLN A   5      -2.154  -2.406  -4.857  1.00 10.48           N
ATOM     31  CA  GLN A   5      -3.247  -1.829  -5.630  1.00 11.24           C
ATOM     32  C   GLN A   5      -4.591  -2.382  -5.178  1.00 11.40           C
ATOM     33  O   GLN A   5      -4.789  -3.599  -5.092  1.00 11.94           O
ATOM     34  CB  GLN A   5      -3.024  -2.023  -7.129  1.00 11.14           C
ATOM     35  CG  GLN A   5      -1.852  -1.222  -7.653  1.00 10.65           C
ATOM     36  CD  GLN A   5      -1.338  -1.748  -8.965  1.00 10.73           C
ATOM     37  OE1 GLN A   5      -0.794  -2.845  -9.028  1.00 10.14           O
ATOM     38  NE2 GLN A   5      -1.511  -0.968 -10.027  1.00 11.31           N
ATOM     39  N   ASN A   6      -5.504  -1.471  -4.872  1.00 11.56           N
ATOM     40  CA  ASN A   6      -6.809  -1.838  -4.359  1.00 12.07           C
ATOM     41  C   ASN A   6      -7.856  -1.407  -5.353  1.00 13.18           C
ATOM     42  O   ASN A   6      -8.257  -0.251  -5.362  1.00 13.64           O
ATOM     43  CB  ASN A   6      -7.053  -1.149  -3.017  1.00 12.12           C
ATOM     44  CG  ASN A   6      -5.966  -1.446  -1.998  1.00 12.31           C
ATOM     45  OD1 ASN A   6      -5.833  -2.579  -1.517  1.00 13.43           O
ATOM     46  ND2 ASN A   6      -5.198  -0.423  -1.645  1.00 11.88           N
ATOM     47  N   TYR A   7      -8.298  -2.332  -6.193  1.00 14.34           N
ATOM     48  CA  TYR A   7      -9.162  -1.980  -7.317  1.00 15.00           C
ATOM     49  C   TYR A   7     -10.603  -1.792  -6.893  1.00 15.64           C
ATOM     50  O   TYR A   7     -11.013  -2.278  -5.838  1.00 15.68           O
ATOM     51  CB  TYR A   7      -9.064  -3.041  -8.412  1.00 15.31           C
ATOM     52  CG  TYR A   7      -7.657  -3.197  -8.931  1.00 15.06           C
ATOM     53  CD1 TYR A   7      -6.785  -4.118  -8.368  1.00 15.24           C
ATOM     54  CD2 TYR A   7      -7.193  -2.400  -9.960  1.00 14.96           C
ATOM     55  CE1 TYR A   7      -5.489  -4.253  -8.830  1.00 14.94           C
ATOM     56  CE2 TYR A   7      -5.905  -2.526 -10.429  1.00 15.13           C
ATOM     57  CZ  TYR A   7      -5.055  -3.451  -9.861  1.00 14.97           C
ATOM     58  OH  TYR A   7      -3.768  -3.572 -10.335  1.00 14.93           O
ATOM     59  OXT TYR A   7     -11.378  -1.149  -7.601  1.00 15.89           O
TER
"""
  pdb_in_ligand = """\
HETATM   60  C   ACT     1      -0.908  -5.503  -5.572  1.00 18.56           C
HETATM   61  O   ACT     1       0.145  -5.391  -6.241  1.00 19.26           O
HETATM   62  OXT ACT     1      -1.449  -4.510  -5.051  1.00 18.36           O
HETATM   63  CH3 ACT     1      -1.540  -6.820  -5.404  1.00 18.08           C
"""
  pdb_in_ligand_with_chain = """\
HETATM   60  C   ACT L   1      -0.908  -5.503  -5.572  1.00 18.56           C
HETATM   61  O   ACT L   1       0.145  -5.391  -6.241  1.00 19.26           O
HETATM   62  OXT ACT L   1      -1.449  -4.510  -5.051  1.00 18.36           O
HETATM   63  CH3 ACT L   1      -1.540  -6.820  -5.404  1.00 18.08           C
"""
  pdb_in_water = """\
HETATM   64  O   HOH S   1     -10.466  -2.347  -3.168  1.00 17.57           O
HETATM   65  O   HOH S   2       6.469   1.081  -7.070  1.00 21.27           O
HETATM   66  O   HOH S   3     -11.809   0.108  -9.956  1.00 27.52           O
HETATM   67  O   HOH S   4       1.580  -3.455 -11.035  1.00 44.76           O
"""
  if use_chained_ligand:
    open("%s_in.pdb" % prefix, "w").write(pdb_in + pdb_in_ligand_with_chain +
      pdb_in_water)
  else:
    open("%s_in.pdb" % prefix, "w").write(pdb_in + pdb_in_ligand +
      pdb_in_water)
  params = """
    high_resolution = 1.75
    r_free_flags_fraction = 0.1
    add_sigmas = True
    random_seed = 12345
    output {
      label = F
      type = *real complex
      file_name = %s_in.mtz
    }
  """ % prefix
  open("%s_fmodel.eff" % prefix, "w").write(params)
  assert (easy_run.fully_buffered(
      "phenix.fmodel %s_in.pdb %s_fmodel.eff" % (prefix, prefix)
    ).raise_if_errors().return_code == 0)
  open("%s_start.pdb" % prefix, "w").write(pdb_in)
  if (make_apo) :
    open("%s_apo.pdb" % prefix, "w").write(pdb_in + pdb_in_water)
    params = """
      high_resolution = 1.75
      r_free_flags_fraction = 0.1
      add_sigmas = True
      random_seed = 12345
      output {
        label = F
        type = *real complex
        file_name = %s_apo.mtz
      }
    """ % prefix
    open("%s_apo_fmodel.eff" % prefix, "w").write(params)
    assert (easy_run.fully_buffered(
      "phenix.fmodel %s_apo.pdb %s_apo_fmodel.eff" % (prefix, prefix)
    ).raise_if_errors().return_code == 0)
  open("%s_seq.fa" % prefix, "w").write("> 1yjp\nGNNQQNY")
