
# test for incorporation of mmtbx.select_best_starting_model

from __future__ import division
from __future__ import print_function
from phenix_regression.ligand_pipeline import make_inputs
import iotbx.pdb
from scitbx.array_family import flex
from libtbx import easy_run
from libtbx.utils import format_cpu_times
import warnings
import shutil
import os
import sys

def exercise (verbose=False) :
  if (os.path.isdir("tst_pipeline_select_model")) :
    shutil.rmtree(os.path.join(os.getcwd(), "tst_pipeline_select_model"))
  os.mkdir("tst_pipeline_select_model")
  os.chdir("tst_pipeline_select_model")
  make_inputs(prefix="tst_ligand_pipeline", make_apo=False)
  pdb_in = iotbx.pdb.input("tst_ligand_pipeline_start.pdb")
  pdb_h = pdb_in.construct_hierarchy()
  sel_cache = pdb_h.atom_selection_cache()
  cs = pdb_in.crystal_symmetry()
  sel1 = sel_cache.selection(
    "name C or name N or name O or name CA or name CB or name CG")
  pdb_trunc = pdb_h.select(sel1)
  open("tst_ligand_pipeline_start_1.pdb", "w").write(
    pdb_trunc.as_pdb_string(crystal_symmetry=cs))
  atoms = pdb_h.atoms()
  shift = flex.vec3_double(atoms.size(), (0.5,0.5,0.5))
  atoms.set_xyz(atoms.extract_xyz() + shift)
  open("tst_ligand_pipeline_start_2.pdb", "w").write(
    pdb_h.as_pdb_string(crystal_symmetry=cs))
  input_files = [
    "tst_ligand_pipeline_in.mtz",
    "tst_ligand_pipeline_seq.fa",
    "tst_ligand_pipeline_start_1.pdb",
    "tst_ligand_pipeline_start_2.pdb",
  ]
  assert ([ os.path.isfile(fn) for fn in input_files ] == [True] * 4)
  args = ["phenix.ligand_pipeline"] + input_files + [
    "build=False",
    "nproc=1",
    "copies=1",
    "ligand_code=ACT",
    "ligandfit.quick=True",
    "after_mr.cycles=2",
    "after_mr.update_waters=False",
    "after_ligand.cycles=3",
    "after_ligand.optimize_weights=False",
    "mr=Auto",
    "make_subdir=False",
    "hydrogens=False",
    "space_group=P2",
    "unit_cell=25.00,10.000,25.477,90.00,107.08,90.00",
    "--export_for_web",
  ]
  if (verbose) :
    assert (easy_run.call(" ".join(args)) == 0)
  else :
    result = easy_run.fully_buffered(" ".join(args)).raise_if_errors()
    assert (result.return_code == 0), "\n".join(result.stdout_lines)
  assert not os.path.isdir("phaser_0") # Phaser should not be run!
  assert os.path.isfile("ACT_final.pdb")
  assert os.path.isfile("final_ligands.json")
  assert os.path.isfile("final_1_2Fo-Fc.dsn6")
  from phenix.refinement.benchmark import extract_phenix_refine_r_factors
  r_work, r_free = extract_phenix_refine_r_factors("ACT_final.pdb", True)
  assert (r_free < 0.2)
  lines = open("ACT_final.pdb").readlines()
  have_ligand = False
  for line in lines :
    if line.startswith("HETATM") and (" ACT " in line) :
      have_ligand = True
      break
  assert have_ligand
  print(format_cpu_times())
  print("OK")

if (__name__ == "__main__") :
  try:
    import phaser # import dependency
  except ImportError:
    warnings.warn("Skipping test because Phaser is not available")
  else :
    exercise(verbose=("--verbose" in sys.argv))
