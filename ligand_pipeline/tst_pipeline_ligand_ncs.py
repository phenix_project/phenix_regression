
from __future__ import division
from __future__ import print_function
from mmtbx.regression import tst_ligand_ncs
import iotbx.pdb
from libtbx import easy_run
import libtbx.load_env
import os.path as op
import shutil
import os
import sys

def exercise () :
  prefix = "tst_pipeline_ligand_ncs"
  if op.isdir(prefix) :
    shutil.rmtree(prefix)
  os.mkdir(prefix)
  os.chdir(prefix)
  pdb_file, mtz_file = tst_ligand_ncs.make_inputs(prefix=prefix)
  pdb_new = prefix + "_start.pdb"
  pdb_in = iotbx.pdb.input(pdb_file)
  hierarchy = pdb_in.construct_hierarchy()
  sel_cache = hierarchy.atom_selection_cache()
  keep_sel = sel_cache.selection("not (resname ACT)")
  hierarchy = hierarchy.select(keep_sel)
  open(pdb_new, "w").write(hierarchy.as_pdb_string(
    crystal_symmetry=pdb_in.crystal_symmetry()))
  act_cif = libtbx.env.find_in_repositories(
    relative_path="chem_data/geostd/a/data_ACT.cif",
    test=op.isfile)
  assert (act_cif is not None)
  args = [
    "phenix.ligand_pipeline",
    pdb_new,
    mtz_file,
    "rigid_body=False",
    "real_space=False",
    "after_mr.cycles=1",
    "after_ligand.cycles=1",
    "hydrogens=False",
    "ligand=%s" % act_cif,
    "ligand_copies=2",
    "ligandfit.first_only=True",
    "min_cc_reference=0.8",
    "--quick_refine",
    "--skip_mr", # XXX test backwards compatibility
    "nproc=1",
    "prune=False",
    "build=False",
    "validate=False",
    "make_subdir=False",
    "ligandfit.quick=True",
  ]
  print(" ".join(args))
  # result = easy_run.call(" ".join(args)).raise_if_errors()
  result = easy_run.fully_buffered(" ".join(args)).raise_if_errors()
  assert not op.isdir("phaser_0")
  assert (result.return_code == 0)
  assert ("attempting to use NCS to improve score(s)" in
          "\n".join(result.stdout_lines))
  pdb_out = iotbx.pdb.input("data_ACT_final.pdb")
  hierarchy = pdb_out.construct_hierarchy()
  assert hierarchy.overall_counts().resnames['ACT'] == 2

if (__name__ == "__main__") :
  if (os.uname()[-1] == "i686") : # FIXME infinite loop?
    sys.stderr.write("WARNING: skipping test on i686\n")
  else :
    exercise()
    print("OK")
