
from __future__ import division
from __future__ import print_function
from phenix_regression.ligand_pipeline import make_inputs
from libtbx import easy_run
from libtbx.utils import format_cpu_times
import warnings
import shutil
import os
import sys

def exercise (verbose=False) :
  if (os.path.isdir("tst_ligand_pipeline_no_lig")) :
    shutil.rmtree(os.path.join(os.getcwd(), "tst_ligand_pipeline_no_lig"))
  os.mkdir("tst_ligand_pipeline_no_lig")
  os.chdir("tst_ligand_pipeline_no_lig")
  make_inputs(prefix="tst_ligand_pipeline", make_apo=False)
  input_files = [
    "tst_ligand_pipeline_start.pdb",
    "tst_ligand_pipeline_in.mtz",
    "tst_ligand_pipeline_seq.fa",
  ]
  assert ([ os.path.isfile(fn) for fn in input_files ] == [True] * 3)
  args = ["phenix.ligand_pipeline"] + input_files + [
    "build=False",
    "nproc=1",
    "copies=1",
    "xray_data.twin_law=None",
    "mr=False",
    "--skip_ligand",
    "phaser.d_min=2.8",
    "after_mr.cycles=3",
    "after_ligand.cycles=3",
    "--optimize",
    "after_ligand.optimize_weights=False",
    "make_subdir=False",
    "hydrogens=False",
    "space_group=P2",
    "unit_cell=25.00,10.000,25.477,90.00,107.08,90.00",
    "--update_waters",
    "--prune",
    "reference_structure=tst_ligand_pipeline_in.pdb",
    "template_pdb=Auto",
  ]
  print(" ".join(args))
  if (verbose) :
    assert (easy_run.call(" ".join(args)) == 0)
  else :
    result = easy_run.fully_buffered(" ".join(args)).raise_if_errors()
    assert (result.return_code == 0)
  assert os.path.isfile("refine_final.pdb")
  from phenix.refinement.benchmark import extract_phenix_refine_r_factors
  r_work, r_free = extract_phenix_refine_r_factors("refine_final.pdb", True)
  assert (r_free < 0.2)
  lines = open("refine_0/refine_refine_1.log").readlines()
  rbr = False
  for line in lines :
    assert (not "6_adp" in line) # make sure we run fewer cycles
    if ("1_rbr" in line) : # check for rigid-body refinement
      rbr = True
  assert rbr
  print(format_cpu_times())
  print("OK")

if (__name__ == "__main__") :
  try:
    import phaser
  except ImportError:
    warnings.warn("Skipping test because Phaser is not available")
  else :
    exercise(verbose=("--verbose" in sys.argv))
